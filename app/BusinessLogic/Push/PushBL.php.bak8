<?php
namespace App\BusinessLogic\Push;

use Log;
use Exception;
use App\Core\Push\PushManager;
use App\Core\Push\PushKey;
use App\Models\Database\PushTokenAgents;
use App\Models\Database\MasterAgent;
use App\Models\Database\Agent;
use App\Jobs\Push\PushJob;


class PushBL {

	//Push modul
	private $pushModul = null;

	//Scope data
	private $agentIDs = [];
	private $masterAgentIDs = [];
	private $whitelabelID = 0;

	//Target type and scope
	private $targetScope = 0;
	private $targetType = 0;

	//Topic
	private $topic = [];

	//Content
	private $option = [];
	private $notification = [];
	private $data = [];
	private $isSetOption = false;
	private $isSetNotification = false;

	private $title = '';
	private $body = '';
	private $platform = PushKey::PLATFORM_ANDROID;
	private $protocol = PushKey::PROTOCOL_GOOGLE_FCM;

	//Use queue
	private $isUseQueue = true;

	//Target device
	private $targetDevice = PushKey::TARGET_DEVICE_ANDROID;

	//Push schedule
	private $delayInMinutes = 0;

	public function __construct($whitelabelID, $platform = PushKey::PLATFORM_ANDROID, 
		$protocol = PushKey::PROTOCOL_GOOGLE_FCM) {
		Log::info('PushManager->__construct: constructing....');
		$this->platform = $platform;
		$this->protocol = $protocol;
		$pushMgr = new PushManager($this->platform, $this->protocol);
		$this->pushModul = $pushMgr->getPushModule();
		$this->whitelabelID = $whitelabelID;
	}

	public function sendPush() {
		$result = null;
		switch ($this->platform) {
			case PushKey::PLATFORM_ANDROID:
				if ($this->protocol == PushKey::PROTOCOL_GOOGLE_FCM) {
					 //Google FCM Push Service
					$result = $this->sendFCMPush();
				} else {
					//Google Non FCM Push Service
				}
				break;
			case PushKey::PLATFORM_IOS:
				//Apple Push Service
				break;
			case PushKey::PLATFORM_OTHER:
				//Other Push Service
				break;		
		}
		return $result;	
	}

	/* Set Target */
	public function setWhitelabel($whitelabelID) {
		$this->whitelabelID = $whitelabelID;
	}

	public function setMasterAgent($masterAgentIDs=[]) {
		$this->masterAgentIDs = $masterAgentIDs;
	}

	public function setAgent($agentIDs=[]) {
		$this->agentIDs = $agentIDs;
	}

	public function setTopics($topics=[]) {
		$this->topics = $topics;
	}

	public function useToken($targetScope = PushKey::SCOPE_WHITELABEL) {
		$this->targetType = PushKey::TARGET_TYPE_TOKEN;
		$this->targetScope = $targetScope;
	}

	public function useTopic($topic = []) {
		$this->targetType = PushKey::TARGET_TYPE_TOPIC;
		$this->topic = $topic;
	}

	public function useQueue($isUseQueue) {
		$this->isUseQueue = $isUseQueue;
	}

	public function setTargetDevice($targetDevice) {
		$this->targetDevice = $targetDevice;
	}

	/* Compose content for both android and apple */
	public function composeRefreshPush($refreshTarget = PushKey::CONTENT_REFRESH_MENU, $saveToInbox = true) {
		$this->data['refresh_target'] = $refreshTarget;
		$this->data['save_to_inbox'] = $saveToInbox;
	}

	public function composeTextPush($text, $title, $body, $saveToInbox = true) {
		$this->data['push_content'] = PushKey::CONTENT_TEXT;
		$this->data['text'] = $text;
		$this->data['save_to_inbox'] = $saveToInbox;
		$this->title = $title;
		$this->body = $body;
	}

	public function composeURLPush($url, $urlTitle, $title, $body, $openBrowser = false, $saveToInbox = true) {
		$this->data['push_content'] = PushKey::CONTENT_URL;
		$this->data['url'] = $url;
		$this->data['url_title'] = $urlTitle;
		$this->data['open_browser'] = $openBrowser;
		$this->data['save_to_inbox'] = $saveToInbox;
		$this->title = $title;
		$this->body = $body;
	}

	public function composeImagePush($imageUrl, $title, $body, $saveToInbox = true) {
		$this->data['push_content'] = PushKey::CONTENT_IMAGE;
		$this->data['image_url'] = $imageUrl;
		$this->data['save_to_inbox'] = $saveToInbox;
		$this->title = $title;
		$this->body = $body;
	}

	public function composeClickableImagePush($imageUrl, $url, $title, $body, $openBrowser, $saveToInbox = true ) {
		$this->data['push_content'] = PushKey::CONTENT_CLICKABLE_IMAGE;
		$this->data['image_url'] = $imageUrl;
		$this->data['url'] = $url;
		$this->data['open_browser'] = $openBrowser;
		$this->data['save_to_inbox'] = $saveToInbox;
		$this->title = $title;
		$this->body = $body;
	}

	public function setDeliveryTime($t=null) {
		try {
			if (!is_null($t)) {
				$start_date = now();
				$since_start = $start_date->diff(new \DateTime($t));
				$minutes = $since_start->days * 24 * 60;
				$minutes += $since_start->h * 60;
				$minutes += $since_start->i;	
				$this->delayInMinutes = $minutes;			
			}
		} catch(Exception $e) {
			Log::error('PushBL->setDeliveryTime: exception at [Line='.$e->getLine().',Code='.$e->getCode().'],'
				.$e->getMessage());
		}
		Log::info('PushBL->setDeliveryTime: push will be send in the next '.$this->delayInMinutes.' minutes');
	}

	/* Get token */
	private function getTokenOfWhitelabel() {
		Log::info("PushBL->getTokenOfWhitelabel: started getting token for whitelabelID=".$this->whitelabelID);
		$tokens = array();
		$selectedToken = null;
		$pushTokenAgents = null;
		if (!empty($this->whitelabelID)) {
			$pushTokenAgents = PushTokenAgents::where('white_label_id',$this->whitelabelID)
				->where('white_label_id',$this->whitelabelID)
				->get();		
			if (!is_null($pushTokenAgents)) {
				foreach ($pushTokenAgents as $token) {
					array_push($tokens,(string)$token->token_standard);   
				}
			}
			if (count($tokens) > 0) {
				if (count($tokens) > 1) {
					Log::info('PushBL->getTokenOfWhitelabel : target is multiple agent');
					$selectedToken = $tokens;
				} else {
					$selectedToken = $tokens[0];
					Log::info('PushBL->getTokenOfWhitelabel : target is single agent');
				}	
			} else {
				Log::info('PushBL->getTokenOfWhitelabel : target is empty');
			}
		}
		return $selectedToken;
	}

	private function getTokenOfMasterAgent() {
		//Find all agent's token for selected master agent. Direct agents and Indirect agents.
		Log::info('PushBL->getTokenOfMasterAgent : getting all agent\'s token in master agent ');
		$pushTokenAgents = [];
		if (isset($this->masterAgentIDs)) {
			if (!empty($this->masterAgentIDs)) {
				if (is_array($this->masterAgentIDs)) {
					//Is an array
					foreach ($this->masterAgentIDs as $maL1ID) {
						//Get direct agent of maL1
						$at = $this->getMAAgent($maL1ID);
						array_merge($pushTokenAgents,$at);						
						//Get downline: MA L2 bellow current MA (L1)
						$downlineMAL2 = MasterAgent::where('master_agent_parent_id',$maL1ID)->get();
						foreach ($downlineMAL2 as $maL2) {
							//Get direct agent of MA L2
							$at = $this->getMAAgent($maL2->id);
							array_merge($pushTokenAgents,$at);
							//Get downline: MA L3 bellow current MA (L2)
							$downlineMAL3 = MasterAgent::where('master_agent_parent_id',$maL2->id)->get();
							foreach ($downlineMAL3 as $maL3) {
								//Get direct agent of MA L3
								$at = $this->getMAAgent($maL3->id);
								array_merge($pushTokenAgents,$at);							
							} //maL3
						} //maL2
					} //maL1ID
				} else {
					//Is string, single MA
					//Get direct agent 
					$at = $this->getMAAgent($this->masterAgentIDs);
					array_merge($pushTokenAgents,$at);
					//Search MA L2 bellow current MA (L1)
					$downlineMAL2 = MasterAgent::where('master_agent_parent_id',$this->masterAgentIDs)->get();
					foreach ($downlineMAL2 as $maL2) {
						//Get direct agent of MA L2
						$at = $this->getMAAgent($maL2->id);
						array_merge($pushTokenAgents,$at);
						//Search MA L3 bellow current MA (L2)
						$downlineMAL3 = MasterAgent::where('master_agent_parent_id',$maL2->id)->get();
						foreach ($downlineMAL3 as $maL3) {
							//Get direct agent of MA L3
							$at = $this->getMAAgent($maL3->id);
							array_merge($pushTokenAgents,$at);
						} //maL3
					} //maL2
				}
			} else {
				Log::info('PushBL->getTokenOfMasterAgent : master agent is empty');
			}
		} else {
			Log::info('PushBL->getTokenOfMasterAgent : master agent is not defined');
		}
		$selectedToken = null;
		if (count($pushTokenAgents) > 0) {
			if (count($pushTokenAgents) > 1) {
				Log::info('PushBL->getTokenOfMasterAgent : target is multiple agent');
				$selectedToken = $pushTokenAgents;
			} else {
				$selectedToken = $pushTokenAgents[0];
				Log::info('PushBL->getTokenOfMasterAgent : target is single agent');
			}	
		} else {
			Log::info('PushBL->getTokenOfMasterAgent : target is empty');
		}
		return $selectedToken;
	}

	private function getTokenOfAgent() {
		Log::info("PushBL->getTokenOfMasterAgent: started getting token for agent=".json_encode($this->masterAgentIDs));
		$tokens = array();
		$selectedToken = null;
		$pushTokenAgents = null;
		if (is_array($this->agentIDs)) {
			$pushTokenAgents = PushTokenAgents::where('white_label_id',$this->whitelabelID)
				->whereIn('agent_id',$this->agentIDs)
				->get();
		} else {
			$pushTokenAgents = PushTokenAgents::where('white_label_id',$this->whitelabelID)
				->where('agent_id',$this->agentIDs)
				->get();
		}
		if (!is_null($pushTokenAgents)) {
			foreach ($pushTokenAgents as $token) {
				array_push($tokens,(string)$token->token_standard);   
			}
		}
		if (count($tokens) > 0) {
			if (count($tokens) > 1) {
				Log::info('PushBL->getTokenOfAgent : target is multiple agent');
				$selectedToken = $tokens;
			} else {
				$selectedToken = $tokens[0];
				Log::info('PushBL->getTokenOfAgent : target is single agent');
			}	
		} else {
			Log::info('PushBL->getTokenOfAgent : target is empty');
		}
		return $selectedToken;
	}

	private function sendFCMPush() {
		$result = array(); 
		$version = 1;	
		try {
			if (!is_null($this->pushModul)) {
				$pushTarget = null;
				if (!$this->isSetNotification) {
					Log::info('PushBL->sendFCMPush: set notification to default');
					$this->setFCMNotification();
				}
				if (!$this->isSetOption) {
					Log::info('PushBL->sendFCMPush: set option to default');
					$this->setFCMOption();
				}
				switch ($this->targetType) {
					case PushKey::TARGET_TYPE_TOKEN:
						Log::info('PushBL->sendFCMPush: target is by token.');
						switch($this->targetScope) {
							case PushKey::SCOPE_WHITELABEL :
								Log::info('PushBL->sendFCMPush: whitelabel scope');
								$pushTarget = $this->getTokenOfWhitelabel();
								break;
							case PushKey::SCOPE_MASTER_AGENT :
								Log::info('PushBL->sendFCMPush: master agent scope');
								$pushTarget = $this->getTokenOfMasterAgent();
								break;
							case PushKey::SCOPE_AGENT :
								Log::info('PushBL->sendFCMPush: agent scope');
								$pushTarget = $this->getTokenOfAgent();
								break;
						}
						if (!is_null($pushTarget) ) {
							if (!empty($pushTarget)) {
								if (is_array($pushTarget)) {
									if (count($pushTarget) > PushKey::MAX_TOKEN_PER_PUSH) {
										//Token is more than 1000. We will send push to each 1000 tokens.
										Log::info('PushBL->sendFCMPush: token is > 1000');
										$start = 0;
										$n = PushKey::MAX_TOKEN_PER_PUSH;
										$i = 1;
										while ($start < count($pushTarget)) {
											$selectedTarget = array_slice($pushTarget,$start,$n);
											$start = $start + $n;
											if (!$this->isUseQueue) {
												//Send push whithout queue
												$rs = $this->pushModul->sendPush($this->option, $this->notification, 
													$this->data, $this->targetType, $selectedTarget, $this->targetDevice);
												array_push($result,$rs);
											} else {
												//Send push with queue
												$this->enqueuePush($selectedTarget, $this->targetDevice, $version);
												array_push($result,"Push sent via queue");
											}
											$i = $i + 1;		
										}									
									} else {
										//Token is less than 1000 
										Log::info('PushBL->sendFCMPush: token is < 1000. It has '.count($pushTarget).' token');
										if (!$this->isUseQueue) {
											//Send push without queue
											$rs = $this->pushModul->sendPush($this->option, $this->notification, $this->data, 
													$this->targetType, $pushTarget, $this->targetDevice);
											array_push($result,$rs);
										} else {
											//Send push with queue
											$this->enqueuePush($pushTarget, $this->targetDevice, $version);
											array_push($result,"Push sent via queue");
										}
									}
								} else {
									Log::info('PushBL->sendFCMPush: single token');
									if (!$this->isUseQueue) {
										//Send push without queue
										$rs = $this->pushModul->sendPush($this->option, $this->notification, $this->data, 
												$this->targetType, $pushTarget, $this->targetDevice);
										array_push($result,$rs);
									} else {
										//Send push with queue
										$this->enqueuePush($pushTarget, $this->targetDevice, $version);
										array_push($result,"Push sent via queue");
									}
								}
							}
						}	
					break;
					case PushKey::TARGET_TYPE_TOPIC:
						Log::info('PushBL->sendFCMPush: target is by topic.');
						$pushTarget = $this->topic;
						if (!is_null($pushTarget) ) {
							if (!empty($pushTarget)) {
								if (is_array($pushTarget)) {
									foreach ($pushTarget as $targetItem) {
										if (!$this->isUseQueue) {
											//Send push without queue
											$rs = $this->pushModul->sendPush($this->option, $this->notification, $this->data, 
													$this->targetType, $targetItem, $this->targetDevice);
											array_push($result,$rs);
										} else {
											//Send push with queue
											$this->enqueuePush($targetItem, $this->targetDevice, $version);
											array_push($result,"Push sent via queue");
										}
									}	
								} else {
									if (!$this->isUseQueue) {
										//Send push without queue
										$rs = $this->pushModul->sendPush($this->option, $this->notification, $this->data, 
												$this->targetType, $pushTarget, $this->targetDevice);
										array_push($result,$rs);
									} else {
										//Send push with queue
										$this->enqueuePush($pushTarget, $this->targetDevice, $version);
										array_push($result,"Push sent via queue");
									}
								}
							}
						}
					break;
				}
			} else {
				Log::error("PushBL->sendPush: push modul is not selected.");
			}
		} catch (Exception $e) {
			Log::info("PushBL->sendFCMPush: exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
		}
		return $result;	
	}

	/* Set Option for Google FCM */
	public function setFCMOption ($TTL = 86400, $delayWhileIddle = false ,$androidPriority = PushKey::PRIORITY_ANDROID_HIGH, $applePriority = PushKey::PRIORITY_APPLE_9) {
		Log::info('PushBL->setOption: started...');
		$this->option[PushKey::OPTION_DELAY_WHILE_IDLE] = $delayWhileIddle;
		$this->option[PushKey::OPTION_TTL] = $TTL;
		$this->option[PushKey::OPTION_PRIORITY_ANDROID] = $androidPriority;
		$this->option[PushKey::OPTION_PRIORITY_APPLE] = $applePriority;
		Log::info('PushBL->setOption: done');
		Log::info('PushBL->setOption: option='.json_encode($this->option));
		$this->isSetOption = true;
	}

	public function subscribeFCMTopicBatch($topicName, $inputTokens=null) {
		if (isset($inputTokens)) {
			if (!is_null($inputTokens)) {
				if (is_array($inputTokens)) {
					Log::info('PushBL->subscribeFCMTopicBatch: subscribe to topic with token');
					$result = $this->pushModul->subscribeTopicBatch($topicName, $inputTokens);
				}
			} else {
				Log::info('PushBL->subscribeFCMTopicBatch: subscribe to topic with agent, whitelable or master agent');
				$tokens = [];
				switch($this->targetScope) {
					case PushKey::SCOPE_WHITELABEL :
						$tokens = $this->getTokenOfWhitelabel();
						break;
					case PushKey::SCOPE_MASTER_AGENT :
						$tokens = $this->getTokenOfMasterAgent();
						break;
					case PushKey::SCOPE_AGENT :
						$tokens = $this->getTokenOfAgent();
						break;
				}
				$result = $this->pushModul->subscribeTopicBatch($topicName, $tokens);
			}
		} 
		return $result;
	}

	public function unsubscribeFCMTopicBatch($topicName) {
		switch($this->targetScope) {
			case PushKey::SCOPE_WHITELABEL :
				$tokens = $this->getTokenOfWhitelabel();
				break;
			case PushKey::SCOPE_MASTER_AGENT :
				$tokens = $this->getTokenOfMasterAgent();
				break;
			case PushKey::SCOPE_AGENT :
				$tokens = $this->getTokenOfAgent();
				break;
		}
		$result = $this->pushModul->unsubscribeTopicBatch($topicName, $tokens);
		return $result;
	}

	/* Set notification Google FCM */
	public function setFCMNotification($sound = false, $icon = '', $tag = '', $color = '', 
		$badge = '', $subTitle = '') {
		$this->notification[PushKey::FIELD_NOTIF_TITLE] = $this->title;
		$this->notification[PushKey::FIELD_NOTIF_BODY] = $this->body;
		$this->notification[PushKey::FIELD_NOTIF_SOUND] = $sound;
		$this->notification[PushKey::FIELD_NOTIF_ICON] = $icon;
		$this->notification[PushKey::FIELD_NOTIF_TAG] = $tag;
		$this->notification[PushKey::FIELD_NOTIF_COLOR] = $color;
		$this->notification[PushKey::FIELD_NOTIF_BADGE] = $badge;
		$this->notification[PushKey::FIELD_NOTIF_SUBTITLE] = $subTitle;
		$this->isSetNotification = true;
	}

	public function sendPushWithQueue($option, $notification, $data, $targetType, $pushTarget, $targetDevice) {
		$result = null;
		switch ($this->platform) {
			case PushKey::PLATFORM_ANDROID:
				$result = $this->pushModul->sendPush($option, $notification, $data, $targetType, 
					$pushTarget, $targetDevice);
				Log::info('PushBL->sendPushWithQueue: result='.json_encode($result));
				break;
			case PushKey::PLATFORM_IOS:
				//Not available yet
				break;
			case PushKey::PLATFORM_OTHER:
				//Not available yet
				break;
		}
		return $result;			
	}

	private function enqueuePush($pushTarget,$targetDevice, $version) {
		$queuename = "fintechapi_log";
		$pushJob = new PushJob($this->whitelabelID, $this->option, $this->notification, 
			$this->data, $this->targetType, $pushTarget, $targetDevice , 
			$this->platform, $this->protocol,$version);
		if (! empty($queuename)) {
			$pushJob->onQueue($queuename);
			$pushJob->delay(now()->addMinutes($this->delayInMinutes));
		}
		dispatch( $pushJob );
	}

	private function getMAAgent($maID) {
		$tokens = null;
		$agentTokens = [];
		$agents = Agent::where('master_agent_id',$maID)->get();
		foreach ($agents as $a) {
			//Get agent token
			$tokens = PushTokenAgents::where('white_label_id',$this->whitelabelID)
				->where('agent_id',$a->id)
				->get();
			foreach ($tokens as $t) {
				if (!is_null($t->token_standard)) {
					array_push($agentTokens,(string)$t->token_standard); 	
				}
			}	
		}
		return $agentTokens;		
	}


} // End of class