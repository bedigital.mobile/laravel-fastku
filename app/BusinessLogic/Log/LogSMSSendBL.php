<?php
namespace App\BusinessLogic\Log;

use Log;
use DB;
use Config;
use Exception;
use App\BusinessLogic\Log\LogBL;
use App\Core\SMS\SMSSenderAbstract;
use App\Models\Database\LogSmsSendHelp;
use App\Models\Database\SMSGateway;

class LogSMSSendBL extends LogBL
{
	public function __construct( $input = null ) {
		parent::__construct( $input );
	}
	
	public static function createInput( $smsGtwID, $numberSender, $msg, $result, $purposeType = "" ) {
		$arr = array(
				'sms_gateway_id' 	=> $smsGtwID,
				'number_sender' 	=> $numberSender,
				'sms_message' 		=> $msg,
				'purpose_type' 		=> $purposeType,
				'result' 			=> $result
		);
		return $arr;
	}
	
	public function insert() {
			 Log::info( "LogSMSSendBL::handle START");
		$idLog = 0; 
		try {
			if (! isset($this->input['result'])) {
				return $idLog;
			}
			if (is_null($this->input['result']) || ! is_array($this->input['result'])) {
				return $idLog;
			}
			if ($this->debug) Log::info("LogSMSSendBL::insert input=".json_encode($this->input));
			for ($i = 0; $i < count($this->input['result']); $i++) {
				try {
					$log = new LogSmsSendHelp();
					$key = self::KEY_LOG_DATETIME;
					if (array_key_exists($key,$this->input)) {
						$log->$key = $this->input[$key];
					} else {
						$log->$key = date('Y-m-d H:i:s');
					}
					if (isset($this->input['sms_gateway_id'])) {
						$log->sms_gateway_id = (int)$this->input['sms_gateway_id'];
					}
					if (isset($this->input['number_sender'])) {
						$log->username = $this->input['number_sender'];
					}
					if (isset($this->input['result'][$i][SMSSenderAbstract::KEY_SMS_NUMBER_RECEIVER])) {
						$log->number_receiver = $this->input['result'][$i][SMSSenderAbstract::KEY_SMS_NUMBER_RECEIVER];
					}
					if (isset($this->input['sms_message'])) {
						$log->sms_message = (strlen($this->input['sms_message']) <= 255 ? $this->input['sms_message'] : substr($this->input['sms_message'], 0, 255));
					}
					if (isset($this->input['result'][$i][SMSSenderAbstract::KEY_SMS_SENDING_ID])) {
						$log->sms_sending_id = (int)$this->input['result'][$i][SMSSenderAbstract::KEY_SMS_SENDING_ID];
					}
					if (isset($this->input['result'][$i][SMSSenderAbstract::KEY_SMS_IS_SUCCESS])) {
						$log->is_success = ($this->input['result'][$i][SMSSenderAbstract::KEY_SMS_IS_SUCCESS] ? 'Y' : 'N');
					} else {
						$log->is_success = 'N';
					}
					if (isset($this->input['result'][$i][SMSSenderAbstract::KEY_SMS_STATUS_CODE])) {
						$log->send_status_code = (int)$this->input['result'][$i][SMSSenderAbstract::KEY_SMS_STATUS_CODE];
					}
					if (isset($this->input['result'][$i][SMSSenderAbstract::KEY_SMS_STATUS_DESCRIPTION])) {
						$log->send_status_desc = (strlen($this->input['result'][$i][SMSSenderAbstract::KEY_SMS_STATUS_DESCRIPTION]) <= 50 ? $this->input['result'][$i][SMSSenderAbstract::KEY_SMS_STATUS_DESCRIPTION] : substr($this->input['result'][$i][SMSSenderAbstract::KEY_SMS_STATUS_DESCRIPTION], 0, 50));
					}
					if (isset($this->input['result'][$i][SMSSenderAbstract::KEY_SMS_PRICE])) {
						$log->price = (int)$this->input['result'][$i][SMSSenderAbstract::KEY_SMS_PRICE];
					}
					if (isset($this->input['purpose_type'])) {
						$log->purpose_type = $this->input['purpose_type'];
					}
					$log->save();
					$idLog = $log->id;
					if (! empty($idLog) && $log->price > 0 && $log->sms_gateway_id > 0) {
						try {
							$mTable = new SMSGateway();
							DB::table( $mTable->getTable() )->where('id', '=', $log->sms_gateway_id)
															->update( [
																'total_balance_latest' => DB::raw('total_balance_latest - ' . $log->price)
															] );
						} catch ( Exception $e ) {
							Log::info("LogSMSSendBL::insert do_update_balance exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
						}
					}
				} catch ( Exception $e ) {
					Log::info("LogSMSSendBL::insert exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
		} catch ( Exception $e ) {
			Log::info("LogSMSSendBL::insert exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $idLog;
	}
	
	public static function updateDeliveryStatus( $arrSMSGtyID, $arrStatus ) {
		try {
			if (is_null($arrStatus) || ! is_array($arrStatus)) {
				return false;
			}
			if (is_null($arrSMSGtyID) || ! is_array($arrSMSGtyID)) {
				$arrSMSGtyID = array();
			}
			$numGty = count($arrSMSGtyID);
			$mTable = new LogSmsSendHelp();
			$tablename = $mTable->getTable();
			for ($i = 0; $i < count($arrStatus); $i++) {
				try {
					$numberReceiver = "";
					$sendingID = 0;
					$statusCode = null;
					$statusDesc = null;
					if (isset($arrStatus[$i][SMSSenderAbstract::KEY_SMS_NUMBER_RECEIVER])) {
						$numberReceiver = $arrStatus[$i][SMSSenderAbstract::KEY_SMS_NUMBER_RECEIVER];
					}
					if (isset($arrStatus[$i][SMSSenderAbstract::KEY_SMS_SENDING_ID])) {
						$sendingID = (int)$arrStatus[$i][SMSSenderAbstract::KEY_SMS_SENDING_ID];
					}
					if (isset($arrStatus[$i][SMSSenderAbstract::KEY_SMS_DELIVERY_STATUS_CODE])) {
						$statusCode = (int)$arrStatus[$i][SMSSenderAbstract::KEY_SMS_DELIVERY_STATUS_CODE];
					}
					if (isset($arrStatus[$i][SMSSenderAbstract::KEY_SMS_DELIVERY_STATUS_DESCRIPTION])) {
						$statusDesc = $arrStatus[$i][SMSSenderAbstract::KEY_SMS_DELIVERY_STATUS_DESCRIPTION];
						if (strlen($statusDesc) > 50) {
							$statusDesc = substr($statusDesc, 0, 50);
						}
					}
					if (! empty($sendingID)) {
						if ($numGty > 0) {
							if (! empty($numberReceiver)) {
								DB::table( $tablename )->where('sms_sending_id', '=', $sendingID)->whereIn('sms_gateway_id', $arrSMSGtyID)->where('number_receiver', '=', $numberReceiver)
									->update( [
										'delivery_status_code' => $statusCode,
										'delivery_status_desc' => $statusDesc,
										'delivery_status_datetime' => date('Y-m-d H:i:s')
									] );
							} else {
								DB::table( $tablename )->where('sms_sending_id', '=', $sendingID)->whereIn('sms_gateway_id', $arrSMSGtyID)
									->update( [
										'delivery_status_code' => $statusCode,
										'delivery_status_desc' => $statusDesc,
										'delivery_status_datetime' => date('Y-m-d H:i:s')
									] );
							}
						} else {
							DB::table( $tablename )->where('sms_sending_id', '=', $sendingID)->where('number_receiver', '=', $numberReceiver)
								->update( [
									'delivery_status_code' => $statusCode,
									'delivery_status_desc' => $statusDesc,
									'delivery_status_datetime' => date('Y-m-d H:i:s')
								] );
						}
					}
				} catch ( Exception $e ) {
					Log::info("LogSMSSendBL::updateDeliveryStatus exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
			return true;
		} catch ( Exception $e ) {
			Log::info("LogSMSSendBL::updateDeliveryStatus exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
}