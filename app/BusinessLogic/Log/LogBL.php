<?php
namespace App\BusinessLogic\Log;

use Log;
use Config;
use Exception;
use App\Definition;

class LogBL 
{
	protected $input;
	protected $debug;
	
	const KEY_LOG_DATETIME = 'log_datetime';
	const KEY_ERROR_CODE = 'error_code';
	const KEY_ERROR_MESSAGE = 'error_message';
	const KEY_IP_CLIENT = 'ip_client';
	const KEY_USER_AGENT = 'user_agent';
	const QUEUE_LOG = "log_cms";
	const QUEUE_EMAIL = "email";


	public function __construct( $input = null ) {
		$this->input = array();
		$this->debug = boolval( Config::get( 'app.debug' ) );
		try {
			if (! is_null($input) && is_array($input)) {
				$this->input = $input;
			}
		} catch ( Exception $e ) {
			Log::info("LogBL::__construct exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		
	}
	
	public function setLogDateTime() {
		$this->input[ self::KEY_LOG_DATETIME ] = date('Y-m-d H:i:s');
		return $this;
	}
	public function setErrorCode( $v ) {
		$this->input[ self::KEY_ERROR_CODE ] = (int)$v;
		return $this;
	}
	public function setErrorMessage( $v ) {
		$this->input[ self::KEY_ERROR_MESSAGE ] = (strlen($v) <= 200 ? (string)$v : substr($v, 0, 200));
		return $this;
	}
	public function setIPClient( $v ) {
		$this->input[ self::KEY_IP_CLIENT ] = (strlen($v) <= 45 ? (string)$v : substr($v, 0, 45));
		return $this;
	}
	public function setUserAgent( $v ) {
		$this->input[ self::KEY_USER_AGENT ] = (strlen($v) <= 255 ? (string)$v : substr($v, 0, 255));
		return $this;
	}
	
	public function setInput( $v ) {
		if (! is_null($v) && is_array($v)) {
			$this->input = $v;
		}
		return $this;
	}
	public function getInput() {
		return $this->input;
	}
	
	public static function getQueueName() {
		return self::QUEUE_LOG;
	}
	
	
}