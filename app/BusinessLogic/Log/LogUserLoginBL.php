<?php
namespace App\BusinessLogic\Log;

use Log;
use Config;
use Exception;
use App\BusinessLogic\Log\LogBL;
use App\Models\LogUserLogin;

class LogUserLoginBL extends LogBL {

	public function __construct( $input = null ) {
		parent::__construct( $input );
		$this->conn = Config::get ( 'webconf.cms.connectionalibaba' );
	}
	
	public static function createInput( $userID, $username, $session="" ) {
		$arr = array(
				'user_id' => $userID,
				'username' => $username,
				'session_id' => $session
		);
		return $arr;
	}
	
	public function insert() {
		$idLog = 0;
		try {
			// $log = new LogUserLogin();

		    $log = new  LogUserLogin;
            $log->setConnection($this->conn); 

            
			$key = self::KEY_LOG_DATETIME;
			if (array_key_exists($key,$this->input)) {
				$log->$key = $this->input[$key];
			} else {
				$log->$key = date('Y-m-d H:i:s');
			}
			if (array_key_exists('user_id',$this->input)) {
				$log->user_id = (int)$this->input['user_id'];
			} 
			if (array_key_exists('username',$this->input)) {
				$log->username = (strlen($this->input['username']) <= 50 ? $this->input['username'] : substr($this->input['username'], 0, 50));
			}
			if (array_key_exists('session_id',$this->input)) {
				$log->session_id = (strlen($this->input['session_id']) <= 50 ? $this->input['session_id'] : substr($this->input['session_id'], 0, 50));
			}
			$key = self::KEY_ERROR_CODE;
			if (array_key_exists($key,$this->input)) {
				$log->$key = $this->input[$key];
			} 			 
			$key = self::KEY_ERROR_MESSAGE;
			if (array_key_exists($key,$this->input)) {
				$log->$key = $this->input[$key];
			}
			$key = self::KEY_IP_CLIENT;
			if (array_key_exists($key,$this->input)) {
				$log->$key = $this->input[$key];
			}
			$key = self::KEY_USER_AGENT;
			if (array_key_exists($key,$this->input)) {
				$log->$key = $this->input[$key];
			}
			$log->save();
			$idLog = $log->id;
		} catch ( Exception $e ) {
			Log::info("LogUserLoginBL::insert exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $idLog;
	}
	
}
