<?php
namespace App\BusinessLogic\Log;

use Log;
use Config;
use Exception;
use App\BusinessLogic\Log\LogBL;
use App\Models\LogUserActivity;

class LogUserActivityBL extends LogBL {

	public function __construct( $input = null ) {
		parent::__construct( $input );
		        $this->conn = Config::get ( 'webconf.cms.connectionreport' );

	}

	public static function createInput( $userID, $sessionID, $activityName, $activityDetail = null ) {
		$arr = array(
				'user_id' => $userID,
				'session_id' => $sessionID,
				'activity_name' => $activityName,
				'activity_detail' => $activityDetail,
		);
		return $arr;
	}

	public function insert() {
		Log::info("LogUserActivityBL::insert");
		$idLog = 0;
		try {

			  	$log = new LogUserActivity;
                // $log->setConnection($this->conn);  
                

			// $log = new LogUserActivity();
			$key = self::KEY_LOG_DATETIME;
			if (array_key_exists($key,$this->input)) {
				$log->$key = $this->input[$key];
			} else {
				$log->$key = date('Y-m-d H:i:s');
			}
			if (array_key_exists('user_id',$this->input)) {
				$log->user_id = $this->input['user_id'];
			} else {
				$log->user_id = 0;
			}
			if (array_key_exists('session_id',$this->input)) {
				$log->cms_type = (strlen($this->input['session_id']) <= 50 ? $this->input['session_id'] : substr($this->input['session_id'], 0, 50));
			} else {
				$log->session_id = "inputlog";
			}
			if (array_key_exists('activity_name',$this->input)) {
				$log->activity_name = (strlen($this->input['activity_name']) <= 50 ? $this->input['activity_name'] : substr($this->input['activity_name'], 0, 50));
			} else {
				$log->activity_name = "";
			}
			if (array_key_exists('activity_detail',$this->input)) {
				$log->activity_detail = (strlen($this->input['activity_detail']) <= 255 ? $this->input['activity_detail'] : substr($this->input['activity_detail'], 0, 255));
			}
			$log->save();
			$idLog = $log->id;
		} catch ( Exception $e ) {
			Log::info("LogUserActivityBL::insert exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $idLog;
	}

}