<?php

namespace App\BusinessLogic;

use Log; 
use Config;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Definition;    
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Core\Util\UtilCommon;
use App\Http\Controllers\BDGBaseController;
use App\Models\PartnerTrxReqModel;
use App\Models\PartnerDepositModel;
use App\Models\LogTrxAgent;
use App\Models\SummaryReconPendingModel;
use App\Models\Database\Product;
use App\Models\Database\ProductBreakup;
use Illuminate\Support\Facades\DB;   
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;  
use Symfony\Component\HttpKernel\Exception\HttpException; 
use App\Http\Controllers\Controller;
use App\BusinessLogic\Auth\AuthUserBL;  
 use App\Models\MainAgentModel;
use App\Models\Agent;
use App\Models\WhiteLabelUserUid;
use App\Models\WhiteLabelUserIP;
use App\Models\admin\WhiteLabelAdminModel;
use App\Models\admin\AgentProfileModel;
 use App\Models\AgentAntv;

use App\Models\MasterAgentProfileModel; 
use App\BusinessLogic\Log\LogUserActivityBL;
use App\BusinessLogic\Log\LogUserLoginBL;
use App\Jobs\InsertLogUserActivityJob; 
use App\Jobs\InsertLogUserLoginJob;  
use App\Jobs\InsertLogPushMailJob;  
use App\BusinessLogic\Push\PushBL;

use App\Core\Util\UtilEmail;
use App\SystemLogic\SMS\SMSSenderManager;
use App\SystemLogic\Auth\TokenLogic;
use App\Core\CacheData\Simple\CacheAgentSendSMSDeviceToken;
use App\Core\CacheData\Simple\CacheAgentSendSMSDeviceTokenCounter;

 use App\Core\Push\PushManager;
use App\Core\Push\PushKey;
use App\Models\Database\PushTokenAgents;
use App\Models\Database\MasterAgent;
use App\Models\ReportSumTrxMa;
use App\Jobs\Push\PushJob;
use App\BusinessLogic\MasterAgent\MasterAgentDownlineBL;


use GuzzleHttp\HandlerStack;
use GuzzleHttp\RequestOptions;

use GraphQL\Client;
use GraphQL\Exception\QueryError;
use GraphQL\Query;
use GraphQL\Mutation;

use App\Core\JWT\JWT;

class HelpdeskBL
{ 
	protected $userID; 

	const APPROVAL_UNKNOWN = 0;  
  const USE_ASYNC_PROCESS = true; 
  
	public function __construct(  ) {
		try {
			  
        // $this->conn =  Config::get ( 'webconf.cms.connectiondata' ); 
        $this->conn =  Config::get ( 'webconf.cms.connectiondata_prod' );  
        $this->conn111 =  Config::get ( 'webconf.cms.connection111' );  
        $this->connali =  Config::get ( 'webconf.cms.connectionalibaba' );  
        $this->urlsite =  Config::get ( 'webconf.cms.urlsite' );  
        $this->downline =[];
        $this->downlineMa =[];
        $this->username ="";
        $this->uidlist=[];
        // 

		} catch ( Exception $e ) {
			Log::info("CmsBL::__construct exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	} 



function verifiedHost($verifiedhost,$originhost)
{
try{
  
  log::info(" this domain is =".$originhost." verified= ".$verifiedhost); 
  // if($verifiedhost != $originhost)
  // {

  //    log::info("access denied");
  //     return Redirect::to($verifiedhost);
      

  // }   


} catch ( Exception $e ) {
            Log::info("HelpdeskBL::verifiedHost exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
} 

}


//=====================================FASTKU====================================
//|                                                                             |
//|                                                                             |
//|                                                                             |

//=====================================FASTKU====================================

   public function getRequestcapthca($head)
    {
log::info("user-agent= ".$head);

      $client = new \GuzzleHttp\Client();

$request = $client->request('get', 'https://fintech-stg.bedigital.co.id/elog/graph/v1/member/captcha/kezxfw/image.png', [
  'headers' => [
    'User-Agent' => $head,
  
  ],
]);
       
        $body = $request->getBody()->getContents();
        $base64 = base64_encode($body);
        $mime = "image/jpeg";
        $img = ('data:' . $mime . ';base64,' . $base64);
        return "<img src=\"".$img."\" alt='capthca'>";

    }
  public function refreshcapthca($head)
    {


        $client = new \GuzzleHttp\Client();
        $request = $client->request('get', 'https://fintech-stg.bedigital.co.id/elog/graph/v1/member?mutation={refreshCaptcha(whitelabel_id:"KEZXFW"){code,message}}', [
          'headers' => [
            'User-Agent' => $head,
          
          ],
        ]);


        // $request = $client->get('https://fintech-stg.bedigital.co.id/elog/graph/v1/member?mutation={refreshCaptcha(whitelabel_id:"KEZXFW"){code,message}}');
        $response = $request->getBody()->getContents();
        log::info("hasil =".$response);
        // $body = $request->getBody()->getContents();
        // $base64 = base64_encode($body);
        // $mime = "image/jpeg";
        // $img = ('data:' . $mime . ';base64,' . $base64);
        return "ok";

    } 






public function gettokenjwt($session)
{
 
date_default_timezone_set("Asia/Bangkok");
$timenow = time();//
$u = gmdate('U');//
$s = strtotime(gmdate('Y-m-d H:i:s T'));

 
// $timenow = strtotime(gmdate('Y-m-d H:i:s'));
  $t = Carbon::now()->timestamp;
  $x = (time() - date("Z"));
  $v = strtotime(gmdate(date("Y-m-d H:i:s", time() - date("Z"))));

 $timeexp = $timenow + 600; 
 //$date = Carbon::parse($timeStamp)->format('M d Y');

// Log::info("helpdeskBL::gettokenjwt - timenow = ".$timenow);
// Log::info("helpdeskBL::gettokenjwt - time2 = ".$s);
// Log::info("helpdeskBL::gettokenjwt - time3 = ".$t);
// Log::info("helpdeskBL::gettokenjwt - time4 = ".$u);
// Log::info("helpdeskBL::gettokenjwt - time5 = ".$v);
// Log::info("helpdeskBL::gettokenjwt - time6 = ".$x);
// exit();
    //"KEY: ODdRxF9M8tpR", 
    //"TOKEN: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXJ0bmVySWQiOiAiT0RkUnhGOU04dHBSIiwiYWNjb3VudE51bWJlciI6ICIwMTAyMDAwNzYzIiwicHJvZHVjdENvZGUiOiAiUE1TUCJ9.a9eE9GGNsQe1rldwC_f4kL1lKpRRFjg1v3oct_I9x3o",
    // $session = "CCFSVMMY31E11ubfTTaoYOI7cZdPCiFdbJzto7zB";
    $apikey = "abcde";
    $alg = 'HS256';
    $SECRET = "XYZ12345XYZ12345XYZ12345XYZ12345xxx";    
    $payload = array(        
        "iat" => $timenow,
        "exp" => $timeexp,
        "session_id" => $session,
        //"accountNumber" => "01S0016",
        //"productCode" => "SEUNKIN"
    );
  
      // Log::info("ProductController::processinvoicepo - payload = ".json_encode($payload)); 
     // $token = JWT::encode($payload);
      $token = JWT::encode($payload, $SECRET, $alg);
    // Log::info("ProductController::processinvoicepo - token = ".json_encode($token));

return $token;
}




    public function getprofilejwt($session_id,$useragent)
   {

      
log::info("=================================================================");
log::info("HelpdeskBL::getprofilejwt , token =".$session_id." ,useragent =".$useragent);
  
$version = "1.0";
$key = "abcde";
$tokenJWT = $this->gettokenjwt($session_id);

log::info("token jwt =".$tokenJWT);



$clients = new Client(
    'https://fintech-stg.bedigital.co.id/elog/graph/v1/member/me',
    [],
    [ 
        
        'headers' => [
            'VERSION' => $version,
             'KEY' => $key,
             'TOKEN' => $tokenJWT,
            'Content-Type' => 'application/json',
        ],
       
       
    ]
);
 
$gqlraw = <<<QUERY
query {
    profile { fullname, email }
}
QUERY;
 $gql = (new Query('profile'))
    ->setSelectionSet(
        [
            'fullname',
            'email' 
        ]
    );
try {
     $results = $clients->runQuery($gql,true);
}
catch (QueryError $exception) {
    print_r($exception->getErrorDetails());
    exit;
}
// print_r($results->getData()['profile']);

$resultnya = $results->getData()['profile'];
//         $result = $clients->runQuery($gql);
        log::info("hasil =".json_encode($results->getData()['profile']));
// var_dump($result);
  
 return $resultnya;
} 
    
public function logoutjwt($session_id)
   {

      
log::info("=================================================================");
log::info("HelpdeskBL::logoutjwt , token =".$session_id);
  
$version = "1.0";
$key = "abcde";
$tokenJWT = $this->gettokenjwt($session_id);

log::info("token jwt =".$tokenJWT);



$client = new \GuzzleHttp\Client();


$queryprofile = <<<GRAPHQL
  query {
    logout { code, message }
}
GRAPHQL;

$res = $client->request(
  'GET',
  $this->urlsite.'elog/graph/v1/member/me?mutation={logout{code,message}}',
  [
     
    'json' => [
      'query' => $queryprofile 
    ],
    'headers' => [
  
       'VERSION' => $version,
             'KEY' => $key,
             'TOKEN' => $tokenJWT,
            'Content-Type' => 'application/json',
    ]
  ]);



try {

   $result =  $res->getBody();
   log::info("result = ".$result);
         

} catch (RequestException $e) {
 
    if ($e->hasResponse()){
        if ($e->getResponse()->getStatusCode() == '400') {
                echo "Got response 400";
        }
    }
exit();
     
} catch (\Exception $e) {
 return "error";
}
 
 return $result;


} 


public function getshipmentarray($session_id,$useragent,$opsi)
   {

      
log::info("=================================================================");
log::info("HelpdeskBL::getprofilejwt , token =".$session_id." ,useragent =".$useragent);
  
$version = "1.0";
$key = "abcde";
$tokenJWT = $this->gettokenjwt($session_id);

log::info("token jwt =".$tokenJWT);



$clients = new Client(
    'https://fintech-stg.bedigital.co.id/elog/graph/v1/member/me',
    [],
    [ 
        
        'headers' => [
            'VERSION' => $version,
             'KEY' => $key,
             'TOKEN' => $tokenJWT,
            'Content-Type' => 'application/json',
        ],
       
       
    ]
);
  
 $gqlpayment = (new Query('draftShipment'))
    ->setSelectionSet(
        [
            'choice_payment_channels' 
        ]
    );

  $gqlcategories = (new Query('draftShipment'))
    ->setSelectionSet(
        [
            'choice_categories' 
        ]
    );

  $gqlpackage = (new Query('draftShipment'))
    ->setSelectionSet(
        [
            'choice_packagings' 
        ]
    );
  $gqlshipment = (new Query('draftShipment'))
    ->setSelectionSet(
        [
            'shipment_id' 
        ]
    );




try {

  if($opsi == "payment")
  {
      $results = $clients->runQuery($gqlpayment,true);
  } 
  else if($opsi == "categories")
  {
      $results = $clients->runQuery($gqlcategories,true);
  }
  else if($opsi == "packaging")
  {
      $results = $clients->runQuery($gqlpackage,true);
  }
  else if($opsi == "shipmentid")
  {
      $results = $clients->runQuery($gqlshipment,true);
  }


}
catch (QueryError $exception) {
    print_r($exception->getErrorDetails());
    exit;
}
// print_r($results->getData()['profile']);

$resultnya = $results->getData()['draftShipment'];
//         $result = $clients->runQuery($gql);
        log::info("hasil =".json_encode($results->getData()['draftShipment']));
// var_dump($result);
  
 return $resultnya;
}



public function getTrxArray($session_id,$useragent,$data,$opsi)
   {

      
log::info("=================================================================");
log::info("HelpdeskBL::getTrxArray , token =".$session_id." ,useragent =".$useragent."data =".json_encode($data));
  
$version = "1.0";
$key = "abcde";
$tokenJWT = $this->gettokenjwt($session_id);

log::info("token jwt =".$tokenJWT); 

$clients = new Client(
    'https://fintech-stg.bedigital.co.id/elog/graph/v1/member/me',
    [],
    [ 
        
        'headers' => [
            'VERSION' => $version,
             'KEY' => $key,
             'TOKEN' => $tokenJWT,
            'Content-Type' => 'application/json',
        ],
       
       
    ]
);



$mutationselCouriers = (new Mutation('draftShipmentSetCourier'))
    ->setArguments(['courier_id' => $data['province_id'],'service_code' => $data['city_id']])
    ->setSelectionSet(
        [ 
            'shipment_id',           
            (new Query('courier'))
                ->setSelectionSet(
                    [
                        'courier_id', 'courier_code', 'courier_name', 'service_code', 'service_name', 'etd', 'valid', 'message' 
                      ]),
            (new Query('cost'))
                ->setSelectionSet(
                    [
                        'delivery_fee','grand_total' 
                      ])
             
        ]
    ); 
   


$mutationpickup = (new Mutation('draftShipmentEditPickupAddress'))
    ->setArguments(['fullname' => $data['fullname'],'province_id' => $data['province_id'],'city_id' => $data['city_id'],'subdistrict_id' => $data['subdistrict_id'],'address' => $data['address'],'phone_number' => $data['phone_number'],'note' => $data['note']])
    ->setSelectionSet(
        [ 
            'shipment_id',           
            (new Query('pickup_address'))
                ->setSelectionSet(
                    [
                        'fullname','line_pickup_address','pickup_note',
                        (new Query('detail'))
                        ->setSelectionSet(
                            [
                              'province_name', 'city_name', 'subdistrict_name', 'village_name'
                              ])
                      ]),
            (new Query('cost'))
                ->setSelectionSet(
                    [
                        'distance_km','grand_total' 
                      ])
             
        ]
    ); 
   
$mutationshipment = (new Mutation('draftShipmentEditShippingAddress'))
    ->setArguments(['fullname' => $data['fullname'],'province_id' => $data['province_id'],'city_id' => $data['city_id'],'subdistrict_id' => $data['subdistrict_id'],'address' => $data['address'],'phone_number' => $data['phone_number'],'note' => $data['note']])
    ->setSelectionSet(
        [ 
            'shipment_id',           
            (new Query('pickup_address'))
                ->setSelectionSet(
                    [
                        'fullname','line_pickup_address','pickup_note',
                        (new Query('detail'))
                        ->setSelectionSet(
                            [
                              'province_name', 'city_name', 'subdistrict_name', 'village_name'
                              ])
                      ]),
            (new Query('cost'))
                ->setSelectionSet(
                    [
                        'distance_km','grand_total' 
                      ])
             
        ]
    ); 
  



try {

  if($opsi == "editpickup")
  {
      $results = $clients->runQuery($mutationpickup,true);
      $ar='draftShipmentEditPickupAddress';
  } else if($opsi == "editreceive")
  {
      $results = $clients->runQuery($mutationshipment,true);
      $ar='draftShipmentEditShippingAddress';
  } else if($opsi == "setkurir")
  {
      $results = $clients->runQuery($mutationselCouriers,true);
      $ar='draftShipmentSetCourier';
  }  


}
catch (QueryError $exception) {
    print_r($exception->getErrorDetails());
    exit;
}
// print_r($results->getData()[$ar]);

$resultnya = $results->getData()[$ar];
//         $result = $clients->runQuery($gql);
        log::info("hasil =".json_encode($results->getData()[$ar]));
// var_dump($result);
  
 return $resultnya;
}









    public function getprofile($token,$useragent)
   {

      
log::info("=================================================================");
log::info("testing helpdeskBL guzzle start , token =".$token." ,useragent =".$useragent);
   
$client = new \GuzzleHttp\Client();


$queryprofile = <<<GRAPHQL
  query {
    profile { fullname, email }
}
GRAPHQL;

$res = $client->request(
  'POST',
  $this->urlsite.'elog/graph/v1/member/me',
  [
     
    'json' => [
      'query' => $queryprofile 
    ],
    'headers' => [
      'Authorization' => 'Bearer '.$token,
      'Content-Type' => 'application/json',
      
    ]
  ]);



try {

   $result =  $res->getBody();
   log::info("result = ".$result);
         

} catch (RequestException $e) {
 
    if ($e->hasResponse()){
        if ($e->getResponse()->getStatusCode() == '400') {
                echo "Got response 400";
        }
    }
exit();
     
} catch (\Exception $e) {
 return "error";
}
 
 return $result;
}


      public function teslogin($user, $password)
{

      
log::info("=================================================================");
log::info("testing helpdeskBL guzzle start");
  
$client = new \GuzzleHttp\Client();


$mutation = <<<GRAPHQL
  mutation { 
    login ( 
        whitelabel_id: "KEZXFW",
        captcha: "3IKG",
        platform: "ANDROID",
        app_build_version: "1.0"
    ) { session_id, user_id, authorization } 
}
GRAPHQL;

$res = $client->request(
  'POST',
  'https://fintech-stg.bedigital.co.id/elog/graph/v1/member',
  [
     'auth' => [$user, $password, 'digest'],
    'json' => [
      'query' => $mutation 
    ],
    'headers' => [
      'WWW-Authenticate' => 'Digest',
      'User-Agent' => 'testing/1.0',
      'realm' => 'BEDIGITAL',
      'Content-Type' => 'application/json',
    ]
  ]);

// echo $res->getBody();

try {

    $res->getBody();
        
    // Here the code for successful request

} catch (RequestException $e) {

    // Catch all 4XX errors 
    
    // To catch exactly error 400 use 
    if ($e->hasResponse()){
        if ($e->getResponse()->getStatusCode() == '400') {
                echo "Got response 400";
        }
    }
exit();
    // You can check for whatever error status code you need 
    
} catch (\Exception $e) {

    // There was another exception.

}



// $endPoint = 'https://fintech-stg.bedigital.co.id/elog/graph/v1/member';

// $query = <<<'GRAPHQL'
//  mutation { 
//     login ( 
//         whitelabel_id: "KEZXFW",
//         captcha: "3IKG",
//         platform: "ANDROID",
//         app_build_version: "1.0"
//     ) { session_id, user_id, authorization } 
// }
// GRAPHQL;
 
//  $response = new Client([
//       'base_uri' => $endPoint,
//       'headers' => [
//             'WWW-Authenticate' => 'Digest',
//             'User-Agent' => 'testing/1.0',
//             'realm' => 'BEDIGITAL',
//             'Content-Type' => 'application/json',
//       ],
//       'body' => json_encode([
//           'query' => $query,
//       ]),
//   ]);

//   return print_r(json_decode($response->getBody()->getContents(), true));
}



public function loggraph($usr){

log::info("=================================================================");
log::info("testing helpdeskBL guzzle222 start");
    $query = "mutation { 
    login ( 
        whitelabel_id: \"KEZXFW\",
        captcha: \"JIJU\",
        platform: \"WEB\",
        app_build_version: \"1.0\"
    ) { session_id, user_id, authorization } 
}";

  $response = $this->graph($query,$usr); // call this function 
  log::info("result=".json_encode($response));
}
public function graph($query , $usr){
    
log::info("=================================================================");
log::info("testing helpdeskBL graph start ".$usr);
    $url = 'https://fintech-stg.bedigital.co.id/elog/graph/v1/member';

    $request = ['query' => $query];

    // if(count($variables) > 0) { $request['variables'] = $variables; }

    $req = json_encode($request);
    $parameters['body'] = $req;

    $stack = HandlerStack::create();
    $client = new \GuzzleHttp\Client([
        'handler'  => $stack,
        'headers'  => [
            'Accept'       => 'application/json',
            'Content-Type' => 'application/json',
             'WWW-Authenticate' => 'Digest',
            'User-Agent' => $usr,
           'realm' => 'BEDIGITAL',
        ],
    ]);

   


    try {

    $response = $client->request('post',$url,$parameters);
    $body =  json_decode($response->getBody(),true);

} catch (RequestException $e) {

    // Catch all 4XX errors 
    
    // To catch exactly error 400 use 
    if ($e->hasResponse()){
        if ($e->getResponse()->getStatusCode() == '401') {
                $body =  "Got response 401";
        }
    }

    // You can check for whatever error status code you need 
    
} catch (\Exception $e) {
 $response = $e->getResponse();
    // $responseBodyAsString = $response->getBody()->getContents();
    $responseBodyAsString = $response->getHeaders();//->getContents();
    $body =  $responseBodyAsString;//"Got response 403";
    // There was another exception.
 // $body =  "Got response 402";
} 

return $body;

 }



function getdetailLog($id)
{
try{

    $someModel = new LogTrxAgent;
    $someModel->setConnection($this->conn); 
    $detail = $someModel->where('id','=',$id)->first();
    // Log::info("RefundController::getdetailLog data=".json_encode($detail));
    $s = UtilCommon::intToBase36($detail['white_label_id']);
    $whitelabelbase = strtoupper($s);
    // Log::info("RefundController::getdetailLog wlbase=".$whitelabelbase);
    $year = date("Y", strtotime($detail['log_datetime']));
    $month = date("m", strtotime($detail['log_datetime']));
    $day = date("d", strtotime($detail['log_datetime']));


$arrData = array (
     'year' => $year,
     'month' => $month,
     'day' => $day,
     'white_label_id' => $whitelabelbase,
     'log_id' => $detail['id'],
     'trx_internal_id' => $detail['trx_internal_id'],
     'trx_retrieval' => $detail['trx_retrieval'],
     'agent_id' => $detail['agent_id'],
     'product_id' => $detail['product_id'],
     'product_code' => $detail['product_code'],
     'trx_bill_number' => $detail['trx_bill_number'],
     'trx_payment_code' => $detail['trx_payment_code']
);


      Log::info("RefundController::getdetailLog data=".json_encode($arrData));


} catch ( Exception $e ) {
            Log::info("RefundController::getdetailLog exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
} 
return $arrData;
}

function getidLogfrombill($id)
{
try{

	// $listdata = explode('#', $id);
    $someModel = new LogTrxAgent;
    $someModel->setConnection($this->conn); 
    $detail = $someModel->where('id','=',$id)
    ->first();
    // Log::info("RefundController::getidLogfrombill detail=".$detail);
 $s = UtilCommon::intToBase36($detail['white_label_id']);
    $whitelabelbase = strtoupper($s);
    // Log::info("RefundController::getdetailLog wlbase=".$whitelabelbase);
    $year = date("Y", strtotime($detail['log_datetime']));
    $month = date("m", strtotime($detail['log_datetime']));
    $day = date("d", strtotime($detail['log_datetime']));
 

$arrData = array (
     'year' => $year,
     'month' => $month,
     'day' => $day,
     'white_label_id' => $whitelabelbase,
     'log_id' => $detail['id'],
     'trx_internal_id' => $detail['trx_internal_id'],
     'trx_retrieval' => $detail['trx_retrieval'],
     'agent_id' => $detail['agent_id'],
     'product_id' => $detail['product_id'],
     'product_code' => $detail['product_code'],
     'trx_bill_number' => $detail['trx_bill_number'],
     'trx_payment_code' => $detail['trx_payment_code']
);


      Log::info("RefundController::getdetailLog data=".json_encode($arrData));


} catch ( Exception $e ) {
            Log::info("RefundController::getdetailLog exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
} 
return $arrData;
}
function getidLogforce($id)
{
try{ 

$whitelabelbase ="";
$agenbase ="";
$masteragenbase ="";
	$listdata = explode('#', $id);
    $someModel = new LogTrxAgent;
    $someModel->setConnection($this->conn); 
    $detail = $someModel->where('trx_bill_number','=',$listdata[1])->where('trx_payment_code','=',$listdata[2])
    ->first();
    // Log::info("RefundController::getidLogfrombill detail=".$detail);
	$s = UtilCommon::intToBase36($detail['white_label_id']);
    $whitelabelbase = strtoupper($s);

    $agen = UtilCommon::intToBase36($detail['agent_id']);
    $agenbase = strtoupper($agen);

    $masteragen = UtilCommon::intToBase36($detail['master_agent_id']);
    $masteragenbase = strtoupper($masteragen);

    // Log::info("RefundController::getdetailLog wlbase=".$whitelabelbase);
    $year = date("Y", strtotime($detail['log_datetime']));
    $month = date("m", strtotime($detail['log_datetime']));
    $day = date("d", strtotime($detail['log_datetime']));
 

$arrData = array ( 
     'white_label_id' => $whitelabelbase,
     'agent_id' => $agenbase,
     'master_agent_id' => $masteragenbase,
     'product_id' => $detail['product_id'],
     'product_code' => $detail['product_code'],
     'bill_number' => ($detail['trx_bill_number'] !='' ? $detail['trx_bill_number'] : $listdata[1] ),
     'check_within_days' => '0',
     'customer_name' => "",
  	 'customer_phone_number' => "" 
);


      // Log::info("HelpdeskBL::getidLogforce data=".json_encode($arrData));


} catch ( Exception $e ) {
            Log::info("RefundController::getidLogforce exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
} 
return $arrData;
}

	function requestPost($url,$arrData)
	{
	try{
	 
	 
	$postData = json_encode($arrData);

	$username = "kheno";
	$password = "UAT0201";
	$headers = array(
	    'Authorization: Basic ' . base64_encode($username.':'.$password),
	    'Content-Type: application/json'
	);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_VERBOSE, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_ENCODING, true);
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
	curl_setopt($ch, CURLOPT_FAILONERROR, true);
	curl_setopt($ch, CURLOPT_USERAGENT, "PHP-curl/1.0");
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$result = curl_exec($ch);
	$curl_error_no = curl_errno($ch);
	$curl_error_string = curl_error($ch);
	$infoHttpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


	// echo "URL POST: ".$url." <br><br>"."\r\n";
	// echo "HEADER POST: ".json_encode($headers)." <br><br>"."\r\n";
	// echo "DATA POST: ".$postData." <br><br>"."\r\n";
	// echo "RESULT POST: ".$result." <br><br>"."\r\n";
	// echo "curl_error_no: ".$curl_error_no." <br><br>"."\r\n";
	// echo "curl_error_string: ".$curl_error_string." <br><br>"."\r\n";
	// echo "info_Http_Code: ".$infoHttpCode." <br><br>"."\r\n";

	// echo $result['error'];
	/*
	URL POST: https://fintech-stg.bedigital.co.id/api/helpdesk/trx/refund <br><br>
	HEADER POST: ["Authorization: Basic a2hlbm86VUFUMDIwMQ==","Content-Type: application\/json"] <br><br>
	DATA POST: {"year":"2019","month":"02","day":"13","white_label_id":"KF12OI","log_id":1704,"trx_internal_id":"20190213114947654836-2jkmuw-phv5ax-pay-36711237","trx_retrieval":"20190213114947654836-2jkmuw-phv5ax","agent_id":153807800,"product_id":4013,"product_code":null,"trx_bill_number":"050410014261","trx_payment_code":"0e13bb9a13bf598bef51c07567f549e0"} <br><br>

	RESULT POST: {"status":"ERROR","error":{"code":1011,"message":"Double refund"},"display_message":"","type":"GENERAL","results":{"log_trx":{"id":"1704","log_datetime":"2019-02-13 11:49:47","agent_id":"2JKMUW","master_agent_id":"PHV5AX","white_label_id":"KF12OI","biller_aggregator_id":"1","product_id":"4013","product_code":"","product_name":"PDAM KOTA DENPASAR","trx_type":"payment","trx_bill_number":"050410014261","trx_result_code":"000","trx_result_desc":"SUKSES","trx_internal_id":"20190213114947654836-2jkmuw-phv5ax-pay-36711237","trx_biller_id":"36711237","trx_retrieval":"20190213114947654836-2jkmuw-phv5ax","trx_payment_code":"0e13bb9a13bf598bef51c07567f549e0","trx_signature":"","trx_info1":"No.Sambungan  : 050410014261|Nama          : I WYN. SUTIARKA|No.Rekening   : 20180267396|Gol           : D1|Rek Bulan     : FEB18|Pemakaian     : 502-478=24|Harga Air     :Rp.      68.100|By Peml Mtr   :Rp.       7.500|Biaya Adm     :Rp.           0|Total Denda   :Rp.           0|Total Tagihan :Rp.      78.600|","trx_info2":"PEMBAYARAN PDAM DENPASAR|##NPWP| 01.126.452.0-904.000 PPn Atas Air DIBEBASKAN, PP no 31 TH 2007","trx_info3":"","trx_sheet_number":"1","trx_price_nominal":"78600","trx_price_fee_admin":"100","trx_price_fee_type":"DIRECT","api_error_code":"0","api_error_message":"Sukses","is_pending":"Y","advice_retry":"0","advice_datetime":"","advice_result_code":"","advice_result_desc":""},"biller_aggregator_id":"1","biller_response":{"type":"advice","accountType":"B2B","service":"responseadvice","account":"0000000018","institutionCode":"180309000002","product":"4013","billNumber":"050410014261","resultCode":"014","resultDesc":"Transaksi tidak ditemukan. mohon hubungi helpdesk kami untuk mengetahui kepastian status transaksi tersebut.","nominal":"","admin":"","trxId":"36711237","retrieval":"20190213114947654836-2jkmuw-phv5ax","paymentCode":"0e13bb9a13bf598bef51c07567f549e0","timestamp":"","resi":"","info1":"","info2":"","info3":"","sign":""},"refund":null}} <br><br>
	curl_error_no: 0 <br><br>
	curl_error_string:  <br><br>
	info_Http_Code: 200 <br><br>
	*/


	      
	        // return $result['result'];
	 



	} catch ( Exception $e ) {
	            Log::info("HelpdeskBL::requestPost exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
	} 
	  return $result;
	   // response()->json($this->notif->build());

	        // return response()->make ( $result )
	        //                 ->withHeaders([
	        //                         'Content-Type' => 'text/html',
	        //                         'Cache-Control' => 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
	        //                         'Pragma' => 'no-cache',
	        //                         'X-Content-Type-Options' => 'nosniff',
	        //                         'X-XSS-Protection' => '1; mode=block',
	        //                         'X-Frame-Options' => 'SAMEORIGIN'
	        //                 ]);


	}




  public function sendSMSDeviceToken( $telpNumber,$uid,$whitelabel,$userid, $userName="" ) {
    try {
      if (empty($telpNumber)) {
        return false;
      }
      $token = $this->createDeviceToken($whitelabel,$userid,$uid);
      if (empty($token)) {
        return false;
      }
       
      // sementara
      $this->username = $userName;
      // $msg = "Gunakan kode ".$token." untuk verifikasi perangkat Anda saat login di aplikasi Helpdesk MA, Kode ini berlaku 10 menit."; 
      $msg= "PosFIn - ".$token." kode verifikasi perangkat utk login dg username ".$userName." di aplikasi Helpdesk MA. Berlaku 1 jam & jgn berikan ke pihak lain.";
      $smsSender = new SMSSenderManager( $whitelabel );
      $smsSender->setPurposeType( "SMSOTPHDMA" );
      $b = $smsSender->send( $telpNumber, $msg );
      if ($b) {
        $cache = new CacheAgentSendSMSDeviceToken();
        $cache->setFlagRecentlySMSSent( $userid );
        $cache = new CacheAgentSendSMSDeviceTokenCounter();
        $cache->increment($userid );
      }
      return $b;
    } catch ( Exception $e ) {
      Log::info("AgentAuthBL::sendSMSDeviceToken telp=".$telpNumber." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
    return false;
  }
  

  public function createDeviceToken($whitelabel,$userid,$uid) {
    try {
      //  $whilalabelIDBase36 = UtilCommon::idConvertToBase36( $whitelabelID );
      // $userIDBase36 = UtilCommon::idConvertToBase36( $userID );
      if (empty($whitelabel)) {
        return "";
      }
      $objLogic = new TokenLogic( $whitelabel, $userid ,$uid );
      $token = $objLogic->generateSMSToken();
       Log::info("AgentAuthBL::createDeviceToken agent_id=".$userid." platform=".$whitelabel." ==> uid=".$uid);
      return $token;
    } catch ( Exception $e ) {
      Log::info("AgentAuthBL::createDeviceToken exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
    return "";
  }
  

  public function verifyDeviceToken( $token,$whitelabel,$userid,$uid ) {
    $b = false;
    try {
      if ( empty($token)) {
        return $b;
      } 
        // Log::info("AgentAuthBL::verifyDeviceToken user=".$this->mAgent->id." token=".$token);
     
        try {
          // $sessionID = md5( $row->device_id_md5 . "|" . $row->app_platform );
          $objLogic = new TokenLogic($whitelabel,$userid,$uid);
          $b = $objLogic->isSMSTokenValid($token);
            Log::info("HelpdeskBL::verifyDeviceToken==> HASIL=".($b?"true":"false"));
          if ($b) {
            try {
             Log::info("HelpdeskBL::verifyDeviceToken save, userid=".$userid);

                  DB::beginTransaction();   


                  



                  $useruid = new WhiteLabelUserUid;
                  $cekuid = $useruid
                    ->where('user_id', $userid)
                    ->where('white_label_id','=', $whitelabel) 
                     ->where('uid','=', $uid) 
                    ->first();

                   if(!$cekuid)
                   {
                        $useruid->user_id = $userid;
                        $useruid->white_label_id = $whitelabel;
                        $useruid->uid = $uid;
                        $useruid->save();
                   }
                   else
                   {

                        WhiteLabelUserUid::where('user_id', $userid)
                        ->where('white_label_id','=', $whitelabel)
                        ->where('uid','=', $uid) 
                        ->update( [
                        'active' => 'Y'
                        ] );
                   }
 

                   

                  DB::commit();
                  


         
            } catch ( Exception $e ) {
              Log::info("HelpdeskBL::verifyDeviceToken token=".$token." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
            }
            // break;
          } 
        } catch ( Exception $e ) {
          Log::info("HelpdeskBL::verifyDeviceToken token=".$token." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
       
    } catch ( Exception $e ) {
      Log::info("HelpdeskBL::verifyDeviceToken token=".$token." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
    return $b;
  }



  public function verifyDeviceTokenCms( $token,$whitelabel,$userid,$uid ) {
    $b = false;
    try {
      if ( empty($token)) {
        return $b;
      } 
        // Log::info("AgentAuthBL::verifyDeviceToken user=".$this->mAgent->id." token=".$token);
     
        try {
          // $sessionID = md5( $row->device_id_md5 . "|" . $row->app_platform );
          $objLogic = new TokenLogic($whitelabel,$userid,$uid);
          $b = $objLogic->isSMSTokenValid($token);
            Log::info("HelpdeskBL::verifyDeviceTokenCms==> HASIL=".($b?"true":"false"));
          if ($b) {
            try {

               DB::beginTransaction();  
                      // User::where('id', '=',$userid)
                      //  ->update([
                      //  'uid' => $uid
                      // ]);  
  
            $useruid = new WhiteLabelUserUid;
            $useruid->user_id = $userid;
            $useruid->uid = $uid;
            $useruid->save();
 
                      DB::commit();
 
            } catch ( Exception $e ) {
              Log::info("HelpdeskBL::verifyDeviceTokenCms token=".$token." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
            }
          } 
        } catch ( Exception $e ) {
          Log::info("HelpdeskBL::verifyDeviceTokenCms token=".$token." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
       
    } catch ( Exception $e ) {
      Log::info("HelpdeskBL::verifyDeviceTokenCms token=".$token." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
    return $b;
  }
  
public function gettoken($whitelabelid,$userid){

        // $q = WhiteLabelAdminModel::where('id', $userid)->where('white_label_id','=', $whitelabelid)
        // ->first();

       $q = WhiteLabelUserUid::where('user_id', $userid)
       ->where('white_label_id','=', $whitelabelid)
       ->where('active','=', 'Y')
       ->select('uid')
       ->get();
        foreach ( $q as $row) {
            array_push($this->uidlist,$row['uid']);
   }
        return $this->uidlist;
    }


    public function getipwhitelist($userid){
      $this->ip_whitelist=[];
        // $q = WhiteLabelAdminModel::where('id', $userid)->where('white_label_id','=', $whitelabelid)
        // ->first();

       $q = WhiteLabelUserIP::where('user_id', $userid) 
       ->where('active','=', 'Y')
       ->select('ip_whitelist')
       ->get();
        foreach ( $q as $row) {
            array_push($this->ip_whitelist,$row['ip_whitelist']);
   }
        return $this->ip_whitelist;
    }


  public  function getBrowser($u_agent) 
{ 
    // $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
    
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }  
    
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    
    return array( 
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
} 

public function getPhoneNo( $userid) {
    // $b=false;
    try { 
      $cekprod = User::where('id','=',$userid)
      ->select('HP')
      ->first();
       
    } catch ( Exception $e ) {
      Log::info("HelpdeskBL::getPhoneNo exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
    return $cekprod;
  }
  public function finduser_id( $userid) {
    // $b=false;
    try { 
      $cekprod = WhiteLabelAdminModel::where('username','=',$userid)
      ->select('id')
      ->first();
       
    } catch ( Exception $e ) {
      Log::info("HelpdeskBL::getPhoneNo exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
    return $cekprod;
  }

public function getAliasName($id,$white_label_id) {
      $alias='';
    try { 
      $alias = WhitelabelProducts::where('product_id','=',$id)->where('white_label_id','=',$white_label_id)
      ->select('product_name_alias')
      ->first();
       
    } catch ( Exception $e ) {
      Log::info("HelpdeskBL::getPhoneNo exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
    return $alias['product_name_alias'];
  }

  
public function sendmaillogin($toEmail,$fullname,$username,$useragent="unknown",$msg=""){ 
    try
    { 
$nameBlade = 'email.loginnotif';
$subject= 'Login Transaction Information '.$msg; 
$fromEmail = 'helpdesk@posfin.id';


$ua=$this->getBrowser($useragent);
$yourbrowser= "" . $ua['name'] . " , " . $ua['version'] . " on " .$ua['platform'];// . " reports: <br >" . $ua['userAgent'];
 



$paramBlade = array(
            'fullname' => $fullname,
            'username' => $username,
            'useragent' =>  $yourbrowser,
            'msg' =>  $msg,
            'waktu' => date('d M Y H:i:s'),
            'expiry_hours' => '13' 
        );
$mails = new UtilEmail();
$mails->sendEmailByTemplateView( $nameBlade, $paramBlade, $subject, $toEmail, $fullname , $fromEmail );




      } catch ( Exception $e ) {
      Log::info("HelpdeskBL::sendmaillogin exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
      } 
   }
 
 
  public function getWhitelabel($id) {
   
    try { 
       $someModel = new MainAgentModel;
        $someModel->setConnection($this->conn); 
        $list = $someModel->where('id','=',$id)
      ->select('white_label_id')->first(); 
       
    } catch ( Exception $e ) {
      Log::info("CmsBL::getWhitelabel exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
    return $list['white_label_id'];
  }
  

public function getdownline( $masterAgentID){
        // $this->downline=[]; 
        $someModel = new MainAgentModel;
        $someModel->setConnection($this->conn);
        $whitelabelID = $this->getWhitelabel($masterAgentID); 
        $list = $someModel->ListByWhiteLabel( $whitelabelID )->get();
        // log::info("wl = ".$whitelabelID." ma=".$masterAgentID." data".json_encode($list));
       
        $response = $this->build_menu($list,$masterAgentID); 
        // echo $response;
        // print_r($this->downline);
   // log::info("wl = ".$whitelabelID." ma=".$masterAgentID." data-downline".json_encode($this->downline));
        return $response;
}

public function getdownlineMA( $masterAgentID){
        // $this->downline=[]; 
        $someModel = new MainAgentModel;
        $someModel->setConnection($this->conn);
        $whitelabelID = $this->getWhitelabel($masterAgentID); 
        $list = $someModel->ListByWhiteLabel( $whitelabelID )->get();
        // log::info("wl = ".$whitelabelID." ma=".$masterAgentID." data".json_encode($list));
       
        $response = $this->build_menu_ma($list,$masterAgentID); 
        // echo $response;
        // print_r($this->downline);
   // log::info("wl = ".$whitelabelID." ma=".$masterAgentID." data-downline".json_encode($this->downline));
        return $response;
}
   


private function has_agent($id) {
  $ldagen=[]; 
  try { 
  	$someModel = new Agent;
  	 $someModel->setConnection($this->conn); 
        $list = $someModel->where('master_agent_id','=',$id)->get();
  $result = "";
  foreach ( $list as $row) {
          $result.= "<li> Agent {$row['username']}";
           array_push($this->downline,$row['username']);
   } 
 // log::info(" data-downline".json_encode($this->downline));
  // Log::info("CmsBL::has_agent ".$result);
} catch ( Exception $e ) {
			Log::info("CmsBL::has_agent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
  return  $result;
}  
private function has_children($rows,$id) {
   try { 
	    foreach ($rows as $row) {
	    if ($row['master_agent_parent_id'] == $id)
	      return true;
	  	}
  	} catch ( Exception $e ) {
			Log::info("CmsBL::has_children exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
  return false;
}
 

public function build_menu($rows,$parent=0)
{  
	 
  $hit = 0; 
  $result = "<ul>";
  try { 

  foreach ($rows as $row) 
  {
    if ($row['master_agent_parent_id'] == $parent){
      $result.= "<li> {$row['username']} ({$row['id']})";    

      if ($this->has_children($rows,$row['id']))
      {
        // $result.= 
        $this->build_menu($rows,$row['id']);
 
      }  
      else
      {
         $result.= "<ul>";
         // $result.=  $this->has_agent($row['id']);

           $someModel = new Agent;
              $someModel->setConnection($this->conn); 
              $list = $someModel->where('master_agent_id','=',$row['id'])->get();
              $result = "";
              // log::info(" data list-agen".json_encode($list));
              foreach ( $list as $rowagensi) {
              $result.= "<li> Agent {$rowagensi['username']}";
              array_push($this->downline,$rowagensi['username']);
              
              } 
           


         $result.= '</ul>';
       }
 
      $result.= "</li>";
      $hit++;
    }

  }
  $result.= "</ul>";
  if($hit <= 0)
  {
  	$result = "Data Kosong";
  }
 
	} catch ( Exception $e ) {
			Log::info("CmsBL::build_menu exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
	}	

   // log::info("  data_dline".json_encode($this->downline));

  return $this->downline;
}









private function build_trees_agent($id) {
  $ldagen=[]; 
  try { 
  $someModel = new Agent;
  $someModel->setConnection($this->conn); 
  $listagent = $someModel->where('master_agent_id','=',$id)->get();
  $result = "";
  foreach ( $listagent as $row) {
          $result.= "<li> Agent {$row['username']}";
 // <a class=\"green bold hand\" onclick=\"javascript:updateDataAgenC({$row['id']})\" > <i class=\"fa fa-pencil\"></i> </a>
  } 

  // $result .= "</ul>";
} catch ( Exception $e ) {
      Log::info("CmsBL::has_agent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
  return  $result;
}  
private function build_trees_children($rows,$id) {
   try { 
      foreach ($rows as $row) {
      if ($row['master_agent_parent_id'] == $id)
        return true;
      }
    } catch ( Exception $e ) {
      Log::info("CmsBL::has_children exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
  return false;
}
 

public function build_trees($rows,$parent=0)
{  
  // Log::info("CmsBL::build_menu start data =".json_encode($rows)); 


  $hit = 0; 
  $result = "<ul>";
  try { 

  foreach ($rows as $row) 
  {
    if ($row['master_agent_parent_id'] == $parent){
      $result.= "<li> {$row['username']} ({$row['id']}) ";       
      $result.= "";

      if ($this->build_trees_children($rows,$row['id']))
      {
        $result.= $this->build_trees($rows,$row['id']);
            // Log::info("SdkStickerBL::build_menu "); 

      }  
      else
      {
         $result.= "<ul>";
         $result.=  $this->build_trees_agent($row['id']);
         $result.= '</ul>';
     // Log::info("SdkStickerBL::has_agent "); 
      }
 
      $result.= "</li>";
      $hit++;
    }

  }
  $result.= "</ul>";
  if($hit <= 0)
  {
    $result = "Data Kosong";
  }
    // Log::info("CmsBL::build_menu end =".json_encode($result)); 

  } catch ( Exception $e ) {
      Log::info("CmsBL::build_menu exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
  } 
  return $result;
}



public function getFullnameMa($id) {
  $fullname ="";

  try { 
  $someModel = new MasterAgentProfileModel;
  $someModel->setConnection($this->connali); 
  $listagent = $someModel->where('master_agent_id','=',$id)->first();
  if(!empty($listagent))
  {
    $fullname = $listagent->fullname;
  }


 
} catch ( Exception $e ) {
      Log::info("CmsBL::getFullnameMa exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
  return  $fullname;
}  



//build ma list

 
private function has_children_ma($rows,$id) {
   try { 
      foreach ($rows as $row) {
      if ($row['master_agent_parent_id'] == $id)
        return true;
      }
    } catch ( Exception $e ) {
      Log::info("CmsBL::has_children exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
  return false;
}
 

public function build_menu_ma($rows,$parent=0)
{  
   
  $hit = 0; 
  $result = "<ul>";
  try { 

  foreach ($rows as $row) 
  {
    if ($row['master_agent_parent_id'] == $parent){
      $result.= "<li> {$row['username']} ({$row['id']}) <a class=\"red bold hand\" onclick=\"javascript:updateDataMaC({$row['id']})\" > <i class=\"fa fa-pencil\"></i> </a>";       
      $result.= "";

           array_push($this->downlineMa,$row['username']);

      if ($this->has_children_ma($rows,$row['id']))
      {
        // $result.= 
        $this->build_menu_ma($rows,$row['id']);
 
      }  
     
 
      $result.= "</li>";
      $hit++;
    }

  }
  $result.= "</ul>";
  if($hit <= 0)
  {
    $result = "Data Kosong";
  }
 
  } catch ( Exception $e ) {
      Log::info("CmsBL::build_menu exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
  } 

   log::info("  data_dline".json_encode($this->downlineMa));

  return $this->downlineMa;
}



// new list tree



function listMabyUpline($masterAgentID)
{        
    // log::info("test1 go"); 
        $someModel = new MainAgentModel;
        $someModel->setConnection($this->conn);
        $data = $someModel->get(); 
         array_push($this->downline,$masterAgentID); 
         $list = $this->buildtree($data,$masterAgentID); 
         // log::info("list data mainagent".json_encode($this->downline));
    
    // $Agent = new Agent;
  //       $Agent->setConnection($this->conn);
  //       $listAgent = $Agent->whereIn('master_agent_id',$this->downline)->get();
 return $this->downline;
}
function listAgentbyUpline($masterAgentID)
{        
   $someModel = new MainAgentModel;
        $someModel->setConnection($this->conn);
        $data = $someModel->get(); 
         array_push($this->downline,$masterAgentID); 
         $list = $this->buildtree($data,$masterAgentID); 
         // log::info("list data masteragent".json_encode($this->downline));
    
        $Agent = new Agent;
        $Agent->setConnection($this->conn);
        $listAgent = $Agent->whereIn('master_agent_id',$this->downline)->get();
        foreach ($listAgent as $row) {
          // $listing['username'] = $row['username'];          
                   array_push($this->downlineMa,$row['id']); 

        }
                 // log::info("list data agent".json_encode($this->downlineMa));

 return $this->downlineMa;
}

function buildtree($src_arr, $parent_id = 0, $tree = array())
{
    foreach($src_arr as $idx => $row)
    {
        if($row['master_agent_parent_id'] == $parent_id)
        {
            foreach($row as $k => $v)
                $tree[$row['id']][$k] = $v;

            array_push($this->downline,$row['id']);
            unset($src_arr[$idx]);
            $tree[$row['id']]['children'] = $this->buildtree($src_arr, $row['id']);
        }
    }
    ksort($tree);
    return $tree;

}

public function getMaEmail($userid) {
    try {
  $emailMA = "";
  $someModel = new MasterAgentProfileModel;
  $someModel->setConnection($this->conn); 
  $MA = $someModel
   ->where('master_agent_id','=',$userid)
   ->first();
    if(!empty($MA))
    {
      $emailMA = $MA->email;
    }
     } catch ( Exception $e ) {
      Log::info("HelpdeskBL::getMaEmail  exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
   return $emailMA;
}

  
  public function putQueueLogActivity( $userID, $sessionID, $activityName, $activityDetail = null ) {
    $b = false;
      Log::info("HelpdeskBL::putQueueLogActivity GO".$userID." , ".$sessionID." , ".$activityName." , ".$activityDetail);
    try {
      $input = LogUserActivityBL::createInput( $userID, $sessionID, $activityName, $activityDetail );
      $logBL = new LogUserActivityBL( $input );
            Log::info("HelpdeskBL::putQueueLogActivity input".json_encode($input));

      $logBL->setLogDateTime();
      if (self::USE_ASYNC_PROCESS) {
        $queuename ="posku_default";
        $input = $logBL->getInput();
        $job = new InsertLogUserActivityJob( $input );
        if (! empty($queuename)) {
          $job->onQueue($queuename);
        }
        dispatch( $job );
        $b = true;
      } else {
         $id = $logBL->insert();
        $b = boolval($id);
      }
    } catch ( Exception $e ) {
      Log::info("HelpdeskBL::putQueueLogActivity user_id=".$userID." session_id=".$sessionID." activity_name=".$activityName." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
    return $b;
  }

  //      $this->putQueueLog( $userID, $username, $this->errCode, $this->errMsg );



  public function sendSMSNotifUpdateHP( $telpNumber,$whitelabel,$agent_id,$userid,$hplama,$hpbru, $userName="" ) {
    try {
      if (empty($telpNumber)) {
        return false;
      }     
           log::info("go send update sms");
      $msg= "PosFIn - Telah dilakukan perubahan No HP untuk AgenID= ".$userid.", dari ".$hplama." menjadi ".$hpbru.". Hub helpdesk jika anda mencurigai perubahan ini.";
      $smsSender = new SMSSenderManager( $whitelabel );
      $smsSender->setPurposeType( "SMSINFOAGENT" );
      $b = $smsSender->send( $telpNumber, $msg );
      if ($b) {
        $cache = new CacheAgentSendSMSDeviceToken();
        $cache->setFlagRecentlySMSSent( $agent_id );
        $cache = new CacheAgentSendSMSDeviceTokenCounter();
        $cache->increment($agent_id );  

        $this->sendSMSNotifUpdateHPMA($agent_id,$msg);


      
      }
      return $b;
    } catch ( Exception $e ) {
      Log::info("AgentAuthBL::sendSMSNotifUpdate exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
  
    return false;
  }

  public function sendSMSNotifUpdateHPMA( $userid,$msg) {
    try {
     log::info("go send upline sms");
  $someModel = new MasterAgentProfileModel;
  $someModel->setConnection($this->conn); 
  $MA = $someModel
  ->join('agent','master_agent_profile.master_agent_id','agent.master_agent_id')
  ->join('master_agent','master_agent.id','agent.master_agent_id')
  ->where('agent.id','=',$userid)->first();

  if (empty($MA->phone_number)) {
        return false;
      }    

      $smsSender = new SMSSenderManager( $MA->white_label_id );
      $smsSender->setPurposeType( "SMSINFOMASTERAGENT" );
      $b = $smsSender->send( $MA->phone_number, $msg );
      if ($b) {
        $cache = new CacheAgentSendSMSDeviceToken();
        $cache->setFlagRecentlySMSSent( $MA->id );
        $cache = new CacheAgentSendSMSDeviceTokenCounter();
        $cache->increment($MA->id );
      }
      return $b;
    } catch ( Exception $e ) {
      Log::info("AgentAuthBL::sendSMSNotifUpdate exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
    return false;
  }
  


public function sendmailnotifupdate($toEmail,$toEmailMA,$fullname,$username,$data,$useragent="unknown",$msg=""){ 
 Log::info("HelpdeskBL::sendmailnotifupdate GO");

    try
    { 
$nameBlade = 'email.updatenotif';
$subject= 'Update Agent Information '.$msg; 
$fromEmail = 'helpdesk@posfin.id';


$ua=$this->getBrowser($useragent);
$yourbrowser= "" . $ua['name'] . " , " . $ua['version'] . " on " .$ua['platform'];// . " reports: <br >" . $ua['userAgent'];
  
$paramBlade = array(
            'fullname' => $fullname,
            'username' => $username,
            'useragent' =>  $yourbrowser,
            'msg' =>  $msg,
            'waktu' => date('d M Y H:i:s'),
            'expiry_hours' => '13' ,
            'userid' => $data['userid'],
            'agentid' => $data['agentid'],
            'malama' => $data['malama'],
            'mabaru' => $data['mabaru'],
            'hplama' => $data['hplama'],
            'hpbru' => $data['hpbru'],
            'maillama' => $data['maillama'],
            'mailbaru' => $data['mailbaru'], 
            'stslama' => $data['stslama'],
            'stsbaru' => $data['stsbaru'],
            'dlama' => $data['dlama'],
            'dbaru' => $data['dbaru']
        );
$mails = new UtilEmail();
$mails->sendEmailByTemplateView( $nameBlade, $paramBlade, $subject, $toEmail, $fullname , $fromEmail );
if(!empty($toEmailMA))
{
  $mails->sendEmailByTemplateView( $nameBlade, $paramBlade, $subject, $toEmailMA, $fullname , $fromEmail );
}




      } catch ( Exception $e ) {
      Log::info("HelpdeskBL::sendmaillogin exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
      } 
   }
 




/* =========================================================== PUSH ===================================================================== 

=========================================================================================================================================
*/

//...

  /*
  //List of samples
  sendPushToAllAgentInWhitelabel();
  sendPushToAllAgentInMasterAgent();
  sendPushToSelectedAgent();
  sendPushToSelectedTokenAgent();
  sendPushToTopic();
  subscribeAllAgentInWhitelabelToTopic();
  subscribeAllAgentInMasterAgentToTopic();
  subscribeSelectedAgentToTopic();
  subscribeSelectedTokenToTopic();
  */

  public function sendPushToAllAgentInWhitelabel($whitelabelID,$arrMsg,$deliverTime="") {
    // Sample : Sending Push to all agent in whitelabel
    // $whitelabelID= 1234567890;
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_WHITELABEL);

    // Compose push content. For example TEXT push. 
    // $arrMsg['text'] = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    // $arrMsg['title'] = "Selamat siang agen PosFin";
    // $arrMsg['body'] = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $openBrowser = false;
    $saveToInbox = true;
    $pushType = "ALLAGENTINWL"; 
    $pushBL->composeTextPush('' , $arrMsg['title'], $arrMsg['body'], $saveToInbox , $pushType);
    $pushBL->setQueueName('posku_default');
    /*
    // You can compose URL push
    $url = "http://www.facebook.com";
    $urlTitle = "Welcome to Facebook";
    $title = "Join with Facebook";
    $body = "Join with facebook today";
    
    $pushBL->composeURLPush($url, $urlTitle, $title, $body, $openBrowser, $saveToInbox);      

    // You can compose IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $saveToInbox = true;
    $pushBL->composeImagePush($imageUrl, $title, $body, $saveToInbox);  

    // You can compose CLICKABLE IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $openBrowser = true;
    $url="http://www.detik.com";
    $saveToInbox = true;
    $pushBL->composeClickableImagePush($imageUrl, $url, $title, $body, $openBrowser, $saveToInbox);
    */

    /*
    // Set Option (Optional). This is can be skipped. System will use default setting. 
    $TTL = 300;
    $delayWhileIddle = true;
    $androidPriority = PushKey::PRIORITY_ANDROID_HIGH;
    $applePriority = PushKey::PRIORITY_APPLE_9;
    $pushBL->setFCMOption($TTL, $delayWhileIddle, $androidPriority, $applePriority);
    
    // Set Notification (Optional). This is can be skipped. System will use default notification. 
    $sound = true;
    $icon = '';
    $tag = '';
    $color = '';
    $badge = 0;
    $subTitle = '';
    $pushBL->setFCMNotification($sound, $icon, $tag, $color, $badge, $subTitle);
    */

    /*
    // Set queue name (Optional). You can set system to use specific queue to send push.
    
    */
    
    /*
    // By default, system will send push through queue.
    // You can send push directly without queue by setting useQueue to false. $pushBL->useQueue(false);
    */
    

    /*
    // Schedulling push or send push at specific date and time.
    // Schedulling requires push in queue mode.
    // When useQueue(false), this schedule will not be applied.*/ 
    if(!empty($deliverTime) && !is_null($deliverTime))
    {
      $pushBL->setDeliveryTime($deliverTime); 
    }
    
    

    // $rs will be an array of  "Push sent via queue" string when push sent using queue.   
    // $rs will be an array of json when not using queue.  
    $rs = $pushBL->sendPush();
    log::info("hasilnya = ".json_encode($rs));
    return $rs;     
  }

  public function sendPushToAllAgentInMasterAgent($whitelabelID,$arrMsg,$arrMA,$deliverTime="") {
    // Sample : Sending Push to all agent in Master Agent
    // $whitelabelID= 1234567890;
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_MASTER_AGENT);
    // Define list ID of Master Agent in array
    // $arrMA = [1541668668,1541475295];
    $pushBL->setMasterAgent($arrMA);
    
    // Compose push content. For example TEXT push. 
    // $text = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    // $title = "Selamat siang agen PosFin";
    // $body = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    // $pushBL->composeTextPush($text, $title, $body);

    // $pushBL->composeTextPush('', $arrMsg['title'], $arrMsg['body']);
    $openBrowser = false;
    $saveToInbox = true;
    $pushType = "ALLAGENTINMA"; 
    $pushBL->composeTextPush('' , $arrMsg['title'], $arrMsg['body'], $saveToInbox , $pushType);

    $pushBL->setQueueName('posku_default');
      if(!empty($deliverTime) && !is_null($deliverTime))
    {
      $pushBL->setDeliveryTime($deliverTime); 
    }


    /*
    // You can compose URL push
    $url = "http://www.facebook.com";
    $urlTitle = "Welcome to Facebook";
    $title = "Join with Facebook";
    $body = "Join with facebook today";
    $openBrowser = false;
    $saveToInbox = true;
    $pushBL->composeURLPush($url, $urlTitle, $title, $body, $openBrowser, $saveToInbox);      

    // You can compose IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $saveToInbox = true;
    $pushBL->composeImagePush($imageUrl, $title, $body, $saveToInbox);  

    // You can compose CLICKABLE IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $openBrowser = true;
    $url="http://www.detik.com";
    $saveToInbox = true;
    $pushBL->composeClickableImagePush($imageUrl, $url, $title, $body, $openBrowser, $saveToInbox);
    */

    /*
    // Set Option (Optional). This is can be skipped. System will use default setting. 
    $TTL = 300;
    $delayWhileIddle = true;
    $androidPriority = PushKey::PRIORITY_ANDROID_HIGH;
    $applePriority = PushKey::PRIORITY_APPLE_9;
    $pushBL->setFCMOption($TTL, $delayWhileIddle, $androidPriority, $applePriority);
    
    // Set Notification (Optional). This is can be skipped. System will use default notification. 
    $sound = true;
    $icon = '';
    $tag = '';
    $color = '';
    $badge = 0;
    $subTitle = '';
    $pushBL->setFCMNotification($sound, $icon, $tag, $color, $badge, $subTitle);
    */

    /*
    // Set queue name (Optional). You can set system to use specific queue to send push.
    $pushBL->setQueueName('fintechapi_log');
    */

    /*
    // By default, system will send push through queue.
    // You can send push directly without queue by setting useQueue to false. 
    $pushBL->useQueue(false);
    */

    /*
    // Schedulling push or send push at specific date and time.
    // Schedulling requires push in queue mode.
    // When useQueue(false), this schedule will not be applied. 
    $pushBL->setDeliveryTime('2019-03-28 10:16:00');
    */

    // $rs will be an array of  "Push sent via queue" string when push sent using queue.   
    // $rs will be an array of json when not using queue.  
    $rs = $pushBL->sendPush();
    return $rs;     
  }

  //{push_type_send=ONEAGENTTEXT, save_to_inbox=true, push_content=TEXT}
public function sendPushToSelectedAgent($whitelabelID,$arrMsg,$arrAgent,$deliverTime="") {
      // public function sendPushToAllAgentInMasterAgent($whitelabelID,$arrMsg,$arrMA,$deliverTime="") 
    // Sample : Sending Push to selected agent
    // $whitelabelID= 1234567890;
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_AGENT);
    // Define list ID of Agent in array 
    // $arrAgent = [1558678800,1547745548];
    $pushBL->setAgent($arrAgent);$saveToInbox = true;
    $pushType = "TEXT"; 
    $bodytext = $this->truncateString($arrMsg['body'], 304, true);
    $pushBL->composeTextPush($arrMsg['text'] , $arrMsg['title'], $arrMsg['body'], $saveToInbox , $pushType);
 
    // Compose push content. For example TEXT push. 
    // $text = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    // $title = "Selamat siang agen PosFin";
    // $body = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    // $pushBL->composeTextPush($text, $title, $body);
    $pushBL->setQueueName('posku_default');
    /*
    // You can compose URL push
    $url = "http://www.facebook.com";
    $urlTitle = "Welcome to Facebook";
    $title = "Join with Facebook"; 
    $body = "Join with facebook today";
    $openBrowser = false;
    
    $pushBL->composeURLPush($url, $urlTitle, $title, $body, $openBrowser, $saveToInbox);      

    // You can compose IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $saveToInbox = true;
    $pushBL->composeImagePush($imageUrl, $title, $body, $saveToInbox);  

    // You can compose CLICKABLE IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $openBrowser = true;
    $url="http://www.detik.com";
    $saveToInbox = true;
    $pushBL->composeClickableImagePush($imageUrl, $url, $title, $body, $openBrowser, $saveToInbox);
    */

    /*
    // Set Option (Optional). This is can be skipped. System will use default setting. 
    $TTL = 300;
    $delayWhileIddle = true;
    $androidPriority = PushKey::PRIORITY_ANDROID_HIGH;
    $applePriority = PushKey::PRIORITY_APPLE_9;
    $pushBL->setFCMOption($TTL, $delayWhileIddle, $androidPriority, $applePriority);
    
    // Set Notification (Optional). This is can be skipped. System will use default notification. 
    $sound = true;
    $icon = '';
    $tag = '';
    $color = '';
    $badge = 0;
    $subTitle = '';
    $pushBL->setFCMNotification($sound, $icon, $tag, $color, $badge, $subTitle);
    */

    /*
    // Set queue name (Optional). You can set system to use specific queue to send push.
    $pushBL->setQueueName('fintechapi_log');
    */

    /*
    // By default, system will send push through queue.
    // You can send push directly without queue by setting useQueue to false. 
    $pushBL->useQueue(false);
    */

    /*
    // Schedulling push or send push at specific date and time.
    // Schedulling requires push in queue mode.
    // When useQueue(false), this schedule will not be applied. 
    $pushBL->setDeliveryTime('2019-03-28 10:16:00');
    */

    // $rs will be an array of  "Push sent via queue" string when push sent using queue.   
    // $rs will be an array of json when not using queue.  

      if(!empty($deliverTime) && !is_null($deliverTime))
    {
      $pushBL->setDeliveryTime($deliverTime); 
    }


    $rs = $pushBL->sendPush();
    return $rs;     
  }
 

  public function sendPushToSelectedTokenAgent() {
    // Sample : Sending Push to selected token agent
    $whitelabelID= 1234567890;
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_DIRECT_TOKEN);
    // Define list agent token in array
    $arrAgenttokens = ['cx7U3ye2dww:APA91bF_-fStx-oF2SrjjH4bLnLVjVtQ7ISIyeFr_2wMQwWU0ZVNbVt4gRKUdSyjLu4fWkpRDhohI5FlugcBUvq0sneEd98xoNr_ciIRQBbYgYTAW11YWl1KEaV-VYBa1JD5FU0--986'];
    $pushBL->setTokens($arrAgenttokens); 
    
    // Compose push content. For example TEXT push. 
    $text = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $title = "Selamat siang agen PosFin";
    $body = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $pushBL->composeTextPush($text, $title, $body);
    
    /*
    // You can compose URL push
    $url = "http://www.facebook.com";
    $urlTitle = "Welcome to Facebook";
    $title = "Join with Facebook";
    $body = "Join with facebook today";
    $openBrowser = false;
    $saveToInbox = true;
    $pushBL->composeURLPush($url, $urlTitle, $title, $body, $openBrowser, $saveToInbox);      

    // You can compose IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $saveToInbox = true;
    $pushBL->composeImagePush($imageUrl, $title, $body, $saveToInbox);  

    // You can compose CLICKABLE IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $openBrowser = true;
    $url="http://www.detik.com";
    $saveToInbox = true;
    $pushBL->composeClickableImagePush($imageUrl, $url, $title, $body, $openBrowser, $saveToInbox);
    */

    /*
    // Set Option (Optional). This is can be skipped. System will use default setting. 
    $TTL = 300;
    $delayWhileIddle = true;
    $androidPriority = PushKey::PRIORITY_ANDROID_HIGH;
    $applePriority = PushKey::PRIORITY_APPLE_9;
    $pushBL->setFCMOption($TTL, $delayWhileIddle, $androidPriority, $applePriority);
    
    // Set Notification (Optional). This is can be skipped. System will use default notification. 
    $sound = true;
    $icon = '';
    $tag = '';
    $color = '';
    $badge = 0;
    $subTitle = '';
    $pushBL->setFCMNotification($sound, $icon, $tag, $color, $badge, $subTitle);
    */

    /*
    // Set queue name (Optional). You can set system to use specific queue to send push.
    $pushBL->setQueueName('fintechapi_log');
    */

    /*
    // By default, system will send push through queue.
    // You can send push directly without queue by setting useQueue to false. 
    $pushBL->useQueue(false);
    */

    /*
    // Schedulling push or send push at specific date and time.
    // Schedulling requires push in queue mode.
    // When useQueue(false), this schedule will not be applied. 
    $pushBL->setDeliveryTime('2019-03-28 10:16:00');
    */

    // $rs will be an array of  "Push sent via queue" string when push sent using queue.   
    // $rs will be an array of json when not using queue.  
    $rs = $pushBL->sendPush();
    return $rs;     
  }

  private function sendPushToTopic() {
    // Sample : Sending Push to selected token agent
    $whitelabelID= 1234567890;
    $pushBL = new PushBL($whitelabelID);
    // Define list of topic in array
    $arrTopic = ['topic_test_123','agent_nakal'];
    $pushBL->useTopic($arrTopic);

    // Compose push content. For example TEXT push. 
    $text = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $title = "Selamat siang agen PosFin";
    $body = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $pushBL->composeTextPush($text, $title, $body);
    
    /*
    // You can compose URL push
    $url = "http://www.facebook.com";
    $urlTitle = "Welcome to Facebook";
    $title = "Join with Facebook";
    $body = "Join with facebook today";
    $openBrowser = false;
    $saveToInbox = true;
    $pushBL->composeURLPush($url, $urlTitle, $title, $body, $openBrowser, $saveToInbox);      

    // You can compose IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $saveToInbox = true;
    $pushBL->composeImagePush($imageUrl, $title, $body, $saveToInbox);  

    // You can compose CLICKABLE IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $openBrowser = true;
    $url="http://www.detik.com";
    $saveToInbox = true;
    $pushBL->composeClickableImagePush($imageUrl, $url, $title, $body, $openBrowser, $saveToInbox);
    */

    /*
    // Set Option (Optional). This is can be skipped. System will use default setting. 
    $TTL = 300;
    $delayWhileIddle = true;
    $androidPriority = PushKey::PRIORITY_ANDROID_HIGH;
    $applePriority = PushKey::PRIORITY_APPLE_9;
    $pushBL->setFCMOption($TTL, $delayWhileIddle, $androidPriority, $applePriority);
    
    // Set Notification (Optional). This is can be skipped. System will use default notification. 
    $sound = true;
    $icon = '';
    $tag = '';
    $color = '';
    $badge = 0;
    $subTitle = '';
    $pushBL->setFCMNotification($sound, $icon, $tag, $color, $badge, $subTitle);
    */

    /*
    // Set queue name (Optional). You can set system to use specific queue to send push.
    $pushBL->setQueueName('fintechapi_log');
    */

    /*
    // By default, system will send push through queue.
    // You can send push directly without queue by setting useQueue to false. 
    $pushBL->useQueue(false);
    */

    /*
    // Schedulling push or send push at specific date and time.
    // Schedulling requires push in queue mode.
    // When useQueue(false), this schedule will not be applied. 
    $pushBL->setDeliveryTime('2019-03-28 10:16:00');
    */

    // $rs will be an array of  "Push sent via queue" string when push sent using queue.   
    // $rs will be an array of json when not using queue.  
    $rs = $pushBL->sendPush();
    return $rs;     
  }

  private function subscribeAllAgentInWhitelabelToTopic() {
    //Sample : Subscribe all agent in whitelabel to topic
    $whitelabelID= 1234567890;
    $topicName = 'topic_test_123';
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_WHITELABEL);
    $result = $pushBL->subscribeFCMTopicBatch($topicName);
    /*
    //To unsubscribe
    $result = $pushBL->unsubscribeFCMTopicBatch($topicName);
    */    
    return $result;   
  }

  private function subscribeAllAgentInMasterAgentToTopic() {
    //Sample : Subscribe all agent in master agent to topic
    $whitelabelID= 1234567890;
    $topicName = 'topic_test_123';
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_MASTER_AGENT);
    // Define list ID of Master Agent in array
    $arrMA = [9999998,9999999];
    $pushBL->setMasterAgent($arrMA);
    $result = $pushBL->subscribeFCMTopicBatch($topicName);    
    /*
    //To unsubscribe
    $result = $pushBL->unsubscribeFCMTopicBatch($topicName);
    */    
    return $result;   
  }

  private function subscribeSelectedAgentToTopic() {
    //Sample : Subscribe selected agent to topic
    $whitelabelID= 1234567890;
    $topicName = 'topic_test_123';
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_AGENT);
    // Define list ID of Agent in array 
    $arrAgent = [1540210622,153807800,1538078495,1538078486];
    $pushBL->setAgent($arrAgent);
    $result = $pushBL->subscribeFCMTopicBatch($topicName);    
    /*
    //To unsubscribe
    $result = $pushBL->unsubscribeFCMTopicBatch($topicName);
    */    
    return $result;   
  }

  private function subscribeSelectedTokenToTopic() {
    //Sample : Subscribe selected agent to topic
    $whitelabelID= 1234567890;
    $topicName = 'topic_test_123';
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_DIRECT_TOKEN);
    // Define list agent token in array
    $arrAgenttokens = ['f_uYqgOVNOQ:APA91bEmPYY37cnFf8yiiefni8XV','ytuYqgOVNOQ:APA91bEmPYY37cnFf8yiiefnijky'];
    $pushBL->setTokens($arrAgenttokens);
    $result = $pushBL->subscribeFCMTopicBatch($topicName);    
    /*
    //To unsubscribe
    $result = $pushBL->unsubscribeFCMTopicBatch($topicName);
    */    
    return $result;   
  }



/* ======================================================= END PUSH ===================================================================== 

=========================================================================================================================================
*/


function listAgentbyUplinebyID($masterAgentID)
{        
   $someModel = new MainAgentModel;
        $someModel->setConnection($this->conn111);
        $data = $someModel->get(); 
         array_push($this->downline,$masterAgentID); 
         $list = $this->buildtree($data,$masterAgentID); 
         log::info("list data masteragent".json_encode($this->downline));
    
    $Agent = new Agent;
        $Agent->setConnection($this->conn111);
        $listAgent = $Agent->whereIn('master_agent_id',$this->downline)->get();
        foreach ($listAgent as $row) {
          // $listing['username'] = $row['username'];          
                   array_push($this->downlineMa,$row['id']); 

        }
                 log::info("list data agent".json_encode($this->downlineMa));

 return $this->downlineMa;
}



function listAgentbyUplinesums($masterAgentID,$status="")
{        
        $this->downline=[];$this->downlineMa=[];
        $someModel = new MainAgentModel;
        $someModel->setConnection($this->conn111);
        $data = $someModel->get(); 
         array_push($this->downline,$masterAgentID); 
         $list = $this->buildtree($data,$masterAgentID); 
         // log::info("list data masteragent".json_encode($this->downline));
    
    $Agent = new Agent;
        $Agent->setConnection($this->conn111);
        if(!empty($status))
        {
           // log::info("q1");
           $listAgent = $Agent->distinct()
                  ->whereIn('master_agent_id',$this->downline)
                  ->where('status','=',$status)
                  ->get();
        }
        else
        {
          // log::info("q2");
            $listAgent = $Agent->distinct()
            ->whereIn('master_agent_id',$this->downline) 
            ->get(); 
        }
       
        foreach ($listAgent as $row) { 
          // $listing['username'] = $row['username'];          
                   array_push($this->downlineMa,$row['id']); 
                   // array_push($this->downlineMa,$row['status']); 

        }
                 // log::info("list data agent".json_encode($this->downlineMa)); 

 return count($this->downlineMa);
}

function listSumTrxMa($masterAgentID,$month)
{        
        $this->downline=[]; $years=date("Y");
        $someModel = new MainAgentModel;
        $someModel->setConnection($this->conn111);
        $data = $someModel->get(); 
         array_push($this->downline,$masterAgentID); 
         $list = $this->buildtree($data,$masterAgentID); 
         // log::info("list data masteragent".json_encode($this->downline));
    
        $Agent = new ReportSumTrxMa;
        $Agent->setConnection($this->conn111);
         
           $listAgent = $Agent 
                  ->whereIn('master_agent_id',$this->downline)
                  ->where('MONTH','=',$month)
                  ->where('YEAR','=',$years)
                  ->where('api_error_code','=',0)
                  ->sum('total_trx'); 
         // log::info("year =".$years." month =".$month." summs =".$listAgent); 
        
       
        // foreach ($listAgent as $row) { 
          // $listing['username'] = $row['username'];         
                   // array_push($this->downlineMa,$row['status']); 
// 
        // }
                 // log::info("list data agent".json_encode($this->downlineMa)); 

 return (int)$listAgent;
}


function totalMaTrxbyAgent($masterAgentID)
{
  $this->downline=[];$this->downlineMa=[];
        $someModel = new MainAgentModel;
        $someModel->setConnection($this->conn111);
        $data = $someModel->get(); 
         array_push($this->downline,$masterAgentID); 
         $list = $this->buildtree($data,$masterAgentID); 
         log::info("list data masteragent".json_encode($this->downline));
    
        $Agent = new LogTrxAgent;
        $Agent->setConnection($this->conn111);
        $thisyear=date("Y");
           log::info("q1");
           $listAgent = $Agent->whereYear('log_datetime', '=', date('Y'))
           ->whereMonth('created_at', '=', date('m'))
                  ->whereIn('master_agent_id',$this->downline)
                  ->selectRaw('master_agent_id,trx_result_code,count(trx_result_code) as total')
                  ->get();
       // DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
        log::info("data ==".json_encode($listAgent));
}
function totalAgentbyMa($masterAgentID)
{
         $Agent = new Agent;
        $Agent->setConnection($this->conn111);
           
           $listAgent = $Agent
                  ->where('master_agent_id','=',$masterAgentID)
                  ->count(); 
        // log::info("data ==".json_encode($listAgent));
        return $listAgent;
}

// endGame 


function truncateString($str, $chars, $to_space, $replacement="...") {
   if($chars > strlen($str)) return $str;

   $str = substr($str, 0, $chars);
   $space_pos = strrpos($str, " ");
   if($to_space && $space_pos >= 0) 
       $str = substr($str, 0, strrpos($str, " "));

   return($str . $replacement);
}

function newlistagentbyMa($ma) {
     // $Agent = new MainAgentModel;
     //    $Agent->setConnection($this->conn111);
           
//            $listAgent = $Agent->selectRaw('SELECT t1.id AS lev1, t2.id as lev2, t3.id as lev3, t4.id as lev4 , t5.id as lev5  
// FROM master_agent AS t1
// LEFT JOIN master_agent AS t2 ON t2.master_agent_parent_id = t1.id
// LEFT JOIN master_agent AS t3 ON t3.master_agent_parent_id = t2.id
// LEFT JOIN master_agent AS t4 ON t4.master_agent_parent_id = t3.id 
// LEFT JOIN master_agent AS t5 ON t5.master_agent_parent_id = t4.id 
// WHERE t1.id = 9850000')->get();
                  
        // log::info("data ==".json_encode($listAgent));

          $listAgent =  DB::select( DB::raw("SELECT t1.id AS lev1, t2.id as lev2, t3.id as lev3, t4.id as lev4 , t5.id as lev5  
FROM master_agent AS t1
LEFT JOIN master_agent AS t2 ON t2.master_agent_parent_id = t1.id
LEFT JOIN master_agent AS t3 ON t3.master_agent_parent_id = t2.id
LEFT JOIN master_agent AS t4 ON t4.master_agent_parent_id = t3.id 
LEFT JOIN master_agent AS t5 ON t5.master_agent_parent_id = t4.id 
WHERE t1.id = '9850000'") );
        return $listAgent;
   /*

 SELECT t1.id AS lev1, t2.id as lev2, t3.id as lev3, t4.id as lev4 , t5.id as lev5  
FROM master_agent AS t1
LEFT JOIN master_agent AS t2 ON t2.master_agent_parent_id = t1.id
LEFT JOIN master_agent AS t3 ON t3.master_agent_parent_id = t2.id
LEFT JOIN master_agent AS t4 ON t4.master_agent_parent_id = t3.id 
LEFT JOIN master_agent AS t5 ON t5.master_agent_parent_id = t4.id 
WHERE t1.id = 9850000 
*/
}

public function sendmailAntv($toEmail, $fullname, $username){ 
 Log::info("HelpdeskBL::sendmailnotifupdate GO ");
 
    try
    { 
        $msg = "new mail";
        $b=true;
        // $toEmail = 'ferizaenalabidin@gmail.com';
        // $fullname = 'Feri Zaenal Abidin';
        // $username = '6285311223131';
        $nameBlade = 'email.userantvnotif';
        $subject= 'Posfin Agent Information '.$username; 
        $fromEmail = 'helpdesk@posfin.id'; 
 
// $ua=$this->getBrowser($useragent);
// $yourbrowser= "" . $ua['name'] . " , " . $ua['version'] . " on " .$ua['platform'];// . " reports: <br >" . $ua['userAgent'];
  
$paramBlade = array(
            'fullname' => $fullname,
            'username' => $username, 
            'msg' =>  $msg,
            'waktu' => date('d M Y H:i:s'),
            'expiry_hours' => '13'  
        );
$mails = new UtilEmail(); 
$mails->sendEmailByTemplateView( $nameBlade, $paramBlade, $subject, $toEmail, $fullname , $fromEmail );

// if(!empty($toEmailMA))
// {
//   $mails->sendEmailByTemplateView( $nameBlade, $paramBlade, $subject, $toEmailMA, $fullname , $fromEmail );
// }
$activityDetail = "send mail to =".$toEmail." fullname=".$fullname." ".$username;
$this->putQueueLogActivity( '09909', $username, "send_email_antv", $activityDetail); 
   AgentAntv::where('username', $username) 
                        ->update( [
                        'send' => 'Y' 
                        ]);   
      } catch ( Exception $e ) { 
      Log::info("HelpdeskBL::sendmailAntv exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
      } 
      return $b;
   }  

// public function sendmailnotifupdate($toEmail,$toEmailMA,$fullname,$username,$data,$useragent="unknown",$msg=""){ 


  public function putQueuePushEmail( $toEmail, $fullname, $username ) {
    $b = false;
    $mindelay=1; 
    $maxdelay=10; 
    $interval = rand($mindelay,$maxdelay);  
    // $interval=10;
     // Log::info("HelpdeskBL::putQueuePushEmail GO");
 Log::info("HelpdeskBL::putQueuePushEmail GO mail=".$toEmail." full=".$fullname." user=".$username);

      // Log::info("HelpdeskBL::putQueueLogActivity GO".$userID." , ".$sessionID." , ".$activityName." , ".$activityDetail);
    try {
      // $input = LogUserActivityBL::createInput( $userID, $sessionID, $activityName, $activityDetail );
      // $logBL = new LogUserActivityBL( $input );
      //       Log::info("HelpdeskBL::putQueueLogActivity input".json_encode($input));
// $toEmail, $fullname, $username 
      // $logBL->setLogDateTime();
      if (self::USE_ASYNC_PROCESS) {
        $queuename ="posku_default";
        $job = new InsertLogPushMailJob( $toEmail, $fullname, $username );
        if (! empty($queuename)) {
          $job->delay($interval);
          $job->onQueue($queuename);
        }
        dispatch( $job );
        $b = true;
      } else {
         $id = $this->sendmailAntv($toEmail, $fullname, $username);
         $b = boolval($id);
      }
    } catch ( Exception $e ) {
      Log::info("HelpdeskBL::putQueueLogActivity user_id=".$userID." session_id=".$sessionID." activity_name=".$activityName." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    }
    return $b;
  }





}