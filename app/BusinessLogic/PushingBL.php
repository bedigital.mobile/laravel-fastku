<?php

namespace App\BusinessLogic;

use Log;
use Config;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Definition;    
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Core\Util\UtilCommon;
use App\Http\Controllers\BDGBaseController;
use App\Models\PartnerTrxReqModel;
use App\Models\PartnerDepositModel;
use App\Models\LogTrxAgent;
use App\Models\SummaryReconPendingModel;
use App\Models\Database\Product;
use App\Models\Database\ProductBreakup;
use Illuminate\Support\Facades\DB;   
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;  
use Symfony\Component\HttpKernel\Exception\HttpException; 
use App\Http\Controllers\Controller;
use App\BusinessLogic\Auth\AuthUserBL;  
 use App\Models\MainAgentModel;
use App\Models\Agent;
use App\Models\MasterAgentProfileModel; 
use App\BusinessLogic\Log\LogUserActivityBL;
use App\BusinessLogic\Log\LogUserLoginBL;
use App\Jobs\InsertLogUserActivityJob; 
use App\Jobs\InsertLogUserLoginJob;  
use App\BusinessLogic\Push\PushBL;

class PushingBL
{ 
	protected $userID; 

	const APPROVAL_UNKNOWN = 0;  
  const USE_ASYNC_PROCESS = false; 
  
	public function __construct(  ) {
		try {
			  
        // $this->conn =  Config::get ( 'webconf.cms.connectiondata' ); 
        $this->conn =  Config::get ( 'webconf.cms.connectiondata' ); 
        $this->downline =[];
        $this->downlineMa =[];
		} catch ( Exception $e ) {
			Log::info("CmsBL::__construct exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	} 

  /*
  //List of samples
  sendPushToAllAgentInWhitelabel();
  sendPushToAllAgentInMasterAgent();
  sendPushToSelectedAgent();
  sendPushToSelectedTokenAgent();
  sendPushToTopic();
  subscribeAllAgentInWhitelabelToTopic();
  subscribeAllAgentInMasterAgentToTopic();
  subscribeSelectedAgentToTopic();
  subscribeSelectedTokenToTopic();
  */

  private function sendPushToAllAgentInWhitelabel() {
    // Sample : Sending Push to all agent in whitelabel
    $whitelabelID= 1234567890;
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_WHITELABEL);

    // Compose push content. For example TEXT push. 
    $text = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $title = "Selamat siang agen PosFin";
    $body = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $pushBL->composeTextPush($text, $title, $body);
    
    /*
    // You can compose URL push
    $url = "http://www.facebook.com";
    $urlTitle = "Welcome to Facebook";
    $title = "Join with Facebook";
    $body = "Join with facebook today";
    $openBrowser = false;
    $saveToInbox = true;
    $pushBL->composeURLPush($url, $urlTitle, $title, $body, $openBrowser, $saveToInbox);      

    // You can compose IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $saveToInbox = true;
    $pushBL->composeImagePush($imageUrl, $title, $body, $saveToInbox);  

    // You can compose CLICKABLE IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $openBrowser = true;
    $url="http://www.detik.com";
    $saveToInbox = true;
    $pushBL->composeClickableImagePush($imageUrl, $url, $title, $body, $openBrowser, $saveToInbox);
    */

    /*
    // Set Option (Optional). This is can be skipped. System will use default setting. 
    $TTL = 300;
    $delayWhileIddle = true;
    $androidPriority = PushKey::PRIORITY_ANDROID_HIGH;
    $applePriority = PushKey::PRIORITY_APPLE_9;
    $pushBL->setFCMOption($TTL, $delayWhileIddle, $androidPriority, $applePriority);
    
    // Set Notification (Optional). This is can be skipped. System will use default notification. 
    $sound = true;
    $icon = '';
    $tag = '';
    $color = '';
    $badge = 0;
    $subTitle = '';
    $pushBL->setFCMNotification($sound, $icon, $tag, $color, $badge, $subTitle);
    */

    /*
    // Set queue name (Optional). You can set system to use specific queue to send push.
    $pushBL->setQueueName('fintechapi_log');
    */

    /*
    // By default, system will send push through queue.
    // You can send push directly without queue by setting useQueue to false. 
    $pushBL->useQueue(false);
    */

    /*
    // Schedulling push or send push at specific date and time.
    // Schedulling requires push in queue mode.
    // When useQueue(false), this schedule will not be applied. 
    $pushBL->setDeliveryTime('2019-03-28 10:16:00');
    */

    // $rs will be an array of  "Push sent via queue" string when push sent using queue.   
    // $rs will be an array of json when not using queue.  
    $rs = $pushBL->sendPush();
    return $rs;     
  }

  private function sendPushToAllAgentInMasterAgent() {
    // Sample : Sending Push to all agent in Master Agent
    $whitelabelID= 1234567890;
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_MASTER_AGENT);
    // Define list ID of Master Agent in array
    $arrMA = [9999998,9999999];
    $pushBL->setMasterAgent($arrMA);
    
    // Compose push content. For example TEXT push. 
    $text = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $title = "Selamat siang agen PosFin";
    $body = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $pushBL->composeTextPush($text, $title, $body);
    
    /*
    // You can compose URL push
    $url = "http://www.facebook.com";
    $urlTitle = "Welcome to Facebook";
    $title = "Join with Facebook";
    $body = "Join with facebook today";
    $openBrowser = false;
    $saveToInbox = true;
    $pushBL->composeURLPush($url, $urlTitle, $title, $body, $openBrowser, $saveToInbox);      

    // You can compose IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $saveToInbox = true;
    $pushBL->composeImagePush($imageUrl, $title, $body, $saveToInbox);  

    // You can compose CLICKABLE IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $openBrowser = true;
    $url="http://www.detik.com";
    $saveToInbox = true;
    $pushBL->composeClickableImagePush($imageUrl, $url, $title, $body, $openBrowser, $saveToInbox);
    */

    /*
    // Set Option (Optional). This is can be skipped. System will use default setting. 
    $TTL = 300;
    $delayWhileIddle = true;
    $androidPriority = PushKey::PRIORITY_ANDROID_HIGH;
    $applePriority = PushKey::PRIORITY_APPLE_9;
    $pushBL->setFCMOption($TTL, $delayWhileIddle, $androidPriority, $applePriority);
    
    // Set Notification (Optional). This is can be skipped. System will use default notification. 
    $sound = true;
    $icon = '';
    $tag = '';
    $color = '';
    $badge = 0;
    $subTitle = '';
    $pushBL->setFCMNotification($sound, $icon, $tag, $color, $badge, $subTitle);
    */

    /*
    // Set queue name (Optional). You can set system to use specific queue to send push.
    $pushBL->setQueueName('fintechapi_log');
    */

    /*
    // By default, system will send push through queue.
    // You can send push directly without queue by setting useQueue to false. 
    $pushBL->useQueue(false);
    */

    /*
    // Schedulling push or send push at specific date and time.
    // Schedulling requires push in queue mode.
    // When useQueue(false), this schedule will not be applied. 
    $pushBL->setDeliveryTime('2019-03-28 10:16:00');
    */

    // $rs will be an array of  "Push sent via queue" string when push sent using queue.   
    // $rs will be an array of json when not using queue.  
    $rs = $pushBL->sendPush();
    return $rs;     
  }

  private function sendPushToSelectedAgent() {
    // Sample : Sending Push to selected agent
    $whitelabelID= 1234567890;
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_AGENT);
    // Define list ID of Agent in array 
    $arrAgent = [1540210622,153807800,1538078495,1538078486];
    $pushBL->setAgent($arrAgent);
    
    // Compose push content. For example TEXT push. 
    $text = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $title = "Selamat siang agen PosFin";
    $body = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $pushBL->composeTextPush($text, $title, $body);
    
    /*
    // You can compose URL push
    $url = "http://www.facebook.com";
    $urlTitle = "Welcome to Facebook";
    $title = "Join with Facebook";
    $body = "Join with facebook today";
    $openBrowser = false;
    $saveToInbox = true;
    $pushBL->composeURLPush($url, $urlTitle, $title, $body, $openBrowser, $saveToInbox);      

    // You can compose IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $saveToInbox = true;
    $pushBL->composeImagePush($imageUrl, $title, $body, $saveToInbox);  

    // You can compose CLICKABLE IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $openBrowser = true;
    $url="http://www.detik.com";
    $saveToInbox = true;
    $pushBL->composeClickableImagePush($imageUrl, $url, $title, $body, $openBrowser, $saveToInbox);
    */

    /*
    // Set Option (Optional). This is can be skipped. System will use default setting. 
    $TTL = 300;
    $delayWhileIddle = true;
    $androidPriority = PushKey::PRIORITY_ANDROID_HIGH;
    $applePriority = PushKey::PRIORITY_APPLE_9;
    $pushBL->setFCMOption($TTL, $delayWhileIddle, $androidPriority, $applePriority);
    
    // Set Notification (Optional). This is can be skipped. System will use default notification. 
    $sound = true;
    $icon = '';
    $tag = '';
    $color = '';
    $badge = 0;
    $subTitle = '';
    $pushBL->setFCMNotification($sound, $icon, $tag, $color, $badge, $subTitle);
    */

    /*
    // Set queue name (Optional). You can set system to use specific queue to send push.
    $pushBL->setQueueName('fintechapi_log');
    */

    /*
    // By default, system will send push through queue.
    // You can send push directly without queue by setting useQueue to false. 
    $pushBL->useQueue(false);
    */

    /*
    // Schedulling push or send push at specific date and time.
    // Schedulling requires push in queue mode.
    // When useQueue(false), this schedule will not be applied. 
    $pushBL->setDeliveryTime('2019-03-28 10:16:00');
    */

    // $rs will be an array of  "Push sent via queue" string when push sent using queue.   
    // $rs will be an array of json when not using queue.  
    $rs = $pushBL->sendPush();
    return $rs;     
  }

  private function sendPushToSelectedTokenAgent() {
    // Sample : Sending Push to selected token agent
    $whitelabelID= 1234567890;
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_DIRECT_TOKEN);
    // Define list agent token in array
    $arrAgenttokens = ['f_uYqgOVNOQ:APA91bEmPYY37cnFf8yiiefni8XV','ytuYqgOVNOQ:APA91bEmPYY37cnFf8yiiefnijky'];
    $pushBL->setTokens($arrAgenttokens);
    
    // Compose push content. For example TEXT push. 
    $text = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $title = "Selamat siang agen PosFin";
    $body = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $pushBL->composeTextPush($text, $title, $body);
    
    /*
    // You can compose URL push
    $url = "http://www.facebook.com";
    $urlTitle = "Welcome to Facebook";
    $title = "Join with Facebook";
    $body = "Join with facebook today";
    $openBrowser = false;
    $saveToInbox = true;
    $pushBL->composeURLPush($url, $urlTitle, $title, $body, $openBrowser, $saveToInbox);      

    // You can compose IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $saveToInbox = true;
    $pushBL->composeImagePush($imageUrl, $title, $body, $saveToInbox);  

    // You can compose CLICKABLE IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $openBrowser = true;
    $url="http://www.detik.com";
    $saveToInbox = true;
    $pushBL->composeClickableImagePush($imageUrl, $url, $title, $body, $openBrowser, $saveToInbox);
    */

    /*
    // Set Option (Optional). This is can be skipped. System will use default setting. 
    $TTL = 300;
    $delayWhileIddle = true;
    $androidPriority = PushKey::PRIORITY_ANDROID_HIGH;
    $applePriority = PushKey::PRIORITY_APPLE_9;
    $pushBL->setFCMOption($TTL, $delayWhileIddle, $androidPriority, $applePriority);
    
    // Set Notification (Optional). This is can be skipped. System will use default notification. 
    $sound = true;
    $icon = '';
    $tag = '';
    $color = '';
    $badge = 0;
    $subTitle = '';
    $pushBL->setFCMNotification($sound, $icon, $tag, $color, $badge, $subTitle);
    */

    /*
    // Set queue name (Optional). You can set system to use specific queue to send push.
    $pushBL->setQueueName('fintechapi_log');
    */

    /*
    // By default, system will send push through queue.
    // You can send push directly without queue by setting useQueue to false. 
    $pushBL->useQueue(false);
    */

    /*
    // Schedulling push or send push at specific date and time.
    // Schedulling requires push in queue mode.
    // When useQueue(false), this schedule will not be applied. 
    $pushBL->setDeliveryTime('2019-03-28 10:16:00');
    */

    // $rs will be an array of  "Push sent via queue" string when push sent using queue.   
    // $rs will be an array of json when not using queue.  
    $rs = $pushBL->sendPush();
    return $rs;     
  }

  private function sendPushToTopic() {
    // Sample : Sending Push to selected token agent
    $whitelabelID= 1234567890;
    $pushBL = new PushBL($whitelabelID);
    // Define list of topic in array
    $arrTopic = ['topic_test_123','agent_nakal'];
    $pushBL->useTopic($arrTopic);

    // Compose push content. For example TEXT push. 
    $text = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $title = "Selamat siang agen PosFin";
    $body = "Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    $pushBL->composeTextPush($text, $title, $body);
    
    /*
    // You can compose URL push
    $url = "http://www.facebook.com";
    $urlTitle = "Welcome to Facebook";
    $title = "Join with Facebook";
    $body = "Join with facebook today";
    $openBrowser = false;
    $saveToInbox = true;
    $pushBL->composeURLPush($url, $urlTitle, $title, $body, $openBrowser, $saveToInbox);      

    // You can compose IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $saveToInbox = true;
    $pushBL->composeImagePush($imageUrl, $title, $body, $saveToInbox);  

    // You can compose CLICKABLE IMAGE push
    $imageUrl = "https://cdn.idntimes.com/content-images/community/2019/02/pua-679fc83ef3c58fefcbb7f4a1d471d091_600x400.jpg";
    $title = "Cute girl";
    $body = "Cute girl is looking at you right now";
    $openBrowser = true;
    $url="http://www.detik.com";
    $saveToInbox = true;
    $pushBL->composeClickableImagePush($imageUrl, $url, $title, $body, $openBrowser, $saveToInbox);
    */

    /*
    // Set Option (Optional). This is can be skipped. System will use default setting. 
    $TTL = 300;
    $delayWhileIddle = true;
    $androidPriority = PushKey::PRIORITY_ANDROID_HIGH;
    $applePriority = PushKey::PRIORITY_APPLE_9;
    $pushBL->setFCMOption($TTL, $delayWhileIddle, $androidPriority, $applePriority);
    
    // Set Notification (Optional). This is can be skipped. System will use default notification. 
    $sound = true;
    $icon = '';
    $tag = '';
    $color = '';
    $badge = 0;
    $subTitle = '';
    $pushBL->setFCMNotification($sound, $icon, $tag, $color, $badge, $subTitle);
    */

    /*
    // Set queue name (Optional). You can set system to use specific queue to send push.
    $pushBL->setQueueName('fintechapi_log');
    */

    /*
    // By default, system will send push through queue.
    // You can send push directly without queue by setting useQueue to false. 
    $pushBL->useQueue(false);
    */

    /*
    // Schedulling push or send push at specific date and time.
    // Schedulling requires push in queue mode.
    // When useQueue(false), this schedule will not be applied. 
    $pushBL->setDeliveryTime('2019-03-28 10:16:00');
    */

    // $rs will be an array of  "Push sent via queue" string when push sent using queue.   
    // $rs will be an array of json when not using queue.  
    $rs = $pushBL->sendPush();
    return $rs;     
  }

  private function subscribeAllAgentInWhitelabelToTopic() {
    //Sample : Subscribe all agent in whitelabel to topic
    $whitelabelID= 1234567890;
    $topicName = 'topic_test_123';
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_WHITELABEL);
    $result = $pushBL->subscribeFCMTopicBatch($topicName);
    /*
    //To unsubscribe
    $result = $pushBL->unsubscribeFCMTopicBatch($topicName);
    */    
    return $result;   
  }

  private function subscribeAllAgentInMasterAgentToTopic() {
    //Sample : Subscribe all agent in master agent to topic
    $whitelabelID= 1234567890;
    $topicName = 'topic_test_123';
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_MASTER_AGENT);
    // Define list ID of Master Agent in array
    $arrMA = [9999998,9999999];
    $pushBL->setMasterAgent($arrMA);
    $result = $pushBL->subscribeFCMTopicBatch($topicName);    
    /*
    //To unsubscribe
    $result = $pushBL->unsubscribeFCMTopicBatch($topicName);
    */    
    return $result;   
  }

  private function subscribeSelectedAgentToTopic() {
    //Sample : Subscribe selected agent to topic
    $whitelabelID= 1234567890;
    $topicName = 'topic_test_123';
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_AGENT);
    // Define list ID of Agent in array 
    $arrAgent = [1540210622,153807800,1538078495,1538078486];
    $pushBL->setAgent($arrAgent);
    $result = $pushBL->subscribeFCMTopicBatch($topicName);    
    /*
    //To unsubscribe
    $result = $pushBL->unsubscribeFCMTopicBatch($topicName);
    */    
    return $result;   
  }

  private function subscribeSelectedTokenToTopic() {
    //Sample : Subscribe selected agent to topic
    $whitelabelID= 1234567890;
    $topicName = 'topic_test_123';
    $pushBL = new PushBL($whitelabelID);
    $pushBL->useToken(PushKey::SCOPE_DIRECT_TOKEN);
    // Define list agent token in array
    $arrAgenttokens = ['f_uYqgOVNOQ:APA91bEmPYY37cnFf8yiiefni8XV','ytuYqgOVNOQ:APA91bEmPYY37cnFf8yiiefnijky'];
    $pushBL->setTokens($arrAgenttokens);
    $result = $pushBL->subscribeFCMTopicBatch($topicName);    
    /*
    //To unsubscribe
    $result = $pushBL->unsubscribeFCMTopicBatch($topicName);
    */    
    return $result;   
  }


}