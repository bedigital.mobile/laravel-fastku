<?php
namespace App\BusinessLogic\MasterAgent;

use Log;
use Config;
use Exception;
use DB;
use Illuminate\Database\Eloquent\Collection;
use App\ApiLogic\ApiDefinition;
use App\BusinessLogic\MasterAgent\MasterAgentBL;
use App\BusinessLogic\Bank\BankBL;
use App\SystemLogic\GeoInfo\GeoProvince;
use App\SystemLogic\GeoInfo\GeoCity;
use App\SystemLogic\GeoInfo\GeoDistrict;
use App\SystemLogic\GeoInfo\GeoVillage;
use App\Core\Util\UtilCommon;
use App\Core\Util\UtilEmail;
use App\Core\Util\UtilPhone;
use App\Models\Api\ApiError;
use App\Models\Database\MasterAgent;
use App\Models\Database\MasterAgentProfile;
use App\Models\Database\MasterAgentAlias;

class MasterAgentProfileBL extends MasterAgentBL
{
	protected $mProfile = null;
	
	public function __construct( $whitelabelID, $masterAgentID = 0, $apiversion = 1 ) {
		parent::__construct( $whitelabelID, $masterAgentID, $apiversion );
	}

	public function setProfileModel( MasterAgentProfile $mProfile ) {
		$this->mProfile = $mProfile;
		return $this;
	}
	
	public function getProfileModel() {
		try {
			if (is_null($this->mMasterAgent) || (! is_null($this->mMasterAgent) && empty($this->mMasterAgent->id))) {
				return $this->mProfile;
			}
			$found = false;
			try {
				if (! is_null($this->mProfile) && ! empty($this->mProfile->id) && $this->mProfile->master_agent_id == $this->mMasterAgent->id) {
					$found = true;
				} else {
					$this->mProfile = MasterAgentProfile::where( 'master_agent_id', '=', $this->mMasterAgent->id )->firstOrFail();
					if (! is_null($this->mProfile) && ! empty($this->mProfile->id)) {
						$found = true;
					}
				}
			} catch ( Exception $e ) {
				if ($this->debug) Log::info("MasterAgentProfileBL::getProfile master_agent_id=".$this->mMasterAgent->id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			if ($found) {
				$tobeSave = false;
				// detail name
				if (empty($this->mProfile->fullname)) {
					$this->mProfile->fullname = $this->mMasterAgent->username; // default fullname sama dengan username
				}
				if (! empty($this->mProfile->fullname) && (empty($this->mProfile->firstname) || empty($this->mProfile->middlename) || empty($this->mProfile->lastname))) {
					$name = trim($this->mProfile->fullname);
					$name = trim(str_replace(array("\t","\r","\n"), " ", $name));
					while ( strpos($name, "  ") > 0 ) {
						$name = trim(str_replace("  ", " ", $name));
					}
					if (! empty($name)) {
						if (strpos($name, " ") > 0) {
							$arr = explode(" ", $name);
							$jml = count($arr);
							if ($jml >= 2) {
								if (empty($this->mProfile->firstname)) {
									$this->mProfile->firstname = $arr[0];
									$tobeSave = true;
								}
								if (empty($this->mProfile->lastname)) {
									$this->mProfile->lastname = $arr[$jml-1];
									$tobeSave = true;
								}
								if ($jml >= 3 && empty($this->mProfile->middlename)) {
									$this->mProfile->middlename = $arr[1];
									$tobeSave = true;
								}
							}
						}
					}
				}
				// propinsi
				if (! empty($this->mProfile->province_id) && empty($this->mProfile->province_name)) {
					try {
						$geoProv = new GeoProvince();
						$this->mProfile->province_name = $geoProv->getProvinceName( $this->mProfile->province_id );
						if (empty($this->mProfile->province_name)) {
							$this->mProfile->province_id = null;
						} else {
							if (strlen($this->mProfile->province_name) > 100) {
								$this->mProfile->province_name = substr($this->mProfile->province_name, 0, 100);
							}
						}
						$tobeSave = true;
					} catch ( Exception $e ) {
						Log::info("MasterAgentProfileBL::getProfile master_agent_id=".$this->mMasterAgent->id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				} else if (is_null($this->mProfile->province_id) && ! empty($this->mProfile->province_name)) {
					try {
						$geoProv = new GeoProvince();
						$this->mProfile->province_id = $geoProv->getProvinceID( $this->mProfile->province_name );
						$tobeSave = true;
					} catch ( Exception $e ) {
						Log::info("MasterAgentProfileBL::getProfile master_agent_id=".$this->mMasterAgent->id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				}
				// city
				if (! empty($this->mProfile->city_id) && empty($this->mProfile->city_name)) {
					try {
						$geoCity = new GeoCity( $this->mProfile->province_id );
						$this->mProfile->city_name = $geoCity->getCityName( $this->mProfile->city_id );
						if (empty($this->mProfile->city_name)) {
							$this->mProfile->city_id = null;
						} else {
							if (strlen($this->mProfile->city_name) > 100) {
								$this->mProfile->city_name = substr($this->mProfile->city_name, 0, 100);
							}
						}
						$tobeSave = true;
					} catch ( Exception $e ) {
						Log::info("MasterAgentProfileBL::getProfile master_agent_id=".$this->mMasterAgent->id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				} else if (is_null($this->mProfile->city_id) && ! empty($this->mProfile->city_name)) {
					try {
						$geoCity = new GeoCity( $this->mProfile->province_id );
						$this->mProfile->city_id = $geoCity->getCityID( $this->mProfile->city_name );
						$tobeSave = true;
					} catch ( Exception $e ) {
						Log::info("MasterAgentProfileBL::getProfile master_agent_id=".$this->mMasterAgent->id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				}
				// district
				if (! empty($this->mProfile->district_id) && empty($this->mProfile->district_name)) {
					try {
						$geoDistrict = new GeoDistrict( $this->mProfile->city_id, $this->mProfile->province_id );
						$this->mProfile->district_name = $geoDistrict->getDistrictName( $this->mProfile->district_id );
						if (empty($this->mProfile->district_name)) {
							$this->mProfile->district_id = null;
						} else {
							if (strlen($this->mProfile->district_name) > 100) {
								$this->mProfile->district_name = substr($this->mProfile->district_name, 0, 100);
							}
						}
						$tobeSave = true;
					} catch ( Exception $e ) {
						Log::info("MasterAgentProfileBL::getProfile master_agent_id=".$this->mMasterAgent->id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				} else if (is_null($this->mProfile->district_id) && ! empty($this->mProfile->district_name)) {
					try {
						$geoDistrict = new GeoDistrict( $this->mProfile->city_id, $this->mProfile->province_id );
						$this->mProfile->district_id = $geoDistrict->getDistrictID( $this->mProfile->district_name );
						$tobeSave = true;
					} catch ( Exception $e ) {
						Log::info("MasterAgentProfileBL::getProfile master_agent_id=".$this->mMasterAgent->id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				}
				// village
				if (! empty($this->mProfile->village_id) && empty($this->mProfile->village_name)) {
					try {
						$geoVillage = new GeoVillage( $this->mProfile->district_id, $this->mProfile->city_id, $this->mProfile->province_id );
						$this->mProfile->village_name = $geoVillage->getVillageName( $this->mProfile->village_id );
						if (empty($this->mProfile->village_name)) {
							$this->mProfile->village_id = null;
						} else {
							if (strlen($this->mProfile->village_name) > 100) {
								$this->mProfile->village_name = substr($this->mProfile->village_name, 0, 100);
							}
						}
						$tobeSave = true;
					} catch ( Exception $e ) {
						Log::info("MasterAgentProfileBL::getProfile master_agent_id=".$this->mMasterAgent->id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				} else if (is_null($this->mProfile->village_id) && ! empty($this->mProfile->village_name)) {
					try {
						$geoVillage = new GeoVillage( $this->mProfile->district_id, $this->mProfile->city_id, $this->mProfile->province_id );
						$this->mProfile->village_id = $geoVillage->getVillageID( $this->mProfile->village_name );
						$tobeSave = true;
					} catch ( Exception $e ) {
						Log::info("MasterAgentProfileBL::getProfile master_agent_id=".$this->mMasterAgent->id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				}
				// fallback propinsi by found city
				if (empty($this->mProfile->province_id) && ! empty($this->mProfile->city_id)) {
					try {
						$geoCity = new GeoCity();
						$mRow = $geoCity->getCityModel( $this->mProfile->city_id );
						if (! is_null($mRow)) {
							$this->mProfile->province_id = $mRow->province_id;
							$tobeSave = true;
						}
					} catch ( Exception $e ) {
						Log::info("MasterAgentProfileBL::getProfile master_agent_id=".$this->mMasterAgent->id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				}
				// fallback city by found district
				if (empty($this->mProfile->city_id) && ! empty($this->mProfile->district_id)) {
					try {
						$geoDistrict = new GeoDistrict();
						$mRow = $geoDistrict->getDistrictModel( $this->mProfile->district_id );
						if (! is_null($mRow)) {
							$this->mProfile->city_id = $mRow->city_id;
							$tobeSave = true;
						}
					} catch ( Exception $e ) {
						Log::info("MasterAgentProfileBL::getProfile master_agent_id=".$this->mMasterAgent->id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				}
				// fallback district by found village
				if (empty($this->mProfile->district_id) && ! empty($this->mProfile->village_id)) {
					try {
						$geoVillage = new GeoVillage();
						$mRow = $geoVillage->getVillageModel( $this->mProfile->village_id );
						if (! is_null($mRow)) {
							$this->mProfile->district_id = $mRow->district_id;
							$tobeSave = true;
						}
					} catch ( Exception $e ) {
						Log::info("MasterAgentProfileBL::getProfile master_agent_id=".$this->mMasterAgent->id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				}
				if ($tobeSave) {
					$this->mProfile->save();
				}
			} else {
				$this->mProfile = new MasterAgentProfile();
				$this->mProfile->master_agent_id = $this->mMasterAgent->id;
				$this->mProfile->fullname = $this->mMasterAgent->username; // default fullname sama dengan username
				$this->mProfile->profile_type = MasterAgent::PROFILE_TYPE_PERSONAL;
				$this->mProfile->save();
				try {
					$id = $this->mProfile->id;
					$this->mProfile = MasterAgentProfile::where( 'id', '=', $id )->firstOrFail();
				} catch ( Exception $e ) {
					Log::info("MasterAgentProfileBL::getProfile master_agent_id=".$this->mMasterAgent->id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentProfileBL::getProfileModel exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $this->mProfile;
	}
	
	public function getProfileArray() {
		$data = array();
		try {
			$this->getProfileModel();
			if (! is_null($this->mProfile)) {
				$data = $this->mProfile->toArray();
				if (isset($data['photo_filename']) && ! empty($data['photo_filename']))
				{
					$this->initModelMasterAgentWhiteLabel();
					$pathURL = $this->getURLPathResourceFile();
					if (! empty($pathURL)) {
						if (isset($data['photo_filename']) && ! empty($data['photo_filename'])) {
							$data['photo_filename'] = $pathURL . $data['photo_filename'];
						}
						if (isset($data['id_card_filename']) && ! empty($data['id_card_filename'])) {
							$data['id_card_filename'] = $pathURL . $data['id_card_filename'];
						}
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentProfileBL::getProfileArray exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $data;
	}
	
	public function getArrayAliasCode() {
		$arr = array();
		try {
			$maID = 0;
			if (! is_null($this->mMasterAgent)) {
				$maID = $this->mMasterAgent->id;
			}
			if (empty($maID) && ! is_null($this->mProfile)) {
				$maID = $this->mProfile->master_agent_id;
			} 
			if (! empty($maID)) {
				$mList = MasterAgentAlias::where('master_agent_id','=',$maID)->where('active','=','Y')->select('alias_code')->orderBy('priority','ASC')->orderBy('id','ASC')->get();
				foreach ($mList as $row) {
					$arr[] = strtoupper(trim($row->alias_code));
				}
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentProfileBL::getArrayAliasCode exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $arr;
	}
	
	public function setEmailVerified( $by = "MASTER_AGENT" ) {
		try {
			$this->getProfileModel();
			if (! empty($this->mProfile->email) ) {
				$this->mProfile->email_verified_at = date("Y-m-d H:i:s");
				$this->mProfile->email_verified_by = (strlen($by) <= 100 ? $by : substr($by, 0, 100));
				$this->mProfile->save();
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentProfileBL::setEmailVerified exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $this;
	}
	
	public function setPhoneVerified( $by = "MASTER_AGENT" ) {
		try {
			$this->getProfileModel();
			if (! empty($this->mProfile->phone_number) ) {
				$this->mProfile->phone_number_verified_at = date("Y-m-d H:i:s");
				$this->mProfile->phone_number_verified_by = (strlen($by) <= 100 ? $by : substr($by, 0, 100));
				$this->mProfile->save();
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentProfileBL::setPhoneVerified exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $this;
	}
	
	public function setPhoto( $filename, &$oldfile ) {
		try {
			$this->getProfileModel();
			$oldfile = $this->mProfile->photo_filename;
			$this->mProfile->photo_filename = (strlen($filename) <= 200 ? $filename : substr($filename, 0, 200));
			$this->mProfile->save();
		} catch ( Exception $e ) {
			Log::info("MasterAgentProfileBL::setPhoto exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $this;
	}
	
	public function setIDCard( $filename, &$oldfile, $originalName, $by = "AGENT" ) {
		try {
			$this->getProfileModel();
			$oldfile = $this->mProfile->id_card_filename;
			$this->mProfile->id_card_filename = (strlen($filename) <= 200 ? $filename : substr($filename, 0, 200));
			$this->mProfile->id_card_originalname = (strlen($originalName) <= 200 ? $originalName : substr($originalName, 0, 200));
			$this->mProfile->id_card_uploaded_at = date("Y-m-d H:i:s");
			$this->mProfile->id_card_uploaded_by = (strlen($by) <= 50 ? $by : substr($by, 0, 50));
			$this->mProfile->save();
		} catch ( Exception $e ) {
			Log::info("MasterAgentProfileBL::setIDCard exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $this;
	}
	
	public function editProfile( $input ) {
		$errcode = ApiError::ERROR_GENERAL;
		try {
			if (is_null($input) || ! is_array($input)) {
				return $errcode;
			}
			$sendEmailVerification = false;
			$sendPhoneVerification = false;
			$sendContactVerification = false;
			$sendFaxVerification = false;
			foreach ($input as $key => $value) {
				$input[$key] = (! is_null($value) ? trim($value) : "");
			}
			$this->getProfileModel();
			if (isset($input['profile_type'])) {
				if (! empty($input['profile_type'])) {
					$input['profile_type'] = strtoupper($input['profile_type']);
					// enum('PERSONAL','CORPORATE')
					if ($input['profile_type'] == MasterAgentProfile::PROFILE_TYPE_PERSONAL || $input['profile_type'] == MasterAgentProfile::PROFILE_TYPE_CORPORATE) {
						$this->mProfile->profile_type = $input['profile_type'];
					} else {
						return ApiError::ERROR_PROFILE_TYPE_INVALID;
					}
				}
			}
			if (isset($input['fullname'])) {
				$this->mProfile->fullname = (! empty($input['fullname']) ? (strlen($input['fullname']) <= 200 ? $input['fullname'] : substr($input['fullname'], 0, 200)) : $this->mProfile->fullname);
			}
			if ($this->mProfile->profile_type == MasterAgentProfile::PROFILE_TYPE_PERSONAL) {
				if (isset($input['firstname'])) {
					$this->mProfile->firstname = (! empty($input['firstname']) ? (strlen($input['firstname']) <= 100 ? $input['firstname'] : substr($input['firstname'], 0, 100)) : $this->mProfile->firstname);
				}
				if (isset($input['middlename'])) {
					$this->mProfile->middlename = (! empty($input['middlename']) ? (strlen($input['middlename']) <= 100 ? $input['middlename'] : substr($input['middlename'], 0, 100)) : $this->mProfile->middlename);
				}
				if (isset($input['lastname'])) {
					$this->mProfile->lastname = (! empty($input['lastname']) ? (strlen($input['lastname']) <= 100 ? $input['lastname'] : substr($input['lastname'], 0, 100)) : $this->mProfile->lastname);
				}
			}
			if (isset($input['description'])) {
				$this->mProfile->description = (! empty($input['description']) ? (strlen($input['description']) <= 255 ? $input['description'] : substr($input['description'], 0, 255)) : $this->mProfile->description);
			}
			if (isset($input['email'])) {
				if (! empty($input['email'])) {
					$input['email'] = strtolower($input['email']);
					if (strlen($input['email']) <= 50 && UtilEmail::isValidEmail($input['email'])) {
						if (empty($this->mProfile->email) || (! empty($this->mProfile->email) && strtolower($this->mProfile->email) != $input['email'])) {
							$this->mProfile->email = $input['email'];
							$this->mProfile->email_verified_at = null;
							$this->mProfile->email_verified_by = null;
							$sendEmailVerification = true;
						}
					} else {
						return ApiError::ERROR_FORMAT_EMAIL_INVALID;
					}
				}
			}
			if (isset($input['phone_number'])) {
				if (! empty($input['phone_number'])) {
					if (UtilPhone::isPhoneValid($input['phone_number'])) {
						$input['phone_number'] = UtilPhone::normalizeToMSISDN( $input['phone_number'] );
						if (empty($this->mProfile->phone_number) || (! empty($this->mProfile->phone_number) && $this->mProfile->phone_number != $input['phone_number'])) {
							$this->mProfile->phone_number = $input['phone_number'];
							$this->mProfile->phone_number_verified_at = null;
							$this->mProfile->phone_number_verified_by = null;
							$sendPhoneVerification = true;
						}
					} else {
						return ApiError::ERROR_FORMAT_PHONE_INVALID;
					}
				}
			}
			if (isset($input['contact_number'])) {
				if (! empty($input['contact_number'])) {
					if (UtilPhone::isPhoneValid($input['contact_number'])) {
						$input['contact_number'] = UtilPhone::normalizeToMSISDN( $input['contact_number'] );
						if (empty($this->mProfile->contact_number) || (! empty($this->mProfile->contact_number) && $this->mProfile->contact_number != $input['contact_number'])) {
							$this->mProfile->contact_number = $input['contact_number'];
							$this->mProfile->contact_number_verified_at = null;
							$this->mProfile->contact_number_verified_by = null;
							$sendContactVerification = true;
						}
					} else {
						return ApiError::ERROR_FORMAT_PHONE_INVALID;
					}
				}
			}
			if (isset($input['fax_number'])) {
				if (! empty($input['fax_number'])) {
					if (UtilPhone::isPhoneValid($input['fax_number'])) {
						$input['fax_number'] = UtilPhone::normalizeToMSISDN( $input['fax_number'] );
						if (empty($this->mProfile->fax_number) || (! empty($this->mProfile->fax_number) && $this->mProfile->fax_number != $input['fax_number'])) {
							$this->mProfile->fax_number = $input['fax_number'];
							$this->mProfile->fax_number_verified_at = null;
							$this->mProfile->fax_number_verified_by = null;
							$sendFaxVerification = true;
						}
					} else {
						return ApiError::ERROR_FORMAT_PHONE_INVALID;
					}
				}
			}
			if (isset($input['npwp_number'])) {
				$this->mProfile->npwp_number = (! empty($input['npwp_number']) ? (strlen($input['npwp_number']) <= 30 ? $input['npwp_number'] : substr($input['npwp_number'], 0, 30)) : $this->mProfile->npwp_number);
			}
			if ($this->mProfile->profile_type == MasterAgentProfile::PROFILE_TYPE_PERSONAL) {
				if (isset($input['place_birth'])) {
					$this->mProfile->place_birth = (! empty($input['place_birth']) ? (strlen($input['place_birth']) <= 50 ? $input['place_birth'] : substr($input['place_birth'], 0, 50)) : $this->mProfile->place_birth);
				}
				if (isset($input['birth_date'])) {
					if (! empty($input['birth_date'])) {
						if (UtilCommon::isDateValid($input['birth_date'])) {
							$this->mProfile->birth_date = $input['birth_date'];
						} else {
							return ApiError::ERROR_FORMAT_BIRTH_DATE_INVALID;
						}
					}
				}
				if (isset($input['gender'])) {
					if (! empty($input['gender'])) {
						$input['gender'] = strtoupper($input['gender']);
						$input['gender'] = ($input['gender'] == 'L' || $input['gender'] == 'LAKI-LAKI' ? 'M' : ($input['gender'] == 'P' || $input['gender'] == 'PEREMPUAN' || $input['gender'] == 'WANITA' ? 'F' : $input['gender']));
						if ($input['gender'] == 'M' || $input['gender'] == 'F') {
							$this->mProfile->gender = $input['gender'];
						} else {
							return ApiError::ERROR_GENDER_CODE_INVALID;
						}
					}
				}
			}
			$isSetProvinceID = false;
			if (isset($input['province_id'])) {
				$input['province_id'] = (int)$input['province_id'];
				if (! empty($input['province_id'])) {
					if (empty($this->mProfile->province_id) || $this->mProfile->province_id != $input['province_id']) {
						$this->mProfile->province_id = $input['province_id'];
						$this->mProfile->province_name = null;
						$isSetProvinceID = true;
					}
				}
			}
			if (isset($input['province_name']) && ! $isSetProvinceID) {
				if (! empty($input['province_name'])) {
					if (empty($this->mProfile->province_name) || $this->mProfile->province_name != $input['province_name']) {
						$this->mProfile->province_name = (strlen($input['province_name']) <= 100 ? $input['province_name'] : substr($input['province_name'], 0, 100));
						$this->mProfile->province_id = null;
					}
				}
			}
			$isSetCityID = false;
			if (isset($input['city_id'])) {
				$input['city_id'] = (int)$input['city_id'];
				if (! empty($input['city_id'])) {
					if (empty($this->mProfile->city_id) || $this->mProfile->city_id != $input['city_id']) {
						$this->mProfile->city_id = $input['city_id'];
						$this->mProfile->city_name = null;
						$isSetCityID = true;
					}
				}
			}
			if (isset($input['city_name']) && ! $isSetCityID) {
				if (! empty($input['city_name'])) {
					if (empty($this->mProfile->city_name) || $this->mProfile->city_name != $input['city_name']) {
						$this->mProfile->city_name = (strlen($input['city_name']) <= 100 ? $input['city_name'] : substr($input['city_name'], 0, 100));
						$this->mProfile->city_id = null;
					}
				}
			}
			$isSetDistrictID = false;
			if (isset($input['district_id'])) {
				$input['district_id'] = (int)$input['district_id'];
				if (! empty($input['district_id'])) {
					if (empty($this->mProfile->district_id) || $this->mProfile->district_id != $input['district_id']) {
						$this->mProfile->district_id = $input['district_id'];
						$this->mProfile->district_name = null;
						$isSetDistrictID = true;
					}
				}
			}
			if (isset($input['district_name']) && ! $isSetDistrictID) {
				if (! empty($input['district_name'])) {
					if (empty($this->mProfile->district_name) || $this->mProfile->district_name != $input['district_name']) {
						$this->mProfile->district_name = (strlen($input['district_name']) <= 100 ? $input['district_name'] : substr($input['district_name'], 0, 100));
						$this->mProfile->district_id = null;
					}
				}
			}
			$isSetVillageID = false;
			if (isset($input['village_id'])) {
				$input['village_id'] = (int)$input['village_id'];
				if (! empty($input['village_id'])) {
					if (empty($this->mProfile->village_id) || $this->mProfile->village_id != $input['village_id']) {
						$this->mProfile->village_id = $input['village_id'];
						$this->mProfile->village_name = null;
						$isSetVillageID = true;
					}
				}
			}
			if (isset($input['village_name']) && ! $isSetVillageID) {
				if (! empty($input['village_name'])) {
					if (empty($this->mProfile->village_name) || $this->mProfile->village_name != $input['village_name']) {
						$this->mProfile->village_name = (strlen($input['village_name']) <= 100 ? $input['village_name'] : substr($input['village_name'], 0, 100));
						$this->mProfile->village_id = null;
					}
				}
			}
			if (isset($input['address'])) {
				$this->mProfile->address = (! empty($input['address']) ? (strlen($input['address']) <= 200 ? $input['address'] : substr($input['address'], 0, 200)) : $this->mProfile->address);
			}
			if (isset($input['postal_code'])) {
				if (! empty($input['postal_code'])) {
					if (strlen($input['postal_code']) == 5 && UtilCommon::isDigit($input['postal_code'])) {
						$this->mProfile->postal_code = $input['postal_code'];
					} else {
						return ApiError::ERROR_FORMAT_POSTAL_CODE_INVALID;
					}
				}
			}
			if ($this->mProfile->profile_type == MasterAgentProfile::PROFILE_TYPE_CORPORATE) {
				if (isset($input['company_name'])) {
					$this->mProfile->company_name = (! empty($input['company_name']) ? (strlen($input['company_name']) <= 50 ? $input['company_name'] : substr($input['company_name'], 0, 50)) : $this->mProfile->company_name);
				}
				if (isset($input['department'])) {
					$this->mProfile->department = (! empty($input['department']) ? (strlen($input['department']) <= 50 ? $input['department'] : substr($input['department'], 0, 50)) : $this->mProfile->department);
				}
				if (isset($input['contact_person'])) {
					$this->mProfile->contact_person = (! empty($input['contact_person']) ? (strlen($input['contact_person']) <= 50 ? $input['contact_person'] : substr($input['contact_person'], 0, 50)) : $this->mProfile->contact_person);
				}
				if (isset($input['branch_office_level'])) {
					$this->mProfile->branch_office_level = (! empty($input['branch_office_level']) ? (strlen($input['branch_office_level']) <= 30 ? $input['branch_office_level'] : substr($input['branch_office_level'], 0, 30)) : $this->mProfile->branch_office_level);
				}
			}
			if (isset($input['id_card_type'])) {
				if (! empty($input['id_card_type'])) {
					$input['id_card_type'] = strtoupper($input['id_card_type']);
					// enum('KTP','SIM','PASSPORT','OTHER')
					if ($input['id_card_type'] == 'KTP' || $input['id_card_type'] == 'SIM' || $input['id_card_type'] == 'PASSPORT' || $input['id_card_type'] == 'OTHER') {
						$this->mProfile->id_card_type = $input['id_card_type'];
					} else {
						return ApiError::ERROR_IDCARD_TYPE_INVALID;
					}
				}
			}
			if (isset($input['id_card_number'])) {
				$this->mProfile->id_card_number = (! empty($input['id_card_number']) ? (strlen($input['id_card_number']) <= 30 ? $input['id_card_number'] : substr($input['id_card_number'], 0, 30)) : $this->mProfile->id_card_number);
			}
			$this->mProfile->save();
			$this->mProfile = null;
			$this->getProfileModel();
			$errcode = ApiError::SUCCESS;
			BankBL::updateProfileBankAccount( "MASTER_AGENT", $this->mProfile->master_agent_id, $this->mProfile->fullname, $this->mProfile->email );
			if ($sendPhoneVerification) {
				// BELUM
			}
			if ($sendContactVerification) {
				// BELUM
			}
			if ($sendFaxVerification) {
				// BELUM
			}
			if ($sendEmailVerification) {
				// BELUM
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentProfileBL::editProfile exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $errcode;
	}
	
	public static function searchEmail( $whitelabelID, $email ) {
		$mList = new Collection();
		try {
			$mList = MasterAgentProfile::SearchByEmail( $whitelabelID, $email )->get();
		} catch ( Exception $e ) {
			Log::info("MasterAgentProfileBL::searchEmail white_label_id=".$whitelabelID." email=".$email." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $mList;
	}
	
	public static function searchPhone( $whitelabelID, $phone ) {
		$mList = new Collection();
		try {
			$phone = UtilPhone::normalizeToMSISDN( $phone );
			$mList = MasterAgentProfile::SearchByPhone( $whitelabelID, $phone )->get();
		} catch ( Exception $e ) {
			Log::info("MasterAgentProfileBL::searchPhone white_label_id=".$whitelabelID." phone=".$phone." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $mList;
	}
	
}