<?php
namespace App\BusinessLogic\MasterAgent;

use Log;
use Config;
use Exception;
use DB;
use Illuminate\Database\Eloquent\Collection;
use App\ApiLogic\ApiDefinition;
use App\BusinessLogic\MasterAgent\MasterAgentBL;
use App\Core\Util\UtilCommon;
use App\Models\Database\MasterAgent;

class MasterAgentUplineBL extends MasterAgentBL
{
	public function __construct( $whitelabelID, $masterAgentID = 0, $apiversion = 1 ) {
		parent::__construct( $whitelabelID, $masterAgentID, $apiversion );
	}
	
	private function getParentToList( Collection &$mList, $parentID, $max = 100, $active = true, $useProfile = true ) {
		try {
			$mMA = null;
			if ($useProfile) {
				if ($this->debug) {
					$sql = MasterAgent::GetWithProfile( $parentID, $active )->toSql();
					$bindings = MasterAgent::GetWithProfile( $parentID, $active )->getBindings();
					$sql = UtilCommon::queryReplace($sql, $bindings);
					Log::info("MasterAgentUplineBL::getParentToList SQL = ". $sql);
				}
				$mMA = MasterAgent::GetWithProfile( $parentID, $active )->firstOrFail();
			} else {
				$mMA = MasterAgent::where( 'id', '=', $parentID )->firstOrFail();
			}
			if (! is_null($mMA) && ! empty($mMA->id)) {
				if ($mMA->white_label_id == $this->whitelabelID && ((! $active) || ($active && $mMA->status == MasterAgent::STATUS_ACTIVE))) {
					$mList->add( $mMA );
					if (! empty($mMA->master_agent_parent_id) && count($mList) < $max) {
						$this->getParentToList( $mList, $mMA->master_agent_parent_id, $max, $active, $useProfile );
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentUplineBL::getParentToList parent_id = ".$parentID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
	public function getListMasterAgentUpline( $max = 100, $active = true, $useProfile = true ) {
		$mList = new Collection();
		try {
			if (! $this->initModelMasterAgentWhiteLabel()) {
				return $mList;
			}
			if (empty($this->mMasterAgent->master_agent_parent_id)) {
				return $mList;
			}
			$this->whitelabelID = $this->mMasterAgent->white_label_id;
			if (empty($this->whitelabelID) && ! is_null($this->mWL)) {
				$this->whitelabelID = $this->mWL->id;
			}
			$this->getParentToList( $mList, $this->mMasterAgent->master_agent_parent_id, $max, $active, $useProfile );
		} catch ( Exception $e ) {
			Log::info("MasterAgentUplineBL::getListMasterAgentUpline exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $mList;
	}
	
}