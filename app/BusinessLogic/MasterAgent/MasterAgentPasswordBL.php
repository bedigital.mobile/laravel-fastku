<?php
namespace App\BusinessLogic\MasterAgent;

use Log;
use Config;
use Exception;
use DB;
use App\ApiLogic\ApiDefinition;
use App\BusinessLogic\MasterAgent\MasterAgentBL;
use App\SystemLogic\Auth\PasswordMasterAgent;
use App\SystemLogic\Auth\SessionMasterAgent;
use App\SystemLogic\Tools\WebShortenURL;
use App\SystemLogic\SMS\SMSSenderManager;
use App\Core\CacheData\Simple\CacheMasterAgentSendSMSForgotPassword;
use App\Core\CacheData\Simple\CacheMasterAgentSendSMSForgotPasswordCounter;
use App\Core\Util\UtilPhone;
use App\Core\Util\UtilCommon;
use App\Core\Util\UtilEmail;
use App\Jobs\Email\SendEmailMasterAgentForgotPasswordJob;
use App\Jobs\SMS\SendSMSMasterAgentForgotPasswordJob;
use App\Models\Api\ApiError;
use App\Models\Database\MasterAgent;
use App\Models\Database\MasterAgentProfile;
use App\Models\Database\MasterAgentEmailForgotPassword;
use App\Models\Database\WhiteLabel;

class MasterAgentPasswordBL extends MasterAgentBL
{
	private $mCheckLink = null;

	public function __construct( $whitelabelID, $masterAgentID = 0, $apiversion = 1 ) {
		parent::__construct( $whitelabelID, $masterAgentID, $apiversion );
	}

	public function changePassword( $pwd ) {
		try {
			if (is_null($this->mMasterAgent)) {
				return false;
			}
			$objLogicPwd = new PasswordMasterAgent( $this->whitelabelID );
			$this->mMasterAgent = $objLogicPwd->setPasswordByModel( $this->mMasterAgent, $pwd );
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentPasswordBL::changePassword exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}

	public function getEmail( &$fullname ) {
		try {
			$fullname = "";
			if (! is_null($this->mMasterAgent)) {
				$email = "";
				$mProfile = MasterAgentProfile::where( 'master_agent_id', '=', $this->mMasterAgent->id )->select('id','fullname','email','email_verified_at','email_verified_by')->firstOrFail();
				if (! is_null($mProfile) && ! empty($mProfile->id)) {
					$fullname = $mProfile->fullname;
					$email = $mProfile->email;
					if (! empty($email) && ! UtilEmail::isValidEmail($email)) {
						$email = "";
					}
				}
				return $email;
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBL::getEmail exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return "";
	}
	
	public function putQueueSendEmailForgotPassword() {
		try {
			$queuename = "fintechapi_email";
			$job = new SendEmailMasterAgentForgotPasswordJob( $this->mMasterAgent->id, $this->apiversion );
			if (! empty($queuename)) {
				$job->onQueue($queuename);
			}
			dispatch( $job );
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentPasswordBL::putQueueSendEmailForgotPassword exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function sendEmailForgotPassword( $email, $fullname ) {
		try {
			if (is_null($this->mMasterAgent)) {
				return false;
			}
			$expiry_hours = 6; // SEMENTARA
			$url = $this->createURLEmailForgotPassword( $expiry_hours );
			if (! empty($url)) {
				$subject = "Master Agent Forgot Password (" . $this->mMasterAgent->username . ")";
				$nameBlade = "email.white_label_".$this->mMasterAgent->white_label_id.".password.master_agent_forgot_password";
				$paramBlade = array(
						'fullname' => $fullname,
						'username' => $this->mMasterAgent->username,
						'expiry_hours' => $expiry_hours,
						'url' => $url
				);
				$b = false;
				if ( empty($this->mWL->email_from_address) ) {
					$b = UtilEmail::sendEmailByTemplateView($nameBlade, $paramBlade, $subject, $email, $fullname);
				} else {
					$b = UtilEmail::sendEmailByTemplateView($nameBlade, $paramBlade, $subject, $email, $fullname, $this->mWL->email_from_address, $this->mWL->email_from_fullname);
				}
				return $b;
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentPasswordBL::sendEmailForgotPassword exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function putQueueSendSMSForgotPassword( $telpNumber ) {
		try {
			$queuename = "fintechapi_email";
			$job = new SendSMSMasterAgentForgotPasswordJob( $this->mMasterAgent->id, $telpNumber, $this->apiversion );
			if (! empty($queuename)) {
				$job->onQueue($queuename);
			}
			dispatch( $job );
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentPasswordBL::putQueueSendSMSForgotPassword exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function sendSMSForgotPassword( $telpNumber ) {
		try {
			$expiry_hours = 6; // SEMENTARA
			$url = $this->createURLEmailForgotPassword( $expiry_hours );
			if (! empty($url)) {
				$web = new WebShortenURL();
				$shortUrl = $web->createShortURL($url);
				if (! empty($shortUrl)) {
					$msg = "Gunakan link ".$shortUrl." untuk forgot password username ".$this->mMasterAgent->username." sebagai koordinator aplikasi ".$this->mWL->product_name.". Link ini berlaku ".$expiry_hours." jam";
					$smsSender = new SMSSenderManager( $this->mMasterAgent->white_label_id );
					$smsSender->setPurposeType( "SMSFORGOTPWD" );
					$b = $smsSender->send( $telpNumber, $msg );
					if ($b) {
						$cache = new CacheMasterAgentSendSMSForgotPassword();
						$cache->setFlagRecentlySMSSent( $this->mMasterAgent->id );
						$cache = new CacheMasterAgentSendSMSForgotPasswordCounter();
						$cache->increment( $this->mMasterAgent->id );
					}
					return $b;
				}
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentPasswordBL::sendSMSForgotPassword exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	private function createURLEmailForgotPassword( $expiryhours = 6 ) {
		$url = "";
		try {
			$guid = UtilCommon::GUID( 50 );
			$dateExpiry = null;
			if ($expiryhours > 0) {
				$expirytime = time() + ($expiryhours * 3600);
				$dateExpiry = date( 'Y-m-d H:i:s', $expirytime );
			}
			$mData = new MasterAgentEmailForgotPassword();
			$mData->guid = $guid;
			$mData->guid_md5 = md5($guid);
			$mData->master_agent_id = (! is_null($this->mMasterAgent) ? $this->mMasterAgent->id : 0);
			$mData->expiry_date = $dateExpiry;
			$mData->executed = 'N';
			$mData->save();
			$id = $mData->id;
			if (! empty($id)) {
				if (is_null($this->mWL)) {
					$this->setWhiteLabelID( $this->mMasterAgent->white_label_id, true );
				}
				$url = $this->getURLPathFintech() . "forgot-password-by-change-password/". $id . "/" . $guid;
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentPasswordBL::createURLEmailForgotPassword exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $url;
	}
	
	public function verifyLinkEmailForgotPassword( &$errMsg, $id, $guid) {
		$b = false;
		$this->mCheckLink = null;
		try {
			if (empty($id)) {
				$this->mCheckLink = MasterAgentEmailForgotPassword::where( 'guid_md5', '=', md5($guid) )->firstorFail();
			} else {
				$this->mCheckLink = MasterAgentEmailForgotPassword::where( 'id', '=', $id )->firstorFail();
			}
			if (! is_null($this->mCheckLink) && ! empty($this->mCheckLink->id)) {
				if ($this->mCheckLink->guid != $guid) {
					$this->mCheckLink = null;
				}
			} else {
				$this->mCheckLink = null;
			}
		} catch ( Exception $e ) {
			$this->mCheckLink = null;
			Log::info("MasterAgentPasswordBL::verifyLinkEmailForgotPassword id=".$id." guid=".$guid." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		try {
			if (! is_null($this->mCheckLink)) {
				if ($this->mCheckLink->executed == 'N') {
					if (empty($this->mCheckLink->expiry_date) || (date('Y-m-d H:i:s') <= $this->mCheckLink->expiry_date)) {
						$this->setMasterAgent( $this->mCheckLink->master_agent_id );
						$mMasterAgent = $this->getMasterAgentModel();
						if (! is_null($mMasterAgent) && ! empty($mMasterAgent->id)) {
							$errMsg = "";
							$b = true;
						} else {
							$errMsg = "Link untuk forgot password ini tidak valid";
						}
					} else {
						$errMsg = "Link ini sudah kadaluarsa";
					}
				} else {
					$errMsg = "Link ini sudah pernah digunakan untuk forgot password Anda";
				}
			} else {
				$errMsg = "Link untuk forgot password ini tidak valid";
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentPasswordBL::verifyLinkEmailForgotPassword id=".$id." guid=".$guid." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
	public function getCheckLinkModel() {
		return $this->mCheckLink;
	}
	
}