<?php
namespace App\BusinessLogic\MasterAgent;

use Log;
use Config;
use Exception;
use App\Core\Util\UtilCommon;
use App\Models\Api\ApiError;
use App\Models\Database\HistoryTrxMasterAgent;
use App\Models\Database\HistoryTrxAgent;
use App\Models\Database\Agent;
use App\Models\Database\AgentProfile;
use App\Models\Database\MasterAgent;

class MasterAgentTrxHistoryBL 
{
	private $whitelabelID = 0;
	private $masterAgentID = 0;
	private $version = "1.0";

	public function __construct( $whitelabelID = 0, $masterAgentID = 0, $version = "1.0" ) {
		$this->whitelabelID = $whitelabelID;
		$this->masterAgentID = $masterAgentID;
		$this->version = $version;
	}

	public function getTrxHistory( $searchFilter = array(), $page = 1, $max = 100 ) {
		
		Log::info("MasterAgentTrxHistoryBL->getTrxHistory: master_agent_id=".$this->masterAgentID);
		Log::info("MasterAgentTrxHistoryBL->getTrxHistory: search_filter=".json_encode($searchFilter));

		$sqlFilterDate = '';

		if ( array_key_exists(HistoryTrxMasterAgent::DATE_BEGIN, $searchFilter) ) {
			if ($searchFilter[HistoryTrxMasterAgent::DATE_BEGIN] != '') {
				//Date begin is exist
				if ( array_key_exists(HistoryTrxMasterAgent::DATE_END, $searchFilter) ) {
					if ($searchFilter[HistoryTrxMasterAgent::DATE_END] != '') {
						//Date end is exist
						$sqlFilterDate = $sqlFilterDate.HistoryTrxMasterAgent::FIELD_TRX_DATETIME.
							" between '".$searchFilter[HistoryTrxMasterAgent::DATE_BEGIN]."' and '".$searchFilter[HistoryTrxMasterAgent::DATE_END]."'"; 
					} else {
						//Date end is not exist. Single date
						$sqlFilterDate = $sqlFilterDate.HistoryTrxMasterAgent::FIELD_TRX_DATETIME." = '".
							$searchFilter[HistoryTrxMasterAgent::DATE_BEGIN]."'";
					}
				}	
			} else {
				//Date begin is not exist
				if ( array_key_exists(HistoryTrxMasterAgent::DATE_END, $searchFilter) ) {
					if ($searchFilter[HistoryTrxMasterAgent::DATE_END] != '') {
						//Date end is exist
						$sqlFilterDate = $sqlFilterDate.HistoryTrxMasterAgent::FIELD_TRX_DATETIME." = '".$searchFilter[HistoryTrxMasterAgent::DATE_END]."'";
					}	
				}
			}
		}

		$sqlFilterProduct = '';
		if ( ( array_key_exists('product_id', $searchFilter) ) && ( ! empty($searchFilter['product_id']) ) ) {
			$sqlFilterProduct = 'trx_product_id = '.$searchFilter['product_id'];
		}
		if ( ( array_key_exists('product_code', $searchFilter) ) && ( ! empty($searchFilter['product_code']) ) ) {
			if (empty($sqlFilterProduct)) {
				$sqlFilterProduct = 'trx_product_code = '.$searchFilter['product_code'];	
			} else {
				$sqlFilterProduct = $sqlFilterProduct.' and trx_product_code = '.$searchFilter['product_code'];
			}			
		}

		$sqlFilterBillNumber = '';
		if ( ( array_key_exists('bill_number', $searchFilter) ) && ( ! empty($searchFilter['bill_number']) ) ) {
			$sqlFilterBillNumber = 'trx_bill_number ='.$searchFilter['bill_number'];
		}	

		$sqlFilterCustomerPhone = '';
		if ( ( array_key_exists('customer_phone_number', $searchFilter) ) && ( ! empty($searchFilter['customer_phone_number']) ) ) {
			$sqlFilterCustomerPhone = 'customer_phone_number like \'%'.$searchFilter['customer_phone_number'].'%\'';
		}

		$sqlFilterCustomerName = '';
		if ( ( array_key_exists('customer_name', $searchFilter) ) && ( ! empty($searchFilter['customer_name']) ) ) {
			$sqlFilterCustomerName = 'customer_name like \'%'.$searchFilter['customer_name'].'%\'';
		}

		$sqlFilterTrxID = '';
		if ( ( array_key_exists('trx_id', $searchFilter) ) && ( ! empty($searchFilter['trx_id']) ) ) {
			$sqlFilterTrxID = 'trx_code like \'%'.$searchFilter['trx_id'].'%\'';
		}	

		$sqlFilterCustomer = '1=1';
		if (!empty($sqlFilterCustomerName)) {
			$sqlFilterCustomer = $sqlFilterCustomer.' or '.$sqlFilterCustomerName; 
		}	
		if (!empty($sqlFilterCustomerPhone)) {
			$sqlFilterCustomer = $sqlFilterCustomer.' or '.$sqlFilterCustomerPhone; 
		}	


		//Join all filter 
		$sqlFilterAll = '1=1';
		if (!empty($sqlFilterTrxID)) {
			$sqlFilterAll = $sqlFilterAll.' and '.$sqlFilterTrxID; 
		}
		if (!empty($sqlFilterProduct)) {
			$sqlFilterAll = $sqlFilterAll.' and '.$sqlFilterProduct; 
		}
		if (!empty($sqlFilterBillNumber)) {
			$sqlFilterAll = $sqlFilterAll.' and '.$sqlFilterBillNumber; 
		}
		if (!empty($sqlFilterCustomer)) {
			$sqlFilterAll = $sqlFilterAll.' and ('.$sqlFilterCustomer.')'; 
		}
		if (!empty($sqlFilterDate)) {
			$sqlFilterAll = $sqlFilterAll.' and ('.$sqlFilterDate.')'; 
		}

		Log::info('MasterAgentTrxHistoryBL->getTrxHistory : sql_filter='.$sqlFilterAll);

		if (!empty($sqlFilterAll)) {
			return $rsTrxHistory = HistoryTrxMasterAgent::search($this->masterAgentID, $searchFilter, $page, $max)
				->whereRaw($sqlFilterAll)
				->get();
		} else {
			return $rsTrxHistory = HistoryTrxMasterAgent::search($this->masterAgentID, $searchFilter, $page, $max)
				->get();
		}
	}

} // End of class
