<?php
namespace App\BusinessLogic\MasterAgent;

use Log;
use Config;
use Exception;
use Excel;
use DB;
use App\ApiLogic\ApiDefinition;
use App\ApiLogic\MasterAgent\ApiMasterAgentDownlineCommission;
use App\BusinessLogic\MasterAgent\MasterAgentBL;
use App\BusinessLogic\MasterAgent\MasterAgentProfileBL;
use App\BusinessLogic\Commission\CommissionBL;
use App\BusinessLogic\Report\Excel\ExcelMasterAgentCommissionRealizeResult;
use App\Jobs\Batch\MasterAgentCommissionRealizeBatchJob;
use App\Core\CacheData\Simple\CacheMasterAgentTrxBatchStatus;
use App\Core\Util\UtilCommon;
use App\Core\Util\UtilEmail;
use App\Models\Api\ApiError;
use App\Models\Api\ApiResponse;
use App\Models\Database\MasterAgent;
use App\Models\Database\MasterAgentProfile;
use App\Models\Database\BatchRealizeCommission;
use App\Models\Database\BatchRealizeCommissionBreakup;
use App\Models\Database\LogRealizeCommissionMasterAgent;
use App\Models\Database\ReportCommissionMasterAgentMonthly;

class MasterAgentBatchCommissionBL extends MasterAgentBL
{
	protected $mBatch = null;
	protected $mBreakup = null;
	protected $errcode = ApiError::ERROR_GENERAL;
	protected $errmsg = "";
	protected $logID = 0;
	
	public function __construct( $whitelabelID, $masterAgentID = 0, $apiversion = 1 ) {
		parent::__construct( $whitelabelID, $masterAgentID, $apiversion );
	}
	
	public function setDataBatchJob( $id ) {
		$this->mBreakup = null;
		try {
			$this->mBatch = BatchRealizeCommission::where('id','=',$id)->firstOrFail();
			if (! is_null($this->mBatch) && ! empty($this->mBatch->id)) {
				if ($this->mBatch->realize_for == "MASTER_AGENT") {
					if (empty($this->mBatch->datetime_start)) {
						$this->mBatch->datetime_start = date('Y-m-d H:i:s');
						$this->mBatch->save();
					}
					$this->setWhiteLabelID( $this->mBatch->white_label_id );
				} else {
					$this->mBatch = null;
				}
			} else {
				$this->mBatch = null;
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchCommissionBL::setDataBatchJob id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			$this->mBatch = null;
		}
		if (! is_null($this->mBatch)) {
			try {
				$this->mBreakup = BatchRealizeCommissionBreakup::where('batch_realize_commission_id','=',$id)->whereNull('api_error_code')->orderBy('id','ASC')->firstOrFail();
				if (! is_null($this->mBreakup) && ! empty($this->mBreakup->id)) {
					// OK
				} else {
					$this->mBreakup = null;
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchCommissionBL::setDataBatchJob id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				$this->mBreakup = null;
			}
		}
		return $this;
	}
	
	public function getBreakupID() {
		return (! is_null($this->mBreakup) ? $this->mBreakup->id : 0);
	}
	
	public function isInvalidDataBatch() {
		return (is_null($this->mBatch) ? true : false);
	}
	
	public function isFoundDataBatchJob() {
		return (! is_null($this->mBatch) && ! is_null($this->mBreakup) ? true : false);
	}
	
	public function startBatch( $batchID ) {
		try {
			$cache = new CacheMasterAgentTrxBatchStatus();
			$cache->setProcessing( ! is_null($this->mBatch) && ! empty($this->mBatch->master_agent_id) ? $this->mBatch->master_agent_id : 0 );
			if ( ! empty($batchID) ) {
				return self::putQueueProcessJob( $batchID );
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchCommissionBL::startBatch exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public static function putQueueProcessJob( $id ) {
		try {
			$queuename = "fintechapi_commission";
			$job = new MasterAgentCommissionRealizeBatchJob( $id );
			if (! empty($queuename)) {
				$job->onQueue($queuename);
			}
			dispatch( $job );
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchCommissionBL::putQueueProcessJob id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function processJob() {
		$isSuccess = false;
		$this->errcode = ApiError::ERROR_GENERAL;
		$this->errmsg = "";
		$this->logID = 0;
		$this->mMasterAgent = null;
		try {
			$cache = new CacheMasterAgentTrxBatchStatus();
			$cache->setProcessing( ! empty($this->mBatch->master_agent_id) ? $this->mBatch->master_agent_id : 0 );
			$validData = false;
			try {
				$this->mMasterAgent = MasterAgent::where('id', '=', $this->mBreakup->master_agent_id)->firstOrFail();
				$this->whitelabelID = $this->mMasterAgent->white_label_id;
				$this->masterAgentID = $this->mMasterAgent->id;
				if ($this->whitelabelID == $this->mBatch->white_label_id) {
					$validData = true;
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchCommissionBL::processJob exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			if ($validData) {
				$input = array(
						'white_label_id' => $this->whitelabelID,
						'year' 			=> $this->mBatch->report_year,
						'month' 		=> $this->mBatch->report_month,
						'status_new' 	=> $this->mBreakup->realize_status,
						'status_info'	=> $this->mBreakup->additional_info,
						'deduction'		=> array(
								'name'	=> $this->mBreakup->realize_deduction_name,
								'type'	=> $this->mBreakup->realize_deduction_type,
								'value'	=> $this->mBreakup->realize_deduction_value
						)
				);
				$api = new ApiMasterAgentDownlineCommission( $input );
				$api->setWhiteLabelID( $this->whitelabelID )
					->setMasterAgentID( ! empty($this->mBatch->master_agent_id) ? $this->mBatch->master_agent_id : $this->mMasterAgent->master_agent_parent_id );
				if (! is_null($this->mWL)) {
					$api->setWhiteLabelModel( $this->mWL );
				} else {
					$this->setWhiteLabelID($this->whitelabelID, true);
					$api->setWhiteLabelModel( $this->mWL );
				}
				$resp = $api->doRealizeCommission( $this->mMasterAgent->id, false, $this->mBatch->realize_by );
				if (is_null($resp) || ! is_array($resp)) {
					$apiResp = new ApiResponse( $this->whitelabelID );
					$resp = $apiResp->getResponse();
				} 
				if ($this->debug) Log::info("MasterAgentBatchCommissionBL::processJob api_resp=".json_encode($resp));
				$this->errcode = $resp[ApiResponse::KEY_ERROR][ApiResponse::KEY_ERRORCODE];
				$this->errmsg = $resp[ApiResponse::KEY_ERROR][ApiResponse::KEY_ERRORMSG];
				if (! is_null($resp[ApiResponse::KEY_RESULTS]) && is_array($resp[ApiResponse::KEY_RESULTS])) {
					if (isset($resp[ApiResponse::KEY_RESULTS]['log_id'])) {
						$this->logID = $resp[ApiResponse::KEY_RESULTS]['log_id'];
					}
				}
			} else {
				$this->errcode = ApiError::ERROR_AGENT_INVALID;
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchCommissionBL::processJob exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		} finally {
			try {
				$this->updateBatchBreakup();
				$isSuccess = ($this->errcode == ApiError::SUCCESS ? true : false);
				$this->mBatch->total_data++;
				if ($isSuccess) {
					$this->mBatch->total_success++;
				} else {
					$this->mBatch->total_failed++;
				}
				$this->mBatch->save();
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchCommissionBL::processJob exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
		}
		return $isSuccess;
	}
	
	public function sendReport() {
		$isSuccess = false;
		$filepath = "";
		try {
			if (! empty($this->mBatch->emails)) {
				$filepath = $this->createFileDataExcelReport();
				if (! empty($filepath)) {
					if (! file_exists($filepath)) {
						$filepath = "";
					}
				}
				$isSuccess = $this->sendEmailReportWithAttachment( $this->mBatch->emails, $filepath );
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchCommissionBL::sendReport exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		} finally {
			try {
				if (empty($this->mBatch->datetime_stop)) {
					$this->mBatch->datetime_stop = date('Y-m-d H:i:s');
					$this->mBatch->save();
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchCommissionBL::sendReport exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				// hapus record breakup..
				$mTable = new BatchRealizeCommissionBreakup();
				DB::table( $mTable->getTable() )->where( 'batch_realize_commission_id', '=', $this->mBatch->id )->delete();
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchCommissionBL::sendReport exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				// hapus file attachment untuk report ke email
				if (! empty($filepath)) {
					unlink($filepath);
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchCommissionBL::sendReport exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				// batch status OFF lagi
				$cache = new CacheMasterAgentTrxBatchStatus();
				$cache->deleteStatus( ! is_null($this->mBatch) && ! empty($this->mBatch->master_agent_id) ? $this->mBatch->master_agent_id : 0 );
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchCommissionBL::sendReport exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
		}
		return $isSuccess;
	}
	
	private function updateBatchBreakup() {
		try {
			if (empty($this->errmsg)) {
				$this->errmsg = ApiError::getErrorString( $this->errcode );
			}
			$this->mBreakup->api_error_code = $this->errcode;
			$this->mBreakup->api_error_message = $this->errmsg;
			$this->mBreakup->last_status = ($this->errcode == ApiError::SUCCESS ? $this->mBreakup->realize_status : ReportCommissionMasterAgentMonthly::STATUS_REPORTED);
			$this->mBreakup->currency_code = 'IDR';
			$this->mBreakup->paymode = (! is_null($this->mMasterAgent) ? $this->mMasterAgent->paymode : null);
			$this->mBreakup->balance_type_id = 0;
			$this->mBreakup->balance_value_before = 0;
			$this->mBreakup->balance_value_after = 0;
			$this->mBreakup->total_commission_received = 0;
			if (! empty($this->logID)) {
				try {
					$mLog = LogRealizeCommissionMasterAgent::where('id','=',$this->logID)->firstOrFail();
					if (! is_null($mLog)) {
						$this->mBreakup->currency_code = $mLog->currency_code;
						$this->mBreakup->total_commission_received = $mLog->total_commission_received;
						$this->mBreakup->realize_commission_received = $mLog->realize_commission_received;
						$this->mBreakup->balance_type_id = $mLog->balance_type_id;
						$this->mBreakup->balance_value_before = $mLog->balance_value_before;
						$this->mBreakup->balance_value_after = $mLog->balance_value_after;
					}
				} catch ( Exception $e ) {
					Log::info("MasterAgentBatchCommissionBL::updateBatchBreakup exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			} else {
				try {
					$mData = ReportCommissionMasterAgentMonthly::where('report_year','=',$this->mBatch->report_year)
														 ->where('report_month','=',$this->mBatch->report_month)
														 ->where('master_agent_id','=',$this->mBreakup->master_agent_id)
														 ->firstOrFail();
					if (! is_null($mData)) {
						$this->mBreakup->currency_code = $mData->currency_code;
						$this->mBreakup->last_status = $mData->status;
						$this->mBreakup->total_commission_received = $mData->total_commission_received;
					}
				} catch ( Exception $e ) {
					Log::info("MasterAgentBatchCommissionBL::updateBatchBreakup exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
			$this->mBreakup->save();
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchCommissionBL::updateBatchBreakup exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		try {
			if (! empty($this->mBreakup->npwp_number)) {
				$mProfile = MasterAgentProfile::where('master_agent_id','=',$this->masterAgentID)->firstOrFail();
				if (! is_null($mProfile)) {
					if (empty($mProfile->npwp_number) || strtolower(trim($mProfile->npwp_number)) != strtolower(trim($this->mBreakup->npwp_number))) {
						$mProfile->npwp_number = $this->mBreakup->npwp_number;
						if ($this->mBreakup->npwp_verified == 'Y') {
							$mProfile->npwp_number_verified_at = date('Y-m-d H:i:s');
							$mProfile->npwp_number_verified_by = $this->mBatch->realize_by;
						} else {
							$mProfile->npwp_number_verified_at = null;
						}
						$mProfile->save();
					} else {
						if (empty($mProfile->npwp_number_verified_at) && $this->mBreakup->npwp_verified == 'Y') {
							$mProfile->npwp_number_verified_at = date('Y-m-d H:i:s');
							$mProfile->npwp_number_verified_by = $this->mBatch->realize_by;
							$mProfile->save();
						} else if (! empty($mProfile->npwp_number_verified_at) && $this->mBreakup->npwp_verified != 'Y') {
							$mProfile->npwp_number_verified_at = null;
							$mProfile->save();
						}
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchCommissionBL::updateBatchBreakup exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
	private function createFileDataExcelReport() {
		$filepathsave = "";
		try {
			if (is_null($this->mWL)) {
				$this->setWhiteLabelID( $this->mBatch->white_label_id, true);
			}
			$sheetname = "RealisasiKomisi_" . sprintf("%04d", $this->mBatch->report_year) . "_" . sprintf("%02d", $this->mBatch->report_month);
			$filename = "result_commission_master_agent_" . 
						UtilCommon::idConvertToBase36( ! empty($this->mBatch->master_agent_id) ? $this->mBatch->master_agent_id : $this->mBatch->white_label_id ) .
						"_" . sprintf("%04d", $this->mBatch->report_year) . "_" . sprintf("%02d", $this->mBatch->report_month) .
						"_" . UtilCommon::randomCode() . ".xlsx";
			Excel::store(new ExcelMasterAgentCommissionRealizeResult( $this->mBatch->id, $sheetname, $this->mWL->product_name ), $filename );
			$filepathsave = (string)storage_path('app/'.$filename);
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchCommissionBL::createFileDataExcelReport exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		} finally {
			try {
				if (! empty($filepathsave) && ! file_exists($filepathsave)) {
					$filepathsave = "";
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchCommissionBL::createFileDataExcelReport exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
		}
		return $filepathsave;
	}
	
	private function sendEmailReportWithAttachment( $emails, $fileReport ) {
		$b = false;
		try {
			$tgl = substr($this->mBatch->datetime_start, 0, 10);
			$hour = substr($this->mBatch->datetime_start, 11, 2);
			$subject = "Report Batch Realize Master Agent Commission of " . sprintf("%04d", $this->mBatch->report_year) . " - " . sprintf("%02d", $this->mBatch->report_month);
			$nameBlade = "email.white_label_".$this->whitelabelID.".batch.report_commission_realize";
			$paramBlade = array(
					'name' 				=> 'MASTER AGENT',
					'date' 				=> $tgl,
					'hour' 				=> $hour,
					'year'				=> sprintf("%04d", $this->mBatch->report_year),
					'month' 			=> sprintf("%02d", $this->mBatch->report_month),
					'original_filename' => $this->mBatch->original_filename,
					'total_data' 		=> $this->mBatch->total_data,
					'total_success' 	=> $this->mBatch->total_success,
					'total_failed' 		=> $this->mBatch->total_failed,
			);
			if (is_null($this->mWL)) {
				$this->setWhiteLabelID( $this->whitelabelID, true );
			}
			if ( empty($this->mWL->email_from_address) ) {
				$b = UtilEmail::sendEmailByTemplateView($nameBlade, $paramBlade, $subject, $emails, "", "", "", $fileReport);
			} else {
				$b = UtilEmail::sendEmailByTemplateView($nameBlade, $paramBlade, $subject, $emails, "", $this->mWL->email_from_address, $this->mWL->email_from_fullname, $fileReport);
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchCommissionBL::sendEmailReportWithAttachment exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
}