<?php
namespace App\BusinessLogic\MasterAgent;

use Log;
use Config;
use Exception;
use DB;
use Illuminate\Database\Eloquent\Collection;
use App\ApiLogic\ApiDefinition;
use App\BusinessLogic\MasterAgent\MasterAgentBL;
use App\BusinessLogic\MasterAgent\MasterAgentProfileBL;
use App\BusinessLogic\Agent\AgentProfileBL;
use App\BusinessLogic\Commission\CommissionBL;
use App\BusinessLogic\Constant\ConstantList;
use App\Jobs\Email\SendEmailRealizeCommissionMasterAgentJob;
use App\Jobs\Email\SendEmailRealizeBonusMasterAgentJob;
use App\Jobs\Email\SendEmailTransferBalanceJob;
use App\Jobs\Push\SendPushTextToAgentJob;
use App\Core\Util\UtilCommon;
use App\Core\Util\UtilEmail;
use App\Models\Database\MasterAgent;
use App\Models\Database\MasterAgentBalances;
use App\Models\Database\AgentBalances;
use App\Models\Database\Agent;
use App\Models\Database\BalanceType;
use App\Models\Database\WhitelabelBalanceTypes;
use App\Models\Database\ReportCommissionMasterAgentMonthly;
use App\Models\Database\ReportBonusMasterAgentMonthly;
use App\Models\Database\HistoryTrxMasterAgent;
use App\Models\Database\HistoryTrxAgent;
use App\Models\Database\LogTransferBalance;
use App\Models\Database\LogRealizeCommissionMasterAgent;
use App\Models\Database\LogRealizeBonusMasterAgent;

class MasterAgentBalanceBL extends MasterAgentBL
{
	private $logID = 0;
	
	public function __construct( $whitelabelID, $masterAgentID = 0, $apiversion = 1 ) {
		parent::__construct( $whitelabelID, $masterAgentID, $apiversion );
	}

	public function getLogID() {
		return $this->logID;
	}
	
	public function getListBalanceDeposit( $page = 1, $max = 100, $maxPlusOne = true, $active = true ) {
		$mList = new Collection();
		try {
			if (! $this->initModelMasterAgentWhiteLabel()) {
				return $mList;
			}
			$balcTypeID = 0;
			if ($this->mMasterAgent->paymode == BalanceType::PAYMODE_POSTPAID) {
				$balcTypeID = $this->mWL->default_postpaid_balance_type_id;
				if ($balcTypeID < BalanceType::TYPE_POSTPAID_MIN || $balcTypeID > BalanceType::TYPE_POSTPAID_MAX) {
					$balcTypeID = BalanceType::TYPE_POSTPAID_MIN;
				}
			} else if ($this->mMasterAgent->paymode == BalanceType::PAYMODE_PREPAID) {
				$balcTypeID = $this->mWL->default_prepaid_balance_type_id;
				if ($balcTypeID < BalanceType::TYPE_PREPAID_MIN || $balcTypeID > BalanceType::TYPE_PREPAID_MAX) {
					$balcTypeID = BalanceType::TYPE_PREPAID_MIN;
				}
			}
			$row = WhitelabelBalanceTypes::firstOrCreate([ 'white_label_id' => $this->whitelabelID,
														   'balance_type_id' => $balcTypeID
														 ] );
			$row = MasterAgentBalances::firstOrCreate([ 'master_agent_id' => $this->mMasterAgent->id,
														'balance_type_id' => $balcTypeID
													  ] );
			if ($this->debug) {
				try {
					$sql = MasterAgentBalances::ListByMasterAgent( $this->whitelabelID, $this->mMasterAgent->id, $this->mMasterAgent->paymode, $balcTypeID, 0, $page, $max, $maxPlusOne, $active )->toSql();
					$bindings = MasterAgentBalances::ListByMasterAgent( $this->whitelabelID, $this->mMasterAgent->id, $this->mMasterAgent->paymode, $balcTypeID, 0, $page, $max, $maxPlusOne, $active )->getBindings();
					$sql = UtilCommon::queryReplace($sql, $bindings);
					Log::info("MasterAgentBalanceBL::getListBalanceDeposit SQL = ". $sql);
				} catch ( Exception $e ) {
					Log::info("MasterAgentBalanceBL::getListBalanceDeposit exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
			$mList = MasterAgentBalances::ListByMasterAgent( $this->whitelabelID, $this->mMasterAgent->id, $this->mMasterAgent->paymode, $balcTypeID, 0, $page, $max, $maxPlusOne, $active )->get();
		} catch ( Exception $e ) {
			Log::info("MasterAgentBalanceBL::getListBalanceDeposit exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $mList;
	}
	
	public function getListBalanceWallet( $page = 1, $max = 100, $maxPlusOne = true, $active = true ) {
		$mList = new Collection();
		try {
			if (! $this->initModelMasterAgentWhiteLabel()) {
				return $mList;
			}
			$balcTypeID = $this->mWL->default_wallet_balance_type_id;
			if ($balcTypeID < BalanceType::TYPE_WALLET_MIN || $balcTypeID > BalanceType::TYPE_WALLET_MAX) {
				$balcTypeID = BalanceType::TYPE_WALLET_MIN;
			}
			$row = WhitelabelBalanceTypes::firstOrCreate([ 'white_label_id' => $this->whitelabelID,
														   'balance_type_id' => $balcTypeID
														 ] );
			$row = MasterAgentBalances::firstOrCreate([ 'master_agent_id' => $this->mMasterAgent->id,
														'balance_type_id' => $balcTypeID
													  ] );
			if ($this->debug) {
				try {
					$sql = MasterAgentBalances::ListByMasterAgent( $this->whitelabelID, $this->mMasterAgent->id, BalanceType::PAYMODE_WALLET, $balcTypeID, 0, $page, $max, $maxPlusOne, $active )->toSql();
					$bindings = MasterAgentBalances::ListByMasterAgent( $this->whitelabelID, $this->mMasterAgent->id, BalanceType::PAYMODE_WALLET, $balcTypeID, 0, $page, $max, $maxPlusOne, $active )->getBindings();
					$sql = UtilCommon::queryReplace($sql, $bindings);
					Log::info("MasterAgentBalanceBL::getListBalanceWallet SQL = ". $sql);
				} catch ( Exception $e ) {
					Log::info("MasterAgentBalanceBL::getListBalanceWallet exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
			$mList = MasterAgentBalances::ListByMasterAgent( $this->whitelabelID, $this->mMasterAgent->id, BalanceType::PAYMODE_WALLET, $balcTypeID, 0, $page, $max, $maxPlusOne, $active )->get();
		} catch ( Exception $e ) {
			Log::info("MasterAgentBalanceBL::getListBalanceWallet exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $mList;
	}
	
	public function doRealizeCommissionMonthly( ReportCommissionMasterAgentMonthly $mReport, MasterAgentBalances $mBalance, $statusInfo = "", $arrDeduction = array(), $realizeBy = "", $collectiveID = 0, $collectorName = "" ) {
		try {
			$balValueOld = 0;
			$balValueNew = 0;
			$infoPrintable = "";
			try {
				if ($mReport->master_agent_id != $this->mMasterAgent->id) {
					return false;
				}
				$mTable = new ReportCommissionMasterAgentMonthly();
				$tablename = $mTable->getTable();
				$currentYear = (int)date("Y");
				$currentMonth = (int)date("n");
				$currentDay = (int)date("j");
				if (($mReport->report_year == $currentYear && $mReport->report_month == $currentMonth) ||
					($mReport->report_year == ($currentMonth > 1 ? $currentYear : ($currentYear - 1)) && $mReport->report_month == ($currentMonth > 1 ? ($currentMonth - 1) : 12) && $currentDay < CommissionBL::REALIZE_DAY_START)
				) {
					DB::table( $tablename )->where('id', '=', $mReport->id)
										->update( [
											'status' => ReportCommissionMasterAgentMonthly::STATUS_REPORTING,
											'status_updated_at' => date('Y-m-d H:i:s')
										] );
					return false;
				}
				$strAddTo = "";
				if (strlen($statusInfo) > 100) {
					$statusInfo = substr($statusInfo, 0, 100);
				}
				// ==== hitung potongan pajaknya ====
				/*
				 "deduction": {
					 "name" : "Potongan Pajak",
					 "type" : "PERCENTAGE", // FIXED / PERCENTAGE / BYRANGE
					 "value": "5"
				 }
				 */
				$deductionValue = 0;
				$deductionName = "";
				$realizationValue = $mReport->total_commission_received;
				if (! is_null($arrDeduction) && is_array($arrDeduction)) {
					if ($mReport->total_commission_received > 0 && isset($arrDeduction['type']) && isset($arrDeduction['value'])) {
						if ($arrDeduction['type'] == CommissionBL::COMMISSION_TYPE_PERCENTAGE) {
							$percent = (float)($arrDeduction['value'] <= 100 ? $arrDeduction['value'] : 100) / (float)100;
							$deductionValue = (int)((float)$mReport->total_commission_received * $percent);
						} else if ($arrDeduction['type'] == CommissionBL::COMMISSION_TYPE_BYRANGE) {
							$deductionValue = CommissionBL::getValueByRange($arrDeduction['value'], $mReport->total_commission_received, $mReport->total_commission_received );
						} else { // type FIXED
							$deductionValue = ($mReport->total_commission_received > $arrDeduction['value'] ? $arrDeduction['value'] : $mReport->total_commission_received );
						}
						$realizationValue = $mReport->total_commission_received - $deductionValue;
					}
					if (isset($arrDeduction['name'])) {
						$deductionName = $arrDeduction['name'];
						if (! is_null($deductionName) && strlen($deductionName) > 50) {
							$deductionName = substr($deductionName, 0, 50);
						}
					}
				}
				$infoPrintable .= "REALISASI KOMISI|";
				$infoPrintable .= "Bln/thn       : ".$mReport->report_month."/".$mReport->report_year."|";
				$infoPrintable .= "Komisi        : Rp. ".number_format($mReport->total_commission_received,2,',','.')."|";
				$infoPrintable .= "Potongan      : Rp. ".number_format($deductionValue,2,',','.')."|";
				$infoPrintable .= "Keterangan    : ".(string)$deductionName."|";
				$infoPrintable .= "Jml Diterima  : Rp. ".number_format($realizationValue,2,',','.')."|";
				if (empty($mBalance->id)) {
					if ($mReport->status != ReportCommissionMasterAgentMonthly::STATUS_TRANSFERRED && $mReport->status != ReportCommissionMasterAgentMonthly::STATUS_PAID && $mReport->status != ReportCommissionMasterAgentMonthly::STATUS_COLLECTIVE) {
						return false;
					}
					DB::table( $tablename )->where('id', '=', $mReport->id)
							->update( [
								'status' => $mReport->status,
								'status_updated_at' => date('Y-m-d H:i:s'),
								'status_updated_info' => $statusInfo,
								'realize_deduction_name' => $deductionName,
								'realize_deduction_value' => $deductionValue,
								'realize_commission_received' => $realizationValue,
								'realize_collective_id'	  => $collectiveID,
								'realize_by' => $realizeBy
							] );
					if ($mReport->status == ReportCommissionMasterAgentMonthly::STATUS_TRANSFERRED) {
						$infoPrintable .= "Mekanisme     : Ditransfer|";
						$strAddTo = " dilakukan transfer";
					} else if ($mReport->status == ReportCommissionMasterAgentMonthly::STATUS_PAID) {
						$infoPrintable .= "Mekanisme     : Dibayar Cash|";
						$strAddTo = " telah dibayar";
					} else if ($mReport->status == ReportCommissionMasterAgentMonthly::STATUS_COLLECTIVE) {
						$strAddTo = " secara kolektif";
						$infoPrintable .= "Mekanisme     : Kolektif|";
						$infoPrintable .= "Nama Kolektor : ".$collectorName."|";
					}
				} else {
					if ($mBalance->master_agent_id != $this->mMasterAgent->id || $mReport->status != ReportCommissionMasterAgentMonthly::STATUS_REPORTED || $mBalance->paymode == BalanceType::PAYMODE_POSTPAID || empty($mBalance->row_id)) {
						return false;
					}
					$balValueOld = $mBalance->balance_value;
					$balValueNew = $balValueOld + $realizationValue;
					$mTable = new MasterAgentBalances();
					try {
						DB::table( $mTable->getTable() )->where('id', '=', $mBalance->row_id)
							->update( [ 'balance_value' => DB::raw("balance_value + " . $realizationValue) ] );
						$mTable = MasterAgentBalances::where('id', '=', $mBalance->row_id)->firstOrFail();
						$balValueNew = $mTable->balance_value;
						$balValueOld = $balValueNew - $realizationValue;
					} catch ( Exception $e ) {
						Log::info("MasterAgentBalanceBL::doRealizeCommissionMonthly exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
					$mBalance->balance_value = $balValueNew;
					if ($mBalance->paymode == BalanceType::PAYMODE_PREPAID) {
						DB::table( $tablename )->where('id', '=', $mReport->id)
									->update( [
										'status' => ReportCommissionMasterAgentMonthly::STATUS_ADDEDDEPOSIT,
										'status_updated_at' => date('Y-m-d H:i:s'),
										'status_updated_info' => $statusInfo,
										'realize_deduction_name' => $deductionName,
										'realize_deduction_value' => $deductionValue,
										'realize_commission_received' => $realizationValue,
										'realize_collective_id'	  => $collectiveID,
										'realize_by' => $realizeBy
									] );
						$mReport->status = ReportCommissionMasterAgentMonthly::STATUS_ADDEDDEPOSIT;
						$infoPrintable .= "Mekanisme     : Ditambahkan ke Deposit|";
						$strAddTo = " ke deposit";
					} else if ($mBalance->paymode == BalanceType::PAYMODE_WALLET) {
						DB::table( $tablename )->where('id', '=', $mReport->id)
									->update( [
										'status' => ReportCommissionMasterAgentMonthly::STATUS_ADDEDWALLET,
										'status_updated_at' => date('Y-m-d H:i:s'),
										'status_updated_info' => $statusInfo,
										'realize_deduction_name' => $deductionName,
										'realize_deduction_value' => $deductionValue,
										'realize_commission_received' => $realizationValue,
										'realize_collective_id'	  => $collectiveID,
										'realize_by' => $realizeBy
									] );
						$mReport->status = ReportCommissionMasterAgentMonthly::STATUS_ADDEDWALLET;
						$infoPrintable .= "Mekanisme     : Ditambahkan ke Wallet|";
						$strAddTo = " ke wallet";
					}
				}
				$logID = $mReport->id;
				try {
					$newLog = new LogRealizeCommissionMasterAgent();
					$newLog->log_datetime = date('Y-m-d H:i:s');
					$newLog->report_year = $mReport->report_year;
					$newLog->report_month = $mReport->report_month;
					$newLog->master_agent_id = $this->mMasterAgent->id;
					$newLog->white_label_id = $this->mMasterAgent->white_label_id;
					$newLog->currency_code = 'IDR';
					$newLog->total_commission_received = $mReport->total_commission_received;
					$newLog->realize_status = $mReport->status;
					$newLog->realize_deduction_name = $deductionName;
					$newLog->realize_deduction_value = $deductionValue;
					$newLog->realize_commission_received = $realizationValue;
					$newLog->realize_collective_id = $collectiveID;
					$newLog->realize_by = $realizeBy;
					$newLog->balance_type_id = (! empty($mBalance->id) ? $mBalance->id : 0);
					$newLog->balance_value_before = $balValueOld;
					$newLog->balance_value_after = $balValueNew;
					$newLog->save();
					if (! empty($newLog->id)) {
						$logID = $newLog->id;
						$this->logID = $logID;
						$tablename = $newLog->getTable();
					}
				} catch ( Exception $e ) {
					Log::info("MasterAgentBalanceBL::doRealizeCommissionMonthly exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
				try {
					$trxCode = UtilCommon::microtimeStamp() . ConstantList::CODE_HEADER . ConstantList::CODE_COMMISSION . UtilCommon::randomCode(8);
					// update history log trx
					$newLog = new HistoryTrxMasterAgent();
					$newLog->master_agent_id = $this->mMasterAgent->id;
					$newLog->trx_datetime = date('Y-m-d H:i:s');
					$newLog->trx_mode = (empty($mBalance->id) ? HistoryTrxMasterAgent::MODE_INFO : HistoryTrxMasterAgent::MODE_CASHIN);
					$newLog->trx_type = HistoryTrxMasterAgent::TYPE_COMMISSION;
					$newLog->trx_code = $trxCode;
					$newLog->trx_bill_number = null;
					$newLog->trx_product_code = null;
					$newLog->trx_product_id = null;
					$newLog->description = 'Realisasi komisi pada bulan ' . $mReport->report_month . " tahun " . $mReport->report_year . $strAddTo;
					$newLog->amount = $realizationValue;
					$newLog->currency_code = 'IDR';
					$newLog->balance_type_id = (empty($mBalance->id) ? 0 : $mBalance->balance_type_id);
					$newLog->balance_value_before = $balValueOld;
					$newLog->balance_value_after = $balValueNew;
					$newLog->info_printable = $infoPrintable;
					$newLog->log_table = $tablename;
					$newLog->log_id = $logID;
					$newLog->save();
					// POST ACTION after that (send push / send email / ...)
					if ($this->mMasterAgent->notif_balance == 'Y') {
						$this->putQueueSendEmailRealizeCommissionNotification( $logID );
					}
				}  catch ( Exception $e ) {
					Log::info("MasterAgentBalanceBL::doRealizeCommissionMonthly exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
				return true;
			} catch ( Exception $e ) {
				Log::info("MasterAgentBalanceBL::doRealizeCommissionMonthly exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBalanceBL::doRealizeCommissionMonthly exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function doRealizeBonusMonthly( ReportBonusMasterAgentMonthly $mReport, MasterAgentBalances $mBalance, $statusInfo = "", $arrDeduction = array(), $realizeBy = "", $collectiveID = 0, $collectorName = "" ) {
		try {
			$balValueOld = 0;
			$balValueNew = 0;
			$infoPrintable = "";
			try {
				if ($mReport->master_agent_id != $this->mMasterAgent->id) {
					return false;
				}
				$mTable = new ReportBonusMasterAgentMonthly();
				$tablename = $mTable->getTable();
				$currentYear = (int)date("Y");
				$currentMonth = (int)date("n");
				$currentDay = (int)date("j");
				if (($mReport->report_year > $currentYear) || ($mReport->report_year == $currentYear && $mReport->report_month >= $currentMonth) ||
					($mReport->report_year == ($currentMonth > 1 ? $currentYear : ($currentYear - 1)) && $mReport->report_month == ($currentMonth > 1 ? ($currentMonth - 1) : 12) && $currentDay < CommissionBL::REALIZE_DAY_START)
				) {
					DB::table( $tablename )->where('id', '=', $mReport->id)
										->update( [
											'status' => ReportBonusMasterAgentMonthly::STATUS_REPORTING,
											'status_updated_at' => date('Y-m-d H:i:s')
										] );
					return false;
				}
				$strAddTo = "";
				if (strlen($statusInfo) > 100) {
					$statusInfo = substr($statusInfo, 0, 100);
				}
				// ==== hitung potongan pajaknya ====
				/*
				 "deduction": {
					 "name" : "Potongan Pajak",
					 "type" : "PERCENTAGE", // FIXED / PERCENTAGE / BYRANGE
					 "value": "5"
				 }
				 */
				$deductionValue = 0;
				$deductionName = "";
				$realizationValue = $mReport->total_bonus_received;
				if (! is_null($arrDeduction) && is_array($arrDeduction) && $mReport->total_bonus_received > 0) {
					if (isset($arrDeduction['type']) && isset($arrDeduction['value'])) {
						if ($arrDeduction['type'] == CommissionBL::BONUS_TYPE_PERCENTAGE) {
							$percent = (float)($arrDeduction['value'] <= 100 ? $arrDeduction['value'] : 100) / (float)100;
							$deductionValue = (int)((float)$mReport->total_bonus_received * $percent);
						} else if ($arrDeduction['type'] == CommissionBL::BONUS_TYPE_BYRANGE) {
							$deductionValue = CommissionBL::getValueByRange($arrDeduction['value'], $mReport->total_bonus_received, $mReport->total_bonus_received );
						} else { // type FIXED
							$deductionValue = ($mReport->total_bonus_received > $arrDeduction['value'] ? $arrDeduction['value'] : $mReport->total_bonus_received );
						}
						$realizationValue = $mReport->total_bonus_received - $deductionValue;
						if (isset($arrDeduction['name'])) {
							$deductionName = $arrDeduction['name'];
							if (! is_null($deductionName) && strlen($deductionName) > 50) {
								$deductionName = substr($deductionName, 0, 50);
							}
						}
					}
				}
				$infoPrintable .= "REALISASI BONUS|";
				$infoPrintable .= "Bln/thn       : ".$mReport->report_month."/".$mReport->report_year."|";
				$infoPrintable .= "Komisi        : Rp. ".number_format($mReport->total_bonus_received,2,',','.')."|";
				$infoPrintable .= "Potongan      : Rp. ".number_format($deductionValue,2,',','.')."|";
				$infoPrintable .= "Keterangan    : ".(string)$deductionName."|";
				$infoPrintable .= "Jml Diterima  : Rp. ".number_format($realizationValue,2,',','.')."|";
				if (empty($mBalance->id)) {
					if ($mReport->status != ReportBonusMasterAgentMonthly::STATUS_TRANSFERRED && $mReport->status != ReportBonusMasterAgentMonthly::STATUS_PAID && $mReport->status != ReportBonusMasterAgentMonthly::STATUS_COLLECTIVE) {
						return false;
					}
					DB::table( $tablename )->where('id', '=', $mReport->id)
							->update( [
								'status' => $mReport->status,
								'status_updated_at' => date('Y-m-d H:i:s'),
								'status_updated_info' => $statusInfo,
								'realize_deduction_name' => $deductionName,
								'realize_deduction_value' => $deductionValue,
								'realize_bonus_received' => $realizationValue,
								'realize_collective_id'	=> $collectiveID,
								'realize_by' => $realizeBy
							] );
					if ($mReport->status == ReportBonusMasterAgentMonthly::STATUS_TRANSFERRED) {
						$infoPrintable .= "Mekanisme     : Ditransfer|";
						$strAddTo = " dilakukan transfer";
					} else if ($mReport->status == ReportBonusMasterAgentMonthly::STATUS_PAID) {
						$infoPrintable .= "Mekanisme     : Dibayar Cash|";
						$strAddTo = " telah dibayar";
					} else if ($mReport->status == ReportBonusMasterAgentMonthly::STATUS_COLLECTIVE) {
						$strAddTo = " secara kolektif";
						$infoPrintable .= "Mekanisme     : Kolektif|";
						$infoPrintable .= "Nama Kolektor : ".$collectorName."|";
					}
				} else {
					if ($mBalance->master_agent_id != $this->mMasterAgent->id || $mReport->status != ReportBonusMasterAgentMonthly::STATUS_REPORTED || $mBalance->paymode == BalanceType::PAYMODE_POSTPAID || empty($mBalance->row_id)) {
						return false;
					}
					$balValueOld = $mBalance->balance_value;
					$balValueNew = $balValueOld + $realizationValue;
					$mTable = new MasterAgentBalances();
					try {
						DB::table( $mTable->getTable() )->where('id', '=', $mBalance->row_id)
						  ->update( [ 'balance_value' => DB::raw("balance_value + " . $realizationValue) ] );
						$mTable = MasterAgentBalances::where('id', '=', $mBalance->row_id)->firstOrFail();
						$balValueNew = $mTable->balance_value;
						$balValueOld = $balValueNew - $realizationValue;
					} catch ( Exception $e ) {
						Log::info("MasterAgentBalanceBL::doRealizeBonusMonthly exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
					$mBalance->balance_value = $balValueNew;
					if ($mBalance->paymode == BalanceType::PAYMODE_PREPAID) {
						DB::table( $tablename )->where('id', '=', $mReport->id)
							->update( [
								'status' => ReportBonusMasterAgentMonthly::STATUS_ADDEDDEPOSIT,
								'status_updated_at' => date('Y-m-d H:i:s'),
								'status_updated_info' => $statusInfo,
								'realize_deduction_name' => $deductionName,
								'realize_deduction_value' => $deductionValue,
								'realize_bonus_received' => $realizationValue,
								'realize_collective_id'	  => $collectiveID,
								'realize_by' => $realizeBy
							] );
						$mReport->status = ReportBonusMasterAgentMonthly::STATUS_ADDEDDEPOSIT;
						$infoPrintable .= "Mekanisme     : Ditambahkan ke Deposit|";
						$strAddTo = " ke deposit";
					} else if ($mBalance->paymode == BalanceType::PAYMODE_WALLET) {
						DB::table( $tablename )->where('id', '=', $mReport->id)
							->update( [
								'status' => ReportBonusMasterAgentMonthly::STATUS_ADDEDWALLET,
								'status_updated_at' => date('Y-m-d H:i:s'),
								'status_updated_info' => $statusInfo,
								'realize_deduction_name' => $deductionName,
								'realize_deduction_value' => $deductionValue,
								'realize_bonus_received' => $realizationValue,
								'realize_collective_id'	  => $collectiveID,
								'realize_by' => $realizeBy
							] );
						$mReport->status = ReportBonusMasterAgentMonthly::STATUS_ADDEDWALLET;
						$infoPrintable .= "Mekanisme     : Ditambahkan ke Wallet|";
						$strAddTo = " ke wallet";
					}
				}
				$logID = $mReport->id;
				try {
					$newLog = new LogRealizeBonusMasterAgent();
					$newLog->log_datetime = date('Y-m-d H:i:s');
					$newLog->report_year = $mReport->report_year;
					$newLog->report_month = $mReport->report_month;
					$newLog->master_agent_id = $this->mMasterAgent->id;
					$newLog->white_label_id = $this->mMasterAgent->white_label_id;
					$newLog->currency_code = 'IDR';
					$newLog->total_bonus_received = $mReport->total_bonus_received;
					$newLog->realize_status = $mReport->status;
					$newLog->realize_deduction_name = $deductionName;
					$newLog->realize_deduction_value = $deductionValue;
					$newLog->realize_bonus_received = $realizationValue;
					$newLog->realize_collective_id = $collectiveID;
					$newLog->realize_by = $realizeBy;
					$newLog->balance_type_id = (! empty($mBalance->id) ? $mBalance->id : 0);
					$newLog->balance_value_before = $balValueOld;
					$newLog->balance_value_after = $balValueNew;
					$newLog->save();
					if (! empty($newLog->id)) {
						$logID = $newLog->id;
						$this->logID = $logID;
						$tablename = $newLog->getTable();
					}
				} catch ( Exception $e ) {
					Log::info("MasterAgentBalanceBL::doRealizeBonusMonthly exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
				try {
					$trxCode = UtilCommon::microtimeStamp() . ConstantList::CODE_HEADER . ConstantList::CODE_BONUS . UtilCommon::randomCode(8);
					// update history log trx
					$newLog = new HistoryTrxMasterAgent();
					$newLog->master_agent_id = $this->mMasterAgent->id;
					$newLog->trx_datetime = date('Y-m-d H:i:s');
					$newLog->trx_mode = (empty($mBalance->id) ? HistoryTrxMasterAgent::MODE_INFO : HistoryTrxMasterAgent::MODE_CASHIN);
					$newLog->trx_type = HistoryTrxMasterAgent::TYPE_BONUS;
					$newLog->trx_code = $trxCode;
					$newLog->trx_bill_number = null;
					$newLog->trx_product_code = null;
					$newLog->trx_product_id = null;
					$newLog->description = 'Realisasi bonus pada bulan ' . $mReport->report_month . " tahun " . $mReport->report_year . $strAddTo;
					$newLog->amount = $realizationValue;
					$newLog->currency_code = 'IDR';
					$newLog->balance_type_id = (empty($mBalance->id) ? 0 : $mBalance->balance_type_id);
					$newLog->balance_value_before = $balValueOld;
					$newLog->balance_value_after = $balValueNew;
					$newLog->info_printable = $infoPrintable;
					$newLog->log_table = $tablename;
					$newLog->log_id = $logID;
					$newLog->save();
					// POST ACTION after that (send push / send email / ...)
					if ($this->mMasterAgent->notif_balance == 'Y') {
						$this->putQueueSendEmailRealizeBonusNotification( $logID );
					}
				}  catch ( Exception $e ) {
					Log::info("MasterAgentBalanceBL::doRealizeBonusMonthly exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
				return true;
			} catch ( Exception $e ) {
				Log::info("MasterAgentBalanceBL::doRealizeBonusMonthly exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBalanceBL::doRealizeBonusMonthly exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function transferToAgent( $value, MasterAgentBalances $rowBalSender, AgentBalances $rowBalRecv, $senderName = "", $recvName = "" ) {
		try {
			if (empty($value) || empty($rowBalSender->master_agent_id) || empty($rowBalRecv->agent_id) || $this->masterAgentID != $rowBalSender->master_agent_id) {
				return false;
			}
			$valSenderBefore = $rowBalSender->balance_value;
			$valSenderAfter = $rowBalSender->balance_value - $value;
			$valRecvBefore = $rowBalRecv->balance_value;
			$valRecvAfter = $rowBalRecv->balance_value + $value;
			try {
				$mTable = new MasterAgentBalances();
				if (empty($rowBalSender->row_id)) {
					DB::table( $mTable->getTable() )->where( 'master_agent_id', '=', $this->masterAgentID )->where( 'balance_type_id', '=', $rowBalSender->balance_type_id )
													->update( ['balance_value' => DB::raw("balance_value - " . $value)] );
					$mTable = MasterAgentBalances::where( 'master_agent_id', '=', $this->masterAgentID )->where( 'balance_type_id', '=', $rowBalSender->balance_type_id )->firstOrFail();
				} else {
					DB::table( $mTable->getTable() )->where( 'id', '=', $rowBalSender->row_id )
													->update( ['balance_value' => DB::raw("balance_value - " . $value)] );
					$mTable = MasterAgentBalances::where( 'id', '=', $rowBalSender->row_id )->firstOrFail();
				}
				$valSenderAfter = $mTable->balance_value;
				$valSenderBefore = $mTable->balance_value + $value;
				$rowBalSender->balance_value = $mTable->balance_value;
			} catch ( Exception $e ) {
				Log::info("MasterAgentBalanceBL::transferToAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				$mTable = new AgentBalances();
				if (empty($rowBalRecv->row_id)) {
					DB::table( $mTable->getTable() )->where( 'agent_id', '=', $rowBalRecv->agent_id )->where( 'balance_type_id', '=', $rowBalRecv->balance_type_id )
													->update( ['balance_value' => DB::raw("balance_value + " . $value)] );
					$mTable = AgentBalances::where( 'agent_id', '=', $rowBalRecv->agent_id )->where( 'balance_type_id', '=', $rowBalRecv->balance_type_id )->firstOrFail();
				} else {
					DB::table( $mTable->getTable() )->where( 'id', '=', $rowBalRecv->row_id )
													->update( ['balance_value' => DB::raw("balance_value + " . $value)] );
					$mTable = AgentBalances::where( 'id', '=', $rowBalRecv->row_id )->firstOrFail();
				}
				$valRecvAfter = $mTable->balance_value;
				$valRecvBefore = $mTable->balance_value - $value;
				$rowBalRecv->balance_value = $mTable->balance_value;
			} catch ( Exception $e ) {
				Log::info("MasterAgentBalanceBL::transferToAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			$tablename = "";
			$logID = 0;
			try {
				$mLog = new LogTransferBalance();
				$mLog->log_datetime = date('Y-m-d H:i:s');
				$mLog->white_label_id = $this->whitelabelID;
				$mLog->transfer_value = $value;
				$mLog->sender_type = LogTransferBalance::SENDER_TYPE_MASTER_AGENT;
				$mLog->sender_id = $rowBalSender->master_agent_id;
				$mLog->sender_paymode = $rowBalSender->paymode;
				$mLog->sender_currency_code = $rowBalSender->currency_code;
				$mLog->sender_balance_type_id = $rowBalSender->balance_type_id;
				$mLog->sender_balance_value_before = $valSenderBefore;
				$mLog->sender_balance_value_after = $valSenderAfter;
				$mLog->receiver_type = LogTransferBalance::RECEIVER_TYPE_AGENT;
				$mLog->receiver_id = $rowBalRecv->agent_id;
				$mLog->receiver_paymode = $rowBalRecv->paymode;
				$mLog->receiver_currency_code = $rowBalRecv->currency_code;
				$mLog->receiver_balance_type_id = $rowBalRecv->balance_type_id;
				$mLog->receiver_balance_value_before = $valRecvBefore;
				$mLog->receiver_balance_value_after = $valRecvAfter;
				$mLog->save();
				$logID = $mLog->id;
				$this->logID = $logID;
				$tablename = $mLog->getTable();
			} catch ( Exception $e ) {
				Log::info("MasterAgentBalanceBL::transferToAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				$trxCode = UtilCommon::microtimeStamp() . ConstantList::CODE_HEADER . ConstantList::CODE_TRANSFER . UtilCommon::randomCode(8);
				// update history log trx
				$newLog = new HistoryTrxMasterAgent();
				$newLog->master_agent_id = $rowBalSender->master_agent_id;
				$newLog->trx_datetime = date('Y-m-d H:i:s');
				$newLog->trx_mode = HistoryTrxMasterAgent::MODE_CASHOUT;
				$newLog->trx_type = HistoryTrxMasterAgent::TYPE_TRANSFER;
				$newLog->trx_code = $trxCode;
				$newLog->trx_bill_number = null;
				$newLog->trx_product_code = null;
				$newLog->trx_product_id = null;
				$newLog->description = 'Send transfer balance to Agent '.$recvName;
				$newLog->amount = $value;
				$newLog->currency_code = $rowBalSender->currency_code;
				$newLog->balance_type_id = $rowBalSender->balance_type_id;
				$newLog->balance_value_before = $valSenderBefore;
				$newLog->balance_value_after = $valSenderAfter;
				$newLog->info_printable = '';
				$newLog->log_table = $tablename;
				$newLog->log_id = $logID;
				$newLog->save();
			}  catch ( Exception $e ) {
				Log::info("MasterAgentBalanceBL::transferToAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				$trxCode = UtilCommon::microtimeStamp() . ConstantList::CODE_HEADER . ConstantList::CODE_TRANSFER . UtilCommon::randomCode(8);
				// update history log trx
				$newLog = new HistoryTrxAgent();
				$newLog->agent_id = $rowBalRecv->agent_id;
				$newLog->trx_datetime = date('Y-m-d H:i:s');
				$newLog->trx_mode = HistoryTrxAgent::MODE_CASHIN;
				$newLog->trx_type = HistoryTrxAgent::TYPE_TRANSFER;
				$newLog->trx_code = $trxCode;
				$newLog->trx_bill_number = null;
				$newLog->trx_product_code = null;
				$newLog->trx_product_id = null;
				$newLog->description = 'Receive transfer balance from Master Agent '.$senderName;
				$newLog->amount = $value;
				$newLog->currency_code = $rowBalRecv->currency_code;
				$newLog->balance_type_id = $rowBalRecv->balance_type_id;
				$newLog->balance_value_before = $valRecvBefore;
				$newLog->balance_value_after = $valRecvAfter;
				$newLog->info_printable = '';
				$newLog->log_table = $tablename;
				$newLog->log_id = $logID;
				$newLog->save();
				// POST ACTION after that (send push / send email / ...)
				try {
					$mMA = MasterAgent::where('id','=',$rowBalSender->master_agent_id)->select('id','notif_balance')->firstOrFail();
					if ($mMA->notif_balance == 'Y') {
						$this->putQueueSendEmailTransferBalanceNotification( $logID, "SENDER" );
					}
				}  catch ( Exception $e ) {
					Log::info("MasterAgentBalanceBL::transferToAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
				try {
					$mAgent = Agent::where('id','=',$rowBalRecv->agent_id)->select('id','notif_balance')->firstOrFail();
					if ($mAgent->notif_balance == 'Y') {
						$this->putQueueSendEmailTransferBalanceNotification( $logID, "RECEIVER" );
						try {
							$text = "Menerima transfer saldo dari Master Agent ".$senderName." sebesar Rp " . number_format($newLog->amount,2,',','.');
							$title = "Info";
							SendPushTextToAgentJob::putQueueSendPush($text, $title, $mAgent->id, $mAgent->white_label_id );
						} catch ( Exception $e ) {
							Log::info("MasterAgentBalanceBL::transferToAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
						}
					}
				}  catch ( Exception $e ) {
					Log::info("MasterAgentBalanceBL::transferToAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}  catch ( Exception $e ) {
				Log::info("MasterAgentBalanceBL::transferToAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentBalanceBL::transferToAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function transferToMasterAgent( $value, MasterAgentBalances $rowBalSender, MasterAgentBalances $rowBalRecv, $senderName = "", $recvName = "" ) {
		try {
			if (empty($value) || empty($rowBalSender->master_agent_id) || empty($rowBalRecv->master_agent_id) || $this->masterAgentID != $rowBalSender->master_agent_id || $rowBalSender->master_agent_id == $rowBalRecv->master_agent_id) {
				return false;
			}
			$valSenderBefore = $rowBalSender->balance_value;
			$valSenderAfter = $rowBalSender->balance_value - $value;
			$valRecvBefore = $rowBalRecv->balance_value;
			$valRecvAfter = $rowBalRecv->balance_value + $value;
			$mTable = new MasterAgentBalances();
			try {
				$mTable = new MasterAgentBalances();
				if (empty($rowBalSender->row_id)) {
					DB::table( $mTable->getTable() )->where( 'master_agent_id', '=', $this->masterAgentID )->where( 'balance_type_id', '=', $rowBalSender->balance_type_id )
					  								->update( ['balance_value' => DB::raw("balance_value - " . $value)] );
					$mTable = MasterAgentBalances::where( 'master_agent_id', '=', $this->masterAgentID )->where( 'balance_type_id', '=', $rowBalSender->balance_type_id )->firstOrFail();
				} else {
					DB::table( $mTable->getTable() )->where( 'id', '=', $rowBalSender->row_id )
													->update( ['balance_value' => DB::raw("balance_value - " . $value)] );
					$mTable = MasterAgentBalances::where( 'id', '=', $rowBalSender->row_id )->firstOrFail();
				}
				$valSenderAfter = $mTable->balance_value;
				$valSenderBefore = $mTable->balance_value + $value;
				$rowBalSender->balance_value = $mTable->balance_value;
			} catch ( Exception $e ) {
				Log::info("MasterAgentBalanceBL::transferToMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				$mTable = new MasterAgentBalances();
				if (empty($rowBalRecv->row_id)) {
					DB::table( $mTable->getTable() )->where( 'master_agent_id', '=', $rowBalRecv->master_agent_id )->where( 'balance_type_id', '=', $rowBalRecv->balance_type_id )
													->update( ['balance_value' => DB::raw("balance_value + " . $value)] );
					$mTable = MasterAgentBalances::where( 'master_agent_id', '=', $rowBalRecv->master_agent_id )->where( 'balance_type_id', '=', $rowBalRecv->balance_type_id )->firstOrFail();
				} else {
					DB::table( $mTable->getTable() )->where( 'id', '=', $rowBalRecv->row_id )
													->update( ['balance_value' => DB::raw("balance_value + " . $value)] );
					$mTable = MasterAgentBalances::where( 'id', '=', $rowBalRecv->row_id )->firstOrFail();
				}
				$valRecvAfter = $mTable->balance_value;
				$valRecvBefore = $mTable->balance_value - $value;
				$rowBalRecv->balance_value = $mTable->balance_value;
			} catch ( Exception $e ) {
				Log::info("MasterAgentBalanceBL::transferToMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			$tablename = "";
			$logID = 0;
			try {
				$mLog = new LogTransferBalance();
				$mLog->log_datetime = date('Y-m-d H:i:s');
				$mLog->white_label_id = $this->whitelabelID;
				$mLog->transfer_value = $value;
				$mLog->sender_type = LogTransferBalance::SENDER_TYPE_MASTER_AGENT;
				$mLog->sender_id = $rowBalSender->master_agent_id;
				$mLog->sender_paymode = $rowBalSender->paymode;
				$mLog->sender_currency_code = $rowBalSender->currency_code;
				$mLog->sender_balance_type_id = $rowBalSender->balance_type_id;
				$mLog->sender_balance_value_before = $valSenderBefore;
				$mLog->sender_balance_value_after = $valSenderAfter;
				$mLog->receiver_type = LogTransferBalance::RECEIVER_TYPE_MASTER_AGENT;
				$mLog->receiver_id = $rowBalRecv->master_agent_id;
				$mLog->receiver_paymode = $rowBalRecv->paymode;
				$mLog->receiver_currency_code = $rowBalRecv->currency_code;
				$mLog->receiver_balance_type_id = $rowBalRecv->balance_type_id;
				$mLog->receiver_balance_value_before = $valRecvBefore;
				$mLog->receiver_balance_value_after = $valRecvAfter;
				$mLog->save();
				$logID = $mLog->id;
				$this->logID = $logID;
				$tablename = $mLog->getTable();
			} catch ( Exception $e ) {
				Log::info("MasterAgentBalanceBL::transferToMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				$trxCode = UtilCommon::microtimeStamp() . ConstantList::CODE_HEADER . ConstantList::CODE_TRANSFER . UtilCommon::randomCode(8);
				// update history log trx
				$newLog = new HistoryTrxMasterAgent();
				$newLog->master_agent_id = $rowBalSender->master_agent_id;
				$newLog->trx_datetime = date('Y-m-d H:i:s');
				$newLog->trx_mode = HistoryTrxMasterAgent::MODE_CASHOUT;
				$newLog->trx_type = HistoryTrxMasterAgent::TYPE_TRANSFER;
				$newLog->trx_code = $trxCode;
				$newLog->trx_bill_number = null;
				$newLog->trx_product_code = null;
				$newLog->trx_product_id = null;
				$newLog->description = 'Send transfer balance to Master Agent '.$recvName;
				$newLog->amount = $value;
				$newLog->currency_code = $rowBalSender->currency_code;
				$newLog->balance_type_id = $rowBalSender->balance_type_id;
				$newLog->balance_value_before = $valSenderBefore;
				$newLog->balance_value_after = $valSenderAfter;
				$newLog->info_printable = '';
				$newLog->log_table = $tablename;
				$newLog->log_id = $logID;
				$newLog->save();
			}  catch ( Exception $e ) {
				Log::info("MasterAgentBalanceBL::transferToMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				$trxCode = UtilCommon::microtimeStamp() . ConstantList::CODE_HEADER . ConstantList::CODE_TRANSFER . UtilCommon::randomCode(8);
				// update history log trx
				$newLog = new HistoryTrxMasterAgent();
				$newLog->master_agent_id = $rowBalRecv->master_agent_id;
				$newLog->trx_datetime = date('Y-m-d H:i:s');
				$newLog->trx_mode = HistoryTrxMasterAgent::MODE_CASHIN;
				$newLog->trx_type = HistoryTrxMasterAgent::TYPE_TRANSFER;
				$newLog->trx_code = $trxCode;
				$newLog->trx_bill_number = null;
				$newLog->trx_product_code = null;
				$newLog->trx_product_id = null;
				$newLog->description = 'Receive transfer balance from Master Agent '.$senderName;
				$newLog->amount = $value;
				$newLog->currency_code = $rowBalRecv->currency_code;
				$newLog->balance_type_id = $rowBalRecv->balance_type_id;
				$newLog->balance_value_before = $valRecvBefore;
				$newLog->balance_value_after = $valRecvAfter;
				$newLog->info_printable = '';
				$newLog->log_table = $tablename;
				$newLog->log_id = $logID;
				$newLog->save();
				// POST ACTION after that (send push / send email / ...)
				try {
					$mMA = MasterAgent::where('id','=',$rowBalSender->master_agent_id)->select('id','notif_balance')->firstOrFail();
					if ($mMA->notif_balance == 'Y') {
						$this->putQueueSendEmailTransferBalanceNotification( $logID, "SENDER" );
					}
				}  catch ( Exception $e ) {
					Log::info("MasterAgentBalanceBL::transferToMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
				try {
					$mMA = MasterAgent::where('id','=',$rowBalRecv->master_agent_id)->select('id','notif_balance')->firstOrFail();
					if ($mMA->notif_balance == 'Y') {
						$this->putQueueSendEmailTransferBalanceNotification( $logID, "RECEIVER" );
					}
				}  catch ( Exception $e ) {
					Log::info("MasterAgentBalanceBL::transferToMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}  catch ( Exception $e ) {
				Log::info("MasterAgentBalanceBL::transferToMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentBalanceBL::transferToMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	private function putQueueSendEmailRealizeCommissionNotification( $logID ) {
		try {
			$queuename = "fintechapi_email";
			$job = new SendEmailRealizeCommissionMasterAgentJob( $logID );
			if (! empty($queuename)) {
				$job->onQueue($queuename);
			}
			dispatch( $job );
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentBalanceBL::putQueueSendEmailRealizeCommissionNotification log_id=".$logID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function sendEmailRealizeCommissionNotification( $logID ) {
		try {
			$mLog = LogRealizeCommissionMasterAgent::where('id','=',$logID)->firstOrFail();
			$this->setMasterAgent( $mLog->master_agent_id );
			// sementara
			$subject = "Notifikasi: Realisasi komisi Master Agent";
			$email = "";
			$fullname = "";
			$profileBL = new MasterAgentProfileBL( $this->whitelabelID );
			$profileBL->setMasterAgentModel( $this->mMasterAgent );
			$mProfile = $profileBL->getProfileModel();
			if (! is_null($mProfile)) {
				$fullname = $mProfile->fullname;
				if (empty($email)) {
					$email = $mProfile->email;
				}
			}
			if (! empty($email)) {
				if (empty($fullname)) {
					$fullname = $this->mMasterAgent->username;
				}
				if (is_null($this->mWL)) {
					$this->setWhiteLabelID( $this->mMasterAgent->white_label_id, true );
				}
				$statusName = "";
				if ($mLog->realize_status == ReportCommissionMasterAgentMonthly::STATUS_TRANSFERRED) {
					$statusName = "Ditransfer";
				} else if ($mLog->realize_status == ReportCommissionMasterAgentMonthly::STATUS_PAID) {
					$statusName = "Dibayar Cash";
				} else if ($mLog->realize_status == ReportCommissionMasterAgentMonthly::STATUS_COLLECTIVE) {
					$statusName = "Kolektif";
				} else if ($mLog->realize_status == ReportCommissionMasterAgentMonthly::STATUS_ADDEDDEPOSIT) {
					$statusName = "Ditambahkan ke Deposit";
				} else if ($mLog->realize_status == ReportCommissionMasterAgentMonthly::STATUS_ADDEDWALLET) {
					$statusName = "Ditambahkan ke Wallet";
				} else {
					$statusName = $mLog->realize_status;
				}
				$nameBlade = "email.white_label_".$this->mMasterAgent->white_label_id.".notification.notif_commission";
				$paramBlade = array(
						'fullname' => $fullname,
						'datetime' => UtilCommon::stringDateTimeTransaction( $mLog->log_datetime ),
						'owner_type' => 'MASTER AGENT (KOORDINATOR)',
						'owner_id' => strtoupper(UtilCommon::idConvertToBase36($this->mMasterAgent->id)),
						'month_name' => UtilCommon::monthName( $mLog->report_month ),
						'month' => $mLog->report_month,
						'year' => $mLog->report_year,
						'total_commission_received' => $mLog->total_commission_received,
						'realize_deduction_value' => $mLog->realize_deduction_value,
						'realize_deduction_name' => $mLog->realize_deduction_name,
						'realize_commission_received' => $mLog->realize_commission_received,
						'realize_status' => $mLog->realize_status,
						'realize_status_name' => $statusName,
						'balance_value_before' => $mLog->balance_value_before,
						'balance_value_after' => $mLog->balance_value_after,
				);
				$b = false;
				if ( empty($this->mWL->email_from_address) ) {
					$b = UtilEmail::sendEmailByTemplateView($nameBlade, $paramBlade, $subject, $email, $fullname);
				} else {
					$b = UtilEmail::sendEmailByTemplateView($nameBlade, $paramBlade, $subject, $email, $fullname, $this->mWL->email_from_address, $this->mWL->email_from_fullname);
				}
				return $b;
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBalanceBL::sendEmailRealizeCommissionNotification log_id=".$logID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}

	private function putQueueSendEmailRealizeBonusNotification( $logID ) {
		try {
			$queuename = "fintechapi_email";
			$job = new SendEmailRealizeBonusMasterAgentJob( $logID );
			if (! empty($queuename)) {
				$job->onQueue($queuename);
			}
			dispatch( $job );
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentBalanceBL::putQueueSendEmailRealizeBonusNotification log_id=".$logID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function sendEmailRealizeBonusNotification( $logID ) {
		try {
			$mLog = LogRealizeBonusMasterAgent::where('id','=',$logID)->firstOrFail();
			$this->setMasterAgent( $mLog->master_agent_id );
			// sementara
			$subject = "Notifikasi: Realisasi bonus Master Agent";
			$email = "";
			$fullname = "";
			$profileBL = new MasterAgentProfileBL( $this->whitelabelID );
			$profileBL->setMasterAgentModel( $this->mMasterAgent );
			$mProfile = $profileBL->getProfileModel();
			if (! is_null($mProfile)) {
				$fullname = $mProfile->fullname;
				if (empty($email)) {
					$email = $mProfile->email;
				}
			}
			if (! empty($email)) {
				if (empty($fullname)) {
					$fullname = $this->mMasterAgent->username;
				}
				if (is_null($this->mWL)) {
					$this->setWhiteLabelID( $this->mMasterAgent->white_label_id, true );
				}
				$statusName = "";
				if ($mLog->realize_status == ReportBonusMasterAgentMonthly::STATUS_TRANSFERRED) {
					$statusName = "Ditransfer";
				} else if ($mLog->realize_status == ReportBonusMasterAgentMonthly::STATUS_PAID) {
					$statusName = "Dibayar Cash";
				} else if ($mLog->realize_status == ReportBonusMasterAgentMonthly::STATUS_COLLECTIVE) {
					$statusName = "Kolektif";
				} else if ($mLog->realize_status == ReportBonusMasterAgentMonthly::STATUS_ADDEDDEPOSIT) {
					$statusName = "Ditambahkan ke Deposit";
				} else if ($mLog->realize_status == ReportBonusMasterAgentMonthly::STATUS_ADDEDWALLET) {
					$statusName = "Ditambahkan ke Wallet";
				} else {
					$statusName = $mLog->realize_status;
				}
				$nameBlade = "email.white_label_".$this->mMasterAgent->white_label_id.".notification.notif_bonus";
				$paramBlade = array(
						'fullname' => $fullname,
						'datetime' => UtilCommon::stringDateTimeTransaction( $mLog->log_datetime ),
						'owner_type' => 'MASTER AGENT (KOORDINATOR)',
						'owner_id' => strtoupper(UtilCommon::idConvertToBase36($this->mMasterAgent->id)),
						'month_name' => UtilCommon::monthName( $mLog->report_month ),
						'month' => $mLog->report_month,
						'year' => $mLog->report_year,
						'total_bonus_received' => $mLog->total_bonus_received,
						'realize_deduction_value' => $mLog->realize_deduction_value,
						'realize_deduction_name' => $mLog->realize_deduction_name,
						'realize_bonus_received' => $mLog->realize_bonus_received,
						'realize_status' => $mLog->realize_status,
						'realize_status_name' => $statusName,
						'balance_value_before' => $mLog->balance_value_before,
						'balance_value_after' => $mLog->balance_value_after,
				);
				$b = false;
				if ( empty($this->mWL->email_from_address) ) {
					$b = UtilEmail::sendEmailByTemplateView($nameBlade, $paramBlade, $subject, $email, $fullname);
				} else {
					$b = UtilEmail::sendEmailByTemplateView($nameBlade, $paramBlade, $subject, $email, $fullname, $this->mWL->email_from_address, $this->mWL->email_from_fullname);
				}
				return $b;
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBalanceBL::sendEmailRealizeBonusNotification log_id=".$logID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	private function putQueueSendEmailTransferBalanceNotification( $logID, $activityType ) {
		try {
			$queuename = "fintechapi_email";
			$job = new SendEmailTransferBalanceJob( $logID, $activityType );
			if (! empty($queuename)) {
				$job->onQueue($queuename);
			}
			dispatch( $job );
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentBalanceBL::putQueueSendEmailTransferBalanceNotification log_id=".$logID." activity_type=".$activityType." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function sendEmailTransferBalanceNotification( $logID, $activityType ) {
		try {
			$mLog = LogTransferBalance::where('id','=',$logID)->firstOrFail();
			$activityType = strtoupper(trim($activityType));
			// sementara
			$subject = "Notifikasi: Transfer Balance";
			$email = "";
			$fullname = "";
			$ownerType = "";
			$ownerID = 0;
			$partnerType = "";
			$partnerID = 0;
			$partnerName = "";
			if ($activityType == "SENDER") {
				$ownerID = $mLog->sender_id;
				$partnerID = $mLog->receiver_id;
				if ($mLog->sender_type == LogTransferBalance::SENDER_TYPE_MASTER_AGENT) {
					$ownerType = "MASTER AGENT (KOORDINATOR)";
					$profileBL = new MasterAgentProfileBL( $mLog->white_label_id );
					$profileBL->setMasterAgent( $mLog->sender_id );
					$mProfile = $profileBL->getProfileModel();
					$email = $mProfile->email;
					$fullname = $mProfile->fullname;
					if (empty($fullname)) {
						$mMA = $profileBL->getMasterAgentModel();
						$fullname = $mMA->username;
					}
				} else if ($mLog->sender_type == LogTransferBalance::SENDER_TYPE_AGENT) {
					$ownerType = "AGENT";
					$profileBL = new AgentProfileBL( $mLog->white_label_id );
					$profileBL->setAgent( $mLog->sender_id );
					$mProfile = $profileBL->getProfileModel();
					$mAgent = $profileBL->getAgentModel();
					if ($mAgent->identity_type == Agent::IDENTITY_TYPE_EMAIL) {
						$email = $mAgent->username;
					}
					if (empty($email)) {
						$email = $mProfile->email;
					}
					$fullname = $mProfile->fullname;
					if (empty($fullname)) {
						$fullname = $mAgent->username;
					}
				}
				if ($mLog->receiver_type == LogTransferBalance::RECEIVER_TYPE_MASTER_AGENT) {
					$partnerType = "MASTER AGENT (KOORDINATOR)";
					$profileBL = new MasterAgentProfileBL( $mLog->white_label_id );
					$profileBL->setMasterAgent( $mLog->receiver_id );
					$mProfile = $profileBL->getProfileModel();
					$partnerName = $mProfile->fullname;
					if (empty($partnerName)) {
						$mMA = $profileBL->getMasterAgentModel();
						$partnerName = $mMA->username;
					}
				} else if ($mLog->receiver_type == LogTransferBalance::RECEIVER_TYPE_AGENT) {
					$partnerType = "AGENT";
					$profileBL = new AgentProfileBL( $mLog->white_label_id );
					$profileBL->setAgent( $mLog->receiver_id );
					$mProfile = $profileBL->getProfileModel();
					$partnerName = $mProfile->fullname;
					if (empty($partnerName)) {
						$mAgent = $profileBL->getAgentModel();
						$partnerName = $mAgent->username;
					}
				}
			} else { // RECEIVER
				$ownerID = $mLog->receiver_id;
				$partnerID = $mLog->sender_id;
				if ($mLog->receiver_type == LogTransferBalance::RECEIVER_TYPE_MASTER_AGENT) {
					$ownerType = "MASTER AGENT (KOORDINATOR)";
					$profileBL = new MasterAgentProfileBL( $mLog->white_label_id );
					$profileBL->setMasterAgent( $mLog->receiver_id );
					$mProfile = $profileBL->getProfileModel();
					$email = $mProfile->email;
					$fullname = $mProfile->fullname;
					if (empty($fullname)) {
						$mMA = $profileBL->getMasterAgentModel();
						$fullname = $mMA->username;
					}
				} else if ($mLog->receiver_type == LogTransferBalance::RECEIVER_TYPE_AGENT) {
					$ownerType = "AGENT";
					$profileBL = new AgentProfileBL( $mLog->white_label_id );
					$profileBL->setAgent( $mLog->receiver_id );
					$mProfile = $profileBL->getProfileModel();
					$mAgent = $profileBL->getAgentModel();
					if ($mAgent->identity_type == Agent::IDENTITY_TYPE_EMAIL) {
						$email = $mAgent->username;
					}
					if (empty($email)) {
						$email = $mProfile->email;
					}
					$fullname = $mProfile->fullname;
					if (empty($fullname)) {
						$fullname = $mAgent->username;
					}
				}
				if ($mLog->sender_type == LogTransferBalance::SENDER_TYPE_MASTER_AGENT) {
					$partnerType = "MASTER AGENT (KOORDINATOR)";
					$profileBL = new MasterAgentProfileBL( $mLog->white_label_id );
					$profileBL->setMasterAgent( $mLog->sender_id );
					$mProfile = $profileBL->getProfileModel();
					$partnerName = $mProfile->fullname;
					if (empty($partnerName)) {
						$mMA = $profileBL->getMasterAgentModel();
						$partnerName = $mMA->username;
					}
				} else if ($mLog->sender_type == LogTransferBalance::SENDER_TYPE_AGENT) {
					$partnerType = "AGENT";
					$profileBL = new AgentProfileBL( $mLog->white_label_id );
					$profileBL->setAgent( $mLog->sender_id );
					$mProfile = $profileBL->getProfileModel();
					$partnerName = $mProfile->fullname;
					if (empty($partnerName)) {
						$mAgent = $profileBL->getAgentModel();
						$partnerName = $mAgent->username;
					}
				}
			}
			if (! empty($email)) {
				if (is_null($this->mWL)) {
					$this->setWhiteLabelID( $mLog->white_label_id, true );
				}
				$nameBlade = "email.white_label_".$mLog->white_label_id.".notification.notif_transfer_balance";
				$paramBlade = array(
						'activity_type' => $activityType,
						'fullname' => $fullname,
						'datetime' => UtilCommon::stringDateTimeTransaction( $mLog->log_datetime ),
						'owner_type' => $ownerType,
						'owner_id' => strtoupper(UtilCommon::idConvertToBase36($ownerID)),
						'transfer_value' => $mLog->transfer_value,
						'balance_value_before' => ($activityType == "SENDER" ? $mLog->sender_balance_value_before : $mLog->receiver_balance_value_before),
						'balance_value_after' =>  ($activityType == "SENDER" ? $mLog->sender_balance_value_after : $mLog->receiver_balance_value_after),
						'partner_type' => $partnerType,
						'partner_id' => strtoupper(UtilCommon::idConvertToBase36($partnerID)),
						'partner_name' => $partnerName,
						
				);
				$b = false;
				if ( empty($this->mWL->email_from_address) ) {
					$b = UtilEmail::sendEmailByTemplateView($nameBlade, $paramBlade, $subject, $email, $fullname);
				} else {
					$b = UtilEmail::sendEmailByTemplateView($nameBlade, $paramBlade, $subject, $email, $fullname, $this->mWL->email_from_address, $this->mWL->email_from_fullname);
				}
				return $b;
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBalanceBL::sendEmailRealizeBonusNotification log_id=".$logID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
}