<?php
namespace App\BusinessLogic\MasterAgent;

use Log;
use Config;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use App\BusinessLogic\WhiteLabel\WhiteLabelBL;
use App\BusinessLogic\MasterAgent\MasterAgentDownlineBL;
use App\SystemLogic\Auth\PasswordMasterAgent;
use App\Core\CacheData\Simple\Username\CacheUsernameMasterAgent;
use App\Core\Util\UtilCommon;
use App\Models\Database\WhiteLabel;
use App\Models\Database\MasterAgent;
use App\Models\Database\MasterAgentCommissionDownline;
use App\Models\Database\WhitelabelCommissionLevel;
use App\Models\Database\MasterAgentBonusDownline;
use App\Models\Database\WhitelabelBonusLevel;
use App\Models\Database\ByRange;
use App\Models\Database\BalanceType;
use App\Models\Api\ApiError;

class MasterAgentBL
{
	protected $debug = false;
	protected $apiversion = 1;
	protected $whitelabelID = 0;
	protected $mWL = null;
	protected $masterAgentID = 0;
	protected $mMasterAgent = null;
	private $isInvalidParent = false;
	
	public function __construct( $whitelabelID, $masterAgentID = 0, $apiversion = 1 ) {
		$this->debug = boolval(Config::get('app.debug'));
		$this->whitelabelID = (int)$whitelabelID;
		$this->masterAgentID = (int)$masterAgentID;
		$this->apiversion = (int)$apiversion;
	}
	
	public function isInvalidMasterAgentParent() {
		return $this->isInvalidParent;
	}
	
	public function setWhiteLabelID( $id, $model = false ) {
		$this->whitelabelID = (int)$id;
		try {
			if (! is_null($this->mWL) && $this->mWL->id != $this->whitelabelID) {
				$this->mWL = null;
			}
			if ($model && is_null($this->mWL)) {
				$objWL = new WhiteLabelBL( $this->apiversion );
				$objWL->setWhiteLabel( $id );
				$this->mWL = $objWL->getWhiteLabelModel();
			}
		} catch ( Exception $e ) {
			if ($this->debug) Log::info("MasterAgentBL::setWhiteLabelID id=".$id." model=".($model?"true":"false")." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $this;
	}
	public function setWhiteLabelModel( WhiteLabel $mWL ) {
		$this->whitelabelID = 0;
		$this->mWL = $mWL;
		if (! is_null($this->mWL)) {
			$this->whitelabelID = $this->mWL->id;
		}
		return $this;
	}
	public function getWhiteLabelID() {
		return $this->whitelabelID;
	}
	public function getWhiteLabelModel() {
		return $this->mWL;
	}
	
	protected function initModelMasterAgentWhiteLabel() {
		try {
			if (is_null($this->mMasterAgent) || (! is_null($this->mMasterAgent) && empty($this->mMasterAgent->id))) {
				return false;
			}
			if (is_null($this->mWL)) {
				if (empty($this->whitelabelID)) {
					$this->whitelabelID = $this->mMasterAgent->white_label_id;
				}
				$this->setWhiteLabelID( $this->whitelabelID, true );
			}
			if (is_null($this->mWL)) {
				return false;
			}
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentBL::initModelMasterAgentWhiteLabel exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function getURLPathFintech() {
		$urlPath = "";
		try {
			if (! is_null($this->mWL)) {
				if (! empty($this->mWL->domain)) {
					$urlPath = "https://" . $this->mWL->domain . "/fintech/master-agent/";
				}
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBL::getURLPathFintech exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		} finally {
			if (empty($urlPath)) {
				$urlPath = "https://apiweb.posfin.id/fintech/master-agent/";
			}
		}
		return $urlPath;
	}
	
	public function getURLPathResourceFile() {
		$urlPath = "";
		try {
			if (! is_null($this->mWL)) {
				if (! empty($this->mWL->web_static)) {
					$urlPath = $this->mWL->web_static . (substr($this->mWL->web_static, -1) == '/' ? "": "/") . "master-agent/";
				} else if (! empty($this->mWL->domain)) {
					$urlPath = "https://" . $this->mWL->domain . "/fintech/master-agent/";
				}
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBL::getURLPathResourceFile exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		} finally {
			if (empty($urlPath)) {
				$urlPath = "https://apiweb.posfin.id/fintech/master-agent/";
			}
		}
		return $urlPath;
	}
	
	public function setMasterAgent( $id, $active = false, $needConvert = false ) {
		try {
			$this->mMasterAgent = null;
			if ($this->debug) Log::info("MasterAgentBL::setMasterAgent id=".$id);
			$oldID = $id;
			$isDigit = preg_match("/^[0-9]*$/", (string)$id);
			$len = strlen((string)$id);
			if ($needConvert || (! $isDigit) || ($isDigit && $len <= 6 && $len >= 4)) {
				$id = UtilCommon::idConvertToInt( $id );
			}
			try {
				if ($active) {
					$this->mMasterAgent = MasterAgent::where('id', '=', $id)->where('status', '=', MasterAgent::STATUS_ACTIVE)->firstOrFail();
				} else {
					$this->mMasterAgent = MasterAgent::where('id', '=', $id)->firstOrFail();
				}
			} catch ( Exception $e ) {
				$this->mMasterAgent = null;
				if ($this->debug) Log::info("MasterAgentBL::setMasterAgent id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			if (is_null($this->mMasterAgent) && UtilCommon::isDigit($oldID) && $id != $oldID) {
				$id = $oldID;
				try {
					if ($active) {
						$this->mMasterAgent = MasterAgent::where('id', '=', $id)->where('status', '=', MasterAgent::STATUS_ACTIVE)->firstOrFail();
					} else {
						$this->mMasterAgent = MasterAgent::where('id', '=', $id)->firstOrFail();
					}
				} catch ( Exception $e ) {
					$this->mMasterAgent = null;
					if ($this->debug) Log::info("MasterAgentBL::setMasterAgent id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
			if ($this->isValid()) {
				$this->whitelabelID = $this->mMasterAgent->white_label_id;
				$this->masterAgentID = $this->mMasterAgent->id;
			} else {
				$this->mMasterAgent = null;
			}
		} catch ( Exception $e ) {
			if ($this->debug) Log::info("MasterAgentBL::setMasterAgent id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		if (! is_null($this->mMasterAgent)) {
			try {
				if (! is_null($this->mMasterAgent->default_commission_value) && $this->mMasterAgent->default_commission_type == "PERCENTAGE") {
					if ($this->mMasterAgent->default_commission_value > 100) {
						$this->mMasterAgent->default_commission_value = 100;
						$this->mMasterAgent->save();
					}
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBL::setMasterAgent id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				if (! is_null($this->mMasterAgent->default_bonus_value) && $this->mMasterAgent->default_bonus_type == "PERCENTAGE") {
					if ($this->mMasterAgent->default_bonus_value > 100) {
						$this->mMasterAgent->default_bonus_value = 100;
						$this->mMasterAgent->save();
					}
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBL::setMasterAgent id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
		}
		return $this;
	}
	
	public function setMasterAgentByUsername( $username, $active = false ) {
		try {
			$this->isInvalidParent = false;
			$this->mMasterAgent = null;
			$cache = new CacheUsernameMasterAgent( $this->whitelabelID );
			$id = $cache->getID($username);
			if (! empty($id)) {
				try {
					try {
						if ($active) {
							$this->mMasterAgent = MasterAgent::where('id', '=', $id)->where('status', '=', MasterAgent::STATUS_ACTIVE)->firstOrFail();
						} else {
							$this->mMasterAgent = MasterAgent::where('id', '=', $id)->firstOrFail();
						}
					} catch ( Exception $e ) {
						$this->mMasterAgent = null;
						if ($this->debug) Log::info("MasterAgentBL::setMasterAgentByUsername username=".$username." id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
					if ($this->isValid()) {
						if (strtolower(trim($username)) == strtolower(trim($this->mMasterAgent->username))) {
							$this->whitelabelID = $this->mMasterAgent->white_label_id;
							$this->masterAgentID = $this->mMasterAgent->id;
						} else {
							$this->mMasterAgent = null;
							$this->masterAgentID = 0;
							$cache->deleteID($username);
						}
					} else {
						$this->mMasterAgent = null;
					}
				} catch ( Exception $e ) {
					if ($this->debug) Log::info("MasterAgentBL::setMasterAgentByUsername username=".$username." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
			if (is_null($this->mMasterAgent) || (! is_null($this->mMasterAgent) && empty($this->mMasterAgent->id))) {
				if (! empty($this->whitelabelID)) {
					try {
						$this->mMasterAgent = MasterAgent::GetByUsername($this->whitelabelID, $username, $active)->firstOrFail();
					} catch ( Exception $e ) {
						$this->mMasterAgent = null;
						if ($this->debug) Log::info("MasterAgentBL::setMasterAgentByUsername username=".$username." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
					if ($this->isValid()) {
						$this->whitelabelID = $this->mMasterAgent->white_label_id;
						$this->masterAgentID = $this->mMasterAgent->master_agent_id;
						$cache->setID($username, $this->mMasterAgent->id);
					} else {
						$this->mMasterAgent = null;
					}
				}
			}
			if ($this->isValid()) {
				if ($this->debug) Log::info("MasterAgentBL::setMasterAgentByUsername CHECK PARENT MASTER AGENT username=".$username);
				if (! $this->verifyParentMasterAgent( $active )) {
					if ($this->debug) Log::info("MasterAgentBL::setAgentByUsername PARENT MASTER AGENT TDK VALID");
					$this->mMasterAgent = null;
					$this->isInvalidParent = true;
				}
			}
		} catch ( Exception $e ) {
			if ($this->debug) Log::info("MasterAgentBL::setMasterAgentByUsername username=".$username." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		if (! is_null($this->mMasterAgent)) {
			try {
				if (! is_null($this->mMasterAgent->default_commission_value) && $this->mMasterAgent->default_commission_type == "PERCENTAGE") {
					if ($this->mMasterAgent->default_commission_value > 100) {
						$this->mMasterAgent->default_commission_value = 100;
						$this->mMasterAgent->save();
					}
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBL::setMasterAgentByUsername username=".$username." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				if (! is_null($this->mMasterAgent->default_bonus_value) && $this->mMasterAgent->default_bonus_type == "PERCENTAGE") {
					if ($this->mMasterAgent->default_bonus_value > 100) {
						$this->mMasterAgent->default_bonus_value = 100;
						$this->mMasterAgent->save();
					}
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBL::setMasterAgentByUsername username=".$username." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
		}
		return $this;
	}
	
	protected function verifyParentMasterAgent( $active = true ) {
		try {
			if (! is_null($this->mMasterAgent) && ! empty($this->mMasterAgent->master_agent_parent_id)) {
				$mParent = null;
				try {
					$mParent = MasterAgent::where( 'id', '=', $this->mMasterAgent->master_agent_parent_id )->firstOrFail();
				} catch ( Exception $e ) {
					$mParent = null;
					if ($this->debug) Log::info("MasterAgentBL::verifyParentMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
				if (! is_null($mParent)) {
					if ($mParent->white_label_id != $this->whitelabelID) {
						return false;
					}
					if ($active) {
						if ($mParent->status != MasterAgent::STATUS_ACTIVE) {
							return false;
						}
					}
				} else {
					return false;
				}
				if (! is_null($mParent)) {
					try {
						if (! empty($this->mMasterAgent->master_agent_max_downline) && $this->mMasterAgent->master_agent_max_downline >= $mParent->master_agent_max_downline) {
							$this->mMasterAgent->master_agent_max_downline = (! empty($mParent->master_agent_max_downline) ? ($mParent->master_agent_max_downline - 1) : 0);
							$this->mMasterAgent->save();
						}
					} catch ( Exception $e ) {
						Log::info("MasterAgentBL::verifyParentMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBL::verifyParentMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return true;
	}
	
	public function isValid() {
		if (! is_null($this->mMasterAgent) && ! empty($this->mMasterAgent->id)) {
			if (! empty($this->whitelabelID) && $this->whitelabelID != $this->mMasterAgent->white_label_id) {
				return false;
			}
			return true;
		}
		return false;
	}
	public function isActive() {
		if ($this->isValid()) {
			return ($this->mMasterAgent->status == MasterAgent::STATUS_ACTIVE ? true : false);
		}
		return false;
	}
	
	public function getMasterAgentStatus() {
		return (! is_null($this->mMasterAgent) ? $this->mMasterAgent->status : null);
	}
	
	public function getMasterAgentModel() {
		return $this->mMasterAgent;
	}
	public function setMasterAgentModel( MasterAgent $mMasterAgent ) {
		$this->mMasterAgent = $mMasterAgent;
		$this->masterAgentID = $this->mMasterAgent->id;
		$this->whitelabelID = $this->mMasterAgent->white_label_id;
		return $this;
	}
	
	public function createNewDownline( $username, $password, &$errcode ) {
		$errcode = ApiError::ERROR_GENERAL;
		try {
			$username = strtolower(trim($username));
			if (empty($username)) {
				$errcode = ApiError::ERROR_FORMAT_USERNAME_INVALID;
				return null;
			}
			if (! UtilCommon::isUsernameValid( $username ) || strlen($username) > 50) {
				$errcode = ApiError::ERROR_FORMAT_USERNAME_INVALID;
				return null;
			}
			if (empty($password)) {
				$password = UtilCommon::randomCode( 8, false, true, false );
			} else {
				if (! UtilCommon::isPasswordValid($password)) {
					$errcode = ApiError::ERROR_FORMAT_PASSWORD_INVALID;
					return null;
				}
			}
			if (is_null($this->mWL)) {
				$this->setWhiteLabelID( $this->whitelabelID, true );
			}
			if (is_null($this->mWL)) {
				$errcode = ApiError::ERROR_WHITELABEL_INVALID;
				return null;
			}
			$parentID = 0;
			if (! is_null($this->mMasterAgent)) {
				if ($this->mMasterAgent->master_agent_max_downline <= 0) {
					$errcode = ApiError::ERROR_NOT_ALLOWED_HAS_DOWNLINE;
					return null;
				}
				$parentID = $this->mMasterAgent->id;
			} else {
				if (! empty($this->masterAgentID)) {
					$this->setMasterAgent( $this->masterAgentID );
					if (is_null($this->mMasterAgent)) {
						$errcode = ApiError::ERROR_PARENT_MASTER_AGENT_INVALID;
						return null;
					} else {
						if ($this->mMasterAgent->master_agent_max_downline <= 0) {
							$errcode = ApiError::ERROR_NOT_ALLOWED_HAS_DOWNLINE;
							return null;
						}
					}
					$parentID = $this->mMasterAgent->id;
				}
			}
			$found = false;
			try {
				$mCheck = MasterAgent::where('white_label_id','=',$this->whitelabelID)->where('username','=',$username)->select('id')->firstOrFail();
				if (! is_null($mCheck) && ! empty($mCheck->id)) {
					$found = true;
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBL::createNewDownline username=".$username." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			if ($found) {
				$errcode = ApiError::ERROR_USERNAME_ALREADY_EXIST;
				return null;
			}
			$id = (int)time();
			$mMasterAgent = new MasterAgent();
			$mMasterAgent->id = $id;
			$mMasterAgent->white_label_id = $this->whitelabelID;
			$mMasterAgent->username = $username;
			$mMasterAgent->master_agent_parent_id = $parentID;
			$mMasterAgent->master_agent_max_downline = (! is_null($this->mMasterAgent) ? ($this->mMasterAgent->master_agent_max_downline - 1) : 0);
			$mMasterAgent->password_enc = $password;
			$mMasterAgent->password_type = MasterAgent::PASSWORD_TYPE_PLAIN;
			$mMasterAgent->status = MasterAgent::STATUS_REGISTERING;
			$mMasterAgent->paymode = BalanceType::PAYMODE_PREPAID;
			$mMasterAgent->postpaid_balance_value_max = $this->mWL->postpaid_balance_value_max;
			$mMasterAgent->prepaid_balance_value_min = $this->mWL->prepaid_balance_value_min;
			$mMasterAgent->default_commission_used = 'N';
			$mMasterAgent->default_commission_type = $this->mWL->master_agent_default_commission_type;
			$mMasterAgent->default_commission_value = $this->mWL->master_agent_default_commission_value;
			$mMasterAgent->default_bonus_used = 'N';
			$mMasterAgent->default_bonus_type = $this->mWL->master_agent_default_bonus_type;
			$mMasterAgent->default_bonus_value = $this->mWL->master_agent_default_bonus_value;
			$mMasterAgent->agent_auto_approve = 'N';
			$mMasterAgent->agent_max_terminal_default = $this->mWL->agent_max_terminal_default;
			$mMasterAgent->account_buffer = null;
			$mMasterAgent->save();
			if (! empty($id)) {
				$mMasterAgent = MasterAgent::where( 'id', '=', $id )->firstOrFail();
				try {
					$objLogicPwd = new PasswordMasterAgent( $this->whitelabelID );
					$mMasterAgent = $objLogicPwd->setPasswordByModel( $mMasterAgent, $mMasterAgent->password_enc );
					$cache = new CacheUsernameMasterAgent( $this->whitelabelID );
					$cache->setID( $mMasterAgent->username, $mMasterAgent->id );
				} catch ( Exception $e ) {
					Log::info("MasterAgentBL::createNewDownline exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
				$errcode = ApiError::SUCCESS;
				return $mMasterAgent;
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBL::createNewDownline exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return null;
	}
	
	public function updateSetting( $arrVerified ) {
		try {
			if (is_null($this->mMasterAgent) || is_null($arrVerified) && ! is_array($arrVerified)) {
				return false;
			}
			if ($this->debug) Log::info("ApiMasterAgentBL::updateSetting arr_verified=".json_encode($arrVerified));
			if (isset($arrVerified['postpaid_balance_value_max'])) {
				$this->mMasterAgent->postpaid_balance_value_max = $arrVerified['postpaid_balance_value_max'];
			}
			if (isset($arrVerified['prepaid_balance_value_min'])) {
				$this->mMasterAgent->prepaid_balance_value_min = $arrVerified['prepaid_balance_value_min'];
			}
			if (isset($arrVerified['default_commission_used'])) {
				$this->mMasterAgent->default_commission_used = $arrVerified['default_commission_used'];
			}
			if (isset($arrVerified['default_commission_type'])) {
				$this->mMasterAgent->default_commission_type = $arrVerified['default_commission_type'];
			}
			if (isset($arrVerified['default_commission_value'])) {
				$this->mMasterAgent->default_commission_value = $arrVerified['default_commission_value'];
			}
			if (isset($arrVerified['default_bonus_used'])) {
				$this->mMasterAgent->default_bonus_used = $arrVerified['default_bonus_used'];
			}
			if (isset($arrVerified['default_bonus_type'])) {
				$this->mMasterAgent->default_bonus_type = $arrVerified['default_bonus_type'];
			}
			if (isset($arrVerified['default_bonus_value'])) {
				$this->mMasterAgent->default_bonus_value = $arrVerified['default_bonus_value'];
			}
			if (isset($arrVerified['master_agent_max_downline'])) {
				$this->mMasterAgent->master_agent_max_downline = $arrVerified['master_agent_max_downline'];
			}
			if (isset($arrVerified['agent_auto_approve'])) {
				$this->mMasterAgent->agent_auto_approve = $arrVerified['agent_auto_approve'];
			}
			if (isset($arrVerified['agent_max_terminal_default'])) {
				$this->mMasterAgent->agent_max_terminal_default = $arrVerified['agent_max_terminal_default'];
			}
			if (isset($arrVerified['notif_login'])) {
				$this->mMasterAgent->notif_login = $arrVerified['notif_login'];
			}
			if (isset($arrVerified['notif_trx'])) {
				$this->mMasterAgent->notif_trx = $arrVerified['notif_trx'];
			}
			if (isset($arrVerified['notif_balance'])) {
				$this->mMasterAgent->notif_balance = $arrVerified['notif_balance'];
			}
			$this->mMasterAgent->save();
			if (isset($arrVerified['master_agent_max_downline'])) {
				// update semua setting master_agent_max_downline dibawah downline dia
				MasterAgentDownlineBL::putQueueUpdateMaxDownline( $this->mMasterAgent->id, $this->apiversion );
			}
			if (isset($arrVerified['downline_commission'])) {
				MasterAgentDownlineBL::editMasterAgentCommissionDownline( $this->mMasterAgent->id, $arrVerified['downline_commission'] );
			}
			if (isset($arrVerified['downline_bonus'])) {
				MasterAgentDownlineBL::editMasterAgentBonusDownline( $this->mMasterAgent->id, $arrVerified['downline_bonus'] );
			}
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentBL::updateSetting exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function getListMasterAgent( $page = 1, $max = 100, $maxPlusOne = true, $active = true, $level = 0 ) {
		$mList = new Collection();
		try {
			if (empty($this->whitelabelID)) {
				return $mList;
			}
			$parentID = -1;
			if ($level > 0) {
				$parentID = self::getArrayMasterAgentParentIDByLevel( $this->whitelabelID, $level );
			}
			if ($this->debug) {
				try {
					$sql = MasterAgent::ListByWhiteLabel( $this->whitelabelID, $page, $max, $maxPlusOne, $active, $parentID )->toSql();
					$bindings = MasterAgent::ListByWhiteLabel( $this->whitelabelID, $page, $max, $maxPlusOne, $active, $parentID )->getBindings();
					$sql = UtilCommon::queryReplace($sql, $bindings);
					Log::info("MasterAgentBL::getListMasterAgent SQL = ". $sql);
				} catch ( Exception $e ) {
					Log::info("MasterAgentBL::getListMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
			$mList = MasterAgent::ListByWhiteLabel( $this->whitelabelID, $page, $max, $maxPlusOne, $active, $parentID )->get();
		} catch ( Exception $e ) {
			Log::info("MasterAgentBL::getListMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $mList;
	}
	
	public function searchMasterAgent( $search, $level = 0, $page = 1, $max = 100, $maxPlusOne = true, $active = true ) {
		$mList = new Collection();
		try {
			$search = trim($search);
			if (empty($this->whitelabelID) || empty($search)) {
				return $mList;
			}
			$arrParentID = self::getArrayMasterAgentParentIDByLevel( $this->whitelabelID, $level );
			if ($this->debug) {
				try {
					$sql = MasterAgent::SearchIncludeProfile( $search, $this->whitelabelID, $arrParentID, $page, $max, $maxPlusOne, $active )->toSql();
					$bindings = MasterAgent::SearchIncludeProfile( $search, $this->whitelabelID, $arrParentID, $page, $max, $maxPlusOne, $active )->getBindings();
					$sql = UtilCommon::queryReplace($sql, $bindings);
					Log::info("MasterAgentBL::searchMasterAgent SQL = ". $sql);
				} catch ( Exception $e ) {
					Log::info("MasterAgentBL::searchMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
			$mList = MasterAgent::SearchIncludeProfile( $search, $this->whitelabelID, $arrParentID, $page, $max, $maxPlusOne, $active )->get();
		} catch ( Exception $e ) {
			Log::info("MasterAgentBL::searchMasterAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $mList;
	}
	
	public static function getArrayMasterAgentParentIDByLevel( $whitelabelID, $findLevel, $status = "ACTIVE" ) {
		$arrParentID = array();
		try {
			self::loopMasterAgentParentIDByLevel( $arrParentID, $whitelabelID, $findLevel, $status, 1, 0 );
		} catch ( Exception $e ) {
			Log::info("MasterAgentBL::getArrayMasterAgentParentIDByLevel exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $arrParentID;
	}
	
	private static function loopMasterAgentParentIDByLevel( &$arrParentID, $whitelabelID, $findLevel, $status, $curLevel, $curParentID ) {
		try {
			if (! is_array($arrParentID)) {
				$arrParentID = array();
			}
			if ($findLevel > 0) {
				if ($findLevel == 1) {
					$arrParentID = array();
					$arrParentID[] = 0;
				} else {
					if ($curLevel < 1) {
						$curLevel = 1;
					}
					if ($curLevel <= $findLevel) {
						if ($curLevel >= 2) {
							if ($curLevel == 2) {
								$curParentID = 0;
							}
							$mList = new Collection();
							if (! empty($status)) {
								$mList = MasterAgent::where('white_label_id','=',$whitelabelID )->where('master_agent_parent_id','=',$curParentID)->where('status','=',$status)->select('id','white_label_id','master_agent_parent_id','username','status')->get();
							} else {
								$mList = MasterAgent::where('white_label_id','=',$whitelabelID )->where('master_agent_parent_id','=',$curParentID)->select('id','white_label_id','master_agent_parent_id','username','status')->get();
							}
							foreach ( $mList as $row ) {
								// Log::info("MasterAgentBL::loopMasterAgentParentIDByLevel findLevel=".$findLevel." curlevel=".$curLevel." ".($findLevel==$curLevel?"FOUND":"")." curParentID=".$curParentID." data=".$row->toJson());
								if ($curLevel == $findLevel) {
									$arrParentID[] = $row->id;
								} else {
									self::loopMasterAgentParentIDByLevel( $arrParentID, $whitelabelID, $findLevel, $status, $curLevel + 1, $row->id );
								}
							}
						} else {
							self::loopMasterAgentParentIDByLevel( $arrParentID, $whitelabelID, $findLevel, $status, $curLevel + 1, 0 );
						}
					} 
				}
			} else {
				$arrParentID = array();
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBL::loopMasterAgentParentIDByLevel exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
	public static function isValidByRangeID( $id, $active = true, $whitelabelID = 0 ) {
		try {
			$mRow = ByRange::where('id','=',$id)->firstOrFail();
			if (! is_null($mRow) && ! empty($mRow->id)) {
				if ($active) {
					if ($mRow->active != 'Y') {
						return false;
					}
				}
				if (! empty($whitelabelID) && $whitelabelID != $mRow->white_label_id) {
					return false;
				}
				return true;
			}
		} catch ( Exception $e ) {
			if ( boolval(Config::get('app.debug')) ) Log::info("MasterAgentBL::isValidByRangeID id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public static function getMasterAgentModelByID( $id ) {
		try {
			$mModel = MasterAgent::where('id','=',$id)->firstOrFail();
			return $mModel;
		} catch ( Exception $e ) {
			if ( boolval(Config::get('app.debug')) ) Log::info("MasterAgentBL::getMasterAgentModelByID id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return null;
	}
	
	public static function getMasterAgentCommissionDownlineModel( $parentID, $id = 0 ) {
		try {
			$mMtrCmDownline = null;
			if (! empty($id)) {
				$mMtrCmDownline = MasterAgentCommissionDownline::where('id','=',$id)->firstOrFail();
			} else if (! empty($parentID)) {
				$mMtrCmDownline = MasterAgentCommissionDownline::where('master_agent_parent_id','=',$parentID)->firstOrFail();
			}
			if (! is_null($mMtrCmDownline) && ! empty($mMtrCmDownline->id)) {
				return $mMtrCmDownline;
			}
		} catch ( Exception $e ) {
			if ( boolval(Config::get('app.debug')) ) Log::info("MasterAgentBL::getMasterAgentCommissionDownlineModel parent_id=".$parentID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return null;
	}
	
	public static function editMasterAgentCommissionDownline( $parentID, $arrData ) {
		try {
			if (empty($parentID) || is_null($arrData) || ! is_array($arrData) || (is_array($arrData) && count($arrData) == 0)) {
				return false;
			}
			$mRow = self::getMasterAgentCommissionDownlineModel( $parentID );
			if (is_null($mRow)) {
				$mRow = new MasterAgentCommissionDownline();
				$mRow->master_agent_parent_id = $parentID;
			}
			if (isset($arrData['default_commission_used'])) {
				$mRow->active = $arrData['default_commission_used'];
			}
			if (isset($arrData['default_commission_type'])) {
				$mRow->default_commission_type = $arrData['default_commission_type'];
			}
			if (isset($arrData['default_commission_value'])) {
				$mRow->default_commission_value = $arrData['default_commission_value'];
			}
			$mRow->save();
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentBL::editMasterAgentCommissionDownline parent_id=".$parentID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public static function getMasterAgentBonusDownlineModel( $parentID, $id = 0 ) {
		try {
			$mMtrBonusDownline = null;
			if (! empty($id)) {
				$mMtrBonusDownline = MasterAgentBonusDownline::where('id','=',$id)->firstOrFail();
			} else if (! empty($parentID)) {
				$mMtrBonusDownline = MasterAgentBonusDownline::where('master_agent_parent_id','=',$parentID)->firstOrFail();
			}
			if (! is_null($mMtrBonusDownline) && ! empty($mMtrBonusDownline->id)) {
				return $mMtrBonusDownline;
			}
		} catch ( Exception $e ) {
			if ( boolval(Config::get('app.debug')) ) Log::info("MasterAgentBL::getMasterAgentBonusDownlineModel parent_id=".$parentID." id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return null;
	}
	
	public static function editMasterAgentBonusDownline( $parentID, $arrData ) {
		try {
			if (empty($parentID) || is_null($arrData) || ! is_array($arrData) || (is_array($arrData) && count($arrData) == 0)) {
				return false;
			}
			$mRow = self::getMasterAgentBonusDownlineModel( $parentID );
			if (is_null($mRow)) {
				$mRow = new MasterAgentBonusDownline();
				$mRow->master_agent_parent_id = $parentID;
			}
			if (isset($arrData['default_bonus_used'])) {
				$mRow->active = $arrData['default_bonus_used'];
			}
			if (isset($arrData['default_bonus_type'])) {
				$mRow->default_bonus_type = $arrData['default_bonus_type'];
			}
			if (isset($arrData['default_bonus_value'])) {
				$mRow->default_bonus_value = $arrData['default_bonus_value'];
			}
			$mRow->save();
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentBL::editMasterAgentBonusDownline parent_id=".$parentID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public static function getWhitelabelCommissionLevelModel( $whitelabelID, $level, $id = 0 ) {
		try {
			$mMtrCmLevel = null;
			if (! empty($id)) {
				$mMtrCmLevel = WhitelabelCommissionLevel::where('id','=',$id)->firstOrFail();
			} else {
				$mMtrCmLevel = WhitelabelCommissionLevel::where('white_label_id','=',$whitelabelID)->where('master_agent_level','=',$level)->firstOrFail();
			}
			if (! is_null($mMtrCmLevel) && ! empty($mMtrCmLevel->id)) {
				return $mMtrCmLevel;
			}
		} catch ( Exception $e ) {
			if ( boolval(Config::get('app.debug')) ) Log::info("MasterAgentBL::getWhitelabelCommissionLevelModel white_label_id=".$whitelabelID." level=".$level." id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return null;
	}
	
	public static function getWhitelabelBonusLevelModel( $whitelabelID, $level, $id = 0 ) {
		try {
			$mMtrBonusLevel = null;
			if (! empty($id)) {
				$mMtrBonusLevel = WhitelabelBonusLevel::where('id','=',$id)->firstOrFail();
			} else {
				$mMtrBonusLevel = WhitelabelBonusLevel::where('white_label_id','=',$whitelabelID)->where('master_agent_level','=',$level)->firstOrFail();
			}
			if (! is_null($mMtrBonusLevel) && ! empty($mMtrBonusLevel->id)) {
				return $mMtrBonusLevel;
			}
		} catch ( Exception $e ) {
			if ( boolval(Config::get('app.debug')) ) Log::info("MasterAgentBL::getWhitelabelBonusLevelModel white_label_id=".$whitelabelID." level=".$level." id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return null;
	}
	
}