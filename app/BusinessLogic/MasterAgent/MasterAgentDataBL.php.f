<?php
namespace App\BusinessLogic\MasterAgent;

use Log;
use Config;
use Exception;
use App\Core\Util\UtilCommon;
use App\Models\Api\ApiError;
use App\Models\Database\HistoryTrxMasterAgent;
use App\Models\Database\HistoryTrxAgent;
use App\Models\Database\Agent;
use App\Models\Database\AgentProfile;
use App\Models\Database\MasterAgent;
use App\Models\Database\AgentBalances;
use App\Models\Database\MasterAgentProfile;
use App\BusinessLogic\MasterAgent\MasterAgentDownlineBL;

class MasterAgentDataBL 
{
	private $whitelabelID = 0;
	private $masterAgentID = 0;
	private $version = "1.0";

	const PREPAID_BALANCE_TYPE_ID = 3000;
	const POSTPAID_BALANCE_TYPE_ID = 1000;

	public function __construct( $whitelabelID = 0, $masterAgentID = 0, $version = "1.0" ) {
		Log::info('MasterAgentDataBL->__construct: whitelabel #'.$whitelabelID);
		Log::info('MasterAgentDataBL->__construct: master agent #'.$masterAgentID);
		$this->whitelabelID = $whitelabelID;
		$this->masterAgentID = $masterAgentID;
		$this->version = $version;
	}

	public function getAgentList( $masterAgentStatus = 'ACTIVE', $agentStatus = 'ACTIVE' ) {
		$qrAgentList = null;
		$qrMasterAgentChildList = null;
		$directAgentList = array();
		$downlineAgentList = array();
		$profile = array(
			"profile_type" => "",
			"fullname" => "",
			"description" => "",
			"photo_filename" => "",
			"email" => "",
			"phone_number" => "",
			"contact_number" => "",
			"fax_number" => "",
			"place_birth" => "",
			"birth_date" => "",
			"gender" => "",
			"npwp_number" => "",
			"province_name" => "",
			"city_name" => "",
			"district_name" => "",
			"village_name" => "",
			"address" => "",
			"postal_code" => "",
			"company_name" => "",
			"department" => "",
			"contact_person" => "",
			"branch_office_level" => "",
			"id_card_type" => "",
			"id_card_number" => ""
		);
		try {
			//Get master agent profile
			$maReg = MasterAgent::where('id',$this->masterAgentID)->first();
			if (!is_null($maReg)) {
				$profile['fullname'] = $maReg["username"];
			}
			$profileMa = MasterAgentProfile::where('master_agent_id',$this->masterAgentID)->first();
			if (!is_null($profileMa)) {
				$profile["profile_type"] = $profileMa["profile_type"];
				$profile["fullname"] = $profileMa["fullname"];
				$profile["description"] = $profileMa["description"];
				$profile["photo_filename"] = $profileMa["photo_filename"];
				$profile["email"] = $profileMa["email"];
				$profile["phone_number"] =  $profileMa["phone_number"];
				$profile["contact_number"] = $profileMa["contact_number"];
				$profile["fax_number"] = $profileMa["fax_number"];
				$profile["place_birth"] = $profileMa["place_birth"];
				$profile["birth_date"] = $profileMa["birth_date"];
				$profile["gender"] = $profileMa["gender"];
				$profile["npwp_number"] = $profileMa["npwp_number"];
				$profile["province_name"] = $profileMa["province_name"];
				$profile["city_name"] = $profileMa["city_name"];
				$profile["district_name"] = $profileMa["district_name"];
				$profile["village_name"] = $profileMa["village_name"];
				$profile["address"] = $profileMa["address"];
				$profile["postal_code"] = $profileMa["postal_code"];
				$profile["company_name"] = $profileMa["company_name"];
				$profile["department"] = $profileMa["department"];
				$profile["contact_person"] = $profileMa["contact_person"];
				$profile["branch_office_level"] = $profileMa["branch_office_level"];
				$profile["id_card_type"] = $profileMa["id_card_type"];
				$profile["id_card_number"] = $profileMa["id_card_number"];				 
			}
			//Get direct agent list
			$maAgentList = $this->getMAAgentList($this->masterAgentID, $agentStatus);
			foreach ($maAgentList as $agent) {
				$agentBalance = $this->getAgentBalance($agent->agent_id, $agent->paymode);
				$directAgentList[] = array(
						'agent' => $agent,
						'balance' => $agentBalance
					);

			}			
			//Get downline master agent
			$maDownlineBL = new MasterAgentDownlineBL( $this->whitelabelID, $this->masterAgentID );
			$maDownlineBL->setMasterAgent($this->masterAgentID);
			$arrMAId = $maDownlineBL->getArrayMasterAgentDownlineAll( (int)$this->masterAgentID);
			Log::info('MasterAgentDataBL->getAgentList: downline='.json_encode($arrMAId));
			Log::info('MasterAgentDataBL->getAgentList: master agent #'.$this->masterAgentID.' has '.
				count($arrMAId)." downline");
			foreach ($arrMAId as $maId) {
				//Get detail master agent
				$ma = $this->getDetailMA($maId, $masterAgentStatus);
				//Get agent list of selected maId
				$maAgentList = $this->getMAAgentList($maId, $agentStatus);
				//Get balance foreach agent 
				$agentList = [];
				foreach ($maAgentList as $agent) {
					$agentBalance = $this->getAgentBalance($agent->agent_id, $agent->paymode);
					$agentList[] = array(
							'agent' => $agent,
							'balance' => $agentBalance
						);

				}
				$downlineAgentList[] = array(
					"child_master_agent" => $ma,
					"agent_list" => $agentList
				); 
			}
		} catch (Exception $e) {
			Log::error("MasterAgentDataBL->getDownloadAgentList: exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return array (
			"profile" => $profile,
			"direct_agent_list" => $directAgentList,
			"downline_master_agent_list" => $downlineAgentList
		);				
	}

	private function getDetailMA($maId, $maStatus) {
		$arrDetailMa = array(
			"master_agent_id" => $maId,
			"status" => $maStatus,
			"paymode" => "",
			"profile_type" => "",
			"fullname" => "",
			"description" => "",
			"photo_filename" => "",
			"email" => "",
			"phone_number" => "",
			"contact_number" => "",
			"fax_number" => "",
			"place_birth" => "",
			"birth_date" => "",
			"gender" => "",
			"npwp_number" => "",
			"province_name" => "",
			"city_name" => "",
			"district_name" => "",
			"village_name" => "",
			"address" => "",
			"postal_code" => "",
			"company_name" => "",
			"department" => "",
			"contact_person" => "",
			"branch_office_level" => "",
			"id_card_type" => "",
			"id_card_number" => ""
		);
		$ma = MasterAgent::where('id',$maId)->first();
		$profileMa = MasterAgentProfile::where('master_agent_id',$maId)->first();
		if (!is_null($ma)) {
			$arrDetailMa["status"] = $ma["status"];
			$arrDetailMa["fullname"] = $ma["username"];
		}
		if (!is_null($profileMa)) {
			$arrDetailMa["profile_type"] = $profileMa["profile_type"];
			$arrDetailMa["fullname"] = $profileMa["fullname"];
			$arrDetailMa["description"] = $profileMa["description"];
			$arrDetailMa["photo_filename"] = $profileMa["photo_filename"];
			$arrDetailMa["email"] = $profileMa["email"];
			$arrDetailMa["phone_number"] =  $profileMa["phone_number"];
			$arrDetailMa["contact_number"] = $profileMa["contact_number"];
			$arrDetailMa["fax_number"] = $profileMa["fax_number"];
			$arrDetailMa["place_birth"] = $profileMa["place_birth"];
			$arrDetailMa["birth_date"] = $profileMa["birth_date"];
			$arrDetailMa["gender"] = $profileMa["gender"];
			$arrDetailMa["npwp_number"] = $profileMa["npwp_number"];
			$arrDetailMa["province_name"] = $profileMa["province_name"];
			$arrDetailMa["city_name"] = $profileMa["city_name"];
			$arrDetailMa["district_name"] = $profileMa["district_name"];
			$arrDetailMa["village_name"] = $profileMa["village_name"];
			$arrDetailMa["address"] = $profileMa["address"];
			$arrDetailMa["postal_code"] = $profileMa["postal_code"];
			$arrDetailMa["company_name"] = $profileMa["company_name"];
			$arrDetailMa["department"] = $profileMa["department"];
			$arrDetailMa["contact_person"] = $profileMa["contact_person"];
			$arrDetailMa["branch_office_level"] = $profileMa["branch_office_level"];
			$arrDetailMa["id_card_type"] = $profileMa["id_card_type"];
			$arrDetailMa["id_card_number"] = $profileMa["id_card_number"];
		}
		/*
		$ma = 'befintech.master_agent';
		$map = 'befintech.master_agent_profile';
		$ma = MasterAgent::where($ma.'.id',$maId)
			->where('status',$maStatus)
			->leftJoin($map,$ma.'.id','=',$map.'.master_agent_id')
			->selectRaw(
				$ma.'.id master_agent_id,'.
				$ma.'.status,'.
				$ma.'.paymode,'.
				$map.'.profile_type,'.
				$map.'.fullname,'.
				$map.'.description,'.
				$map.'.photo_filename,'.
				$map.'.email,'.
				$map.'.phone_number,'.
				$map.'.contact_number,'.
				$map.'.fax_number,'.
				$map.'.place_birth,'.
				$map.'.birth_date,'.
				$map.'.gender,'.
				$map.'.npwp_number,'.
				$map.'.province_name,'.
				$map.'.city_name,'.
				$map.'.district_name,'.
				$map.'.village_name,'.
				$map.'.address,'.
				$map.'.postal_code,'.
				$map.'.company_name,'.
				$map.'.department,'.
				$map.'.contact_person,'.
				$map.'.branch_office_level,'.
				$map.'.id_card_type,'.
				$map.'.id_card_number'
				)
			->get();
		*/	
		return $arrDetailMa;		
	}

	private function getMAAgentList($maId, $agentStatus) {
		$agentInfo = array(
			"agent_id" => "",
			"status" => "",
			"paymode" => "",
			"fullname" => "",
			"description" => "",
			"photo_filename" => "",
			"email" => "",
			"phone_number" => "",
			"contact_number" => "",
			"fax_number" => "",
			"place_birth" => "",
			"birth_date" => "",
			"gender" => "",
			"npwp_number" => "",
			"province_name" => "",
			"city_name" => "",
			"district_name" => "",
			"village_name" => "",
			"address" => "",
			"postal_code" => "",
			"contact_person" => "",
			"id_card_type" => "",
			"id_card_number" => ""
		);
		$arrAgent = array(); 

		$agentList = Agent::where('master_agent_id',$maId)->get();
		foreach ($agentList as $agent) {
			$selectedAgentInfo = $agentInfo;
			$selectedAgentInfo["agent_id"] = $agent->id;
			$selectedAgentInfo["status"] = $agent->status;
			$selectedAgentInfo["fullname"] = $agent->username;
			$agentProfile = AgentProfile::where('agent_id',$agent->id)->first();
			if (!is_null($agentProfile)) {
				$selectedAgentInfo["fullname"] = $agentProfile["fullname"];
				$selectedAgentInfo["description"] = $agentProfile["description"];
				$selectedAgentInfo["photo_filename"] = $agentProfile["photo_filename"];
				$selectedAgentInfo["email"] = $agentProfile["email"];
				$selectedAgentInfo["phone_number"] = $agentProfile["phone_number"];
				$selectedAgentInfo["contact_number"] = $agentProfile["contact_number"];
				$selectedAgentInfo["fax_number"] = $agentProfile["fax_number"];
				$selectedAgentInfo["place_birth"] = $agentProfile["place_birth"];
				$selectedAgentInfo["birth_date"] = $agentProfile["birth_date"];
				$selectedAgentInfo["gender"] = $agentProfile["gender"];
				$selectedAgentInfo["npwp_number"] = $agentProfile["npwp_number"];
				$selectedAgentInfo["province_name"] = $agentProfile["province_name"];
				$selectedAgentInfo["city_name"] = $agentProfile["city_name"];
				$selectedAgentInfo["district_name"] = $agentProfile["district_name"];
				$selectedAgentInfo["village_name"] = $agentProfile["village_name"];
				$selectedAgentInfo["postal_code"] = $agentProfile["postal_code"];
				$selectedAgentInfo["contact_person"] = $agentProfile["contact_person"];
				$selectedAgentInfo["id_card_type"] = $agentProfile["id_card_type"];
				$selectedAgentInfo["id_card_number"] = $agentProfile["id_card_number"];
			}
			$arrAgent[] = $selectedAgentInfo;
		}

		/*
		$a = 'befintech.agent';
		$ap = 'befintech.agent_profile';
		$agentList = Agent::where('master_agent_id',$maId)
			->where('status',$agentStatus)
			->leftJoin($ap, $a.'.id','=',$ap.'.agent_id')
			->selectRaw(
				$a.'.id agent_id,'.
				$a.'.status,'.
				$a.'.paymode,'.
				$ap.'.fullname,'.
				$ap.'.description,'.
				$ap.'.photo_filename,'.
				$ap.'.email,'.
				$ap.'.phone_number,'.
				$ap.'.contact_number,'.
				$ap.'.fax_number,'.
				$ap.'.place_birth,'.
				$ap.'.birth_date,'.
				$ap.'.gender,'.
				$ap.'.npwp_number,'.
				$ap.'.province_name,'.
				$ap.'.city_name,'.
				$ap.'.district_name,'.
				$ap.'.village_name,'.
				$ap.'.address,'.
				$ap.'.postal_code,'.
				$ap.'.contact_person,'.
				$ap.'.id_card_type,'.
				$ap.'.id_card_number'								
				)
			->get();
		Log::info('MasterAgentDataBL->getMAAgentList: ma #'.$maId.'  has '.count($agentList).' agents');
		*/	
		if (count($agentList)>0) {
			Log::info('MasterAgentDataBL->getMAAgentList: sample agent='.json_encode($agentList[0]));
		}
		return $arrAgent;		
	}

	public function getAgentLatestTrx( $n = 10 ) {
		$agentList = array();
		$agentWithTrxList = array();
		$ma = 'befintech.master_agent';
		$map = 'befintech.master_agent_profile';
		$a = 'befintech.agent';
		$ap = 'befintech.agent_profile';
		$profile = null;

		try {
			//Get master agent profile
			$profile = MasterAgentProfile::where('master_agent_id',$this->masterAgentID)->first();
			$maDownlineBL = new MasterAgentDownlineBL( $this->whitelabelID, $this->masterAgentID );
			$arrMAId = $maDownlineBL->getArrayMasterAgentDownlineAll( $this->masterAgentID );
			$arrMAId[] = $this->masterAgentID;
			foreach ($arrMAId as $maId) {
				$agentList = $this->getMAAgentList($maId, 'ACTIVE');
				foreach ($agentList as $agent) {
					$trxList = HistoryTrxAgent::where('agent_id',$agent->agent_id)
						->where('trx_mode','CASHOUT')
						->whereNotNull('trx_product_id')
						->orderBy('id', 'desc')->take($n)->get();
					array_push($agentWithTrxList,
						array(
							"agent" => $agent,
							"agent_trx" => $trxList
						)
					);
				}
			}
		} catch (Exception $e) {
			Log::error("MasterAgentDataBL->getAgentLatestTrx: exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return array(
			"profile" => $profile,
			"agent_with_trx" => $agentWithTrxList
		);
	}

	private function getAgentBalance($agentID, $paymode) {
		//Default paymode is prepaid 
		$balanceTypeId = self::PREPAID_BALANCE_TYPE_ID;
		$balanceValue = 0;
		try {
			if ($paymode == 'POSTPAID') {
				$balanceTypeId = self::POSTPAID_BALANCE_TYPE_ID;
			}
			$qrAgentBalances = AgentBalances::where('agent_id',$agentID)
				->where('balance_type_id',$balanceTypeId)->first();
			if ( ! is_null($qrAgentBalances) ) {
				$balanceValue = $qrAgentBalances->balance_value;
			}
		} catch(Exception $e) {
			Log::error("MasterAgentDataBL->getAgentBalance: exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());			
		}
		return $balanceValue;
	}

} // End of class
