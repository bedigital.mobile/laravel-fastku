<?php
namespace App\BusinessLogic\MasterAgent;

use Log;
use Config;
use Exception;
use DB;
use Illuminate\Database\Eloquent\Collection;
use App\ApiLogic\ApiDefinition;
use App\BusinessLogic\MasterAgent\MasterAgentBL;
use App\Core\Util\UtilCommon;
use App\Jobs\MasterAgent\UpdateMasterAgentMaxDownlineJob;
use App\Models\Database\MasterAgent;

class MasterAgentDownlineBL extends MasterAgentBL
{
	private $isDownlineInvalid = false;
	
	public function __construct( $whitelabelID, $masterAgentID = 0, $apiversion = 1 ) {
		parent::__construct( $whitelabelID, $masterAgentID, $apiversion );
	}
	
	public function isDownlineMasterAgentInvalid() {
		return $this->isDownlineInvalid;
	}
	
	private function checkInMyDownline( $parentID, $findDownlineMasterAgentID, $downlineLevel, $maxLevel = 100, $active = true ) {
		$found = false;
		try {
			if ($this->debug) Log::info("MasterAgentDownlineBL::checkInMyDownline parent_id=".$parentID." find=".$findDownlineMasterAgentID." level=".$downlineLevel." max_level=".$maxLevel);
			try {
				$mMA = MasterAgent::where( 'id', '=', $findDownlineMasterAgentID )->where( 'master_agent_parent_id', '=', $parentID )->firstOrFail();
				if (! is_null($mMA) && ! empty($mMA->id)) {
					$found = true;
					Log::info("MasterAgentDownlineBL::checkInMyDownline found=TRUE  mMA->white_label_id=".$mMA->white_label_id." this->whitelabelID=".$this->whitelabelID);
					if ($mMA->white_label_id == $this->whitelabelID && ((! $active) || ($active && $mMA->status == MasterAgent::STATUS_ACTIVE))) {
						return $found;
					}
				}
			} catch ( Exception $e ) {
				if ($this->debug) Log::info("MasterAgentDownlineBL::checkInMyDownline #1 exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			if (! $found) {
				$downlineLevel++;
				if ($this->debug) Log::info("MasterAgentDownlineBL::checkInMyDownline downlineLevel=".$downlineLevel." maxLevel=".$maxLevel);
				if ($downlineLevel < $maxLevel) {
					$mList = MasterAgent::where( 'master_agent_parent_id', '=', $parentID )->select( 'id','white_label_id','master_agent_max_downline','status')->orderBy('id','ASC')->get();
					foreach ($mList as $row) {
						try {
							if ($row->white_label_id == $this->whitelabelID && ((! $active) || ($active && $row->status == MasterAgent::STATUS_ACTIVE))) {
								$found = $this->checkInMyDownline( $row->id, $findDownlineMasterAgentID, $downlineLevel, $maxLevel, $active);
								if ($found) {
									return $found;
								}
							}
						} catch ( Exception $e ) {
							Log::info("MasterAgentDownlineBL::checkInMyDownline #2 exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
						}
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentDownlineBL::checkInMyDownline #3 exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $found;
	}
	
	public function isMyDownline( $findDownlineMasterAgentID, $active = true ) {
		try {
			if (! is_null($this->mMasterAgent) && ! empty($this->mMasterAgent->id)) {
				$this->masterAgentID = $this->mMasterAgent->id;
				$this->whitelabelID = $this->mMasterAgent->white_label_id;
			}
			if (empty($this->masterAgentID)) {
				return false;
			}
			if (empty($this->whitelabelID) && ! is_null($this->mWL)) {
				$this->whitelabelID = $this->mWL->id;
			}
			$maxLevel = 10; // SEMENTARA
			$downlineLevel = 1;
			return $this->checkInMyDownline( $this->masterAgentID, $findDownlineMasterAgentID, $downlineLevel, $maxLevel, $active );
		} catch ( Exception $e ) {
			Log::info("MasterAgentDownlineBL::getListMasterAgentDownline exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function getListMasterAgentDownline( $parentMasterAgentID = 0, $useProfile = false, $page = 1, $max = 100, $maxPlusOne = true, $active = true, $status = "" ) {
		$mList = new Collection();
		try {
			$this->isDownlineInvalid = false;
			if (! $this->initModelMasterAgentWhiteLabel()) {
				return $mList;
			}
			if (! is_null($this->mMasterAgent) && ! empty($this->mMasterAgent->id)) {
				$this->masterAgentID = $this->mMasterAgent->id;
				$this->whitelabelID = $this->mMasterAgent->white_label_id;
			}
			$parentID = 0;
			if (! empty($parentMasterAgentID) && $parentMasterAgentID != $this->masterAgentID) {
				if (! $this->isMyDownline( $parentMasterAgentID, false )) {
					$this->isDownlineInvalid = true;
					return $mList;
				}
				$parentID = $parentMasterAgentID;
			} else {
				$parentID = $this->masterAgentID;
			}
			if ($this->debug) {
				try {
					$sql = MasterAgent::ListDownline( $this->whitelabelID, $parentID, $useProfile, $page, $max, $maxPlusOne, $active, $status )->toSql();
					$bindings = MasterAgent::ListDownline( $this->whitelabelID, $parentID, $useProfile, $page, $max, $maxPlusOne, $active, $status )->getBindings();
					$sql = UtilCommon::queryReplace($sql, $bindings);
					Log::info("MasterAgentDownlineBL::getListMasterAgentDownline SQL = ". $sql);
				} catch ( Exception $e ) {
					Log::info("MasterAgentDownlineBL::getListMasterAgentDownline exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
			$mList = MasterAgent::ListDownline( $this->whitelabelID, $parentID, $useProfile, $page, $max, $maxPlusOne, $active, $status )->get();
		} catch ( Exception $e ) {
			Log::info("MasterAgentDownlineBL::getListMasterAgentDownline exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $mList;
	}
	
	public function getArrayMasterAgentDownlineAll( $parentMasterAgentID = 0, $active = false, $status = "" ) {
		$arrID = array();
		try {
			$this->isDownlineInvalid = false;
			if (! $this->initModelMasterAgentWhiteLabel()) {
				return $arrID;
			}
			if (! is_null($this->mMasterAgent) && ! empty($this->mMasterAgent->id)) {
				$this->masterAgentID = $this->mMasterAgent->id;
				$this->whitelabelID = $this->mMasterAgent->white_label_id;
			}
			$parentID = 0;
			if (! empty($parentMasterAgentID) && $parentMasterAgentID != $this->masterAgentID) {
				if (! $this->isMyDownline( $parentMasterAgentID, false )) {
					$this->isDownlineInvalid = true;
					return $arrID;
				}
				$parentID = $parentMasterAgentID;
			} else {
				$parentID = $this->masterAgentID;
			}
			$this->getArrayMasterAgentDownlineRecursive( $arrID, $parentID, $active, $status );
		} catch ( Exception $e ) {
			Log::info("MasterAgentDownlineBL::getArrayMasterAgentDownlineAll exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $arrID;
	}
	
	private function getArrayMasterAgentDownlineRecursive( &$arrID, $parentID, $active = false, $status = ""  ) {
		try {
			if (is_null($arrID) || ! is_array($arrID)) {
				$arrID = array();
			}
			$mList = MasterAgent::ListDownline( $this->whitelabelID, $parentID, false, 1, 1000000, false, $active, $status )->get();
			foreach ($mList as $row) {
				$arrID[] = $row->id;
				$this->getArrayMasterAgentDownlineRecursive( $arrID, $row->id, $active, $status );
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentDownlineBL::getArrayMasterAgentDownlineRecursive exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
	public function searchListMasterAgentDownline( $search, $parentMasterAgentID = 0, $page = 1, $max = 100, $maxPlusOne = true, $active = true, $status = "" ) {
		$mList = new Collection();
		try {
			if (! is_null($this->mMasterAgent) && ! empty($this->mMasterAgent->id)) {
				$this->masterAgentID = $this->mMasterAgent->id;
				$this->whitelabelID = $this->mMasterAgent->white_label_id;
			}
			$parentID = 0;
			if (! empty($parentMasterAgentID) && $parentMasterAgentID != $this->masterAgentID) {
				if (! $this->isMyDownline( $parentMasterAgentID, false )) {
					$this->isDownlineInvalid = true;
					return $mList;
				}
				$parentID = $parentMasterAgentID;
			} else {
				$parentID = $this->masterAgentID;
			}
			$search = trim($search);
			if (empty($this->whitelabelID) || empty($search)) {
				return $mList;
			}
			if ($this->debug) {
				try {
					$sql = MasterAgent::SearchIncludeProfile( $search, $this->whitelabelID, $parentID, $page, $max, $maxPlusOne, $active, $status )->toSql();
					$bindings = MasterAgent::SearchIncludeProfile( $search, $this->whitelabelID, $parentID, $page, $max, $maxPlusOne, $active, $status )->getBindings();
					$sql = UtilCommon::queryReplace($sql, $bindings);
					Log::info("MasterAgentDownlineBL::searchListMasterAgentDownline SQL = ". $sql);
				} catch ( Exception $e ) {
					Log::info("MasterAgentDownlineBL::searchListMasterAgentDownline exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
			$mList = MasterAgent::SearchIncludeProfile( $search, $this->whitelabelID, $parentID, $page, $max, $maxPlusOne, $active, $status )->get();
		} catch ( Exception $e ) {
			Log::info("MasterAgentDownlineBL::searchListMasterAgentDownline exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $mList;
	}
	
	public function hasDownline( $parentID, $active = true ) {
		try {
			$mCheck = MasterAgent::ListDownline( $this->whitelabelID, $parentID, false, 1, 1, false, $active )->firstOrFail();
			if (! empty($mCheck) && ! empty($mCheck->id)) {
				return true;
			}
		} catch ( Exception $e ) {
			if ($this->debug) Log::info("MasterAgentDownlineBL::hasDownline exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public static function putQueueUpdateMaxDownline( $masterAgentID, $apiversion = 1 ) {
		try {
			Log::info("MasterAgentDownlineBL::putQueueUpdateMaxDownline master_agent_id=".$masterAgentID);
			$job = new UpdateMasterAgentMaxDownlineJob( $masterAgentID, $apiversion );
			dispatch( $job );
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentDownlineBL::putQueueUpdateMaxDownline master_agent_id=".$masterAgentID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function doUpdateMaxDownline() {
		try {
			if (is_null($this->mMasterAgent)) {
				return false;
			}
			if (empty($this->mMasterAgent->master_agent_max_downline)) {
				DB::table( $this->mMasterAgent->getTable() )
						->where('master_agent_parent_id', '=', $this->mMasterAgent->id)
						->where('master_agent_max_downline', '>', 0)
						->update( [
								'master_agent_max_downline' => 0
						] );
			} else {
				DB::table( $this->mMasterAgent->getTable() )
						->where('master_agent_parent_id', '=', $this->mMasterAgent->id)
						->where('master_agent_max_downline', '>=', $this->mMasterAgent->master_agent_max_downline)
						->update( [
							'master_agent_max_downline' => ($this->mMasterAgent->master_agent_max_downline - 1)
						] );
			}
			try {
				$mList = MasterAgent::where('master_agent_parent_id','=',$this->mMasterAgent->id)->select('id')->orderBy('id','ASC')->get();
				foreach ($mList as $row) {
					$this->putQueueUpdateMaxDownline( $row->id );
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentDownlineBL::doUpdateMaxDownline exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentDownlineBL::doUpdateMaxDownline exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
}