<?php
namespace App\BusinessLogic\MasterAgent;

use Log;
use Config;
use Exception;
use Excel;
use DB;
use App\ApiLogic\ApiDefinition;
use App\ApiLogic\MasterAgent\ApiMasterAgentDownlineBalance;
use App\BusinessLogic\MasterAgent\MasterAgentBL;
use App\BusinessLogic\MasterAgent\MasterAgentProfileBL;
use App\BusinessLogic\Report\Excel\ExcelBalanceTransferResult;
use App\Jobs\Batch\TransferBalanceToDownlineBatchJob;
use App\Core\CacheData\Simple\CacheMasterAgentTrxBatchStatus;
use App\Core\Util\UtilCommon;
use App\Core\Util\UtilEmail;
use App\Models\Api\ApiError;
use App\Models\Api\ApiResponse;
use App\Models\Database\MasterAgent;
use App\Models\Database\MasterAgentProfile;
use App\Models\Database\BatchTransferBalance;
use App\Models\Database\BatchTransferBalanceBreakup;
use App\Models\Database\LogTransferBalance;

class MasterAgentBatchTransferBalanceBL extends MasterAgentBL
{
	protected $mBatch = null;
	protected $mBreakup = null;
	protected $errcode = ApiError::ERROR_GENERAL;
	protected $errmsg = "";
	protected $logID = 0;

	public function __construct( $whitelabelID, $masterAgentID = 0, $apiversion = 1 ) {
		parent::__construct( $whitelabelID, $masterAgentID, $apiversion );
	}

	public function setDataBatchJob( $id ) {
		$this->mBreakup = null;
		try {
			$this->mBatch = BatchTransferBalance::where('id','=',$id)->firstOrFail();
			if (! is_null($this->mBatch) && ! empty($this->mBatch->id)) {
				if ($this->mBatch->transfer_to == "MASTER_AGENT") {
					if (empty($this->mBatch->datetime_start)) {
						$this->mBatch->datetime_start = date('Y-m-d H:i:s');
						$this->mBatch->save();
					}
					$this->setWhiteLabelID( $this->mBatch->white_label_id );
				} else {
					$this->mBatch = null;
				}
			} else {
				$this->mBatch = null;
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchTransferBalanceBL::setDataBatchJob id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			$this->mBatch = null;
		}
		if (! is_null($this->mBatch)) {
			try {
				$totalfailed = BatchTransferBalanceBreakup::where('batch_transfer_balance_id','=',$id)->where('transfer_value','<=',0)->whereNull('api_error_code')->count();
				if ($this->debug) Log::info("MasterAgentBatchTransferBalanceBL::setDataBatchJob #1 total_failed=".$totalfailed);
				if ($totalfailed > 0) {
					$mTable = new BatchTransferBalanceBreakup();
					DB::table( $mTable->getTable() )->where('batch_transfer_balance_id','=',$id)->where('transfer_value','<=',0)
						->update( [
							'api_error_code' => ApiError::ERROR_TRANSFER_VALUE_INVALID,
							'api_error_message' => ApiError::getErrorString( ApiError::ERROR_TRANSFER_VALUE_INVALID )
						] );
					$this->mBatch->total_data += $totalfailed;
					$this->mBatch->total_failed += $totalfailed;
					$this->mBatch->save();
				}
				$totalfailed = BatchTransferBalanceBreakup::where('batch_transfer_balance_id','=',$id)->where('master_agent_id','=',$this->mBatch->master_agent_id)->whereNull('api_error_code')->count();
				if ($this->debug) Log::info("MasterAgentBatchTransferBalanceBL::setDataBatchJob #2 total_failed=".$totalfailed);
				if ($totalfailed > 0) {
					$mTable = new BatchTransferBalanceBreakup();
					DB::table( $mTable->getTable() )->where('batch_transfer_balance_id','=',$id)->where('master_agent_id','=',$this->mBatch->master_agent_id)
						->update( [
							'api_error_code' => ApiError::ERROR_MASTER_AGENT_NOT_UNDER_DOWNLINE,
							'api_error_message' => ApiError::getErrorString( ApiError::ERROR_MASTER_AGENT_NOT_UNDER_DOWNLINE )
						] );
					$this->mBatch->total_data += $totalfailed;
					$this->mBatch->total_failed += $totalfailed;
					$this->mBatch->save();
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchTransferBalanceBL::setDataBatchJob id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				$this->mBreakup = BatchTransferBalanceBreakup::where('batch_transfer_balance_id','=',$id)->whereNull('api_error_code')->orderBy('id','ASC')->firstOrFail();
				if (! is_null($this->mBreakup) && ! empty($this->mBreakup->id)) {
					// OK
				} else {
					$this->mBreakup = null;
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchTransferBalanceBL::setDataBatchJob id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				$this->mBreakup = null;
			}
		}
		return $this;
	}
	
	public function getBreakupID() {
		return (! is_null($this->mBreakup) ? $this->mBreakup->id : 0);
	}
	
	public function isInvalidDataBatch() {
		return (is_null($this->mBatch) ? true : false);
	}
	
	public function isFoundDataBatchJob() {
		return (! is_null($this->mBatch) && ! is_null($this->mBreakup) ? true : false);
	}
	
	public function startBatch( $batchID ) {
		try {
			$cache = new CacheMasterAgentTrxBatchStatus();
			$cache->setProcessing( ! is_null($this->mBatch) && ! empty($this->mBatch->master_agent_id) ? $this->mBatch->master_agent_id : 0 );
			if ( ! empty($batchID) ) {
				return self::putQueueProcessJob( $batchID );
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchTransferBalanceBL::startBatch exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public static function putQueueProcessJob( $id ) {
		try {
			$queuename = "fintechapi_commission";
			$job = new TransferBalanceToDownlineBatchJob( $id );
			if (! empty($queuename)) {
				$job->onQueue($queuename);
			}
			dispatch( $job );
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchTransferBalanceBL::putQueueProcessJob id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function processJob() {
		$isSuccess = false;
		$this->errcode = ApiError::ERROR_GENERAL;
		$this->errmsg = "";
		$this->logID = 0;
		$this->mMasterAgent = null;
		try {
			$cache = new CacheMasterAgentTrxBatchStatus();
			$cache->setProcessing( ! empty($this->mBatch->master_agent_id) ? $this->mBatch->master_agent_id : 0 );
			$validData = false;
			try {
				if (! empty($this->mBatch->master_agent_id)) {
					$this->mMasterAgent = MasterAgent::where('id', '=', $this->mBatch->master_agent_id)->firstOrFail();
					$this->whitelabelID = $this->mMasterAgent->white_label_id;
					$this->masterAgentID = $this->mMasterAgent->id;
					if ($this->whitelabelID == $this->mBatch->white_label_id) {
						$validData = true;
					}
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchTransferBalanceBL::processJob exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			if ($validData) {
				$input = array(
						'white_label_id' => $this->whitelabelID,
						'transfer_value' => $this->mBreakup->transfer_value
				);
				$api = new ApiMasterAgentDownlineBalance( $input );
				$api->setWhiteLabelID( $this->whitelabelID )
					->setMasterAgentModel( $this->mMasterAgent );
				if (! is_null($this->mWL)) {
					$api->setWhiteLabelModel( $this->mWL );
				} else {
					$this->setWhiteLabelID($this->whitelabelID, true);
					$api->setWhiteLabelModel( $this->mWL );
				}
				$resp = $api->transferBalance( $this->mBreakup->master_agent_id, false, $this->mBatch->transfer_by );
				if (is_null($resp) || ! is_array($resp)) {
					$apiResp = new ApiResponse( $this->whitelabelID );
					$resp = $apiResp->getResponse();
				}
				if ($this->debug) Log::info("MasterAgentBatchTransferBalanceBL::processJob api_resp=".json_encode($resp));
				$this->errcode = $resp[ApiResponse::KEY_ERROR][ApiResponse::KEY_ERRORCODE];
				$this->errmsg = $resp[ApiResponse::KEY_ERROR][ApiResponse::KEY_ERRORMSG];
				if (! is_null($resp[ApiResponse::KEY_RESULTS]) && is_array($resp[ApiResponse::KEY_RESULTS])) {
					if (isset($resp[ApiResponse::KEY_RESULTS]['log_id'])) {
						$this->logID = $resp[ApiResponse::KEY_RESULTS]['log_id'];
					}
				}
			} else {
				$this->errcode = ApiError::ERROR_MASTER_AGENT_INVALID;
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchTransferBalanceBL::processJob exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		} finally {
			try {
				$this->updateBatchBreakup();
				$isSuccess = ($this->errcode == ApiError::SUCCESS ? true : false);
				$this->mBatch->total_data++;
				if ($isSuccess) {
					$this->mBatch->total_success++;
					$this->mBatch->total_transferred += $this->mBreakup->transfer_value;
				} else {
					$this->mBatch->total_failed++;
				}
				$this->mBatch->save();
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchTransferBalanceBL::processJob exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
		}
		return $isSuccess;
	}
	
	public function sendReport() {
		$isSuccess = false;
		$filepath = "";
		try {
			if (! empty($this->mBatch->emails)) {
				$filepath = $this->createFileDataExcelReport();
				if (! empty($filepath)) {
					if (! file_exists($filepath)) {
						$filepath = "";
					}
				}
				$isSuccess = $this->sendEmailReportWithAttachment( $this->mBatch->emails, $filepath );
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchTransferBalanceBL::sendReport exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		} finally {
			try {
				if (empty($this->mBatch->datetime_stop)) {
					$this->mBatch->datetime_stop = date('Y-m-d H:i:s');
					$this->mBatch->save();
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchTransferBalanceBL::sendReport exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				// hapus record breakup..
				$mTable = new BatchTransferBalanceBreakup();
				DB::table( $mTable->getTable() )->where( 'batch_transfer_balance_id', '=', $this->mBatch->id )->delete();
			} catch ( Exception $e ) {
				Log::info("AgentBatchTransferBalanceBL::sendReport exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				// hapus file attachment untuk report ke email
				if (! empty($filepath)) {
					unlink($filepath);
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchTransferBalanceBL::sendReport exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
			try {
				// batch status OFF lagi
				$cache = new CacheMasterAgentTrxBatchStatus();
				$cache->deleteStatus( ! is_null($this->mBatch) && ! empty($this->mBatch->master_agent_id) ? $this->mBatch->master_agent_id : 0 );
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchTransferBalanceBL::sendReport exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
		}
		return $isSuccess;
	}
	
	private function updateBatchBreakup() {
		try {
			if (empty($this->errmsg)) {
				$this->errmsg = ApiError::getErrorString( $this->errcode );
			}
			$this->mBreakup->api_error_code = $this->errcode;
			$this->mBreakup->api_error_message = $this->errmsg;
			$this->mBreakup->currency_code = 'IDR';
			$this->mBreakup->paymode = (! is_null($this->mMasterAgent) ? $this->mMasterAgent->paymode : null);
			$this->mBreakup->balance_type_id = 0;
			$this->mBreakup->balance_value_before = 0;
			$this->mBreakup->balance_value_after = 0;
			if (! empty($this->logID)) {
				try {
					$mLog = LogTransferBalance::where('id','=',$this->logID)->firstOrFail();
					if (! is_null($mLog)) {
						$this->mBreakup->currency_code = $mLog->receiver_currency_code;
						$this->mBreakup->balance_type_id = $mLog->receiver_balance_type_id;
						$this->mBreakup->balance_value_before = $mLog->receiver_balance_value_before;
						$this->mBreakup->balance_value_after = $mLog->receiver_balance_value_after;
					}
				} catch ( Exception $e ) {
					Log::info("MasterAgentBatchTransferBalanceBL::updateBatchBreakup exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
			$this->mBreakup->save();
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchTransferBalanceBL::updateBatchBreakup exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
	private function createFileDataExcelReport() {
		$filepathsave = "";
		try {
			if (is_null($this->mWL)) {
				$this->setWhiteLabelID( $this->mBatch->white_label_id, true);
			}
			$sheetname = "TransferBalanceToDownline";
			$filename = "result_transfer_balance_to_downline_" .
					UtilCommon::idConvertToBase36( ! empty($this->mBatch->master_agent_id) ? $this->mBatch->master_agent_id : $this->mBatch->white_label_id ) .
					"_" . UtilCommon::randomCode() . ".xlsx";
			Excel::store(new ExcelBalanceTransferResult( $this->mBatch->id, "MASTER_AGENT", $sheetname, $this->mWL->product_name ), $filename );
			$filepathsave = (string)storage_path('app/'.$filename);
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchTransferBalanceBL::createFileDataExcelReport exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		} finally {
			try {
				if (! empty($filepathsave) && ! file_exists($filepathsave)) {
					$filepathsave = "";
				}
			} catch ( Exception $e ) {
				Log::info("MasterAgentBatchTransferBalanceBL::createFileDataExcelReport exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
		}
		return $filepathsave;
	}
	
	private function sendEmailReportWithAttachment( $emails, $fileReport ) {
		$b = false;
		try {
			$tgl = substr($this->mBatch->datetime_start, 0, 10);
			$hour = substr($this->mBatch->datetime_start, 11, 2);
			$subject = "Report Batch Transfer Balance to Downline of Master Agent";
			$nameBlade = "email.white_label_".$this->whitelabelID.".batch.report_transfer_balance";
			$paramBlade = array(
					'name' 				=> 'DOWNLINE MASTER AGENT',
					'date' 				=> $tgl,
					'hour' 				=> $hour,
					'original_filename' => $this->mBatch->original_filename,
					'total_data' 		=> $this->mBatch->total_data,
					'total_success' 	=> $this->mBatch->total_success,
					'total_failed' 		=> $this->mBatch->total_failed,
					'total_transferred' => $this->mBatch->total_transferred
			);
			if (is_null($this->mWL)) {
				$this->setWhiteLabelID( $this->whitelabelID, true );
			}
			if ( empty($this->mWL->email_from_address) ) {
				$b = UtilEmail::sendEmailByTemplateView($nameBlade, $paramBlade, $subject, $emails, "", "", "", $fileReport);
			} else {
				$b = UtilEmail::sendEmailByTemplateView($nameBlade, $paramBlade, $subject, $emails, "", $this->mWL->email_from_address, $this->mWL->email_from_fullname, $fileReport);
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentBatchTransferBalanceBL::sendEmailReportWithAttachment exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
}