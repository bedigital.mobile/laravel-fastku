<?php
namespace App\BusinessLogic\MasterAgent;

use Log;
use Config;
use Exception;
use DB;
use App\ApiLogic\ApiDefinition;
use App\BusinessLogic\MasterAgent\MasterAgentBL;
use App\BusinessLogic\MasterAgent\MasterAgentProfileBL;
use App\SystemLogic\Auth\PasswordMasterAgent;
use App\SystemLogic\Auth\SessionAgent;
use App\SystemLogic\Auth\TokenLogic;
use App\SystemLogic\SMS\SMSSenderManager;
use App\SystemLogic\GeoInfo\GeoLocIP;
use App\Core\GeoInfo\IPInfo;
use App\Core\Util\UtilCommon;
use App\Core\Util\UtilEmail;
use App\Core\Util\UtilPhone;
use App\Core\CacheData\CacheMasterAgentDevice;
use App\Core\CacheData\Simple\CacheMasterAgentSendSMSDeviceToken;
use App\Core\CacheData\Simple\CacheMasterAgentSendSMSDeviceTokenCounter;
use App\Jobs\Email\SendEmailMasterAgentLoginJob;
use App\Jobs\SMS\SendSMSTokenMasterAgentDeviceJob;
use App\Models\Api\ApiError;
use App\Models\Database\MasterAgent;
use App\Models\Database\MasterAgentDevice;
use WhichBrowser;

class MasterAgentAuthBL extends MasterAgentBL
{
	private $mDevice = null;
	
	public function __construct( $whitelabelID, $masterAgentID = 0, $apiversion = 1 ) {
		parent::__construct( $whitelabelID, $masterAgentID, $apiversion );
	}

	public function verifyUsername( $username ) {
		try {
			$this->setMasterAgentByUsername($username);
			return $this->isValid();
		} catch ( Exception $e ) {
			Log::info("MasterAgentAuthBL::verifyUsername username=".$username." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function verifyPassword( $username, $pwdEnc, $pwdType, $digestresponse = "", $nonce = "", $realm = "", $uri = "", $method = "POST" ) {
		try {
			if (strtolower(trim($username)) != strtolower(trim($this->mMasterAgent->username))) {
				return false;
			}
			$objLogicPwd = new PasswordMasterAgent( $this->whitelabelID );
			$pwdEncDB = "";
			if ($this->mMasterAgent->password_type != 'MD5') {
				// awalnya tersimpan password dalam PLAIN TEXT, dan dipaksa diganti password disimpan dalam MD5
				$this->mMasterAgent = $objLogicPwd->setPasswordByModel( $this->mMasterAgent, $this->mMasterAgent->password_enc );
			}
			$pwdEncAtDB = $this->mMasterAgent->password_enc;
			if ($pwdType == ApiDefinition::PASSWORD_TYPE_DIGEST) {
				$realmAtDB = "";
				if (is_null($this->mWL)) {
					$this->setWhiteLabelID( $this->whitelabelID, true );
				}
				if (! is_null($this->mWL)) {
					$realmAtDB = $this->mWL->realm;
				}
				if (empty($realmAtDB)) {
					$realmAtDB = $realm;
				}
				if (empty($realmAtDB)) {
					$realmAtDB = "BEDIGITAL";
				}
				// refer to RFC-2069 HTTP Digest Access Authentication
				$responseAtDB = PasswordMasterAgent::stringResponseDigestAccessAuthentication( $username, $pwdEncAtDB, $nonce, $realmAtDB, $method, $uri );
				if ($this->debug) Log::info("MasterAgentAuthBL::verifyPassword username=".$username." pwd_type=".$pwdType." nonce=".$nonce." digestresponse=".$digestresponse." response_at_db=".$responseAtDB." response_valid=".($responseAtDB == $digestresponse ? "true" : "false")." realm=".$realm." realm_at_db=".$realmAtDB." uri=".$uri." method=".$method);
				return ($responseAtDB == $digestresponse ? true : false);
			} else if ($pwdType == ApiDefinition::PASSWORD_TYPE_MD5) {
				// input password sdh dalam MD5, tidak perlu dikonversi dalam encryption MD5
				if ($this->debug) Log::info("MasterAgentAuthBL::verifyPassword pwd_enc=".$pwdEnc." pwd_type=".$pwdType." pwd_enc_at_db=".$pwdEncAtDB);
				return ($pwdEnc == $pwdEncAtDB ? true : false);
			} else {
				// dianggap input password using PLAIN, maka konversi dalam encryption MD5
				$pwdEncCheck = $objLogicPwd->encryptPassword( $this->mMasterAgent->username, $pwdEnc );
				if ($this->debug) Log::info("MasterAgentAuthBL::verifyPassword pwd_enc=".$pwdEnc." pwd_enc_check=".$pwdEncCheck." pwd_type=".$pwdType." pwd_enc_at_db=".$pwdEncAtDB);
				return ($pwdEncCheck == $pwdEncAtDB ? true : false);
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentAuthBL::verifyPassword ".($this->debug ? ("pwd_enc=".$pwdEnc." pwd_type=".$pwdType) : "")." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function setDevice( MasterAgentDevice $mDevice ) {
		$this->mDevice = $mDevice;
		return $this;
	}
	
	public function verifyDevice( $deviceID, $platform, $appUserAgent = "" ) {
		try {
			if (empty($deviceID) || is_null($this->mMasterAgent) || (! is_null($this->mMasterAgent) && empty($this->mMasterAgent->id))) {
				return false;
			}
			$platform = ApiDefinition::normalizePlatform( $platform );
			if (empty($platform)) {
				$platform = ApiDefinition::PLATFORM_WEB;
			}
			$deviceMD5 = md5($deviceID);
			$this->mDevice = null;
			$cache = new CacheMasterAgentDevice();
			$id = $cache->getID($this->mMasterAgent->id, $deviceMD5, $platform);
			if (! empty($id)) {
				try {
					$this->mDevice = MasterAgentDevice::where( 'id', '=', $id )->firstOrFail();
				} catch ( Exception $e ) {
					$this->mDevice = null;
					if ($this->debug) Log::info("MasterAgentAuthBL::verifyDevice device_id=".$deviceID." platform=".$platform." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
				if (! is_null($this->mDevice)) {
					if (empty($this->mDevice->id) || $this->mDevice->master_agent_id != $this->mMasterAgent->id || $this->mDevice->device_id_md5 != $deviceMD5 || $this->mDevice->app_platform != $platform ) {
						$mDevice = null;
						$cache->deleteID($this->mMasterAgent->id, $deviceMD5, $platform);
					}
				} else {
					$cache->deleteID($this->mMasterAgent->id, $deviceMD5, $platform);
				}
			}
			if (is_null($this->mDevice) || (! is_null($this->mDevice) && empty($this->mDevice->id))) {
				try {
					$this->mDevice = MasterAgentDevice::where('master_agent_id','=',$this->mMasterAgent->id)->where('device_id_md5','=',$deviceMD5)->where('app_platform','=',$platform)->firstOrFail();
				} catch ( Exception $e ) {
					$this->mDevice = null;
					if ($this->debug) Log::info("MasterAgentAuthBL::verifyDevice device_id=".$deviceID." platform=".$platform." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
			if (is_null($this->mDevice) || (! is_null($this->mDevice) && empty($this->mDevice->id))) {
				$this->mDevice = new MasterAgentDevice();
				$this->mDevice->master_agent_id = $this->mMasterAgent->id;
				$this->mDevice->device_id = (strlen($deviceID) <= 100 ? $deviceID : substr($deviceID, 0, 100));
				$this->mDevice->device_id_md5 = $deviceMD5;
				$this->mDevice->app_platform = $platform;
				$this->mDevice->app_user_agent = (strlen($appUserAgent) <= 200 ? $appUserAgent : substr($appUserAgent, 0, 200));
				$this->mDevice->is_allowed = 'N';
				$this->mDevice->save();
				$id = $this->mDevice;
				if (! empty($id)) {
					$cache->setID( $this->mMasterAgent->id, $deviceMD5, $platform, $id );
				}
			}
			return ($this->mDevice->is_allowed == 'Y' ? true : false);
		} catch ( Exception $e ) {
			Log::info("MasterAgentAuthBL::verifyDevice device_id=".$deviceID." platform=".$platform." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function findDeviceNotAllowed() {
		try {
			if (is_null($this->mMasterAgent)) {
				return false;
			}
			$this->mDevice = MasterAgentDevice::where('master_agent_id','=',$this->mMasterAgent->id)->where('is_allowed','=','N')->orderBy('id','DESC')->firstOrFail();
			if (! is_null($this->mDevice) && ! empty($this->mDevice->id)) {
				return true;
			}
		} catch ( Exception $e ) {
			$this->mDevice = null;
			Log::info("MasterAgentAuthBL::findDeviceNotAllowed exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function putQueueSendSMSTokenDevice( $telpNumber ) {
		try {
			if (is_null($this->mDevice) || empty($telpNumber)) {
				return false;
			}
			$queuename = "fintechapi_email";
			$job = new SendSMSTokenMasterAgentDeviceJob( $this->whitelabelID, $this->mDevice->id, $telpNumber, $this->apiversion );
			if (! empty($queuename)) {
				$job->onQueue($queuename);
			}
			dispatch( $job );
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentAuthBL::putQueueSendSMSTokenDevice telp=".$telpNumber." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function sendSMSDeviceToken( $telpNumber ) {
		try {
			if (is_null($this->mDevice) || empty($telpNumber)) {
				return false;
			}
			$token = $this->createDeviceToken();
			if (empty($token)) {
				return false;
			}
			if (is_null($this->mWL)) {
				$this->setWhiteLabelID( $this->whitelabelID, true );
			}
			if (is_null($this->mMasterAgent) || (! is_null($this->mMasterAgent) && $this->mMasterAgent->id != $this->mDevice->master_agent_id)) {
				$this->setMasterAgent( $this->mDevice->master_agent_id, false, false );
			}
			// sementara
			// $msg = "Gunakan kode ".$token." utk verifikasi saat login dg username ".$this->mMasterAgent->username." di Web Koordinator aplikasi ".$this->mWL->product_name.". Kode ini berlaku 1 jam & jgn berikan ke pihak lain.";
			$msg = $token." adl kode verifikasi perangkat utk login dg username ".$this->mMasterAgent->username." di Main Agen ".$this->mWL->product_name.". Kode ini berlaku 1 jam & jgn berikan ke pihak lain.";
			$smsSender = new SMSSenderManager( $this->whitelabelID );
			$smsSender->setPurposeType( "SMSTOKENDEVICE" );
			$b = $smsSender->send( $telpNumber, $msg );
			if ($b) {
				$cache = new CacheMasterAgentSendSMSDeviceToken();
				$cache->setFlagRecentlySMSSent( $this->mDevice->agent_id );
				$cache = new CacheMasterAgentSendSMSDeviceTokenCounter();
				$cache->increment( $this->mDevice->master_agent_id );
			}
			return $b;
		} catch ( Exception $e ) {
			Log::info("MasterAgentAuthBL::sendSMSDeviceToken telp=".$telpNumber." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	private function createDeviceToken() {
		try {
			if (empty($this->whitelabelID) && ! is_null($this->mWL)) {
				$this->whitelabelID = $this->mWL->id;
			}
			if (empty($this->whitelabelID) && ! is_null($this->mMasterAgent)) {
				$this->whitelabelID = $this->mMasterAgent->white_label_id;
			}
			if (empty($this->whitelabelID)) {
				$this->setMasterAgent( $this->mDevice->master_agent_id, false, false );
			}
			if (empty($this->whitelabelID) && ! is_null($this->mMasterAgent)) {
				$this->whitelabelID = $this->mMasterAgent->white_label_id;
			}
			if (empty($this->whitelabelID)) {
				return "";
			}
			$sessionID = md5( $this->mDevice->device_id_md5 . "|" . $this->mDevice->app_platform );
			$objLogic = new TokenLogic( $this->whitelabelID, $this->mDevice->master_agent_id, $sessionID );
			$token = $objLogic->generateSMSToken();
			if ($this->debug) Log::info("MasterAgentAuthBL::createDeviceToken master_agent_id=".$this->mDevice->master_agent_id." platform=".$this->mDevice->app_platform." ==> token=".$token);
			return $token;
		} catch ( Exception $e ) {
			Log::info("MasterAgentAuthBL::createDeviceToken exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return "";
	}
	
	public function verifyDeviceToken( $token ) {
		$b = false;
		try {
			if (is_null($this->mMasterAgent) || empty($token)) {
				return $b;
			}
			if (empty($this->whitelabelID) && ! is_null($this->mWL)) {
				$this->whitelabelID = $this->mWL->id;
			}
			if (empty($this->whitelabelID) && ! is_null($this->mMasterAgent)) {
				$this->whitelabelID = $this->mMasterAgent->white_label_id;
			}
			if ($this->debug) Log::info("MasterAgentAuthBL::verifyDeviceToken master_agent_id=".$this->mMasterAgent->id." token=".$token);
			$mTable = new MasterAgentDevice();
			$tablename = $mTable->getTable();
			$max = 100;
			$mList = MasterAgentDevice::where('master_agent_id','=',$this->mMasterAgent->id)->orderBy('is_allowed','DESC')->orderBy('id','DESC')->take( $max )->get();
			foreach ($mList as $row) {
				try {
					$sessionID = md5( $row->device_id_md5 . "|" . $row->app_platform );
					$objLogic = new TokenLogic( $this->whitelabelID, $this->mMasterAgent->id, $sessionID );
					$b = $objLogic->isSMSTokenValid($token);
					if ($this->debug) Log::info("MasterAgentAuthBL::verifyDeviceToken row_id=".$row->id." platform=".$row->app_platform." device_id_md5=".$row->device_id_md5." ==> HASIL=".($b?"true":"false"));
					if ($b) {
						try {
							DB::table( $tablename )->where('id', '=', $row->id)
									->update( [
										'is_allowed' => 'Y'
									] );
						} catch ( Exception $e ) {
							Log::info("MasterAgentAuthBL::verifyDeviceToken token=".$token." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
						}
						break;
					}
				} catch ( Exception $e ) {
					Log::info("MasterAgentAuthBL::verifyDeviceToken token=".$token." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
		} catch ( Exception $e ) {
			Log::info("MasterAgentAuthBL::verifyDeviceToken token=".$token." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
	public static function putQueueSendEmailLoginNotification( $masterAgentID, $datetime, $platform, $httpUserAgent = "", $deviceName = "", $ipAddr = "" ) {
		try {
			$queuename = "fintechapi_email";
			$job = new SendEmailMasterAgentLoginJob( $masterAgentID, $datetime, $platform, $httpUserAgent, $deviceName, $ipAddr );
			if (! empty($queuename)) {
				$job->onQueue($queuename);
			}
			dispatch( $job );
			return true;
		} catch ( Exception $e ) {
			Log::info("MasterAgentAuthBL::putQueueSendEmailLoginNotification master_agent_id=".$masterAgentID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function sendEmailLoginNotification( $datetime, $platform, $httpUserAgent = "", $deviceName = "", $ipAddr = "" ) {
		try {
			if (is_null($this->mMasterAgent)) {
				return false;
			}
			// sementara
			$subject = "Notifikasi: Koordinator/Master Agent login";
			$email = "";
			$fullname = "";
			$profileBL = new MasterAgentProfileBL( $this->whitelabelID );
			$profileBL->setMasterAgentModel( $this->mMasterAgent );
			$mProfile = $profileBL->getProfileModel();
			if (! is_null($mProfile)) {
				$fullname = $mProfile->fullname;
				if (empty($email)) {
					$email = $mProfile->email;
				}
			}
			if (! empty($email)) {
				if (empty($fullname)) {
					$fullname = $this->mMasterAgent->username;
				}
				if (is_null($this->mWL)) {
					$this->setWhiteLabelID( $this->mMasterAgent->white_label_id, true );
				}
				$info = "";
				$info = "";
				if (! empty($httpUserAgent)) {
					try {
						if ($platform == ApiDefinition::PLATFORM_ANDROID) {
							$arr = explode(";", $httpUserAgent);
							$jml = count($arr);
							if (strlen($httpUserAgent) <= 80 && $jml >= 3) {
								$manufacture = $arr[1];
								$model = $arr[2];
								if (strpos($manufacture, '/') === false && strpos($model, '/') === false) {
									$info = $manufacture . ' - ' . $model;
								}
							}
						}
						if (empty($info)) {
							$objDetector = new WhichBrowser\Parser( $httpUserAgent );
							$info = $objDetector->toString();
						}
					} catch ( Exception $e ) {
						Log::info("MasterAgentAuthBL::sendEmailLoginNotification exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				}
				if (! empty($deviceName)) {
					$info .= " (" . $deviceName . ")";
				}
				if ($platform == ApiDefinition::PLATFORM_ANDROID) {
					$platform = "Aplikasi Android POSFIN Koordinator/Master Agen";
				} else if ($platform == ApiDefinition::PLATFORM_IOS) {
					$platform = "Aplikasi iPhone POSFIN Koordinator/Master Agen";
				} else if ($platform == ApiDefinition::PLATFORM_WEB) {
					$platform = "Web POSFIN Koordinator/Master Agen";
				}
				$stripInfo = "";
				if (! empty($ipAddr)) {
					try {
						$geoIP = new GeoLocIP();
						$ipInfo = $geoIP->queryLocation($ipAddr);
						if (! empty($ipInfo->ip_long)) {
							if (! empty($ipInfo->organization)) {
								$stripInfo = $ipInfo->organization;
								if (! empty($ipInfo->country_name) && stripos($ipInfo->organization,$ipInfo->country_name) === false) {
									$stripInfo .= ", " . $ipInfo->country_name;
								}
							} else if (! empty($ipInfo->country_name)) {
								$stripInfo = $ipInfo->country_name;
							} else {
								$stripInfo = $ipInfo->country_iso;
							}
						}
					} catch ( Exception $e ) {
						Log::info("MasterAgentAuthBL::sendEmailLoginNotification exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				}
				$nameBlade = "email.white_label_".$this->mMasterAgent->white_label_id.".notification.notif_login";
				$paramBlade = array(
						'fullname' => $fullname,
						'datetime' => UtilCommon::stringDateTimeTransaction( $datetime ),
						'user_type' => 'MASTER AGENT (Koordinator)',
						'id' => strtoupper(UtilCommon::idConvertToBase36( $this->mMasterAgent->id )),
						'platform' => $platform,
						'info' => $info,
						'ip_address' => $ipAddr,
						'ip_info' => $stripInfo
				);
				$b = false;
				if ( empty($this->mWL->email_from_address) ) {
					$b = UtilEmail::sendEmailByTemplateView($nameBlade, $paramBlade, $subject, $email, $fullname);
				} else {
					$b = UtilEmail::sendEmailByTemplateView($nameBlade, $paramBlade, $subject, $email, $fullname, $this->mWL->email_from_address, $this->mWL->email_from_fullname);
				}
				return $b;
			}
		} catch ( Exception $e ) {
			Log::info("AgentAuthBL::sendEmailLoginNotification exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
}