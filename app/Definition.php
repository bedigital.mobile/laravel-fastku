<?php
namespace App;

class Definition
{
	const PAGE_ROOT = "https://fintech-stg.bedigital.co.id/admin/";
	const PAGE_ROOT_DEV = "https://fintech-stg.bedigital.co.id/admin/";
 	const PORT_DEV = "";
	// Page USER
	const PAGE_USER = "cmsuser/";
	const PAGE_USER_MAIN = "cmsuser/main";
	const PAGE_USER_LOGIN = "cmsuser/login";
	const PAGE_USER_OTP = "cmsuser/otp";


	
	const PAGE_USER_INQUIRY_CUSTOMER = "cmsuser/customer/";
	const PAGE_USER_INQUIRY_CUSTOMER_PROFILE = "cmsuser/customer/inquiry-profile";
	const PAGE_USER_INQUIRY_CUSTOMER_COMPANY = "cmsuser/customer/inquiry-company";
	const PAGE_USER_INQUIRY_CUSTOMER_DOCUMENT = "cmsuser/customer/inquiry-document";
	const PAGE_USER_INQUIRY_CUSTOMER_ORDER = "cmsuser/customer/inquiry-order";
	const PAGE_USER_INQUIRY_CUSTOMER_APPROVAL = "cmsuser/customer/inquiry-approval";
	const PAGE_USER_APPROVAL_PROFILE_ORDER = "cmsuser/customer/approval-profile-order";
	const PAGE_USER_APPROVAL_PAYMENT = "cmsuser/customer/approval-payment";
	const PAGE_USER_APPROVAL_REGISTERED = "cmsuser/customer/approval-registered";
	const PAGE_USER_APPROVAL_ACTIVATED = "cmsuser/customer/approval-activated";
	const PAGE_USER_INQUIRY_SUBSCRIBER = "cmsuser/subscriber/inquiry";
	  
	
	// folder dan URL untuk file resources 
	// const FOLDER_RESOURCES_ROOT = "/var/www/resourceskring/";
	// const FOLDER_RESOURCES_PROFILE = "/var/www/resourceskring/profile/";
	const FOLDER_RESOURCES_ROOT = "/var/www/laravelkring/public/res/";
	const FOLDER_RESOURCES_PROFILE = "/var/www/laravelkring/public/res/profile/";
	const FOLDER_RESOURCES_COMPLAIN = "/var/www/laravelkring/public/res/complain/";
	const PAGE_RESOURCES_ROOT = "res/";
	const PAGE_RESOURCES_PROFILE = "res/profile/";
	const PAGE_RESOURCES_IMAGE = "res/image/";
	const PAGE_RESOURCES_IMAGE_PRODUCT = "res/product/"; 
	 
	// flag apakah menggunakan simple profile saat entry FAB
	const SIMPLE_PROFILE_FAB = true;
	
	// flag penggunaan asynchronous process
	const USE_ASYNC_PROCESS = false;
	// Queue
	const QUEUE_LOG = "log";
	const QUEUE_EMAIL = "email";
	
	// phone number
	const PHONE_COUNTRY_CODE = "62";
	
	// jangka waktu berlangganan dalam bulan
	const SUBSCRIPTION_PERIOD_MIN = 3; // 3 bulan
	const SUBSCRIPTION_PERIOD_MAX = 60; // 60 bulan = 5 tahun
	const SUBSCRIPTION_PERIOD_FOREVER = 0; // 0 untuk kode berlangganan selamanya
	const SUBSCRIPTION_PERIOD_DELTA = 3; // per 3 bulan
	// jumlah maximal pembelian nomor
	const LIST_NUMBER_MAX = 10;
	const ORDER_QUANTITY_MAX = 30; 
	
}