<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;

use App\Models\ProductModel;
use App\Models\PartnerModel;
use App\Models\PartnerDepositModel;
use App\Models\PartnerTrxReqModel;
use App\Models\WhiteLabelProductModel;
use App\Models\PartnerProductExcludeModel;
use App\Models\PartnerProductModel;
use App\Models\BillerAggratorModel;
use App\Models\TrxRequestWorkerModel;

use App\Jobs\PartnerTrxRequestCallbackJob;
use App\Jobs\PartnerActivityLogJob;
use App\Http\Helpers\NotificationHelper;
use App\Http\Helpers\HttpClientHelper;
use App\Http\Helpers\PartnerActivityLogHelper;

use App\Core\Util\UtilCommon;

class TrxRequestHelper {
    
    var $request;
    var $ip;
    var $apiKey;
    var $workerNumber;
    var $accessKey;
    var $notif;
    var $trxId = 0;
    var $partner;
    var $partnerDeposit;
    var $productFee;
    var $billerAggregator = ['id' => 0];
    
    public function __construct(Request $request, $workerNumber=0, $accessKey='') {
        $this->request = $request;
        $this->workerNumber = $workerNumber;
        $this->accessKey = $accessKey;
        $this->notif = new NotificationHelper();
    }
    
    function run() {
        $this->ip = $this->request->input('ip', '');
        $this->apiKey = $this->request->input('api_key', '');
        
        $token = $this->request->input('token', '');
        $productCode = $this->request->input('product', '');
        $billNumber = $this->request->input('billNumber', '');
        $sign = $this->request->input('sign', '');
        $trxType = strtoupper($this->request->input('type', PartnerTrxReqModel::TRX_TYPE_INQUIRY));
        $partnerCallbackURL = $this->request->input('callback_url', '');
        
        if ($this->apiKey == '') { $this->notif->addMessage('API Key is required'); }
        if ($trxType == '') { $this->notif->addMessage('Trx type is required'); }
        
        if ($trxType !== PartnerTrxReqModel::TRX_TYPE_BALANCE) {
            if ($productCode == '') { $this->notif->addMessage('Product code is required'); }
            if ($billNumber == '') { $this->notif->addMessage('Bill number is required'); }
        }
        
        if ($this->notif->isOK() == false) {
            if ($partnerCallbackURL != '') {
                PartnerTrxRequestCallbackJob::dispatch($this->request->input('callback_url'), $this->notif->build());
            }
            
            $response = $this->notif->build();
            Log::info('Partner '.$this->apiKey.' access: '. json_encode($response));
            PartnerActivityLogJob::dispatch($this->ip, $this->apiKey, '/trx/request/'.strtolower($trxType), json_encode($this->request->input()), json_encode($response), $this->notif->code, json_encode($this->notif->messages));
            
            return $response;
        }

        $this->partner = PartnerModel::where('partner.api_key', '=', $this->apiKey)
                            ->first();
        if ($this->partner) {
            if ($this->checkToken($token) == false) { 
                $this->notif->addMessage('Invalid token');
            } else if ($this->checkProductExclude($productCode) == true) {
                $this->notif->addMessage('Unauthorized product');
            } else if ($this->partner->active == 'Y') {
                $this->productFee = $this->productFee($productCode);
                if ($this->productFee != null) {
                    $this->partnerDeposit = PartnerDepositModel::where('partner_id', '=', $this->partner->id)
                                                ->first();
                    
                    $queue = TrxRequestHelper::queueName();

                    if (strtolower($trxType) == PartnerTrxReqModel::TRX_TYPE_INQUIRY) {
                        $result = $this->inquiry();

                        if ($partnerCallbackURL != '') {
                            PartnerTrxRequestCallbackJob::dispatch($partnerCallbackURL, $result)->onQueue($queue);
                        }

                        return $result;
                    } else if (strtolower($trxType) == PartnerTrxReqModel::TRX_TYPE_ADVICE) {
                        $result = $this->advice();

                        if ($partnerCallbackURL != '') {
                            PartnerTrxRequestCallbackJob::dispatch($partnerCallbackURL, $result)->onQueue($queue);
                        }

                        return $result;
                    } else if (strtolower($trxType) == PartnerTrxReqModel::TRX_TYPE_BALANCE) {
                        $result = $this->balance();

                        if ($partnerCallbackURL != '') {
                            PartnerTrxRequestCallbackJob::dispatch($partnerCallbackURL, $result)->onQueue($queue);
                        }

                        return $result;
                    } else {
                        $result = $this->inquiry();
                        if ($this->trxId > 0) {                        
                            $result = $this->purchase($result);
                        }

                        if ($partnerCallbackURL != '') {
                            PartnerTrxRequestCallbackJob::dispatch($partnerCallbackURL, $result)->onQueue($queue);
                        }

                        return $result;
                    }
                } else {                
                    $this->notif->addMessage('Undefined Product');
                }
            } else {
                $this->notif->inactive();
            }
        } else {
            $this->notif->addMessage('Not Valid Partner.');
        }
        
        $response = $this->notif->build();
        
        if ($partnerCallbackURL != '') {
            PartnerTrxRequestCallbackJob::dispatch($partnerCallbackURL, $response)->onQueue($queue);
        }
        
        Log::info('['.$this->ip.' - '.$this->apiKey.'] /trx/request/'.strtolower($trxType).': '.json_encode($response));
        PartnerActivityLogJob::dispatch($this->ip, $this->apiKey, '/trx/request/'.strtolower($trxType), json_encode($this->request->input()), json_encode($response), $this->notif->code, json_encode($this->notif->messages));

        return $response;
    }
    
    function inquiry() {
        $productCode = $this->request->input('product', '');
        $billNumber = $this->request->input('billNumber', '');
        $trxType = strtoupper($this->request->input('type', PartnerTrxReqModel::TRX_TYPE_INQUIRY));
        $sign = $this->request->input('sign', '');
        $partnerCallbackURL = $this->request->input('callback_url', null);        
        
        $trxCode = $this->transactionCode();
        $retrievalCode = $this->retrievalCode($trxCode);
        
        try {
            Log::info('**** Start Biller connection '.PartnerTrxReqModel::TRX_TYPE_INQUIRY.' ****');
            $response = $this->callBiller(PartnerTrxReqModel::TRX_TYPE_INQUIRY, $productCode, $billNumber, $trxCode, $retrievalCode);
            Log::info('**** End Biller connection '.PartnerTrxReqModel::TRX_TYPE_INQUIRY.' ****');

            $responseBody = preg_replace('/[^[:print:]]/', ' ', $response['body']);
            $respArr = json_decode($responseBody, true);        
            $respDesc = isset($respArr['resultDesc']) ? $respArr['resultDesc'] : '';
            $respCode = '';
            $amount = 0;
            $billerFee = 0;
            $paymentCode = '';
            $whiteLabelBalance = 0;
            if ($response['status'] == 200) {
                $respCode =  isset($respArr['resultCode']) ? $respArr['resultCode'] : '';
                $amount = isset($respArr['nominal']) && $respArr['nominal'] != '' && is_numeric($respArr['nominal']) ? $respArr['nominal'] : 0;
                $billerFee = isset($respArr['admin']) && $respArr['admin'] != '' && is_numeric($respArr['admin']) ? $respArr['admin'] : 0;
                $paymentCode = isset($respArr['paymentCode']) && $respArr['paymentCode'] != '' ? $respArr['paymentCode'] : '';
                $whiteLabelBalance = isset($respArr['saldo']) && $respArr['saldo'] != '' && is_numeric($respArr['saldo']) ? $respArr['saldo'] : 0;
            }

            $adminFee = 0;
            if ($this->productFee->fee_type != PartnerTrxReqModel::PRODUCT_FEE_TYPE_INDIRECT) {
                $adminFee = $adminFee + $billerFee;

                if (strtoupper($this->productFee->additional_fee_type) == PartnerTrxReqModel::ADD_FEE_TYPE_PERCENTAGE) {
                    $adminFee = $adminFee + ($amount * ($this->productFee->additional_fee_value / 100));
                } else {
                    $adminFee = $adminFee + $this->productFee->additional_fee_value;
                }
                if (strtoupper($this->productFee->system_owner_fee_type) == PartnerTrxReqModel::ADD_FEE_TYPE_PERCENTAGE) {
                    $adminFee = $adminFee + ($amount * ($this->productFee->system_owner_fee_value / 100));
                } else {
                    $adminFee = $adminFee + $this->productFee->system_owner_fee_value;
                }
                if (strtoupper($this->productFee->white_label_fee_type) == PartnerTrxReqModel::ADD_FEE_TYPE_PERCENTAGE) {
                    $adminFee = $adminFee + ($amount * ($this->productFee->white_label_fee_value / 100));
                } else {
                    $adminFee = $adminFee + $this->productFee->white_label_fee_value;
                }
            }

            $trx = new PartnerTrxReqModel();
            $trx->partner_data_received_date = date('Y-m-d H:i:s');
            $trx->partner_id = $this->partner->id;
            $trx->partner_ip_address = $this->ip;
            $trx->partner_deposit_before = $this->partnerDeposit->current_deposit;
            $trx->partner_deposit_after = $this->partnerDeposit->current_deposit;
            $trx->trx_inquiry_id = 0;
            $trx->product_code = $productCode;
            $trx->bill_number = $billNumber;
            $trx->trx_code = $trxCode;
            $trx->retrieval = $retrievalCode;
            $trx->trx_type = PartnerTrxReqModel::TRX_TYPE_INQUIRY;
            $trx->partner_sign = $sign;
            $trx->callback_method = 'POST';
            $trx->callback_url = $partnerCallbackURL;
            $trx->biller_calling_date = date('Y-m-d H:i:s');
            $trx->biller_response_date = date('Y-m-d H:i:s');
            $trx->partner_data_responsed_date = date('Y-m-d H:i:s');
            $trx->product_denom = $amount;
            $trx->payment_code = $paymentCode;
            $trx->trx_status = $respCode != '' ? ($respCode == '000' ? PartnerTrxReqModel::TRX_STATUS_SUCCESS : $respCode) : PartnerTrxReqModel::TRX_STATUS_ERROR_CONNECTION;
            $trx->trx_status_desc = $respDesc;
            $trx->fee_type = $this->productFee->fee_type;
            $trx->additional_fee_type = $this->productFee->additional_fee_type;
            $trx->additional_fee_value = $this->productFee->additional_fee_value;
            $trx->system_owner_fee_type = $this->productFee->system_owner_fee_type;
            $trx->system_owner_fee_value = $this->productFee->system_owner_fee_value;
            $trx->white_label_fee_type = $this->productFee->white_label_fee_type;
            $trx->white_label_fee_value = $this->productFee->white_label_fee_value;
            $trx->end_user_fee_value = $this->productFee->end_user_fee_value;
            $trx->local_biller_fee = $this->productFee->local_biller_fee;
            $trx->biller_fee = $billerFee;
            $trx->bonus_type = $this->productFee->bonus_type;
            $trx->bonus_value = $this->productFee->bonus_value;
            $trx->system_owner_bonus_type = $this->productFee->system_owner_bonus_type;
            $trx->system_owner_bonus_value = $this->productFee->system_owner_bonus_value;
            $trx->white_label_bonus_type = $this->productFee->white_label_bonus_type;
            $trx->white_label_bonus_value = $this->productFee->white_label_bonus_value;
            $trx->partner_bonus_type = $this->productFee->partner_bonus_type;
            $trx->partner_bonus_value = $this->productFee->partner_bonus_value;
            $trx->white_label_current_balance = $whiteLabelBalance;
            $trx->partner_fee = $adminFee;
            $trx->biller_aggregator_id = $this->billerAggregator->id;
            $trx->biller_response = $responseBody;
            $trx->save();

            $this->trxId = $response['status'] == 200 ? $trx->id : 0;

            $this->notif->code = $respCode != '' ? ($respCode == '000' ? PartnerTrxReqModel::TRX_STATUS_SUCCESS : PartnerTrxReqModel::TRX_STATUS_ERROR_TRX) : PartnerTrxReqModel::TRX_STATUS_ERROR_CONNECTION;
            $this->notif->messages = [$respDesc];
            $this->notif->data = [
                'type' => strtolower(PartnerTrxReqModel::TRX_TYPE_INQUIRY),
                'service' => (isset($respArr['service']) ? $respArr['service'] : ''),
                'product' => $productCode,
                'billNumber' => $billNumber,
                'nominal' => (int) $amount,
                'admin' => (int) $adminFee,
                'paymentCode' => $paymentCode,
                'trxId' => $trxCode,
                'signature' => $sign,
                'info1' => (isset($respArr['info1']) ? $respArr['info1'] : ''),
                'info2' => (isset($respArr['info2']) ? $respArr['info2'] : ''),
                'info3' => (isset($respArr['info3']) ? $respArr['info3'] : ''),
                'jmlLembar' => (isset($respArr['jmlLembar']) ? $respArr['jmlLembar'] : '')
            ];

            $dataResponse = $this->notif->build();
            
            Log::info('['.$this->ip.' - '.$this->apiKey.'] /trx/request/'.PartnerTrxReqModel::TRX_TYPE_INQUIRY.': '.json_encode($this->request->input()).' - response: '.json_encode($dataResponse));
            PartnerActivityLogJob::dispatch($this->ip, $this->apiKey, '/trx/request/'.PartnerTrxReqModel::TRX_TYPE_INQUIRY, json_encode($this->request->input()), json_encode($dataResponse));

            return $dataResponse;
            
        } catch (\Exception $ex) {
            Log::error('['.$this->ip.' - '.$this->apiKey.'] /trx/request/'.PartnerTrxReqModel::TRX_TYPE_INQUIRY.': '.json_encode($this->request->input()).' - error: '.$ex->getMessage());
            PartnerActivityLogJob::dispatch($this->ip, $this->apiKey, '/trx/request/'.PartnerTrxReqModel::TRX_TYPE_INQUIRY, json_encode($this->request->input()), $ex->getMessage());
            $this->notif->internalServerError();
        }
        
        return $this->notif->build();
    }
    
    private function purchase($trxInquiry) {
        $this->notif->resetMessage();
        
        $productCode = $this->request->input('product', '');
        $billNumber = $this->request->input('billNumber', '');
        $trxType = $this->request->input('type', PartnerTrxReqModel::TRX_TYPE_PURCHASE);
        $sign = $this->request->input('sign', '');
        $partnerCallbackURL = $this->request->input('callback_url', null);
        
        $amount = $trxInquiry['data']['nominal'];
        $adminFee = $trxInquiry['data']['admin'];
        $paymentCode = $trxInquiry['data']['paymentCode'];
        
        try {
            $trxAmount = $amount + $adminFee;        
            if (strtoupper($this->partnerDeposit->paymode) == PartnerModel::PARTNER_PAYMODE_PREPAID) {
                $balance = $this->partnerDeposit->current_deposit - $this->partnerDeposit->prepaid_deposit_min;
                $charged = $trxAmount + $this->partnerDeposit->booking_charge;
                if ($charged < $balance) {
                    $this->partnerDeposit->booking_charge = $charged;
                    $this->partnerDeposit->save();
                } else {
                    $this->notif->addMessage('Insufficient balance', PartnerTrxReqModel::TRX_STATUS_INSUFFICIENT_BALANCE);
                }
            } else {
                $charged = $trxAmount + $this->partnerDeposit->current_deposit;
                if ($charged > $this->partnerDeposit->postpaid_deposit_max) {
                    $this->notif->addMessage('Exceed maximum deposit', PartnerTrxReqModel::TRX_STATUS_EXCEED_BALANCE_LIMIT);
                }
            }

            if ($this->notif->isOK() == true) {
                Log::info($trxType.' from Inquiry: '.json_encode($trxInquiry));

                $trxCode = $this->transactionCode();
                $retrievalCode = $this->retrievalCode($trxCode);

                Log::info('**** Start Biller connection '.strtoupper($trxType).' ****');
                $response = $this->callBiller($trxType, $productCode, $billNumber, $trxCode, $retrievalCode, $paymentCode);
                Log::info('**** End Biller connection '.strtoupper($trxType).' ****');

                $responseBody = preg_replace('/[^[:print:]]/', ' ', $response['body']);
                $respArr = json_decode($responseBody, true);
                $respDesc = isset($respArr['resultDesc']) ? $respArr['resultDesc'] : '';
                $respCode = '';
                $billerFee = 0;
                $whiteLabelBalance = 0;
                if ($response['status'] == 200) {
                    $respCode =  isset($respArr['resultCode']) ? $respArr['resultCode'] : '';
                    $billerFee = isset($respArr['admin']) && $respArr['admin'] != '' && is_numeric($respArr['admin']) ? $respArr['admin'] : 0;
                    $whiteLabelBalance = isset($respArr['saldo']) && $respArr['saldo'] != '' && is_numeric($respArr['saldo']) ? $respArr['saldo'] : 0;
                }

                $trx = new PartnerTrxReqModel();
                $trx->partner_deposit_before = $this->partnerDeposit->current_deposit;

                if ($this->partnerDeposit->booking_charge > 0) {
                    $this->partnerDeposit->booking_charge = $this->partnerDeposit->booking_charge - $trxAmount;
                }

                if ($respCode != PartnerTrxReqModel::TRX_STATUS_ERROR_TRX) {
                    if ($this->partnerDeposit->paymode == PartnerModel::PARTNER_PAYMODE_PREPAID) {
                        $this->partnerDeposit->current_deposit = $this->partnerDeposit->current_deposit - $trxAmount;
                    } else {
                        $this->partnerDeposit->current_deposit = $this->partnerDeposit->current_deposit + $trxAmount;
                    }
                }                
                $this->partnerDeposit->save();

                $trx->partner_data_received_date = date('Y-m-d H:i:s');
                $trx->partner_id = $this->partner->id;
                $trx->partner_ip_address = $this->ip;
                $trx->trx_inquiry_id = $this->trxId;
                $trx->product_code = $productCode;
                $trx->product_denom = $amount;
                $trx->payment_code = $paymentCode;
                $trx->bill_number = $billNumber;
                $trx->trx_code = $trxCode;
                $trx->retrieval = $retrievalCode;
                $trx->trx_type = $trxType;
                $trx->partner_sign = $sign;
                $trx->callback_method = 'POST';
                $trx->callback_url = $partnerCallbackURL;
                $trx->biller_calling_date = date('Y-m-d H:i:s');
                $trx->biller_response_date = date('Y-m-d H:i:s');
                $trx->partner_data_responsed_date = date('Y-m-d H:i:s');
                $trx->trx_status = $respCode != '' ? ($respCode == '000' ? PartnerTrxReqModel::TRX_STATUS_SUCCESS : $respCode) : PartnerTrxReqModel::TRX_STATUS_ERROR_CONNECTION;
                $trx->trx_status_desc = $respDesc;
                $trx->fee_type = $this->productFee->fee_type;
                $trx->additional_fee_type = $this->productFee->additional_fee_type;
                $trx->additional_fee_value = $this->productFee->additional_fee_value;
                $trx->system_owner_fee_type = $this->productFee->system_owner_fee_type;
                $trx->system_owner_fee_value = $this->productFee->system_owner_fee_value;
                $trx->white_label_fee_type = $this->productFee->white_label_fee_type;
                $trx->white_label_fee_value = $this->productFee->white_label_fee_value;
                $trx->end_user_fee_value = $this->productFee->end_user_fee_value;
                $trx->local_biller_fee = $this->productFee->local_biller_fee;
                $trx->biller_fee = $billerFee;
                $trx->bonus_type = $this->productFee->bonus_type;
                $trx->bonus_value = $this->productFee->bonus_value;
                $trx->system_owner_bonus_type = $this->productFee->system_owner_bonus_type;
                $trx->system_owner_bonus_value = $this->productFee->system_owner_bonus_value;
                $trx->white_label_bonus_type = $this->productFee->white_label_bonus_type;
                $trx->white_label_bonus_value = $this->productFee->white_label_bonus_value;
                $trx->partner_bonus_type = $this->productFee->partner_bonus_type;
                $trx->partner_bonus_value = $this->productFee->partner_bonus_value;
                $trx->white_label_current_balance = $whiteLabelBalance;
                $trx->partner_fee = $adminFee;
                $trx->partner_deposit_after = $this->partnerDeposit->current_deposit;
                $trx->biller_aggregator_id = $this->billerAggregator->id;
                $trx->biller_response = $responseBody;
                $trx->save();

                $this->notif->code = $respCode != '' ? ($respCode == '000' ? PartnerTrxReqModel::TRX_STATUS_SUCCESS : PartnerTrxReqModel::TRX_STATUS_ERROR_TRX) : PartnerTrxReqModel::TRX_STATUS_ERROR_CONNECTION;
                $this->notif->messages = [$respDesc];
                $this->notif->data = [
                    'type' => strtolower($trxType),
                    'service' => (isset($respArr['service']) ? $respArr['service'] : ''),
                    'product' => $productCode,
                    'billNumber' => $billNumber,
                    'nominal' => $amount,
                    'admin' => $adminFee,
                    'paymentCode' => $paymentCode,
                    'trxId' => $trxCode,
                    'signature' => $sign,
                    'current_balance' => (int) $this->partnerDeposit->current_deposit,
                    'info1' => (isset($respArr['info1']) ? $respArr['info1'] : ''),
                    'info2' => (isset($respArr['info2']) ? $respArr['info2'] : ''),
                    'info3' => (isset($respArr['info3']) ? $respArr['info3'] : ''),
                    'jmlLembar' => (isset($respArr['jmlLembar']) ? $respArr['jmlLembar'] : '')
                ];
            }

            $dataResponse = $this->notif->build();
            
            Log::info('['.$this->ip.' - '.$this->apiKey.'] /trx/request/'.strtolower($trxType).': '.json_encode($trxInquiry).' - response: '.json_encode($dataResponse));
            PartnerActivityLogJob::dispatch($this->ip, $this->apiKey, '/trx/request/'.strtolower($trxType), json_encode($trxInquiry), json_encode($dataResponse));

            return $dataResponse;
            
        } catch (\Exception $ex) {
            Log::error('['.$this->ip.' - '.$this->apiKey.'] /trx/request/'.strtolower($trxType).': '.json_encode($trxInquiry).' - error: '.$ex->getMessage());
            PartnerActivityLogJob::dispatch($this->ip, $this->apiKey, '/trx/request/'.strtolower($trxType), json_encode($this->request->input()), $ex->getMessage());
            $this->notif->internalServerError();
        }
        
        return $this->notif->build();
    }
    
    private function advice() {
        $productCode = $this->request->input('product', '');
        $billNumber = $this->request->input('billNumber', '');
        $sign = $this->request->input('sign', '');
        $paymentCode = $this->request->input('payment_code', '');
        $partnerCallbackURL = $this->request->input('callback_url', null);
        $retrievalCode = $this->request->input('retreival');
        $trxCode = $this->request->input('trx_code');
        
        if ($productCode == '') { $this->notif->addMessage('Product code is required'); }
        if ($billNumber == '') { $this->notif->addMessage('Bill number is required'); }
        if ($paymentCode == '') { $this->notif->addMessage('Payment code is required'); }
        if ($trxCode == '') { $this->notif->addMessage('Trx code is required'); }
        if ($retrievalCode == '') { $this->notif->addMessage('Retreival code is required'); }
        
        if ($this->notif->isOK() == false) {
            $dataResponse = $this->notif->build();
            Log::info('['.$this->ip.' - '.$this->apiKey.'] /trx/request/'.PartnerTrxReqModel::TRX_TYPE_ADVICE.': '.json_encode($this->request->input()).' - response: '.json_encode($dataResponse));
            PartnerActivityLogJob::dispatch($this->ip, $this->apiKey, '/trx/request/'.PartnerTrxReqModel::TRX_TYPE_ADVICE, json_encode($this->request->input()), json_encode($dataResponse));
            return $dataResponse;
        }
        
        try {
            $response = $this->callBiller(PartnerTrxReqModel::TRX_TYPE_ADVICE, $productCode, $billNumber, $trxCode, $retrievalCode);
            $resp = json_decode($response['body'], true);
            $respCode = $response['status'] == 200 ? (isset($resp['resultCode']) && $resp['resultCode'] == '000' ? PartnerTrxReqModel::TRX_STATUS_SUCCESS : PartnerTrxReqModel::TRX_STATUS_ERROR_TRX) : PartnerTrxReqModel::TRX_STATUS_ERROR_CONNECTION;
            $respDesc = isset($resp['resultDesc']) ? $resp['resultDesc'] : '';
            $amount = isset($resp['nominal']) && $resp['nominal'] != '' && is_numeric($resp['nominal']) ? $resp['nominal'] : 0;
            $billerFee = isset($resp['admin']) && $resp['admin'] != '' && is_numeric($resp['admin']) ? $resp['admin'] : 0;
            $whiteLabelBalance = isset($resp['saldo']) && $resp['saldo'] != '' && is_numeric($resp['saldo']) ? $resp['saldo'] : 0;

            $adminFee = $this->productFee->fee_type == PartnerTrxReqModel::PRODUCT_FEE_TYPE_DIRECT ? $billerFee + (strtoupper($this->productFee->additional_fee_type) == PartnerTrxReqModel::ADD_FEE_TYPE_PERCENTAGE ? ($amount * ($this->productFee->additional_fee_value / 100)) : $this->productFee->additional_fee_value) : 0;

            $this->notif->code = $respCode;
            $this->notif->messages = [$respDesc];
            $this->notif->data = [
                'type' => strtolower(PartnerTrxReqModel::TRX_TYPE_ADVICE),
                'service' => (isset($resp['service']) ? $resp['service'] : ''),
                'product' => $productCode,
                'billNumber' => $billNumber,
                'nominal' => (int) $amount,
                'admin' => (int) $adminFee,
                'paymentCode' => $paymentCode,
                'trxId' => $trxCode,
                'signature' => $sign,
                'info1' => (isset($resp['info1']) ? $resp['info1'] : ''),
                'info2' => (isset($resp['info2']) ? $resp['info2'] : ''),
                'info3' => (isset($resp['info3']) ? $resp['info3'] : ''),
                'jmlLembar' => (isset($resp['jmlLembar']) ? $resp['jmlLembar'] : '')
            ];

            $dataResponse = $this->notif->build();

            Log::info('['.$this->ip.' - '.$this->apiKey.'] /trx/request/'.PartnerTrxReqModel::TRX_TYPE_ADVICE.': '.json_encode($this->request->input()).' - response: '.json_encode($dataResponse));
            PartnerActivityLogJob::dispatch($this->ip, $this->apiKey, '/trx/request/'.PartnerTrxReqModel::TRX_TYPE_ADVICE, json_encode($this->request->input()), json_encode($dataResponse));

            return $dataResponse;
            
        } catch (\Exception $ex) {
            Log::error('['.$this->ip.' - '.$this->apiKey.'] /trx/request/'.PartnerTrxReqModel::TRX_TYPE_ADVICE.': '.json_encode($this->request->input()).' - error: '.$ex->errorMessage());
            PartnerActivityLogJob::dispatch($this->ip, $this->apiKey, '/trx/request/'.PartnerTrxReqModel::TRX_TYPE_ADVICE, json_encode($this->request->input()), $ex->errorMessage());
            $this->notif->internalServerError();
        }
        
        return $this->notif->build();
    }
    
    private function balance() {
        $sign = $this->request->input('sign', '');
        $partnerCallbackURL = $this->request->input('callback_url', null);
        $trxCode = $this->transactionCode();
        
        try {
            $trx = new PartnerTrxReqModel();
            $trx->partner_id = $this->partner->id;
            $trx->partner_ip_address = $this->ip;
            $trx->trx_inquiry_id = 0;
            $trx->product_code = '';
            $trx->product_denom = 0;
            $trx->payment_code = '';
            $trx->bill_number = '';
            $trx->trx_code = $trxCode;
            $trx->retrieval = '';
            $trx->trx_type = PartnerTrxReqModel::TRX_TYPE_BALANCE;
            $trx->callback_method = 'POST';
            $trx->callback_url = $partnerCallbackURL;
            $trx->trx_status = PartnerTrxReqModel::TRX_STATUS_SUCCESS;
            $trx->trx_status_desc = 'OK';
            $trx->fee_type = PartnerTrxReqModel::PRODUCT_FEE_TYPE_DIRECT;
            $trx->additional_fee_type = PartnerTrxReqModel::ADD_FEE_TYPE_FIXED;
            $trx->additional_fee_value = 0;
            $trx->system_owner_fee_type = PartnerTrxReqModel::ADD_FEE_TYPE_FIXED;
            $trx->system_owner_fee_value = 0;
            $trx->white_label_fee_type = PartnerTrxReqModel::ADD_FEE_TYPE_FIXED;
            $trx->white_label_fee_value = 0;
            $trx->end_user_fee_value = 0;
            $trx->local_biller_fee = 0;
            $trx->biller_fee = 0;
            $trx->bonus_type = PartnerTrxReqModel::ADD_FEE_TYPE_FIXED;
            $trx->bonus_value = 0;
            $trx->system_owner_bonus_type = PartnerTrxReqModel::ADD_FEE_TYPE_FIXED;
            $trx->system_owner_bonus_value = 0;
            $trx->white_label_bonus_type = PartnerTrxReqModel::ADD_FEE_TYPE_FIXED;
            $trx->white_label_bonus_value = 0;
            $trx->partner_bonus_type = PartnerTrxReqModel::ADD_FEE_TYPE_FIXED;
            $trx->partner_bonus_value = 0;
            $trx->white_label_current_balance = 0;
            $trx->partner_fee = 0;
            $trx->partner_sign = $sign;
            $trx->biller_aggregator_id = 0;
            $trx->biller_response = '';
            $trx->save();

            $this->notif->code = PartnerTrxReqModel::TRX_STATUS_SUCCESS;
            $this->notif->messages = ['OK'];
            $this->notif->data = ['signature' => $sign, 'balance' => (int) $this->partnerDeposit->current_deposit];

            if ($this->partnerDeposit->paymode == PartnerModel::PARTNER_PAYMODE_PREPAID) {
                $this->notif->data['minimum_balance'] = (int) $this->partnerDeposit->prepaid_deposit_min;
            } else {
                $this->notif->data['maximum_balance'] = (int) $this->partnerDeposit->postpaid_deposit_max;
            }

            $dataResponse = $this->notif->build();

            Log::info('['.$this->ip.' - '.$this->apiKey.'] /trx/request/'.PartnerTrxReqModel::TRX_TYPE_BALANCE.': '.json_encode($this->request->input()).' - response: '.json_encode($dataResponse));
            PartnerActivityLogJob::dispatch($this->ip, $this->apiKey, '/trx/request/'.PartnerTrxReqModel::TRX_TYPE_BALANCE, json_encode($this->request->input()), json_encode($dataResponse));

            return $dataResponse;
            
        } catch (\Exception $ex) {
            Log::error('['.$this->ip.' - '.$this->apiKey.'] /trx/request/'.PartnerTrxReqModel::TRX_TYPE_BALANCE.': '.json_encode($this->request->input()).' - error: '.$ex->getMessage());
            PartnerActivityLogJob::dispatch($this->ip, $this->apiKey, '/trx/request/'.PartnerTrxReqModel::TRX_TYPE_BALANCE, json_encode($this->request->input()), $ex->getMessage());
            $this->notif->internalServerError();
        }
        
        return $this->notif->build();
    }
        
    private function callBiller($trxType, $productCode, $billNumber, $trxCode, $retrievalCode, $paymentCode = '') {
        $aggKey = $this->partner->white_label_id.'_'.$this->partner->id.'_'.$productCode.'_biller_aggregator';
        $this->billerAggregator = Cache::remember($aggKey, 604800, function () use ($productCode) {
            $agg = new \stdClass();
            $agg->id = 0;
            $agg->url_endpoint = '';
            $agg->accountType = '';
            $agg->account = '';
            $agg->institutionCode = '';
            $agg->key1 = '';
            $agg->key2 = '';
            $agg->clientId = '';
            $agg->clientSecret = '';
            
            $q = BillerAggratorModel::where('partner_products.product_id', '=', $productCode)
                    ->join('partner_products', 'biller_aggregator.id', '=', 'partner_products.biller_aggregator_id')
                    ->join('biller_aggregator_config', 'biller_aggregator.id', '=', 'biller_aggregator_config.biller_aggregator_id')
                    ->selectRaw('biller_aggregator.id as bag_id, biller_aggregator.url_endpoint')
                    ->selectRaw('biller_aggregator_config.config_name, biller_aggregator_config.config_value')
                    ->get();
            if ($q->isEmpty()) {
                $q = BillerAggratorModel::where('white_label_products.product_id', '=', $productCode)
                        ->join('white_label_products', 'biller_aggregator.id', '=', 'white_label_products.biller_aggregator_id')
                        ->join('biller_aggregator_config', 'biller_aggregator.id', '=', 'biller_aggregator_config.biller_aggregator_id')
                        ->selectRaw('biller_aggregator.id as bag_id, biller_aggregator.url_endpoint')
                        ->selectRaw('biller_aggregator_config.config_name, biller_aggregator_config.config_value')
                        ->get();
            }
            
            foreach ($q as $row) {
                if ($agg->id == 0) { $agg->id = $row->bag_id; }
                if ($agg->url_endpoint == '') { $agg->url_endpoint = $row->url_endpoint; }
                if ($row->config_name == 'accountType' && $agg->accountType == '') { $agg->accountType = $row->config_value; }
                if ($row->config_name == 'account' && $agg->account == '') { $agg->account = $row->config_value; }
                if ($row->config_name == 'institutionCode' && $agg->institutionCode == '') { $agg->institutionCode = $row->config_value; }
                if ($row->config_name == 'key1' && $agg->key1 == '') { $agg->key1 = $row->config_value; }
                if ($row->config_name == 'key2' && $agg->key2 == '') { $agg->key2 = $row->config_value; }
                if ($row->config_name == 'client_id' && $agg->clientId == '') { $agg->clientId = $row->config_value; }
                if ($row->config_name == 'client_secret' && $agg->clientSecret == '') { $agg->clientSecret = $row->config_value; }
            }
                
            return $agg;
        });
        Log::info('biller aggregator: '.json_encode($this->billerAggregator));

        //=============##### for load test only #####=============
//        sleep(3);
//        if ($trxType == PartnerTrxReqModel::TRX_TYPE_INQUIRY) {
//            return ['status' => 200, 'body' => '{"type":"inquiry","service":"responseInquiry","accountType":"B2B","account":"0000000018","institutionCode":"180309000002","product":"V010TSI","billNumber":"0822820996201","resultCode":"000","resultDesc":"SUKSES","nominal":"9910","admin":"2090","trxId":"drjh7l7d","retrieval":"0015415701886296","paymentCode":"e76014ac486f29ea8b26cc53d269b447","info1":"JNS VOUCHER : TELKOMSEL SIMPATI|NO.HANDPHONE: 0822820996201|JML VOUCHER :Rp.    10.000|","info2":"PEMBELIAN VOUCHER TELKOMSEL|##1","info3":"","jmlLembar":"1","saldo":"","signature":""}'];
//        }
//        return ['status' => 200, 'body' => '{"type":"payment","service":"responsePayment","accountType":"B2B","account":"0000000018","institutionCode":"180309000002","product":"V010TSI","billNumber":"0822820996201","resultCode":"000","resultDesc":"SUKSES","nominal":"9910","admin":"2090","trxId":"40429915","retrieval":"0015415805060150","paymentCode":"32c5245d026829f5b114f1d954c86cbf","info1":"NO. HANDPHONE : 0822820996201|NILAI VOUCHER : 10000|NO.REF        : 0000000000020257|","info2":"PEMBELIAN PULSA TELKOMSEL|##Untuk keluhan Hub.133|Npwp TSEL: 01.718.327.8.093.000","info3":"","jmlLembar":"1","saldo":"877821789","signature":""}'];
        //=============##### for load test only #####=============
        
        $signature = strtolower($trxType).$this->billerAggregator->account.$this->billerAggregator->institutionCode;
        $signature .= $productCode.$billNumber.$trxCode.$this->billerAggregator->key1.date('Y-m-d').$this->billerAggregator->key2;
//        Log::info('signature: '.$signature);
        $signatureMD5 = md5($signature);
        
        $authParams = $this->billerAggregator->clientId.':'.$this->billerAggregator->clientSecret;
        
        $params = [
            "type" => strtolower($trxType),
            "accountType" => $this->billerAggregator->accountType,
            "account" => $this->billerAggregator->account,
            "institutionCode" => $this->billerAggregator->institutionCode,
            "product" => $productCode,
            "billNumber" => $billNumber,
            "trxId" => $trxCode,
            "retrieval" => $retrievalCode,
            "paymentCode" => $paymentCode,
            "sign" => $signatureMD5
        ];
        Log::info('biller params: '.json_encode($params));

        Log::info('['.$trxCode.'] calling biller: '.json_encode($params));
//        if (strtolower($trxType) != PartnerTrxReqModel::TRX_TYPE_INQUIRY) {
//            $this->billerAggregator->url_endpoint = 'https://h2h-stg.bedigital.co.id/h2h/timeout';
//        }
        $response = HttpClientHelper::invoke($this->billerAggregator->url_endpoint, 'POST', $params, $trxCode, $authParams);
        Log::info('['.$trxCode.'] biller response: '.$response['status'].' - '.$response['body']);
        
        return $response;
    }
    
    private function productFee($productCode) {
        $whiteLabelId = $this->partner->white_label_id;
        $partnerId = $this->partner->id;
        $feeKey = $whiteLabelId.'_'.$partnerId.'_'.$productCode.'_fee';
        $fee = Cache::remember($feeKey, 604800, function () use ($whiteLabelId, $partnerId, $productCode) {
            $partnerFee = PartnerProductModel::where('partner_id', '=', $partnerId)
                            ->where('product_id', '=', $productCode)
                            ->select('fee_type', 'additional_fee_type', 'additional_fee_value',
                                    'system_owner_fee_type', 'system_owner_fee_value',
                                    'white_label_fee_type', 'white_label_fee_value',
                                    'end_user_fee_value', 'local_biller_fee',
                                    'bonus_type', 'bonus_value',
                                    'system_owner_bonus_type', 'system_owner_bonus_value',
                                    'white_label_bonus_type', 'white_label_bonus_value',
                                    'partner_bonus_type', 'partner_bonus_value')
                            ->first();
            if ($partnerFee) {
                return $partnerFee;
            } else {
                $wlFee = WhiteLabelProductModel::where('white_label_id', '=', $whiteLabelId)
                            ->where('product_id', '=', $productCode)
                            ->select('fee_type', 'additional_fee_type', 'additional_fee_value',
                                    'system_owner_fee_type', 'system_owner_fee_value',
                                    'white_label_fee_type', 'white_label_fee_value',
                                    'end_user_fee_value', 'local_biller_fee',
                                    'bonus_type', 'bonus_value',
                                    'system_owner_bonus_type', 'system_owner_bonus_value',
                                    'white_label_bonus_type', 'white_label_bonus_value',
                                    'partner_bonus_type', 'partner_bonus_value')
                            ->first();
                if ($wlFee) {
                    return $wlFee;
                }
            }
        });
        
        return $fee;
    }
    
    private function checkProductExclude($productCode) {
        $partnerId = $this->partner->id;
        $key = $this->partner->white_label_id.'_'.$partnerId.'_'.$productCode.'_exclude';        
        
        $exc = Cache::remember($key, 604800, function () use ($partnerId, $productCode){
            $q = PartnerProductExcludeModel::where('partner_id', '=', $partnerId)
                    ->where('product_id', '=', $productCode)
                    ->first();
            
            return ($q ? true : false);
        });

        return $exc;
    }
    
    private function checkToken($token) {
        if ($token != '') {
            $secretKey = $this->partner->secret_key;
            $apiKey = $this->partner->api_key;
            $seconValid = 30;
            $timeNow = gmdate('U') - $seconValid;

            for ($i=0; $i<($seconValid * 2); $i++) {
                $check = md5($secretKey.$apiKey.($timeNow+$i));
                if (strcmp($token, $check) == 0) {
                    return true;
                }
            }        
        }
        
        return false;
    }
    
    private function transactionCode() {
        $key = rand(1000, 9999).rand(1000, 9999);
        
        return $key;
    }
    
    private function retrievalCode($trxCode = '') {
        return UtilCommon::microtimeStamp().'-be-'.$trxCode;
    }
    
    static function queueName() {
        return 'h2h-worker'.rand(0, 3);
    }
    
}

