<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;

use App\Jobs\PartnerActivityLogJob;

class PartnerActivityLogHelper {

    static function log($ipAddress, $apiKey, $resourceName, $params='', $response='') {

        PartnerActivityLogJob::dispatch($ipAddress, $apiKey, $resourceName, $params, $response);

    }

}
