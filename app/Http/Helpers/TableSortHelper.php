<?php

namespace App\Http\Helpers;

class TableSortHelper {
	
	var $url = '';
	var $fields = array();
	var $q = '';
	var $mnt = 0;
	var $yr = 0;
	var $_columns = array();
	
	var $db;
	var $rowCount = 0;
	var $currPage = 1;
	var $itemPerPage = 15;
	var $skippedRows = 0;
	var $_todatesPage = 1;
    var $todatesPageInBlock = 5;
    //week is added by amin
    var $week = 1;
	
	public function __construct() {
		
	}
	
	public function setupSorter($url, $fields, $q, $mnt=0, $yr=0,$fromdates=0,$todates=0,$tableselect=0,$search=0,$daterange=0,$tableselect2=0,$search2=0,$opsidates=0,$week=1) {
		$this->url = $url;
		$this->fields = $fields;
		$this->q = $q;
		$this->mnt = $mnt;
		$this->yr = $yr;
 		$this->todates = $todates;
		$this->fromdates = $fromdates;
		$this->tableselect = $tableselect;
		$this->search = $search;	

		$this->tableselect2 = $tableselect2;
		$this->search2 = $search2;
		$this->daterange = $daterange;
		$this->opsidates = $opsidates;
		$this->week = $week;

		$icon = 'glyphicon-chevron-up';
		 

		$q = '?q='.urlencode($q);
		
		for ($i=0; $i<count($fields); $i++) {
			$mnt = '&mnt='.$mnt;
			$yr = '&yr='.$yr;
			$fromdates = '&fromdates='.$fromdates; 
			$todates = '&todates='.$todates; 
			$search = '&search='.$search; 
			$tableselect = '&tableselect='.$tableselect;   
			$daterange = '&daterange='.$daterange;   
			$tableselect2 = '&tableselect2='.$tableselect2;
			$search2 = '&search2='.$search2;  
			$opsidates = '&opsidates='.$opsidates;  
			$week= '&week='.$week;
		    $this->_columns[$i] = '<a href="#">'.$fields[$i][1].'</a>';
			 
		}
	}
	
	public function field($index) {
		if (isset($this->_columns[$index])) {
			return $this->_columns[$index];
		}
		
		return '&nbsp;';
	}
	
	public function searchFor() {
		return $this->q;
	}
	
	public function sortedField() {
		if (isset($this->fields[$this->mnt])) {
			return $this->fields[$this->mnt][0];
		}
		
		return $this->fields[0][0];
	}
	
	public function sortMethod() {
		return $this->yr == 1 ? 'desc' : 'asc';
	}
	
	public function setupPaging($db, $currentPage, $itemPerPage = 15) {
		$this->db = $db;
		$this->rowCount = $db->count();
		$this->itemPerPage = $itemPerPage;		
		$this->skippedRows = ($currentPage - 1) * $itemPerPage;
		$this->_todatesPage = ceil($this->rowCount / $itemPerPage);
		$this->currPage = $currentPage < 1 ? 1 : ($currentPage > $this->_todatesPage ? $this->_todatesPage : $currentPage);
	}
	
	public function pageRows() {
		/*
		return $this->db				
				->orderBy($this->sortedField(), $this->sortMethod())
				->skip($this->skippedRows)
				->take($this->itemPerPage)
				->get();
		*/	
		//Modified by Amin @2019-10-07	
		return $this->db				
				->skip($this->skippedRows)
				->take($this->itemPerPage)
				->get();
	}
	
	public function rowCount() {
		return $this->rowCount;
	}
	
	public function currentPage() {
		return $this->currPage;
	}
	
	public function previousButton() {
		if ($this->currPage > 1) {
			$page = $this->currPage - 1;	
			$url = $this->url.'?pg='.$page.'&q='.urlencode($this->searchFor());
			$url .= '&mnt='.$this->mnt.'&yr='.$this->yr.'&week='.$this->week;
			
			return '<div class="pull-left"><a class="btn btn-default btn-yr" href="javascript:getContentInTable(\''.url($url).'\');"><i class="glyphicon glyphicon-arrow-left"></i> Prev</a></div>';
		}
		
		return "";
	}
	
	public function nextButton() {		
		if ($this->_todatesPage > 1 && $this->currPage < $this->_todatesPage) {
			$page = $this->currPage + 1;
			$url = $this->url.'?pg='.$page.'&q='.urlencode($this->searchFor());
			$url .= '&mnt='.$this->mnt.'&yr='.$this->yr.'&week='.$this->week;
			
			return '<div class="pull-right"><a class="btn btn-default btn-yr" href="javascript:getContentInTable(\''.url($url).'\');">Next <i class="glyphicon glyphicon-arrow-right"></i></a></div>';
		}
		
		return "";
	}
    
    public function pagination() {
        $paging = '';
        $button = '';
        
		if ($this->currPage > 1) {
			$page = $this->currPage - 1;	
			$url = $this->url.'?pg='.$page.'&q='.urlencode($this->searchFor());
			$url .= '&mnt='.$this->mnt.'&yr='.$this->yr;
			$url .= '&fromdates='.$this->fromdates.'&todates='.$this->todates;
			$url .= '&search='.$this->search.'&tableselect='.$this->tableselect.'&daterange='.$this->daterange.'&tableselect2='.$this->tableselect2.'&search2='.$this->search2.'&opsidates='.$this->opsidates.'&week='.$this->week; 
            $button .= '<a class="btn btn-default" href="javascript:getContentInTable(\''.url($url).'\');">&larr; Prev</a>';
        } else {
            $button .= '<span class="btn btn-default disabled">&larr; Prev</span>';
        }
        
        if ($this->_todatesPage > 1) {
            $end = $this->todatesPageInBlock * ceil(($this->currPage / $this->todatesPageInBlock));
            $start = $end - ($this->todatesPageInBlock - 1);
            for ($i=$start; $i<=$end; $i++) {
                if ($i > $this->_todatesPage) {
                    break;
                }
                
                $url = $this->url.'?pg='.$i.'&q='.urlencode($this->searchFor());
                $url .= '&mnt='.$this->mnt.'&yr='.$this->yr;
			$url .= '&fromdates='.$this->fromdates.'&todates='.$this->todates;
			$url .= '&search='.$this->search.'&tableselect='.$this->tableselect.'&daterange='.$this->daterange.'&tableselect2='.$this->tableselect2.'&search2='.$this->search2.'&opsidates='.$this->opsidates.'&week='.$this->week;  
                if ($i != $this->currPage) {
                    $button .= '<a class="btn btn-default" href="javascript:getContentInTable(\''.url($url).'\');">'.$i.'</a>';
                } else {
                    $button .= '<span class="btn btn-info disabled">'.$i.'</span>';
                }
            }
        } else {
            $button .= '<span class="btn btn-info disabled">1</span>';
        }

        if ($this->_todatesPage > 1 && $this->currPage < $this->_todatesPage) {
			$page = $this->currPage + 1;
			$url = $this->url.'?pg='.$page.'&q='.urlencode($this->searchFor());
			$url .= '&mnt='.$this->mnt.'&yr='.$this->yr;
			$url .= '&fromdates='.$this->fromdates.'&todates='.$this->todates;
			$url .= '&search='.$this->search.'&tableselect='.$this->tableselect.'&daterange='.$this->daterange.'&tableselect2='.$this->tableselect2.'&search2='.$this->search2.'&opsidates='.$this->opsidates.'&week='.$this->week; 
            $button .= '<a class="btn btn-default" href="javascript:getContentInTable(\''.url($url).'\');">Next &rarr;</a>';
        } else {
            $button .= '<span class="btn btn-default disabled">Next &rarr;</span>';
        }
        
        $paging = '<div class="btn-group pull-right">';
        $paging .= $button;
        $paging .= '</div>';
        
        return $paging;
    }
    //Added by Amin @2019-10-04
    public function getItemPerPage() {
    	return $this->itemPerPage;
    }
    //Added by Amin @2019-10-10
    public function setWeek($week) {
    	$this->week = $week;
    }

}

