<?php

namespace App\Http\Helpers;

use Illuminate\Support\Facades\Session;
use App\Models\admin\WhiteLabelAdminModel;

class SessionHelper {

    private $sessionId  = '__FASTKU__';
    private $session    = null;
	
    public function save($data) {
        if($this->exist()) {
            $this->remove();
        }
		
        Session::put($this->sessionId, $data);
    }
	
	public function saveCompanyId($id) {
		Session::put($this->sessionId, ['company_id' => $id]);
	}
    
    public function exist() {
        $isTrue = false;
        
        if ($this->get('user_id') != '' && $this->get('session_id') != '') {
            // $q = WhiteLabelAdminModel::where('username', '=', $this->get('username'))
            // $q = WhiteLabelAdminModel::where('token', '=', $this->get('token'))
                    // ->first();
            // if ($q != null) {
                $isTrue = true;
            // }
        }
        
        return $isTrue;
    }
    
    public function get($key) {
        if($this->session == null) {
            $this->session = Session::has($this->sessionId) ? Session::get($this->sessionId) : [];
        }
        
        return isset($this->session[$key]) ? $this->session[$key] : '';
    }
    
    public function whiteLabelId() {
        return $this->get('white_label_id');
    }
    
    public function remove() {
        Session::forget($this->sessionId);
    }
	
	public function isAdmin() {
		return $this->get('admin_type') == 'ADMIN';
	}
	
	public function authorizedPath($path) {
		if ($this->exist()) {
			if (!$this->isAdmin()) {
				$els = explode('/', $path);
				if (count($els) > 1 && $els[0] == AdminContrller::ROOT_PATH) {
					if (!in_array($els[1], AdminContrller::UNAUTH_PATHS)) {
						$auths = explode(',', $this->get('auth_paths'));

						return in_array($els[1], $auths);
					}
				}
			}

			return true;
		}
		
		return false;
	}
}

