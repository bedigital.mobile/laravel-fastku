<?php

namespace App\Http\Helpers;

class TableSortHelperSimple {
	
	var $url = '';
	var $fields = array();
	var $q = '';
	var $sf = 0;
	var $sm = 0;
	var $_columns = array();
	
	var $db;
	var $rowCount = 0;
	var $currPage = 1;
	var $itemPerPage = 15;
	var $skippedRows = 0;
	var $_maxPage = 1;
    var $maxPageInBlock = 5;
	
	public function __construct() {
		
	}
	
	public function setupSorter($url, $fields, $q, $sf=0, $sm=0,$min=0,$max=0,$tableselect=0,$search=0) {
		$this->url = $url;
		$this->fields = $fields;
		$this->q = $q;
		$this->sf = $sf;
		$this->sm = $sm;
 		$this->max = $max;
		$this->min = $min;
		$this->tableselect = $tableselect;
		$this->search = $search;

		$icon = 'glyphicon-chevron-up';
		if ($sm == 1) {
			$icon = 'glyphicon-chevron-down';
		}

		$q = '?q='.urlencode($q);
		
		for ($i=0; $i<count($fields); $i++) {
			$sortField = '&sf='.$i;
			$sortMethod = '&sm='.($sm == 1 ? 0 : 1);
			$min = '&min='.$min; 
			$max = '&max='.$max; 
			$search = '&search='.$search; 
			$tableselect = '&tableselect='.$tableselect;   
			if ($this->sf == $i) {
                $this->_columns[$i] = $fields[$i][1].' <a href="javascript:getContentInTable(\''.$this->url.$q.$sortField.$sortMethod.'\');"><i class="glyphicon '.$icon.'"></i></a>';
			} else {
                $this->_columns[$i] = '<a href="javascript:getContentInTable(\''.$this->url.$q.$sortField.$sortMethod.$min.$max.$tableselect.$search.'\');">'.$fields[$i][1].'</a>';
			}
		}
	}
	
	public function field($index) {
		if (isset($this->_columns[$index])) {
			return $this->_columns[$index];
		}
		
		return '&nbsp;';
	}
	
	public function searchFor() {
		return $this->q;
	}
	
	public function sortedField() {
		if (isset($this->fields[$this->sf])) {
			return $this->fields[$this->sf][0];
		}
		
		return $this->fields[0][0];
	}
	
	public function sortMethod() {
		return $this->sm == 1 ? 'desc' : 'asc';
	}
	
	public function setupPaging($db, $currentPage, $itemPerPage = 15) {
		$this->db = $db;
		$this->rowCount = $db->count();
		$this->itemPerPage = $itemPerPage;		
		$this->skippedRows = ($currentPage - 1) * $itemPerPage;
		$this->_maxPage = ceil($this->rowCount / $itemPerPage);
		$this->currPage = $currentPage < 1 ? 1 : ($currentPage > $this->_maxPage ? $this->_maxPage : $currentPage);
	}
	
	public function pageRows() {
		return $this->db				
				->orderBy($this->sortedField(), $this->sortMethod())
				->skip($this->skippedRows)
				->take($this->itemPerPage)
				->get();
	}
	
	public function rowCount() {
		return $this->rowCount;
	}
	
	public function currentPage() {
		return $this->currPage;
	}
	
	public function previousButton() {
		if ($this->currPage > 1) {
			$page = $this->currPage - 1;	
			$url = $this->url.'?pg='.$page.'&q='.urlencode($this->searchFor());
			$url .= '&sf='.$this->sf.'&sm='.$this->sm;
			
			return '<div class="pull-left"><a class="btn btn-default btn-sm" href="javascript:getContentInTable(\''.url($url).'\');"><i class="glyphicon glyphicon-arrow-left"></i> Prev</a></div>';
		}
		
		return "";
	}
	
	public function nextButton() {		
		if ($this->_maxPage > 1 && $this->currPage < $this->_maxPage) {
			$page = $this->currPage + 1;
			$url = $this->url.'?pg='.$page.'&q='.urlencode($this->searchFor());
			$url .= '&sf='.$this->sf.'&sm='.$this->sm;
			
			return '<div class="pull-right"><a class="btn btn-default btn-sm" href="javascript:getContentInTable(\''.url($url).'\');">Next <i class="glyphicon glyphicon-arrow-right"></i></a></div>';
		}
		
		return "";
	}
    
    public function pagination() {
        $paging = '';
        $button = '';
        
		if ($this->currPage > 1) {
			$page = $this->currPage - 1;	
			$url = $this->url.'?pg='.$page.'&q='.urlencode($this->searchFor());
			$url .= '&sf='.$this->sf.'&sm='.$this->sm;
			$url .= '&min='.$this->min.'&max='.$this->max;
			$url .= '&search='.$this->search.'&tableselect='.$this->tableselect; 
            $button .= '<a class="btn btn-default" href="javascript:getContentInTable(\''.url($url).'\');">&larr; Prev</a>';
        } else {
            $button .= '<span class="btn btn-default disabled">&larr; Prev</span>';
        }
        
        if ($this->_maxPage > 1) {
            $end = $this->maxPageInBlock * ceil(($this->currPage / $this->maxPageInBlock));
            $start = $end - ($this->maxPageInBlock - 1);
            for ($i=$start; $i<=$end; $i++) {
                if ($i > $this->_maxPage) {
                    break;
                }
                
                $url = $this->url.'?pg='.$i.'&q='.urlencode($this->searchFor());
                $url .= '&sf='.$this->sf.'&sm='.$this->sm;
			$url .= '&min='.$this->min.'&max='.$this->max;
			$url .= '&search='.$this->search.'&tableselect='.$this->tableselect; 
                if ($i != $this->currPage) {
                    $button .= '<a class="btn btn-default" href="javascript:getContentInTable(\''.url($url).'\');">'.$i.'</a>';
                } else {
                    $button .= '<span class="btn btn-info disabled">'.$i.'</span>';
                }
            }
        } else {
            $button .= '<span class="btn btn-info disabled">1</span>';
        }

        if ($this->_maxPage > 1 && $this->currPage < $this->_maxPage) {
			$page = $this->currPage + 1;
			$url = $this->url.'?pg='.$page.'&q='.urlencode($this->searchFor());
			$url .= '&sf='.$this->sf.'&sm='.$this->sm;
			$url .= '&min='.$this->min.'&max='.$this->max;
			$url .= '&search='.$this->search.'&tableselect='.$this->tableselect; 
            $button .= '<a class="btn btn-default" href="javascript:getContentInTable(\''.url($url).'\');">Next &rarr;</a>';
        } else {
            $button .= '<span class="btn btn-default disabled">Next &rarr;</span>';
        }
        
        $paging = '<div class="btn-group pull-right">';
        $paging .= $button;
        $paging .= '</div>';
        
        return $paging;
    }
}

