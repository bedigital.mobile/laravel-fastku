<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;

use App\Models\PartnerModel;
use App\Models\PartnerTrxReqModel;

class PartnerDepositCheckingHelper {
    
    var $partner;
    var $product;
    
    public function __construct($partner, $product) {
        $this->partner = $partner;
        $this->product = $product;
    }
    
    function isOK() {
        $isTrue = true;
        
        if ($this->partner['paymode'] == PartnerModel::PARTNER_PAYMODE_PREPAID) {
            $booking = $this->product['denom'] + $this->partner['bookingCharge'];
            if ($booking > $this->partner['currentBalance']) {
                $isTrue = false;
            }
        }
        
        return $isTrue;
    }
    
}

