<?php

namespace App\Http\Helpers;

use App\Http\Helpers\SessionHelper;

class PasswordHelper {
    
    static function masterAgentEncrypt($username, $pwd) {
        $pre = '';
        $prefix = (new SessionHelper())->whiteLabelId();
        $str = strtoupper(substr($prefix, -3, 3));
        $pre = base_convert($str, 10, 36);
//        echo 'prefix: '.$prefix.' - '.$str.' - '.$pre.'<br/>';
        
        $suf = strtoupper(substr($username, -3, 3));
//        echo 'suffix: '.$username.' - '.$suf.'<br/>';
        
        $password = md5($pre.$pwd.$suf);
        
        return $password;
    }
    
}

