<?php

namespace App\Http\Helpers;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class HttpClientHelper {
    
    static function invoke($url, $method = 'GET', $params = [], $sign = '', $authParams = '') {
        $result = [
            'status' => 500,
            'body' => ''
        ];
        
        try {
            $paramKey = 'json';
            $headers = [
                'Content-type'      => 'application/json',
                'Accept'            => 'application/json',
                'Content-length'    => strlen(json_encode($params)),
                'Authorization' 	=> base64_encode(($authParams != '' ? $authParams : 'be'.time())),
				'Origin' 			=> 'https://116.90.163.10'
            ];
            
            if ($method == 'GET') {
                $paramKey = 'query';
                $headers = [
                    'Content-type' => 'text/html'
                ];
            }
            
            $sendParams = ['headers' => $headers, $paramKey => $params];
//            Log::info('['.$sign.'] call sync worker: '.$url.', params: '.json_encode($sendParams));

            $client = new Client(['timeout' => 30, 'verify' => false]);
            $response = $method == 'GET' ? $client->get($url, $sendParams) : $client->post($url, $sendParams);
            Log::info('call sync response: '.$response->getStatusCode().' - '.$response->getReasonPhrase());
            
            $result = [
                'status' => $response->getStatusCode(),
                'body' => $response->getBody()
            ];
        } catch (\GuzzleHttp\Exception\RequestException $ex) {
            report($ex);
            Log::info('[ERROR] - call sync: Request failed: '.$ex->getCode());
            $result = [
                'status' => $ex->getCode(),
                'body' => json_encode(['resultDesc' => 'Pending'])
            ];
        } catch (\GuzzleHttp\Exception\BadResponseException $ex) {
            report($ex);
            Log::info('[ERROR] - call sync: Response failed: '.$ex->getCode());
            $result = [
                'status' => $ex->getCode(),
                'body' => json_encode(['resultDesc' => 'Bad Response'])
            ];
        } catch (\GuzzleHttp\Exception\ServerException $ex) {
            report($ex);
            Log::info('[ERROR] - call sync: Server failed: '.$ex->getCode());
            $result = [
                'status' => $ex->getCode(),
                'body' => json_encode(['resultDesc' => 'Internal Server Error'])
            ];
        }
        
        return $result;
    }
    
    static function invokeAsync($url, $method = 'POST', $params = [], $sign = '') {
        try {            
            $paramKey = 'json';
            $headers = [
                'Content-type' => 'application/json',
                'Accept' => 'application/json',
                'Content-length' => strlen(json_encode($params)),
                'Authorization' => base64_encode('be'.time()),
				'Origin' => Request::getHost()
            ];
            
            if ($method == 'GET') {
                $paramKey = 'query';
                $headers = [
                    'Content-type' => 'text/html'
                ];
            }
            $sendParams = ['headers' => $headers, $paramKey => $params];
//            Log::info('['.$sign.'] call async worker: '.$url.', params: '.json_encode($sendParams));
            
            $client = new Client(['timeout' => 30]);
            $promise = $client->requestAsync($method, $url, $sendParams);
            $promise->then(
//                Log::info('call async worker: succeeded'),
                function (RequestException $e) {
                    Log::info('call async worker failed:'.$e->getMessage());
                }
            );
            $promise->wait();
        } catch (Exception $ex) {
            Log::info('call async worker failed: '.$ex->getMessage());
        }
    }
    
}

