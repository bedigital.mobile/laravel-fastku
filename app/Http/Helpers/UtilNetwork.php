<?php

namespace App\Http\Helpers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Exception;
use App\Exceptions\Handler;


class UtilNetwork
{
	
	public static function getMyServerIP( $toArray = false ) {
		$ip = "";
		try {
			if ($toArray) {
				$ip = array();
				$output = null;
				$ip = exec("ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'", $output);
				if (! is_null($output)) {
					foreach ($output as $line) {
						$ip[] = trim($line);
					}
				}
			} else {
				$ip = exec("ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'");
			}
		} catch ( Exception $e ) {
			Log::info("UtilNetwork::getMyServerIP exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $ip;
	}
	
	public static function getClientIP() {
		$phpNewVersion = false;
		if (version_compare(phpversion(), "5.0.0", ">=")) {
			$phpNewVersion = true;
		}
		$debug = boolval(Config::get('app.debug'));
		// if ( $debug ) Log::info("UtilNetwork::getClientIP SERVER = " . json_encode( $_SERVER ) );
		$ip_keys = array('HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'HTTP_X_REAL_IP', 'HTTP_CLIENT_IP', 'HTTP_X_CLUSTER_CLIENT_IP', 'REMOTE_ADDR');
		$arrIPLocal = array();
		foreach ($ip_keys as $key) {
			try {
				if (array_key_exists($key, $_SERVER) === true) {
					// if ($debug) Log::info("UtilNetwork::getClientIP " . $key . " = " . $_SERVER[$key] );
					$listIP = explode(',', $_SERVER[$key]);
					foreach ($listIP as $ip) {
						try {
							// trim for safety measures
							$ip = trim($ip);
							// Skip RFC 1918 IP's 10.0.0.0/8, 172.16.0.0/12 and 192.168.0.0/16
							// add from internal: 172.31.19.* and 172.19.91.*
							if ($ip != '127.0.0.1' && !preg_match('/^(?:10|172\.(?:1[6-9]|2\d|3[01])|192\.168)\./', $ip)) {
								if ($phpNewVersion) {
									if (ip2long($ip) == false) {
										$ip = "";
									}
								} else {
									if (ip2long($ip) <= 0) {
										$ip = "";
									}
								}
							} else {
								if ($key == 'HTTP_X_FORWARDED_FOR' || $key == 'HTTP_X_FORWARDED' || $key == 'HTTP_FORWARDED_FOR' || $key == 'HTTP_FORWARDED') {
									if (count($arrIPLocal) == 0) {
										$arrIPLocal[0] = $ip;
									} else {
										$arrIPLocal[] = $arrIPLocal[0];
										$arrIPLocal[0] = $ip;
									}
								} else if ($key == 'HTTP_X_REAL_IP') {
									$num = count($arrIPLocal);
									if ($num == 0) {
										$arrIPLocal[0] = $ip;
									} else {
										if ($num >= 2) {
											$arrIPLocal[] = $arrIPLocal[1];
										}
										$arrIPLocal[1] = $ip;
									}
								} else {
									$arrIPLocal[] = $ip;
								}
								$ip = "";
							}
							if (!empty($ip)) {
								return $ip;
							}
						} catch ( Exception $e ) {
							Log::info("UtilNetwork::getClientIP exception: [" . $e->getLine() . "] " . $e->getMessage());
						}
					}
				}
			} catch ( Exception $e ) {
				Log::info("UtilNetwork::getClientIP exception: [" . $e->getLine() . "] " . $e->getMessage());
			}
		}
		$num = count($arrIPLocal);
		// if ($debug) Log::info("UtilNetwork::getClientIP num arrIPLocal = " . $num );
		if ($num > 0) {
			$ip = $arrIPLocal[0];
			return $ip;
		} else {
			if (array_key_exists($_SERVER, 'REMOTE_ADDR')) {
				$ip = $_SERVER['REMOTE_ADDR'];
				return $ip;
			}
		}
		return "127.0.0.1";
	}
	
	public static function isDomainValid( $domain ) {
		return preg_match('/^ (?: [a-z0-9] (?:[a-z0-9\-]* [a-z0-9])? \. )* 
   							[a-z0-9] (?:[a-z0-9\-]* [a-z0-9])?            
   							\. [a-z]{2,6} $ 
							/ix', $domain);
	}
	
	public static function isURLValid( $uri ){
		if (preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i', $uri) ) {
			return true;
		} else{
			return false;
		}
	}
	
	public static function isUseProtocolHTTP( $url ) {
		$b = false;
		try {
			if (! empty($url)) {
				if (strlen($url) >= 8) {
					if (strtolower(substr($url, 0, 7)) == 'http://' || strtolower(substr($url, 0, 8)) == 'https://' || strtolower(substr($url, 0, 7)) == 'rtsp://') {
						$b = true;
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("UtilNetwork::isUseProtocolHTTP url=".$url." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
	public static function isFormatIPValid( $ip ) {
		$valid = preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\z/', $ip);
		return $valid;
	}
	
}