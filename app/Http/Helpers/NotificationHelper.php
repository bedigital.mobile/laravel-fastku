<?php

namespace App\Http\Helpers;

class NotificationHelper
{
    const CODE_OK               = 200;
    const CODE_INVALID_DATA     = 400;
    const CODE_INACTIVE         = 401;
    const CODE_NOT_FOUND        = 404;
    const CODE_NOT_ALLOWED      = 405;
    const CODE_SERVER_ERROR     = 500;
    
	var $code;
	var $messages = array();
	var $data;
    
    public function __construct() {
        $this->code = NotificationHelper::CODE_OK;
    }

    public function addMessage($msg, $code=NotificationHelper::CODE_INVALID_DATA) {
        $this->code = $code;
        array_push($this->messages, $msg);
        $this->data = null;
    }
    
    public function setData($data) {
        $this->data = $data;
    }
    
    public function isOK() {
        return $this->code == NotificationHelper::CODE_OK;
    }
    
    public function internalServerError($msg='Internal Server Error') {
        $this->code = NotificationHelper::CODE_SERVER_ERROR;
        $this->messages = [$msg];
    }
	
	public function inactive($msg='Inactive') {
        $this->code = NotificationHelper::CODE_INACTIVE;
        $this->messages = [$msg];
    }
    
	public function notAllowed($msg='Not Allowed') {
        $this->code = NotificationHelper::CODE_NOT_ALLOWED;
        $this->messages = [$msg];
    }
    
	public function notFound($msg='Not Found') {
        $this->code = NotificationHelper::CODE_NOT_FOUND;
        $this->messages = [$msg];
    }
    
    public function resetMessage() {
        $this->code = NotificationHelper::CODE_OK;
        $this->messages = [];
        $this->data = null;
    }
    
	public function build()
	{
		$data = [
            'error' => [
                    'code' => $this->code,
                    'messages' => ($this->code == NotificationHelper::CODE_OK ? ['OK'] : $this->messages)
                ],
            'data' => $this->data
        ];
		
		return $data;
	}
}

?>