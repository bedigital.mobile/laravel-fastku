<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use App\Http\Controllers\BDGBaseController;
use App\Models\PartnerModel;
use App\Models\PartnerTrxReqModel;
use App\Models\PartnerWhiteListIpModel;
use App\Models\BillerAggratorModel;
use App\Models\TrxRequestWorkerModel;
use App\Jobs\TrxWorkerJob;

use App\Http\Helpers\HttpClientHelper;
use App\Http\Helpers\PartnerDepositCheckingHelper;

class TestTrx extends BDGBaseController
{

    function index(Request $request){
        
                $this->notif->addMessage('System busy. Please try again in short moment');
           
        
        return response()->json($this->notif->build());
    }
}