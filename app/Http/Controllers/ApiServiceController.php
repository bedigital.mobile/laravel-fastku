<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use App\Http\Controllers\BDGBaseController;
use App\Models\PartnerModel;
use App\Models\PartnerTrxReqModel;
use App\Models\PartnerWhiteListIpModel;
use App\Models\BillerAggratorModel;
use App\Models\TrxRequestWorkerModel;
use App\Jobs\TrxWorkerJob;

use App\Http\Helpers\HttpClientHelper;
use App\Http\Helpers\PartnerDepositCheckingHelper;

class ApiServiceController extends BDGBaseController
{

    function submitData(Request $request){
        $ip = $request ->ip();
        $apiKey = $request ->input('api_key');
        $type = $request->input('type');
        $product = $request->input('product');
        $billNumber = $request->input('billNumber');
        $callbackUrl = $request->input('callback_url' ,"");
       
        $access = PartnerModel::where('partner_whitelist_ip.ip_address', '=', $ip)
                ->where('partner.api_key', '=', $apiKey)
                ->join('partner_whitelist_ip', 'partner.id', '=', 'partner_whitelist_ip.partner_id')
                ->first();
        
        if (!$access) { 
            $this->notif->notAllowed();
            return response()->json($this->notif->build());
        }
        
        if($access->active == 'Y'){
            $accessKey = md5(time());
            $q = TrxRequestWorkerModel::where('active' , '=' , 'N')
                    ->orderBy('worker_number', 'asc')
                    ->first();
            if($q){
                $q->access_key = $accessKey;
                $q->active = 'Y';
                $q->save();

                $workerURL = secure_url('/h2h/trx/request/'.$q->worker_number.$accessKey);
                if($callbackUrl != ''){
                    $params = [
                        'worker_url' => $workerURL,
                        'api_key' => $apiKey,
                        'type' => $type,
                        'product' => $product,
                        'billNumber' => $billNumber,
                        "callback_url" => $callbackUrl
                    ];
                    TrxWorkerJob::dispatch($params);
                } else {                    
                    $workerParams = 'api_key='.$apiKey.'&type='.$type.'&product='.$product.'&billNumber='.$billNumber;
                    return redirect($workerURL.'?'.$workerParams);
                }
            } else {
                $this->notif->addMessage('System busy. Please try again in short moment');
            }
        } else {
            $this->notif->addMessage('Your account is inactive. Please contact administrator.');
        }
        
        return response()->json($this->notif->build());
    }
    
    function callback(Request $request) {
        $resp = [
            'type' => $request->input('data.type'),
            'service' => $request->input('data.service'),
            'trxId' => $request->input('data.trxId'),
            'resultCode' => $request->input('error.code'),
        ];
        Log::info('callback received: '.json_encode($resp));
    }
}
