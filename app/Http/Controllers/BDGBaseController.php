<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;

use App\Http\Helpers\NotificationHelper;
use App\Http\Helpers\SessionHelper;
use App\Http\Helpers\TableSortHelper;
use App\Http\Helpers\TableSortHelperSimple;

use App\Models\admin\MainAgentModel;
use App\Models\admin\MainAgentProfileModel;

class BDGBaseController extends Controller
{
    protected $session;
    protected $tableSorter;
    protected $viewData;
    var $notif;
    var $request;
    
    protected static $HEADER_API_JSON = array(
        'Content-Type' => 'application/json',
        'Cache-Control' => 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
        'Pragma' => 'no-cache',
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Headers' => 'Origin, Content-Type, Authorization, X-Auth-Token',
        'Strict-Transport-Security' => 'max-age=31536000; includeSubDomains',
        'X-Frame-Options' => 'sameorigin',
        'X-Content-Type-Options' => 'nosniff',
        'X-XSS-Protection' => '1; mode=block'
	);
    
    public function __construct() {
        $this->request['partner'] = Input::get('partner');
        $this->request['biller'] = Input::get('biller');
        $this->request['trx'] =  Input::get('trx');
        $this->request['product'] = Input::get('product');
        
        $this->notif = new NotificationHelper();
        $this->session = new SessionHelper();
        $this->tableSorter = new TableSortHelper();
        $this->tableSortersimple = new TableSortHelperSimple();

        $this->viewData['session'] = $this->session;
        $this->viewData['sorter'] = $this->tableSorter;
        $this->viewData['STATUS_ACTIVE'] = MainAgentModel::STATUS_ACTIVE;
        $this->viewData['STATUS_INACTIVE'] = MainAgentModel::STATUS_INACTIVE;
        $this->viewData['STATUS_REGISTER'] = MainAgentModel::STATUS_REGISTER;
        $this->viewData['STATUS_SUSPENDED'] = MainAgentModel::STATUS_SUSPENDED;
        $this->viewData['STATUS_UNAPPROVED'] = MainAgentModel::STATUS_UNAPPROVED;
        $this->viewData['PAYMODE_POSTPAID'] = MainAgentModel::PAYMODE_POSTPAID;
        $this->viewData['PAYMODE_PREPAID'] = MainAgentModel::PAYMODE_PREPAID;
        $this->viewData['PROFILE_TYPE_CORPORATE'] = MainAgentProfileModel::PROFILE_TYPE_CORPORATE;
        $this->viewData['PROFILE_TYPE_PERSONAL'] = MainAgentProfileModel::PROFILE_TYPE_PERSONAL;
    }
    
    function params() {
        $params = [
            'partner' => $this->request['partner'],
            'biller' => $this->request['biller'],
            'trx' => $this->request['trx'],
            'product' => $this->request['product']
        ];
        
        return $params;
    }
}
