<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Config;

use App\Http\Controllers\BDGBaseController;
use App\Models\PartnerModel;
use App\Models\PartnerDepositModel;
use App\Models\PartnerTrxReqModel;
use App\Models\PartnerWhiteListIpModel;
use App\Models\PartnerDepositAccountModel;
use App\Models\BillerAggratorModel;
use App\Models\WhiteLabelProductModel;
use App\Models\TrxRequestWorkerModel;
use App\Jobs\TrxWorkerJob;

use App\Http\Helpers\HttpClientHelper;
use App\Http\Helpers\PartnerActivityLogHelper;
use App\Http\Helpers\UtilNetwork;
use App\Http\Helpers\PartnerDepositCheckingHelper;
use App\Http\Helpers\TrxRequestHelper;
use App\Jobs\TrxWorkerReportJob;
use App\Jobs\PartnerActivityLogJob;

class PartnerTrx extends BDGBaseController
{

    function ListenerMaster(Request $request){
        $ip = UtilNetwork::getClientIP();//$this->notif->data = $ip;return response()->json($this->notif->build());
        $apiKey = $request->input('api_key', '');
        $token = $request->input('token', '');
        $type = $request->input('type', '');
        $product = $request->input('product', '');
        $billNumber = $request->input('billNumber', '');
        $sign = $request->input('signature', '');
        $callbackUrl = $request->input('callback_url', '');

        $partner = PartnerModel::where('partner.api_key', '=', $apiKey)
                        ->where('partner_whitelist_ip.ip_address', '=', $ip)
                        ->join('partner_whitelist_ip', 'partner.id', '=', 'partner_whitelist_ip.partner_id')
                        ->first();

        if (!$partner) {
            $this->notif->notAllowed();
            $response = $this->notif->build();
            Log::info('['.$ip.' - '.$apiKey.'] /trx/request: '. json_encode($response));
            PartnerActivityLogJob::dispatch($ip, $apiKey, '/trx/request', json_encode($request->input()), json_encode($response), $this->notif->code, json_encode($this->notif->messages));

            return response()->json($response);
        }

        if($partner->active == 'Y'){
            if($callbackUrl != ''){
                $params = [
                    'ip' => $ip,
                    'api_key' => $apiKey,
                    'token' => $token,
                    'type' => $type,
                    'product' => $product,
                    'billNumber' => $billNumber,
                    'sign' => $sign,
                    "callback_url" => $callbackUrl
                ];
                TrxWorkerJob::dispatch($params)->onQueue(TrxRequestHelper::queueName());
            } else {
                $request->merge(['ip' => $ip]);
                $response = (new TrxRequestHelper($request))->run();

                return $response;
            }
        } else {
            $this->notif->addMessage('Your account is inactive. Please contact administrator.');
        }

        $response = $this->notif->build();
        Log::info('['.$ip.' - '.$apiKey.'] /trx/request: '.json_encode($request->input()).' - response: '. json_encode($response));
        PartnerActivityLogJob::dispatch($ip, $apiKey , '/trx/request', json_encode($request->input()), json_encode($response), $this->notif->code, json_encode($this->notif->messages));

        return response()->json($response)->withHeaders(self::$HEADER_API_JSON);
    }

    function partnerPaid(Request $request) {
        $ip = UtilNetwork::getClientIP();
        $va = $request->input('data_callback.account_number', '');
        $topUp = $request->input('data_callback.payment_amount', 0);
        $feeInternal = $request->input('data_callback.fee_internal', 0);
        $feeProvider = $request->input('data_callback.fee_provider', 0);

        try {
            if ($va != '') {
                $partner = PartnerModel::where('bank_va', '=', $va)
                        ->first();
                if ($partner) {
                    $amount = $topUp - (((int) $feeInternal) + ((int) $feeProvider));
                    
                    $deposit = PartnerDepositModel::where('partner_id', '=', $partner->id)
                                    ->first();
                    
                    $beforeDeposit = $deposit->current_deposit;

                    if ($deposit->paymode == PartnerModel::PARTNER_PAYMODE_PREPAID) {
                        $deposit->current_deposit = $deposit->current_deposit + ((int) $amount);
                    } else {
                        if ($deposit->current_deposit >= $amount) {
                            $deposit->current_deposit = 0;
                        } else {
                            $deposit->current_deposit = $deposit->current_deposit - $amount;
                        }
                    }
                    $deposit->save();

                    $q = new PartnerDepositAccountModel();
                    $q->partner_id = $partner->id;
                    $q->bank_id = 0;
                    $q->account_number = '';
                    $q->bank_receiver_id = $partner->bank_id;
                    $q->account_receiver_number = $partner->bank_va;
                    $q->method = PartnerDepositAccountModel::PAYMENT_METHOD_VA;
                    $q->deposit_before = $beforeDeposit;
                    $q->amount = (int) $amount;
                    $q->topup_amount = (int) $topUp;
                    $q->fee_internal = (int) $feeInternal;
                    $q->fee_provider = (int) $feeProvider;
                    $q->deposit_after = $deposit->current_deposit;
                    $q->save();
                } else {
                    $this->notif->addMessage('Invalid VA number');
                }
            } else {
                $this->notif->addMessage('VA number is required');
            }
        
            Log::info('['.$ip.'] /payment-gateway/callback: '.json_encode($request->input()).' - response: '.json_encode($this->notif->build()));
            PartnerActivityLogJob::dispatch($ip, '', '/payment-gateway/callback', json_encode($request->input()), json_encode($this->notif->build()), $this->notif->code, json_encode($this->notif->messages));
            
        } catch (\Exception $ex) {
            Log::error('['.$this->ip.'] /payment-gateway/callback - request: '.json_encode($this->request->input()).' - error: '.$ex->getMessage());
            PartnerActivityLogJob::dispatch($ip, '', '/payment-gateway/callback', json_encode($request->input()), $ex->getMessage(), $this->notif->code, json_encode($this->notif->messages));
            $this->notif->internalServerError();
        }
        
        return response()->json($this->notif->build());
    }
    
    function partnerDeposit(Request $request) {
        $ip = UtilNetwork::getClientIP();
        $apiKey = $request->input('api_key');
        $token = $request->input('token');
        $user = $request->input('user');
        $description = $request->input('description');
        $amount = $request->input('amount', 0);
        $feeInternal = $request->input('fee_internal', 0);
        $feeProvider = $request->input('fee_provider', 0);
        $bankReceiver = $request->input('bank_receiver_id', 0);
        $accountReceiver = $request->input('account_receiver_number');
        $method = $request->input('method', PartnerDepositAccountModel::PAYMENT_METHOD_TRANSFER_BANK);
        
        if ($apiKey == '') { $this->notif->addMessage('API Key is required'); }
        if ($user == '') { $this->notif->addMessage('User Name is required'); }
        if ($description == '') { $this->notif->addMessage('Description is required'); }
        if ($amount == '') { $this->notif->addMessage('Topup value is required'); }
        
        $exception = '';
        
        if ($this->notif->isOK() == true) {
            $partner = PartnerModel::where('api_key', '=', $apiKey)
                    ->first();

            if ($partner) {

                try {
                    if ($this->checkToken($apiKey, $token) == false) {
                        $this->notif->addMessage('Invalid Token');
                    } else {
                        $totalAmount = abs($amount) - (((int) $feeInternal) + ((int) $feeProvider));

                        $deposit = PartnerDepositModel::where('partner_id', '=', $partner->id)
                                        ->first();

                        if ($amount < 0 && abs($totalAmount) > $deposit->current_deposit) {
                            $this->notif->addMessage('Amount must be less or equal of current deposit');
                        }

                        if ($this->notif->isOK() == true) {
                            $beforeDeposit = $deposit->current_deposit;

                            $deposit->current_deposit = $deposit->current_deposit + ((int) $totalAmount);
                            $deposit->save();

                            $q = new PartnerDepositAccountModel();
                            $q->partner_id = $partner->id;
                            $q->bank_id = 0;
                            $q->account_number = '';
                            $q->bank_receiver_id = $bankReceiver;
                            $q->account_receiver_number = $accountReceiver;
                            $q->method = PartnerDepositAccountModel::PAYMENT_METHOD_VA;
                            $q->deposit_before = $beforeDeposit;
                            $q->amount = (int) $totalAmount;
                            $q->topup_amount = (int) $amount;
                            $q->fee_internal = (int) $feeInternal;
                            $q->fee_provider = (int) $feeProvider;
                            $q->deposit_after = $deposit->current_deposit;
                            $q->save();
                        }
                    }
                    
                } catch (\Exception $ex) {
                    $exception = $ex->getMessage();
                    $this->notif->internalServerError();
                }
                
            } else {
                $this->notif->addMessage('Not Valid API Key');
            }
        }
        
        $response = $this->notif->build();
        
        if ($exception != '') {
            Log::error('['.$ip.'] /updatedeposit: '.json_encode($request->input()).' - error: '.json_encode($exception));
            PartnerActivityLogJob::dispatch($ip, '', '/updatedeposit', json_encode($request->input()), $exception, $this->notif->code, json_encode($this->notif->messages));
        } else {
            Log::info('['.$ip.'] /updatedeposit: '.json_encode($request->input()).' - response: '.json_encode($response));
            PartnerActivityLogJob::dispatch($ip, '', '/updatedeposit', json_encode($request->input()), json_encode($this->notif->build()), $this->notif->code, json_encode($this->notif->messages));
        }
        
        return response()->json($response);
    }

    function callback(Request $request) {
        Log::info('callback received: '.json_encode($request->input()));

        return response()->json($this->notif->build());
    }

    function token(Request $request, $apiKey) {
        if (boolval(config('app.debug')) == false) {
            echo 'Unauthorized';
            return;
        }
        
        $secretParam = $request->input('secret');
        if ($secretParam != '') {
            echo 'secret = '.$secretParam.'<br/>';
        }
        
        $q = PartnerModel::where('partner.api_key', '=', $apiKey)
                ->first();

        $secretKey = $secretParam != '' ? $secretParam : ($q ? $q->secret_key : '');
        $time = gmdate('U');
        $token = md5($secretKey.$apiKey.$time);

        echo 'token: '.$token.'<br/><br/>';

//        sleep(29);
//        $this->checkToken($apiKey, $token);
    }

    function checkToken($apiKey, $token) {
        $q = PartnerModel::where('partner.api_key', '=', $apiKey)
                ->first();

        $secretKey = $q->secret_key;
        $seconValid = 30;
        $timeNow = gmdate('U') - $seconValid;
        $valid = false;

        for ($i=0; $i<($seconValid * 2); $i++) {
            $check = md5($secretKey.$apiKey.($timeNow+$i));
//            echo $token.' - '.$check;
            if (strcmp($token, $check) == 0) {
//                echo ' - match';
                $valid = true;
                break;
            }
//            echo '<br/>';
        }
        
        return $valid;
    }

}
