<?php
namespace App\Http\Controllers;

use app\Providers\ErrorServiceProvider;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

use App\Http\Helpers\HttpClientHelper;
use App\Http\Helpers\TrxRequestHelper;
use App\Models\PartnerModel;
use App\Models\TrxRequestWorkerModel;
use App\Http\Helpers\UtilNetwork;
use App\Models\ProductModel;

class PagesController extends Controller
{

    public function apiService(Request $request)
    {
    	$q = ProductModel::selectRaw('concat(id , "") as prodid, product_name')
                ->orderBy('id')
                ->get();
    	$response = '';
        
        $apiKey = $request->input('api_key', '');
        $token = $request->input('token', '');
        $type = $request->input('type', '');
        $product = $request->input('product', '');
        $billNumber = $request->input('billNumber', '');
        $sign = $request->input('sign', '');
        $callbackUrl = $request->input('callback_url', '');


        if ($apiKey != '') {
            $params = [
                'api_key' => $apiKey,
                'token' => $token,
                'type' => $type,
                'product' => $product,
                'billNumber' => $billNumber,
                'sign' => $sign,
                'callback_url' => $callbackUrl
            ];            
            $resp = HttpClientHelper::invoke(secure_url('/h2h/trx/request'), 'POST', $params);
            Log::info('response api service:'.json_encode($resp));
            $response = $resp['body'];
        }

        $viewData = ['response' => '' , 'products' => $q , 'response' => $response];
        
        return view('apiService', $viewData);
    }
    
    // function apiProcess(Request $request) {
    // 	$response = '';
        
    //     $apiKey = $request->input('api_key', '');
    //     $token = $request->input('token', '');
    //     $type = $request->input('type', '');
    //     $product = $request->input('product', '');
    //     $billNumber = $request->input('billNumber', '');
    //     $sign = $request->input('sign', '');
    //     $callbackUrl = $request->input('callback_url', '');


    //     if ($apiKey != '') {
    //         $params = [
    //             'api_key' => $apiKey,
    //             'token' => $token,
    //             'type' => $type,
    //             'product' => $product,
    //             'billNumber' => $billNumber,
    //             'sign' => $sign,
    //             'callback_url' => $callbackUrl
    //         ];            
    //         $resp = HttpClientHelper::invoke(secure_url('/trx/request'), 'POST', $params);            
    //         $response = $resp['body'];
    //     }
        
    //     $viewData = ['response' => $response];

    //     return view('apiService', $viewData);
    // }
}
