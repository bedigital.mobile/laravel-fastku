<?php

namespace App\Http\Controllers\admin\cms;

use Log;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Controllers\BDGBaseController;
use App\Models\admin\AgentModel;

class AgentIdController extends BDGBaseController {
   function search_agentId(Request $request){
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

    $min =Input::get('min'); //$request->get('min').' 00:00:00';
    $max = Input::get('max');//$request->get('max').' 23:59:59'; //$request->get('max');
    $tableselect = $request->tableselect;
    $search =$request->input('search');
    $timeMin = '00:00:00';
    $timeMax = '23:59:59';

    if($min != '')
    {
        $result1 = $min. ' ' . $timeMin; 
    } 
    else
    {
      $result1 = '';  
    }

    if($max != '')
    {
        $result2 = $max . ' ' . $timeMax;
    } 
    else
    {
      $result2 = '';  
    }

    $fields = [
            ['id', 'Id'],
            ['master_agent_id', 'Master Agent Id'],
            ['username', 'Username'],
            ['status', 'Status'],
            ['created_at','Created At']
            
        ]; 
        //,$min,$max,$tableselect,$search)
        $this->tableSorter->setupSorter(url('/agency/agent_id.html'), $fields, $searchFor, $sf, $sm, $min, $max, $tableselect, $search);
        if(($search != '') && ($min != '' && $max != '')) {         
            $q = AgentModel::whereBetween(
                'created_at', 
                [
                    $result1,
                    $result2
                ]
                )
                ->where($tableselect , $search)
                ->orderBy('created_at' , 'desc');

        }else if($min != '' && $max != ''){
            $q = AgentModel::whereBetween(
                'created_at', 
                [
                    $result1,
                    $result2
                ]
                )
                ->orderBy('created_at' , 'desc');
        }else if(($search != '' && $tableselect != '') && ($min == '' && $max == '')){
            $q = AgentModel::where($tableselect , $search)
                ->orderBy('created_at' , 'desc');      
        }else {
            $q = AgentModel::orderBy('created_at' , 'desc')
            ->selectRaw('id ,master_agent_id, username , status , created_at');
        }
  
        $this->tableSorter->setupPaging($q, $pg);

        $this->viewData['sorter'] = $this->tableSorter;
        $this->viewData['pg'] = $pg;
        $this->viewData['searchFor'] = $searchFor;
        $this->viewData['min'] = $result1;
        $this->viewData['max'] = $result2;
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
        
        return view('admin.report.agentId', $this->viewData );
 }
    
    function exportAgentIdToCSV(Request $request){
            $min =Input::get('min' ); //$request->get('min').' 00:00:00';
            $max = Input::get('max' );//$request->get('max').' 23:59:59'; //$request->get('max');
            $tableselect = $request->tableselect;
            $search =$request->input('search');

            $timeMin = '00:00:00';
            $timeMax = '23:59:59';

            $result1 = $min . ' ' . $timeMin;
            $result2 = $max . ' ' . $timeMax;

            if(($search != '') && ($min != '' && $max != '')) {         
            $table = AgentModel::whereBetween(
                'created_at', 
                [
                    $result1,
                    $result2
                ]
                )
                ->where($tableselect , $search)
                ->orderBy('created_at' , 'desc')
                ->get();

        }else if($min != '' && $max != ''){
            $table = AgentModel::whereBetween(
                'created_at', 
                [
                    $result1,
                    $result2
                ]
                )
                ->orderBy('created_at' , 'desc')
                ->get();
        }else if(($search != '') && ($min == '' && $max == '')){
            $table = AgentModel::where($tableselect , $search)
                ->orderBy('created_at' , 'desc')
                ->get();      
        }else {
            $table = AgentModel::orderBy('created_at' , 'desc')
            ->get();
        }
                // $table =PartnerTrxReqModel::whereBetween(
                // 'created_at', 
                // [
                //     $result1,
                //     $result2
                // ]   
                // )
                //  ->where('partner_id' , '=' , $this->session->get('partner_id'))
                //  ->where($tableselect , $search)
                //  ->orderBy('created_at' , 'desc')
                //  ->get();
            // }
            
            $filename = "report.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('id' ,'master_agent_id', 'username' , 'status' , 'created_at'));

            foreach($table as $row) {
                fputcsv($handle, array($row['id'], $row['master_agent_id'], $row['username'] , $row['status'], $row['created_at']));
            }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            return response()->download($filename, 'report.csv', $headers);
    }
}