<?php

namespace App\Http\Controllers\admin\cms;



use Log;
use Config;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\BusinessLogic\HelpdeskBL;
use App\Core\Util\UtilNetwork;
use App\Core\Util\UtilPhone;
use App\Definition;
use App\BusinessLogic\Log\LogUserLoginBL;
use App\Jobs\InsertLogUserLoginJob;
use App\Http\Controllers\BDGBaseController;
use App\Models\admin\WhiteLabelAdminModel;
use App\Models\RunningText;
use App\BusinessLogic\Log\LogUserActivityBL;
use App\Jobs\InsertLogUserActivityJob; 

// use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\RequestOptions;

use GraphQL\Client;
use GraphQL\Exception\QueryError;
use GraphQL\Query;
use GraphQL\Mutation;

use App\Http\Helpers\SessionHelper;


 class LoginController extends BDGBaseController
{
     protected $ipClient;
     public function __construct() {
        parent::__construct();    
        // $this->conn = Config::get ( 'mysql_stg' );
        $this->conn = Config::get ( 'webconf.cms.connectiondata_prod' );
        $this->conn111 = Config::get ( 'webconf.cms.connection111' );
        $this->whitelabel = Config::get ( 'webconf.cms.whitelabel' );
        $this->whitelabelID = Config::get ( 'webconf.cms.whitelabelID' );
        $this->platform = Config::get ( 'webconf.cms.platform' );
        $this->AppID = Config::get ( 'webconf.cms.appid' );
        $this->hostcms = Config::get ( 'webconf.cms.hostcms' );
        $this->urlsite = Config::get ( 'webconf.cms.urlsite' );
        $this->helpdeskBL = new HelpdeskBL; 
        $this->apps = "DASHBOARD-FASTKU"; 
        $this->ipClient = UtilNetwork::getClientIP();
        $this->arraytokenuid=[];
        
    }
 

    function getlogin(Request $request) {
      $host = $request->getHttpHost();
      $this->helpdeskBL->verifiedHost($this->hostcms, $host);
       return view('admin.login.index');

    }

    function getcaptch(Request $request) {
      $ipku = $this->ipClient;
      $usr = $request->header('User-Agent').';ip='.$ipku;
      log::info("UserController::getcaptch STARTS , ip=".$usr); 
      $cap = $this->helpdeskBL->getRequestcapthca($usr);
      echo $cap;

    }
    function refreshcaptch(Request $request) {
          $ipku = $this->ipClient;        
          $usr = $request->header('User-Agent').';ip='.$ipku;
          log::info("UserController::refreshcaptch STARTS , usr=".$usr); 
          $cap = $this->helpdeskBL->refreshcapthca($usr);           
          echo $cap;

        }
 
 function login(Request $request) {


log::info("LoginController::login start =========================================================");
 // $request->get("usernameOrEmail");
        $userName2 = $request->post('username');
        $userName = $request->input('username');
        $plainpassword = $request->input('password');
        $encpass = md5($plainpassword);
        $recaptcha = $request->input('textcapthca');
        $ipku = $this->ipClient;        
        $usr = $request->header('User-Agent').';ip='.$ipku;
        log::info("UserController::testlogin STARTS , usr=".$usr); 
        log::info("UserController::testlogin data , userName = ".$userName2); 
        log::info("UserController::testlogin data , userName = ".$userName." , plainpassword=".$plainpassword." , recaptcha=".$recaptcha); 
 
        $usernamecs1 =strtoupper(substr($userName,-3));
        $whitelabel = strtoupper(substr(base_convert($this->whitelabelID, 10, 36),-3));
        $whitelabelID = strtoupper(base_convert($this->whitelabelID, 10, 36));

        $encpassword = md5($whitelabel.$plainpassword.$usernamecs1);
        log::info("encrypt =".$encpassword);
         $query = "mutation { 
          login ( 
              whitelabel_id: \"$whitelabelID\",
              captcha: \"$recaptcha\",
              platform: \"$this->platform\",
              app_build_version: \"$this->AppID\"
          ) { session_id, user_id, authorization,username,nickname } 
      }"; 

    $url = $this->urlsite.'elog/graph/v1/member';
    $request = ['query' => $query];  
    $req = json_encode($request);
    $parameters['body'] = $req;
    $stack = HandlerStack::create();
    $client = new \GuzzleHttp\Client([
        'handler'  => $stack,
         [
     	 'auth' => [$userName, $encpassword, 'digest'],
    	],
        'headers'  => [
            'Accept'       => 'application/json', 
            'Content-Type' => 'application/json',
             'WWW-Authenticate' => 'Digest',
            'User-Agent' => $usr,
           'realm' => 'BEDIGITAL',
        ],
       
    ]); 

    try {

    $response = $client->request('post',$url,$parameters);
    $body =  $response->getBody();
    log::info("oke bosy=".$response->getBody());
    } catch (\Exception $e) {
    $response = $e->getResponse();
    log::info($response->getStatusCode());
    if($response->getStatusCode() == '401')
    {
        $responseBodyAsString = $response->getHeaders();//->getContents();
        log::info($responseBodyAsString);
        // log::info($responseBodyAsString['www-authenticate'][0]);
        
        if(!empty($responseBodyAsString['Www-Authenticate'][0]) || !is_null($responseBodyAsString['Www-Authenticate'][0]))
        {        	
        	$char = explode('"', $responseBodyAsString['Www-Authenticate'][0]);
        }else if(!empty($responseBodyAsString['Www-Authenticate'][0]) || !is_null($responseBodyAsString['Www-Authenticate'][0]))
        {
           	$char = explode('"', $responseBodyAsString['www-authenticate'][0]);
        }

        log::info('realm='.$char[1]);
        log::info('nonce='.$char[3]);
        log::info('opaque='.$char[5]);

        $method = 'POST';
        $uri = '/elog/graph/v1/member';
        $A1 = md5( $userName . ":" .$char[1]. ":" . $encpassword );
        $A2 = md5( $method . ":" . $uri );
        $response = md5( $A1 . ":" . $char[3] . ":" . $A2 );

        //second attempt
        $authorized = 'Digest username="'.$userName.'",realm="'.$char[1].'",nonce="'.$char[3].'",uri="'.$uri.'",response="'.$response.'",opaque="'.$char[5].'"';
        log::info("second--attempt post ========================".$query);
        log::info("second--attempt a1".$A1); 
        log::info("second--attempt a2".$A2);
        log::info("second--attempt useragent".$usr);
        log::info("second--attempt auth".$authorized);
               
              $request2 = ['query' => $query];  
              $req2 = json_encode($request2);
              $parameters2['body'] = $req2;
              $stack2 = HandlerStack::create();
              $client2 = new \GuzzleHttp\Client([
              'handler'  => $stack2,
              'headers'  => [
              'Content-Type' => 'application/json',
			  'User-Agent' => $usr,
  			  'Authorization'=> $authorized
                ],
              ]); 
              log::info("second--attempt guzzling");
/*Digest username="6281380298484", realm="BEDIGITAL", nonce="0bd4e86a8db759db65666d992dae24b9", uri="/graph/v1/member", response="6ba91ead14bf28735e5a8b6f51a7de4e", opaque="a37f9a0aa83b5a0767bf63eba781e2e5"*/
              try {
             log::info("second--attempt try one");
             $response2 = $client2->request('post',$url,$parameters2);
             $body =  $response2->getBody();
			  log::info('hasilnnnn '.$body); 
			  $data = array();
			  $datasession = array();
			  $json = json_decode($body);
              $json2     = get_object_vars($json);
              log::info("hasil2 =".json_encode($body));
              foreach($json2['data'] as &$blog) {
              $blog     = get_object_vars($blog);
              log::info("after - session=".$blog['session_id']); 
              log::info("after - user=".$blog['user_id']); 
              log::info("after - user=".$blog['username']); 
              log::info("after - user=".$blog['nickname']); 
              log::info("after - after json=".json_encode($blog)); 
              foreach($blog['authorization'] as &$log2) {
                  // $log2     = get_object_vars($log2);
                  $data[] = $log2;
                  log::info("after - data log=".json_encode($log2));
                  }         
              }
              $dataprofile = $this->helpdeskBL->getprofilejwt($blog['session_id'],$usr);
                $datasession = [
                              'session_id' => $blog['session_id'],
                              'user_id' => $blog['user_id'],
                              'username' => $blog['username'],
                              'nickname' => $blog['nickname'],
                              'token' => $data[0], 
                              'type' => $data[1], 
                              'duration' => $data[2],
                              'email' => $dataprofile['email'],
                              'fullname' => $dataprofile['fullname'],

                          ];   
                $this->session->save($datasession);  
                log::info("after - data session=".json_encode($datasession)); 
              } 
              catch (\Exception $f) {
                log::info("second--attempt try exc".json_encode($body));
                  
             }
 
    } 
    else
    {
      $body = "Unknown Error";
    }
} 

return $body;
// return response()->json($body);



}
 
 function register(Request $request) {

log::info("==========================================================================");
log::info("LoginController::register start ");
        $namaawal = $request->post('namaawal');
        $namaakhir = $request->input('namaakhir');
        $fullname = $namaawal.' '.$namaakhir;
        $nohp = $request->input('nohp');
        $email = $request->input('email');
        $pin = $request->input('pin');
        $pin2 = $request->input('pin2');
        $whitelabelID = strtoupper(base_convert($this->whitelabelID, 10, 36));
        if(empty($fullname) || empty($nohp) || empty($email))
        {
            $this->notif->addMessage('Lengkapi Data Anda'); 
            return response()->json($this->notif->build());

        }

        $verifytype = "SMS";
        $textcapthcas = $request->input('textcapthcas');
        $ipku = $this->ipClient;        
        $usr = $request->header('User-Agent').';ip='.$ipku;

        log::info("LoginController::testlogin STARTS , usr=".$usr);  
        log::info("LoginController::testlogin data , nama = ".$fullname." , recaptcha=".$textcapthcas); 
  

    $query = "mutation { 
          signup ( 
              whitelabel_id: \"$whitelabelID\",
              captcha: \"$textcapthcas\",
              fullname: \"$fullname\",
              email: \"$email\",
              phone_number: \"$nohp\",
              is_email_tobe_username: false,
              is_phone_number_tobe_username: true,
              phone_number_verify_type: \"$verifytype\",
              password_login: \"$pin\",
              password_confirm: \"$pin2\",
              platform: \"$this->platform\",
              app_build_version: \"$this->AppID\"
          ) {username, identity_type, user_id, message } 
      }"; 

    $url = $this->urlsite.'elog/graph/v1/member';
    $request = ['query' => $query];  
    $req = json_encode($request);
    $parameters['body'] = $req;
    $stack = HandlerStack::create();
    $client = new \GuzzleHttp\Client([
        'handler'  => $stack,
         [
       'auth' => ['','', ''],
      ],
        'headers'  => [
            'Accept'       => 'application/json', 
            'Content-Type' => 'application/json',
             'User-Agent' => $usr,
         ],
       
    ]); 

    try {

    $response = $client->request('post',$url,$parameters);
    $body =  $response->getBody();
    log::info("oke bosy=".$response->getBody());
    } catch (\Exception $e) {
    $response = $e->getResponse();
    log::info($response->getStatusCode());
    // if($response->getStatusCode() == '401')
    // {

    // }
  }
        return $body;
     
 
 // $mutationsignup = (new Mutation('signup')) 
 //     ->setArguments(['whitelabel_id' => $whitelabelID, 'phone_number' => $nohp , 'captcha' => $textcapthcas,'email' => $email,'fullname' => $fullname,'platform' => $this->platform,'app_build_version' => $this->AppID , 'is_email_tobe_username' => false, 'is_phone_number_tobe_username' => true , 'phone_number_verify_type' => $verifytype, 'password_login' => $pin , 'password_confirm' => $pin2])
 //    ->setSelectionSet(
 //        [
 //            'username',
 //            'identity_type',
 //            'user_id',
 //            'message',
 //        ]
 //    );

//  log::info("mutation =".$mutationsignup);
//  try {
//   log::info("step 1  =");
//       $results = $clients->runQuery($mutationsignup,true);
// log::info(" hasil registrasi =".json_encode($results));

// }
// catch (QueryError $exception) {
//    log::info("Exception");
//      log::info($exception->getErrorDetails());
//      log::info($exception->getErrorDetails());

//     // $response = $exception->getResponse();
//     // log::info($response);
//     exit;
// }
 
}

function verifyotp(Request $request) {

log::info("==========================================================================");
log::info("LoginController::verifyotp start ");
log::info("==========================================================================");

    $userid = $request->post('useridotp');
    $otp = $request->input('isiotp');
     
    $whitelabelID = strtoupper(base_convert($this->whitelabelID, 10, 36));
    if(empty($userid) || empty($otp))
    {
        $this->notif->addMessage('OTP dan UserID tidak dikenal'); 
        return response()->json($this->notif->build());

    }

    $verifytype = "SMS";
    $ipku = $this->ipClient;        
    $usr = $request->header('User-Agent').';ip='.$ipku;

     log::info("LoginController::verifyotp data , userid = ".$userid." , otp=".$otp); 
 
    $query = "mutation { 
          verifyPIN ( 
              whitelabel_id: \"$whitelabelID\",
              user_id: \"$userid\",
              verification_type: \"$verifytype\",
              verification_code: \"$otp\"
          ) {username, user_status } 
      }"; 

    $url = $this->urlsite.'elog/graph/v1/member';
    $request = ['query' => $query];  
    $req = json_encode($request);
    $parameters['body'] = $req;
    $stack = HandlerStack::create();
    $client = new \GuzzleHttp\Client([
        'handler'  => $stack,
         [
       'auth' => ['','', ''],
      ],
        'headers'  => [
            'Accept'       => 'application/json', 
            'Content-Type' => 'application/json',
             'User-Agent' => $usr,
         ],
       
    ]); 

    try {

    $response = $client->request('post',$url,$parameters);
    $body =  $response->getBody();
    log::info("oke bosy=".$response->getBody());
    } catch (\Exception $e) {
    $response = $e->getResponse();
    log::info($response->getStatusCode());
    // if($response->getStatusCode() == '401')
    // {

    // }
  }
        return $body; 
 
}
function resendotp(Request $request) {

log::info("==========================================================================");
log::info("LoginController::resendotp start ");
log::info("==========================================================================");

    $userid = $request->post('useridotp');
    $verifytype = "SMS";
    $whitelabelID = strtoupper(base_convert($this->whitelabelID, 10, 36));
    if(empty($userid))
    {
        $this->notif->addMessage('UserID tidak dikenal'); 
        return response()->json($this->notif->build());

    }
    $ipku = $this->ipClient;        
    $usr = $request->header('User-Agent').';ip='.$ipku;

     log::info("LoginController::resendotp data , userid = ".$userid); 
 
    $query = "mutation { 
          resendPIN ( 
              whitelabel_id: \"$whitelabelID\",
              user_id: \"$userid\",
              verification_type: \"$verifytype\",
           ) {username, user_status,message } 
      }"; 

    $url = $this->urlsite.'elog/graph/v1/member';
    $request = ['query' => $query];  
    $req = json_encode($request);
    $parameters['body'] = $req;
    $stack = HandlerStack::create();
    $client = new \GuzzleHttp\Client([
        'handler'  => $stack,
         [
       'auth' => ['','', ''],
      ],
        'headers'  => [
            'Accept'       => 'application/json', 
            'Content-Type' => 'application/json',
             'User-Agent' => $usr,
         ],
       
    ]); 

    try {

    $response = $client->request('post',$url,$parameters);
    $body =  $response->getBody();
    log::info("oke bosy=".$response->getBody());
    } catch (\Exception $e) {
    $response = $e->getResponse();
    log::info($response->getStatusCode());
   
  }
        return $body; 
 
}

 



function forgotpass(Request $request) {

log::info("==========================================================================");
log::info("LoginController::forgotpass start ");
log::info("==========================================================================");

    $userid = $request->post('usernameforgot');
         
    $whitelabelID = strtoupper(base_convert($this->whitelabelID, 10, 36));
    
    // $verifytype = "SMS";
    $ipku = $this->ipClient;        
    $usr = $request->header('User-Agent').';ip='.$ipku;

     log::info("LoginController::forgotpass data , userid = ".$userid); 

     $query = "mutation { 
          forgotPassword ( 
              whitelabel_id: \"$whitelabelID\",
              username: \"$userid\"
          ) {code, message } 
      }"; 

    $url = $this->urlsite.'elog/graph/v1/member';
    $request = ['query' => $query];  
    $req = json_encode($request);
    $parameters['body'] = $req;
    $stack = HandlerStack::create();
    $client = new \GuzzleHttp\Client([
        'handler'  => $stack,
         [
       'auth' => ['','', ''],
      ],
        'headers'  => [
            'Accept'       => 'application/json', 
            'Content-Type' => 'application/json',
             'User-Agent' => $usr,
         ],
       
    ]); 

    try {

    $response = $client->request('post',$url,$parameters);
    $body =  $response->getBody();
    log::info("oke bosy=".$response->getBody());
    } catch (\Exception $e) {
    $response = $e->getResponse();
    log::info($response->getStatusCode());
    // if($response->getStatusCode() == '401')
    // {

    // }
  }
        return $body; 
 
}


function logoutuser(Request $request) {

log::info("==========================================================================");
log::info("LoginController::logoutuser start ");
log::info("==========================================================================");

    // $userid = $request->post('usernameforgot');
         
    $whitelabelID = strtoupper(base_convert($this->whitelabelID, 10, 36));
    
    // $verifytype = "SMS";
    $ipku = $this->ipClient;        
    $usr = $request->header('User-Agent').';ip='.$ipku;

// $this->helpdeskBL->logoutjwt()
     log::info("LoginController::forgotpass data , session = ".$this->session->get('session_id')); 
 $body="";


	    // Route::get('/logout', function() {
	        $session = new App\Http\Helpers\SessionHelper();
	        $session->remove();
	 
	        return redirect('/fastku/login');  
	    // });



  //   try {

   
  //   } catch (\Exception $e) {
  //   $response = $e->getResponse();
  //   log::info($response->getStatusCode());
    
  // }
  //       return $body; 
 
}


 function res(Request $request) {

log::info("==========================================================================");
log::info("LoginController::resend start ");
log::info("==========================================================================");

        $userid = $request->post('userid');
        $ipku = $this->ipClient;        
        $usr = $request->header('User-Agent').';ip='.$ipku;

        log::info("UserController::testlogin STARTS , usr=".$usr);  
        log::info("UserController::testlogin data , namaawal = ".$namaawal." , namaakhir=".$namaakhir." , recaptcha=".$recaptcha); 
 
        
 $mutationresendotp = (new Mutation('resendPIN'))
    ->setArguments(['whitelabel_id' => '$this->whitelabelID'])
    ->setArguments(['user_id' => '$textcapthcas'])
    ->setArguments(['verification_type' => '$verifytype'])
    ->setSelectionSet(
        [
            'username',
            'user_status',
            'message',
        ]
    ); 


$results = $client->runQuery($mutationresendotp);
log::info(" hasil registrasi =".json_encode($results));

}


public static function parse($query)
        {
          log::info("parsing 11");
                $query = str_replace("{", "{\n", $query);
                $query = str_replace("}", "\n}\n", $query);
                $query = array_map("trim", explode("\n", $query));
                foreach ($query as $k => $line) {
                        // strip comments
                        $line = explode("#", $line);
                        $line = $line[0];
                        // skip opening or closing tags
                        if ($line === "{" || $line === "") {
                                continue;
                        }
                        // declare as object value
                        if (strpos($line, "{") !== false) {
                                $name = trim(str_replace("{", "", $line));
                                $query[$k] = '"' . $name . '": {';
                                continue;
                        }
                        if (strpos($line, "}") !== false) {
                                $query[$k] .= ',';
                                continue;
                        }
                        $query[$k] = '"' . $line . '": true,';
                }
                $query = implode("", $query);
                // cut last comma
                $query = substr($query, 0, -1);
                // cut trailing commas
                $query = str_replace(",}", "}", $query);
                // produce php array
                $retval = json_decode($query, true);
                if (is_null($retval)) {
                        throw new \Exception(sprintf("Error when parsing GraphQL fields: '%s'", $query));
                }
                log::info("parsing 11 hasil =".$retval);
                return $retval;
        }

        /** Filter a PHP array with the GraphQL fields provided by parse() */
public static function executegraph($query, $data)
{
  log::info("parsing");
        $filter = self::parse($query);
        $result = $this->array_intersect_key_recursive($data, $filter);
        log::info("parsing hasil =".$result);
        return $result;
}

function array_intersect_key_recursive($arr, $filter)
{
  log::info("parsing array");
        $is_int = true;
        foreach ($arr as $k => $v) {
                if (!is_int($k) || !is_array($v)) {
                        $is_int = false;
                        break;
                }
        }

        if ($is_int) {
                foreach ($arr as $k => $v) {
                        $arr[$k] = array_intersect_key_recursive($v, $filter);
                }
                return $arr;
        }

        $retval = array();
        foreach ($filter as $key => $value) {
                if (!isset($arr[$key])) {
                        continue;
                }
                if (is_array($value)) {
                        $retval[$key] = array_intersect_key_recursive($arr[$key], $value);
                        continue;
                }
                $retval[$key] = $arr[$key];
        }
        log::info("parsing array hasil =".$retval);
        return $retval;
}


    function testguzzle(Request $request) {

  log::info("UserController::testguzzle"); 
  $user='085311223131';
  $password='test';
     $ipku = $this->ipClient;        
        $usr = $request->header('User-Agent').';ip='.$ipku;
        log::info("UserController::testlogin STARTS , usr=".$usr); 
          $cap = $this->helpdeskBL->loggraph($usr);           
          echo $cap;



    }
    function testingguzzle(Request $request) {
           // echo "test";
      
log::info("=================================================================");
log::info("testing guzzle start");
$realm = "realm";
$nonce = "nonce"; 
$opaque = "opaque";
// if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
//     header('HTTP/1.1 401 Unauthorized');
//     header(sprintf('WWW-Authenticate: Digest realm="%s", nonce="%s", opaque="%s"', $realm, $nonce, $opaque));
//     header('Content-Type: text/html');
//     echo '<p>You need to authenticate.</p>';
//     exit;
// }
      $user = "testbca5";
      $username =strtoupper(substr($user,-3));
      $password = "AbcXyz123";
      $whitelabel = strtoupper(substr(base_convert("1234514444", 10, 36),-3));

      // $result = md5($whitelabel.$password.$username);
      // echo "SALTPREFIX  =".$whitelabel;
      // echo "<br> SALTSUFFIX   =".$username;
      // echo "<br> password   =".$result;

$query = <<<GQL
  mutation { 
    login ( 
        whitelabel_id: "KEZXFW",
        captcha: "3IKG",
        platform: "ANDROID",
        app_build_version: "1.0"
    ) { session_id, user_id, authorization } 
}
GQL;
$graphqlEndpoint = 'https://fintech-stg.bedigital.co.id/elog/graph/v1/member';
$client = new \GuzzleHttp\Client();

$response = $client->request('POST', $graphqlEndpoint, [
  'auth' => [$user, $password, 'digest'],
  'headers' => [
    'WWW-Authenticate' => 'Digest',
    'User-Agent' => 'testing/1.0',
    'realm' => 'BEDIGITAL',
  ],
  'json' => [
    'query' => $query
  ],
]);

$json = $response->getBody()->getContents();
$body = json_decode($json);
$data = $body->data;

// var_dump($body);

// $client = new \GuzzleHttp\Client();
// $response = $client->request('POST', 'https://fintech-stg.bedigital.co.id/elog/graph/v1/member',
// [
//                     'auth'    => [
//                         $user,
//                         $password
//                     ],
//                     'headers' => [
//                         'Realm : BEDIGITAL'
//                     ]
// ]
// );
// mutation { 
//     login ( 
//         whitelabel_id: "KEZXFW",
//         captcha: "NYUS",
//         platform: "ANDROID",
//         app_build_version: "1.0"
//     ) { session_id, user_id, authorization } 
// }

// $response = Guzzle::post('https://fintech-stg.bedigital.co.id/elog/graph/v1/member', array(
//     'headers' => array('Realm' => 'BEDIGITAL'),
//     'body'    => array('Test' => '123'),
//     'auth'    => array('username' => $user , 'password' => $password),
//     'timeout' => 10
// ));


// echo $response->getStatusCode(); // 200
// echo $response->getHeaderLine('content-type'); // 'application/json; charset=utf8'
// echo $response->getBody(); // '{"id": 1420053, "name": "guzzle", ...}'




//  $client = new GuzzleHttp\Client();

 
// $result = $client->request(
//         'POST',
//         'https://fintech-stg.bedigital.co.id/elog/graph/v1/member', [
//             'verify' => false,
//             'auth' => [$user, $password, 'digest'] 
//         ]);


     

      // Function to parse the http auth header.
      // From http://www.php.net/manual/en/features.http-auth.php
      

 // $digest_values = $this->http_digest_parse($_ENV['HTTP_AUTHORIZATION']);

      // var_dump($result);

// fastku
        //-- graphql -----------------------------------------------------------------------//
//         whitelabel_id = 1234514444 ==> base36 = KEZXFW
// username = testbca5
// SALTPREFIX = XFW
// SALTSUFFIX = CA5
// PasswordPlain = AbcXyz123
// HASIL encryption Password = MD5 (  SALTPREFIX + PasswordPlain + SALTSUFFIX  )
//                                               = MD5 ( "XFWAbcXyz123CA5" )
//                                               = edbdd8ecf34e4bba5779c2d29cd59ecf

// username = 6281380298484
// SALTPREFIX = XFW
// SALTSUFFIX = 484
// PasswordPlain = 123456
// HASIL encryption Password = MD5 (  SALTPREFIX + PasswordPlain + SALTSUFFIX  )
//                                               = MD5 ( "XFW123456484" )
//                                               = 36c331040a6aaabc12ceb70875720a82





       // return view('admin.trx.index', $this->viewData);

    } function getkirim(Request $request) {
           
       return view('admin.trx.index', $this->viewData);

    }
    function getpengaturan(Request $request) {
           
       return view('admin.trx.pengaturan', $this->viewData);
 
    } function getpengaturanalm(Request $request) {
           
       return view('admin.trx.pengaturanalamat', $this->viewData);

    }

    function lamalogin(Request $request) {
        
        $host = $request->getHttpHost();
        log::info("login STart".$host);
        $userName = $request->input('username');
        $plainpassword = $request->input('password');
        $userID = 0; 
        $recaptcha = $request->input('textcapthca');
        // $recaptcha = $request->input('g-recaptcha-response');
        $tokenuid = $request->input('uid');
        $tokenotp = $request->input('otp');
        
        $user = "testbca5";
      $usernamesf =strtoupper(substr($userName,-3));
      
      $whitelabel = strtoupper(substr(base_convert($this->whitelabelID, 10, 36),-3));
      $whitelabelID = strtoupper(base_convert($this->whitelabelID, 10, 36));

      // $result = md5($whitelabel.$password.$username);
      // echo "<img src=\"https://fintech-stg.bedigital.co.id/elog/graph/v1/member/captcha/kezxfw/image.png\">";
      // echo "<br> SALTSUFFIX   =".$username;
      // echo "<br> password   =".$result;
      
      $wlID = "KEZXFW";
      $captcha = "AQT9";
      $platform = "WEB";
      $AppID = "1.0";



        $tokenuidfinal='';
        $userID=0;
        $msg="Admin White Label";
        $this->userAgent = $request->header('User-Agent'); 

 

      $resultdarisufix = md5($whitelabel.$plainpassword.$username);
      // echo "SALTPREFIX  =".$whitelabel;
      // echo "<br> SALTSUFFIX   =".$username;
      // echo "<br> password   =".$result;



$clients = new Client(
    'https://fintech-stg.bedigital.co.id/elog/graph/v1/member',
    [ 
         'auth' => [$userName, $plainpassword, 'digest'],
        'headers' => [
            'WWW-Authenticate' => 'Digest',
            'User-Agent' => 'webadminfastku/1.0',
            'realm' => 'BEDIGITAL',
            'Content-Type' => 'application/json',
        ],
       
       
    ]
);

$gql = <<<QUERY
mutation { 
    login ( 
        whitelabel_id: "$whitelabelID",
        captcha: "$recaptcha",
        platform: "$platform",
        app_build_version: "$AppID"
    ) { session_id, user_id, authorization } 
}
QUERY;

try {
    
        $results = $clients->runRawQuery($gql);


}
catch (QueryError $exception) {
    print_r($exception->getErrorDetails());
    exit;
}
// print_r($results->getData());
   $results->reformatResults(true);
     log::info("login - data=".json_encode($results));





        // $password = $request->input('xpwd');
        // $this->userAgent = $request->header('User-Agent');
        $q = WhiteLabelAdminModel::where('username', $userName)
        ->where('web_app', 'KURIRKU')
                ->first();
        // log::info("username ".json_encode($q)); 
                // log::info(" data list-agen-controller".json_encode($q));

  // if(!is_null($recaptcha) || (!empty($recaptcha)))
  //      {
 
        if ($q != null) {
          // if ($q->master_agent_id != null) {


                $this->arraytokenuid = $this->helpdeskBL->gettoken($q->white_label_id,$q->id); 
                $this->arrayIpList = $this->helpdeskBL->getipwhitelist($q->id); 
                $ipwhitelistAccess = in_array($this->ipClient, $this->arrayIpList)?"grandedIp":"NotgrandedIp";


// $this->ipClient
        // log::info("array_token wl=".$q->white_label_id." id=".$q->id." ,".json_encode( $this->arraytokenuid));
        log::info("array_ip, =".json_encode( $this->arrayIpList));
                   

          if ( $ipwhitelistAccess == "grandedIp" || $q->ip_allowed == 'Y' ) {
 

            if ($q->active == 'Y') {

                 if($q->password_type == 'MD5'){
                        $password = md5($plainpassword);
                    }else if($q->password_type == 'PLAIN'){
                        $password =  $plainpassword;
                    }


                if (strcmp($password, $q->password_enc) == 0) {
                    $token = time();
                    
                     $q->token = $token;
                     $q->save();

                     $master_agent_fullname = $this->helpdeskBL->getFullnameMa($q->master_agent_id);

                    $data = [
                        'id' => $q->id,
                        'white_label_id' => $q->white_label_id,
                        'username' => $q->username,
                        'fullname' => $q->fullname,
                        'email' => $q->email,
                        'group' => $q->group,
                        'token' => $token,
                        'master_agent_id' => $q->master_agent_id,
                        'master_agent_fullname' => $master_agent_fullname,
                        'appname' => $this->apps
                    ];           
                    
                    $userID =  $q->id;

                        log::info("data session".json_encode($data));

                    if(!empty($tokenotp) && !is_null($tokenotp))
                    {

                      

                    //      // $objBL = new CmsBL();
                         if($this->helpdeskBL->verifyDeviceToken( $tokenotp,$q->white_label_id,$q->id,$tokenuid))
                         {
                                $this->putQueueLog( $userID, $userName, "Sukses" , "0" );
                                 $this->session->save($data);  
                                  // Log::info("SUKSES phase 1");
                                 return response()->json($this->notif->build());
                    //         // Log::info("SessionLogic::setData tokenuid=".$tokenuid);
                           
                    //         // $cmsbl = new CmsBL();
                    //         $this->helpdeskBL->sendmaillogin($q->email,$q->fullname,$q->username,$this->userAgent,$msg);

                            
                         }
                         else
                         {
                                $this->notif->addMessage('OTP yang anda masukkan salah'); return response()->json($this->notif->build());
                                $this->putQueueLog( $userID, $userName, "OTP yang anda masukkan salah" , "7" );
                         }
                        

                    }
                   

                  // Log::info("TokenArray==> HASIL=".(in_array($tokenuid, $this->arraytokenuid)?"true":"false"));
                            $result_token=in_array($tokenuid, $this->arraytokenuid)?"allowed":"false";
                            // log::info("result token = ".$result_token." is_allowd= ".$q->is_allowed." array=".json_encode($this->arraytokenuid));
 
                         if ( ($result_token == "allowed") || ($q->is_allowed == 'Y')) {
                                $this->putQueueLog( $userID, $userName, "Sukses" , "0" );
                                $this->session->save($data);                     
                                 // Log::info("SUKSES phase 2");
                            //     // $cmsbl = new CmsBL();
                            //     $this->helpdeskBL->sendmaillogin($q->email,$q->fullname,$q->username,$this->userAgent,$msg);
                                 return response()->json($this->notif->build());

             
                              }
                       else { 

                                    //     // $objBL = new CmsBL();
                                        $core = new UtilPhone();
                                        // $objBL->createDeviceToken($q->white_label_id,$q->id,$tokenuid);
                                        if(!empty($q->phone_number) && $core->isPhoneValid($q->phone_number))
                                        {
                                            if($this->helpdeskBL->sendSMSDeviceToken( $q->phone_number,$tokenuid,$q->white_label_id,$q->id, $userName ))
                                           {
                                                $this->notif->addMessage('getotp'); return response()->json($this->notif->build());

                                           }
                                           else
                                           {    
                                                $this->putQueueLog( $userID, $userName, "error sending OTP" , "5" );
                                                $this->notif->addMessage('error sending OTP'); return response()->json($this->notif->build());

                                           }
                                        }
                                        else{
                                                $this->putQueueLog( $userID, $userName, "error, invalid Phone Numbe" , "6" );
                                                $this->notif->addMessage('error, invalid Phone Number<br> please contact your admin to validate your phone number'); return response()->json($this->notif->build());

                                        }
                 
                            }
                }
                 else {
                     $this->putQueueLog( $userID, $userName, "Invalid User name or password" , "3" );

                    $this->notif->addMessage('Invalid User name or password '); return response()->json($this->notif->build());
                }
           

           } else {
                $this->putQueueLog( $userID, $userName, "Your user is not active. Please contact administrator" , "4" );
                $this->notif->addMessage('Your user is not active. Please contact administrator.'); return response()->json($this->notif->build());
            }

             } else {
                $this->putQueueLog( $userID, $userName, "Your IP is not Allowed. Please contact administrator" , "99" );
                $this->notif->addMessage('Your IP is not Allowed. Please contact administrator.<br>  Your IP is '.$this->ipClient); return response()->json($this->notif->build());
            } 
            
        // } else {
        //     $this->putQueueLog( $userID, $userName, "Invalid Master Agent" , "3" );

        //     $this->notif->addMessage('Invalid Master Agent, Please contact administrator'); return response()->json($this->notif->build());
        // }  
      } else {
            $this->putQueueLog( $userID, $userName, "Invalid User name or password" , "3" );

            $this->notif->addMessage('Invalid User name or password'); return response()->json($this->notif->build());
        }
    // } else  {
    //     $this->notif->addMessage('Are you a human'); return response()->json($this->notif->build());
    // }
      
    }
 

    function changePasswd(Request $request) {
        return view('admin.login.password', $this->viewData);
 
    } 


     function dash2(Request $request) {
Log::info("UserCOntroller::dash2");
Log::info("UserCOntroller::dash2 START DASHBOARD empty_content");
  return view('admin.index', $this->viewData);
     }

     

     function dash(Request $request) {
      $this->viewData['sumtrxma']=[];
       $this->viewData['totlallagen'] = $this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'));
       $this->viewData['actagen'] = $this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'),'ACTIVE');
       $this->viewData['regagen'] = $this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'),'REGISTERING');
      $this->viewData['ungagen'] = $this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'),'UNAPPROVED');
  
      /*
      $this->viewData['totlallagen'] = ''; // $this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'));
      $this->viewData['actagen'] = ''; //$this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'),'ACTIVE');
      $this->viewData['regagen'] = ''; //$this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'),'REGISTERING');
      $this->viewData['ungagen'] = ''; //$this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'),'UNAPPROVED');
      */

       
for($a=1;$a<=12;$a++)
  {
     $sumtrxma = $this->helpdeskBL->listSumTrxMa($this->session->get('master_agent_id'),$a);
     array_push($this->viewData['sumtrxma'],$sumtrxma); 

  }
 
              return view('admin.dashboard', $this->viewData);
 
    }


function infoTarif(Request $request) {
  $this->viewData['x']='';
  return view('admin.info_tarif', $this->viewData);
}


function getUserList(Request $request) {

        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', '');
        $sm = Input::get('sm', '');
        $tableselect="";
        $search = Input::get('search');
        
        $fields = [
            ['username', 'Agent ID'],
            ['fullname', 'username'],
            ['master_agent_id', 'Master Agent'],
            ['email', 'Agent Email'],
            ['phone_number', 'Phone Number'],            
            ['group', 'Group Priveleges']

        ]; 
          $tableselect = Input::get('tableselect');  
    try {
         $this->tableSorter->setupSorter(secure_url('/ma/userhdesk_list.html'), $fields, $searchFor, $sf, $sm, $tableselect, $search);   
         // $listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id'));
 
switch ($tableselect) {
    case 'agent_id':
      $tablename="agent.id";
    break;
    case 'name':
      $tablename="agent_profile.fullname";
    break; 
    case 'username':
      $tablename="agent.username";
    break;
    case 'master_agent':
      $tablename="agent.master_agent_id";
    break;
    case 'email':
      $tablename="agent_profile.email";
    break;
     case 'phone':
      $tablename="agent_profile.phone_number";
    break; 
    default:
      $tablename="";
    break;
}

        if(!empty($search) && (!empty($tableselect))) {
          
            $someModel = new  WhiteLabelAdminModel;
            $someModel->setConnection($this->conn111); 
            $q = $someModel->whereIn('agent.id',  $listAgent)
                    ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
                    ->join('master_agent_profile','master_agent_profile.master_agent_id','agent.master_agent_id')
                    ->selectRaw('agent.id as agent_id,CONV(agent.id, 10, 36) as agent_base, agent.status, username,  agent_profile.fullname  as agent_name,agent_profile.email,agent_profile.phone_number,agent.master_agent_id,ifnull(master_agent_profile.fullname, master_agent_profile.master_agent_id) as m_agent_name,agent_profile.npwp_number,agent_profile.id_card_type ,agent_profile.id_card_number, agent_profile.address,agent_profile.city_name,daily_trx_total_price_max')
                    ->where(function ($query) use ($tablename,$search) { 
                        $query->where( $tablename, 'like', '%'.$search.'%');
                    });
        } else {
            $someModel = new  WhiteLabelAdminModel;
            $someModel->setConnection($this->conn111); 
            $q = $someModel
                    ->where('web_app','MA')
                    ->where('active','Y')
                    ->where('master_agent_id',$this->session->get('master_agent_id'));
        }
        

        //$q = $someModel->whereIn('id',  ['1544844145','1544756392','1544775157','1548735594']);
        $this->tableSorter->setupPaging($q, $pg);
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;        
        $this->viewData['sorter'] = $this->tableSorter;

        // log::info(" data".json_encode($this->viewData));
        } catch ( Exception $e ) {
            Log::info("getUserList::index exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        return view('admin.user.index', $this->viewData);
     }

 function getkurirfromcity(Request $request) {
log::info("getkurirfrompostcode - START");
// Create Client object to contact the GraphQL endpoint
$client = new Client(
    'https://fintech-stg.bedigital.co.id/elog/graph/v1/shipment',
    []  // Replace with array of extra headers to be sent with request for auth or other purposes
);

$provinceId = Input::get('idprov_from');
$provinceId2 = Input::get('idprov_to');
$cityId = Input::get('city_from');
$cityId2 = Input::get('city_to');
$kecId = Input::get('kec_from');
$kecId2 = Input::get('kec_to');
log::info("getlocationfrompostcode - provinceId=".$provinceId);
log::info("getlocationfrompostcode - provinceId2=".$provinceId2);
log::info("getlocationfrompostcode - cityId=".$cityId);
log::info("getlocationfrompostcode - cityId2=".$cityId2);
log::info("getlocationfrompostcode - kecId=".$kecId);
log::info("getlocationfrompostcode - kecId=".$kecId2);

// $postCode = '15810';
// $postCodeDest = '12940';
// $provinceId = '31'; 
// $cityName = 'Jakarta Barat';
// $addrName = 'Jawa Tengah, Sragen';

$spl= $builder = (new QueryBuilder('rates'))
    ->setArgument('origin', new RawObject('{province_id: "'.$provinceId.'" , city_id : "'.$cityId.'" , village_id : "'.$kecId.'"  }'))
    ->setArgument('destination', new RawObject('{province_id: "'.$provinceId2.'" , city_id : "'.$cityId2.'" , village_id : "'.$kecId2.'"  }'))
    // ->setArgument('destination', new RawObject('{province_id: "'.$provinceId2.'" , city_name : "'.$cityName2.'"  }'))
    // ->setArgument('origin', new RawObject('{postal_code: "'.$postCode.'" }'))
    // ->setArgument('destination', new RawObject('{postal_code: "'.$postCodeDest.'" }'))
    // ->setArgument('destination', new RawObject('{address: "'.$addrName.'" }'))
    ->selectField('couriers');
// $gql = $builder->getQuery();
// print_r($gql);


    /*
    query {
    rates (
        origin: {
            province_id: "31", 
            city_name: "Jakarta Barat"
        },
        destination: {
            province_id: "11", 
            city_name: "Aceh Barat"
        }
    ) 
    { couriers }
}

query {
    rates (
        origin: {
            province_id: "31", 
            city_name: "Jakarta Barat"
        },
        destination: {
            address: "Jawa Tengah, Sragen"
        }
    ) 
    { services }
}


*/
 
// Run query to get results
try {
    $results = $client->runQuery($spl);
}
catch (QueryError $exception) {

     print_r($exception->getErrorDetails());
    exit;
}

// Display original response from endpoint
// var_dump($results->getResponseObject());

// Display part of the returned results of the object
// var_dump($results->getData()->rates);

// Reformat the results to an array and get the results of part of the array
$results->reformatResults(true);
log::info("getlocationfrompostcode - data=".json_encode($results->getData()['rates']));
return $results->getData()['rates'];

     }

 function getlocationfrompostcode(Request $request) {
log::info("getlocationfrompostcode - START");
// Create Client object to contact the GraphQL endpoint
$client = new Client(
    'https://fintech-stg.bedigital.co.id/elog/graph/geoname',
    []  // Replace with array of extra headers to be sent with request for auth or other purposes
);
  
        $postCode = Input::get('postalcode');
log::info("getlocationfrompostcode - postCode=".$postCode);

// $postCode = '15810';
// $postCodeDest = '12940';
// $provinceId = '31';
// $cityName = 'Jakarta Barat';
// $addrName = 'Jawa Tengah, Sragen';

$spl= $builder = (new QueryBuilder('postal_code'))
    // ->setArgument('origin', new RawObject('{province_id: "'.$provinceId.'" , city_name : "'.$cityName.'"  }'))
    ->setArgument('code ', $postCode)
    // ->setArgument(['code' => $postCode])
    // ->setArgument('destination', new RawObject('{postal_code: "'.$postCodeDest.'" }'))
    // ->setArgument('destination', new RawObject('{address: "'.$addrName.'" }'))
    ->selectField('city_id')
    ->selectField('city_name')
    ->selectField('province_id')
    ->selectField('province_name');

// $gql = $builder->getQuery();
// print_r($gql);
    /*
    query {
    rates (
        origin: {
            province_id: "31", 
            city_name: "Jakarta Barat"
        },
        destination: {
            province_id: "11", 
            city_name: "Aceh Barat"
        }
    ) 
    { couriers }
}

query {
    rates (
        origin: {
            province_id: "31", 
            city_name: "Jakarta Barat"
        },
        destination: {
            address: "Jawa Tengah, Sragen"
        }
    ) 
    { services }
}


*/
 
// Run query to get results
try {
    $results = $client->runQuery($spl);
}
catch (QueryError $exception) {

     print_r($exception->getErrorDetails());
    exit;
}

// Display original response from endpoint
// var_dump($results->getResponseObject());

// Display part of the returned results of the object
// var_dump($results->getData()->rates);

// Reformat the results to an array and get the results of part of the array
$results->reformatResults(true);
return $results->getData()['postal_code'];
     }


 function getrefreshcaptcha(Request $request) {
log::info("getrefreshcaptcha - START");
 $client = new Client(
    'https://fintech-stg.bedigital.co.id/elog/graph/v1/member',
    [] 
);  
        
log::info("getrefreshcaptcha - WL= ".$this->whitelabel);

// mutation {
//     refreshCaptcha ( whitelabel_id:"KEZXFW" )
//     { code, message }
// }

$mutation = (new Mutation('refreshCaptcha'))
    // ->setArguments(whitelabel_id: "'.$this->whitelabel.'" }')])
          ->setArguments(['whitelabel_id' => "'.$this->whitelabel.'"])
    ->setSelectionSet(
        [
            'code',
            'message',
         ]
    );
$results = $client->runQuery($mutation);
   log::info("Result data = ".json_encode($results));
// Reformat the results to an array and get the results of part of the array
$results->reformatResults(true);
return $results->getData('refreshCaptcha');

     }

 function cekuseraktif(Request $request) {
log::info("cekuser - START");
// Create Client object to contact the GraphQL endpoint
$client = new Client(
    'https://fintech-stg.bedigital.co.id/elog/graph/v1/member',
    []  // Replace with array of extra headers to be sent with request for auth or other purposes
);

   /*
    query {
    user (
        whitelabel_id: "KEZXFW",
        username: "6281380298484"
    ) 
        { username, user_id, user_status }
}

*/

// Create the GraphQL query
$gql2 = $builder = (new QueryBuilder('user'))
    ->setArgument('whitelabel_id', 'KEZXFW')
    ->setArgument('username', '6281380298484')
    ->selectField('username')
    ->selectField('user_id')
    ->selectField('user_status');
    
// Run query to get results
try {
    $results = $client->runQuery($gql2);
}
catch (QueryError $exception) {

    // Catch query error and desplay error details
    print_r($exception->getErrorDetails());
    exit;
}

// Display original response from endpoint
var_dump($results->getResponseObject());

// Display part of the returned results of the object
var_dump($results->getData()->user);

// Reformat the results to an array and get the results of part of the array
$results->reformatResults(true);
print_r($results->getData()['user']);

     }


    function saveRunningText(Request $request) { 
        Log::info("START Running Text" );
        $opsi = $request->input('opsi');
        $owner_type = $request->input('sendto');
        $agentid = $request->input('agentid');
        $starts = $request->input('min');
        $stops = $request->input('max');
        $text_message = $request->input('rtext');
       
 
        try {

        if ($starts == '') { $this->notif->addMessage('start date is required'); }
        if ($stops == '') { $this->notif->addMessage('End date is required'); } 
        if ($text_message == '') { $this->notif->addMessage('Running Text is required'); }
                  $q = new RunningText(); 
                  $q->setConnection($this->conn); 

                  if ($this->notif->code == \App\Http\Helpers\NotificationHelper::CODE_OK) {
                    try {
                         
                          if($owner_type == "MASTER_AGENT")
                          {
                             $userid = $this->session->get('master_agent_id'); 
                          }
                          else
                          {
                            $userid = $agentid;
                          }
                          if($opsi == "input")
                                  {
                                   
                                    $q->white_label_id = $this->whitelabel;
                                    $q->owner_type =  $owner_type;//'MASTER_AGENT';
                                    $q->owner_id =  $userid ;
                                    $q->show_date_start = $starts;
                                    $q->show_date_stop = $stops;
                                    $q->text_message = $text_message; 
                                    $q->active = 'Y'; 
                                    $q->created_at = date('Y-m-d H:i:s');
                                    $q->save(); 
                               
                                  }
                                  else
                                  {
                                    $q= RunningText::find($userid); 
                                    $q->password_enc = md5($pspwd);
                                    $q->password_type = 'md5';
                                    $q->save(); 

                                  }


                          } catch (Exception $e) {
                            // DB::rollBack();
                            $this->notif->addMessage($e->getMessage());
                          }
                    }
            
            




        } catch ( Exception $e ) {
        Log::info("UserBL::saveRunningText exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        
        return response()->json($this->notif->build());
    }  

    function delRunningText(Request $request) { 
        
        $idnya = $request->input('id'); 
       // Log::info("Delete Text id=".$idnya );
 
        try {

        // if ($idnya == '') { $this->notif->addMessage('id is required'); } 
                 

                  if ($this->notif->code == \App\Http\Helpers\NotificationHelper::CODE_OK) {
                    // try {
                        

                         // Log::info("start to Delete  " );
                                    // $q = new RunningText(); 
                                    // $q->setConnection($this->conn); 
                                    // $q->where('id',$idnya); 
                                    // $q->active = "N";  
                                    // $q->save();


                                              $BankAccounts2 = new  RunningText;
                                              $BankAccounts2->setConnection($this->conn); 
                                              $B2 = $BankAccounts2
                                              ->where('id', '=', $idnya) 
                                              ->update([
                                              'active' => 'N' ,
                                              'updated_at' => date('Y-m-d H:i:s') 
                                              ]); 




                        // Log::info("end Delete".json_encode($B2));
                                  

                          // } catch (Exception $e) {
                          //   // DB::rollBack();
                          //   $this->notif->addMessage($e->getMessage());
                          // }
                    }
            
            




        } catch ( Exception $e ) {
        Log::info("User::delRunningText exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        
        return response()->json($this->notif->build());
    }
    function savePasswd(Request $request) { 
        Log::info("START Update Password" );
        $pspwd = $request->input('new_pswd');
        $pspwd2 = $request->input('new_pswd2');
        $userid = $this->session->get('id');
        $userName = $this->session->get('username');
        Log::info("savePasswd:: id=".$userid." username=".$userName);

        try {
            if($pspwd == $pspwd2)
            {
                  if ($this->notif->code == \App\Http\Helpers\NotificationHelper::CODE_OK) {
                    try {
                          Log::info("savePasswd:: Go passowrd = ".$pspwd);
                            $q = WhiteLabelAdminModel::find($userid); 
                                    $q->password_enc = md5($pspwd);
                                    $q->password_type = 'md5';
                                    $q->save(); 
                               
                        // $this->notif->addMessage('Saved',200);
                          } catch (Exception $e) {
                            DB::rollBack();
                            $this->notif->addMessage($e->getMessage());
                          }
                    }
            }
            else
            {
                 $this->notif->addMessage('Konfirmasi dengan Password berbeda!'); 

            }
            




        } catch ( Exception $e ) {
        Log::info("AuthUserBL::putQueueLog user_id=".$userID." username=".$username." err_code=".$errCode." err_msg=".$errMsg." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        
        return response()->json($this->notif->build());
    }

    public function putQueueLog( $userID, $username,  $errMsg ,$errCode ) {
        Log::info("Mulai ngelog - agen".$this->userAgent.'--'.$this->ipClient);
        $b = false;
        try {
            $queuename = "log-userlogin-MA-Helpdesk".$userID;
            $input = LogUserLoginBL::createInput( $userID, $username, $this->apps );
            $logBL = new LogUserLoginBL( $input );
            log::info("input =".json_encode($input));
            $logBL->setLogDateTime()
                  ->setErrorCode( $errCode )
                  ->setErrorMessage( $errMsg )
                  ->setIPClient( $this->ipClient )
                  ->setUserAgent( $this->userAgent );
            if (Definition::USE_ASYNC_PROCESS) {
                $input = $logBL->getInput();
                $job = new InsertLogUserLoginJob( $input );
                if (! empty($queuename)) {
                    $job->onQueue($queuename);
                }
                dispatch( $job );
                $b = true;
            } else {
                $id = $logBL->insert();
                $b = boolval($id);
            }
        } catch ( Exception $e ) {
            Log::info("AuthUserBL::putQueueLog user_id=".$userID." username=".$username." err_code=".$errCode." err_msg=".$errMsg." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        return $b;
    }

  

    }
 
 
