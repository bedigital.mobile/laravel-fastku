<?php

namespace App\Http\Controllers\admin\cms;

use DB;
use Log;
use Config;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\BusinessLogic\HelpdeskBL;

use App\Http\Controllers\BDGBaseController;
use App\Models\PartnerTrxReqModel;
use App\Models\PartnerDepositModel;
use App\Models\SummaryTrxDaily;
use App\Models\admin\AgentModel;
use App\Models\DynamicDuoTables;

use Rap2hpoutre\FastExcel\FastExcel;
class SummaryController extends BDGBaseController
{

     public function __construct() {
        parent::__construct();    
        // $this->conn = Config::get ( 'mysql_stg' );
        $this->conn = Config::get ( 'webconf.cms.connectiondata' );
        $this->connali = Config::get ( 'webconf.cms.connectionalibaba' );
        $this->helpdesk = new HelpdeskBL; 

        $this->PartnerTrxReqModel = new  PartnerTrxReqModel; 
        $this->PartnerTrxReqModel->setConnection($this->conn); 

        $this->SummaryTrxDaily = new  SummaryTrxDaily; 
        $this->SummaryTrxDaily->setConnection($this->connali); 

    }
    

 function sumreportpaketku(Request $request){
   
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0); 

    $tableselect = $request->tableselect;
    $search =$request->input('search');

    $tableselect2 = $request->tableselect2;
    $search2 =$request->input('search2');
 
    $opsidates =$request->input('opsidates');

    $fromdates =$request->input('fromdates');
    $todates =$request->input('todates');

    $mnt =$request->input('mnt');
    $mnt=$mnt+1;
    $yr =$request->input('yr');

    $week= Input::get('week',1);
 
    $thismonth = date("m"); 
    $thisyear = date("Y");  


    $daterange = 0;
    $listfrom=[];
   
    $fields = [
            ['trx_date','Log DateTime'],
            ['trx_id', 'Transaction Id'],
            ['biller_name' , 'Biller'],
            ['partner_name', 'Partner'],
            ['product_id', 'Product Id'],
            ['product_code', 'Product Code'],
            ['product_name' , 'Product Name'],
            ['product_denom' , 'Denom'],
            ['nominal','Nominal'],
            ['info1','Transaction Info 1'],
            ['info2','Transaction Info 2'],
            ['info3','Transaction Info 3'],
            ['created_at','Created At'], 
            ['created_at','Balance Before'], 
            ['created_at','Balance After'] 

        ];  
  

        $dateString = date("Y-m-d");
        $listmnth=[];
        $listyear=[];
        $lastDateOfMonth = date("t", strtotime($dateString));
        for($a=1;$a<=$lastDateOfMonth;$a++)
        {
            array_push($listfrom,$a);
        }

        for($i = 1 ; $i <= 12; $i++)
        { 
         array_push($listmnth,date("F",strtotime(date("Y")."-".$i."-01"))); 
        }


        for($m = 2019 ; $m <= 2030; $m++)
        { 
         array_push($listyear,$m); 
        }



        if(($opsidates == 'month')) {  
        
 
 
$tables =  $this->SummaryTrxDaily 
            ->where('trx_month','=',$mnt)
            ->where('trx_year','=',$yr)
            ->where('id_biller','=','1')->get();
 


if (empty($tables)) { 

        $this->viewData['listfrom'] = $listfrom;
        $this->viewData['listmnth'] = $listmnth;
        $this->viewData['listyear'] = $listyear;



        $this->viewData['today'] = date("Y-m-d");
        $this->viewData['months'] = date("F");
        $this->viewData['years'] = date("Y");
    return view('admin.report.report_sum_empty', $this->viewData );
}
else
{
 

         if(($search == '') && ($search2 == '') )
        {

log::info("query month 1");
log::info("query month none search month =".$mnt." and year=".$yr); 

   $q =  $this->SummaryTrxDaily 
            ->where('month','=',$mnt)
            ->where('year','=',$yr)
            ->get();
    $this->viewData['infosearch'] =" ".date("F", mktime(0, 0, 0, $mnt, 1)).", ".$yr;
         }   
            }
     
             }else {
 
            log::info("query default"); 
            log::info("query month none search month =".$thismonth." and year=".$thisyear); 
            $q =  $this->SummaryTrxDaily 
            ->orderby('month','asc')
                 ->get();
            $this->viewData['infosearch'] =" ".date("F").", ".$thisyear;

        }
 

        $this->viewData['listdata'] = $q; 
        $this->viewData['yr'] = $yr; 
        $this->viewData['mnt'] = $mnt; 
        $this->viewData['opsidates'] = $opsidates;
        $this->viewData['fromdates'] = $fromdates;
        $this->viewData['todates'] = $todates;
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search; 
        $this->viewData['tableselect2'] = $tableselect2;
        $this->viewData['search2'] = $search2;


        $this->viewData['today'] = date("Y-m-d");
        $this->viewData['months'] = date("F");
        $this->viewData['years'] = date("Y");
        $this->viewData['listfrom'] = $listfrom;
        $this->viewData['listmnth'] = $listmnth;
        $this->viewData['listyear'] = $listyear;
        $this->viewData['week'] = $week;

 


        
        return view('admin.report.sum_report_trx', $this->viewData );
 }
 function expsumreportamanah(Request $request){
   
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0); 

    $tableselect = $request->tableselect;
    $search =$request->input('search');

    $tableselect2 = $request->tableselect2;
    $search2 =$request->input('search2');
 
    $opsidates =$request->input('opsidates');

    $fromdates =$request->input('fromdates');
    $todates =$request->input('todates');

    $mnt =$request->input('mnt');
    // $mnt=$mnt+1;
    $yr =$request->input('yr');

    $week= Input::get('week',1);
 
    $thismonth = date("m"); 
    $thisyear = date("Y");  


    $daterange = 0;
    $listfrom=[];
   
    $fields = [
            ['trx_date','Log DateTime'],
            ['trx_id', 'Transaction Id'],
            ['biller_name' , 'Biller'],
            ['partner_name', 'Partner'],
            ['product_id', 'Product Id'],
            ['product_code', 'Product Code'],
            ['product_name' , 'Product Name'],
            ['product_denom' , 'Denom'],
            ['nominal','Nominal'],
            ['info1','Transaction Info 1'],
            ['info2','Transaction Info 2'],
            ['info3','Transaction Info 3'],
            ['created_at','Created At'], 
            ['created_at','Balance Before'], 
            ['created_at','Balance After'] 

        ];  
  

        $dateString = date("Y-m-d");
        $listmnth=[];
        $listyear=[];
        $lastDateOfMonth = date("t", strtotime($dateString));
        for($a=1;$a<=$lastDateOfMonth;$a++)
        {
            array_push($listfrom,$a);
        }

        for($i = 1 ; $i <= 12; $i++)
        { 
         array_push($listmnth,date("F",strtotime(date("Y")."-".$i."-01"))); 
        }


        for($m = 2019 ; $m <= 2030; $m++)
        { 
         array_push($listyear,$m); 
        }



        if(($opsidates == 'month')) {  
        
 
 
$tables =  $this->SummaryTrxDaily 
            ->where('trx_month','=',$mnt)
            ->where('trx_year','=',$yr)
            ->where('id_biller','=','1')->get();
 


if (empty($tables)) { 

        $this->viewData['listfrom'] = $listfrom;
        $this->viewData['listmnth'] = $listmnth;
        $this->viewData['listyear'] = $listyear;



        $this->viewData['today'] = date("Y-m-d");
        $this->viewData['months'] = date("F");
        $this->viewData['years'] = date("Y");
    return view('admin.report.report_sum_empty', $this->viewData );
}
else
{
 

         if(($search == '') && ($search2 == '') )
        {

log::info("query month 1");
log::info("query month none search month =".$mnt." and year=".$yr); 

   $q =  $this->SummaryTrxDaily 
            ->where('trx_month','=',$mnt)
            ->where('trx_year','=',$yr)
            ->where('id_biller','=','1') 
              ->selectRaw('trx_date as "Transaction Date",nama_produk as "Product Name",trx_total as "Total Trx",trx_nominal as "Total Nominal"')
            ->cursor();
    $this->viewData['infosearch'] =" ".date("F", mktime(0, 0, 0, $mnt, 1)).", ".$yr;
         }   
            }
     
             }else {
 
            log::info("query default"); 
            
            $q =  $this->SummaryTrxDaily 
            ->where('trx_month','=',$thismonth)
            ->where('trx_year','=',$thisyear)
            ->where('id_biller','=','1') 
            ->selectRaw('trx_date as "Transaction Date",nama_produk as "Product Name",trx_total as "Total Trx",trx_nominal as "Total Nominal"')
            ->cursor();
            $this->viewData['infosearch'] =" ".date("F").", ".$thisyear;

        }
 
 


        $tx = microtime(true);
        $micro = sprintf("%06d",($tx - floor($tx)) * 1000000);
        $theTime = new \DateTime( date('Y-m-d H:i:s.'.$micro, $tx) );
        $theTimestamp = $theTime->format("YmdHisu");

        $filename = "Report_Bakrie_Amanah_Summary_".$theTimestamp.".xlsx"; 
  

        return (new FastExcel($this->usersGenerator($q)))->download($filename);


 } 

 function usersGenerator($q) {
    foreach ($q as $user) {
        yield $user;
    }
}





}
