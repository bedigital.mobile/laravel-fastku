<?php

namespace App\Http\Controllers\admin\cms;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Controllers\BDGBaseController;
use App\Http\Controllers\admin\cms\FileUploadController;
use App\Models\admin\WhiteLabelModel;
use App\Models\admin\WhiteLabelAdminModel;

class WhiteLabelController extends BDGBaseController
{
    function index() {
		$pg = Input::get('pg', 1);
		$searchFor = Input::get('q');
		$sf = Input::get('sf', 0);
		$sm = Input::get('sm', 0);
		
		$fields = [
			['white_label_name', 'White Label']
		];
		
		$this->tableSorter->setupSorter(url('/white_label.html'), $fields, $searchFor, $sf, $sm);
		
		if($searchFor != '') {
            
		} else {
			
		}
		
//		$this->tableSorter->setupPaging($q, $pg);
        
        return view('admin.white_label.index', $this->viewData);
    }
    
    function myProfile() {
        $q = WhiteLabelAdminModel::find($this->session->get('id'));
        
        $this->viewData['rs'] = $q;
        $this->viewData['STATUS_ACTIVE'] = WhiteLabelAdminModel::STATUS_ACTIVE;
        $this->viewData['STATUS_INACTIVE'] = WhiteLabelAdminModel::STATUS_INACTIVE;
        
        return view('admin.white_label.profile', $this->viewData);
    }
    
    function updateProfile(Request $request) {
        $avatar = $request->input('avatar-file');
        $fullName = $request->input('fullname');
        $description = $request->input('description');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $password = $request->input('xpwd');
        $cpassword = $request->input('ypwd');
        
        if ($fullName == '') { $this->notif->addMessage('Full name is required'); }
        if ($email == '') { $this->notif->addMessage('Email address is required'); }
        else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) { $this->notif->addMessage('Invalid email address format'); }
        if ($phone == '') { $this->notif->addMessage('Phone number is required'); }
        if ($password != '' && strcmp($password, $cpassword) != 0) { $this->notif->addMessage('Password is not match with Confirm Password'); }
        
        if ($this->notif->code == \App\Http\Helpers\NotificationHelper::CODE_OK) {
            $q = WhiteLabelAdminModel::where('white_label_id', '=', $this->session->whiteLabelId())
                    ->where('id', '=', $this->session->get('id'))
                    ->first();
            
            $q->fullname = $fullName;
            $q->description = $description;
            if ($q->photo_filename != $avatar) {
                FileUploadController::deleteAvatar($q->photo_filename);
                $q->photo_filename = $avatar;
            }
            if ($password != '') {
                $q->password_enc = $password;
                $q->password_type = 'MD5';
            }
            $q->email = $email;
            $q->phone_number = $phone;
            $q->updated_at = date('Y-m-d H:i:s');
            $q->save();
        }
        
        return response()->json($this->notif->build());
    }
}
