<?php

namespace App\Http\Controllers\admin\cms;

use Log;
use Config;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Controllers\BDGBaseController;
use App\Models\MasterAgentProfileModel;
use App\Models\admin\MainAgentModel;

class MasterAgentController extends BDGBaseController
{

     public function __construct() {
        parent::__construct();    
        $this->conn = Config::get ( 'webconf.cms.connectiondata' );    
    }

    function search_masteragent(Request $request){
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

    $min =Input::get('min'); //$request->get('min').' 00:00:00';
    $max = Input::get('max');//$request->get('max').' 23:59:59'; //$request->get('max');
    $tableselect = $request->tableselect;
    $search =$request->input('search');
    $timeMin = '00:00:00';
    $timeMax = '23:59:59';
    $someModel = new  MasterAgentProfileModel;
    $someModel->setConnection($this->conn); 
            
    if($min != '')
    {
        $result1 = $min. ' ' . $timeMin; 
    } 
    else
    {
      $result1 = '';  
    }

    if($max != '')
    {
        $result2 = $max . ' ' . $timeMax;
    } 
    else
    {
      $result2 = '';  
    }

    $fields = [
            ['master_agent_id', 'Master Agent Id'],
            ['profile_type', 'Profile Type'],
            ['fullname', 'FullName'],
            ['firstname', 'First Name'],
            ['middlename', 'Middle Name'],
            ['lastname', 'Last Name'],
            ['description', 'Description'],
            ['email', 'Email'],
            ['phone_number', 'Phone Number'],
            ['contact_number', 'Contact_number'],
            ['fax_number' , 'Fax Number'],
            ['place_birth' , 'Place Birth'],
            ['birth_date' , 'Birth Data'],
            ['gender' , 'gender'],
            ['npwp_number' , 'NPWP'],
            ['province_name', 'Province Name'],
            ['city_name', 'City Name'],
            ['address' , 'Address'],
            ['postal_code' , 'Postal Code'],
            ['department' , 'Department'],
            ['contact_person', 'Contact Person'],
            ['created_at', 'Created At'],
            ['master_agent_parent_id', 'Master Agent Upline'],
            ['status', 'Status']


        ]; 
        //,$min,$max,$tableselect,$search)
        $this->tableSorter->setupSorter(url('/agency/master_agent.html'), $fields, $searchFor, $sf, $sm, $min, $max, $tableselect, $search);
        if(($search != '') && ($min != '' && $max != '')) {  

             $q = $someModel->whereBetween(
                'created_at', 
                [
                    $result1,
                    $result2
                ]
                )
                ->where($tableselect , $search)
                ->orderBy('created_at' , 'desc')
            ->join('master_agent','master_agent.id','master_agent_id')
            ->selectRaw('master_agent_id ,master_agent_parent_id, profile_type , fullname ,firstname ,middlename, lastname , description ,email , phone_number , contact_number , fax_number, place_birth , birth_date , gender , npwp_number ,province_name , city_name , address , postal_code , department , contact_person ,master_agent.created_at,master_agent.status');

        }else if($min != '' && $max != ''){
            $q = $someModel->whereBetween(
                'created_at', 
                [
                    $result1,
                    $result2
                ]
                )
                ->orderBy('created_at' , 'desc');
        }else if(($search != '' && $tableselect != '') && ($min == '' && $max == '')){
            $q = $someModel->where($tableselect , $search)
                ->orderBy('created_at' , 'desc');      
        }else {
            $q = $someModel->orderBy('created_at' , 'desc')
            ->join('master_agent','master_agent.id','master_agent_id')
            ->selectRaw('master_agent_id ,master_agent_parent_id, profile_type , fullname ,firstname ,middlename, lastname , description ,email , phone_number , contact_number , fax_number, place_birth , birth_date , gender , npwp_number ,province_name , city_name , address , postal_code , department , contact_person ,master_agent.created_at,master_agent.status ');
        }
  
        $this->tableSorter->setupPaging($q, $pg);

        $this->viewData['sorter'] = $this->tableSorter;
        $this->viewData['pg'] = $pg;
        $this->viewData['searchFor'] = $searchFor;
        $this->viewData['min'] = $result1;
        $this->viewData['max'] = $result2;
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
        
        return view('admin.report.master_agent', $this->viewData );
 }


    function view($agentId) {
    $this->viewData['rs']="";

    $this->viewData['ma_list']="";
    $this->viewData['status_list']=array('SUSPENDED','DEACTIVATED','UNAPPROVED');

    $MaModel2 = new  MasterAgentProfileModel;
    $MaModel2->setConnection($this->conn);  

    $MaModel = new  MainAgentModel;
    $MaModel->setConnection($this->conn); 


    $this->viewData['ma_list'] = $MaModel->join('master_agent_profile','master_agent_profile.master_agent_id','master_agent.id')
    ->selectRaw('master_agent.id as master_agent_id, ifnull(master_agent_profile.fullname, master_agent.username) as m_agent_name')->get();


    $this->viewData['rs'] = $MaModel2->join('master_agent','master_agent.id','master_agent_id')
            ->selectRaw('master_agent_id ,master_agent_parent_id, profile_type , fullname ,firstname ,middlename, lastname , description ,email , phone_number , contact_number , fax_number, place_birth , birth_date , gender , npwp_number ,province_name , city_name , address , postal_code , department , contact_person ,master_agent.created_at,master_agent.status ')->where('master_agent.id', '=', $agentId)
                                    ->firstorFail();
                            Log::info("AgentController::view data=".json_encode( $this->viewData['rs']));
 
       return view('admin.report.update_ma', $this->viewData);
    }


     function updateProfile(Request $request) {
        $fullName = $request->input('fullname');
        $upline_master_agent_id = $request->input('upline_master_agent_id');
        $email = $request->input('email');
        $phone = $request->input('phone_number'); 
        $master_agent_id = $request->input('master_agent_id'); 
        $status = $request->input('status'); 
         
        if ($fullName == '') { $this->notif->addMessage('Full name is required'); }
        if ($email == '') { $this->notif->addMessage('Email address is required'); }
        else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) { $this->notif->addMessage('Invalid email address format'); }
        if ($phone == '') { $this->notif->addMessage('Phone number is required'); }
        if ($master_agent_id == '') { $this->notif->addMessage('Ma is required'); }
          

        if ($this->notif->code == \App\Http\Helpers\NotificationHelper::CODE_OK) {
           

            $someModel = new  MainAgentModel;
            $someModel->setConnection($this->conn); 
            $q = $someModel->where('id', '=', $master_agent_id)
                            ->first();            
            $q->master_agent_parent_id = $upline_master_agent_id; 
            $q->status = $status;             
            $q->updated_at = date('Y-m-d H:i:s');
            $q->save();




            $profileModel = new  AgentProfileModel;
            $profileModel->setConnection($this->conn); 
            $q = $profileModel->where('agent_id', '=', $agent_id)
                            ->first();
             
            $q->fullname = $fullName;
            $q->email = $email;
            $q->phone_number = $phone; 
            $q->updated_at = date('Y-m-d H:i:s');
            $q->save();
        }
        
        return response()->json($this->notif->build());
    }
    
    function exportMasterToCSV(Request $request){
            $min =Input::get('min' ); //$request->get('min').' 00:00:00';
            $max = Input::get('max' );//$request->get('max').' 23:59:59'; //$request->get('max');
            $tableselect = $request->tableselect;
            $search =$request->input('search');

            $timeMin = '00:00:00';
            $timeMax = '23:59:59';

            $result1 = $min . ' ' . $timeMin;
            $result2 = $max . ' ' . $timeMax;

    $someModel = new  MasterAgentProfileModel;
    $someModel->setConnection($this->conn); 
            if(($search != '') && ($min != '' && $max != '')) {         
            $table =   $someModel->whereBetween(
                'created_at', 
                [
                    $result1,
                    $result2
                ]
                )
                ->where($tableselect , $search)
                ->orderBy('created_at' , 'desc')
                ->join('master_agent','master_agent.id','master_agent_id')
                ->selectRaw('master_agent_id ,master_agent_parent_id, profile_type , fullname ,firstname ,middlename, lastname , description ,email , phone_number , contact_number , fax_number, place_birth , birth_date , gender , npwp_number ,province_name , city_name , address , postal_code , department , contact_person ,master_agent.created_at,master_agent.status ')
                ->get();

        }else if($min != '' && $max != ''){
            $table =  $someModel->whereBetween(
                'created_at', 
                [
                    $result1,
                    $result2
                ]
                )
                ->orderBy('created_at' , 'desc')
                ->join('master_agent','master_agent.id','master_agent_id')
                ->selectRaw('master_agent_id ,master_agent_parent_id, profile_type , fullname ,firstname ,middlename, lastname , description ,email , phone_number , contact_number , fax_number, place_birth , birth_date , gender , npwp_number ,province_name , city_name , address , postal_code , department , contact_person ,master_agent.created_at,master_agent.status ')
                ->get();
        }else if(($search != '') && ($min == '' && $max == '')){
             $table =  $someModel->where($tableselect , $search)
                ->orderBy('created_at' , 'desc')
                ->get();      
        }else {
             $table =  $someModel->orderBy('created_at' , 'desc')
            ->join('master_agent','master_agent.id','master_agent_id')
            ->selectRaw('master_agent_id ,master_agent_parent_id, profile_type , fullname ,firstname ,middlename, lastname , description ,email , phone_number , contact_number , fax_number, place_birth , birth_date , gender , npwp_number ,province_name , city_name , address , postal_code , department , contact_person ,master_agent.created_at,master_agent.status ')
            ->get();
        }
            
            $filename = "report.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('master_agent_id' , 'master_agent_parent_id', 'profile_type' , 'fullname' ,'firstname' ,'middlename', 'lastname' , 'description' ,'email' , 'phone_number' , 'contact_number' , 'fax_number', 'place_birth' , 'birth_date' , 'gender' , 'npwp_number' ,'province_name' , 'city_name' , 'address' , 'postal_code' , 'department' , 'contact_person' , 'created_at', 'status' ));

            foreach($table as $row) {
                fputcsv($handle, array($row['master_agent_id'], $row['master_agent_parent_id'], $row['profile_type'], $row['fullname'], $row['firstname'], $row['middlename'], $row['lastname'], $row['description'],$row['email'],$row['phone_number'],$row['contact_number'],$row['fax_number'], $row['place_birth'], $row['birth_date'], $row['gender'], $row['npwp_number'], $row['province_name'] , $row['city_name'], $row['address'], $row['postal_code'], $row['department'] , $row['contact_person'], $row['created_at'], $row['status']));
            }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            return response()->download($filename, 'report.csv', $headers);
    }
}
