<?php

namespace App\Http\Controllers\admin\cms;

use Log;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Core\Util\UtilCommon;

use App\Http\Controllers\BDGBaseController;
use App\Models\PartnerTrxReqModel;
use App\Models\PartnerDepositModel;
use App\Models\LogTrxAgent;
use App\Models\SummaryReconPendingModel;
use App\BusinessLogic\HelpdeskBL;

class RefundController extends BDGBaseController
{
    public function __construct() {
        parent::__construct();
 
        $this->conn = Config::get ( 'webconf.cms.connectionreport' );
        $this->connProd = Config::get ( 'webconf.cms.connectiondata' );
        $this->webapi = Config::get ( 'webconf.cms.api_web' );
        $this->helpdeskBL = new HelpdeskBL;  
    }


    function index() {
        return view('admin.report.index', $this->viewData);
    } 

 function search_test(Request $request){
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

    $min =Input::get('min'); //$request->get('min').' 00:00:00';
    $max = Input::get('max');//$request->get('max').' 23:59:59'; //$request->get('max');
    $tableselect = $request->tableselect;
    $search =$request->input('search');
    $timeMin = '00:00:00';
    $timeMax = '23:59:59';
 

    if($min != '')
    {
        $result1 = $min. ' ' . $timeMin; 
    } 
    else
    {
      $result1 = '';  
    }

    if($max != '')
    {
        $result2 = $max . ' ' . $timeMax;
    } 
    else
    {
      $result2 = '';  
    }

    $fields = [
            ['log_datetime','Log DateTime'],
            ['agent_id', 'Agent Id'],
            ['master_agent_id', 'Master Agent Id'],
            ['product_id', 'Product Id'],
            ['product_code', 'Product Code'],
            ['trx_type' , 'Trx Type'],
            ['trx_bill_number' , 'Trx Bill Number'],
            ['trx_result_code','Trx Result Code'],
            ['trx_result_desc','Trx Result Desc'],
            ['trx_retrieval','Trx Retrieval'],
            ['trx_payment_code','Trx Payment Code'],
            ['trx_signature','Trx Signature'],
            ['trx_info1','Trx Info 1'],
            ['trx_info2','Trx Info 2'],
            ['trx_info3','Trx Info 3'],
            ['trx_sheet_number','Trx Sheet Number'],
            ['api_error_code','Api Error Code'],
            ['api_error_message','Api Error Message'],
 
            ['is_pending','Pending'],
            ['advice_retry','Adv Retry'],
            ['advice_datetime','Adv Last Attempt'],
            ['advice_result_code','Adv Code'],
            ['advice_result_desc','Adv Desc'],

            //['app_version', 'App Version'],
            ['created_at','Created At'],
            ['id','Id Trx'],
            ['product_billnumber_paymentcode','Product Trx'],
            ['trx_internal_id','Trx Internal ID']
            // ,
            // ['app_user_agent','App User Agent'],
            // 

        ]; 
        //,$min,$max,$tableselect,$search)
        $this->tableSorter->setupSorter(url('/agency/refund_info.html'), $fields, $searchFor, $sf, $sm, $min, $max, $tableselect, $search);
        if(($search != '') && ($min != '' && $max != '')) {         
             $someModel = new LogTrxAgent;
            $someModel->setConnection($this->connProd); 
            $q = $someModel->whereBetween(
                'created_at', 
                [
                    $result1,
                    $result2
                ]
                )
                ->where($tableselect , $search)              
                ->orderBy('created_at' , 'desc')                
            ->where('is_pending','=','Y')
            ->selectRaw("id, log_datetime , agent_id ,master_agent_id, product_id , product_code , trx_type , trx_bill_number, trx_result_code , trx_result_desc,trx_internal_id, trx_retrieval,  trx_payment_code , trx_signature ,trx_info1 , trx_info2 , trx_info3 ,trx_sheet_number , api_error_code , api_error_message ,is_pending,advice_retry,advice_datetime ,advice_result_code,advice_result_desc,  app_platform, app_version, app_user_agent, created_at,concat(product_id,'#',trx_bill_number,'#',trx_payment_code,'#',trx_price_nominal) as product_billnumber_paymentcode ");

        }else if($min != '' && $max != ''){
             $someModel = new LogTrxAgent;
            $someModel->setConnection($this->connProd); 
            $q = $someModel->whereBetween(
                'created_at', 
                [
                    $result1,
                    $result2
                ]
                )              
                ->orderBy('created_at' , 'desc')                
            ->where('is_pending','=','Y')
            ->selectRaw("id, log_datetime , agent_id ,master_agent_id, product_id , product_code , trx_type , trx_bill_number, trx_result_code , trx_result_desc,trx_internal_id, trx_retrieval,  trx_payment_code , trx_signature ,trx_info1 , trx_info2 , trx_info3 ,trx_sheet_number , api_error_code , api_error_message ,is_pending,advice_retry,advice_datetime ,advice_result_code,advice_result_desc,  app_platform, app_version, app_user_agent, created_at,concat(product_id,'#',trx_bill_number,'#',trx_payment_code,'#',trx_price_nominal) as product_billnumber_paymentcode ");
        }else if(($search != '' && $tableselect != '') && ($min == '' && $max == '')){

            $someModel = new LogTrxAgent;
            $someModel->setConnection($this->connProd); 
            $q = $someModel->where($tableselect , $search)              
                ->orderBy('created_at' , 'desc')                
            ->where('is_pending','=','Y')
            ->selectRaw("id, log_datetime , agent_id ,master_agent_id, product_id , product_code , trx_type , trx_bill_number, trx_result_code , trx_result_desc,trx_internal_id, trx_retrieval,  trx_payment_code , trx_signature ,trx_info1 , trx_info2 , trx_info3 ,trx_sheet_number , api_error_code , api_error_message ,is_pending,advice_retry,advice_datetime ,advice_result_code,advice_result_desc,  app_platform, app_version, app_user_agent, created_at,concat(product_id,'#',trx_bill_number,'#',trx_payment_code,'#',trx_price_nominal) as product_billnumber_paymentcode ");    
        }else {
            $someModel = new LogTrxAgent;
            $someModel->setConnection($this->connProd); 
            $q = $someModel->orderBy('created_at' , 'desc')                
            ->where('is_pending','=','Y')
            ->selectRaw("id, log_datetime , agent_id ,master_agent_id, product_id , product_code , trx_type , trx_bill_number, trx_result_code , trx_result_desc,trx_internal_id, trx_retrieval,  trx_payment_code , trx_signature ,trx_info1 , trx_info2 , trx_info3 ,trx_sheet_number , api_error_code , api_error_message ,is_pending,advice_retry,advice_datetime ,advice_result_code,advice_result_desc,  app_platform, app_version, app_user_agent, created_at,concat(product_id,'#',trx_bill_number,'#',trx_payment_code,'#',trx_price_nominal) as product_billnumber_paymentcode ");
        }
  
        $this->tableSorter->setupPaging($q, $pg);

        $this->viewData['sorter'] = $this->tableSorter;
        $this->viewData['pg'] = $pg;
        $this->viewData['searchFor'] = $searchFor;
        $this->viewData['min'] = $result1;
        $this->viewData['max'] = $result2;
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
              Log::info("RefundController::getlist");

        return view('admin.report.pending_to_success', $this->viewData );
 } 


 function forcePendingtoSuccess(Request $request)
{ 
 $s = "";
try{
 
    $id_trx = $request->input('id');
    $detail = $this->helpdeskBL->getdetailLog($id_trx);

    $url= $this->webapi."pending-to-successful";
  
  $result = $this->helpdeskBL->requestPost($url,$detail);
      // Log::info("RefundController::getdetailLog data=".json_encode($result));
 

} catch ( Exception $e ) {
        Log::info("RefundController::forcePendingtoSuccess exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
} 
return $result;
}

 function refundPending(Request $request)
{ 
 $s = "";
try{
 
    $id_trx = $request->input('id');
    $detail = $this->helpdeskBL->getdetailLog($id_trx);

    $url=$this->webapi."refund"; 
  
  $result =  $this->helpdeskBL->requestPost($url,$detail);
      Log::info("RefundController::refundPending data=".json_encode($result));
   $hasil = json_decode($result);
   $res['status'] =  $hasil->status;
    $res['code'] =  $hasil->error->code;
    $res['message'] =  $hasil->error->message;
 

} catch ( Exception $e ) {
        Log::info("RefundController::refundPending exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
} 
return $res;
}

function logTrx(Request $request){
    $someModel = new LogTrxAgent;
    $someModel->setConnection($this->connProd); 
    $something = $someModel->where('is_pending','=','Y')->get();

    return $something;
}


    function exportToCSV(Request $request){
            $min =Input::get('min' ); //$request->get('min').' 00:00:00';
            $max = Input::get('max' );//$request->get('max').' 23:59:59'; //$request->get('max');
            $tableselect = $request->tableselect;
            $search =$request->input('search');

            $timeMin = '00:00:00';
            $timeMax = '23:59:59';

            $result1 = $min . ' ' . $timeMin;
            $result2 = $max . ' ' . $timeMax;

            if(($search != '') && ($min != '' && $max != '')) {         
            $table = PartnerTrxReqModel::whereBetween(
                'created_at', 
                [
                    $result1,
                    $result2
                ]
                )
                ->where($tableselect , $search)
                ->orderBy('created_at' , 'desc')
                ->get();

        }else if($min != '' && $max != ''){
            $table = PartnerTrxReqModel::whereBetween(
                'created_at', 
                [
                    $result1,
                    $result2
                ]
                )
                ->orderBy('created_at' , 'desc')
                ->get();
        }else if(($search != '') && ($min == '' && $max == '')){
            $table = PartnerTrxReqModel::where($tableselect , $search)
                ->orderBy('created_at' , 'desc')
                ->get();      
        }else {
            $table = PartnerTrxReqModel::orderBy('created_at' , 'desc')
            ->get();
        }
                // $table =PartnerTrxReqModel::whereBetween(
                // 'created_at', 
                // [
                //     $result1,
                //     $result2
                // ]   
                // )
                //  ->where('partner_id' , '=' , $this->session->get('partner_id'))
                //  ->where($tableselect , $search)
                //  ->orderBy('created_at' , 'desc')
                //  ->get();
            // }
            
            $filename = "report.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('Log DateTime' , 'Agent Id', 'Master Agent Id', 'Product Id' , 'Product Code' ,'Transaction Type' ,'Transaction Bill Number' , 'Transaction Result Code' , 'Transaction Result Desc' , 'Transaction Retrieval' , 'Transaction Payment Code' , 'Transaction Signature' , 'Transaction Info 1' , 'Transaction Info 2' , 'Transaction Info 3' , 'Transaction Sheet Number' , 'Api Error Code' ,'API error Message' ,'App Platform' , 'App Version' , 'App User Agent' ,'Created At'));

            foreach($table as $row) {
                fputcsv($handle, array($row['log_datetime'], $row['agent_id'], $row['master_agent_id'], $row['product_id'], $row['product_code'], $row['trx_type'], $row['trx_bill_number'], $row['trx_result_code'], $row['trx_result_desc'], $row['trx_retrieval'], $row['trx_payment_code'], $row['trx_signature'], $row['trx_info1'], $row['trx_info2'], $row['trx_info3'], $row['trx_sheet_number'] , $row['api_error_code'] ,$row['api_error_message'], $row['app_platform'], $row['app_version'], $row['app_user_agent'], $row['created_at']));
            }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            return response()->download($filename, 'report.csv', $headers);
    }
}
