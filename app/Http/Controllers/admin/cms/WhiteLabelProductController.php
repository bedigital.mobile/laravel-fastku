<?php

namespace App\Http\Controllers\admin\cms;

use Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\BDGBaseController;
use App\Models\ServiceModel;
use App\Models\ProductModel;
use App\Models\admin\ProductBreakupModel;
use App\Models\admin\BillerModel;
use App\Models\admin\WhiteLabelBillerModel;
use App\Models\admin\WhiteLabelServiceModel;
use App\Models\WhiteLabelProductModel;
use App\Models\admin\WhiteLabelProductBreakUpModel;

class WhiteLabelProductController extends BDGBaseController
{   
    function index($serviceId) {
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', 0);
        $sm = Input::get('sm', 0);
        
        $fields = [
            ['product.product_name', 'Product Name'],
            ['white_label_products.product_name_alias', 'Alias'],
            ['is_active', 'Status']
        ];
        
        $this->tableSorter->setupSorter(url('/wlservice_sub_'.$serviceId.'_product.html'), $fields, $searchFor, $sf, $sm);
        
        if($searchFor != '') {
            $q = WhiteLabelServiceModel::where('white_label_services.service_id', '=', $serviceId)
                    ->where('white_label_services.white_label_id', '=', $this->session->whiteLabelId())
                    ->where(function ($query) use ($searchFor) {
                        $query->where('product.product_name', 'like', '%'.$searchFor.'%')
                                ->orWhere('white_label_products.product_name_alias', 'like', '%'.$searchFor.'%');
                    })
                    ->join('service', 'white_label_services.service_id', '=', 'service.id')
                    ->join('service_products', 'service.id', '=', 'service_products.service_id')
                    ->join('product', 'service_products.product_id', '=', 'product.id')
                    ->join('white_label_products', function($join) {
                        $join->on('product.id', '=', 'white_label_products.product_id');
                        $join->on('white_label_services.white_label_id', '=', 'white_label_products.white_label_id');
                    })
                    ->selectRaw('service.active as service_active, service_products.active as service_product_active')
                    ->selectRaw('product.id as product_id, product.product_name, product.active as product_active')
                    ->selectRaw('white_label_products.product_name_alias, white_label_products.show_order as wlp_order, white_label_products.active as wlp_active')
                    ->selectRaw('if(white_label_products.active = \'N\', 0, if(product.active = \'N\', 0, if(service_products.active = \'N\', 0, if(service.active = \'N\', 0, 1)))) as is_active');
        } else {
            $q = WhiteLabelServiceModel::where('white_label_services.service_id', '=', $serviceId)
                    ->where('white_label_services.white_label_id', '=', $this->session->whiteLabelId())
                    ->join('service', 'white_label_services.service_id', '=', 'service.id')
                    ->join('service_products', 'service.id', '=', 'service_products.service_id')
                    ->join('product', 'service_products.product_id', '=', 'product.id')
                    ->join('white_label_products', function($join) {
                        $join->on('product.id', '=', 'white_label_products.product_id');
                        $join->on('white_label_services.white_label_id', '=', 'white_label_products.white_label_id');
                    })
                    ->selectRaw('service.active as service_active, service_products.active as service_product_active')
                    ->selectRaw('product.id as product_id, product.product_name, product.active as product_active')
                    ->selectRaw('white_label_products.product_name_alias, white_label_products.show_order as wlp_order, white_label_products.active as wlp_active')
                    ->selectRaw('if(white_label_products.active = \'N\', 0, if(product.active = \'N\', 0, if(service_products.active = \'N\', 0, if(service.active = \'N\', 0, 1)))) as is_active');
        }
        
        $this->tableSorter->setupPaging($q, $pg);
        
        $this->viewData['sorter'] = $this->tableSorter;
        $this->viewData['serviceId'] = $serviceId;
        
        return view('white_label_product.index', $this->viewData);
    } 

    function indexhome($serviceId) {
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', 0);
        $sm = Input::get('sm', 0);
        
        $fields = [
            ['product.product_name', 'Product Name'],
            ['white_label_products.product_name_alias', 'Alias'],
            ['is_active', 'Status']
        ];
         $q = WhiteLabelServiceModel::where('white_label_services.service_id', '=', $serviceId)
                    ->where('white_label_services.white_label_id', '=', $this->session->whiteLabelId())
                    ->join('service', 'white_label_services.service_id', '=', 'service.id')
                    ->join('service_products', 'service.id', '=', 'service_products.service_id')
                    ->join('product', 'service_products.product_id', '=', 'product.id')
                    ->join('white_label_products', function($join) {
                        $join->on('product.id', '=', 'white_label_products.product_id');
                        $join->on('white_label_services.white_label_id', '=', 'white_label_products.white_label_id');
                    })
                    ->selectRaw('service.active as service_active, service_products.active as service_product_active')
                    ->selectRaw('product.id as product_id, product.product_name, product.active as product_active')
                    ->selectRaw('white_label_products.product_name_alias, white_label_products.show_order as wlp_order, white_label_products.active as wlp_active')
                    ->selectRaw('if(white_label_products.active = \'N\', 0, if(product.active = \'N\', 0, if(service_products.active = \'N\', 0, if(service.active = \'N\', 0, 1)))) as is_active')->get();
          
        
        $this->viewData['listproduct'] =$q;
        $this->viewData['serviceId'] = $serviceId;
        Log::info("WhiteLabelProductController::index data=".json_encode($this->viewData));  
        return view('white_label_product.listproductbyservice', $this->viewData);
    }
    function allproduct() { 
		
		$fields = [
            ['product.product_name', 'Product Name'],
			['white_label_products.product_name_alias', 'Alias'],
            ['is_active', 'Status']
		];
		 $q = WhiteLabelServiceModel::where('white_label_services.white_label_id', '=', $this->session->whiteLabelId())
                    ->join('service', 'white_label_services.service_id', '=', 'service.id')
                    ->join('service_products', 'service.id', '=', 'service_products.service_id')
                    ->join('product', 'service_products.product_id', '=', 'product.id')
                    ->join('white_label_products', function($join) {
                        $join->on('product.id', '=', 'white_label_products.product_id');
                        $join->on('white_label_services.white_label_id', '=', 'white_label_products.white_label_id');
                    })
                    ->selectRaw('service.active as service_active, service_name, service_products.active as service_product_active')
                    ->selectRaw('product.id as product_id, product.product_name, product.active as product_active')
                    ->selectRaw('white_label_products.product_name_alias, white_label_products.show_order as wlp_order, white_label_products.active as wlp_active')
                    ->selectRaw('if(white_label_products.active = \'N\', 0, if(product.active = \'N\', 0, if(service_products.active = \'N\', 0, if(service.active = \'N\', 0, 1)))) as is_active')->get();
		  
        
        $this->viewData['listproduct'] =$q;
         Log::info("WhiteLabelProductController::index data=".json_encode($this->viewData));  
        return view('white_label_product.listproduct', $this->viewData);
    }
    
    function create($serviceId, $productId) {
        $this->viewData['serviceId'] = $serviceId;
        $this->viewData['productId'] = $productId;
        
        if ($productId > 0) {
            $this->viewData['rs'] = ProductModel::where('product.id', '=', $productId)
                                        ->first();
            
            if ($this->viewData['rs']->has_breakup == 'Y') {
                $this->viewData['rsbu'] = ServiceModel::where('product.id', '=', $productId)
                                            ->where('white_label_services.white_label_id', '=', $this->session->whiteLabelId())
                                            ->join('service_products', 'service.id', '=', 'service_products.service_id')
                                            ->leftJoin('white_label_services', 'service_products.service_id', '=', 'white_label_services.service_id')
                                            ->join('product', 'service_products.product_id', '=', 'product.id')
                                            ->join('product_breakup', 'product.id', '=', 'product_breakup.product_id')
                                            ->leftJoin('white_label_products_breakup', function($join) {
                                                $join->on('product_breakup.product_id', '=', 'white_label_products_breakup.product_id');
                                                $join->on('product_breakup.product_code', '=', 'white_label_products_breakup.product_code');
                                            })
                                            ->selectRaw('product.id as product_id, product.product_name, product.active as product_active')
                                            ->selectRaw('product_breakup.product_code, product_breakup.active as product_breakup_active')
                                            ->get();
            }        
        } else {
            $this->viewData['rs'] = new \stdClass();
            $this->viewData['rs']->has_breakup = 'N';
        }
        
        $this->viewData['rsserv'] = ServiceModel::where('service.id', '=', $serviceId)
                                        ->join('service_products', 'service.id', '=', 'service_products.service_id')
                                        ->leftJoin('white_label_services', 'service_products.service_id', '=', 'white_label_services.service_id')
                                        ->selectRaw('service.id as service_id, ifnull(white_label_services.service_name_alias, service.service_name) as service_name')
                                        ->first();
        
        $this->viewData['rsprod'] = ProductModel::where('service.id', '=', $serviceId)
                                    ->where('service.active', '=', 'Y')
                                    ->where('service_products.active', '=', 'Y')
                                    ->where('product.active', '=', 'Y')
                                    ->whereRaw('product.id not in (select product_id from white_label_products where white_label_id = '.$this->session->whiteLabelId().')')
                                    ->join('service_products', 'product.id', '=', 'service_products.product_id')
                                    ->join('service', 'service_products.service_id', '=', 'service.id')
                                    ->selectRaw('product.id as product_id, product.product_name')
                                    ->get();
        
        $this->viewData['rsbill'] = WhiteLabelBillerModel::where('white_label_biller_aggregators.active', '=', 'Y')
                                        ->join('biller_aggregator', 'white_label_biller_aggregators.biller_aggregator_id', '=', 'biller_aggregator.id')
                                        ->orderBy('white_label_biller_aggregators.priority', 'asc')
                                        ->selectRaw('biller_aggregator.id as biller_id, biller_aggregator.aggregator_name')
                                        ->get();
        
        return view('white_label_product.new', $this->viewData);
    }
    
    function store(Request $request) {
        $wlService = $request->input('wlservice');
        $biller = $request->input('biller');
        $product = $request->input('product');
        $alias = $request->input('alias');
        $currency = $request->input('currency', 'IDR');
        $feeType = $request->input('fee-type');
        $feeVal = $request->input('fee');
        $discountType = $request->input('discount-type');
        $discountVal = $request->input('discount');
        $hasBreakup = $request->input('has-breakup', 'N');
        $buCount = $request->input('bu-count', 0);
        
        if ($wlService == '') { $this->notif->addMessage('Service is required'); }
        if ($product == '') { $this->notif->addMessage('Product is required'); }
        else {
            $q = WhiteLabelProductModel::where('white_label_id', '=', $this->session->whiteLabelId())
                    ->where('product_id', '=', $product)
                    ->first();
            if ($q != null) {
                $this->notif->addMessage('Product exists. Please choose another');
            }
        }
        if ($hasBreakup != 'Y') {
            if ($currency == '') { $this->notif->addMessage('Currency code is required'); }
            if ($feeVal == '' || !is_numeric($feeVal) || $feeVal < 0) { $this->notif->addMessage('Trx fee value must be a number and greater than or equal zero'); }
            if ($discountVal == '' || !is_numeric($discountVal) || $discountVal < 0) { $this->notif->addMessage('Trx fee value must be a number and greater than or equal zero'); }
        }
        
        if ($this->notif->isOK() == true) {
            try {
                DB::beginTransaction();
                
                $q = new WhiteLabelProductModel();
                $q->white_label_id = $this->session->whiteLabelId();
                $q->product_id = $product;
                $q->product_name_alias = $hasBreakup == 0 ? $alias : null;
//                $q->currency_code = $hasBreakup == 0 ? $currency : 'IDR';
                $q->trx_fee_type = $hasBreakup == 0 ? $feeType : WhiteLabelProductModel::TRX_FEE_TYPE_FIXED;
                $q->trx_fee_value = $hasBreakup == 0 ? $feeVal : 0;
                $q->discount_type = $hasBreakup == 0 ? $discountType : WhiteLabelProductModel::DISCOUNT_TYPE_FIXED;
                $q->discount_value = $hasBreakup == 0 ? $discountVal : 0;
                $q->biller_aggregator_id = $biller;
                $q->active = 'Y';
                $q->created_at = date('Y-m-d H:i:s');
                $q->updated_at = date('Y-m-d H:i:s');
                $q->save();
                
                if ($hasBreakup == 'Y' && $buCount > 0) {
                    for ($i=0; $i<$buCount; $i++) {
                        $q = new WhiteLabelProductBreakUpModel();
                        $q->white_label_id = $this->session->whiteLabelId();
                        $q->product_id = $product;
                        $q->product_code = $request->input('bu-code-'.$i, null);
                        $q->product_name_alias = $request->input('bu-alias-'.$i, null);
//                        $q->currency_code = $request->input('bu-currency-'.$i, 'IDR');
                        $q->trx_fee_type = $request->input('bu-fee-type-'.$i, null);
                        $q->trx_fee_value = $request->input('bu-fee-'.$i, null);
                        $q->discount_type = $request->input('bu-discount-type-'.$i, null);
                        $q->discount_value = $request->input('bu-discount-'.$i, null);
                        $q->active = 'Y';
                        $q->created_at = date('Y-m-d H:i:s');
                        $q->updated_at = date('Y-m-d H:i:s');
                        $q->save();
                    }
                }
                
                DB::commit();
                
            } catch (Exception $ex) {
                DB::rollBack();
                $this->notif->addMessage($ex->getMessage());
            }
        }
        
        return response()->json($this->notif->build());
    }
    
    function edit($serviceId, $productId) {
        $this->viewData['serviceId'] = $serviceId;
        $this->viewData['productId'] = $productId;
        
        $this->viewData['rs'] = WhiteLabelServiceModel::where('white_label_services.service_id', '=', $serviceId)
                                    ->where('white_label_products.product_id', '=', $productId)
                                    ->where('white_label_services.white_label_id', '=', $this->session->whiteLabelId())
                                    ->join('service', 'white_label_services.service_id', '=', 'service.id')
                                    ->join('service_products', 'service.id', '=', 'service_products.service_id')
                                    ->join('product', 'service_products.product_id', '=', 'product.id')
                                    ->join('white_label_products', function($join) {
                                        $join->on('product.id', '=', 'white_label_products.product_id');
                                        $join->on('white_label_services.white_label_id', '=', 'white_label_products.white_label_id');
                                    })
                                    ->leftJoin('white_label_biller_aggregators', function($join) {
                                        $join->on('white_label_products.biller_aggregator_id', '=', 'white_label_biller_aggregators.biller_aggregator_id');
                                        $join->on('white_label_products.white_label_id', '=', 'white_label_biller_aggregators.white_label_id');
                                    })
                                    ->leftJoin('biller_aggregator', 'white_label_products.biller_aggregator_id', '=', 'biller_aggregator.id')
                                    ->selectRaw('service.active, service.service_name, service_products.active')
                                    ->selectRaw('product.id as product_id, product.product_name, product.active, product.has_breakup')
                                    ->selectRaw('white_label_products.id as wlp_id, white_label_products.product_name_alias, white_label_products.logo_filename_alias, white_label_products.currency_code')
                                    ->selectRaw('white_label_products.trx_fee_type, white_label_products.trx_fee_value, white_label_products.discount_type, white_label_products.discount_value')
                                    ->selectRaw('if(white_label_products.active = \'N\', 0, if(product.active = \'N\', 0, if(service_products.active = \'N\', 0, if(service.active = \'N\', 0, 1)))) as is_active')
                                    ->selectRaw('biller_aggregator.aggregator_name')
                                    ->first();
        
        if ($this->viewData['rs']) {
            if ($this->viewData['rs']->has_breakup == 'Y') {
                $this->viewData['rsbu'] = ProductBreakUpModel::where('product_breakup.product_id', '=', $this->viewData['rs']->product_id)
                                            ->where('white_label_products_breakup.white_label_id', '=', $this->session->whiteLabelId())
                                            ->join('white_label_products_breakup', function($join) {
                                                $join->on('product_breakup.product_id', '=', 'white_label_products_breakup.product_id');
                                                $join->on('product_breakup.product_code', '=', 'white_label_products_breakup.product_code');
                                            })
                                            ->orderBy('display_order', 'asc')
                                            ->selectRaw('product_breakup.product_code as prod_code, product_breakup.product_name')
                                            ->selectRaw('white_label_products_breakup.id as wlpb_id, white_label_products_breakup.product_name_alias')
                                            ->selectRaw('white_label_products_breakup.trx_fee_type, white_label_products_breakup.trx_fee_value')
                                            ->selectRaw('white_label_products_breakup.discount_type, white_label_products_breakup.discount_value')
                                            ->selectRaw('white_label_products_breakup.active')
                                            ->selectRaw('if(white_label_products_breakup.active = \'N\', 0, if(product_breakup.active = \'N\', 0, 1)) as is_active')
                                            ->selectRaw('if(product_breakup.show_order > white_label_products_breakup.show_order, product_breakup.show_order, white_label_products_breakup.show_order) display_order')
                                            ->get();
            }

            return view('white_label_product.edit', $this->viewData);
        } else {
            return response('', 404);
        }
    }
    
    function update(Request $request) {
        $id = $request->input('id');
        $service = $request->input('service');
        $avatar = $request->input('avatar-file');
        $biller = $request->input('biller');
        $product = $request->input('product-id');
        $alias = $request->input('alias');
        $currency = $request->input('currency', 'IDR');
        $feeType = $request->input('fee-type');
        $feeVal = $request->input('fee');
        $discountType = $request->input('discount-type');
        $discountVal = $request->input('discount');
        $status = $request->input('status', 'N');
        $hasBreakup = $request->input('has-breakup', 'N');
        $buCount = $request->input('bu-count', 0);
        
        if ($hasBreakup != 'Y') {
            if ($currency == '') { $this->notif->addMessage('Currency code is required'); }
            if ($feeVal == '' || !is_numeric($feeVal) || $feeVal < 0) { $this->notif->addMessage('Trx fee value must be a number and greater than or equal zero'); }
            if ($discountVal == '' || !is_numeric($discountVal) || $discountVal < 0) { $this->notif->addMessage('Trx fee value must be a number and greater than or equal zero'); }
        }
        
        if ($this->notif->isOK() == true) {
            try {
                DB::beginTransaction();
                
                $q = WhiteLabelProductModel::where('white_label_id', '=', $this->session->whiteLabelId())
                        ->where('id', '=', $id)
                        ->first();
                $q->logo_filename_alias = $avatar;
                $q->product_name_alias = $alias;
                $q->currency_code = $currency;
                $q->trx_fee_type = $feeType;
                $q->trx_fee_value = $feeVal;
                $q->discount_type = $discountType;
                $q->discount_value = $discountVal;
                $q->updated_at = date('Y-m-d H:i:s');
                $q->save();
                
                if ($hasBreakup == 'Y' && $buCount > 0) {
                    for ($i=0; $i<$buCount; $i++) {
                        $id = $request->input('bu-id-'.$i, 0);
                        $q = WhiteLabelProductBreakUpModel::find($id);
                        $q->product_name_alias = $request->input('bu-alias-'.$i, null);
                        $q->trx_fee_type = $request->input('bu-fee-type-'.$i, null);
                        $q->trx_fee_value = $request->input('bu-fee-'.$i, null);
                        $q->discount_type = $request->input('bu-discount-type-'.$i, null);
                        $q->discount_value = $request->input('bu-discount-'.$i, null);
                        $q->updated_at = date('Y-m-d H:i:s');
                        $q->save();
                    }
                }
                
                DB::commit();
                
            } catch (Exception $ex) {
                DB::rollBack();
                $this->notif->addMessage($ex->getMessage());
            }
        }
        
        return response()->json($this->notif->build());
    }

    public function productService()
    {
        $q = ProductModel::selectRaw('concat(id , "") as prodid, product_name')
                ->orderBy('id')
                ->get();
        
        $viewData = ['response' => '' , 'products' => $q ];
        
        return view('productService', $viewData);
    }
}
