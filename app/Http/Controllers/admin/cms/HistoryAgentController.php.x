<?php

namespace App\Http\Controllers\admin\cms;

use Log;
use Config;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\BusinessLogic\HelpdeskBL;
use App\Http\Controllers\BDGBaseController;
use App\Models\PartnerDepositAccountModel;
use App\Models\PartnerActivityLogModel;
use App\Models\LogPaymentGatewayModel;
use App\Models\HistoryTrxMasterAgent;
use App\Models\MainAgentModel;
use App\Models\admin\AgentModel;
use App\Core\Util\UtilCommon;
use App\Models\DynamicDuoTables;

class HistoryAgentController extends BDGBaseController
{



     public function __construct() { 
        parent::__construct();     
        $this->conn = Config::get ( 'webconf.cms.connection111' );
        $this->helpdesk = new HelpdeskBL; 

        $this->Util = new  UtilCommon;
        $this->PartnerDepositAccountModel = new  PartnerDepositAccountModel;
        $this->PartnerDepositAccountModel->setConnection($this->conn); 


         $this->HistoryTrxMasterAgent = new  HistoryTrxMasterAgent;
        $this->HistoryTrxMasterAgent->setConnection($this->conn); 




    }
    



    function index() {
        return view('admin.report.index', $this->viewData);
    }

    function history(){

    	$deposit = PartnerDepositAccountModel::get();

    		$this->viewData['history'] = $deposit;
  
    		return view('admin.report.deposit_history', $this->viewData );

    }

function search_history(Request $request) {
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

    $tableselect = $request->tableselect;
    $search =$request->input('search');

    $tableselect2 = $request->tableselect2;
    $search2 =$request->input('search2');
 
    $opsidates =$request->input('opsidates');

    $fromdates =$request->input('fromdates');
    $todates =$request->input('todates');

    $mnt =$request->input('mnt');
    $yr =$request->input('yr');

    //Week variable is added by amin @2019-10-10 
    $week= Input::get('week',1);

    $thismonth = date("Y_m"); 
    $this_month_table =  "befintechlog.history_trx_agent_".$thismonth;
    $this->LogTrxthisMonth = new DynamicDuoTables; 
    $this->LogTrxthisMonth->setConnection($this->conn); 
    $this->LogTrxthisMonth->setTable($this_month_table);

    $daterange = 0;
    $listfrom=[];
 
    $this->listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id')); 
    $this->listMasterAgent = $this->helpdesk->listMabyUpline($this->session->get('master_agent_id')); 
 
    $fields = [
            ['agent_id', 'Agent Id'],
            ['username', 'Username'],
            ['trx_datetime', 'Trx DateTime'],
            ['trx_mode', 'Trx Mode'],
            ['trx_type', 'Trx Type'],
            ['trx_code', 'Trx Code'],
            ['trx_bill_number', 'Trx Bill Number'],
            ['trx_product_code', 'Trx Product Code'],
            ['trx_product_id', 'Trx Product Id'],
            ['description' , 'Description'],
            ['amount' , 'Amount'],
            ['balance_value_before' , 'Balance Before'],
            ['balance_value_after' , 'Balance After'],
            ['info_printable' , 'App Info'],
            ['trx_sheet_number' , 'Trx Sheet Number'],
            ['created_at', 'Created At']
    ]; 
 
    $this->tableSorter->setupSorter(
        secure_url('/ma/history_agent.html'), 
        $fields, 
        $searchFor, 
        $mnt, 
        $yr,
        $fromdates,
        $todates,
        $tableselect,
        $search,
        $daterange,
        $tableselect2,
        $search2,
        $opsidates
    );
    
    if(($opsidates == 'month')) { //M.001
        $monthcurrent = $mnt+1 ; 
        $usagemonth = $yr.'_'.str_pad($monthcurrent,2,"0",STR_PAD_LEFT); 
        $this_month_table =  "befintechlog.history_trx_agent_".$usagemonth;
        $logtrxtable =  "log_trx_agent_".$usagemonth;
        $this->LogTrxMonth = new DynamicDuoTables; 
        $this->LogTrxMonth->setConnection($this->conn); 
        $this->LogTrxMonth->setTable($this_month_table);

        $vMonth= str_pad($monthcurrent,2,"0",STR_PAD_LEFT);

        $filterDateStart = '';
        $filterDateEnd = '';

        switch ($week) {
            case 1:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'01 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'07 23:59:59'.'\'';    
                break;
            case 2:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'08 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'15 23:59:59'.'\'';    
                break;
            case 3:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'16 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'23 23:59:59'.'\'';    
                break;
            case 4:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'24 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'31 23:59:59'.'\'';    
                break;
        }
        Log::info('HistoryAgentController->search_history: Selected group of day='.$week);
        Log::info('HistoryAgentController->search_history: filter date='.$filterDateStart.' and '.$filterDateEnd);

        //Get minID and maxID
        $rsMinMaxID =  $this->LogTrxMonth
            ->selectRaw('min(id) as min_id, max(id) as max_id')
            ->whereRaw($this_month_table.'.trx_datetime between '.$filterDateStart.' and '.$filterDateEnd)
            ->first();
        $minID = 0;
        $maxID = 0;    
        if (! is_null($rsMinMaxID) ) {
            $minID = $rsMinMaxID->min_id;
            $maxID = $rsMinMaxID->max_id;
        }
        Log::info('HistoryAgentController->search_history : min ID = '.$minID);
        Log::info('HistoryAgentController->search_history : max ID = '.$maxID);    

        switch ($tableselect) { //M.001.001
            case 'id':
                $tablename=$this_month_table.".agent_id";
                 $searchname = $this->Util->base36ToInt( $search );
                break;
            case 'mode':
                $tablename=$this_month_table.".trx_mode";
                $searchname = $search;
                break; 
            case 'username':
                $tablename="username";$searchname = $search;
                break;
            case 'type':
                $tablename=$this_month_table.".trx_type";$searchname = $search;
                break; 
            case 'product_id':
                $tablename=$this_month_table.".trx_product_id";$searchname = $search;
                break; 
            case 'product_code':
                $tablename=$this_month_table.".trx_product_code";$searchname = $search;
                break; 
            case 'bill_no':
                $tablename=$this_month_table.".trx_bill_number";$searchname = $search;
                break; 
            case 'trx_code':
                $tablename=$this_month_table.".trx_code";$searchname = $search;
                break; 
            default:
                $tablename="";$searchname = $search;
            break;
        } //M.001.001*

        switch ($tableselect2) { //M.001.002
             case 'id':
                $tablename2=$this_month_table.".agent_id";
                 $searchname2 = $this->Util->base36ToInt( $search2 );
                break;
            case 'mode':
                $tablename2=$this_month_table.".trx_mode";
                $searchname2 = $search2;
                break; 
            case 'username':
                $tablename2="username";
                $searchname2 = $search2;
                break;
            case 'type':
                $tablename2=$this_month_table.".trx_type";
                $searchname2 = $search2;
                break;
            case 'product_id':
                $tablename2=$this_month_table.".trx_product_id";
                $searchname2 = $search2;
                break;
            case 'product_code':
                $tablename2=$this_month_table.".trx_product_code";
                $searchname2 = $search2;
                break;
            case 'bill_no':
                $tablename2=$this_month_table.".trx_bill_number";
                $searchname2 = $search2;
                break;
            case 'trx_code':
                $tablename2=$this_month_table.".trx_code";
                $searchname2 = $search2;
                break;
            default:
                $tablename2="";
                $searchname2 = $search2; 
            break;
        } //M.001.002*

        if(($search == '') && ($search2 == '') ) { //M.001.003
            log::info("query month 1 none search");
            log::info("  history table=".$this_month_table); 
            log::info("  log table=".$logtrxtable); 
            log::info(" data MA=".json_encode($this->listMasterAgent)); 
           
            $q = $this->LogTrxMonth  
                ->Join('agent','agent.id', '=',  $this_month_table.'.agent_id')             
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id')
                ->where($this_month_table.'.id','>=',$minID)
                ->where($this_month_table.'.id','<=',$maxID)
                ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)
                ->orderBy($this_month_table.'.id' , 'asc') // change order from trx_datetime to id 
                ->selectRaw
                (
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id ,'.
                    $this_month_table.'.trx_datetime ,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id , '.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                );
         } //M.001.003*
         else if(($search != '') && ($search2 == '') ) { //M.001.003*.001(else)
            log::info("query month 2 search =".$search);
            $q = $this->LogTrxMonth->Join('agent','agent.id', '=',  $this_month_table.'.agent_id')   
                ->where($this_month_table.'.id','>=',$minID)
                ->where($this_month_table.'.id','<=',$maxID)
                ->whereIn('agent.master_agent_id',  $this->listMasterAgent)
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id')
                ->orderBy($this_month_table.'.id' , 'desc')
                ->where
                (
                    function ($query) use ($tablename,$searchname) 
                    { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    }
                )
                ->selectRaw
                (
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id,'.
                    $this_month_table.'.trx_datetime,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id,'.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after ,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                );
            log::info("query month tablename2 =".$tablename2." and ".$searchname2);
        } //M.001.003*.001(else)* 
        else if(($search == '') && ($search2 != '') ) { //M.001.003*.001(else)*.001(else)
            log::info("query month 3 search2 =".$search2);
            $q = $this->LogTrxMonth                
                ->where
                (
                    function ($query) use ($tablename2,$searchname2) 
                    { 
                        $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                    }
                ) 
                ->Join('agent','agent.id', '=',  $this_month_table.'.agent_id')
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id') 
                ->where($this_month_table.'.id','>=',$minID)
                ->where($this_month_table.'.id','<=',$maxID)
                ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)              
                ->orderBy($this_month_table.'.id' , 'desc')
                ->selectRaw
                (
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id ,'.
                    $this_month_table.'.trx_datetime,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id,'.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                );
        } //M.001.003*.001(else)*.001(else)* 
        else if(($search != '') && ($search2 != '') ) { //M.001.003*.001(else)*.001(else)*.001(else) 
            log::info("query month 4 search =".$search." search2 =".$search2);
            $q = $this->LogTrxMonth 
                ->where
                (
                    function ($query) use ($tablename,$searchname)
                    { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    }
                )  
                ->where
                (
                    function ($query) use ($tablename2,$searchname2) 
                    { 
                        $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                    }
                ) 
                ->Join('agent','agent.id', '=',  $this_month_table.'.agent_id')
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id')  
                ->where($this_month_table.'.id','>=',$minID)
                ->where($this_month_table.'.id','<=',$maxID)
                ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)              
                ->orderBy($this_month_table.'.id' , 'desc')
                ->selectRaw
                (
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id,'.
                    $this_month_table.'.trx_datetime,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id,'.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                );
        } //M.001.[003*.001(else)*.001(else)*.001(else)*]
    } //M.001*.[*] 
    else if(($opsidates == 'range')) { //M.001*.[*].001(else) 
        $logtrxtable =  "log_trx_agent_".$thismonth;
        $fromdates = date("Y-m-").$fromdates;
        $todates = date("Y-m-").$todates; 

        switch ($tableselect) {//M.001*.[*].001(else).001
            case 'id':
                $tablename=$this_month_table.".agent_id";
                 $searchname = $this->Util->base36ToInt( $search );
                break;
            case 'mode':
                $tablename=$this_month_table.".trx_mode";
                $searchname = $search;
                break; 
            case 'username':
                $tablename="username";$searchname = $search;
                break;
            case 'type':
                $tablename=$this_month_table.".trx_type";$searchname = $search;
                break; 
            case 'product_id':
                $tablename=$this_month_table.".trx_product_id";$searchname = $search;
                break; 
            case 'product_code':
                $tablename=$this_month_table.".trx_product_code";$searchname = $search;
                break; 
            case 'bill_no':
                $tablename=$this_month_table.".trx_bill_number";$searchname = $search;
                break; 
            case 'trx_code':
                $tablename=$this_month_table.".trx_code";$searchname = $search;
                break; 
            default:
                $tablename="";$searchname = $search;
            break;
        }//M.001*.[*].001(else).001*

        switch ($tableselect2) { //M.001*.[*].001(else).002
             case 'id':
                $tablename2=$this_month_table.".agent_id";
                 $searchname2 = $this->Util->base36ToInt( $search2 );
                break;
            case 'mode':
                $tablename2=$this_month_table.".trx_mode";
                $searchname2 = $search2;
                break; 
            case 'username':
                $tablename2="username";
                $searchname2 = $search2;
                break;
            case 'type':
                $tablename2=$this_month_table.".trx_type";
                $searchname2 = $search2;
                break;
            case 'product_id':
                $tablename2=$this_month_table.".trx_product_id";
                $searchname2 = $search2;
                break;
            case 'product_code':
                $tablename2=$this_month_table.".trx_product_code";
                $searchname2 = $search2;
                break;
            case 'bill_no':
                $tablename2=$this_month_table.".trx_bill_number";
                $searchname2 = $search2;
                break;
            case 'trx_code':
                $tablename2=$this_month_table.".trx_code";
                $searchname2 = $search2;
                break;
            default:
                $tablename2="";
                $searchname2 = $search2; 
            break;
        } //M.001*.[*].001(else).002*

        $rsMinMaxID = $this->LogTrxthisMonth
                        ->whereRaw("trx_datetime >= ? AND trx_datetime <= ?", 
                            array($fromdates." 00:00:00", $todates." 23:59:59"))
                        ->selectRaw('min(id) as min_id, max(id) as max_id')
                        ->first();
        $minID = 0;
        $maxID = 0;
        if (!is_null($rsMinMaxID)) {
           $minID = $rsMinMaxID->min_id;
           $maxID = $rsMinMaxID->max_id; 
        }                

        if(($search == '') && ($search2 == '') ) { //M.001*.[*].001(else).003
            log::info("query range none search");
            $q = $this->LogTrxthisMonth
                ->Join('befintech.agent','agent.id', '=',  $this_month_table.'.agent_id')            
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id')
                ->where($this_month_table.'.id','>=',$minID)
                ->where($this_month_table.'.id','<=',$maxID)
                ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)              
                ->orderBy($this_month_table.'.id' , 'desc')
                ->selectRaw(
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id ,'.
                    $this_month_table.'.trx_datetime,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id,'.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                ); 
         } //M.001*.[*].001(else).003* 
         else if(($search != '') && ($search2 == '') ) { //M.001*.[*].001(else).003*.001(else)
            log::info("query range search =".$search);
            $q = $this->LogTrxthisMonth
                ->whereRaw("trx_datetime >= ? AND trx_datetime <= ?", 
                    array($fromdates." 00:00:00", $todates." 23:59:59"))
                ->where(
                    function ($query) use ($tablename,$searchname) { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    }
                )  
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id')
                ->Join('befintech.agent','agent.id', '=',  $this_month_table.'.agent_id') 
                ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)              
                ->orderBy($this_month_table.'.id' , 'desc')
                ->selectRaw(
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id ,'.
                    $this_month_table.'.trx_datetime,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id,'.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                ); 
            } //M.001*.[*].001(else).003*.001(else)*
            else if(($search == '') && ($search2 != '') ) { //M.001*.[*].001(else).003*.001(else)*.001(else)
                log::info("query range search2 =".$search2);
                $q = $this->LogTrxthisMonth
                    ->whereRaw("trx_datetime >= ? AND trx_datetime <= ?", 
                    array($fromdates." 00:00:00", $todates." 23:59:59"))
                    ->where(
                        function ($query) use ($tablename2,$searchname2) { 
                            $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                        }
                    )    
                    ->Join('befintech.agent','agent.id', '=',  $this_month_table.'.agent_id')
                    ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id') 
                    ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)              
                    ->orderBy($this_month_table.'.id' , 'desc')
                    ->selectRaw(
                        'customer_phone_number,'.
                        'customer_name,'.
                        'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id ,'.
                        $this_month_table.'.trx_datetime,'.
                        $this_month_table.'.trx_mode,'.
                        $this_month_table.'.trx_type,'.
                        $this_month_table.'.trx_code,'.
                        $this_month_table.'.trx_bill_number,'.
                        $this_month_table.'.trx_product_code,'.
                        $this_month_table.'.trx_product_id,'.
                        $this_month_table.'.description,'.
                        $this_month_table.'.amount,'.
                        $this_month_table.'.balance_value_before,'.
                        $this_month_table.'.balance_value_after,'.
                        $this_month_table.'.info_printable,'.
                        $this_month_table.'.created_at,'.
                        'trx_sheet_number,'.
                        'app_info,'.
                        'username'
                    );        
            } //M.001*.[*].001(else).003*.001(else)*.001(else)*
            else if(($search != '') && ($search2 != '') ) { //M.001*.[*].001(else).003*.001(else)*.001(else)*.001(else)
                log::info("query range search =".$search." search2 =".$search2);
                $q = $this->LogTrxthisMonth
                    ->whereRaw("trx_datetime >= ? AND trx_datetime <= ?", 
                        array($fromdates." 00:00:00", $todates." 23:59:59"))
                    ->where(
                        function ($query) use ($tablename,$searchname) { 
                            $query->where( $tablename, 'like', '%'.$searchname.'%');
                        }
                    )  
                ->where(function ($query) use ($tablename2,$searchname2) { 
                            $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                        })   
                ->Join('befintech.agent','agent.id', '=',  $this_month_table.'.agent_id') 
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id') 
                ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)              
                ->orderBy($this_month_table.'.id' , 'desc')
                ->selectRaw(
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id ,'.
                    $this_month_table.'.trx_datetime,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id,'.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                );        
            } //M.001*.[*].001(else).003*.001(else)*.001(else)*.001(else)*
        } //M.001*.[*].001(else)*.[*]
        else { //M.001*.[*].001(else)*.[*] ==> M.001*.[*].x*.001(else)  
            log::info("query 1");
            log::info("  history table=".$this_month_table);
            log::info("data dates=".$fromdates." and ".$todates);
            log::info(" data MA=".json_encode($this->listMasterAgent)); 
            
            $logtrxtable = 'log_trx_agent_'.date("Y_m");log::info("  log table=".$logtrxtable); 

            $q = $this->LogTrxthisMonth 
                ->Join('agent','agent.id', '=',  $this_month_table.'.agent_id') 
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id') 
                ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)              
                ->orderBy($this_month_table.'.id' , 'desc')
                ->selectRaw(
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id ,'.
                    $this_month_table.'.trx_datetime,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id,'.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                );        
        } //M.001*.[*].001(else)*.[*] ==> M.001*.[*].x*.001(else)*

        $dateString = date("Y-m-d");
        $listmnth=[];
        $listyear=[];
        $lastDateOfMonth = date("t", strtotime($dateString));
        
        for($a=1;$a<=$lastDateOfMonth;$a++) {
            array_push($listfrom,$a);
        }

        for($i = 1 ; $i <= 12; $i++) { 
            array_push($listmnth,date("F",strtotime(date("Y")."-".$i."-01"))); 
        }

        for($m = 2019 ; $m <= 2030; $m++) { 
            array_push($listyear,$m); 
        }

        $this->tableSorter->setupPaging($q, $pg);
        $this->tableSorter->setWeek($week);
        $this->viewData['sorter'] = $this->tableSorter;
        $this->viewData['pg'] = $pg;
        $this->viewData['searchFor'] = $searchFor; 

        $this->viewData['mnt'] = $mnt; 
        $this->viewData['yr'] = $yr; 
        $this->viewData['opsidates'] = $opsidates;
        $this->viewData['fromdates'] = $fromdates;
        $this->viewData['todates'] = $todates;
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search; 
        $this->viewData['tableselect2'] = $tableselect2;
        $this->viewData['search2'] = $search2;

        $this->viewData['today'] = date("Y-m-d");
        $this->viewData['months'] = date("F");
        $this->viewData['years'] = date("Y");
        $this->viewData['listfrom'] = $listfrom;
        $this->viewData['listmnth'] = $listmnth;
        $this->viewData['listyear'] = $listyear;
        $this->viewData['week'] = $week;
        
        return view('admin.report.deposit_history', $this->viewData );
 }
    


    function search_trx(Request $request){
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

    $min =Input::get('min'); //$request->get('min').' 00:00:00';
    $max = Input::get('max');//$request->get('max').' 23:59:59'; //$request->get('max');
    $tableselect = $request->tableselect;
    $search =$request->input('search');

    if ($search == '') { $this->notif->addMessage('Phone number is required'); }



    $timeMin = '00:00:00';
    $timeMax = '23:59:59';
    $this->listMA = $this->helpdesk->listMabyUpline($this->session->get('master_agent_id')); 
      log::info("session MA =".$this->session->get('master_agent_id'));
      log::info("list ma =".json_encode($this->listMA));
    $this->listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id')); 
    // log::info(" History Agent ");
    if($min != '')
    {
        $result1 = $min. ' ' . $timeMin; 
    } 
    else
    {
      $result1 = '';  
    }

    if($max != '')
    {
        $result2 = $max . ' ' . $timeMax;
    } 
    else
    {
      $result2 = '';  
    }

    $fields = [
            // ['customer_phone_number', 'Customer Phone Number'],
            // ['customer_name', 'Customer Name'],


        // Transaksi   Deskripsi   Saldo Sebelum   Saldo Sesudah   Waktu



            ['trx_datetime', 'Trx DateTime'],
            ['master_agent_id', 'Master Agent'],
            ['master_agent_id', 'Fullname'],
            ['trx_code', 'Trx Code'],
            ['trx_code', 'Trx Mode'],
            ['trx_code', 'Trx Type'],
            ['username', 'Deskripsi'],
            ['trx_mode', 'Sal Before'],
            ['trx_type', 'Sal After'] 

        ]; 


        switch ($tableselect) {
            case 'id':
                $tablename="history_trx_master_agent.master_agent_id";
                // $search = $this->Util->base36ToInt( $search );
                 // $searchname = $this->Util->base36ToInt( $search );
                break;
            case 'mode':
                $tablename="history_trx_master_agent.trx_mode";
                $searchname = $search;
                break; 
            case 'fullname':
                $tablename="fullname";
                $searchname = $search;
                break;
            case 'type':
                $tablename="history_trx_master_agent.trx_type";
                $searchname = $search;
                break; 
            case 'code':
                $tablename="history_trx_master_agent.trx_code";
                $searchname = $search;
                break;   
            case 'description':
                $tablename="history_trx_master_agent.description";
                $searchname = $search;
                break;  
            default:
                $tablename="";
                $searchname = $search;
            break;
        }
        
 
        $this->tableSorter->setupSorter(secure_url('/ma/history_trx.html'), $fields, $searchFor, $sf, $sm, $min, $max, $tableselect, $search);
       

         if(($search != '') && ($min != '' && $max != '')) {         
             $q = $this->HistoryTrxMasterAgent->whereBetween(
                'befintechlog.history_trx_master_agent.trx_datetime', 
                [ 
                    $result1,
                    $result2
                ]
                ) 
                ->where(function ($query) use ($tablename,$searchname) { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    })
             ->Join('master_agent_profile','history_trx_master_agent.master_agent_id', '=', 'befintech.master_agent_profile.master_agent_id')    
             ->whereIn('history_trx_master_agent.master_agent_id',  $this->listMA)  
             ->orderby('trx_datetime','desc')
             ->selectRaw('history_trx_master_agent.master_agent_id,fullname,trx_datetime,trx_code,trx_mode,trx_type, amount,balance_value_before,balance_value_after,history_trx_master_agent.description');
        }else if($min != '' && $max != ''){
             $q = $this->HistoryTrxMasterAgent->whereBetween(
                'befintechlog.history_trx_master_agent.trx_datetime', 
                [
                    $result1,
                    $result2
                ]
                )
              ->Join('master_agent_profile','history_trx_master_agent.master_agent_id', '=', 'befintech.master_agent_profile.master_agent_id')    
             ->whereIn('history_trx_master_agent.master_agent_id',  $this->listMA)  
             ->orderby('trx_datetime','desc')
             ->selectRaw('history_trx_master_agent.master_agent_id,fullname,trx_datetime,trx_code,trx_mode,trx_type, amount,balance_value_before,balance_value_after,history_trx_master_agent.description');
        }else if(($search != '' && $tableselect != '') && ($min == '' && $max == '')){
            log::info("query 3");
            $q = $this->HistoryTrxMasterAgent
            ->where(function ($query) use ($tablename,$searchname) { 
            $query->where( $tablename, 'like', '%'.$searchname.'%');
            })                 
            ->Join('master_agent_profile','history_trx_master_agent.master_agent_id', '=', 'befintech.master_agent_profile.master_agent_id')    
             ->whereIn('history_trx_master_agent.master_agent_id',  $this->listMA)  
             ->orderby('trx_datetime','desc')
             ->selectRaw('history_trx_master_agent.master_agent_id,fullname,trx_datetime,trx_code,trx_mode,trx_type, amount,balance_value_before,balance_value_after,history_trx_master_agent.description'); 
        }else {
             $q = $this->HistoryTrxMasterAgent            
             ->Join('master_agent_profile','history_trx_master_agent.master_agent_id', '=', 'befintech.master_agent_profile.master_agent_id')    
             ->whereIn('history_trx_master_agent.master_agent_id',  $this->listMA)  
             ->orderby('trx_datetime','desc')
             ->selectRaw('history_trx_master_agent.master_agent_id,fullname,trx_datetime,trx_code,trx_mode,trx_type, amount,balance_value_before,balance_value_after,history_trx_master_agent.description'); 
        }
  

        $this->tableSorter->setupPaging($q, $pg);
        $this->viewData['sorter'] = $this->tableSorter;
        $this->viewData['pg'] = $pg;
        $this->viewData['searchFor'] = $searchFor;
        $this->viewData['min'] = $result1;
        $this->viewData['max'] = $result2;
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
        $this->viewData['today'] = date("Y-m-d");
        
        return view('admin.report.deposit_history_trx', $this->viewData );
 }
    
    function exportTrxToCSV(Request $request){
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

    $min =Input::get('min'); //$request->get('min').' 00:00:00';
    $max = Input::get('max');//$request->get('max').' 23:59:59'; //$request->get('max');
    $tableselect = $request->tableselect; 
    $search =$request->input('search');

    if ($search == '') { $this->notif->addMessage('Phone number is required'); }



    $timeMin = '00:00:00';
    $timeMax = '23:59:59';
    $this->listMA = $this->helpdesk->listMabyUpline($this->session->get('master_agent_id')); 
      log::info("session MA =".$this->session->get('master_agent_id'));
      log::info("list ma =".json_encode($this->listMA));
    $this->listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id')); 
    // log::info(" History Agent ");
    if($min != '')
    {
        $result1 = $min. ' ' . $timeMin; 
    } 
    else
    {
      $result1 = '';  
    }

    if($max != '')
    {
        $result2 = $max . ' ' . $timeMax;
    } 
    else
    {
      $result2 = '';  
    }

    $fields = [
            // ['customer_phone_number', 'Customer Phone Number'],
            // ['customer_name', 'Customer Name'],


        // Transaksi   Deskripsi   Saldo Sebelum   Saldo Sesudah   Waktu



            ['trx_datetime', 'Trx DateTime'],
            ['master_agent_id', 'Master Agent'],
            ['master_agent_id', 'Fullname'],
            ['trx_code', 'Trx Code'],
            ['trx_code', 'Trx Mode'],
            ['trx_code', 'Trx Type'],
            ['username', 'Deskripsi'],
            ['trx_mode', 'Sal Before'],
            ['trx_type', 'Sal After'] 

        ]; 


        switch ($tableselect) {
            case 'id':
                $tablename="history_trx_master_agent.master_agent_id";
                // $search = $this->Util->base36ToInt( $search );
                 // $searchname = $this->Util->base36ToInt( $search );
                break;
            case 'mode':
                $tablename="history_trx_master_agent.trx_mode";$searchname = $search;
                break; 
            case 'fullname':
                $tablename="fullname";$searchname = $search;
                break;
            case 'type':
                $tablename="history_trx_master_agent.trx_type";$searchname = $search;
                break; 
            case 'code':
                $tablename="history_trx_master_agent.trx_code";$searchname = $search;
                break;   
            case 'description':
                $tablename="history_trx_master_agent.description";$searchname = $search;
                break;  
            default:
                $tablename="";$searchname = $search;
            break;
        }
        
 
        $this->tableSorter->setupSorter(secure_url('/ma/history_trx.html'), $fields, $searchFor, $sf, $sm, $min, $max, $tableselect, $search);
       

         if(($search != '') && ($min != '' && $max != '')) {         
             $q = $this->HistoryTrxMasterAgent->whereBetween(
                'befintechlog.history_trx_master_agent.trx_datetime', 
                [ 
                    $result1,
                    $result2
                ]
                ) 
                ->where(function ($query) use ($tablename,$searchname) { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    })
             ->Join('master_agent_profile','history_trx_master_agent.master_agent_id', '=', 'befintech.master_agent_profile.master_agent_id')    
             ->whereIn('history_trx_master_agent.master_agent_id',  $this->listMA)  
             ->orderby('trx_datetime','desc')
             ->selectRaw('history_trx_master_agent.master_agent_id,fullname,trx_datetime,trx_code,trx_mode,trx_type, amount,balance_value_before,balance_value_after,history_trx_master_agent.description')->get();
        }else if($min != '' && $max != ''){
             $q = $this->HistoryTrxMasterAgent->whereBetween(
                'befintechlog.history_trx_master_agent.trx_datetime', 
                [
                    $result1,
                    $result2
                ]
                )
              ->Join('master_agent_profile','history_trx_master_agent.master_agent_id', '=', 'befintech.master_agent_profile.master_agent_id')    
             ->whereIn('history_trx_master_agent.master_agent_id',  $this->listMA)  
             ->orderby('trx_datetime','desc')
             ->selectRaw('history_trx_master_agent.master_agent_id,fullname,trx_datetime,trx_code,trx_mode,trx_type, amount,balance_value_before,balance_value_after,history_trx_master_agent.description')->get();
        }else if(($search != '' && $tableselect != '') && ($min == '' && $max == '')){
            log::info("query 3");
            $q = $this->HistoryTrxMasterAgent
            ->where(function ($query) use ($tablename,$searchname) { 
            $query->where( $tablename, 'like', '%'.$searchname.'%');
            })                 
            ->Join('master_agent_profile','history_trx_master_agent.master_agent_id', '=', 'befintech.master_agent_profile.master_agent_id')    
             ->whereIn('history_trx_master_agent.master_agent_id',  $this->listMA)  
             ->orderby('trx_datetime','desc')
             ->selectRaw('history_trx_master_agent.master_agent_id,fullname,trx_datetime,trx_code,trx_mode,trx_type, amount,balance_value_before,balance_value_after,history_trx_master_agent.description')->get(); 
        }else {
             $q = $this->HistoryTrxMasterAgent            
             ->Join('master_agent_profile','history_trx_master_agent.master_agent_id', '=', 'befintech.master_agent_profile.master_agent_id')    
             ->whereIn('history_trx_master_agent.master_agent_id',  $this->listMA)  
             ->orderby('trx_datetime','desc')
             ->selectRaw('history_trx_master_agent.master_agent_id,fullname,trx_datetime,trx_code,trx_mode,trx_type, amount,balance_value_before,balance_value_after,history_trx_master_agent.description')->get(); 
        }
  

            $filename = "Report_History_MasterAgent.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array( 'trx_datetime' ,'master_agent_id' ,'fullname', 'trx_code ', 'trx_mode', 'trx_type' , 'description' , 'balance_value_before' , 'balance_value_after'));

            foreach($q as $row) {
                fputcsv($handle, array($row['trx_datetime'], $row['master_agent_id'], $row['fullname'], $row['trx_code'], $row['trx_mode'], $row['trx_type'],$row['description'],$row['balance_value_before'],$row['balance_value_after']));
 
            }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            return response()->download($filename, 'history_trx_ma.csv', $headers);
 }
    
function exportAgentToCSV(Request $request){
    Log::info('HistoryAgentController->exportAgentToCSV: aaa started');
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

    $tableselect = $request->tableselect;
    $search =$request->input('search');

    $tableselect2 = $request->tableselect2;
    $search2 =$request->input('search2');
 
    $opsidates =$request->input('opsidates');

    $fromdates =$request->input('fromdates');
    $todates =$request->input('todates');

    $mnt =$request->input('mnt');
    $yr =$request->input('yr');

    //Week variable is added by amin @2019-10-10 
    $week= Input::get('week',1);

    $thismonth = date("Y_m"); 
    $this_month_table =  "befintechlog.history_trx_agent_".$thismonth;
    $this->LogTrxthisMonth = new DynamicDuoTables; 
    $this->LogTrxthisMonth->setConnection($this->conn); 
    $this->LogTrxthisMonth->setTable($this_month_table);

    $daterange = 0;
    $listfrom=[];
 
    $this->listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id')); 
    $this->listMasterAgent = $this->helpdesk->listMabyUpline($this->session->get('master_agent_id')); 
 
    $fields = [
            ['agent_id', 'Agent Id'],
            ['username', 'Username'],
            ['trx_datetime', 'Trx DateTime'],
            ['trx_mode', 'Trx Mode'],
            ['trx_type', 'Trx Type'],
            ['trx_code', 'Trx Code'],
            ['trx_bill_number', 'Trx Bill Number'],
            ['trx_product_code', 'Trx Product Code'],
            ['trx_product_id', 'Trx Product Id'],
            ['description' , 'Description'],
            ['amount' , 'Amount'],
            ['balance_value_before' , 'Balance Before'],
            ['balance_value_after' , 'Balance After'],
            ['info_printable' , 'App Info'],
            ['trx_sheet_number' , 'Trx Sheet Number'],
            ['created_at', 'Created At']
    ]; 
 
    $this->tableSorter->setupSorter(
        secure_url('/ma/history_agent.html'), 
        $fields, 
        $searchFor, 
        $mnt, 
        $yr,
        $fromdates,
        $todates,
        $tableselect,
        $search,
        $daterange,
        $tableselect2,
        $search2,
        $opsidates
    );

     Log::info('HistoryAgentController->exportAgentToCSV: aaa end');
    
    if(($opsidates == 'month')) { //M.001
        $monthcurrent = $mnt+1 ; 
        $usagemonth = $yr.'_'.str_pad($monthcurrent,2,"0",STR_PAD_LEFT); 
        $this_month_table =  "befintechlog.history_trx_agent_".$usagemonth;
        $logtrxtable =  "log_trx_agent_".$usagemonth;
        $this->LogTrxMonth = new DynamicDuoTables; 
        $this->LogTrxMonth->setConnection($this->conn); 
        $this->LogTrxMonth->setTable($this_month_table);

        $vMonth= str_pad($monthcurrent,2,"0",STR_PAD_LEFT);

        $filterDateStart = '';
        $filterDateEnd = '';

        switch ($week) {
            case 1:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'01 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'07 23:59:59'.'\'';    
                break;
            case 2:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'08 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'15 23:59:59'.'\'';    
                break;
            case 3:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'16 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'23 23:59:59'.'\'';    
                break;
            case 4:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'24 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'31 23:59:59'.'\'';    
                break;
        }
        Log::info('HistoryAgentController->search_history: Selected group of day='.$week);
        Log::info('HistoryAgentController->search_history: filter date='.$filterDateStart.' and '.$filterDateEnd);

        //Get minID and maxID
        $rsMinMaxID =  $this->LogTrxMonth
            ->selectRaw('min(id) as min_id, max(id) as max_id')
            ->whereRaw($this_month_table.'.trx_datetime between '.$filterDateStart.' and '.$filterDateEnd)
            ->first();
        $minID = 0;
        $maxID = 0;    
        if (! is_null($rsMinMaxID) ) {
            $minID = $rsMinMaxID->min_id;
            $maxID = $rsMinMaxID->max_id;
        }
        Log::info('HistoryAgentController->search_history : min ID = '.$minID);
        Log::info('HistoryAgentController->search_history : max ID = '.$maxID);    

        switch ($tableselect) { //M.001.001
            case 'id':
                $tablename=$this_month_table.".agent_id";
                 $searchname = $this->Util->base36ToInt( $search );
                break;
            case 'mode':
                $tablename=$this_month_table.".trx_mode";
                $searchname = $search;
                break; 
            case 'username':
                $tablename="username";$searchname = $search;
                break;
            case 'type':
                $tablename=$this_month_table.".trx_type";$searchname = $search;
                break; 
            case 'product_id':
                $tablename=$this_month_table.".trx_product_id";$searchname = $search;
                break; 
            case 'product_code':
                $tablename=$this_month_table.".trx_product_code";$searchname = $search;
                break; 
            case 'bill_no':
                $tablename=$this_month_table.".trx_bill_number";$searchname = $search;
                break; 
            case 'trx_code':
                $tablename=$this_month_table.".trx_code";$searchname = $search;
                break; 
            default:
                $tablename="";$searchname = $search;
            break;
        } //M.001.001*

        switch ($tableselect2) { //M.001.002
             case 'id':
                $tablename2=$this_month_table.".agent_id";
                 $searchname2 = $this->Util->base36ToInt( $search2 );
                break;
            case 'mode':
                $tablename2=$this_month_table.".trx_mode";
                $searchname2 = $search2;
                break; 
            case 'username':
                $tablename2="username";
                $searchname2 = $search2;
                break;
            case 'type':
                $tablename2=$this_month_table.".trx_type";
                $searchname2 = $search2;
                break;
            case 'product_id':
                $tablename2=$this_month_table.".trx_product_id";
                $searchname2 = $search2;
                break;
            case 'product_code':
                $tablename2=$this_month_table.".trx_product_code";
                $searchname2 = $search2;
                break;
            case 'bill_no':
                $tablename2=$this_month_table.".trx_bill_number";
                $searchname2 = $search2;
                break;
            case 'trx_code':
                $tablename2=$this_month_table.".trx_code";
                $searchname2 = $search2;
                break;
            default:
                $tablename2="";
                $searchname2 = $search2; 
            break;
        } //M.001.002*



        if(($search == '') && ($search2 == '') ) { //M.001.003
            log::info("query month 1 none search");
            log::info("  history table=".$this_month_table); 
            log::info("  log table=".$logtrxtable); 
            log::info(" data MA=".json_encode($this->listMasterAgent)); 
           
            $q = $this->LogTrxMonth  
                ->Join('agent','agent.id', '=',  $this_month_table.'.agent_id')             
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id')
                ->where($this_month_table.'.id','>=',$minID)
                ->where($this_month_table.'.id','<=',$maxID)
                ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)
                ->orderBy($this_month_table.'.id' , 'asc') // change order from trx_datetime to id 
                ->selectRaw
                (
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id ,'.
                    $this_month_table.'.trx_datetime ,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id , '.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                )
                ->get();
         } //M.001.003*
         else if(($search != '') && ($search2 == '') ) { //M.001.003*.001(else)
            log::info("query month 2 search =".$search);
            $q = $this->LogTrxMonth->Join('agent','agent.id', '=',  $this_month_table.'.agent_id')   
                ->where($this_month_table.'.id','>=',$minID)
                ->where($this_month_table.'.id','<=',$maxID)
                ->whereIn('agent.master_agent_id',  $this->listMasterAgent)
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id')
                ->orderBy($this_month_table.'.id' , 'desc')
                ->where
                (
                    function ($query) use ($tablename,$searchname) 
                    { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    }
                )
                ->selectRaw
                (
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id,'.
                    $this_month_table.'.trx_datetime,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id,'.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after ,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                )
                ->get();
            log::info("query month tablename2 =".$tablename2." and ".$searchname2);
        } //M.001.003*.001(else)* 
        else if(($search == '') && ($search2 != '') ) { //M.001.003*.001(else)*.001(else)
            log::info("query month 3 search2 =".$search2);
            $q = $this->LogTrxMonth                
                ->where
                (
                    function ($query) use ($tablename2,$searchname2) 
                    { 
                        $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                    }
                ) 
                ->Join('agent','agent.id', '=',  $this_month_table.'.agent_id')
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id') 
                ->where($this_month_table.'.id','>=',$minID)
                ->where($this_month_table.'.id','<=',$maxID)
                ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)              
                ->orderBy($this_month_table.'.id' , 'desc')
                ->selectRaw
                (
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id ,'.
                    $this_month_table.'.trx_datetime,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id,'.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                )
                ->get();
        } //M.001.003*.001(else)*.001(else)* 
        else if(($search != '') && ($search2 != '') ) { //M.001.003*.001(else)*.001(else)*.001(else) 
            log::info("query month 4 search =".$search." search2 =".$search2);
            $q = $this->LogTrxMonth 
                ->where
                (
                    function ($query) use ($tablename,$searchname)
                    { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    }
                )  
                ->where
                (
                    function ($query) use ($tablename2,$searchname2) 
                    { 
                        $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                    }
                ) 
                ->Join('agent','agent.id', '=',  $this_month_table.'.agent_id')
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id')  
                ->where($this_month_table.'.id','>=',$minID)
                ->where($this_month_table.'.id','<=',$maxID)
                ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)              
                ->orderBy($this_month_table.'.id' , 'desc')
                ->selectRaw
                (
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id,'.
                    $this_month_table.'.trx_datetime,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id,'.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                )
                ->get();
        } //M.001.[003*.001(else)*.001(else)*.001(else)*]
    } //M.001*.[*] 
    else if(($opsidates == 'range')) { //M.001*.[*].001(else) 
        $logtrxtable =  "log_trx_agent_".$thismonth;
        //Two lines below deactivated because causing error   
        //$fromdates = date("Y-m-").$fromdates;
        //$todates = date("Y-m-").$todates; 

        switch ($tableselect) {//M.001*.[*].001(else).001
            case 'id':
                $tablename=$this_month_table.".agent_id";
                 $searchname = $this->Util->base36ToInt( $search );
                break;
            case 'mode':
                $tablename=$this_month_table.".trx_mode";
                $searchname = $search;
                break; 
            case 'username':
                $tablename="username";$searchname = $search;
                break;
            case 'type':
                $tablename=$this_month_table.".trx_type";$searchname = $search;
                break; 
            case 'product_id':
                $tablename=$this_month_table.".trx_product_id";$searchname = $search;
                break; 
            case 'product_code':
                $tablename=$this_month_table.".trx_product_code";$searchname = $search;
                break; 
            case 'bill_no':
                $tablename=$this_month_table.".trx_bill_number";$searchname = $search;
                break; 
            case 'trx_code':
                $tablename=$this_month_table.".trx_code";$searchname = $search;
                break; 
            default:
                $tablename="";$searchname = $search;
            break;
        }//M.001*.[*].001(else).001*

        switch ($tableselect2) { //M.001*.[*].001(else).002
             case 'id':
                $tablename2=$this_month_table.".agent_id";
                 $searchname2 = $this->Util->base36ToInt( $search2 );
                break;
            case 'mode':
                $tablename2=$this_month_table.".trx_mode";
                $searchname2 = $search2;
                break; 
            case 'username':
                $tablename2="username";
                $searchname2 = $search2;
                break;
            case 'type':
                $tablename2=$this_month_table.".trx_type";
                $searchname2 = $search2;
                break;
            case 'product_id':
                $tablename2=$this_month_table.".trx_product_id";
                $searchname2 = $search2;
                break;
            case 'product_code':
                $tablename2=$this_month_table.".trx_product_code";
                $searchname2 = $search2;
                break;
            case 'bill_no':
                $tablename2=$this_month_table.".trx_bill_number";
                $searchname2 = $search2;
                break;
            case 'trx_code':
                $tablename2=$this_month_table.".trx_code";
                $searchname2 = $search2;
                break;
            default:
                $tablename2="";
                $searchname2 = $search2; 
            break;
        } //M.001*.[*].001(else).002*

        Log::info('HistoryAgentController->exportAgentToCSV: search by range => check #0001 started');
        Log::info('HistoryAgentController->exportAgentToCSV: search by range fromdates='.$fromdates);
        Log::info('HistoryAgentController->exportAgentToCSV: search by range todates='.$todates);

        $qMinMaxID = $this->LogTrxthisMonth
                        ->whereRaw("trx_datetime >= ? AND trx_datetime <= ?", 
                            array($fromdates." 00:00:00", $todates." 23:59:59"))
                        ->selectRaw('min(id) as min_id, max(id) as max_id');
        $sql = $qMinMaxID->toSql();                
                      
        Log::info('HistoryAgentController->exportAgentToCSV : rsMinMaxID::sql='.$sql);                

        $rsMinMaxID = $qMinMaxID->first();

        $minID = 0;
        $maxID = 0;
        if (!is_null($rsMinMaxID)) {
           Log::info('HistoryAgentController->exportAgentToCSV:rsMinMaxID='.json_encode($rsMinMaxID)); 
           $minID = $rsMinMaxID->min_id;
           $maxID = $rsMinMaxID->max_id; 
        }                

        Log::info('HistoryAgentController->exportAgentToCSV: minID='.$minID);
        Log::info('HistoryAgentController->exportAgentToCSV: maxID='.$maxID);

        Log::info('HistoryAgentController->exportAgentToCSV: search by range => check #0001 passed');

        if(($search == '') && ($search2 == '') ) { //M.001*.[*].001(else).003
            Log::info('HistoryAgentController->exportAgentToCSV: search by range => check #0002 started');
            log::info("query range none search");
            $q = $this->LogTrxthisMonth
                ->Join('befintech.agent','agent.id', '=',  $this_month_table.'.agent_id')            
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id')
                ->where($this_month_table.'.id','>=',$minID)
                ->where($this_month_table.'.id','<=',$maxID)
                ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)              
                ->orderBy($this_month_table.'.id' , 'desc')
                ->selectRaw(
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id ,'.
                    $this_month_table.'.trx_datetime,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id,'.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                )
                ->get(); 
                Log::info('HistoryAgentController->exportAgentToCSV: search by range => check #0002 passed');
         } //M.001*.[*].001(else).003* 
         else if(($search != '') && ($search2 == '') ) { //M.001*.[*].001(else).003*.001(else)
            Log::info('HistoryAgentController->exportAgentToCSV: search by range => check #0003 started');
            log::info("query range search =".$search);
            $q = $this->LogTrxthisMonth
                ->whereRaw("trx_datetime >= ? AND trx_datetime <= ?", 
                    array($fromdates." 00:00:00", $todates." 23:59:59"))
                ->where(
                    function ($query) use ($tablename,$searchname) { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    }
                )  
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id')
                ->Join('befintech.agent','agent.id', '=',  $this_month_table.'.agent_id') 
                ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)              
                ->orderBy($this_month_table.'.id' , 'desc')
                ->selectRaw(
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id ,'.
                    $this_month_table.'.trx_datetime,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id,'.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                )
                ->get(); 
                Log::info('HistoryAgentController->exportAgentToCSV: search by range => check #0003 passed');
            } //M.001*.[*].001(else).003*.001(else)*
            else if(($search == '') && ($search2 != '') ) { //M.001*.[*].001(else).003*.001(else)*.001(else)
                Log::info('HistoryAgentController->exportAgentToCSV: search by range => check #0004 started');
                log::info("query range search2 =".$search2);
                $q = $this->LogTrxthisMonth
                    ->whereRaw("trx_datetime >= ? AND trx_datetime <= ?", 
                    array($fromdates." 00:00:00", $todates." 23:59:59"))
                    ->where(
                        function ($query) use ($tablename2,$searchname2) { 
                            $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                        }
                    )    
                    ->Join('befintech.agent','agent.id', '=',  $this_month_table.'.agent_id')
                    ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id') 
                    ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)              
                    ->orderBy($this_month_table.'.id' , 'desc')
                    ->selectRaw(
                        'customer_phone_number,'.
                        'customer_name,'.
                        'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id ,'.
                        $this_month_table.'.trx_datetime,'.
                        $this_month_table.'.trx_mode,'.
                        $this_month_table.'.trx_type,'.
                        $this_month_table.'.trx_code,'.
                        $this_month_table.'.trx_bill_number,'.
                        $this_month_table.'.trx_product_code,'.
                        $this_month_table.'.trx_product_id,'.
                        $this_month_table.'.description,'.
                        $this_month_table.'.amount,'.
                        $this_month_table.'.balance_value_before,'.
                        $this_month_table.'.balance_value_after,'.
                        $this_month_table.'.info_printable,'.
                        $this_month_table.'.created_at,'.
                        'trx_sheet_number,'.
                        'app_info,'.
                        'username'
                    )
                    ->get();   
                    Log::info('HistoryAgentController->exportAgentToCSV: search by range => check #0004 passed');     
            } //M.001*.[*].001(else).003*.001(else)*.001(else)*
            else if(($search != '') && ($search2 != '') ) { //M.001*.[*].001(else).003*.001(else)*.001(else)*.001(else)
                Log::info('HistoryAgentController->exportAgentToCSV: search by range => check #0005 started');
                log::info("query range search =".$search." search2 =".$search2);
                $q = $this->LogTrxthisMonth
                    ->whereRaw("trx_datetime >= ? AND trx_datetime <= ?", 
                        array($fromdates." 00:00:00", $todates." 23:59:59"))
                    ->where(
                        function ($query) use ($tablename,$searchname) { 
                            $query->where( $tablename, 'like', '%'.$searchname.'%');
                        }
                    )  
                ->where(function ($query) use ($tablename2,$searchname2) { 
                            $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                        })   
                ->Join('befintech.agent','agent.id', '=',  $this_month_table.'.agent_id') 
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id') 
                ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)              
                ->orderBy($this_month_table.'.id' , 'desc')
                ->selectRaw(
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id ,'.
                    $this_month_table.'.trx_datetime,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id,'.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                )
                ->get();
                Log::info('HistoryAgentController->exportAgentToCSV: search by range => check #0005 passed');        
            } //M.001*.[*].001(else).003*.001(else)*.001(else)*.001(else)*
        } //M.001*.[*].001(else)*.[*]
        else { //M.001*.[*].001(else)*.[*] ==> M.001*.[*].x*.001(else)  
            Log::info('HistoryAgentController->exportAgentToCSV: search by range => check #0006 started');
            log::info("query 1");
            log::info("  history table=".$this_month_table);
            log::info("data dates=".$fromdates." and ".$todates);
            log::info(" data MA=".json_encode($this->listMasterAgent)); 
            
            $logtrxtable = 'log_trx_agent_'.date("Y_m");log::info("  log table=".$logtrxtable); 

            $q = $this->LogTrxthisMonth 
                ->Join('agent','agent.id', '=',  $this_month_table.'.agent_id') 
                ->LeftJoin('befintechlog.'.$logtrxtable, 'befintechlog.'.$logtrxtable.'.id', '=',  $this_month_table.'.log_id') 
                ->whereIn('befintech.agent.master_agent_id',  $this->listMasterAgent)              
                ->orderBy($this_month_table.'.id' , 'desc')
                ->selectRaw(
                    'customer_phone_number,'.
                    'customer_name,'.
                    'CONV('.$this_month_table.'.agent_id, 10, 36) as agent_id ,'.
                    $this_month_table.'.trx_datetime,'.
                    $this_month_table.'.trx_mode,'.
                    $this_month_table.'.trx_type,'.
                    $this_month_table.'.trx_code,'.
                    $this_month_table.'.trx_bill_number,'.
                    $this_month_table.'.trx_product_code,'.
                    $this_month_table.'.trx_product_id,'.
                    $this_month_table.'.description,'.
                    $this_month_table.'.amount,'.
                    $this_month_table.'.balance_value_before,'.
                    $this_month_table.'.balance_value_after,'.
                    $this_month_table.'.info_printable,'.
                    $this_month_table.'.created_at,'.
                    'trx_sheet_number,'.
                    'app_info,'.
                    'username'
                )
                ->get();    
                Log::info('HistoryAgentController->exportAgentToCSV: search by range => check #0006 passed');    
        } //M.001*.[*].001(else)*.[*] ==> M.001*.[*].x*.001(else)*

        Log::info('HistoryAgentController->exportAgentToCSV: search by range => check #0007 started');

        $filename = "Report_History_Agent.csv";
        $handle = fopen($filename, 'w+');
        fputcsv(
            $handle, 
            array(
                'agent_id', 
                'trx_datetime', 
                'trx_mode',
                'trx_type',
                'trx_code',
                'trx_bill_number',
                'trx_product_code',
                'trx_product_id',
                'description',
                'amount',
                'balance_value_before',
                'balance_value_after',
                'trx_sheet_number',
                'App Info' ,
                'created_at'
            )
        );
        foreach($q as $row) {
            fputcsv(
                $handle, 
                array(
                    $row['agent_id'],
                    $row['trx_datetime'],
                    $row['trx_mode'],
                    $row['trx_type'],
                    $row['trx_code'],
                    $row['trx_bill_number'],
                    $row['trx_product_code'],
                    $row['trx_product_id'],
                    $row['description'],
                    $row['amount'],
                    $row['balance_value_before'],
                    $row['balance_value_after'],
                    $row['trx_sheet_number'],
                    $row['app_info'],
                    $row['created_at']
                )
            );
        }

        Log::info('HistoryAgentController->exportAgentToCSV: search by range => check #0007 (end point) passed');

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );
        return response()->download($filename, 'Report_History_Agent.csv', $headers);
    }

}
