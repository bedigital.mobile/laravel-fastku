<?php

namespace App\Http\Controllers\admin\cms;

use DB;
use Log;
use Config;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\BusinessLogic\HelpdeskBL;

use App\Http\Controllers\BDGBaseController;
use App\Models\PartnerTrxReqModel;
use App\Models\PartnerDepositModel;
use App\Models\admin\AgentModel;
use App\Models\DynamicDuoTables;

use Rap2hpoutre\FastExcel\FastExcel;
class ReportController extends BDGBaseController
{

     public function __construct() {
        parent::__construct();    
        // $this->conn = Config::get ( 'mysql_stg' );
        $this->conn = Config::get ( 'webconf.cms.connectiondata' );
        $this->helpdesk = new HelpdeskBL; 

        $this->PartnerTrxReqModel = new  PartnerTrxReqModel; 
        $this->PartnerTrxReqModel->setConnection($this->conn); 

    }
    

 function reportamanah(Request $request){
   
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0); 

    $tableselect = $request->tableselect;
    $search =$request->input('search');

    $tableselect2 = $request->tableselect2;
    $search2 =$request->input('search2');
 
    $opsidates =$request->input('opsidates');

    $fromdates =$request->input('fromdates');
    $todates =$request->input('todates');

    $mnt =$request->input('mnt');
    $yr =$request->input('yr');

    $week= Input::get('week',1);

    $tablenamelog="gwbillerreport.trx_report_";
    $tablenames="trx_report_";
    $thismonth = date("Ym"); 
    $this_month_table =  $tablenamelog.$thismonth;
    //log::info(" datanya=".$this_month_table); 
    $this->LogTrxthisMonth = new DynamicDuoTables; 
    $this->LogTrxthisMonth->setConnection($this->conn); 
    $this->LogTrxthisMonth->setTable($this_month_table);




    $daterange = 0;
    $listfrom=[];
   
    $fields = [
            ['trx_date','Log DateTime'],
            ['trx_id', 'Transaction Id'],
            ['biller_name' , 'Biller'],
            ['partner_name', 'Partner'],
            ['product_id', 'Product Id'],
            ['product_code', 'Product Code'],
            ['product_name' , 'Product Name'],
            ['product_denom' , 'Denom'],
            ['nominal','Nominal'],
            ['info1','Transaction Info 1'],
            ['info2','Transaction Info 2'],
            ['info3','Transaction Info 3'],
            ['created_at','Created At'], 
            ['created_at','Balance Before'], 
            ['created_at','Balance After'],
            ['created_at','Transaction Type']

        ];  
 
 
    $this->tableSorter->setupSorter(secure_url('/amanah/report'), $fields, $searchFor, $mnt, $yr,$fromdates,$todates, $tableselect, $search ,$daterange, $tableselect2, $search2, $opsidates );



        $dateString = date("Y-m-d");
        $listmnth=[];
        $listyear=[];
        $lastDateOfMonth = date("t", strtotime($dateString));
        for($a=1;$a<=$lastDateOfMonth;$a++)
        {
            array_push($listfrom,$a);
        }

        for($i = 1 ; $i <= 12; $i++)
        { 
         array_push($listmnth,date("F",strtotime(date("Y")."-".$i."-01"))); 
        }


        for($m = 2019 ; $m <= 2030; $m++)
        { 
         array_push($listyear,$m); 
        }



        if(($opsidates == 'month')) {  
        





            $monthcurrent = $mnt+1 ; 
            $usagemonth = $yr.''.str_pad($monthcurrent,2,"0",STR_PAD_LEFT); 
            $this_month_table =  $tablenamelog.$usagemonth; 
            $this->LogTrxMonth = new DynamicDuoTables; 
            $this->LogTrxMonth->setConnection($this->conn); 
            $this->LogTrxMonth->setTable($this_month_table);
            $vMonth= str_pad($monthcurrent,2,"0",STR_PAD_LEFT);
 
$tables = DB::connection($this->conn)->select("SELECT table_schema,table_name, table_catalog FROM information_schema.tables where table_schema = 'gwbillerreport' and TABLE_NAME='".$tablenames.$usagemonth."'");

 // log::info("table ".$this_month_table." not exist".json_encode($tables));


if (empty($tables)) { 

        $this->viewData['listfrom'] = $listfrom;
        $this->viewData['listmnth'] = $listmnth;
        $this->viewData['listyear'] = $listyear;



        $this->viewData['today'] = date("Y-m-d");
        $this->viewData['months'] = date("F");
        $this->viewData['years'] = date("Y");
    return view('admin.report.report_empty', $this->viewData );
}
else
{

        $filterDateStart = '';
        $filterDateEnd = '';

        switch ($week) {
            case 1:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'01 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'07 23:59:59'.'\'';    
                break;
            case 2:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'08 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'15 23:59:59'.'\'';    
                break;
            case 3:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'16 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'23 23:59:59'.'\'';    
                break;
            case 4:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'24 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'31 23:59:59'.'\'';    
                break;
        } 

          $tablename=$tableselect;$searchname = $search;
          $tablename2=$tableselect2;$searchname2 = $search2;
 

         if(($search == '') && ($search2 == '') )
        {
log::info("query month 1");
log::info("query month none search");
/*"id","wl_id","wl_name","biller_id","biller_name","partner_id","partner_name","product_id","product_code","product_name","product_denom","nominal","info1","info2","info3","trx_date","trx_id","fee_value","wl_fee_value","so_fee_value","created_at","updated_at"*/

   $q = $this->LogTrxMonth
   ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')
   ->orderBy($this_month_table.'.created_at' , 'desc');
 

         }  else if(($search != '') && ($search2 == '') )
            {
log::info("query month 2");
log::info("query month search =".$search." tablename=".$tablename);

            $q = $this->LogTrxMonth 
              ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')->orderBy($this_month_table.'.created_at' , 'desc')
              ->where(function ($query) use ($tablename,$searchname) { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    })     
           ;
 

            }
            else if(($search == '') && ($search2 != '') )
            {
                log::info("query month search2 =".$search2);


                 $q = $this->LogTrxMonth            
             ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')->orderBy($this_month_table.'.created_at' , 'desc')
              ->where(function ($query) use ($tablename2,$searchname2) { 
                        $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                    })      
             ;
 



            }else if(($search != '') && ($search2 != '') )
            {
                log::info("query month search =".$search." search2 =".$search2);



                 $q = $this->LogTrxMonth              
             ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')->orderBy($this_month_table.'.created_at' , 'desc')
             ->where(function ($query) use ($tablename,$searchname) { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    })  
             ->where(function ($query) use ($tablename2,$searchname2) { 
                        $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                    });




 

              }
            }
        }
        else if(($opsidates == 'range')) {   
           
             $logtrxtable =  "log_trx_agent_".$thismonth;

            $fromdates = date("Y-m-").$fromdates;
            $todates = date("Y-m-").$todates; 

// log::info("data dates=".$fromdates." and ".$todates);

 
          $tablename=$tableselect;$searchname = $search;
          $tablename2=$tableselect2;$searchname2 = $search2;



$tables = DB::connection($this->conn)->select("SELECT table_schema,table_name, table_catalog FROM information_schema.tables where table_schema = 'gwbillerreport' and TABLE_NAME='".$logtrxtable."'");

 log::info("table ".$logtrxtable." not exist".json_encode($tables));   


if (empty($tables)) { 

        $this->viewData['listfrom'] = $listfrom;
        $this->viewData['listmnth'] = $listmnth;
        $this->viewData['listyear'] = $listyear;



        $this->viewData['today'] = date("Y-m-d");
        $this->viewData['months'] = date("F");
        $this->viewData['years'] = date("Y");
    return view('admin.report.report_empty', $this->viewData );
}
else
{


         if(($search == '') && ($search2 == '') )
        {
log::info("query range none search");
  

            $q =  $this->LogTrxthisMonth              
            ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')->orderBy($this_month_table.'.created_at' , 'desc')   
            ->whereRaw("trx_date >= ? AND trx_date <= ?", 
            array($fromdates." 00:00:00", $todates." 23:59:59"));



         }  else if(($search != '') && ($search2 == '') )
            {
log::info("go query range search =".$search);
            

            $q =  $this->LogTrxthisMonth
            ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')               
             ->whereRaw("trx_date >= ? AND trx_date <= ?", 
            array($fromdates." 00:00:00", $todates." 23:59:59"))             
            ->orderBy($this_month_table.'.created_at' , 'desc')
            ->where(function ($query) use ($tablename,$searchname) { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    });
 

            }
            else if(($search == '') && ($search2 != '') )
            {
                log::info("query range search2 =".$search2);
            $q =  $this->LogTrxthisMonth              
            ->where('biller_id','=','1')->where('trx_type','=','PURCHASE') 
            ->whereRaw("trx_date >= ? AND trx_date <= ?", 
            array($fromdates." 00:00:00", $todates." 23:59:59"))             
            ->orderBy($this_month_table.'.created_at' , 'desc')
            ->where(function ($query) use ($tablename2,$searchname2) { 
                        $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                    });
 




            }else if(($search != '') && ($search2 != '') )
            {
                log::info("query range search =".$search." search2 =".$search2); 


  $q =  $this->LogTrxthisMonth              
            ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')
             ->whereRaw("trx_date >= ? AND trx_date <= ?", 
            array($fromdates." 00:00:00", $todates." 23:59:59"))
            ->where(function ($query) use ($tablename,$searchname) { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    })  
            ->where(function ($query) use ($tablename2,$searchname2) { 
                        $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                    });

            }

           }


        }else {
 
            log::info("query 1");
            
            $q =  $this->LogTrxthisMonth 
            ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')             
             ->orderBy($this_month_table.'.created_at' , 'desc');
 
        }

 
        $this->tableSorter->setupPaging($q, $pg);
        $this->tableSorter->setWeek($week);
        $this->viewData['sorter'] = $this->tableSorter;
        $this->viewData['pg'] = $pg;
        $this->viewData['searchFor'] = $searchFor; 

        $this->viewData['mnt'] = $mnt; 
        $this->viewData['yr'] = $yr; 
        $this->viewData['opsidates'] = $opsidates;
        $this->viewData['fromdates'] = $fromdates;
        $this->viewData['todates'] = $todates;
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search; 
        $this->viewData['tableselect2'] = $tableselect2;
        $this->viewData['search2'] = $search2;


        $this->viewData['today'] = date("Y-m-d");
        $this->viewData['months'] = date("F");
        $this->viewData['years'] = date("Y");
        $this->viewData['listfrom'] = $listfrom;
        $this->viewData['listmnth'] = $listmnth;
        $this->viewData['listyear'] = $listyear;
        $this->viewData['week'] = $week;

 


        
        return view('admin.report.report_trx', $this->viewData );
 }
  
 function expreportamanah(Request $request){
   
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0); 

    $tableselect = $request->tableselect;
    $search =$request->input('search');

    $tableselect2 = $request->tableselect2;
    $search2 =$request->input('search2');
 
    $opsidates =$request->input('opsidates');

    $fromdates =$request->input('fromdates');
    $todates =$request->input('todates');

    $mnt =$request->input('mnt');
    $yr =$request->input('yr');

    $week= Input::get('week',1);

    $tablenamelog="gwbillerreport.trx_report_";
    $tablenames="trx_report_";
    $thismonth = date("Ym"); 
    $this_month_table =  $tablenamelog.$thismonth;
    //log::info(" datanya=".$this_month_table); 
    $this->LogTrxthisMonth = new DynamicDuoTables; 
    $this->LogTrxthisMonth->setConnection($this->conn); 
    $this->LogTrxthisMonth->setTable($this_month_table);




    $daterange = 0;
    $listfrom=[];
   
    $fields = [
            ['trx_date','Log DateTime'],
            ['trx_id', 'Transaction Id'],
            ['biller_name' , 'Biller'],
            ['partner_name', 'Partner'],
            ['product_id', 'Product Id'],
            ['product_code', 'Product Code'],
            ['product_name' , 'Product Name'],
            ['product_denom' , 'Denom'],
            ['nominal','Nominal'],
            ['info1','Transaction Info 1'],
            ['info2','Transaction Info 2'],
            ['info3','Transaction Info 3'],
            ['created_at','Created At'], 
            ['created_at','Balance Before'], 
            ['created_at','Balance After']

        ];  
 
 //,partner_name as "Partner Name"
$this->queryreport1 = 'trx_date as "Log DateTime",biller_name as "Biller",trx_id "Transaction Id",trx_type as "Transaction Type",product_id as "Product ID",product_code as "Product Code",product_name as "Product Name",product_denom as "Denom",nominal as "Nominal",info1 as "Info 1",info2 as "Info 2",info3 as "Info 3",balance_before as "Balance Before",balance_after as "Balance After"';


    $this->tableSorter->setupSorter(secure_url('/amanah/report'), $fields, $searchFor, $mnt, $yr,$fromdates,$todates, $tableselect, $search ,$daterange, $tableselect2, $search2, $opsidates );



        $dateString = date("Y-m-d");
        $listmnth=[];
        $listyear=[];
        $lastDateOfMonth = date("t", strtotime($dateString));
        for($a=1;$a<=$lastDateOfMonth;$a++)
        {
            array_push($listfrom,$a);
        }

        for($i = 1 ; $i <= 12; $i++)
        { 
         array_push($listmnth,date("F",strtotime(date("Y")."-".$i."-01"))); 
        }


        for($m = 2019 ; $m <= 2030; $m++)
        { 
         array_push($listyear,$m); 
        }



        if(($opsidates == 'month')) {  
        





            $monthcurrent = $mnt+1 ; 
            $usagemonth = $yr.''.str_pad($monthcurrent,2,"0",STR_PAD_LEFT); 
            $this_month_table =  $tablenamelog.$usagemonth; 
            $this->LogTrxMonth = new DynamicDuoTables; 
            $this->LogTrxMonth->setConnection($this->conn); 
            $this->LogTrxMonth->setTable($this_month_table);
            $vMonth= str_pad($monthcurrent,2,"0",STR_PAD_LEFT);
 
$tables = DB::connection($this->conn)->select("SELECT table_schema,table_name, table_catalog FROM information_schema.tables where table_schema = 'gwbillerreport' and TABLE_NAME='".$tablenames.$usagemonth."'");

 // log::info("table ".$this_month_table." not exist".json_encode($tables));


if (empty($tables)) { 

        $this->viewData['listfrom'] = $listfrom;
        $this->viewData['listmnth'] = $listmnth;
        $this->viewData['listyear'] = $listyear;



        $this->viewData['today'] = date("Y-m-d");
        $this->viewData['months'] = date("F");
        $this->viewData['years'] = date("Y");
    return view('admin.report.report_empty', $this->viewData );
}
else
{

        $filterDateStart = '';
        $filterDateEnd = '';

        switch ($week) {
            case 1:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'01 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'07 23:59:59'.'\'';    
                break;
            case 2:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'08 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'15 23:59:59'.'\'';    
                break;
            case 3:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'16 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'23 23:59:59'.'\'';    
                break;
            case 4:
                $filterDateStart = '\''.$yr.'-'.$vMonth.'-'.'24 00:00:00'.'\'';
                $filterDateEnd = '\''.$yr.'-'.$vMonth.'-'.'31 23:59:59'.'\'';    
                break;
        } 

          $tablename=$tableselect;$searchname = $search;
          $tablename2=$tableselect2;$searchname2 = $search2;
 

         if(($search == '') && ($search2 == '') )
        {
log::info("query month 1");
log::info("query month none search");
/*"id","wl_id","wl_name","biller_id","biller_name","partner_id","partner_name","product_id","product_code","product_name","product_denom","nominal","info1","info2","info3","trx_date","trx_id","fee_value","wl_fee_value","so_fee_value","created_at","updated_at"*/

   $q = $this->LogTrxMonth
   ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')->orderBy($this_month_table.'.created_at' , 'desc')
   ->selectRaw($this->queryreport1)
   ->cursor();
  

         }  else if(($search != '') && ($search2 == '') )
            {
log::info("query month 2");
log::info("query month search =".$search." tablename=".$tablename);

            $q = $this->LogTrxMonth 
              ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')->orderBy($this_month_table.'.created_at' , 'desc')
              ->where(function ($query) use ($tablename,$searchname) { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    })     
              ->selectRaw($this->queryreport1)
           ->cursor();
 

            }
            else if(($search == '') && ($search2 != '') )
            {
                log::info("query month search2 =".$search2);


                 $q = $this->LogTrxMonth            
             ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')->orderBy($this_month_table.'.created_at' , 'desc')
              ->where(function ($query) use ($tablename2,$searchname2) { 
                        $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                    })      
             ->selectRaw($this->queryreport1)
             ->cursor();
 



            }else if(($search != '') && ($search2 != '') )
            {
                log::info("query month search =".$search." search2 =".$search2);



                 $q = $this->LogTrxMonth              
             ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')->orderBy($this_month_table.'.created_at' , 'desc')
             ->where(function ($query) use ($tablename,$searchname) { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    })  
             ->where(function ($query) use ($tablename2,$searchname2) { 
                        $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                    })
             ->selectRaw($this->queryreport1)
             ->cursor(); 
 

              }
            }
        }
        else if(($opsidates == 'range')) {   
           
             $logtrxtable =  "log_trx_agent_".$thismonth;

            $fromdates = date("Y-m-").$fromdates;
            $todates = date("Y-m-").$todates; 

// log::info("data dates=".$fromdates." and ".$todates);

 
          $tablename=$tableselect;$searchname = $search;
          $tablename2=$tableselect2;$searchname2 = $search2;



$tables = DB::connection($this->conn)->select("SELECT table_schema,table_name, table_catalog FROM information_schema.tables where table_schema = 'gwbillerreport' and TABLE_NAME='".$logtrxtable."'");

 log::info("table ".$logtrxtable." not exist".json_encode($tables));   


if (empty($tables)) { 

        $this->viewData['listfrom'] = $listfrom;
        $this->viewData['listmnth'] = $listmnth;
        $this->viewData['listyear'] = $listyear;



        $this->viewData['today'] = date("Y-m-d");
        $this->viewData['months'] = date("F");
        $this->viewData['years'] = date("Y");
    return view('admin.report.report_empty', $this->viewData );
}
else
{


         if(($search == '') && ($search2 == '') )
        {
log::info("query range none search");
  

            $q =  $this->LogTrxthisMonth              
            ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')->orderBy($this_month_table.'.created_at' , 'desc')   
            ->whereRaw("trx_date >= ? AND trx_date <= ?", 
            array($fromdates." 00:00:00", $todates." 23:59:59"))
            ->selectRaw($this->queryreport1)
            ->cursor();



         }  else if(($search != '') && ($search2 == '') )
            {
log::info("go query range search =".$search);
            

            $q =  $this->LogTrxthisMonth
            ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')               
             ->whereRaw("trx_date >= ? AND trx_date <= ?", 
            array($fromdates." 00:00:00", $todates." 23:59:59"))             
            ->orderBy($this_month_table.'.created_at' , 'desc')
            ->where(function ($query) use ($tablename,$searchname) { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    })
           ->selectRaw($this->queryreport1)
            ->cursor();
 

            }
            else if(($search == '') && ($search2 != '') )
            {
                log::info("query range search2 =".$search2);
            $q =  $this->LogTrxthisMonth              
            ->where('biller_id','=','1')->where('trx_type','=','PURCHASE') 
            ->whereRaw("trx_date >= ? AND trx_date <= ?", 
            array($fromdates." 00:00:00", $todates." 23:59:59"))             
            ->orderBy($this_month_table.'.created_at' , 'desc')
            ->where(function ($query) use ($tablename2,$searchname2) { 
                        $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                    })
           ->selectRaw($this->queryreport1)
            ->cursor();
 


            }else if(($search != '') && ($search2 != '') )
            {
                log::info("query range search =".$search." search2 =".$search2); 


  $q =  $this->LogTrxthisMonth              
            ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')
             ->whereRaw("trx_date >= ? AND trx_date <= ?", 
            array($fromdates." 00:00:00", $todates." 23:59:59"))
            ->where(function ($query) use ($tablename,$searchname) { 
                        $query->where( $tablename, 'like', '%'.$searchname.'%');
                    })  
            ->where(function ($query) use ($tablename2,$searchname2) { 
                        $query->where( $tablename2, 'like', '%'.$searchname2.'%');
                    })
            ->selectRaw($this->queryreport1)
            ->cursor();

            }

           }


        }else {
 
            log::info("query 1"); 
            
            $q =  $this->LogTrxthisMonth 
            ->where('biller_id','=','1')->where('trx_type','=','PURCHASE')             
             ->orderBy($this_month_table.'.created_at' , 'desc');

 
        }
  




        $tx = microtime(true);
        $micro = sprintf("%06d",($tx - floor($tx)) * 1000000);
        $theTime = new \DateTime( date('Y-m-d H:i:s.'.$micro, $tx) );
        $theTimestamp = $theTime->format("YmdHisu");

        $filename = "Report_Bakrie_Amanah_".$theTimestamp.".xlsx"; 
  

        return (new FastExcel($this->usersGenerator($q)))->download($filename);



 }
  


 function usersGenerator($q) {
    foreach ($q as $user) {
        yield $user;
    }
}





}
