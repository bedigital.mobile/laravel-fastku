<?php

namespace App\Http\Controllers\admin\cms;



use Log;
use Config;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\BusinessLogic\HelpdeskBL;
use App\Core\Util\UtilNetwork;
use App\Core\Util\UtilPhone;
use App\Definition;
use App\BusinessLogic\Log\LogUserLoginBL;
use App\Jobs\InsertLogUserLoginJob;
use App\Http\Controllers\BDGBaseController;
use App\Models\admin\WhiteLabelAdminModel;
use App\Models\RunningText;
use App\BusinessLogic\Log\LogUserActivityBL;
use App\Jobs\InsertLogUserActivityJob; 
use GraphQL\Client;
use GraphQL\Exception\QueryError;
use GraphQL\Query; 
use GraphQL\RawObject; 
use GraphQL\Mutation; 
use GraphQL\QueryBuilder\QueryBuilder;



 class UserController extends BDGBaseController
{
     protected $ipClient;
     public function __construct() {
        parent::__construct();    
        // $this->conn = Config::get ( 'mysql_stg' );
        $this->conn = Config::get ( 'webconf.cms.connectiondata_prod' );
        $this->conn111 = Config::get ( 'webconf.cms.connection111' );
        $this->whitelabel = Config::get ( 'webconf.cms.whitelabel' );
        $this->whitelabelID = Config::get ( 'webconf.cms.whitelabelID' );
        $this->platform = Config::get ( 'webconf.cms.platform' );
        $this->AppID = Config::get ( 'webconf.cms.appid' );
        $this->hostcms = Config::get ( 'webconf.cms.hostcms' );
        $this->urlsite =  Config::get ( 'webconf.cms.urlsite' );  
        $this->helpdeskBL = new HelpdeskBL; 
        $this->apps = "DASHBOARD-FASTKU"; 
        $this->ipClient = UtilNetwork::getClientIP();
        $this->arraytokenuid=[];
        
    }
 

    function getlogin(Request $request) {
      $host = $request->getHttpHost();
      $this->helpdeskBL->verifiedHost($this->hostcms, $host);
       return view('admin.login.index');

    }

    function getcaptch(Request $request) {
      $ipku = $this->ipClient;
      log::info("UserController::getcaptch STARTS , ip=".$ipku); 
      $usr = $request->header('User-Agent').';ip='.$ipku;
      $cap = $this->helpdeskBL->getRequestcapthca($usr);
      echo $cap;

    }
     

public function getpaymentchannel(Request $request){ 
    log::info("UserController::getpayment - start");
    $ipku = $this->ipClient;
    $toke = $this->session->get('session_id'); 
    $useragent = $request->header('User-Agent').';ip='.$ipku;
    $result = $this->helpdeskBL->getshipmentarray($toke,$useragent,'payment');
    return $result; 

}

public function getchoicecat(Request $request){ 
    log::info("UserController::getpayment - start");
    $ipku = $this->ipClient;
    $toke = $this->session->get('session_id'); 
    $useragent = $request->header('User-Agent').';ip='.$ipku;
    $result = $this->helpdeskBL->getshipmentarray($toke,$useragent,'categories');
    return $result; 

}
public function getchoicepackage(Request $request){ 
    log::info("UserController::getpayment - start");
    $ipku = $this->ipClient;
    $toke = $this->session->get('session_id'); 
    $useragent = $request->header('User-Agent').';ip='.$ipku;
    $result = $this->helpdeskBL->getshipmentarray($toke,$useragent,'packaging');
    return $result; 

}
public function getshipmentid(Request $request){ 
    log::info("UserController::getshipmentid - start");
    $ipku = $this->ipClient;
    $toke = $this->session->get('session_id'); 
    $useragent = $request->header('User-Agent').';ip='.$ipku;
    $result = $this->helpdeskBL->getshipmentarray($toke,$useragent,'shipmentid');
    return $result; 

}
public function setkurirforshipment(Request $request){ 
    log::info("UserController::setkurirforshipment - start");
    $ipku = $this->ipClient;
    $toke = $this->session->get('session_id'); 
    $useragent = $request->header('User-Agent').';ip='.$ipku;

     $courier_id = Input::get('idkurir');
     $service_name = Input::get('kurirname');
       $datakurir = array(
                  'fullname' => '',
                  'province_id' => intval($courier_id),
                  'city_id'  => $service_name,
                  'subdistrict_id'  => '',
                  'address'  => '',
                  'phone_number'  => '',
                  'note'  => ''
              );
 

    $result = $this->helpdeskBL->getTrxArray($toke,$useragent,$datakurir,'setkurir');
    return $result; 

}



function http_digest_parse($txt)
      {
          // protect against missing data
          $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
          $data = array();
          $keys = implode('|', array_keys($needed_parts));

          preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

          foreach ($matches as $m) {
              $data[$m[1]] = $m[3] ? $m[3] : $m[4];
              unset($needed_parts[$m[1]]);
          }

          return $needed_parts ? false : $data;
      }
    function testlogin(Request $request) {

log::info("=========================================================");
log::info("testing LOGIN start");
log::info("=========================================================");

        $userName = $request->input('username');
        $plainpassword = $request->input('password');
        $userID = 0; 
        $recaptcha = $request->input('textcapthca');
        // $usr = $request->header('User-Agent');


        $ipku = $this->ipClient;        
        $usr = $request->header('User-Agent').';ip='.$ipku;
        log::info("UserController::testlogin STARTS , usr=".$usr); 

        log::info("UserController::testlogin data , userName = ".$userName." , plainpassword=".$plainpassword." , recaptcha=".$recaptcha); 



      $user = "testbca5";
      $username =strtoupper(substr($user,-3));
      $password = "AbcXyz123";
      $whitelabel = strtoupper(substr(base_convert("1234514444", 10, 36),-3));
      $whitelabelID = strtoupper(base_convert("1234514444", 10, 36));

      $res = md5($whitelabel.$password.$username);
    
      $wlID = "KEZXFW";
      $captcha = "AQT9";
      $platform = "WEB";
      $AppID = "1.0";


$clients = new Client(
    'https://fintech-stg.bedigital.co.id/elog/graph/v1/member',
    [
      'auth' => [$userName, $plainpassword, 'digest'],
    ],
    [ 
         'connect_timeout' => 5,
        'timeout' => 5,
        'headers' => [
            'WWW-Authenticate' => 'Digest',
            'User-Agent' => $usr,
            'realm' => 'BEDIGITAL',
            'Content-Type' => 'application/json',
        ], 
        'http_errors' => false
    ]
);

$gql = <<<QUERY
mutation { 
    login ( 
        whitelabel_id: "$whitelabelID",
        captcha: "$recaptcha",
        platform: "$platform",
        app_build_version: "$AppID"
    ) { session_id, user_id, authorization } 
}
QUERY;

// $mutation = (new Mutation('login'))
//     ->setArguments(['whitelabel_id' =>  new RawObject($whitelabelID)])
//     ->setArguments(['captcha' => new RawObject($recaptcha)])
//     ->setArguments(['platform' => $platform])
//     ->setArguments(['app_build_version' => $AppID]) 
//     //    ->setArguments(['companyObject' => new RawObject('{name: "Trial Company", employees: 200}')])
//   ->setSelectionSet(
//         [
//             'session_id',
//             'user_id',
//             'authorization',
//         ]
//     );


    /*
   mutation { 
    login ( 
        whitelabel_id: "KEZXFW",
        captcha: "NYUS",
        platform: "ANDROID",
        app_build_version: "1.0"
    ) { session_id, user_id, authorization } 
}
*/
// Run query to get results
// try {
//     $results = $client->runQuery($mutation);
// }
// catch (QueryError $exception) {

//      print_r($exception->getErrorDetails());
//     exit;
// }
// $results->reformatResults(true);
// log::info("getlocationfrompostcode - data=".json_encode($results));
// // return $results->getData()['login'];
try {
    // $name = readline('Enter pokemon name: ');
    // $resultsx = $clients->runQuery($mutation);
     log::info("processing"); 
     $results = $clients->runRawQuery($gql, true);
      $code = $clients->getStatusCode();

      log::info("res 1 ".$code);
      // log::info("res 2 ".$results->getData()['data']);
      

//         $results = $clients->runRawQuery($gql,true);
//  log::info("getlocationfrompostcode - data=".json_encode($results));
//  log::info("end... ");
//  var_dump($results->getResponseObject());
// var_dump($results->getData()->login);
// $results->reformatResults(true);
// print_r($results->getData()['login']);
    // {{ $row->message }}
    
// @endforeach

   // $results->reformatResults(true);
        // var_dump($resultsx->getResponseObject());
        // var_dump($resultsx->getData()->message);
     
// $results->reformatResults(true);
// print_r($results->getData()['login']); 
}

// catch(Exception $e)
// {
//     if ($e instanceof HttpException && $exception->getStatusCode()== 401)
//     {
//       
//     }
// }



catch (QueryError $exception) {
  // if ($exception->getStatusCode()== 401)
  // {
  //   log::info('you are not authorized');
  // }
    $info = $exception->getErrorDetails();
//     log::info("error ".json_encode($exception->getErrorDetails())); 
//      // log::info("error ".json_encode($exception->getErrorDetails())); 
//     // $info = $exception->getErrorDetails();
//     // return $info;
    // exit();
}
catch (\Exception $e) {
 $response = $e->getResponse();
    // $responseBodyAsString = $response->getBody()->getContents();
    $responseBodyAsString = $response->getHeaders();//->getContents();
    $info =  $responseBodyAsString;//"Got response 403";
    // There was another exception.
 // $body =  "Got response 402";
} 

return $info;

    } 


    function testguzzle(Request $request) {

  log::info("UserController::testguzzle"); 
  $user='085311223131';
  $password='test';
     $ipku = $this->ipClient;        
        $usr = $request->header('User-Agent').';ip='.$ipku;
        log::info("UserController::testlogin STARTS , usr=".$usr); 
          $cap = $this->helpdeskBL->loggraph($usr);           
          echo $cap;



    }
    function testingguzzle(Request $request) {
           // echo "test";
      
log::info("=================================================================");
log::info("testing guzzle start");
$realm = "realm";
$nonce = "nonce"; 
$opaque = "opaque";
// if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
//     header('HTTP/1.1 401 Unauthorized');
//     header(sprintf('WWW-Authenticate: Digest realm="%s", nonce="%s", opaque="%s"', $realm, $nonce, $opaque));
//     header('Content-Type: text/html');
//     echo '<p>You need to authenticate.</p>';
//     exit;
// }
      $user = "testbca5";
      $username =strtoupper(substr($user,-3));
      $password = "AbcXyz123";
      $whitelabel = strtoupper(substr(base_convert("1234514444", 10, 36),-3));

      // $result = md5($whitelabel.$password.$username);
      // echo "SALTPREFIX  =".$whitelabel;
      // echo "<br> SALTSUFFIX   =".$username;
      // echo "<br> password   =".$result;

$query = <<<GQL
  mutation { 
    login ( 
        whitelabel_id: "KEZXFW",
        captcha: "3IKG",
        platform: "ANDROID",
        app_build_version: "1.0"
    ) { session_id, user_id, authorization } 
}
GQL;
$graphqlEndpoint = 'https://fintech-stg.bedigital.co.id/elog/graph/v1/member';
$client = new \GuzzleHttp\Client();

$response = $client->request('POST', $graphqlEndpoint, [
  'auth' => [$user, $password, 'digest'],
  'headers' => [
    'WWW-Authenticate' => 'Digest',
    'User-Agent' => 'testing/1.0',
    'realm' => 'BEDIGITAL',
  ],
  'json' => [
    'query' => $query
  ],
]);

$json = $response->getBody()->getContents();
$body = json_decode($json);
$data = $body->data;

// var_dump($body);

// $client = new \GuzzleHttp\Client();
// $response = $client->request('POST', 'https://fintech-stg.bedigital.co.id/elog/graph/v1/member',
// [
//                     'auth'    => [
//                         $user,
//                         $password
//                     ],
//                     'headers' => [
//                         'Realm : BEDIGITAL'
//                     ]
// ]
// );
// mutation { 
//     login ( 
//         whitelabel_id: "KEZXFW",
//         captcha: "NYUS",
//         platform: "ANDROID",
//         app_build_version: "1.0"
//     ) { session_id, user_id, authorization } 
// }

// $response = Guzzle::post('https://fintech-stg.bedigital.co.id/elog/graph/v1/member', array(
//     'headers' => array('Realm' => 'BEDIGITAL'),
//     'body'    => array('Test' => '123'),
//     'auth'    => array('username' => $user , 'password' => $password),
//     'timeout' => 10
// ));


// echo $response->getStatusCode(); // 200
// echo $response->getHeaderLine('content-type'); // 'application/json; charset=utf8'
// echo $response->getBody(); // '{"id": 1420053, "name": "guzzle", ...}'




//  $client = new GuzzleHttp\Client();

 
// $result = $client->request(
//         'POST',
//         'https://fintech-stg.bedigital.co.id/elog/graph/v1/member', [
//             'verify' => false,
//             'auth' => [$user, $password, 'digest'] 
//         ]);


     

      // Function to parse the http auth header.
      // From http://www.php.net/manual/en/features.http-auth.php
      

 // $digest_values = $this->http_digest_parse($_ENV['HTTP_AUTHORIZATION']);

      // var_dump($result);

// fastku
        //-- graphql -----------------------------------------------------------------------//
//         whitelabel_id = 1234514444 ==> base36 = KEZXFW
// username = testbca5
// SALTPREFIX = XFW
// SALTSUFFIX = CA5
// PasswordPlain = AbcXyz123
// HASIL encryption Password = MD5 (  SALTPREFIX + PasswordPlain + SALTSUFFIX  )
//                                               = MD5 ( "XFWAbcXyz123CA5" )
//                                               = edbdd8ecf34e4bba5779c2d29cd59ecf

// username = 6281380298484
// SALTPREFIX = XFW
// SALTSUFFIX = 484
// PasswordPlain = 123456
// HASIL encryption Password = MD5 (  SALTPREFIX + PasswordPlain + SALTSUFFIX  )
//                                               = MD5 ( "XFW123456484" )
//                                               = 36c331040a6aaabc12ceb70875720a82





       // return view('admin.trx.index', $this->viewData);

    } function getkirim(Request $request) {
           
       return view('admin.trx.index', $this->viewData);

    }
    function getpengaturan(Request $request) {
           
       return view('admin.trx.pengaturan', $this->viewData);

    } function getpengaturanalm(Request $request) {
           
       return view('admin.trx.pengaturanalamat', $this->viewData);

    }


 function userlogin(Request $request) {
        $userName = $request->input('username');
        $plainpassword = $request->input('password');
        $userID = 0; 

        $recaptcha = $request->input('g-recaptcha-response');
        $tokenuid = $request->input('uid');
        $tokenotp = $request->input('otp');
        log::info("UserController::testlogin data , userName = ".$userName." , plainpassword=".$plainpassword." , recaptcha=".$recaptcha); 

        $tokenuidfinal='';
        $userID=0;
        $msg="Admin White Label";
        $this->userAgent = $request->header('User-Agent'); 
        // $password = $request->input('xpwd');
        
        // $this->userAgent = $request->header('User-Agent');
        $q = WhiteLabelAdminModel::where('username', $userName)
        ->where('web_app', 'GROSIRUN')
                ->first();
        // log::info("username ".json_encode($q));
                // log::info(" data list-agen-controller".json_encode($q));

  // if(!is_null($recaptcha) || (!empty($recaptcha)))
  //      {
 
        if ($q != null) {
          if ($q->supplier_id != null) {


                $this->arraytokenuid = $this->helpdeskBL->gettoken($q->white_label_id,$q->id); 
                $this->arrayIpList = $this->helpdeskBL->getipwhitelist($q->id); 
                $ipwhitelistAccess = in_array($this->ipClient, $this->arrayIpList)?"grandedIp":"NotgrandedIp";


// $this->ipClient
        // log::info("array_token wl=".$q->white_label_id." id=".$q->id." ,".json_encode( $this->arraytokenuid));
        log::info("array_ip, =".json_encode( $this->arrayIpList));
                   

          if ( $ipwhitelistAccess == "grandedIp" || $q->ip_allowed == 'Y' ) {
 

            if ($q->active == 'Y') {

                 if($q->password_type == 'MD5'){
                        $password = md5($plainpassword);
                    }else if($q->password_type == 'PLAIN'){
                        $password =  $plainpassword;
                    }


                if (strcmp($password, $q->password_enc) == 0) {
                    $token = time();
                    
                     $q->token = $token;
                     $q->save();

                     $supplier_name = $this->helpdeskBL->getsuppliername($q->supplier_id);
                     // $supplier_name = '';//$this->helpdeskBL->getMaAgentList($q->master_agent_id);

log::info("supplier_name = ".json_encode($supplier_name));

                    $data = [
                        'id' => $q->id,
                        'white_label_id' => $q->white_label_id,
                        'username' => $q->username,
                        'fullname' => $q->fullname,
                        'email' => $q->email,
                        'group' => $q->group,
                        'token' => $token,
                        'supplier_id' => $q->supplier_id,
                        // 'master_agent_list_agent' => $master_agent_agent,
                        'supplier_name' => $supplier_name 
                    ];           
                    
                    $userID =  $q->id;

                        log::info("data session".json_encode($data));

                    if(!empty($tokenotp) && !is_null($tokenotp))
                    {

                      

                    //      // $objBL = new CmsBL();
                         if($this->helpdeskBL->verifyDeviceToken( $tokenotp,$q->white_label_id,$q->id,$tokenuid))
                         {
                                $this->putQueueLog( $userID, $userName, "Sukses" , "0" );
                                 $this->session->save($data);  
                                  // Log::info("SUKSES phase 1");
                                 return response()->json($this->notif->build());
                    //         // Log::info("SessionLogic::setData tokenuid=".$tokenuid);
                           
                    //         // $cmsbl = new CmsBL();
                    //         $this->helpdeskBL->sendmaillogin($q->email,$q->fullname,$q->username,$this->userAgent,$msg);

                            
                         }
                         else
                         {
                                $this->notif->addMessage('OTP yang anda masukkan salah'); return response()->json($this->notif->build());
                                $this->putQueueLog( $userID, $userName, "OTP yang anda masukkan salah" , "7" );
                         }
                        

                    }
                   

                  // Log::info("TokenArray==> HASIL=".(in_array($tokenuid, $this->arraytokenuid)?"true":"false"));
                            $result_token=in_array($tokenuid, $this->arraytokenuid)?"allowed":"false";
                            // log::info("result token = ".$result_token." is_allowd= ".$q->is_allowed." array=".json_encode($this->arraytokenuid));
 
                         if ( ($result_token == "allowed") || ($q->is_allowed == 'Y')) {
                                $this->putQueueLog( $userID, $userName, "Sukses" , "0" );
                                $this->session->save($data);                     
                                 // Log::info("SUKSES phase 2");
                            //     // $cmsbl = new CmsBL();
                            //     $this->helpdeskBL->sendmaillogin($q->email,$q->fullname,$q->username,$this->userAgent,$msg);
                                 return response()->json($this->notif->build());

             
                              }
                       else { 

                                    //     // $objBL = new CmsBL();
                                        $core = new UtilPhone();
                                        // $objBL->createDeviceToken($q->white_label_id,$q->id,$tokenuid);
                                        if(!empty($q->phone_number) && $core->isPhoneValid($q->phone_number))
                                        {
                                            if($this->helpdeskBL->sendSMSDeviceToken( $q->phone_number,$tokenuid,$q->white_label_id,$q->id, $userName ))
                                           {
                                                $this->notif->addMessage('getotp'); return response()->json($this->notif->build());

                                           }
                                           else
                                           {    
                                                $this->putQueueLog( $userID, $userName, "error sending OTP" , "5" );
                                                $this->notif->addMessage('error sending OTP'); return response()->json($this->notif->build());

                                           }
                                        }
                                        else{
                                                $this->putQueueLog( $userID, $userName, "error, invalid Phone Numbe" , "6" );
                                                $this->notif->addMessage('error, invalid Phone Number<br> please contact your admin to validate your phone number'); return response()->json($this->notif->build());

                                        }
                 
                            }
                }
                 else {
                     $this->putQueueLog( $userID, $userName, "Invalid User name or password" , "3" );

                    $this->notif->addMessage('Invalid User name or password '); return response()->json($this->notif->build());
                }
           

           } else {
                $this->putQueueLog( $userID, $userName, "Your user is not active. Please contact administrator" , "4" );
                $this->notif->addMessage('Your user is not active. Please contact administrator.'); return response()->json($this->notif->build());
            }

             } else {
                $this->putQueueLog( $userID, $userName, "Your IP is not Allowed. Please contact administrator" , "99" );
                $this->notif->addMessage('Your IP is not Allowed. Please contact administrator.<br>  Your IP is '.$this->ipClient); return response()->json($this->notif->build());
            }   
          } else {
                $this->putQueueLog( $userID, $userName, "Your Dont Have Supplier. Please contact administrator" , "5" );
                $this->notif->addMessage('Your Dont Have Supplier. Please contact administrator'); return response()->json($this->notif->build());
            } 
            
        
      } else {
            $this->putQueueLog( $userID, $userName, "Invalid User name or password" , "3" );

            $this->notif->addMessage('Invalid User name or password'); return response()->json($this->notif->build());
        }
    // } else  {
    //     $this->notif->addMessage('Are you a human'); return response()->json($this->notif->build());
    // }
      
    }


    function lamalogin(Request $request) {
        
        $host = $request->getHttpHost();
        log::info("login STart".$host);
        $userName = $request->input('username');
        $plainpassword = $request->input('password');
        $userID = 0; 
        $recaptcha = $request->input('textcapthca');
        // $recaptcha = $request->input('g-recaptcha-response');
        $tokenuid = $request->input('uid');
        $tokenotp = $request->input('otp');
        
        $user = "testbca5";
      $usernamesf =strtoupper(substr($userName,-3));
      
      $whitelabel = strtoupper(substr(base_convert($this->whitelabelID, 10, 36),-3));
      $whitelabelID = strtoupper(base_convert($this->whitelabelID, 10, 36));

      // $result = md5($whitelabel.$password.$username);
      // echo "<img src=\"https://fintech-stg.bedigital.co.id/elog/graph/v1/member/captcha/kezxfw/image.png\">";
      // echo "<br> SALTSUFFIX   =".$username;
      // echo "<br> password   =".$result;
      
      $wlID = "KEZXFW";
      $captcha = "AQT9";
      $platform = "WEB";
      $AppID = "1.0";



        $tokenuidfinal='';
        $userID=0;
        $msg="Admin White Label";
        $this->userAgent = $request->header('User-Agent'); 

 

      $resultdarisufix = md5($whitelabel.$plainpassword.$username);
      // echo "SALTPREFIX  =".$whitelabel;
      // echo "<br> SALTSUFFIX   =".$username;
      // echo "<br> password   =".$result;



$clients = new Client(
    'https://fintech-stg.bedigital.co.id/elog/graph/v1/member',
    [ 
         'auth' => [$userName, $plainpassword, 'digest'],
        'headers' => [
            'WWW-Authenticate' => 'Digest',
            'User-Agent' => 'webadminfastku/1.0',
            'realm' => 'BEDIGITAL',
            'Content-Type' => 'application/json',
        ],
       
       
    ]
);

$gql = <<<QUERY
mutation { 
    login ( 
        whitelabel_id: "$whitelabelID",
        captcha: "$recaptcha",
        platform: "$platform",
        app_build_version: "$AppID"
    ) { session_id, user_id, authorization } 
}
QUERY;

try {
    
        $results = $clients->runRawQuery($gql);


}
catch (QueryError $exception) {
    print_r($exception->getErrorDetails());
    exit;
}
// print_r($results->getData());
   $results->reformatResults(true);
     log::info("login - data=".json_encode($results));





        // $password = $request->input('xpwd');
        // $this->userAgent = $request->header('User-Agent');
        $q = WhiteLabelAdminModel::where('username', $userName)
        ->where('web_app', 'KURIRKU')
                ->first();
        // log::info("username ".json_encode($q)); 
                // log::info(" data list-agen-controller".json_encode($q));

  // if(!is_null($recaptcha) || (!empty($recaptcha)))
  //      {
 
        if ($q != null) {
          // if ($q->master_agent_id != null) {


                $this->arraytokenuid = $this->helpdeskBL->gettoken($q->white_label_id,$q->id); 
                $this->arrayIpList = $this->helpdeskBL->getipwhitelist($q->id); 
                $ipwhitelistAccess = in_array($this->ipClient, $this->arrayIpList)?"grandedIp":"NotgrandedIp";


// $this->ipClient
        // log::info("array_token wl=".$q->white_label_id." id=".$q->id." ,".json_encode( $this->arraytokenuid));
        log::info("array_ip, =".json_encode( $this->arrayIpList));
                   

          if ( $ipwhitelistAccess == "grandedIp" || $q->ip_allowed == 'Y' ) {
 

            if ($q->active == 'Y') {

                 if($q->password_type == 'MD5'){
                        $password = md5($plainpassword);
                    }else if($q->password_type == 'PLAIN'){
                        $password =  $plainpassword;
                    }


                if (strcmp($password, $q->password_enc) == 0) {
                    $token = time();
                    
                     $q->token = $token;
                     $q->save();

                     $master_agent_fullname = $this->helpdeskBL->getFullnameMa($q->master_agent_id);

                    $data = [
                        'id' => $q->id,
                        'white_label_id' => $q->white_label_id,
                        'username' => $q->username,
                        'fullname' => $q->fullname,
                        'email' => $q->email,
                        'group' => $q->group,
                        'token' => $token,
                        'master_agent_id' => $q->master_agent_id,
                        'master_agent_fullname' => $master_agent_fullname,
                        'appname' => $this->apps
                    ];           
                    
                    $userID =  $q->id;

                        log::info("data session".json_encode($data));

                    if(!empty($tokenotp) && !is_null($tokenotp))
                    {

                      

                    //      // $objBL = new CmsBL();
                         if($this->helpdeskBL->verifyDeviceToken( $tokenotp,$q->white_label_id,$q->id,$tokenuid))
                         {
                                $this->putQueueLog( $userID, $userName, "Sukses" , "0" );
                                 $this->session->save($data);  
                                  // Log::info("SUKSES phase 1");
                                 return response()->json($this->notif->build());
                    //         // Log::info("SessionLogic::setData tokenuid=".$tokenuid);
                           
                    //         // $cmsbl = new CmsBL();
                    //         $this->helpdeskBL->sendmaillogin($q->email,$q->fullname,$q->username,$this->userAgent,$msg);

                            
                         }
                         else
                         {
                                $this->notif->addMessage('OTP yang anda masukkan salah'); return response()->json($this->notif->build());
                                $this->putQueueLog( $userID, $userName, "OTP yang anda masukkan salah" , "7" );
                         }
                        

                    }
                   

                  // Log::info("TokenArray==> HASIL=".(in_array($tokenuid, $this->arraytokenuid)?"true":"false"));
                            $result_token=in_array($tokenuid, $this->arraytokenuid)?"allowed":"false";
                            // log::info("result token = ".$result_token." is_allowd= ".$q->is_allowed." array=".json_encode($this->arraytokenuid));
 
                         if ( ($result_token == "allowed") || ($q->is_allowed == 'Y')) {
                                $this->putQueueLog( $userID, $userName, "Sukses" , "0" );
                                $this->session->save($data);                     
                                 // Log::info("SUKSES phase 2");
                            //     // $cmsbl = new CmsBL();
                            //     $this->helpdeskBL->sendmaillogin($q->email,$q->fullname,$q->username,$this->userAgent,$msg);
                                 return response()->json($this->notif->build());

             
                              }
                       else { 

                                    //     // $objBL = new CmsBL();
                                        $core = new UtilPhone();
                                        // $objBL->createDeviceToken($q->white_label_id,$q->id,$tokenuid);
                                        if(!empty($q->phone_number) && $core->isPhoneValid($q->phone_number))
                                        {
                                            if($this->helpdeskBL->sendSMSDeviceToken( $q->phone_number,$tokenuid,$q->white_label_id,$q->id, $userName ))
                                           {
                                                $this->notif->addMessage('getotp'); return response()->json($this->notif->build());

                                           }
                                           else
                                           {    
                                                $this->putQueueLog( $userID, $userName, "error sending OTP" , "5" );
                                                $this->notif->addMessage('error sending OTP'); return response()->json($this->notif->build());

                                           }
                                        }
                                        else{
                                                $this->putQueueLog( $userID, $userName, "error, invalid Phone Numbe" , "6" );
                                                $this->notif->addMessage('error, invalid Phone Number<br> please contact your admin to validate your phone number'); return response()->json($this->notif->build());

                                        }
                 
                            }
                }
                 else {
                     $this->putQueueLog( $userID, $userName, "Invalid User name or password" , "3" );

                    $this->notif->addMessage('Invalid User name or password '); return response()->json($this->notif->build());
                }
           

           } else {
                $this->putQueueLog( $userID, $userName, "Your user is not active. Please contact administrator" , "4" );
                $this->notif->addMessage('Your user is not active. Please contact administrator.'); return response()->json($this->notif->build());
            }

             } else {
                $this->putQueueLog( $userID, $userName, "Your IP is not Allowed. Please contact administrator" , "99" );
                $this->notif->addMessage('Your IP is not Allowed. Please contact administrator.<br>  Your IP is '.$this->ipClient); return response()->json($this->notif->build());
            } 
            
        // } else {
        //     $this->putQueueLog( $userID, $userName, "Invalid Master Agent" , "3" );

        //     $this->notif->addMessage('Invalid Master Agent, Please contact administrator'); return response()->json($this->notif->build());
        // }  
      } else {
            $this->putQueueLog( $userID, $userName, "Invalid User name or password" , "3" );

            $this->notif->addMessage('Invalid User name or password'); return response()->json($this->notif->build());
        }
    // } else  {
    //     $this->notif->addMessage('Are you a human'); return response()->json($this->notif->build());
    // }
      
    }
 

    function changePasswd(Request $request) {
        return view('admin.login.password', $this->viewData);
 
    } 


     function dash2(Request $request) {
Log::info("UserCOntroller::dash2");
Log::info("UserCOntroller::dash2 START DASHBOARD empty_content");
  return view('admin.index', $this->viewData);
     }

     

     function dash(Request $request) {
      $this->viewData['sumtrxma']=[];
       $this->viewData['totlallagen'] = $this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'));
       $this->viewData['actagen'] = $this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'),'ACTIVE');
       $this->viewData['regagen'] = $this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'),'REGISTERING');
      $this->viewData['ungagen'] = $this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'),'UNAPPROVED');
  
      /*
      $this->viewData['totlallagen'] = ''; // $this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'));
      $this->viewData['actagen'] = ''; //$this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'),'ACTIVE');
      $this->viewData['regagen'] = ''; //$this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'),'REGISTERING');
      $this->viewData['ungagen'] = ''; //$this->helpdeskBL->listAgentbyUplinesums($this->session->get('master_agent_id'),'UNAPPROVED');
      */

       
for($a=1;$a<=12;$a++)
  {
     $sumtrxma = $this->helpdeskBL->listSumTrxMa($this->session->get('master_agent_id'),$a);
     array_push($this->viewData['sumtrxma'],$sumtrxma); 

  }
 
              return view('admin.dashboard', $this->viewData);
 
    }


function infoTarif(Request $request) {
  $this->viewData['x']='';
  return view('admin.info_tarif', $this->viewData);
}


function savetrxkurir(Request $request) {
  log::info("=================================================================");
  log::info("UserController::savetrxkurir - start");
  log::info("UserController::getpayment - start");
    $ipku = $this->ipClient;
    $toke = $this->session->get('session_id'); 
    $useragent = $request->header('User-Agent').';ip='.$ipku;

    $namapengirim = $request->post('namapengirim');
    $alamatpengirim = $request->input('alamat');  
    $provid = $request->input('selectprov');  
    $cityid = $request->input('selectkota');  
    $villid = $request->input('selectkec');  
    $notes = $request->input('detail');  


    $data = array(
              'fullname' => $namapengirim,
              'province_id'  => $provid,
              'city_id'   => $cityid,
              'village_id'  => $villid,
              'address'  => $alamatpengirim,
              'postal_code'  => $postal_code,
              'note'  => $notes
          );


    $result = $this->helpdeskBL->getTrxArray($toke,$useragent,$data,'shipment');
    return $result; 




   
$version = "1.0";
$key = "abcde";
$tokenJWT = $this->helpdeskBL->gettokenjwt($toke);

log::info("token jwt =".$tokenJWT);



$clients = new Client(
    'https://fintech-stg.bedigital.co.id/elog/graph/v1/member/me',
    [],
    [ 
        
        'headers' => [
            'VERSION' => $version,
             'KEY' => $key,
             'TOKEN' => $tokenJWT,
            'Content-Type' => 'application/json',
        ],
       
       
    ]
);
  
 $gqltrx = (new Query('draftShipment'))
    ->setSelectionSet(
        [
            'choice_payment_channels' 
        ]
    );
 

try {
 
     $results = $clients->runQuery($gqlpayment,true);
  


}
catch (QueryError $exception) {
    print_r($exception->getErrorDetails());
    exit;
}
// print_r($results->getData()['profile']);

$resultnya = $results->getData()['draftShipment'];
         log::info("hasil =".json_encode($results->getData()['draftShipment']));
   
 return $resultnya;
 }



function getkurirfromcity(Request $request) {
  log::info("getkurirfrompostcode - START");
  // Create Client object to contact the GraphQL endpoint
  $client = new Client(
      $this->urlsite.'elog/graph/v1/shipment',
      []  // Replace with array of extra headers to be sent with request for auth or other purposes
  );
  $ipku = $this->ipClient;
  $toke = $this->session->get('session_id'); 
  $useragent = $request->header('User-Agent').';ip='.$ipku;
  $provinceId = Input::get('idprov_from');
  $provinceId2 = Input::get('idprov_to');
  $cityId = Input::get('city_from');
  $cityId2 = Input::get('city_to');
  $kecId = Input::get('kec_from');
  $kecId2 = Input::get('kec_to');

  $namapengirim = Input::get('namapengirim');
  $alamatpengirim = Input::get('alamatpengirim');
  $postal_code = Input::get('postal_code');
  $phone_no = Input::get('hppengirim');
  $notes = Input::get('notes'); 


  $namapenerima = Input::get('namapenerima');
  $alamatpenerima = Input::get('alamatpenerima');
  $postal_code2 = Input::get('postal_code2');
  $phone_no2 = Input::get('hppenerima');
  $notes2 = Input::get('notes2'); 

  $spl= $builder = (new QueryBuilder('rates'))
      ->setArgument('origin', new RawObject('{province_id: "'.$provinceId.'" , city_id : "'.$cityId.'" , village_id : "'.$kecId.'"  }'))
      ->setArgument('destination', new RawObject('{province_id: "'.$provinceId2.'" , city_id : "'.$cityId2.'" , village_id : "'.$kecId2.'"  }')) 
      ->selectField('service_types');
   
      $data = array(
                  'fullname' => $namapengirim,
                  'province_id'  => $provinceId,
                  'city_id'   => $cityId,
                  'subdistrict_id'  => $kecId,
                  'address'  => $alamatpengirim,
                  'phone_number'  => $phone_no,
                  'note'  => $notes
              );

      $datareceive = array(
                  'fullname' => $namapenerima,
                  'province_id'  => $provinceId2,
                  'city_id'   => $cityId2,
                  'subdistrict_id'  => $kecId2,
                  'address'  => $alamatpenerima,
                  'phone_number'  => $phone_no2,
                  'note'  => $notes2
              );

   
        $this->helpdeskBL->getTrxArray($toke,$useragent,$data,'editpickup');
        $this->helpdeskBL->getTrxArray($toke,$useragent,$datareceive,'editreceive');

  // Run query to get results
  try {
      

      

     
      $results = $client->runQuery($spl);
   }
  catch (QueryError $exception) {

       print_r($exception->getErrorDetails());
      exit;
  }
   
  $results->reformatResults(true);
  log::info("getlocationfrompostcode - data=".json_encode($results->getData()['rates']));
  return $results->getData()['rates'];

     }

 function getlocationfrompostcode(Request $request) {
log::info("getlocationfrompostcode - START");
// Create Client object to contact the GraphQL endpoint
$client = new Client(
    'https://fintech-stg.bedigital.co.id/elog/graph/geoname',
    []  // Replace with array of extra headers to be sent with request for auth or other purposes
);
  
        $postCode = Input::get('postalcode');
log::info("getlocationfrompostcode - postCode=".$postCode);

// $postCode = '15810';
// $postCodeDest = '12940';
// $provinceId = '31';
// $cityName = 'Jakarta Barat';
// $addrName = 'Jawa Tengah, Sragen';

$spl= $builder = (new QueryBuilder('postal_code'))
    // ->setArgument('origin', new RawObject('{province_id: "'.$provinceId.'" , city_name : "'.$cityName.'"  }'))
    ->setArgument('code ', $postCode)
    // ->setArgument(['code' => $postCode])
    // ->setArgument('destination', new RawObject('{postal_code: "'.$postCodeDest.'" }'))
    // ->setArgument('destination', new RawObject('{address: "'.$addrName.'" }'))
    ->selectField('city_id')
    ->selectField('city_name')
    ->selectField('province_id')
    ->selectField('province_name');

// $gql = $builder->getQuery();
// print_r($gql);
    /*
    query {
    rates (
        origin: {
            province_id: "31", 
            city_name: "Jakarta Barat"
        },
        destination: {
            province_id: "11", 
            city_name: "Aceh Barat"
        }
    ) 
    { couriers }
}

query {
    rates (
        origin: {
            province_id: "31", 
            city_name: "Jakarta Barat"
        },
        destination: {
            address: "Jawa Tengah, Sragen"
        }
    ) 
    { services }
}


*/
 
// Run query to get results
try {
    $results = $client->runQuery($spl);
}
catch (QueryError $exception) {

     print_r($exception->getErrorDetails());
    exit;
}

// Display original response from endpoint
// var_dump($results->getResponseObject());

// Display part of the returned results of the object
// var_dump($results->getData()->rates);

// Reformat the results to an array and get the results of part of the array
$results->reformatResults(true);
return $results->getData()['postal_code'];
     }


 function getrefreshcaptcha(Request $request) {
log::info("getrefreshcaptcha - START");
 $client = new Client(
    'https://fintech-stg.bedigital.co.id/elog/graph/v1/member',
    [] 
);  
        
log::info("getrefreshcaptcha - WL= ".$this->whitelabel);

// mutation {
//     refreshCaptcha ( whitelabel_id:"KEZXFW" )
//     { code, message }
// }

$mutation = (new Mutation('refreshCaptcha'))
    // ->setArguments(whitelabel_id: "'.$this->whitelabel.'" }')])
          ->setArguments(['whitelabel_id' => "'.$this->whitelabel.'"])
    ->setSelectionSet(
        [
            'code',
            'message',
         ]
    );
$results = $client->runQuery($mutation);
   log::info("Result data = ".json_encode($results));
// Reformat the results to an array and get the results of part of the array
$results->reformatResults(true);
return $results->getData('refreshCaptcha');

     }

 function cekuseraktif(Request $request) {
log::info("cekuser - START");
// Create Client object to contact the GraphQL endpoint
$client = new Client(
    'https://fintech-stg.bedigital.co.id/elog/graph/v1/member',
    []  // Replace with array of extra headers to be sent with request for auth or other purposes
);

   /*
    query {
    user (
        whitelabel_id: "KEZXFW",
        username: "6281380298484"
    ) 
        { username, user_id, user_status }
}

*/

// Create the GraphQL query
$gql2 = $builder = (new QueryBuilder('user'))
    ->setArgument('whitelabel_id', 'KEZXFW')
    ->setArgument('username', '6281380298484')
    ->selectField('username')
    ->selectField('user_id')
    ->selectField('user_status');
    
// Run query to get results
try {
    $results = $client->runQuery($gql2);
}
catch (QueryError $exception) {

    // Catch query error and desplay error details
    print_r($exception->getErrorDetails());
    exit;
}

// Display original response from endpoint
var_dump($results->getResponseObject());

// Display part of the returned results of the object
var_dump($results->getData()->user);

// Reformat the results to an array and get the results of part of the array
$results->reformatResults(true);
print_r($results->getData()['user']);

     }


    function saveRunningText(Request $request) { 
        Log::info("START Running Text" );
        $opsi = $request->input('opsi');
        $owner_type = $request->input('sendto');
        $agentid = $request->input('agentid');
        $starts = $request->input('min');
        $stops = $request->input('max');
        $text_message = $request->input('rtext');
       
 
        try {

        if ($starts == '') { $this->notif->addMessage('start date is required'); }
        if ($stops == '') { $this->notif->addMessage('End date is required'); } 
        if ($text_message == '') { $this->notif->addMessage('Running Text is required'); }
                  $q = new RunningText(); 
                  $q->setConnection($this->conn); 

                  if ($this->notif->code == \App\Http\Helpers\NotificationHelper::CODE_OK) {
                    try {
                         
                          if($owner_type == "MASTER_AGENT")
                          {
                             $userid = $this->session->get('master_agent_id'); 
                          }
                          else
                          {
                            $userid = $agentid;
                          }
                          if($opsi == "input")
                                  {
                                   
                                    $q->white_label_id = $this->whitelabel;
                                    $q->owner_type =  $owner_type;//'MASTER_AGENT';
                                    $q->owner_id =  $userid ;
                                    $q->show_date_start = $starts;
                                    $q->show_date_stop = $stops;
                                    $q->text_message = $text_message; 
                                    $q->active = 'Y'; 
                                    $q->created_at = date('Y-m-d H:i:s');
                                    $q->save(); 
                               
                                  }
                                  else
                                  {
                                    $q= RunningText::find($userid); 
                                    $q->password_enc = md5($pspwd);
                                    $q->password_type = 'md5';
                                    $q->save(); 

                                  }


                          } catch (Exception $e) {
                            // DB::rollBack();
                            $this->notif->addMessage($e->getMessage());
                          }
                    }
            
            




        } catch ( Exception $e ) {
        Log::info("UserBL::saveRunningText exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        
        return response()->json($this->notif->build());
    }  

    function delRunningText(Request $request) { 
        
        $idnya = $request->input('id'); 
       // Log::info("Delete Text id=".$idnya );
 
        try {

        // if ($idnya == '') { $this->notif->addMessage('id is required'); } 
                 

                  if ($this->notif->code == \App\Http\Helpers\NotificationHelper::CODE_OK) {
                    // try {
                        

                         // Log::info("start to Delete  " );
                                    // $q = new RunningText(); 
                                    // $q->setConnection($this->conn); 
                                    // $q->where('id',$idnya); 
                                    // $q->active = "N";  
                                    // $q->save();


                                              $BankAccounts2 = new  RunningText;
                                              $BankAccounts2->setConnection($this->conn); 
                                              $B2 = $BankAccounts2
                                              ->where('id', '=', $idnya) 
                                              ->update([
                                              'active' => 'N' ,
                                              'updated_at' => date('Y-m-d H:i:s') 
                                              ]); 




                        // Log::info("end Delete".json_encode($B2));
                                  

                          // } catch (Exception $e) {
                          //   // DB::rollBack();
                          //   $this->notif->addMessage($e->getMessage());
                          // }
                    }
            
            




        } catch ( Exception $e ) {
        Log::info("User::delRunningText exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        
        return response()->json($this->notif->build());
    }
    function savePasswd(Request $request) { 
        Log::info("START Update Password" );
        $pspwd = $request->input('new_pswd');
        $pspwd2 = $request->input('new_pswd2');
        $userid = $this->session->get('id');
        $userName = $this->session->get('username');
        Log::info("savePasswd:: id=".$userid." username=".$userName);

        try {
            if($pspwd == $pspwd2)
            {
                  if ($this->notif->code == \App\Http\Helpers\NotificationHelper::CODE_OK) {
                    try {
                          Log::info("savePasswd:: Go passowrd = ".$pspwd);
                            $q = WhiteLabelAdminModel::find($userid); 
                                    $q->password_enc = md5($pspwd);
                                    $q->password_type = 'md5';
                                    $q->save(); 
                               
                        // $this->notif->addMessage('Saved',200);
                          } catch (Exception $e) {
                            DB::rollBack();
                            $this->notif->addMessage($e->getMessage());
                          }
                    }
            }
            else
            {
                 $this->notif->addMessage('Konfirmasi dengan Password berbeda!'); 

            }
            




        } catch ( Exception $e ) {
        Log::info("AuthUserBL::putQueueLog user_id=".$userID." username=".$username." err_code=".$errCode." err_msg=".$errMsg." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        
        return response()->json($this->notif->build());
    }

    public function putQueueLog( $userID, $username,  $errMsg ,$errCode ) {
        Log::info("Mulai ngelog - agen".$this->userAgent.'--'.$this->ipClient);
        $b = false;
        try {
            $queuename = "log-userlogin-MA-Helpdesk".$userID;
            $input = LogUserLoginBL::createInput( $userID, $username, $this->apps );
            $logBL = new LogUserLoginBL( $input );
            log::info("input =".json_encode($input));
            $logBL->setLogDateTime()
                  ->setErrorCode( $errCode )
                  ->setErrorMessage( $errMsg )
                  ->setIPClient( $this->ipClient )
                  ->setUserAgent( $this->userAgent );
            if (Definition::USE_ASYNC_PROCESS) {
                $input = $logBL->getInput();
                $job = new InsertLogUserLoginJob( $input );
                if (! empty($queuename)) {
                    $job->onQueue($queuename);
                }
                dispatch( $job );
                $b = true;
            } else {
                $id = $logBL->insert();
                $b = boolval($id);
            }
        } catch ( Exception $e ) {
            Log::info("AuthUserBL::putQueueLog user_id=".$userID." username=".$username." err_code=".$errCode." err_msg=".$errMsg." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        return $b;
    }

 
public function sendmail($id){ 
  // $mList = array();
  Log::info("start email ".$id);
    try
    { 
        if(!empty($id))
        {
            $offset = $id * 100;
            if($offset < 1200)
            {
                echo "id =".$id." , offset=".$offset; 

                        $list = AgentAntv::join('agent','agent.username','data_username_antv.username') 
                        ->join('agent_profile','agent_profile.agent_id','agent.id') 
                        ->select('agent.username','agent_profile.fullname','agent_profile.email')
                        ->offset($offset)
                        ->orderBy('data_username_antv.id', 'ASC')
                        ->limit('100') 
                        ->get(); 
echo count($list); 
                        for ($i = 0; $i < count($list); $i++)  {
                        if(!empty($list[$i]['email']) && !is_null($list[$i]['email']))
                            {

                            echo "list ke ".$i." ".$list[$i]['username'] ."--";
                            echo $list[$i]['fullname'] ."--";
                            echo $list[$i]['email'] ."<br />";

       
                            $msg = "emailbaru";
                            $toEmail = $list[$i]['email']; //'ferizaenalabidin@gmail.com';
                            $fullname = $list[$i]['fullname']; //'Feri Zaenal Abidin';
                            $username = $list[$i]['username'];// '6285311223131';
                            $nameBlade = 'email.userantvnotif';  
                            $subject= 'PosFin Agent Information '.$msg; 
                            $fromEmail = 'helpdesk@posfin.id';

                            $paramBlade = array(
                            'fullname' => $fullname,
                            'username' => $username,  
                            'msg' =>  $msg,
                            'waktu' => date('d M Y H:i:s'),
                            'expiry_hours' => '13'  
                            );
                            Log::info("TestController::sendmailnotifupdate GO mail=".$toEmail." full=".$fullname." user=".$username);
                            // $mails = new HelpdeskBL(); 
                            // $mails->putQueuePushEmail( $toEmail, $fullname, $username); 


                            }

                        } 

            }
            else
            {
                echo "limit terlampaui";
            }
        }
        else
        {
             echo "ID kosong!";
        }
        echo "<br>"; 
 
echo "done";


      } catch ( Exception $e ) {
      Log::info("HelpdeskBL::sendmaillogin exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
      } 
   }

    }
 
