<?php

namespace App\Http\Controllers\admin\cms;

use Log;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\BDGBaseController;
use App\Models\SummaryReconPendingModel;
use App\Models\SummaryReconModel;
use App\BusinessLogic\HelpdeskBL;  

class PendingController extends BDGBaseController {

  public function __construct() {
        parent::__construct();
 
        $this->conn = Config::get ( 'webconf.cms.connectionreport' );
        $this->webapi = Config::get ( 'webconf.cms.api_web' );
        $this->helpdeskBL = new HelpdeskBL;  
    }
    function recon(){

    return view('admin.report.test' , $this->viewData);
    }
    
   function search_pending(Request $request){
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

log::info(" search_pending" );
    $min =Input::get('min'); //$request->get('min').' 00:00:00';
    $max = Input::get('max');//$request->get('max').' 23:59:59'; //$request->get('max');
    $tableselect = $request->tableselect;
    $search =$request->input('search');
    $timeMin = '00:00:00';
    $timeMax = '23:59:59';

    if($min != '')
    {
        $result1 = $min. ' ' . $timeMin; 
    } 
    else
    {
      $result1 = '';  
    }

    if($max != '')
    {
        $result2 = $max . ' ' . $timeMax;
    } 
    else
    {
      $result2 = '';  
    }

    $fields = [
            ['created_date', 'Created DateTime'],
            ['product_billnumber_paymentcode_denom', 'Pending Trx'],
            ['partner_id', 'Partner Id'],
            ['partner_id', 'Product'],
            ['partner_id', 'BillNumber'],
            ['partner_id', 'PaymentCode'],
            ['partner_id', 'Denom'],
            ['agent_id', 'Agent ID'],
            ['master_agent_id', 'Master Agent ID'],
            ['trx_price_fee_type', 'Transaction Price Fee Type'],
            ['trx_type', 'Transaction Type'],
            ['recon_code','Recon Code'],  
            ['recon_desc','Recon Desc'],
            ['recon_status','Recon Status'],
            ['confirmation_status','Confirmation Status'],
            ['summary_status','Summary Status'],
            ['notes', 'Notes']
            
        ]; 


        $listAgent =  $this->helpdeskBL->listAgentbyUpline($this->session->get('master_agent_id'));

// log::info(" list agen =".json_encode($listAgent));
        //,$min,$max,$tableselect,$search)
        $this->tableSorter->setupSorter(url('/ma/pending_trx.html'), $fields, $searchFor, $sf, $sm, $min, $max, $tableselect, $search);
        if(($search != '') && ($min != '' && $max != '')) {         
            $someModel = new SummaryReconPendingModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->orderBy('created_datetime' , 'desc')->orderBy('created_datetime' , 'desc')
                        ->whereIn('SUMMARY_RECON_PENDING.master_agent_id', $listAgent)
                         ->where('process_type','=','1')
                         ->where('recon_status','=','READY')
                        ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id')->whereBetween(
                'created_datetime', 
                [
                    $result1,
                    $result2
                ]
                )
                ->where($tableselect , $search)
                ->orderBy('created_datetime' , 'desc');

        }else if($min != '' && $max != ''){


                $someModel = new SummaryReconPendingModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->orderBy('created_datetime' , 'desc')->orderBy('created_datetime' , 'desc') ->whereIn('SUMMARY_RECON_PENDING.master_agent_id', $listAgent)
                        ->where('process_type','=','1')
                        ->where('recon_status','=','READY')
                        ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id')->whereBetween(
                'created_datetime', 
                [
                    $result1,
                    $result2
                ]
                )
                ->orderBy('created_datetime' , 'desc');
        }else if(($search != '' && $tableselect != '') && ($min == '' && $max == '')){
            // $q = SummaryReconPendingModel::where($tableselect , $search)
            //     ->orderBy('created_datetime' , 'desc');      
 
                $someModel = new SummaryReconPendingModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->orderBy('created_datetime' , 'desc')->where($tableselect , $search)->orderBy('created_datetime' , 'desc')   ->whereIn('SUMMARY_RECON_PENDING.master_agent_id', $listAgent)
                        ->where('process_type','=','1')
                        ->where('recon_status','=','READY')
                        ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id');



        }else {

    $someModel = new SummaryReconPendingModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->orderBy('created_datetime' , 'desc')
            ->where('process_type','=','1')
            ->where('recon_status','=','READY') 
            ->whereIn('SUMMARY_RECON_PENDING.master_agent_id', $listAgent)
            ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id');

        }
  
        $this->tableSorter->setupPaging($q, $pg);

        $this->viewData['sorter'] = $this->tableSorter;
        $this->viewData['pg'] = $pg;
        $this->viewData['searchFor'] = $searchFor;
        $this->viewData['min'] = $result1;
        $this->viewData['max'] = $result2;
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
          // Log::info("PendingController::pending_trx list");  log::info("data".json_encode( $this->viewData));
        return view('admin.report.pending_trx', $this->viewData );
 }
   function search_refund(Request $request){
    // log::info(" list refund trx " );
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

    $min =Input::get('min'); //$request->get('min').' 00:00:00';
    $max = Input::get('max');//$request->get('max').' 23:59:59'; //$request->get('max');
    $tableselect = $request->tableselect;
    $search =$request->input('search');
    $timeMin = '00:00:00';
    $timeMax = '23:59:59';

    if($min != '')
    {
        $result1 = $min. ' ' . $timeMin; 
    } 
    else
    {
      $result1 = '';  
    }

    if($max != '')
    {
        $result2 = $max . ' ' . $timeMax;
    } 
    else
    {
      $result2 = '';  
    }

    $fields = [
            ['created_date', 'Created DateTime'],
            ['product_billnumber_paymentcode_denom', 'Pending Trx'],
            ['partner_id', 'Partner Id'],
            ['product_billnumber_paymentcode_denom', 'Product'],
            ['product_billnumber_paymentcode_denom', 'BillNumber'],
            ['product_billnumber_paymentcode_denom', 'PaymentCode'],
            ['product_billnumber_paymentcode_denom', 'Denom'],
            ['agent_id', 'Agent ID'],
            ['master_agent_id', 'Master Agent ID'],
            ['trx_price_fee_type', 'Transaction Price Fee Type'],
            ['trx_type', 'Transaction Type'],
            ['recon_code','Recon Code'],  
            ['recon_desc','Recon Desc'],
            ['recon_status','Recon Status'],
            ['confirmation_status','Confirmation Status'],
            ['summary_status','Summary Status'],
            ['notes', 'Notes']
            
        ]; 




        $listAgent =  $this->helpdeskBL->listAgentbyUpline($this->session->get('master_agent_id'));
// log::info(" list agen =".json_encode($listAgent));
        //,$min,$max,$tableselect,$search)
        $this->tableSorter->setupSorter(url('/ma/refund_trx.html'), $fields, $searchFor, $sf, $sm, $min, $max, $tableselect, $search);
        if(($search != '') && ($min != '' && $max != '')) {         
            $someModel = new SummaryReconPendingModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->orderBy('created_datetime' , 'desc')->orderBy('created_datetime' , 'desc')
                        ->whereNotNull('agent_id')             
                        ->whereIn('SUMMARY_RECON_PENDING.master_agent_id', $listAgent)
                        ->where('recon_status','=','CONFIRMED')
                        ->where('confirmation_status','=','4000')
                        ->where('summary_status','=','PAID')
                        ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id')->whereBetween(
                'created_datetime', 
                [
                    $result1,
                    $result2
                ]
                )
                ->where($tableselect , $search)
                ->orderBy('created_datetime' , 'desc');

        }else if($min != '' && $max != ''){


                $someModel = new SummaryReconPendingModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->orderBy('created_datetime' , 'desc')->orderBy('created_datetime' , 'desc')
                        ->whereNotNull('agent_id') ->where('recon_status','=','CONFIRMED')            ->whereIn('SUMMARY_RECON_PENDING.master_agent_id', $listAgent)
                        ->where('confirmation_status','=','4000')
                        ->where('summary_status','=','PAID')
                        ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id')->whereBetween(
                'created_datetime', 
                [
                    $result1,
                    $result2
                ]
                )
                ->orderBy('created_datetime' , 'desc');
        }else if(($search != '' && $tableselect != '') && ($min == '' && $max == '')){
            // $q = SummaryReconPendingModel::where($tableselect , $search)
            //     ->orderBy('created_datetime' , 'desc');      
 
                $someModel = new SummaryReconPendingModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->orderBy('created_datetime' , 'desc')->where($tableselect , $search)->orderBy('created_datetime' , 'desc')
                        ->whereNotNull('agent_id') ->where('recon_status','=','CONFIRMED')
                        ->whereIn('SUMMARY_RECON_PENDING.master_agent_id', $listAgent)
                        ->where('confirmation_status','=','4000')
                        ->where('summary_status','=','PAID')
                        ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id');



        }else {

    $someModel = new SummaryReconPendingModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->orderBy('created_datetime' , 'desc')
            ->whereNotNull('agent_id')
            ->where('recon_status','=','CONFIRMED')
            ->where('confirmation_status','=','4000')
            ->where('summary_status','=','PAID')
            ->whereIn('SUMMARY_RECON_PENDING.master_agent_id', $listAgent)
            ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id');
        }
  
        $this->tableSorter->setupPaging($q, $pg);

        $this->viewData['sorter'] = $this->tableSorter;
        $this->viewData['pg'] = $pg;
        $this->viewData['searchFor'] = $searchFor;
        $this->viewData['min'] = $result1;
        $this->viewData['max'] = $result2;
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
          Log::info("PendingController::refund_trx list");
        return view('admin.report.refund_trx', $this->viewData );
 }
 function search_force(Request $request){


 $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

    $min =Input::get('min'); //$request->get('min').' 00:00:00';
    $max = Input::get('max');//$request->get('max').' 23:59:59'; //$request->get('max');
    $tableselect = $request->tableselect;
    $search =$request->input('search');
    $timeMin = '00:00:00';
    $timeMax = '23:59:59';

    if($min != '')
    {
        $result1 = $min. ' ' . $timeMin; 
    } 
    else
    {
      $result1 = '';  
    }

    if($max != '')
    {
        $result2 = $max . ' ' . $timeMax;
    } 
    else
    {
      $result2 = '';  
    }

    $fields = [
            ['created_date', 'Created DateTime'],
            ['product_billnumber_paymentcode_denom', 'Pending Trx'],
            ['partner_id', 'Partner Id'],
            ['product_billnumber_paymentcode_denom', 'Product'],
            ['product_billnumber_paymentcode_denom', 'BillNumber'],
            ['product_billnumber_paymentcode_denom', 'PaymentCode'],
            ['product_billnumber_paymentcode_denom', 'Denom'], 
            ['trx_price_fee_type', 'Transaction Price Fee Type'],
            ['trx_type', 'Transaction Type'],
            ['recon_code','Recon Code'],  
            ['recon_desc','Recon Desc'],
            ['recon_status','Recon Status'],
            ['confirmation_status','Confirmation Status'],
            ['summary_status','Summary Status'],
            ['notes', 'Notes']
            
        ]; 
        //,$min,$max,$tableselect,$search)
        $this->tableSorter->setupSorter(url('/ma/pending_trx.html'), $fields, $searchFor, $sf, $sm, $min, $max, $tableselect, $search);
        if(($search != '') && ($min != '' && $max != '')) {         
            $someModel = new SummaryReconPendingModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->orderBy('created_datetime' , 'desc')->orderBy('created_datetime' , 'desc')
                        ->where('process_type','=','0')
                        ->where('recon_status','=','REPORTED')
            ->selectRaw('id,created_date ,created_datetime, product_billnumber_paymentcode, trx_price_fee_type ,trx_type , recon_code , recon_status , confirmation_status , summary_status ,notes')->whereBetween(
                'created_datetime', 
                [
                    $result1,
                    $result2
                ]
                )
                ->where($tableselect , $search)
                ->orderBy('created_datetime' , 'desc');

        }else if($min != '' && $max != ''){


                $someModel = new SummaryReconPendingModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->orderBy('created_datetime' , 'desc')->orderBy('created_datetime' , 'desc')
                         ->where('process_type','=','0')
                        ->where('recon_status','=','REPORTED')
                        ->selectRaw('id,created_date ,created_datetime, product_billnumber_paymentcode, trx_price_fee_type ,trx_type , recon_code , recon_status , confirmation_status , summary_status ,notes')
                        ->whereBetween(
                            'created_datetime', 
                            [
                                $result1,
                                $result2
                            ]
                            )
                ->orderBy('created_datetime' , 'desc');
        }else if(($search != '' && $tableselect != '') && ($min == '' && $max == '')){
            // $q = SummaryReconPendingModel::where($tableselect , $search)
            //     ->orderBy('created_datetime' , 'desc');      
 
                $someModel = new SummaryReconPendingModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->orderBy('created_datetime' , 'desc')->where($tableselect , $search)->orderBy('created_datetime' , 'desc')
                        ->where('process_type','=','0')
                        ->where('recon_status','=','REPORTED')
            ->selectRaw('id,created_date ,created_datetime, product_billnumber_paymentcode, trx_price_fee_type ,trx_type , recon_code , recon_status , confirmation_status , summary_status ,notes');



        }else {

    $someModel = new SummaryReconModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->orderBy('created_datetime' , 'desc')
          ->where('process_type','=','0')
                        ->where('recon_status','=','REPORTED')
            ->selectRaw('id,created_date ,created_datetime, product_billnumber_paymentcode, trx_price_fee_type ,trx_type , recon_code , recon_status , confirmation_status , summary_status ,notes');
        }
  
        $this->tableSorter->setupPaging($q, $pg);

        $this->viewData['sorter'] = $this->tableSorter;
        $this->viewData['pg'] = $pg;
        $this->viewData['searchFor'] = $searchFor;
        $this->viewData['min'] = $result1;
        $this->viewData['max'] = $result2;
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
          Log::info("PendingController::pending_trx list");
        return view('admin.report.force_trx', $this->viewData );

    } 
    
   function refund_pending(Request $request){
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

    $min =Input::get('min'); //$request->get('min').' 00:00:00';
    $max = Input::get('max');//$request->get('max').' 23:59:59'; //$request->get('max');
    $tableselect = $request->tableselect;
    $search =$request->input('search');
    $timeMin = '00:00:00';
    $timeMax = '23:59:59';

    if($min != '')
    {
        $result1 = $min. ' ' . $timeMin; 
    } 
    else
    {
      $result1 = '';  
    }

    if($max != '')
    {
        $result2 = $max . ' ' . $timeMax;
    } 
    else
    {
      $result2 = '';  
    }

    $fields = [
            ['created_date', 'Created DateTime'],
            ['product_billnumber_paymentcode_denom', 'Pending Trx'],
            ['partner_id', 'Partner Id'],
            ['product_billnumber_paymentcode_denom', 'Product'],
            ['product_billnumber_paymentcode_denom', 'BillNumber'],
            ['product_billnumber_paymentcode_denom', 'PaymentCode'],
            ['product_billnumber_paymentcode_denom', 'Denom'],
            ['agent_id', 'Agent ID'],
            ['master_agent_id', 'Master Agent ID'],
            ['trx_price_fee_type', 'Transaction Price Fee Type'],
            ['trx_type', 'Transaction Type'],
            ['recon_code','Recon Code'],  
            ['recon_desc','Recon Desc'],
            ['recon_status','Recon Status'],
            ['confirmation_status','Confirmation Status'],
            ['summary_status','Summary Status'],
            ['notes', 'Notes']
            
        ]; 
        //,$min,$max,$tableselect,$search)
        $this->tableSorter->setupSorter(url('/ma/refund_pending_trx.html'), $fields, $searchFor, $sf, $sm, $min, $max, $tableselect, $search);
        if(($search != '') && ($min != '' && $max != '')) {         
             $someModel = new SummaryReconPendingModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->orderBy('created_datetime' , 'desc')
                        ->whereNotNull('agent_id')
                        ->where('recon_status','=','CONFIRMED')
                        ->where('confirmation_status','=','4000')
                        ->where('summary_status','=','')
                        ->selectRaw('id,id_log,created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id')->whereBetween(
                            'created_datetime', 
                            [
                                $result1,
                                $result2
                            ]
                )
                 ->orderBy('created_datetime' , 'desc');

        }else if($min != '' && $max != ''){
             $someModel = new SummaryReconPendingModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->orderBy('created_datetime' , 'desc')
            ->whereNotNull('agent_id')
            ->where('recon_status','=','CONFIRMED')
            ->where('confirmation_status','=','4000')
            ->where('summary_status','=','')
            ->selectRaw('id,id_log,created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id')->whereBetween(
                'created_datetime', 
                [
                    $result1,
                    $result2
                ]
                )
                ->orderBy('created_datetime' , 'desc');
        }else if(($search != '' && $tableselect != '') && ($min == '' && $max == '')){
    $someModel = new SummaryReconPendingModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->orderBy('created_datetime' , 'desc')
            ->whereNotNull('agent_id')
            ->where('recon_status','=','CONFIRMED')
            ->where('confirmation_status','=','4000')
            ->where('summary_status','=','')
            ->selectRaw('id,id_log,created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id')->where($tableselect , $search)
                ->orderBy('created_datetime' , 'desc');      
        }else {

    $someModel = new SummaryReconPendingModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->orderBy('created_datetime' , 'desc')
            ->whereNotNull('agent_id')
            ->where('recon_status','=','CONFIRMED')
            ->where('confirmation_status','=','4000')
            ->where('summary_status','=','')
            ->selectRaw('id,id_log,created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id');
        }
  
        $this->tableSorter->setupPaging($q, $pg);

        $this->viewData['sorter'] = $this->tableSorter;
        $this->viewData['pg'] = $pg;
        $this->viewData['searchFor'] = $searchFor;
        $this->viewData['min'] = $result1;
        $this->viewData['max'] = $result2;
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
          Log::info("PendingController::search_pending list");
        return view('admin.report.pending_refund_trx', $this->viewData );
 }
    



 function prosesFailed(Request $request)
{ 
              // Log::info("PendingController::prosesFailed proses failed");

$result ="Failed";
try{
 
    $id_trx = $request->input('id');
    $time_trx = $request->input('tim'); 
            if ($id_trx == '') { $this->notif->addMessage('Pending ID Trx required'); }
            if ($time_trx == '') { $this->notif->addMessage('Pending ID Trx required'); }
if ($this->notif->isOK() == true) {
                   // Log::info("PendingController::prosesFailed proses 1  ");

      // Log::info("PendingController::prosesFailed proses 2  ");
    $someModel = new SummaryReconPendingModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->where('product_billnumber_paymentcode_denom', '=',$id_trx)
            ->where('created_datetime', '=',$time_trx)
            ->update([
            // 'result_desc' => "Pending", 
            'recon_status' => "CONFIRMED" ,
            'confirmation_status' => "4000",
            'updated_by' => $this->session->get('username')."- Helpdesk CMS",
            'updated_at' => date('Y-m-d H:i:s')
            ]);  
        
 }     
  

} catch ( Exception $e ) {
        Log::info("RefundController::prosesFailed exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
} 
       // Log::info("PendingController::prosesFailed proses failed end");     
       return response()->json($this->notif->build());  
}


 function prosesSukses(Request $request)
{ 
              Log::info("PendingController::prosesSukses proses sukses");

$result ="Failed";
try{
  
    $id_trx = $request->input('id');
    $time_trx = $request->input('tim');  
            if ($id_trx == '') { $this->notif->addMessage('Pending ID Trx required'); }
            if ($time_trx == '') { $this->notif->addMessage('Pending ID Trx required'); }
if ($this->notif->isOK() == true) {
                   Log::info("PendingController::prosesSukses proses 1  ");

$someModel = new SummaryReconPendingModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->where('product_billnumber_paymentcode_denom', '=',$id_trx)
            ->where('created_datetime', '=',$time_trx)
            ->update([
            // 'result_desc' => "Pending", 
            'recon_status' => "CONFIRMED" ,
            'confirmation_status' => "0",
            'updated_by' => $this->session->get('username')."- Helpdesk CMS",
            'updated_at' => date('Y-m-d H:i:s') 
            ]);  
              Log::info("PendingController::prosesSukses proses 2  ");

 }     
  

} catch ( Exception $e ) {
        Log::info("RefundController::prosesSukses exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
} 
       Log::info("PendingController::prosesSukses proses failed end");     
       return response()->json($this->notif->build());               

} function prosesForcedTrx(Request $request)
{ 
             // Log::info("PendingController::prosesForcedTrx proses sukses");

$result ="Failed";
try{  

    // if($this->session->get('group') !="ADMIN")
    // {
    //     $res['status'] =  "Error";
    //     $res['code'] =  "1112";
    //     $res['message'] =  'You Are Not Allowed to Access this page!!';
    // }
    // else
    // {   
        $id_trx = $request->input('id');
        $time_trx = $request->input('tim');  
        $code_trx = $request->input('code');  
        if ($id_trx == '') { $this->notif->addMessage('Pending ID Trx required'); }
        if ($time_trx == '') { $this->notif->addMessage('Pending ID Trx required'); }         
           Log::info("PendingController::prosesForcedTrx proses 1  ".$code_trx);

        // if ($this->notif->isOK() == true) {
 
    $reslog = $this->helpdeskBL->getidLogforce($code_trx);  
    $url= $this->webapi."refund";   
       Log::info("PendingController::prosesForcedTrx reslog=".json_encode($reslog));

    $result = $this->helpdeskBL->requestPost($url,$reslog);
      Log::info("RefundController::refundPending data=".json_encode($result));
    $hasil = json_decode($result);
    $res['status'] =  $hasil->status;
    $res['code'] =  $hasil->error->code;
    $res['message'] =  $hasil->error->message;

        if($hasil->status == "OK")
        {
                $someModel = new SummaryReconModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->where('id', '=',$id_trx)
                ->update([ 
                'recon_status' => "CONFIRMED",  
                'confirmation_status' => "00",  
                'notes' => "force transaction success by".$this->session->get('username')."- Helpdesk CMS | ".date('Y-m-d H:i:s'),  
                'process_type' => "1",  
                'summary_status' => "Force Trx ".$reslog,  
                'updated_by' => $this->session->get('username')."- Helpdesk CMS",
                'updated_at' => date('Y-m-d H:i:s')  
                ]);  
                Log::info("PendingController::prosesForcedTrx update table done  ");
        }



    // }     
   // }

} catch ( Exception $e ) {
        Log::info("RefundController::prosesForcedTrx exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
} 
       Log::info("PendingController::prosesSukses proses failed end".json_encode($res));     
     return $res;
} 


function prosesRefund(Request $request)
{ 
              Log::info("PendingController::prosesRefund proses sukses");

$result ="Failed";
try{  

    if($this->session->get('group') !="FINANCE")
    {
          // $this->notif->addMessage('You Are Not Allowed to Access this page!!');  
          $res['status'] =  "Error";
            $res['code'] =  "1112";
            $res['message'] =  'You Are Not Allowed to Access this page!!';
    }
    else
    {

   
    $id_trx = $request->input('id'); 
    if ($id_trx == '') { $this->notif->addMessage('Pending ID Trx required'); } 
    if ($this->notif->isOK() == true) {
        Log::info("PendingController::prosesRefund proses 1  ");
 
    
    $reslog = $this->helpdeskBL->getidLogfrombill($id_trx);  
    $url= $this->webapi."refund";   
       Log::info("RefundController::refundPending reslog=".json_encode($reslog));

    $result = $this->helpdeskBL->requestPost($url,$reslog);
      Log::info("RefundController::refundPending data=".json_encode($result));
    $hasil = json_decode($result);
    $res['status'] =  $hasil->status;
    $res['code'] =  $hasil->error->code;
    $res['message'] =  $hasil->error->message;

    if($hasil->status == "OK")
    {
            $someModel = new SummaryReconPendingModel;
            $someModel->setConnection($this->conn); 
            $q = $someModel->where('product_billnumber_paymentcode_denom', '=',$id_trx)
            ->where('created_datetime', '=',$time_trx)
            ->update([
            // 'result_desc' => "Pending", 
            'summary_status' => "PAID",  
            'updated_by' => $this->session->get('username')."- Helpdesk CMS",
            'updated_at' => date('Y-m-d H:i:s')  
            ]);  
            Log::info("PendingController::prosesSukses update table done  ");
    }
    }     
   }

} catch ( Exception $e ) {
        Log::info("RefundController::prosesSukses exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
} 
       // Log::info("PendingController::prosesSukses proses failed end".json_encode($res));     
     return $res;
}




 function act_duplicate(Request $request)
{ 
 
$result ="Failed";
try{
 
    $id_trx = $request->input('id');
    $notes_trx = $request->input('notes');
             if ($id_trx == '') { $this->notif->addMessage('ID Trx required'); }
 if ($this->notif->isOK() == true) {
    $someModel = new SummaryReconModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->where('id', '=',$id_trx)
             ->update([
            'recon_status' => "CONFIRMED" ,
            'confirmation_status' => "00",
            'summary_status' => "DUPLICATE notes:".$notes_trx,
            'updated_by' => $this->session->get('username')."- Helpdesk CMS",
            'updated_at' => date('Y-m-d H:i:s')
            ]);  
        
 }     
  

} catch ( Exception $e ) {
        Log::info("RefundController::act_duplicate exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
} 
        return response()->json($this->notif->build());  
}
 function act_allocate(Request $request)
{ 
 
$result ="Failed";
try{
 
    $id_trx = $request->input('id');
             if ($id_trx == '') { $this->notif->addMessage('ID Trx required'); }
 if ($this->notif->isOK() == true) {
    $someModel = new SummaryReconModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->where('id', '=',$id_trx)
             ->update([
            'recon_status' => "TOBEFORCED" ,
            'process_type' => "2",
            'updated_by' => $this->session->get('username')."- Helpdesk CMS",
            'updated_at' => date('Y-m-d H:i:s')
            ]);  
        
 }     
  

} catch ( Exception $e ) {
        Log::info("RefundController::act_allocate exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
} 
        return response()->json($this->notif->build());  
}






function refundPendingTrx($id_trx)
{ 
 $s = "";
try{
 
    $detail = $this->helpdeskBL->getdetailLog($id_trx);
    $url= $this->webapi."refund";   
    $result = $this->helpdeskBL->requestPost($url,$detail);
      // Log::info("RefundController::refundPending data=".json_encode($result));
    $hasil = json_decode($result);
    $res['status'] =  $hasil->status;
    $res['code'] =  $hasil->error->code;
    $res['message'] =  $hasil->error->message;
 

} catch ( Exception $e ) {
        Log::info("RefundController::refundPending exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
} 
return $res;
}




function updateRecon($id_trx,$time_trx)
{
    Log::info("PendingController::updateRecon proses 1  ".$id_trx." time=".$time_trx);
    $b = false;
    try{
        if($this->cekRecon($id_trx,$time_trx))
        {
            $someModel = new SummaryReconModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->where('product_billnumber_paymentcode', '=',$id_trx)
            ->where('created_datetime', '=',$time_trx)
            ->update([
            'result_desc' => "Pending", 
            'recon_status' => "Confirmed" ,
            'confirmation_status' => "4000"
            ]); 
            DB::commit();
            $b=true;
        }
      Log::info("PendingController::updateRecon proses end ".$b);
} catch ( Exception $e ) {
        Log::info("RefundController::prosesFailed exception=[Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
        $b=false;
} 
return $b;
}


public function cekRecon($id_trx,$time_trx) {
    Log::info("PendingController::cekRecon proses 1  ");
        $b=false;
        try { 
            $someModel = new SummaryReconModel;
    $someModel->setConnection($this->conn); 
    $cekprod = $someModel->where('product_billnumber_paymentcode', '=',$id_trx)
            ->where('created_datetime', '=',$time_trx)
            ->first();
            if ((empty($cekprod))&& is_null($cekprod )) {
                $b= true; 
            }
        } catch ( Exception $e ) {
            Log::info("RefundController::cekRecon exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
            Log::info("PendingController::cekRecon proses end ,value b=".$b);

        return $b;
    }   




    function exportPendingToCSV(Request $request){
        

    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

    $min =Input::get('min'); //$request->get('min').' 00:00:00';
    $max = Input::get('max');//$request->get('max').' 23:59:59'; //$request->get('max');
    $tableselect = $request->tableselect;
    $search =$request->input('search');
    $timeMin = '00:00:00';
    $timeMax = '23:59:59';

    if($min != '')
    {
        $result1 = $min. ' ' . $timeMin; 
    } 
    else
    {
      $result1 = '';  
    }

    if($max != '')
    {
        $result2 = $max . ' ' . $timeMax;
    } 
    else
    {
      $result2 = '';  
    }



         if(($search != '') && ($min != '' && $max != '')) {         
            $someModel = new SummaryReconPendingModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->orderBy('created_datetime' , 'desc')->orderBy('created_datetime' , 'desc')
                        ->whereNotNull('agent_id')
                        ->where('recon_status','=','REPORTED')
                        ->where('beh2hlog.SUMMARY_RECON_PENDING.master_agent_id', '=', $this->session->get('master_agent_id'))
                        ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id')->whereBetween(
                'created_datetime', 
                [
                    $result1,
                    $result2
                ]
                )
                ->where($tableselect , $search)
                ->orderBy('created_datetime' , 'desc')->get();

        }else if($min != '' && $max != ''){


                $someModel = new SummaryReconPendingModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->orderBy('created_datetime' , 'desc')->orderBy('created_datetime' , 'desc')
                        ->whereNotNull('agent_id')
                        ->where('recon_status','=','REPORTED')
                        ->where('beh2hlog.SUMMARY_RECON_PENDING.master_agent_id', '=', $this->session->get('master_agent_id'))
                        ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id')->whereBetween(
                'created_datetime', 
                [
                    $result1,
                    $result2
                ]
                )
                ->orderBy('created_datetime' , 'desc')->get();
        }else if(($search != '' && $tableselect != '') && ($min == '' && $max == '')){
            // $q = SummaryReconPendingModel::where($tableselect , $search)
            //     ->orderBy('created_datetime' , 'desc');      
 
                $someModel = new SummaryReconPendingModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->orderBy('created_datetime' , 'desc')->where($tableselect , $search)->orderBy('created_datetime' , 'desc')
                        ->whereNotNull('agent_id')
                        ->where('recon_status','=','REPORTED')
                        ->where('beh2hlog.SUMMARY_RECON_PENDING.master_agent_id', '=', $this->session->get('master_agent_id'))
                        ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id')->get();



        }else {

    $someModel = new SummaryReconPendingModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->orderBy('created_datetime' , 'desc')
            ->whereNotNull('agent_id')
            ->where('recon_status','=','REPORTED')
            ->where('beh2hlog.SUMMARY_RECON_PENDING.master_agent_id', '=', $this->session->get('master_agent_id'))
            ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes, agent_id,master_agent_id')->get();
        }
  
                // $table =PartnerTrxReqModel::whereBetween(
                // 'created_at', 
                // [
                //     $result1,
                //     $result2
                // ]   
                // )
                //  ->where('partner_id' , '=' , $this->session->get('partner_id'))
                //  ->where($tableselect , $search)
                //  ->orderBy('created_at' , 'desc')
                //  ->get(); 
            // }
            
            $filename = "report_pending_trx.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('created_date' ,'created_datetime', 'partner_id' ,'product_billnumber_paymentcode_denom', 'trx_price_fee_type' ,'trx_type' , 'recon_code' , 'recon_desc', 'recon_status' , 'confirmation_status' , 'summary_status' ,'notes','agent_id','master_agent_id'));

            foreach($q as $row) {
                fputcsv($handle, array($row['created_date'], $row['created_datetime'], $row['partner_id'], $row['product_billnumber_paymentcode_denom'], $row['trx_price_fee_type'], $row['trx_type'], $row['recon_code'] , $row['recon_desc'] , $row['recon_status'], $row['confirmation_status'], $row['summary_status'],$row['notes'],$row['agent_id'],$row['master_agent_id']));
            }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            return response()->download($filename, 'report_pending_trx.csv', $headers);
    }


    // ==========================  refun to csv ==============================//
    //
    // =======================================================================//


   function exportRefundToCSV(Request $request){
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

    $min =Input::get('min'); //$request->get('min').' 00:00:00';
    $max = Input::get('max');//$request->get('max').' 23:59:59'; //$request->get('max');
    $tableselect = $request->tableselect;
    $search =$request->input('search');
    $timeMin = '00:00:00';
    $timeMax = '23:59:59';

    if($min != '')
    {
        $result1 = $min. ' ' . $timeMin; 
    } 
    else
    {
      $result1 = '';  
    }

    if($max != '')
    {
        $result2 = $max . ' ' . $timeMax;
    } 
    else
    {
      $result2 = '';  
    }

    $fields = [
            ['created_date', 'Created DateTime'],
            ['product_billnumber_paymentcode_denom', 'Pending Trx'],
            ['partner_id', 'Partner Id'],
            ['product_billnumber_paymentcode_denom', 'Product'],
            ['product_billnumber_paymentcode_denom', 'BillNumber'],
            ['product_billnumber_paymentcode_denom', 'PaymentCode'],
            ['product_billnumber_paymentcode_denom', 'Denom'],
            ['agent_id', 'Agent ID'],
            ['master_agent_id', 'Master Agent ID'],
            ['trx_price_fee_type', 'Transaction Price Fee Type'],
            ['trx_type', 'Transaction Type'],
            ['recon_code','Recon Code'],  
            ['recon_desc','Recon Desc'],
            ['recon_status','Recon Status'],
            ['confirmation_status','Confirmation Status'],
            ['summary_status','Summary Status'],
            ['notes', 'Notes']
            
        ]; 
        //,$min,$max,$tableselect,$search)
        $this->tableSorter->setupSorter(url('/ma/refund_pending_trx.html'), $fields, $searchFor, $sf, $sm, $min, $max, $tableselect, $search);
        if(($search != '') && ($min != '' && $max != '')) {         
             $someModel = new SummaryReconPendingModel;
                $someModel->setConnection($this->conn); 
                $q = $someModel->orderBy('created_datetime' , 'desc')
                        ->where('process_type','=','1')
                        ->where('recon_status','=','CONFIRMED')
                        ->where('confirmation_status','=','4000')
                        ->where('summary_status','=','PAID')
                        ->where('beh2hlog.SUMMARY_RECON_PENDING.master_agent_id', '=', $this->session->get('master_agent_id'))
                        ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id')->whereBetween(
                            'created_datetime', 
                            [
                                $result1,
                                $result2
                            ]
                )
                 ->orderBy('created_datetime' , 'desc')->get();

        }else if($min != '' && $max != ''){
             $someModel = new SummaryReconPendingModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->orderBy('created_datetime' , 'desc')
            ->where('process_type','=','1')
            ->where('recon_status','=','CONFIRMED')
            ->where('confirmation_status','=','4000')
            ->where('summary_status','=','PAID')
            ->where('beh2hlog.SUMMARY_RECON_PENDING.master_agent_id', '=', $this->session->get('master_agent_id'))
            ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id')->whereBetween(
                'created_datetime', 
                [
                    $result1,
                    $result2
                ]
                )
                ->orderBy('created_datetime' , 'desc')->get();
        }else if(($search != '' && $tableselect != '') && ($min == '' && $max == '')){
    $someModel = new SummaryReconPendingModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->orderBy('created_datetime' , 'desc')
            ->where('process_type','=','1')
            ->where('recon_status','=','CONFIRMED')
            ->where('confirmation_status','=','4000')
            ->where('summary_status','=','PAID')
            ->where('beh2hlog.SUMMARY_RECON_PENDING.master_agent_id', '=', $this->session->get('master_agent_id'))
            ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id')->where($tableselect , $search)
                ->orderBy('created_datetime' , 'desc')->get();      
        }else {

    $someModel = new SummaryReconPendingModel;
    $someModel->setConnection($this->conn); 
    $q = $someModel->orderBy('created_datetime' , 'desc') 
            ->where('process_type','=','1')
            ->where('recon_status','=','CONFIRMED')
            ->where('confirmation_status','=','4000')
            ->where('summary_status','=','PAID')
            ->where('beh2hlog.SUMMARY_RECON_PENDING.master_agent_id', '=', $this->session->get('master_agent_id'))
            ->selectRaw('created_date ,created_datetime, partner_id ,product_billnumber_paymentcode_denom, trx_price_fee_type ,trx_type , recon_code , recon_desc , recon_status , confirmation_status , summary_status ,notes,agent_id,master_agent_id')->get();
        }
  

   $filename = "report_refund_trx.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('created_date' ,'created_datetime', 'partner_id' ,'product_billnumber_paymentcode_denom', 'trx_price_fee_type' ,'trx_type' , 'recon_code' , 'recon_desc', 'recon_status' , 'confirmation_status' , 'summary_status' ,'notes','agent_id','master_agent_id'));

            foreach($q as $row) {
                fputcsv($handle, array($row['created_date'], $row['created_datetime'], $row['partner_id'], $row['product_billnumber_paymentcode_denom'], $row['trx_price_fee_type'], $row['trx_type'], $row['recon_code'] , $row['recon_desc'] , $row['recon_status'], $row['confirmation_status'], $row['summary_status'],$row['notes'],$row['agent_id'],$row['master_agent_id']));
            }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            return response()->download($filename, 'report_refund_trx.csv', $headers);

 
 }
    


}