<?php

namespace App\Http\Controllers\admin\cms;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Controllers\BDGBaseController;
use App\Models\admin\BillerModel;
use App\Models\admin\WhiteLabelBillerModel;

class WhiteLabelBillerController extends BDGBaseController
{
    function index() {
        $pg = Input::get('pg', 1);
		$searchFor = Input::get('q');
		$sf = Input::get('sf', 0);
		$sm = Input::get('sm', 0);
		
		$fields = [
			['biller_aggregator.aggregator_name', 'Biller Aggregator Name'],
            ['white_label_biller_aggregators.priority', 'Priority'],
            ['is_active', 'Status']
		];
		
		$this->tableSorter->setupSorter(url('/wlbiller.html'), $fields, $searchFor, $sf, $sm);
		
		if($searchFor != '') {
            $q = WhiteLabelBillerModel::where('white_label_biller_aggregators.white_label_id', '=', $this->session->whiteLabelId())
                    ->where('biller_aggregator.aggregator_name', 'like', '%'.$searchFor.'%')
                    ->join('biller_aggregator', 'white_label_biller_aggregators.biller_aggregator_id', '=', 'biller_aggregator.id')
                    ->selectRaw('white_label_biller_aggregators.id as wlb_id, white_label_biller_aggregators.active, white_label_biller_aggregators.priority, biller_aggregator.aggregator_name')
                    ->selectRaw('if(white_label_biller_aggregators.active = \'Y\', if(biller_aggregator.active = \'Y\', 1, 0), 0) as is_active');
		} else {
			$q = WhiteLabelBillerModel::where('white_label_biller_aggregators.white_label_id', '=', $this->session->whiteLabelId())
                    ->join('biller_aggregator', 'white_label_biller_aggregators.biller_aggregator_id', '=', 'biller_aggregator.id')
                    ->selectRaw('white_label_biller_aggregators.id as wlb_id, white_label_biller_aggregators.active, white_label_biller_aggregators.priority, biller_aggregator.aggregator_name')
                    ->selectRaw('if(white_label_biller_aggregators.active = \'Y\', if(biller_aggregator.active = \'Y\', 1, 0), 0) as is_active');
		}
		
		$this->tableSorter->setupPaging($q, $pg);
        
        $this->viewData['sorter'] = $this->tableSorter;
        
        return view('white_label_biller.index', $this->viewData);
    }
    
    function edit($id) {
        $this->viewData['rs'] = BillerModel::where('white_label_biller_aggregators.white_label_id', '=', $this->session->whiteLabelId())
                                    ->where('white_label_biller_aggregators.id', '=', $id)
                                    ->leftJoin('white_label_biller_aggregators', 'biller_aggregator.id', '=', 'white_label_biller_aggregators.biller_aggregator_id')
                                    ->selectRaw('white_label_biller_aggregators.*, biller_aggregator.id as bill_id, biller_aggregator.aggregator_name')
                                    ->selectRaw('if(white_label_biller_aggregators.active = \'Y\', if(biller_aggregator.active = \'Y\', 1, 0), 0) as is_active')
                                    ->first();
        
        return view('white_label_biller.edit', $this->viewData);
    }
    
    function update(Request $request) {
        $id = $request->input('id');
        $priority = $request->input('priority');
        $status = $request->input('status');
        
        if (!is_numeric($priority) || $priority < 0) { $this->notif->addMessage('Priority must be numeric and greater than or equal zero'); }
        else {
            $q = WhiteLabelBillerModel::where('white_label_id', '=', $this->session->whiteLabelId())
                    ->where('priority', '=', $priority)
                    ->where('id', '<>', $id)
                    ->first();
            if ($q != null) {
                $this->notif->addMessage('Priority exists. Please choose another.');
            }
        }
        
        if ($this->notif->isOK() == true) {
            $q = WhiteLabelBillerModel::find($id);
            $q->priority = $priority;
            $q->updated_at = date('Y-m-d H:i:s');
            $q->save();
        }
        
        return response()->json($this->notif->build());
    }
    
        function create() {
        $this->viewData['rsbill'] = BillerModel::where('active', '=', 'Y')
                                        ->get();
        
        return view('white_label_biller.new', $this->viewData);
    }
    
    function store(Request $request) {
        $biller = $request->input('biller');
        $priority = $request->input('priority');
        $status = $request->input('status');
        
        if ($biller == '') { $this->notif->addMessage('Biller Aggregator is required'); }
        else {
            $q = WhiteLabelBillerModel::where('white_label_id', '=', $this->session->whiteLabelId())
                    ->where('biller_aggregator_id', '=', $biller)
                    ->first();
            if ($q != null) {
                $this->notif->addMessage('Biller Aggregator exists. Please choose another.');
            }
        }
        if (!is_numeric($priority) || $priority < 0) { $this->notif->addMessage('Priority must be numeric and greater than or equal zero'); }
        else {
            $q = WhiteLabelBillerModel::where('white_label_id', '=', $this->session->whiteLabelId())
                    ->where('priority', '=', $priority)
                    ->first();
            if ($q != null) {
                $this->notif->addMessage('Priority exists. Please choose another.');
            }
        }
        
        if ($this->notif->isOK() == true) {
            $q = new WhiteLabelBillerModel();
            $q->white_label_id = $this->session->whiteLabelId();
            $q->biller_aggregator_id = $biller;
            $q->priority = $priority;
            $q->active = $status;
            $q->created_at = date('Y-m-d H:i:s');
            $q->updated_at = date('Y-m-d H:i:s');
            $q->save();
        }
        
        return response()->json($this->notif->build());
    }
}
