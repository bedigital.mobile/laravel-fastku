<?php

namespace App\Http\Controllers\admin\cms;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Controllers\BDGBaseController;
use App\Models\admin\BalanceTypeModel;
use App\Models\admin\WhiteLabelBalanceTypeModel;

class WhiteLabelBalanceTypeController extends BDGBaseController
{
    function index() {
        $pg = Input::get('pg', 1);
		$searchFor = Input::get('q');
		$sf = Input::get('sf', 0);
		$sm = Input::get('sm', 0);
		
		$fields = [
			['balance_type.balance_name', 'Service Name'],
            ['white_label_balance_types.balance_name_alias', 'Alias'],
            ['balance_type.paymode', 'Pay Mode'],
            ['balance_type.curreny_code', 'Currency'],
            ['is_active', 'Status']
		];
		
		$this->tableSorter->setupSorter(url('/wlbalancetype.html'), $fields, $searchFor, $sf, $sm);
		
		if($searchFor != '') {
            $q = BalanceTypeModel::where('white_label_balance_types.white_label_id', '=', $this->session->whiteLabelId())
                    ->join('white_label_balance_types', 'balance_type.id', '=', 'white_label_balance_types.balance_type_id')
                    ->where(function ($query) use ($searchFor) {
                        $query->where('white_label_balance_types.balance_name_alias', 'like', '%'.$searchFor.'%')                              
                              ->orWhere('balance_type.balance_name', 'like', '%'.$searchFor.'%');
                    })
                    ->selectRaw('balance_type.balance_name, balance_type.paymode, balance_type.currency_code')
                    ->selectRaw('white_label_balance_types.id as wlb_id, white_label_balance_types.balance_name_alias')
                    ->selectRaw('if(white_label_balance_types.active = \'Y\', if(balance_type.active = \'Y\', 1, 0), 0) as is_active');
		} else {
			$q = BalanceTypeModel::where('white_label_balance_types.white_label_id', '=', $this->session->whiteLabelId())
                    ->join('white_label_balance_types', 'balance_type.id', '=', 'white_label_balance_types.balance_type_id')
                    ->selectRaw('balance_type.balance_name, balance_type.paymode, balance_type.currency_code')
                    ->selectRaw('white_label_balance_types.id as wlb_id, white_label_balance_types.balance_name_alias')
                    ->selectRaw('if(white_label_balance_types.active = \'Y\', if(balance_type.active = \'Y\', 1, 0), 0) as is_active');
		}
		
		$this->tableSorter->setupPaging($q, $pg);
        
        $this->viewData['sorter'] = $this->tableSorter;
        
        return view('white_label_balance_type.index', $this->viewData);
    }
    
    function edit($id) {
        $this->viewData['rs'] = BalanceTypeModel::where('white_label_balance_types.white_label_id', '=', $this->session->whiteLabelId())
                                    ->where('white_label_balance_types.id', '=', $id)
                                    ->join('white_label_balance_types', 'balance_type.id', '=', 'white_label_balance_types.balance_type_id')
                                    ->selectRaw('balance_type.balance_name, balance_type.paymode, balance_type.currency_code')
                                    ->selectRaw('white_label_balance_types.id as wlb_id, white_label_balance_types.balance_name_alias')
                                    ->selectRaw('if(white_label_balance_types.active = \'Y\', if(balance_type.active = \'Y\', 1, 0), 0) as is_active')
                                    ->first();
        
        return view('white_label_balance_type.edit', $this->viewData);
    }
    
    function update(Request $request) {
        $id = $request->input('id');
        $alias = $request->input('alias');
        $description = $request->input('description');
        
        $q = WhiteLabelBalanceTypeModel::find($id);
        $q->balance_name_alias = $alias;
        $q->description = $description;
        $q->updated_at = date('Y-m-d H:i:s');
        $q->save();
        
        return response()->json($this->notif->build());
    }
}
