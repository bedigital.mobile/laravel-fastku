<?php
namespace App\Http\Controllers\admin\cms;



use Log;
use Config;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\BDGBaseController; 
use App\Models\kurir\Member;
use App\Models\kurir\MemberProfile;

use App\Models\Database\AgentWorkstation;
use App\BusinessLogic\HelpdeskBL;
use App\Core\Util\UtilCommon;
use App\Models\LogSmsSend;


class AgentController extends BDGBaseController
{
    public function __construct() {
        parent::__construct();    
        // $this->conn = Config::get ( 'mysql_stg' );
        $this->conn = Config::get ( 'webconf.cms.connectionalibaba' );
        $this->connkurir = Config::get ( 'webconf.cms.connectionkurir' );
        $this->cms_type = Config::get ( 'webconf.cms.cms_type' );
        $this->helpdesk = new HelpdeskBL; 
        $this->hostcms = Config::get ( 'webconf.cms.hostcms' );
        
    }
    
    function verified(Request $request) {
        $host = $request->getHttpHost();
        $this->helpdesk->verifiedHost($this->hostcms, $host);
        
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', '');
        $sm = Input::get('sm', '');
        $tableselect="";
        $search = Input::get('search');
         
        $fields = [
             
            ['gerai.id', 'Nomor Daftar'],
            ['gerai.nama', 'Nama Gerai'],            
            ['gerai.alamat', 'Alamat Gerai'],
            ['gerai.status', 'Status Gerai'] 

        ]; 
          $tableselect = Input::get('tableselect');  
    try {
         $this->tableSorter->setupSorter(secure_url('/admin/verifikasi'), $fields, $searchFor, $sf, $sm, $tableselect, $search);   
         // $listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id'));
 
        // $listAgent = $this->helpdesk->listMabyUpline($this->session->get('master_agent_id'));?
        // Log::info("list agent=".json_encode($listAgent));
switch ($tableselect) {
    case 'nomordaftar':
      $tablename="gerai.id";
    break;
    case 'nama':
      $tablename="gerai.nama";
    break; 
    case 'alamat':
      $tablename="gerai.alamat";
    break; 
    default:
      $tablename="";
    break;
}

        if(!empty($search) && (!empty($tableselect))) {
          
            $someModel = new  Gerai;
            $someModel->setConnection($this->conn);  
            $q = $someModel->Join('agent', 'agent.id', '=', 'gerai.id_agent')
            ->where('gerai.status','=','REGISTERING')
                ->where(function ($query) use ($tablename,$search) { 
                        $query->where( $tablename, 'like', '%'.$search.'%'); 
                    })
                ->selectRaw('gerai.nama,gerai.id,alamat,status');


        } else {
            $someModel = new  Gerai;
            $someModel->setConnection($this->conn); 
             $q = $someModel->Join('agent', 'agent.id', '=', 'gerai.id_agent')
             ->where('gerai.status','=','REGISTERING')
                ->selectRaw('gerai.nama,gerai.id,alamat,status');
           
        }
         
        $this->tableSorter->setupPaging($q, $pg);
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
        
        $this->viewData['sorter'] = $this->tableSorter;
 
        } catch ( Exception $e ) {
            Log::info("AgentController::index exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        return view('admin.agent.verifikasi', $this->viewData);
    }
     

    function index(Request $request) {
       // $host = $request->getHttpHost();
       // $this->helpdesk->verifiedHost($this->hostcms, $host);
         Log::info("AgentController::index listuser START");
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', '');
        $sm = Input::get('sm', '');
        $tableselect="";
        $search = Input::get('search');
         
        $fields = [
            ['gerai.id', 'User ID'],
            ['gerai.nama', 'Username'],
            ['gerai.alamat', 'Full Name'],
            ['gerai.alamat', 'Email'],
            ['gerai.alamat', 'Desc'],
            ['gerai.status', 'Status'] 

        ]; 
          $tableselect = Input::get('tableselect');  
    try {
         $this->tableSorter->setupSorter(secure_url('/kurirku/listuser'), $fields, $searchFor, $sf, $sm, $tableselect, $search);   
         // $listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id'));
 
        // $listAgent = $this->helpdesk->listMabyUpline($this->session->get('master_agent_id'));?
        // Log::info("list agent=".json_encode($listAgent));
switch ($tableselect) {
    case 'user':
      $tablename="username";
    break;
    case 'nama':
      $tablename="nicknama";
    break; 
    case 'status':
      $tablename="user_status";
    break; 
    default:
      $tablename="";
    break;
}

        if(!empty($search) && (!empty($tableselect))) {
          
            $someModel = new  Member;
            $someModel->setConnection($this->connkurir);  
            $q = $someModel->where('user_status','=','ACTIVE')
                ->where(function ($query) use ($tablename,$search) { 
                        $query->where( $tablename, 'like', '%'.$search.'%'); 
                    })
                ->selectRaw('id,username,nickname,user_status');


        } else {
            $someModel = new  Member;
            $someModel->setConnection($this->connkurir); 
             $q = $someModel
             ->join('member_profile','member_id','member.id')
             ->where('user_status','=','ACTIVE')
                ->selectRaw('member.id,username,fullname,email,description,user_status');
           
        }
         
        $this->tableSorter->setupPaging($q, $pg);
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
        
        $this->viewData['sorter'] = $this->tableSorter;
 
        } catch ( Exception $e ) {
            Log::info("AgentController::index exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        return view('admin.agent.index', $this->viewData);
    }
     

    function view($agentId) {
        log::info("View Detail Gerai");
 


    $this->viewData['rs']="";
   
  $someModel = new  Gerai; 
            $someModel->setConnection($this->conn);  
             $this->viewData['rs'] = $someModel->Join('agent', 'agent.id', '=', 'gerai.id_agent')
                ->where('gerai.id','=',$agentId)
                ->selectRaw('agent.nama nama_agent,agent.ektp,agent.npwp,agent.email,agent.telp,gerai.id id_gerai,gerai.nama,gerai.id,alamat,status,ukuran,jarak_sesama_agen,data_pesaing,photo_gerai_depan,photo_gerai_belakang,photo_gerai_denah,gerai.province_name,gerai.city_name') 
                ->firstorFail();


 
        return view('admin.agent.view', $this->viewData);
    } 

 function exportAgen(Request $request) {
            // $tableselect = $request->tableselect;
            // $search =$request->input('search');

        // if(($search != '') && ($tableselect != '')){
        //     // $someModel = new  AgentModel;
        //     // $someModel->setConnection($this->conn); 
        //     // $table = InvoiceSummaryModel::where('partner_id' , '=' , $this->session->get('partner_id'))
        //     //     ->where($tableselect , $search)
        //     //     ->orderBy('created_date' , 'desc')
        //     //     ->get();      
        // }else {
        //     $someModel = new  AgentModel;
        //     $someModel->setConnection($this->conn); 
        //     $q = $someModel->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
        //     // $table = InvoiceSummaryModel::orderBy('created_date' , 'desc')
        //     ->get();
        // }



        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', '');
        $sm = Input::get('sm', '');
      
        $search = Input::get('search');
        
        $fields = [
            ['agent_id', 'Agent ID'],
            ['agent_name', 'Agent Name'],
            ['master_agent_id', 'Master Agent Id'],
            ['email', 'Agent Email'],
            ['phone_number', 'Phone Number'],            ['daily_trx_total_price_max', 'Daily Tot Max Trx'],

            ['npwp_number', 'NPWP'],
            ['address', 'Alamat'],
            ['agent.status', 'Status']

        ];
          $tableselect = Input::get('tableselect');
         $this->tableSorter->setupSorter(secure_url('/ma/agent_update.html'), $fields, $searchFor, $sf, $sm, $tableselect, $search);
         
 
         // $listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id'));
        $listAgent = $this->helpdesk->listMabyUpline($this->session->get('master_agent_id'));


switch ($tableselect) {
    case 'agent_id':
      $tablename="agent.id";
    break;
    case 'name':
      $tablename="agent_profile.fullname";
    break; 
    case 'username':
      $tablename="agent.username";
    break;
    case 'master_agent':
      $tablename="agent.master_agent_id";
    break;
    case 'email':
      $tablename="agent_profile.email";
    break;
     case 'phone':
      $tablename="agent_profile.phone_number";
    break; 
    default:
      $tablename="";
    break;
}



        if(!empty($search) && (!empty($tableselect))) {
          

            $someModel = new  AgentModel;
            $someModel->setConnection($this->conn); 
            $q = $someModel
                    ->whereIn('agent.master_agent_id',  $listAgent)
                    ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
                    ->leftJoin('master_agent_profile','master_agent_profile.master_agent_id','agent.master_agent_id')
                    ->selectRaw('agent.id as agent_id, agent.status,  agent_profile.fullname  as agent_name,username,agent_profile.email,agent_profile.phone_number,agent.master_agent_id,ifnull(master_agent_profile.fullname, master_agent_profile.master_agent_id) as m_agent_name,agent_profile.npwp_number,agent_profile.id_card_type ,agent_profile.id_card_number, agent_profile.address,agent_profile.city_name,daily_trx_total_price_max,account_buffer')
                    ->where(function ($query) use ($tablename,$search) { 
                        $query->where( $tablename, 'like', '%'.$search.'%'); 
                    })->get();

        } else {
            $someModel = new  AgentModel;
            $someModel->setConnection($this->conn); 
            $q = $someModel ->whereIn('agent.master_agent_id',  $listAgent)
                    ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
                    ->leftJoin('master_agent_profile','master_agent_profile.master_agent_id','agent.master_agent_id')
                    ->selectRaw('agent.id as agent_id, agent.status,  agent_profile.fullname  as agent_name,username,agent_profile.email,agent_profile.phone_number,agent.master_agent_id,ifnull(master_agent_profile.fullname, master_agent_profile.master_agent_id) as m_agent_name,agent_profile.npwp_number,agent_profile.id_card_type ,agent_profile.id_card_number, agent_profile.address,agent_profile.city_name,daily_trx_total_price_max,account_buffer')
            ->get();
        }
        
        // $this->tableSorter->setupPaging($q, $pg);
        // $this->viewData['sorter'] = $this->tableSorter;
 
        $tx = microtime(true);
        $micro = sprintf("%06d",($tx - floor($tx)) * 1000000);
        $theTime = new \DateTime( date('Y-m-d H:i:s.'.$micro, $tx) );
        $theTimestamp = $theTime->format("YmdHisu");

        $filename = "Agent_".$this->session->get('master_agent_id')."_".$theTimestamp.".csv";

            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('agent_id', 'username', 'agent_name' ,'account_buffer','master_agent_id','master_agent_name', 'email' , 'phone_number' ,'daily_trx_total_price_max' , 'npwp_number' , 'id_card_type' , 'id_card_number' ,'address','status'));
         
            foreach($q as $row) {
                fputcsv($handle, array( strtoupper(UtilCommon::intToBase36($row['agent_id'])),$row['username'], $row['agent_name'] , $row['account_buffer']  , $row['master_agent_id'] , $row['m_agent_name'] , $row['email'], $row['phone_number'] ,$row['daily_trx_total_price_max'], $row['npwp_number'] , $row['id_card_type'] ,  $row['id_card_number'] ,  $row['address'], $row['status']));
            }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            return response()->download($filename, $filename , $headers);
    }





    //  -------------   komisi  ----------------

    //=============================================


function listKomisiMonthly(Request $request) {
      
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', '');
        $sm = Input::get('sm', '');
        $tableselect="";
        $search = Input::get('search'); 
        
        $fields = [
            ['master_agent_id', 'Agent ID'],
            ['master_agent_name', ' Agent Name'],
            ['master_agent_name', 'Comission Year'],
            ['master_agent_parent_id', 'Month'],
            ['email', 'Total Trx'],
            ['email', 'Total Komisi'],
            ['phone_number', 'Status']

        ]; 
          $tableselect = Input::get('tableselect');   
    try {
         $this->tableSorter->setupSorter(secure_url('/ma/komisi_agen.html'), $fields, $searchFor, $sf, $sm, $tableselect, $search); 

        $listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id')); 
 
            $someModel = new  ReportCommissionAgenMonthly;
            $someModel->setConnection($this->conn); 
             
            
        if(empty($sm) && (!empty($sf))  && (empty($search)) && (empty($tableselect))) {
          
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_commission_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_commission_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_commission_received,report_commission_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_year','=',$sf);
        } else if(!empty($sm) && (empty($sf))  && (empty($search)) && (empty($tableselect))) {
          
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_commission_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_commission_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_commission_received,report_commission_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_month','=',$sm);
        } else if(!empty($sm) && (!empty($sf)) && (empty($search)) && (empty($tableselect))) {
          
            $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_commission_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_commission_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_commission_received,report_commission_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_month','=',$sm)            
            ->where('report_year','=',$sf);
        } else if(!empty($search) && (!empty($tableselect))  && (empty($sm)) && (empty($sf))) {
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_commission_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_commission_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_commission_received,report_commission_agent_monthly.status,status_updated_at,status_updated_info')
            ->where(function ($query) use ($tableselect,$search) { 
                        $query->where( $tableselect, 'like', ''.$search.'%');
                    }); 
        } else if(!empty($search) && (!empty($tableselect)) && (!empty($sm)) && (!empty($sf))) {
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_commission_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
           ->selectRaw('agent.white_label_id,report_commission_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_commission_received,report_commission_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_month','=',$sm)            
            ->where('report_year','=',$sf)  
            ->where(function ($query) use ($tableselect,$search) { 
                        $query->where( $tableselect, 'like', ''.$search.'%');
                    });
        } else {
            $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_commission_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_commission_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_commission_received,report_commission_agent_monthly.status,status_updated_at,status_updated_info');
 
        } 
         
            $someModel = new  ReportCommissionAgenMonthly;
            $someModel->setConnection($this->conn); 
            $this->viewData['listmonth'] = $someModel
            ->distinct('report_month')->orderBy('report_month','asc')
            ->selectRaw('report_month')->get(); 


            $this->viewData['listyear'] = $someModel
            ->distinct('report_year')->orderBy('report_year','asc')
            ->selectRaw('report_year')->get(); 


        $this->tableSorter->setupPaging($q, $pg);
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
        $this->viewData['sf'] = $sf;
        $this->viewData['sm'] = $sm;
        
        $this->viewData['sorter'] = $this->tableSorter; 
        } catch ( Exception $e ) {
            Log::info("AgentController::index exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        return view('admin.agent.komisi_monhtly', $this->viewData);
    }

function listKomisiDetail($wl,$ag,$y,$m) {
      
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('year');
        $sm = Input::get('month');
        $tableselect="";
        $year="";
        $month=""; 
        $search = Input::get('search');
        $year =  $sf;//Input::get('year');
        $month =$sm;// Input::get('month');
        $refund = "N";
        $fields = [
            ['master_agent_id', 'Log id Trx'],  
            ['master_agent_parent_id', 'Product ID'],
            ['master_agent_parent_id', 'Product Code'],
            ['master_agent_parent_id', 'Product Name'],
            ['email', 'Trx Date Time'],
            ['phone_number', 'Comission Receive']
        ]; 
            
    try {
         $this->tableSorter->setupSorter(url('/ma/komisiagendet_'.$wl.'_'.$ag.'_'.$y.'_'.$m.'.html'), $fields, $searchFor, $sf, $sm, $tableselect, $search); 

        // $listMAgent = $this->helpdesk->getdownlineMA($this->session->get('master_agent_id')); 
          // Log::info("list ma ".json_encode($listMAgent));
      

      $tablema_comissionAgent = "befintechreport.report_commission_agent_".$wl."_".$y."_".$m;

            $productLog = new ReportTable;
            $productLog->setConnection($this->conn);  
            $productLog->setTable($tablema_comissionAgent);

             $q = $productLog
            ->leftJoin('agent', 'agent.id', '=',  $tablema_comissionAgent.'.agent_id')
            ->leftJoin('agent_profile','agent.id', '=', 'agent_profile.agent_id')  
            ->where($tablema_comissionAgent.'.agent_id','=',  $ag) 
            ->where('is_refund','=',$refund) 
            ->selectRaw('log_trx_agent_id,agent.id as agent_id,agent_profile.fullname,product_id,product_code,product_name,trx_datetime,trx_commission_received');
  
         
        $this->tableSorter->setupPaging($q, $pg);
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;        
        $this->viewData['year'] = $year;        
        $this->viewData['month'] = $month;        
        $this->viewData['sorter'] = $this->tableSorter; 
 
        $this->viewData['search'] = $search;
        $this->viewData['agents'] = $ag;
        $this->viewData['sf'] = $sf;
        $this->viewData['sm'] = $sm;
        $this->viewData['export'] = '/ma/exportkomisiagendet_'.$wl.'_'.$ag.'_'.$y.'_'.$m.'.html';
         
        // log::info("data=".json_encode($this->viewData));
             


        } catch ( Exception $e ) {
            Log::info("AgentController::listKomisiDetail exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        return view('admin.agent.komisi_detail', $this->viewData);
    }

function exportListKomisiDetail($wl,$ag,$y,$m) {
      
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('year');
        $sm = Input::get('month');
        $tableselect="";
        $year="";
        $month=""; 
        $search = Input::get('search');
        $year =  $sf;//Input::get('year');
        $month =$sm;// Input::get('month');
        $refund="N";

        $fields = [
            ['master_agent_id', 'Log id Trx'],
            ['master_agent_id', 'Master Agent ID'],
            ['master_agent_name', 'Agent ID'],            ['master_agent_name', 'Agent Name'],

            ['master_agent_parent_id', 'Product ID'],
            ['master_agent_parent_id', 'Product Code'],
            ['master_agent_parent_id', 'Product Name'],
            ['email', 'Trx Date Time'],
            ['phone_number', 'Comission Receive']
        ]; 
            
    try { 
      
      $tablema_comissionAgent = "befintechreport.report_commission_agent_".$wl."_".$y."_".$m;

            $productLog = new ReportTable;
            $productLog->setConnection($this->conn);  
            $productLog->setTable($tablema_comissionAgent);

             $q = $productLog
            ->leftJoin('agent', 'agent.id', '=',  $tablema_comissionAgent.'.agent_id')
            ->leftJoin('agent_profile','agent.id', '=', 'agent_profile.agent_id')  
            ->where($tablema_comissionAgent.'.agent_id','=',  $ag) 
            ->where('is_refund','=',$refund) 
            ->selectRaw('log_trx_agent_id,agent.id as agent_id,agent_profile.fullname,product_id,product_code,product_name,trx_datetime,trx_commission_received')->get();
  
         
        
        $filename = "report_list_komisi_agen_detail.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('Log Trx Id' ,'  Agent Id', '  Agent Name'  ,'Product Id','Product Code','Product Name','Trx Date Time','Total Komisi'));


            foreach($q as $row) {
                fputcsv($handle, array($row['log_trx_agent_id'],$row['agent_id'], $row['fullname'] , $row['product_id'] , $row['product_code'],$row['product_name'], $row['trx_datetime'],$row['trx_commission_received']));
            }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            return response()->download($filename, 'report_list_komisi_agen_detail.csv', $headers);

        } catch ( Exception $e ) {
            Log::info("AgentController::index exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        


            
           
    }





 function exportlistKomisiMonthly(Request $request) {
        
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', '');
        $sm = Input::get('sm', '');
        $tableselect="";
        $search = Input::get('search'); 
        
        $fields = [
            ['master_agent_id', 'Agent ID'],
            ['master_agent_name', ' Agent Name'],
            ['master_agent_name', 'Comission Year'],
            ['master_agent_parent_id', 'Month'],
            ['email', 'Total Trx'],
            ['email', 'Total Komisi'],
            ['phone_number', 'Status']

        ]; 
          $tableselect = Input::get('tableselect');  
   
        $listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id')); 
 
            $someModel = new  ReportCommissionAgenMonthly;
            $someModel->setConnection($this->conn); 
             
            
        if(empty($sm) && (!empty($sf))  && (empty($search)) && (empty($tableselect))) {
          
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_commission_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_commission_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_commission_received,report_commission_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_year','=',$sf)->get();
        } else if(!empty($sm) && (empty($sf))  && (empty($search)) && (empty($tableselect))) {
          
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_commission_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_commission_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_commission_received,report_commission_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_month','=',$sm)->get();
        } else if(!empty($sm) && (!empty($sf)) && (empty($search)) && (empty($tableselect))) {
          
            $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_commission_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_commission_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_commission_received,report_commission_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_month','=',$sm)            
            ->where('report_year','=',$sf)->get();
        } else if(!empty($search) && (!empty($tableselect))  && (empty($sm)) && (empty($sf))) {
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_commission_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_commission_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_commission_received,report_commission_agent_monthly.status,status_updated_at,status_updated_info')
            ->where(function ($query) use ($tableselect,$search) { 
                        $query->where( $tableselect, 'like', ''.$search.'%');
                    })->get(); 
        } else if(!empty($search) && (!empty($tableselect)) && (!empty($sm)) && (!empty($sf))) {
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_commission_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
           ->selectRaw('agent.white_label_id,report_commission_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_commission_received,report_commission_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_month','=',$sm)            
            ->where('report_year','=',$sf)  
            ->where(function ($query) use ($tableselect,$search) { 
                        $query->where( $tableselect, 'like', ''.$search.'%');
                    })->get();
        } else {
            $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_commission_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_commission_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_commission_received,report_commission_agent_monthly.status,status_updated_at,status_updated_info')->get();
 
        } 
         
            $someModel = new  ReportCommissionAgenMonthly;
            $someModel->setConnection($this->conn); 
            $this->viewData['listmonth'] = $someModel
            ->distinct('report_month')->orderBy('report_month','asc')
            ->selectRaw('report_month')->get(); 


            $this->viewData['listyear'] = $someModel
            ->distinct('report_year')->orderBy('report_year','asc')
            ->selectRaw('report_year')->get(); 
 

            
            $filename = "report_list_komisi_agen.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('agent_id' , 'agent_name' ,'year' , 'month' , 'total trx' ,'total komisi' , 'status'));


            foreach($q as $row) {
                fputcsv($handle, array($row['agent_id'], $row['fullname'] , $row['report_year']   , $row['report_month'],  $row['total_trx_event'], $row['total_commission_received'] , $row['report_commission_agent_monthly.status']));
            }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            return response()->download($filename, 'report_list_komisi_agen.csv', $headers);
    }



    //  -------------   bonus  ----------------

    //=============================================


function listBonusMonthly(Request $request) {
      
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', '');
        $sm = Input::get('sm', '');
        $tableselect="";
        $search = Input::get('search'); 
        $tablename="";
        $fields = [
            ['master_agent_id', 'Agent ID'],
            ['master_agent_name', ' Agent Name'],
            ['master_agent_name', 'Comission Year'],
            ['master_agent_parent_id', 'Month'],
            ['email', 'Total Trx'],
            ['email', 'Total Bonus'],
            ['phone_number', 'Status']

        ]; 
          $tableselect = Input::get('tableselect');  
    try {
         $this->tableSorter->setupSorter(url('/ma/bonus_agen.html'), $fields, $searchFor, $sf, $sm, $tableselect, $search); 

        $listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id')); 
 
            $someModel = new  ReportBonusAgenMonthly;
            $someModel->setConnection($this->conn); 
         


    switch ($tableselect) {
        case 'id':
        $tablename="report_bonus_agent_monthly.agent_id";
        break;
        case 'name':
        $tablename="agent_profile.fullname";
        break;
        
        default:
           $tablename="";
        break;
    }
 
            
        if(empty($sm) && (!empty($sf))  && (empty($search)) && (empty($tableselect))) {
          
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_bonus_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_bonus_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_bonus_received,report_bonus_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_year','=',$sf);
        } else if(!empty($sm) && (empty($sf))  && (empty($search)) && (empty($tableselect))) {
          
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_bonus_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_bonus_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_bonus_received,report_bonus_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_month','=',$sm);
        } else if(!empty($sm) && (!empty($sf)) && (empty($search)) && (empty($tableselect))) {
          
            $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_bonus_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_bonus_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_bonus_received,report_bonus_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_month','=',$sm)            
            ->where('report_year','=',$sf);
        } else if(!empty($search) && (!empty($tableselect))  && (empty($sm)) && (empty($sf))) {
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_bonus_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_bonus_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_bonus_received,report_bonus_agent_monthly.status,status_updated_at,status_updated_info')
            ->where(function ($query) use ($tablename,$search) { 
                        $query->where( $tablename, 'like', '%'.$search.'%');
                    }); 
        } else if(!empty($search) && (!empty($tableselect)) && (!empty($sm)) && (!empty($sf))) {
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_bonus_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
           ->selectRaw('agent.white_label_id,report_bonus_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_bonus_received,report_bonus_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_month','=',$sm)            
            ->where('report_year','=',$sf)  
            ->where(function ($query) use ($tablename,$search) { 
                        $query->where( $tablename, 'like', '%'.$search.'%');
                    });
        } else {
            $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_bonus_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_bonus_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_bonus_received,report_bonus_agent_monthly.status,status_updated_at,status_updated_info');
 
        } 
         
            $someModel = new  ReportBonusAgenMonthly;
            $someModel->setConnection($this->conn); 

            $this->viewData['listmonth'] = $someModel
            ->distinct('report_month')->orderBy('report_month','asc')
            ->selectRaw('report_month')->get(); 
 
            $this->viewData['listyear'] = $someModel
            ->distinct('report_year')->orderBy('report_year','asc')
            ->selectRaw('report_year')->get(); 

            
// log::info("dataview=".json_encode($this->viewData));



        $this->tableSorter->setupPaging($q, $pg);
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
        $this->viewData['sf'] = $sf;
        $this->viewData['sm'] = $sm;
        
        $this->viewData['sorter'] = $this->tableSorter; 
        } catch ( Exception $e ) {
            Log::info("AgentController::index exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        return view('admin.agent.bonus_monhtly', $this->viewData);
    }

function listBonusDetail($wl,$ag,$y,$m) {
      
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('year');
        $sm = Input::get('month');
        $tableselect="";
        $year="";
        $month=""; 
        $search = Input::get('search');
        $year =  $sf;//Input::get('year');
        $month =$sm;// Input::get('month');
        $refund="N";
        $fields = [
            ['master_agent_id', 'Log id Trx'],  
            ['master_agent_parent_id', 'Product ID'],
            ['master_agent_parent_id', 'Product Code'],
            ['master_agent_parent_id', 'Product Name'],
            ['email', 'Trx Date Time'],
            ['phone_number', 'Bonus Receive']
        ]; 
            
    try {
         $this->tableSorter->setupSorter(url('/ma/bonusagendet_'.$wl.'_'.$ag.'_'.$y.'_'.$m.'.html'), $fields, $searchFor, $sf, $sm, $tableselect, $search); 

        // $listMAgent = $this->helpdesk->getdownlineMA($this->session->get('master_agent_id')); 
          // Log::info("list ma ".json_encode($listMAgent));
      

      $tablema_comissionAgent = "befintechreport.report_bonus_agent_".$wl."_".$y."_".$m;

            $productLog = new ReportTable;
            $productLog->setConnection($this->conn);  
            $productLog->setTable($tablema_comissionAgent);

             $q = $productLog
            ->leftJoin('agent', 'agent.id', '=',  $tablema_comissionAgent.'.agent_id')
            ->leftJoin('agent_profile','agent.id', '=', 'agent_profile.agent_id')  
            ->where($tablema_comissionAgent.'.agent_id','=',  $ag)    
            ->where('is_refund','=',$refund) 
            ->selectRaw('log_trx_agent_id,agent.id as agent_id,agent_profile.fullname,product_id,product_code,product_name,trx_datetime,trx_bonus_received');
  
         
        $this->tableSorter->setupPaging($q, $pg);
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;        
        $this->viewData['year'] = $year;        
        $this->viewData['month'] = $month;        
        $this->viewData['sorter'] = $this->tableSorter; 
 
        $this->viewData['search'] = $search;
        $this->viewData['agents'] = $ag;
        $this->viewData['sf'] = $sf;
        $this->viewData['sm'] = $sm;
        $this->viewData['export'] = '/ma/exportbonusagendet_'.$wl.'_'.$ag.'_'.$y.'_'.$m.'.html';
         
        // log::info("data=".json_encode($this->viewData));
             


        } catch ( Exception $e ) {
            Log::info("AgentController::listBonusDetail exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        return view('admin.agent.bonus_detail', $this->viewData);
    }

function exportListBonusDetail($wl,$ag,$y,$m) {
      
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('year');
        $sm = Input::get('month');
        $tableselect="";
        $year="";
        $month=""; 
        $search = Input::get('search');
        $year =  $sf;//Input::get('year');
        $month =$sm;// Input::get('month');
        $refund="N";

        $fields = [
            ['master_agent_id', 'Log id Trx'],
            ['master_agent_id', 'Master Agent ID'],
            ['master_agent_name', 'Agent ID'],            ['master_agent_name', 'Agent Name'],

            ['master_agent_parent_id', 'Product ID'],
            ['master_agent_parent_id', 'Product Code'],
            ['master_agent_parent_id', 'Product Name'],
            ['email', 'Trx Date Time'],
            ['phone_number', 'Comission Receive']
        ]; 
            
    try { 
      
      $tablema_comissionAgent = "befintechreport.report_bonus_agent_".$wl."_".$y."_".$m;

            $productLog = new ReportTable;
            $productLog->setConnection($this->conn);  
            $productLog->setTable($tablema_comissionAgent);

             $q = $productLog
            ->leftJoin('agent', 'agent.id', '=',  $tablema_comissionAgent.'.agent_id')
            ->leftJoin('agent_profile','agent.id', '=', 'agent_profile.agent_id')  
            ->where($tablema_comissionAgent.'.agent_id','=',  $ag)     
            ->where('is_refund','=',$refund) 
            ->selectRaw('log_trx_agent_id,agent.id as agent_id,agent_profile.fullname,product_id,product_code,product_name,trx_datetime,trx_bonus_received')->get();
  
         
        
        $filename = "report_list_bonus_agen_detail.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('Log Trx Id' ,'  Agent Id', '  Agent Name'  ,'Product Id','Product Code','Product Name','Trx Date Time','Total Bonus'));


            foreach($q as $row) {
                fputcsv($handle, array($row['log_trx_agent_id'],$row['agent_id'], $row['fullname'] , $row['product_id'] , $row['product_code'],$row['product_name'], $row['trx_datetime'],$row['trx_bonus_received']));
            }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            return response()->download($filename, 'report_list_bonus_agen_detail.csv', $headers);

        } catch ( Exception $e ) {
            Log::info("AgentController::index exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        


            
           
    }





 function exportlistBonusMonthly(Request $request) {
        
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', '');
        $sm = Input::get('sm', '');
        $tableselect="";
        $tablename="";
        $search = Input::get('search'); 
        
        $fields = [
            ['master_agent_id', 'Agent ID'],
            ['master_agent_name', ' Agent Name'],
            ['master_agent_name', 'Comission Year'],
            ['master_agent_parent_id', 'Month'],
            ['email', 'Total Trx'],
            ['email', 'Total Bonus'],
            ['phone_number', 'Status']

        ]; 
          $tableselect = Input::get('tableselect');  
   
        $listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id')); 
 
            $someModel = new  ReportBonusAgenMonthly;
            $someModel->setConnection($this->conn); 
        
    switch ($tableselect) {
        case 'id':
        $tablename="report_bonus_agent_monthly.agent_id";
        break;
        case 'name':
        $tablename="agent_profile.fullname";
        break;
        
        default:
           $tablename="";
        break;
    }     
            
        if(empty($sm) && (!empty($sf))  && (empty($search)) && (empty($tableselect))) {
          
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_bonus_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_bonus_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_bonus_received,report_bonus_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_year','=',$sf)->get();
        } else if(!empty($sm) && (empty($sf))  && (empty($search)) && (empty($tableselect))) {
          
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_bonus_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_bonus_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_bonus_received,report_bonus_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_month','=',$sm)->get();
        } else if(!empty($sm) && (!empty($sf)) && (empty($search)) && (empty($tableselect))) {
          
            $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_bonus_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_bonus_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_bonus_received,report_bonus_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_month','=',$sm)            
            ->where('report_year','=',$sf)->get();
        } else if(!empty($search) && (!empty($tableselect))  && (empty($sm)) && (empty($sf))) {
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_bonus_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_bonus_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_bonus_received,report_bonus_agent_monthly.status,status_updated_at,status_updated_info')
            ->where(function ($query) use ($tablename,$search) { 
                        $query->where( $tablename, 'like', '%'.$search.'%');
                    })->get(); 
        } else if(!empty($search) && (!empty($tableselect)) && (!empty($sm)) && (!empty($sf))) {
           $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_bonus_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
           ->selectRaw('agent.white_label_id,report_bonus_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_bonus_received,report_bonus_agent_monthly.status,status_updated_at,status_updated_info')
            ->where('report_month','=',$sm)            
            ->where('report_year','=',$sf)  
            ->where(function ($query) use ($tablename,$search) { 
                        $query->where( $tablename, 'like', '%'.$search.'%');
                    })->get();
        } else {
            $q = $someModel->whereIn('agent.id',  $listAgent)            
            ->leftJoin('agent', 'agent.id', '=', 'report_bonus_agent_monthly.agent_id')
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->selectRaw('agent.white_label_id,report_bonus_agent_monthly.agent_id,agent_profile.fullname,report_year,report_month, currency_code,total_trx_event,total_bonus_received,report_bonus_agent_monthly.status,status_updated_at,status_updated_info')->get();
 
        } 
         
            $someModel = new  ReportBonusAgenMonthly;
            $someModel->setConnection($this->conn); 
            $this->viewData['listmonth'] = $someModel
            ->distinct('report_month')->orderBy('report_month','asc')
            ->selectRaw('report_month')->get(); 


            $this->viewData['listyear'] = $someModel
            ->distinct('report_year')->orderBy('report_year','asc')
            ->selectRaw('report_year')->get(); 
 

            
            $filename = "report_list_bonus_agen.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('agent_id' , 'agent_name' ,'year' , 'month' , 'total trx' ,'total Bonus' , 'status'));


            foreach($q as $row) {
                fputcsv($handle, array($row['agent_id'], $row['fullname'] , $row['report_year']   , $row['report_month'],  $row['total_trx_event'], $row['total_bonus_received'] , $row['report_bonus_agent_monthly.status']));
            }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );
 
            return response()->download($filename, 'report_list_bonus_agen.csv', $headers);
    }





function getSmsInfo(Request $request) {
      
         // forbidden
          if($this->session->get('group') != 'ADMIN')
          {
            $this->viewData['msg'] = "This User is Forbidden";
            return view('errors.forbidden', $this->viewData);     
          }
          
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', '');
        $sm = Input::get('sm', '');
        $min = Input::get('min', '');
        $max = Input::get('max', '');
        $tablename="";
        $search = Input::get('search');
        
        $fields = [
            ['log_datetime', 'Log Date Time'],
            ['agent_id', 'Agent ID'],
            ['agent_name', 'Agent Name'],
            ['number_receiver', 'Phone Number'],
            ['email', 'Agent Email'],
            ['sms_message', 'SMS Message'],            
            ['delivery_status_desc', 'SMS Status'] 

        ];
          $tableselect = Input::get('tableselect');
         $this->tableSorter->setupSorter(secure_url('/ma/agent_sms.html'), $fields, $searchFor, $sf, $sm,$min,$max, $tableselect, $search);
       
        $listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id')); 
        switch ($tableselect) {
            case 'id':
                $tablename="agent_id";
                break;
            case 'name':
                $tablename="fullname";
                break;
            case 'email':
                $tablename="email";
                break;
            case 'phone':
                $tablename="number_receiver";
                break; 
             case 'detail':
                $tablename="sms_message";
                break; 
            
            default:
                $tablename="";
            break;
        }
      

      // forbidden
          if($this->session->get('group') != 'ADMIN')
          {
            $this->viewData['msg'] = "This User is Forbidden";
            return view('errors.forbidden', $this->viewData);     
          }



        $someModel = new  LogSmsSend;
        $someModel->setConnection($this->conn); 

         if(($search != '' && $tableselect != '') ){
               
         $q =  $someModel->where(function ($query) use ($tablename,$search) { 
                        $query->where( $tablename, 'like', '%'.$search.'%');
                    })->Join('agent_profile', 'agent_profile.phone_number', '=','log_sms_send.number_receiver' )
                ->selectRaw('log_datetime,number_receiver,sms_message,delivery_status_desc,agent_id,fullname,email ')->wherein('agent_id',$listAgent)
                ->orderBy('log_datetime','desc');

        }
        else
        {
             $q =  $someModel->Join('agent_profile', 'agent_profile.phone_number', '=','log_sms_send.number_receiver' )
                ->selectRaw('log_datetime,number_receiver,sms_message,delivery_status_desc,agent_id,fullname,email ')->wherein('agent_id',$listAgent)
                ->orderBy('log_datetime','desc');
        }
       
    
        
        $this->tableSorter->setupPaging($q, $pg);
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
        
        $this->viewData['sorter'] = $this->tableSorter;

        return view('admin.agent.sms', $this->viewData);
    }
    


    function listOperator(Request $request) {
      
        Log::info("START List Operator" );
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', '');
        $sm = Input::get('sm', '');
        $tableselect="";
        $search = Input::get('search');
        
        $fields = [
            ['agent_id', 'Agent ID'],
            ['agent_name', 'Agent Name'],
            ['master_agent_id', 'Workstation code'],
            ['master_agent_id', 'Workstation name'],
            ['master_agent_id', 'Active']

        ]; 
          $tableselect = Input::get('tableselect');  
    try {
         $this->tableSorter->setupSorter(secure_url('/ma/list_operator.html'), $fields, $searchFor, $sf, $sm, $tableselect, $search);  
          $listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id'));
 
switch ($tableselect) {
    case 'agent_id':
      $tablename="agent_workstations.agent_id";
    break;
    case 'name':
      $tablename="agent_profile.fullname";
    break; 
    case 'operator':
      $tablename="workstation_name";
    break; 
    default:
      $tablename="";
    break;
}

        if(!empty($search) && (!empty($tableselect))) {
          
            $someModel = new  AgentWorkstation;
            $someModel->setConnection($this->conn); 
            // $q = $someModel->whereIn('agent.id',  $listAgent)
            //         ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            //         ->join('master_agent_profile','master_agent_profile.master_agent_id','agent.master_agent_id')
            //         ->selectRaw('agent.id as agent_id,CONV(agent.id, 10, 36) as agent_base, agent.status, username,  agent_profile.fullname  as agent_name,agent_profile.email,agent_profile.phone_number,agent.master_agent_id,ifnull(master_agent_profile.fullname, master_agent_profile.master_agent_id) as m_agent_name,agent_profile.npwp_number,agent_profile.id_card_type ,agent_profile.id_card_number, agent_profile.address,agent_profile.city_name,daily_trx_total_price_max')
            //         ->where(function ($query) use ($tablename,$search) { 
            //             $query->where( $tablename, 'like', '%'.$search.'%');
            //         });

                    $q = $someModel->leftJoin('agent_profile', 'agent_profile.agent_id', '=', 'agent_workstations.agent_id')
                    ->whereIn('agent_workstations.agent_id',  $listAgent)->distinct()->where(function ($query) use ($tablename,$search) { 
                        $query->where( $tablename, 'like', '%'.$search.'%');
                    })
                    ->selectRaw('agent_workstations.id as workstation_id , agent_workstations.agent_id  , fullname ,workstation_code , workstation_name, active');


                    
        } else {
             // log::info(" data listAgent".json_encode($listAgent));
            $someModel = new  AgentWorkstation;
            $someModel->setConnection($this->conn); 
            $q = $someModel->leftJoin('agent_profile', 'agent_profile.agent_id', '=', 'agent_workstations.agent_id')
                    ->whereIn('agent_workstations.agent_id',  $listAgent)->distinct()
                    ->selectRaw('agent_workstations.id as workstation_id , agent_workstations.agent_id  , fullname ,workstation_code , workstation_name, active');
                     // log::info(" data listAgent".json_encode($q));
        }
        

        //$q = $someModel->whereIn('id',  ['1544844145','1544756392','1544775157','1548735594']);
        $this->tableSorter->setupPaging($q, $pg);
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
        
        $this->viewData['sorter'] = $this->tableSorter;

        // log::info(" data".json_encode($this->viewData));
        } catch ( Exception $e ) {
            Log::info("AgentController::index exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        return view('admin.agent.listoperator', $this->viewData);
    }


 function UpdtOperator(Request $request) { 
        Log::info("START Upd Operator" );
        $id = $request->input('id');
        $val = $request->input('val'); 
       
 
        try {

        if ($id == '') { $this->notif->addMessage('id is required'); }
        if ($val == '') { $this->notif->addMessage('value is required'); } 
               

                  if ($this->notif->code == \App\Http\Helpers\NotificationHelper::CODE_OK) {
                    try {
                         
                            $this->helpdesk->putQueueLogActivity( $this->session->get('username'), $this->cms_type , "UPDATE-OPERATOR" , "operator_id=" . $id." value :".$val );
                     
                

                                $AgentWorkstation = new  AgentWorkstation;
                                $AgentWorkstation->setConnection($this->conn); 
                                $B2 = $AgentWorkstation->where('id', $id)
                                ->update([
                                'active' => $val ,
                                'updated_at' => date('Y-m-d H:i:s') 
                                ]); 
                                                               
                                 


                          } catch (Exception $e) {
                            // DB::rollBack();
                            $this->notif->addMessage($e->getMessage());
                          }
                    }
             


        } catch ( Exception $e ) {
        Log::info("AgentController::UpdtOperator exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        
        return response()->json($this->notif->build());
    }  

 function updateVerified(Request $request) { 
        Log::info("START Verifikasi" );
        $id = $request->input('id_gerai');
         
        try {

        if ($id == '') { $this->notif->addMessage('id is required'); }
        // if ($val == '') { $this->notif->addMessage('value is required'); } 
               

                  if ($this->notif->code == \App\Http\Helpers\NotificationHelper::CODE_OK) {
                    try {
                         
                            $this->helpdesk->putQueueLogActivity( $this->session->get('username'), $this->cms_type , "VERIFIED-GERAI" , "gerai_id=" . $id );
                     
                

                                $AgentWorkstation = new  Gerai;
                                $AgentWorkstation->setConnection($this->conn); 
                                $B2 = $AgentWorkstation->where('id', $id)
                                ->update([
                                'status' => 'ACTIVE',
                                'active_approved_by' =>  $this->session->get('username') ,
                                'active_approved_at' => date('Y-m-d H:i:s') 
                                ]); 
                                                               
                                 


                          } catch (Exception $e) {
                            // DB::rollBack();
                            $this->notif->addMessage($e->getMessage());
                          }
                    }
             


        } catch ( Exception $e ) {
        Log::info("AgentController::UpdtOperator exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
        
        return response()->json($this->notif->build());
    }  
}
 
