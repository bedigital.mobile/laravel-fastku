<?php

namespace App\Http\Controllers\admin\cms;



use Log;
use Config;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\BDGBaseController;
use App\Models\admin\MainAgentModel;
use App\Models\admin\AgentModel;
use App\Models\admin\AgentProfileModel;
use App\Models\admin\MainAgentProfileModel;
use App\Models\LogSmsSend;
use App\Models\WhiteLabelUserUid;
use App\Models\Database\LogSmsSendHelp;
use App\Models\Database\AgentDevice;
use App\Models\Database\PushTokenAgents;
use App\BusinessLogic\HelpdeskBL; 
use App\Core\Util\UtilCommon;


class PushController extends BDGBaseController
{
    public function __construct() {
        parent::__construct();    
        $this->conn = Config::get ( 'webconf.cms.connectiondata_prod' );
        $this->connOri = Config::get ( 'webconf.cms.connection111' );
        $this->whitelabelID = Config::get ( 'webconf.cms.whitelabel' );
        $this->helpdeskBL = new HelpdeskBL;  
    }
     

    function pushWhitelabel(Request $request) {

                return view('admin.push.push_all', $this->viewData);

    }
    function pushMa(Request $request) {

         $listMAgent = $this->helpdeskBL->listMabyUpline($this->session->get('master_agent_id')); 
         // log::info("list MA=".json_encode($listMAgent));
         $someModel = new  MainAgentModel;
            $someModel->setConnection($this->conn); 
            $this->viewData['ma'] = $someModel->leftJoin('master_agent_profile', 'master_agent.id', '=', 'master_agent_profile.master_agent_id')
            ->orderBy('master_agent.id','asc') ->whereIn('master_agent.id',  $listMAgent)
            ->selectRaw('master_agent.id as master_agent_id,username,  ifnull(master_agent_profile.company_name, master_agent.username) as master_agent_name')
            ->get();
            // log::info(" push ma data =".json_encode($this->viewData['ma']));

                return view('admin.push.push_ma', $this->viewData);

    }
    function pushAgen(Request $request) {
  try { 
            $list=[];
            $listMAgent = $this->helpdeskBL->listMabyUpline($this->session->get('master_agent_id')); 

            $someModel = new   AgentModel;
            $someModel->setConnection($this->conn); 
            $liatagen  = $someModel->distinct()
            ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->Join('push_token_agents', 'agent.id', '=', 'push_token_agents.agent_id')
            ->whereIn('master_agent_id',  $listMAgent)
            ->orderBy('agent.id','asc')
            ->where('status','=','ACTIVE')
            ->selectRaw('agent.id, username,  ifnull( agent_profile.fullname,  agent.username) as  fullname')
            ->get();

            // foreach ($liatagen  as $r) {
            //     // $list['id']  = $r['id'];
            //     // array_push($list['id'],$r['id']);
            //     //
            //     array_push($list,$r['id']); 
            //     array_push($list,strtoupper(UtilCommon::intToBase36($r['id'])));
            //     array_push($list,$r['fullname']);
                 
            // }
           

            $this->viewData['agen'] =$liatagen;
            // log::info(" push data =".json_encode($this->viewData)); 

            } catch ( Exception $e ) {
                Log::info("AgentController::doInquiryApprovalPage exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
            } 
            // array( strtoupper(UtilCommon::intToBase36($row['agent_id']))
            // $this->viewData['agen'] = json_encode($this->viewData['agen']);
            return view('admin.push.push_agen', $this->viewData);

    }
   function sendPushTextWL(Request $request) {
        $title = $request->input('title');
        $txtbody = $request->input('txtbody');
        $optionsRadios = $request->input('optionsRadios');
        $send_datetime = $request->input('send_datetime');  
         $timeDelay ="";


        if ($title == '') { $this->notif->addMessage('title is required'); }
        if ($txtbody == '') { $this->notif->addMessage('txtbody is required'); }
        if ($send_datetime != '') { 
            $date =   date_create($send_datetime);
            $datepushdelay = date_format($date, 'Y-m-d H:i:s');
            // log::info("tgl =".$datepushdelay );
        }

        if ($this->notif->code == \App\Http\Helpers\NotificationHelper::CODE_OK) {
            
            // $whitelabelID = 1234567890;
    $arrMsg=[];
    $arrMsg['text'] = " ";
    $arrMsg['title'] = $title;//"PosFin - Push Text - ALLAGENTINWL";
    $arrMsg['body'] = $txtbody;//"Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    


    if($optionsRadios == "later")
    {
        $timeDelay = $send_datetime;
    }

    $list = $this->helpdeskBL->sendPushToAllAgentInWhitelabel($this->whitelabelID,$arrMsg,$timeDelay); 


 
            // if(!empty($q) && !empty($r))
            // {
            //     try { 
            //     $this->helpdeskBL->putQueueLogActivity( $this->session->get('username'), "HELPDESK Agency", "UPDATE-AGENT", "agent_id=" . $agent_id." ma :".$q->master_agent_id." to ".$master_agent_id.", status :". $q->status." to ".$status.", ttl_max_trx ".$q->daily_trx_total_price_max." to ".$total_price_max." name :".$r->fullname." to ".$fullName.", email :".$r->email." to ".$email." phone :".$r->phone_number." to ".$phone );

            //     } catch ( Exception $e ) {
            //         Log::info("AgentController::doInquiryApprovalPage exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
            //     }          
 
            // }

             


        }
        
        return response()->json($this->notif->build());
    }
    function sendPushTextMa(Request $request) {
        $user_ma = $request->input('user_ma');
        $title = $request->input('title');
        // $txtbody = $request->input('txtbody');  
        $txtbody = $request->input('editor');
        $txttext = $request->input('body');



        $optionsRadios = $request->input('optionsRadios');
        $send_datetime = $request->input('send_datetime');  
        $timeDelay ="";


        if ($user_ma == '') { $this->notif->addMessage('MA is required'); }
        if ($title == '') { $this->notif->addMessage('title is required'); }
        if ($txtbody == '') { $this->notif->addMessage('txtbody is required'); }
        if ($send_datetime != '') { 
            $date =   date_create($send_datetime);
            $datepushdelay = date_format($date, 'Y-m-d H:i:s');
            // log::info("tgl =".$datepushdelay );
        }

        if ($this->notif->code == \App\Http\Helpers\NotificationHelper::CODE_OK) {
            
            // $whitelabelID = 1234567890;
    $arrMsg=[];
    $arrMsg['text'] = $txtbody;
    $arrMsg['title'] = $title;//"PosFin - Push Text - ALLAGENTINWL";
    $arrMsg['body'] = $txttext;//"Selamat siang agen PosFin. Tingkatkan terus transaksi Anda.";
    
 

    if($optionsRadios == "later")
    {
        $timeDelay = $send_datetime;
    }


    $agent_id = $this->helpdeskBL->listAgentbyUplinebyID($user_ma);
      // log::info("agent_id =".json_encode($agent_id));
    // $list = $this->helpdeskBL->sendPushToAllAgentInMasterAgent($this->whitelabelID,$arrMsg,$user_ma,$timeDelay); 

    $list = $this->helpdeskBL->sendPushToSelectedAgent($this->whitelabelID,$arrMsg,$agent_id,$timeDelay); 

 
            // if(!empty($q) && !empty($r))
            // {
            //     try { 
            //     $this->helpdeskBL->putQueueLogActivity( $this->session->get('username'), "HELPDESK Agency", "UPDATE-AGENT", "agent_id=" . $agent_id." ma :".$q->master_agent_id." to ".$master_agent_id.", status :". $q->status." to ".$status.", ttl_max_trx ".$q->daily_trx_total_price_max." to ".$total_price_max." name :".$r->fullname." to ".$fullName.", email :".$r->email." to ".$email." phone :".$r->phone_number." to ".$phone );

            //     } catch ( Exception $e ) {
            //         Log::info("AgentController::doInquiryApprovalPage exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
            //     }          
 
            // }

             


        }
        
        return response()->json($this->notif->build());
    }
 function sendPushTextAgen(Request $request) {
        $agent_id = $request->input('agent_id');
        $title = $request->input('title'); 
        $txtbody = $request->input('editor');
        $txttext = $request->input('body');
        $optionsRadios = $request->input('optionsRadios');
        $send_datetime = $request->input('send_datetime');  
        $timeDelay ="";


        if ($agent_id == '') { $this->notif->addMessage('agent_id is required'); }
        if ($title == '') { $this->notif->addMessage('title is required'); }
        if ($txtbody == '') { $this->notif->addMessage('push text is required'); }
        if ($txttext == '') { $this->notif->addMessage('push Body is required'); }
        if ($send_datetime != '') { 
            $date =   date_create($send_datetime);
            $datepushdelay = date_format($date, 'Y-m-d H:i:s');
            log::info("tgl =".$datepushdelay );
        }

        if ($this->notif->code == \App\Http\Helpers\NotificationHelper::CODE_OK) {
            
            // $whitelabelID = 1234567890;
    $arrMsg=[];
    // $arrMsg['text'] = " ";
    $arrMsg['title'] = $title;    
    $arrMsg['body'] = $txttext; 
    $arrMsg['text'] = $txtbody; 
    
 

    if($optionsRadios == "later")
    {
        $timeDelay = $send_datetime;
    }

    $list = $this->helpdeskBL->sendPushToSelectedAgent($this->whitelabelID,$arrMsg,$agent_id,$timeDelay); 
 
 
            // if(!empty($q) && !empty($r))
            // {
            //     try { 
            //     $this->helpdeskBL->putQueueLogActivity( $this->session->get('username'), "HELPDESK Agency", "UPDATE-AGENT", "agent_id=" . $agent_id." ma :".$q->master_agent_id." to ".$master_agent_id.", status :". $q->status." to ".$status.", ttl_max_trx ".$q->daily_trx_total_price_max." to ".$total_price_max." name :".$r->fullname." to ".$fullName.", email :".$r->email." to ".$email." phone :".$r->phone_number." to ".$phone );

            //     } catch ( Exception $e ) {
            //         Log::info("AgentController::doInquiryApprovalPage exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
            //     }          
 
            // }

             


        }
        
        return response()->json($this->notif->build());
    }

 function exportAgen(Request $request) { 

        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', '');
        $sm = Input::get('sm', '');
      
        $search = Input::get('search');
        
        $fields = [
            ['agent_id', 'Agent ID'],
            ['agent_name', 'Agent Name'],
            ['master_agent_id', 'Master Agent Id'],
            ['email', 'Agent Email'],
            ['phone_number', 'Phone Number'],            
            ['daily_trx_total_price_max', 'Daily Tot Max Trx'],
            ['npwp_number', 'NPWP'],
            ['address', 'Alamat'],
            ['agent.status', 'Status']

        ];
          $tableselect = Input::get('tableselect');
         $this->tableSorter->setupSorter(url('/agency/agent_update.html'), $fields, $searchFor, $sf, $sm, $tableselect, $search);
        

        /*  if($tableselect == "downline")
            { 
                 $listdownlineAgent = $this->helpdeskBL->listAgentbyUpline($search);
                 // log::info("list agent".json_encode($listdownlineAgent));
                //  if(!empty($listdownlineAgent)) 
                // {
                    
                    $q = $someModel->whereIn('agent.username',  $listdownlineAgent)
                    ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
                    ->join('master_agent_profile','master_agent_profile.master_agent_id','agent.master_agent_id')
                    ->selectRaw('agent.id as agent_id, agent.status,  agent_profile.fullname as agent_name, agent.username, agent_profile.email,agent_profile.phone_number,agent.master_agent_id,ifnull(master_agent_profile.fullname, master_agent_profile.master_agent_id) as m_agent_name,agent_profile.npwp_number,agent_profile.id_card_type ,agent_profile.id_card_number, agent_profile.address,agent_profile.city_name,daily_trx_total_price_max');

                // }

            }
            else
            {


            $q = $someModel->where(function ($query) use ($tableselect,$search) { 
                        $query->where( $tableselect, 'like', '%'.$search.'%');
                    })
                    ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
                    ->join('master_agent_profile','master_agent_profile.master_agent_id','agent.master_agent_id')
                    ->selectRaw('agent.id as agent_id, agent.status, agent_profile.fullname as agent_name, agent.username,agent_profile.email,agent_profile.phone_number,agent.master_agent_id,ifnull(master_agent_profile.fullname, master_agent_profile.master_agent_id) as m_agent_name,agent_profile.npwp_number,agent_profile.id_card_type ,agent_profile.id_card_number, agent_profile.address,agent_profile.city_name,daily_trx_total_price_max');
            }
        } else {
            $someModel = new  AgentModel;
            $someModel->setConnection($this->conn); 
            $q = $someModel->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->join('master_agent_profile','master_agent_profile.master_agent_id','agent.master_agent_id')
                    ->selectRaw('agent.id as agent_id, agent.status,  agent_profile.fullname as agent_name, agent.username,agent_profile.email,agent_profile.phone_number,agent.master_agent_id,ifnull(master_agent_profile.fullname, master_agent_profile.master_agent_id) as m_agent_name,agent_profile.npwp_number, agent_profile.id_card_type ,agent_profile.id_card_number ,agent_profile.address,agent_profile.city_name,daily_trx_total_price_max');
        }*/

        if(!empty($search) && (!empty($tableselect))) {
          
            $someModel = new  AgentModel;
            $someModel->setConnection($this->conn); 

             if($tableselect == "downline")
            { 
                 $listdownlineAgent = $this->helpdeskBL->listAgentbyUpline($search);
                     
                    $q = $someModel->whereIn('agent.username',  $listdownlineAgent)
                    ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
                    ->join('master_agent_profile','master_agent_profile.master_agent_id','agent.master_agent_id')
                    ->selectRaw('agent.id as agent_id, agent.status, ifnull(agent_profile.fullname, agent.username) as agent_name, agent.username,agent_profile.email,agent_profile.phone_number,agent.master_agent_id,ifnull(master_agent_profile.fullname, master_agent_profile.master_agent_id) as m_agent_name,agent_profile.npwp_number,agent_profile.id_card_type ,agent_profile.id_card_number, agent_profile.address,agent_profile.city_name,daily_trx_total_price_max,account_buffer')->get();
 

            }
            else
            {


            $q = $someModel->where(function ($query) use ($tableselect,$search) { 
                        $query->where( $tableselect, 'like', '%'.$search.'%');
                    })
                    ->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
                    ->join('master_agent_profile','master_agent_profile.master_agent_id','agent.master_agent_id')
                    ->selectRaw('agent.id as agent_id, agent.status, ifnull(agent_profile.fullname, agent.username) as agent_name , agent.username, agent_profile.email,agent_profile.phone_number,agent.master_agent_id,ifnull(master_agent_profile.fullname, master_agent_profile.master_agent_id) as m_agent_name,agent_profile.npwp_number,agent_profile.id_card_type ,agent_profile.id_card_number, agent_profile.address,agent_profile.city_name,daily_trx_total_price_max,account_buffer')->get();
            }
        } else {
            $someModel = new  AgentModel;
            $someModel->setConnection($this->conn); 
            $q = $someModel->leftJoin('agent_profile', 'agent.id', '=', 'agent_profile.agent_id')
            ->join('master_agent_profile','master_agent_profile.master_agent_id','agent.master_agent_id')
                    ->selectRaw('agent.id as agent_id, agent.status, ifnull(agent_profile.fullname, agent.username) as agent_name , agent.username, agent_profile.email,agent_profile.phone_number,agent.master_agent_id,ifnull(master_agent_profile.fullname, master_agent_profile.master_agent_id) as m_agent_name,agent_profile.npwp_number, agent_profile.id_card_type ,agent_profile.id_card_number ,agent_profile.address,agent_profile.city_name,daily_trx_total_price_max,account_buffer')->get();
        }
 
            
                $filename = "report.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('agent_id', 'username', 'agent_name' ,'account_buffer','master_agent_id','master_agent_name', 'email' , 'phone_number' ,'daily_trx_total_price_max' , 'npwp_number' , 'id_card_type' , 'id_card_number' ,'address','status'));
         
            foreach($q as $row) {
                fputcsv($handle, array( strtoupper(UtilCommon::intToBase36($row['agent_id'])),$row['username'], $row['agent_name'] , $row['account_buffer']  , $row['master_agent_id'] , $row['m_agent_name'] , $row['email'], $row['phone_number'] ,$row['daily_trx_total_price_max'], $row['npwp_number'] , $row['id_card_type'] ,  $row['id_card_number'] ,  $row['address'], $row['status']));
            }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            return response()->download($filename, 'report.csv', $headers);
    }


function getSmsInfo(Request $request) {
      
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', '');
        $sm = Input::get('sm', '');
        $min = Input::get('min', '');
        $max = Input::get('max', '');
        $tablename="";
        $search = Input::get('search');
        
        $fields = [
            ['log_datetime', 'Log Trx Date Time'],
            ['agent_id', 'Agent ID'],
            ['agent_name', 'Agent Name'],
            ['number_receiver', 'Phone Number'],
            ['email', 'Agent Email'],
            ['sms_message', 'SMS Message'],            
            ['delivery_status_desc', 'SMS Status'] 

        ];
          $tableselect = Input::get('tableselect');
         $this->tableSorter->setupSorter(url('/agency/agent_sms.html'), $fields, $searchFor, $sf, $sm,$min,$max, $tableselect, $search);
       
        switch ($tableselect) {
            case 'id':
                $tablename="agent_id";
                break;
            case 'name':
                $tablename="fullname";
                break;
            case 'email':
                $tablename="email";
                break;
            case 'phone':
                $tablename="number_receiver";
                break; 
            
            default:
                $tablename="";
            break;
        }
      

      // forbidden
          if($this->session->get('group') != 'ADMIN')
          {
            $this->viewData['msg'] = "This User is Forbidden";
            return view('errors.forbidden', $this->viewData);     
          }



        $someModel = new  LogSmsSend;
        $someModel->setConnection($this->conn); 

         if(($search != '' && $tableselect != '') ){
               
         $q =  $someModel->where(function ($query) use ($tablename,$search) { 
                        $query->where( $tablename, 'like', '%'.$search.'%');
                    })->leftJoin('agent_profile', 'agent_profile.phone_number', '=','log_sms_send.number_receiver' )
                ->selectRaw('log_datetime,number_receiver,sms_message,delivery_status_desc,agent_id,fullname,email ')
                ->orderBy('log_datetime','desc');

        }
        else
        {
             $q =  $someModel->leftJoin('agent_profile', 'agent_profile.phone_number', '=','log_sms_send.number_receiver' )
                ->selectRaw('log_datetime,number_receiver,sms_message,delivery_status_desc,agent_id,fullname,email ')
                ->orderBy('log_datetime','desc');
        }
       
    
        
        $this->tableSorter->setupPaging($q, $pg);
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
        
        $this->viewData['sorter'] = $this->tableSorter;

        return view('admin.agent.sms', $this->viewData);
    }
    


function getSmsInfoMa(Request $request) {
      
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', '');
        $sm = Input::get('sm', '');
        $min = Input::get('min', '');
        $max = Input::get('max', '');
        $tablename="";
        $search = Input::get('search');
        
        $fields = [
            ['log_datetime', 'Log Trx Date Time'],
            ['agent_id', 'Username'],
            ['agent_name', 'Agent Name'],
            ['number_receiver', 'Phone Number'],
            ['email', 'Agent Email'],
            ['sms_message', 'SMS Message'],            
            ['delivery_status_desc', 'SMS Status'] 

        ];
          $tableselect = Input::get('tableselect');
         $this->tableSorter->setupSorter(url('/agency/agent_sms_ma.html'), $fields, $searchFor, $sf, $sm,$min,$max, $tableselect, $search);
       
        switch ($tableselect) {
            case 'id':
                $tablename="agent_id";
                break;
            case 'name':
                $tablename="fullname";
                break;
            case 'email':
                $tablename="email";
                break;
            case 'phone':
                $tablename="number_receiver";
                break; 
            
            default:
                $tablename="";
            break;
        }
      

      // forbidden
          if($this->session->get('group') != 'ADMIN')
          {
            $this->viewData['msg'] = "This User is Forbidden";
            return view('errors.forbidden', $this->viewData);     
          }



        $someModel = new  LogSmsSendHelp;
        $someModel->setConnection($this->connOri); 

         if(($search != '' && $tableselect != '') ){
               
         $q =  $someModel->where(function ($query) use ($tablename,$search) { 
                        $query->where( $tablename, 'like', '%'.$search.'%');
                    })->leftJoin('agent_profile', 'agent_profile.phone_number', '=','log_sms_send_helpdesk.number_receiver' )
                ->where('purpose_type','=','SMSOTPHDMA')
                ->selectRaw('log_datetime,number_receiver,sms_message,delivery_status_desc,white_label_admin_user.username,fullname,email ')
                ->orderBy('log_datetime','desc');

        }
        else
        {
             $q =  $someModel->leftJoin('white_label_admin_user', 'white_label_admin_user.phone_number', '=','log_sms_send_helpdesk.number_receiver' )
                ->selectRaw('log_datetime,number_receiver,sms_message,delivery_status_desc,white_label_admin_user.username,fullname,email ')
                ->where('purpose_type','=','SMSOTPHDMA')
                ->orderBy('log_datetime','desc');
        }
       
    
        
        $this->tableSorter->setupPaging($q, $pg);
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
        
        $this->viewData['sorter'] = $this->tableSorter;

        return view('admin.agent.sms_helpdesk', $this->viewData);
    }
    
function getDeviceAgent(Request $request) {
      
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', '');
        $sm = Input::get('sm', '');
        $min = Input::get('min', '');
        $max = Input::get('max', '');
        $tablename="";
        $search = Input::get('search');
        
        $fields = [
            ['log_datetime', 'Log Trx Date Time'],
            ['agent_id', 'Agent ID'],
            ['agent_name', 'Agent Name'],
            ['number_receiver', 'Device id'],
            ['email', 'App Platform'],
            ['sms_message', 'User Agent'],            
            ['delivery_status_desc', 'SMS Status'] 

        ];
          $tableselect = Input::get('tableselect');
         $this->tableSorter->setupSorter(url('/agency/agent_sms_ma.html'), $fields, $searchFor, $sf, $sm,$min,$max, $tableselect, $search);
       
        switch ($tableselect) {
            case 'id':
                $tablename="agent_device.agent_id";
                break;
            case 'name':
                $tablename="fullname";
                break;
            case 'device':
                $tablename="device_id";
                break; 
            default:
                $tablename="";
            break;
        }
      

      // forbidden
          if($this->session->get('group') != 'ADMIN')
          {
            $this->viewData['msg'] = "This User is Forbidden";
            return view('errors.forbidden', $this->viewData);     
          }



        $someModel = new  AgentDevice;
        $someModel->setConnection($this->conn); 

         if(($search != '' && $tableselect != '') ){
               
         $q =  $someModel
                 ->where(function ($query) use ($tablename,$search) { 
                        $query->where( $tablename, 'like', '%'.$search.'%');
                    })
               ->Join('agent_profile', 'agent_profile.agent_id', '=','agent_device.agent_id' )
                ->selectRaw('agent_device.updated_at as log_datetime,agent_device.agent_id,fullname,device_id,device_id_md5,app_platform,app_user_agent,is_allowed')
                 ->orderBy('agent_device.updated_at','desc');

        }
        else
        {
             $q =  $someModel->Join('agent_profile', 'agent_profile.agent_id', '=','agent_device.agent_id' )
                ->selectRaw('agent_device.updated_at as log_datetime,agent_device.agent_id,fullname,device_id,device_id_md5,app_platform,app_user_agent,is_allowed')
                 ->orderBy('agent_device.updated_at','desc');
        }
       
    
        
        $this->tableSorter->setupPaging($q, $pg);
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
        
        $this->viewData['sorter'] = $this->tableSorter;

        return view('admin.agent.sms_device', $this->viewData);
    }
    
 

}
