<?php

namespace App\Http\Controllers\admin\cms;
use Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Controllers\BDGBaseController;
use App\Models\admin\WhiteLabelServiceModel;
use App\Models\admin\ServiceModel;
use App\Models\admin\ServiceProductModel;

class WhiteLabelServiceController extends BDGBaseController
{

    function indexhome() {
        $pg = Input::get('pg', 1);
        $searchFor = Input::get('q');
        $sf = Input::get('sf', 2);
        $sm = Input::get('sm', 0);
        
        $fields = [
            ['service.service_name', 'Service Name'],
            ['white_label_services.service_name_alias', 'Alias'],
            ['white_label_services.show_order', 'Order Number'],
            ['white_label_services.status', 'active']
        ];
        
        $this->tableSorter->setupSorter(url('/wlservice.html'), $fields, $searchFor, $sf, $sm);
        
        if($searchFor != '') {
            $q = ServiceModel::where('white_label_services.white_label_id', '=', $this->session->whiteLabelId())
                    ->leftJoin('white_label_services', 'service.id', '=', 'white_label_services.service_id')
                    ->where(function ($query) use ($searchFor) {
                        $query->where('white_label_services.service_name_alias', 'like', '%'.$searchFor.'%')                              
                              ->orWhere('white_label_services.description', 'like', '%'.$searchFor.'%')
                              ->orWhere('service.service_name', 'like', '%'.$searchFor.'%');
                    })
                    ->selectRaw('service.id as serv_id, service.service_name, service.active as service_active, white_label_services.*');
        } else {
            $q = ServiceModel::where('white_label_services.white_label_id', '=', $this->session->whiteLabelId())
                    ->leftJoin('white_label_services', 'service.id', '=', 'white_label_services.service_id')
                    ->selectRaw('service.id as service_id, service.service_name, service.active as service_active')
                    ->selectRaw('white_label_services.id as wls_id, white_label_services.service_name_alias, white_label_services.show_order, white_label_services.active');
        }
        
        $this->tableSorter->setupPaging($q, $pg);
        
        $this->viewData['sorter'] = $this->tableSorter;

        return view('white_label_service.index', $this->viewData);
    }
    

    function index() {
 
       $qserve = ServiceModel::where('white_label_services.white_label_id', '=', $this->session->whiteLabelId())
                    ->leftJoin('white_label_services', 'service.id', '=', 'white_label_services.service_id')
                    ->selectRaw('service.id as service_id, service.service_name, service.active as service_active')
                    ->selectRaw('white_label_services.id as wls_id, white_label_services.service_name_alias, white_label_services.show_order, white_label_services.active')
                    ->orderBy('service_id','ASC')->get();  
        foreach ($qserve as $srv) { 
            $qtotalProd = ServiceProductModel::where('service_id', '=', $srv->service_id)->count();  
            $services[$srv->service_id]=$qtotalProd;
        }

         $this->viewData['countproduct'] = $services;

        $this->viewData['listService'] = $qserve;         
                 Log::info("WhiteLabelServiceController::index data=".json_encode($this->viewData));  

        return view('white_label_service.indexservice', $this->viewData);

    } 

    function create() {
        $this->viewData['rsserv'] = ServiceModel::where('active', '=', 'Y')
                                        ->whereRaw('id not in (select service_id from white_label_services where white_label_id = '.$this->session->whiteLabelId().')')
                                        ->get();
        
        return view('white_label_service.new', $this->viewData);
    }
    
    function store(Request $request) {
        $service = $request->input('service');
        $alias = $request->input('alias');
        $description = $request->input('description');
        $avatar = $request->input('avatar-file');
        $priority = $request->input('priority');
        $status = $request->input('status', 'N');
        
        if ($service == '') { $this->notif->addMessage('Service name is required'); }
        else {
            $q = WhiteLabelServiceModel::where('white_label_id', '=', $this->session->whiteLabelId())
                    ->where('service_id', '=', $service)
                    ->first();
            if ($q) {
                $this->notif->addMessage('Service is registered. Please choose another.');
            }
        }
        
        if ($priority == '' || !is_numeric($priority) || $priority < 1) { $this->notif->addMessage('Priority must be numeric and greater than zero.'); }
        else {
            $q = WhiteLabelServiceModel::where('white_label_id', '=', $this->session->whiteLabelId())
                    ->where('show_order', '=', $priority)
                    ->first();
            if ($q) {
                $this->notif->addMessage('Show order exists. Please input another.');
            }
        }
        
        if ($this->notif->isOK() == true) {
            $q = new WhiteLabelServiceModel();
            $q->white_label_id = $this->session->whiteLabelId();
            $q->service_id = $service;
            $q->service_name_alias = $alias;
            $q->logo_filename_alias = $avatar;
            $q->description = $description;
            $q->show_order = $priority;
            $q->active = $status;
            $q->created_at = date('Y-m-d H:i:s');
            $q->updated_at = date('Y-m-d H:i:s');
            $q->save();
            
            $this->notif->data = $q->id;
        }
        
        return response()->json($this->notif->build());
    }
    
     function edit($serviceId) {
        $this->viewData['rs'] = ServiceModel::where('white_label_services.white_label_id', '=', $this->session->whiteLabelId())
                                    ->where('service.id', '=', $serviceId)
                                    ->leftJoin('white_label_services', 'service.id', '=', 'white_label_services.service_id')
                                    ->selectRaw('white_label_services.*, service.id as service_id, service.service_name, service.active as service_active')
                                    ->first();
        
        return view('white_label_service.edit', $this->viewData);
    }

    function detailwlservice($serviceId) {
       
          $this->viewData['rs'] = ServiceModel::where('white_label_services.white_label_id', '=', $this->session->whiteLabelId())
                                    ->where('service.id', '=', $serviceId)
                                    ->leftJoin('white_label_services', 'service.id', '=', 'white_label_services.service_id')
                                    ->selectRaw('white_label_services.*, service.id as service_id, service.service_name, service.active as service_active')
                                    ->first();



       // Log::info("WhiteLabelServiceController::detailwlservice data=".json_encode($this->viewData));   

        return view('white_label_service.detailwlservice', $this->viewData);
    }
    
    function update(Request $request) {
        $serviceId = $request->input('service-id');
        $alias = $request->input('alias');
        $description = $request->input('description');
        $avatar = $request->input('avatar-file');
        $priority = $request->input('priority');
        $status = $request->input('status', 'N');
        
        if ($priority == '' || !is_numeric($priority) || $priority < 1) { $this->notif->addMessage('Priority must be numeric and greater than zero.'); }
        else {
            $q = WhiteLabelServiceModel::where('white_label_id', '=', $this->session->whiteLabelId())
                    ->where('show_order', '=', $priority)
                    ->where('service_id', '<>', $serviceId)
                    ->first();
            if ($q) {
                $this->notif->addMessage('Show order exists. Please input another.');
            }
        }        
        
        if ($this->notif->isOK() == true) {
            $q = WhiteLabelServiceModel::where('service_id', '=', $serviceId)->first();
            $q->service_name_alias = $alias;
            $q->logo_filename_alias = $avatar;
            $q->description = $description;
            $q->show_order = $priority;
            $q->updated_at = date('Y-m-d H:i:s');
            $q->save();
        }
        
        return response()->json($this->notif->build());
    }
    
}
