<?php

namespace App\Http\Controllers\admin\cms;

use Log;
use Config;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\BusinessLogic\HelpdeskBL;
use App\Http\Controllers\BDGBaseController;
use App\Models\PartnerDepositAccountModel;
use App\Models\PartnerActivityLogModel;
use App\Models\LogPaymentGatewayModel;
use App\Models\LogUserActivity;
use App\Models\admin\WhiteLabelAdminModel;


class HistoryController extends BDGBaseController
{




     public function __construct() {
        parent::__construct();     
        $this->conn = Config::get ( 'webconf.cms.connectiondata_prod' );
        $this->conn111 = Config::get ( 'webconf.cms.connection111' );
        $this->helpdesk = new HelpdeskBL; 

        $this->LogPaymentGatewayModel = new  LogPaymentGatewayModel;
        $this->LogPaymentGatewayModel->setConnection($this->conn); 


         $this->LogActMa = new  LogUserActivity;
        $this->LogActMa->setConnection($this->conn111); 




    }



    function index() {
        return view('admin.report.index', $this->viewData);
    }

    function history(){

        $deposit = PartnerDepositAccountModel::get();

            $this->viewData['history'] = $deposit;
  
            return view('admin.report.deposit_history', $this->viewData );

    }


    function history_deposit(Request $request){
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

    $min =Input::get('min'); //$request->get('min').' 00:00:00';
    $max = Input::get('max');//$request->get('max').' 23:59:59'; //$request->get('max');
    $tableselect = $request->tableselect;
    $search =$request->input('search');
    $timeMin = '00:00:00';
    $timeMax = '23:59:59';
    $this->listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id')); 
    // log::info("list =".json_encode($this->listAgent));

    if($min != '')
    {
        $result1 = $min. ' ' . $timeMin; 
    } 
    else
    {
      $result1 = '';  
    }

    if($max != '')
    {
        $result2 = $max . ' ' . $timeMax;
    } 
    else
    {
      $result2 = '';  
    }

    $fields = [
            ['log_datetime', 'Log DateTime'],
            ['payment_gateway_provider', 'Payment Gateway'],
            ['bank_id', 'Institution'],
            ['owner_id', 'Agent Id'],
            ['username', 'Username'],
            ['account_name', 'Agent Name'],
            ['account_number', 'Agent VA Number'],
            ['account_type', 'Account Type'],
            ['payment_amount', 'Payment Amount'],
            ['cumulative_payment_amount', 'Comulative Payment'],
            ['datetime_payment', 'DateTime Payment'],
            ['created_at', 'Created At']

        ]; 


        switch ($tableselect) {
            case 'bank':
                $tablename="befintech.bank.bank_name"; 
                break;
            case 'agent':
                $tablename="befintechlog.log_payment_gateway_callback.account_name"; 
                break;   
            case 'pg':
                $tablename="payment_gateway_provider"; 
                break; 
            case 'username':
                $tablename="username"; 
                break;
            case 'va':
                $tablename="befintechlog.log_payment_gateway_callback.account_number"; 
                break; 
            default:
                $tablename=""; 
            break;
        }
 
        $this->tableSortersimple->setupSorter(secure_url('/ma/history_deposit.html'), $fields, $searchFor, $sf, $sm, $min, $max, $tableselect, $search);


        if(($search != '') && ($min != '' && $max != '')) {         
            $q = $this->LogPaymentGatewayModel->whereBetween(
                'datetime_payment', 
                [
                    $result1,
                    $result2
                ]
                )
                ->where(function ($query) use ($tablename,$search) { 
                $query->where( $tablename, 'like', '%'.$search.'%');
                }) 
                ->orderBy('created_at' , 'desc')
                ->join('befintech.bank_accounts', 'befintechlog.log_payment_gateway_callback.account_number', '=', 'befintech.bank_accounts.account_number')    
                ->join('befintech.agent', 'befintech.bank_accounts.owner_id', '=', 'befintech.agent.id')
                ->join('befintech.bank', 'befintech.bank.id', '=', 'befintechlog.log_payment_gateway_callback.bank_id')
                ->whereIn('agent.id',  $this->listAgent)
                ->selectRaw('log_datetime , payment_gateway_provider , befintech.bank.bank_name , befintech.bank_accounts.owner_id ,username,befintechlog.log_payment_gateway_callback.account_name ,befintechlog.log_payment_gateway_callback.account_number, befintechlog.log_payment_gateway_callback.account_type , payment_amount ,cumulative_payment_amount , datetime_payment , befintechlog.log_payment_gateway_callback.created_at');

        }else if($min != '' && $max != ''){
  
            $q = $this->LogPaymentGatewayModel->whereBetween(
                'datetime_payment', 
                [
                    $result1,
                    $result2
                ]
                ) 
              ->orderBy('created_at' , 'desc')
                ->join('befintech.bank_accounts', 'befintechlog.log_payment_gateway_callback.account_number', '=', 'befintech.bank_accounts.account_number')    
                ->join('befintech.agent', 'befintech.bank_accounts.owner_id', '=', 'befintech.agent.id')
                ->join('befintech.bank', 'befintech.bank.id', '=', 'befintechlog.log_payment_gateway_callback.bank_id')
                ->whereIn('agent.id',  $this->listAgent)
                ->selectRaw('log_datetime , payment_gateway_provider , befintech.bank.bank_name , befintech.bank_accounts.owner_id ,username,befintechlog.log_payment_gateway_callback.account_name ,befintechlog.log_payment_gateway_callback.account_number, befintechlog.log_payment_gateway_callback.account_type , payment_amount ,cumulative_payment_amount , datetime_payment , befintechlog.log_payment_gateway_callback.created_at');
        }else if(($search != '' && $tableselect != '') && ($min == '' && $max == '')){
            $q = $this->LogPaymentGatewayModel
                 ->where(function ($query) use ($tablename,$search) { 
                $query->where( $tablename, 'like', '%'.$search.'%');
                }) 
               ->orderBy('created_at' , 'desc')
                ->join('befintech.bank_accounts', 'befintechlog.log_payment_gateway_callback.account_number', '=', 'befintech.bank_accounts.account_number')    
                ->join('befintech.agent', 'befintech.bank_accounts.owner_id', '=', 'befintech.agent.id')
                ->join('befintech.bank', 'befintech.bank.id', '=', 'befintechlog.log_payment_gateway_callback.bank_id')
                ->whereIn('agent.id',  $this->listAgent)
                ->selectRaw('log_datetime , payment_gateway_provider , befintech.bank.bank_name , befintech.bank_accounts.owner_id ,username,befintechlog.log_payment_gateway_callback.account_name ,befintechlog.log_payment_gateway_callback.account_number, befintechlog.log_payment_gateway_callback.account_type , payment_amount ,cumulative_payment_amount , datetime_payment , befintechlog.log_payment_gateway_callback.created_at');  
        }else {
            // log::info("datalist=".json_encode($this->listAgent));
           $q = $this->LogPaymentGatewayModel
                ->orderBy('created_at' , 'desc')
                ->join('befintech.bank_accounts', 'befintechlog.log_payment_gateway_callback.account_number', '=', 'befintech.bank_accounts.account_number')    
                ->join('befintech.agent', 'befintech.bank_accounts.owner_id', '=', 'befintech.agent.id')
                ->join('befintech.bank', 'befintech.bank.id', '=', 'befintechlog.log_payment_gateway_callback.bank_id')
                ->whereIn('agent.id',  $this->listAgent)
                ->selectRaw('log_datetime , payment_gateway_provider , befintech.bank.bank_name , befintech.bank_accounts.owner_id ,username,befintechlog.log_payment_gateway_callback.account_name ,befintechlog.log_payment_gateway_callback.account_number, befintechlog.log_payment_gateway_callback.account_type , payment_amount ,cumulative_payment_amount , datetime_payment , befintechlog.log_payment_gateway_callback.created_at');
        }
  
        $this->tableSortersimple->setupPaging($q, $pg);

        $this->viewData['sorter'] = $this->tableSortersimple;
        $this->viewData['pg'] = $pg;
        $this->viewData['searchFor'] = $searchFor;
        $this->viewData['min'] = $result1;
        $this->viewData['max'] = $result2;
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
        
        return view('admin.report.activity', $this->viewData );
 } 

 function history_logma(Request $request){
    $pg = Input::get('pg', 1);
    $searchFor = Input::get('q');
    $sf = Input::get('sf', 0);
    $sm = Input::get('sm', 0);

    $min =Input::get('min'); //$request->get('min').' 00:00:00';
    $max = Input::get('max');//$request->get('max').' 23:59:59'; //$request->get('max');
    $tableselect = $request->tableselect;
    $search =$request->input('search');
    $timeMin = '00:00:00';
    $timeMax = '23:59:59';
    // $this->listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id')); 
    // log::info("list =".json_encode($this->listAgent));

    if($min != '')
    {
        $result1 = $min. ' ' . $timeMin; 
    } 
    else
    {
      $result1 = '';  
    }

    if($max != '')
    {
        $result2 = $max . ' ' . $timeMax;
    } 
    else
    {
      $result2 = '';  
    }

    $fields = [
            ['log_datetime', 'Log DateTime'],
            ['payment_gateway_provider', 'Username'],
            ['bank_id', 'Fullname'],
            ['owner_id', 'Activity Id'],
            ['username', 'Detail Act'] 

        ]; 


        switch ($tableselect) {
            case 'username':
                $tablename="befintech.white_label_admin_user.username"; 
                break;
            case 'detail':
                $tablename="activity_detail"; 
                break; 
            default:
                $tablename=""; 
            break;
        }
 
        $this->tableSorter->setupSorter(secure_url('/ma/history_log.html'), $fields, $searchFor, $sf, $sm, $min, $max, $tableselect, $search);


        if(($search != '') && ($min != '' && $max != '')) {         
            $q = $this->LogActMa->whereBetween(
                'log_datetime', 
                [
                    $result1,
                    $result2
                ]
                )
                ->where(function ($query) use ($tablename,$search) { 
                $query->where( $tablename, 'like', '%'.$search.'%');
                }) 
                ->orderBy('log_datetime' , 'desc')                
                ->join('white_label_admin_user','befintechlog.log_cms_activity.user_id','=','befintech.white_label_admin_user.username')
                ->where('befintech.white_label_admin_user.master_agent_id', '=', $this->session->get('master_agent_id'))
                ->where('befintech.white_label_admin_user.web_app', '=', "MA")
                ->where('befintechlog.log_cms_activity.cms_type', '=', "HELPDESK-MA")
                ->where('befintech.white_label_admin_user.master_agent_id', '=', $this->session->get('master_agent_id'))
                ->selectRaw('username,fullname,activity_name,activity_detail,log_cms_activity.created_at,log_datetime');

        }else if($min != '' && $max != ''){
  
            $q = $this->LogActMa->whereBetween(
                'log_datetime', 
                [
                    $result1,
                    $result2
                ]
                ) 
              ->orderBy('log_datetime' , 'desc')                
                ->join('white_label_admin_user','befintechlog.log_cms_activity.user_id','=','befintech.white_label_admin_user.username')
                ->where('befintech.white_label_admin_user.master_agent_id', '=', $this->session->get('master_agent_id'))
                ->where('befintech.white_label_admin_user.web_app', '=', "MA")
                ->where('befintechlog.log_cms_activity.cms_type', '=', "HELPDESK-MA")
                ->where('befintech.white_label_admin_user.master_agent_id', '=', $this->session->get('master_agent_id'))
                ->selectRaw('username,fullname,activity_name,activity_detail,log_cms_activity.created_at,log_datetime');
        }else if(($search != '' && $tableselect != '') && ($min == '' && $max == '')){
            $q = $this->LogActMa
                 ->where(function ($query) use ($tablename,$search) { 
                $query->where( $tablename, 'like', '%'.$search.'%');
                }) 
               ->orderBy('log_cms_activity.created_at' , 'desc')                
                ->join('white_label_admin_user','befintechlog.log_cms_activity.user_id','=','befintech.white_label_admin_user.username')
                ->where('befintech.white_label_admin_user.master_agent_id', '=', $this->session->get('master_agent_id'))
                ->where('befintech.white_label_admin_user.web_app', '=', "MA")
                ->where('befintechlog.log_cms_activity.cms_type', '=', "HELPDESK-MA")
                ->where('befintech.white_label_admin_user.master_agent_id', '=', $this->session->get('master_agent_id'))
                ->selectRaw('username,fullname,activity_name,activity_detail,log_cms_activity.created_at,log_datetime');  
        }else {
            //
           $q = $this->LogActMa
                ->orderBy('log_cms_activity.created_at' , 'desc')                
                ->join('white_label_admin_user','befintechlog.log_cms_activity.user_id','=','befintech.white_label_admin_user.username')
                ->where('befintech.white_label_admin_user.master_agent_id', '=', $this->session->get('master_agent_id'))
                ->where('befintech.white_label_admin_user.web_app', '=', "MA")
                ->where('befintechlog.log_cms_activity.cms_type', '=', "HELPDESK-MA")
                ->where('befintech.white_label_admin_user.master_agent_id', '=', $this->session->get('master_agent_id'))
                ->selectRaw('username,fullname,activity_name,activity_detail,log_cms_activity.created_at,log_datetime'); 
         }
   
        $this->tableSorter->setupPaging($q, $pg);

        $this->viewData['sorter'] = $this->tableSorter;
        $this->viewData['pg'] = $pg;
        $this->viewData['searchFor'] = $searchFor;
        $this->viewData['min'] = $result1;
        $this->viewData['max'] = $result2;
        $this->viewData['tableselect'] = $tableselect;
        $this->viewData['search'] = $search;
        
        return view('admin.report.history_logma', $this->viewData );
 }
    
    function exportToCSVdeposit(Request $request){
            // $min =Input::get('min' ); //$request->get('min').' 00:00:00';
            // $max = Input::get('max' );//$request->get('max').' 23:59:59'; //$request->get('max');
            // $tableselect = $request->tableselect;
            // $search =$request->input('search');

            // $timeMin = '00:00:00';
            // $timeMax = '23:59:59';
 
            $pg = Input::get('pg', 1);
            $searchFor = Input::get('q');
            $sf = Input::get('sf', 0);
            $sm = Input::get('sm', 0);

            $min =Input::get('min'); //$request->get('min').' 00:00:00';
            $max = Input::get('max');//$request->get('max').' 23:59:59'; //$request->get('max');
            $tableselect = $request->tableselect;
            $search =$request->input('search');
            $timeMin = '00:00:00';
            $timeMax = '23:59:59';
            $this->listAgent = $this->helpdesk->listAgentbyUpline($this->session->get('master_agent_id')); 


            $result1 = $min . ' ' . $timeMin;
            $result2 = $max . ' ' . $timeMax;


        //     if(($search != '') && ($min != '' && $max != '')) {         
        //     $table = LogPaymentGatewayModel::whereBetween(
        //         'created_at', 
        //         [
        //             $result1,
        //             $result2
        //         ]
        //         )
        //         ->where($tableselect , $search)
        //         ->orderBy('created_at' , 'desc')
        //         ->leftJoin('befintech.bank_accounts', 'befintechlog.log_payment_gateway_callback.account_number', '=', 'befintech.bank_accounts.account_number')
        //         ->where('befintech.bank_accounts.owner_id', '=', $this->session->get('master_agent_id'))
        //         ->get();

        // }else if($min != '' && $max != ''){
        //     $table = LogPaymentGatewayModel::whereBetween(
        //         'created_at', 
        //         [
        //             $result1,
        //             $result2
        //         ]
        //         )
        //         ->orderBy('created_at' , 'desc')
        //         ->leftJoin('befintech.bank_accounts', 'befintechlog.log_payment_gateway_callback.account_number', '=', 'befintech.bank_accounts.account_number')
        //         ->where('befintech.bank_accounts.owner_id', '=', $this->session->get('master_agent_id'))
        //         ->get();
        // }else if(($search != '') && ($min == '' && $max == '')){
        //     $table = LogPaymentGatewayModel::where($tableselect , $search)
        //         ->orderBy('created_at' , 'desc')
        //         ->leftJoin('befintech.bank_accounts', 'befintechlog.log_payment_gateway_callback.account_number', '=', 'befintech.bank_accounts.account_number')
        //         ->where('befintech.bank_accounts.owner_id', '=', $this->session->get('master_agent_id'))
        //         ->get();      
        // }else {
        //     $table = LogPaymentGatewayModel::orderBy('created_at' , 'desc')
        //     ->leftJoin('befintech.bank_accounts', 'befintechlog.log_payment_gateway_callback.account_number', '=', 'befintech.bank_accounts.account_number')
        //     ->where('befintech.bank_accounts.owner_id', '=', $this->session->get('master_agent_id'))
        //     ->get();
        // }
            



            if(($search != '') && ($min != '' && $max != '')) {         
            $q = $this->LogPaymentGatewayModel->whereBetween(
                'datetime_payment', 
                [
                    $result1,
                    $result2
                ]
                )
                ->where($tableselect ,'like', $search.'%')
                ->orderBy('created_at' , 'desc')
                ->join('befintech.bank_accounts', 'befintechlog.log_payment_gateway_callback.account_number', '=', 'befintech.bank_accounts.account_number')    
                ->join('befintech.agent', 'befintech.bank_accounts.owner_id', '=', 'befintech.agent.id')
                ->whereIn('agent.id',  $this->listAgent)
                ->selectRaw('log_datetime , payment_gateway_provider , befintechlog.log_payment_gateway_callback.bank_id , befintech.bank_accounts.owner_id ,befintechlog.log_payment_gateway_callback.account_name ,befintechlog.log_payment_gateway_callback.account_number, befintechlog.log_payment_gateway_callback.account_type , payment_amount ,cumulative_payment_amount , datetime_payment , befintechlog.log_payment_gateway_callback.created_at')
                 ->get();

        }else if($min != '' && $max != ''){
                    // log::info("result1=". $result1." result2=". $result2);
 
            $q = $this->LogPaymentGatewayModel->whereBetween(
                'datetime_payment', 
                [
                    $result1,
                    $result2
                ]
                ) 
              ->orderBy('created_at' , 'desc')
                ->join('befintech.bank_accounts', 'befintechlog.log_payment_gateway_callback.account_number', '=', 'befintech.bank_accounts.account_number')    
                ->join('befintech.agent', 'befintech.bank_accounts.owner_id', '=', 'befintech.agent.id')
                ->whereIn('agent.id',  $this->listAgent)
                ->selectRaw('log_datetime , payment_gateway_provider , befintechlog.log_payment_gateway_callback.bank_id , befintech.bank_accounts.owner_id ,befintechlog.log_payment_gateway_callback.account_name ,befintechlog.log_payment_gateway_callback.account_number, befintechlog.log_payment_gateway_callback.account_type , payment_amount ,cumulative_payment_amount , datetime_payment , befintechlog.log_payment_gateway_callback.created_at')
                 ->get();
        }else if(($search != '' && $tableselect != '') && ($min == '' && $max == '')){
            $q = $this->LogPaymentGatewayModel->where($tableselect ,'like', $search.'%')
               ->orderBy('created_at' , 'desc')
                ->join('befintech.bank_accounts', 'befintechlog.log_payment_gateway_callback.account_number', '=', 'befintech.bank_accounts.account_number')    
                ->join('befintech.agent', 'befintech.bank_accounts.owner_id', '=', 'befintech.agent.id')
                ->whereIn('agent.id',  $this->listAgent)
                ->selectRaw('log_datetime , payment_gateway_provider , befintechlog.log_payment_gateway_callback.bank_id , befintech.bank_accounts.owner_id ,befintechlog.log_payment_gateway_callback.account_name ,befintechlog.log_payment_gateway_callback.account_number, befintechlog.log_payment_gateway_callback.account_type , payment_amount ,cumulative_payment_amount , datetime_payment , befintechlog.log_payment_gateway_callback.created_at')
                 ->get();     
        }else {
            // log::info("datalist=".json_encode($this->listAgent));
           $q = $this->LogPaymentGatewayModel->orderBy('created_at' , 'desc')
                ->join('befintech.bank_accounts', 'befintechlog.log_payment_gateway_callback.account_number', '=', 'befintech.bank_accounts.account_number')    
                ->join('befintech.agent', 'befintech.bank_accounts.owner_id', '=', 'befintech.agent.id')
                ->whereIn('agent.id',  $this->listAgent)
                ->selectRaw('log_datetime , payment_gateway_provider , befintechlog.log_payment_gateway_callback.bank_id , befintech.bank_accounts.owner_id ,befintechlog.log_payment_gateway_callback.account_name ,befintechlog.log_payment_gateway_callback.account_number, befintechlog.log_payment_gateway_callback.account_type , payment_amount ,cumulative_payment_amount , datetime_payment , befintechlog.log_payment_gateway_callback.created_at')
                 ->get();
        }



            $filename = "report_history_deposit.csv";
            $handle = fopen($filename, 'w+');
            fputcsv($handle, array('log_datetime' , 'payment_gateway_provider' , 'bank_id' ,'owner_id','account_name' ,'account_number', 'account_type' , 'payment_amount' ,'cumulative_payment_amount' , 'datetime_payment' , 'created_at'));

            foreach($q as $row) {
                fputcsv($handle, array($row['log_datetime'], $row['payment_gateway_provider'], $row['bank_id'], $row['owner_id'], $row['account_name'], $row['account_number'], $row['account_type'], $row['payment_amount'],$row['cumulative_payment_amount'],$row['datetime_payment'], $row['created_at']));
            }

            fclose($handle);

            $headers = array(
                'Content-Type' => 'text/csv',
            );

            return response()->download($filename, 'report.csv', $headers);
    }
}
