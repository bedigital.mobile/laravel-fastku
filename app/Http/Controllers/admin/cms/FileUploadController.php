<?php

namespace App\Http\Controllers\admin\cms;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\BDGBaseController;

class FileUploadController extends BDGBaseController
{
    var $mimeType = [
		'docs' => ['application/pdf', 'application/msword'],
		'images' => ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'],
		'avatars' => ['image/jpeg', 'image/pjpeg', 'image/png']
	];
    
    public function uploadDoc() {
		$title = Input::get('title');
        return view('admin.utils.file_upload', ['folder' => FileUploadController::FOLDER_DOC, 'title' => $title]);
	}

	public function uploadImage() {
		$title = Input::get('title');
		$prop = Input::get('prop');
		$dimension = Input::get('dim');
		
		$data = ['folder' => FileUploadController::FOLDER_IMAGE, 'title' => $title];
		if ($prop == 1) {
			$data['prop'] = true;
		}
		
		if ($dimension != '') {
			$dimensions = explode('_', $dimension);
			if (count($dimensions) == 2) {
				$data['dimension'] = $dimensions[0].'px X '.$dimensions[1].'px';
			}
		}
		
        return view('admin.utils.image_upload', $data);
	}

	public function uploadProfile() {
		$title = Input::get('title');
        return view('admin.utils.profile_upload', ['folder' => FileUploadController::FOLDER_PROFILE, 'title' => $title]);
	}
    
    public function store(Request $request)
    {
        $folder = $request->input('folder');
		$mimeType = $this->mimeType[$folder];
		
		$file = $request->file('file-name');
		$ext = $file->getClientOriginalExtension();
		$mime = $file->getMimeType();
		$fileSize = $file->getSize();
		if ($fileSize > UploadedFile::getMaxFilesize() && $file->isValid()) {
			$this->notif->addMessage('File size exceeds max. file size');
		} else if (!in_array($mime, $mimeType)) {
			$this->notif->addMessage('Invalid file type');
		} else {
            $fileName = rand(100, 999).'-'.date('YmdHis').'-'.rand(1000, 9999).'.'.$ext;
            $file->storeAs($folder, $fileName);
            $this->notif->data = $fileName;
		}
		
		return response()->json($this->notif->build());
    }
    
    public function fileAvatar($fileName) {
        $path = 'avatars/'.$fileName;
		if (Storage::exists($path)) {
    		$content = Storage::get($path);
            $url = Storage::path($path);
			return response($content, 200)
				->header('Content-Type', mime_content_type($url).'; charset=utf-8')
				->header('Content-Length', filesize($url));
		} else {
			return response('', 404);
		}
	}
    
    static function deleteAvatar($fileName) {
        $path = 'avatars/'.$fileName;
		if (Storage::exists($path)) {
            Storage::delete($path);
        }
    }

}
