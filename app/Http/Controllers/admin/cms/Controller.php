<?php

namespace App\Http\Controllers\admin\cms;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Exceptions\Handler;
use Illuminate\Http\Request;
use App\Definition;
use App\Core\Util\UtilNetwork;
use Log;
use Config;
use Exception;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $debug;
    protected $ipClient;
    protected $userAgent;
    protected $httpReferer;
    protected $port;
    
    public function __construct() {
    	$this->init();
    }
    
    protected function init() {
    	try {
    		$this->debug = false;
    		$this->ipClient = "";
    		$this->userAgent = "";
    		$this->debug = boolval( Config::get( 'app.debug' ) );
    		$this->ipClient = UtilNetwork::getClientIP();
    		$this->userAgent = (array_key_exists('HTTP_USER_AGENT', $_SERVER) ? $_SERVER ['HTTP_USER_AGENT'] : "");
    		$this->httpReferer = (array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER ['HTTP_REFERER'] : "");
    		$this->port = (array_key_exists('SERVER_PORT', $_SERVER) ? $_SERVER ['SERVER_PORT'] : 0);
    		if (empty($this->httpReferer)) {
    			$pathinfo = ( array_key_exists('PATH_INFO', $_SERVER) ? $_SERVER['PATH_INFO'] : "");
    			if (empty($pathinfo)) {
    				$pathinfo = ( array_key_exists('REQUEST_URI', $_SERVER) ? $_SERVER['REQUEST_URI'] : "");
    			}
    			$this->httpReferer = "https://" . $_SERVER ['HTTP_HOST']. $pathinfo;
    		} else {
    			$pathinfo = ( array_key_exists('PATH_INFO', $_SERVER) ? $_SERVER['PATH_INFO'] : "");
    			if (! empty($pathinfo) && substr($pathinfo, 0, 5) == '/admin/') {
    				$this->port = 80;
    			}
    		}
    		if (! empty($this->httpReferer)) {
    			$urlparts = parse_url($this->httpReferer);           
                Log::info("Controller::init urlpart=".json_encode($urlparts));

    			if ($this->debug) Log::info("Controller::init url_part=".json_encode($urlparts));
    			if (isset($urlparts['path'])) {
    				$pathinfo = $urlparts['path'];
    				if (! empty($pathinfo) && substr($pathinfo, 0, 5) == '/admin/') {
    					$this->port = 80;
    				}
    				if ($this->port == 80 && ! empty($pathinfo) && substr($pathinfo, 0, 5) != '/admin/') {
    					$urlparts['path'] = '/admin' . $pathinfo;
    				}
    			}
    			$this->httpReferer = 'https://' . $urlparts['host'] .'/admin'. (isset($urlparts['path']) ? $urlparts['path'] : "");
    		}
    		if ($this->debug) Log::info("Controller::init port=".$this->port." httpReferer=".$this->httpReferer);
    	} catch ( Exception $e ) {
    		Log::info("Controller::init exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
    		if (empty($this->ipClient)) {
    			$this->ipClient = "127.0.0.1";
    		}
    	}
    }
    
}
