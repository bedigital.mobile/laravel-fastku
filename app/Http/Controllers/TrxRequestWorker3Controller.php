<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\BDGBaseController;
use App\Http\Helpers\TrxRequestHelper;
use App\Http\Helpers\HttpClientHelper;

use App\Jobs\TrxWorkerReportJob;

class TrxRequestWorker3Controller extends BDGBaseController
{
    function store(Request $request, $accessKey) {
        $workerNumber = 3;
        
        $response = (new TrxRequestHelper($request, $workerNumber, $accessKey))->run();
        
        TrxWorkerReportJob::dispatch($workerNumber);
        
        return response()->json($response);
    }
}
