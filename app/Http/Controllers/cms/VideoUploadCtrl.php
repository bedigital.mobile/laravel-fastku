<?php
namespace App\Http\Controllers\cms;

use Log;
use Config;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use App\Exceptions\Handler;
use App\Http\Controllers\Controller;
use App\Core\Util\UtilCommon;


class VideoUploadCtrl extends Controller {
	
	const HEADER_API_JSON = array(
		'Content-Type' => 'application/json',
		'Cache-Control' => 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
		'Pragma' => 'no-cache',
		'Access-Control-Allow-Origin' => '*',
		'Access-Control-Allow-Headers' => 'Origin, Content-Type, Authorization, X-Auth-Token',
		'Strict-Transport-Security' => 'max-age=31536000; includeSubDomains',
		'X-Frame-Options' => 'sameorigin',
		'X-Content-Type-Options' => 'nosniff',
		'X-XSS-Protection' => '1; mode=block'
	);
	
	private $response = null;

	public function __construct( ) {
		//...
	}

	public function showVideoUpload( Request $request ) {
		$data = null;
		$content = "";
		try {
			if (strtoupper($request->method()) == 'OPTIONS') {
			return response()->make( "200 OK" )->withHeaders( self::HEADER_API_JSON );
			}
			Log::info("VideoUploadCtrl->showVideoUpload : started");
		    $content = View::make('cms.tvonenews-video-upload', compact('data'));
		} catch ( Exception $e ) {
			Log::info("VideoUploadCtrl->showVideoUpload exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			$content = "Something went wrong : internal service error";
		}
		return response ()->make ( $content )->header ( 'Content-Type', 'text/html' );
	}

} //End of class