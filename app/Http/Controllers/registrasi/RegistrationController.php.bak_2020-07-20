<?php
namespace App\Http\Controllers\registrasi;

use Log;
use Config;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use App\Exceptions\Handler;
use App\Http\Controllers\Controller;
use App\Core\Util\UtilCommon;
use App\Models\Database\Agent;
use App\Models\Database\Gerai;


class RegistrationController extends Controller {
	
	const HEADER_API_JSON = array(
		'Content-Type' => 'application/json',
		'Cache-Control' => 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
		'Pragma' => 'no-cache',
		'Access-Control-Allow-Origin' => '*',
		'Access-Control-Allow-Headers' => 'Origin, Content-Type, Authorization, X-Auth-Token',
		'Strict-Transport-Security' => 'max-age=31536000; includeSubDomains',
		'X-Frame-Options' => 'sameorigin',
		'X-Content-Type-Options' => 'nosniff',
		'X-XSS-Protection' => '1; mode=block'
	);
	
	private $response = null;

	public function __construct( ) {
		//...
	}

	public function showFormRegistrasi( Request $request ) {
		$data = null;
		$content = "";
		try {
			if (strtoupper($request->method()) == 'OPTIONS') {
			return response()->make( "200 OK" )->withHeaders( self::HEADER_API_JSON );
			}
			Log::info("RegistrationController->showFormRegistrasi : started");
		    $content = View::make('registrasi.registrasi_paketkupos', compact('data'));
		} catch ( Exception $e ) {
			Log::info("RegistrationController->showFormRegistrasi exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			$content = "Something went wrong : internal service error";
		}
		return response ()->make ( $content )->header ( 'Content-Type', 'text/html' );
	}

	public function showKeuntunganAgent( Request $request ) {
		$data = null;
		$content = "";
		try {
			if (strtoupper($request->method()) == 'OPTIONS') {
			return response()->make( "200 OK" )->withHeaders( self::HEADER_API_JSON );
			}
			Log::info("RegistrationController->showKeuntunganAgent : started");
		    $content = View::make('registrasi.keuntungan_agent_paketkupos', compact('data'));
		} catch ( Exception $e ) {
			Log::info("RegistrationController->showKeuntunganAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			$content = "Something went wrong : internal service error";
		}
		return response ()->make ( $content )->header ( 'Content-Type', 'text/html' );
	}


	public function submitFormRegistrasi( Request $request ) {
		Log::info("RegistrationController->submitFormRegistrasi: started");
		/*Get all inputs in multipart*/
		$nama = $request->nama;
		$ektp = $request->ektp;
		$npwp = $request->npwp;
		$telp = $request->telp;
		$email = $request->email;
		$namaGerai = $request->namagerai;
		$alamatGerai = $request->alamatgerai;
		$ukuranGerai = $request->ukurangerai;
		$jarakSesamaAgentPOS = $request->jaraksesamaagenpos;
		$dataPesaing = $request->datapesaing;
		$filenamePhotoBelakangGerai = $request->filenamephotobelakanggerai;
		$filenamePhotoDepanGerai = $request->filenamephotodepangerai;
		$filenamePhotoDenahGerai = $request->filenamephotodenahgerai;

		/*Save agent*/
		//check agent duplication based on ektp
		Log::info("RegistrationController->submitFormRegistrasi: ektp=".$ektp);
		$isAgentExist = true;
		$agentID = 0;
		$msg = "Agent sudah pernah registrasi";
		$agent = new Agent();
		$rsAgent = $agent->where('ektp',trim($ektp))->first();
		if (is_null($rsAgent)) {
			$isAgentExist = false;
			//Insert agent
			$newAgent = new Agent();
			$newAgent->ektp = $ektp;
			$newAgent->nama = $nama;
			$newAgent->npwp = $npwp;
			$newAgent->email = $email;
			$newAgent->save();
			$agentID = $newAgent->id;
			Log::info("RegistrationController->submitFormRegistrasi: Agent berhasil disimpan");
		}

		/*Save gerai*/
		//Currently system support only for single gerai
		if (!$isAgentExist) {
			$fileNameDepanGerai = $request->filenamephotodepangerai.".jpg";
			$fileNameBelakangGerai = $request->filenamephotobelakanggerai."jpg";
			$fileNameDenahGerai = $request->filenamephotodenahgerai.".jpg";
			
			/*
			//Save file depan gerai
			if ($request->hasFile('photodepangerai')) {
	            $file = $request->file('photodepangerai');
	            $fileNameDepanGerai = $filenamePhotoDepanGerai . '.' . $file->getClientOriginalExtension();
				$file = $file->move('/var/www/laravelposku/public/img/gerai', $fileNameDepanGerai);
				Log::info("RegistrationController->submitFormRegistrasi: photo depan gerai is saved");            
	      	}
	      	//Save file belakang gerai
			if ($request->hasFile('photobelakanggerai')) {
	            $file = $request->file('photobelakanggerai');
	            $fileNameBelakangGerai = $filenamePhotoBelakangGerai . '.' . $file->getClientOriginalExtension();
				$file = $file->move('/var/www/laravelposku/public/img/gerai', $fileNameBelakangGerai);            
				Log::info("RegistrationController->submitFormRegistrasi: photo belakang gerai is saved");            
			}
	      	//Save file denah gerai
			if ($request->hasFile('photodenahgerai')) {
	            $file = $request->file('photodenahgerai');
	            $fileNameDenahGerai = $filenamePhotoDenahGerai . '.' . $file->getClientOriginalExtension();
				$file = $file->move('/var/www/laravelposku/public/img/gerai', $fileNameDenahGerai);            
				Log::info("RegistrationController->submitFormRegistrasi: photo denah gerai is saved");            
			}
			*/

			$newGerai = new Gerai();
			$newGerai->id_agent = $agentID;
			$newGerai->nama = $namaGerai;
			$newGerai->alamat = $alamatGerai;
			$newGerai->ukuran = $ukuranGerai;
			$newGerai->jarak_sesama_agen = $jarakSesamaAgentPOS;
			$newGerai->data_pesaing = $dataPesaing;
			$newGerai->photo_gerai_depan = $fileNameDepanGerai;
			$newGerai->photo_gerai_belakang = $fileNameBelakangGerai;
			$newGerai->photo_gerai_denah = $fileNameDenahGerai;
			$newGerai->status = 'REGISTERING';
			$newGerai->save();
			$msg = "OK";

			Log::info("RegistrationController->submitFormRegistrasi: Gerai berhasil disimpan");
		}  else {
			Log::info("RegistrationController->submitFormRegistrasi: Gerai tidak disimpan karena sebelumnya pernah registrasi");
		}

		Log::info("RegistrationController->submitFormRegistrasi: done");
		return response()->make($msg)->withHeaders( self::HEADER_API_JSON ); 
	}

	public function submitImage( Request $request ) {
		//Save base64 image
		Log::info("RegistrationController->submitImageRegistrasi: nama_photo=".$request->filename);
		Log::info("RegistrationController->submitImageRegistrasi: data_photo=".$request->photo);
		$this->base64ToJpeg($request->photo,'/var/www/laravelposku/public/img/gerai/'.$request->filename.".jpg" );
	}
		
	private function base64ToJpeg($base64Data, $outputFile) {
	    // open the output file for writing
	    $ifp = fopen( $outputFile, 'wb' ); 

	    // split the string on commas
	    // $data[ 0 ] == "data:image/png;base64"
	    // $data[ 1 ] == <actual base64 string>
	    $data = explode( ',', $base64Data );

	    // we could add validation here with ensuring count( $data ) > 1
	    if (count($data) > 1) {
		    fwrite( $ifp, base64_decode( $data[ 1 ] ) );
	    }

	    // clean up the file resource
	    fclose( $ifp ); 
	}

} //End of class