<?php
namespace App\Http\Controllers\registrasi;

use Log;
use Config;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use App\Exceptions\Handler;
use App\Http\Controllers\Controller;
use App\Core\Util\UtilCommon;
use App\Models\Database\Agent;
use App\Models\Database\Gerai;
use App\Models\Database\Province;
use App\Models\Database\City;
use App\Models\Database\District;
use App\Models\Database\Village;


class RegistrationController extends Controller {
	
	const HEADER_API_JSON = array(
		'Content-Type' => 'application/json',
		'Cache-Control' => 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
		'Pragma' => 'no-cache',
		'Access-Control-Allow-Origin' => '*',
		'Access-Control-Allow-Headers' => 'Origin, Content-Type, Authorization, X-Auth-Token',
		'Strict-Transport-Security' => 'max-age=31536000; includeSubDomains',
		'X-Frame-Options' => 'sameorigin',
		'X-Content-Type-Options' => 'nosniff',
		'X-XSS-Protection' => '1; mode=block'
	);
	
	private $response = null;

	public function __construct( ) {
		//...
	}

	public function showFormRegistrasi( Request $request ) {
		$data = null;
		$content = "";
		try {
			if (strtoupper($request->method()) == 'OPTIONS') {
			return response()->make( "200 OK" )->withHeaders( self::HEADER_API_JSON );
			}
			Log::info("RegistrationController->showFormRegistrasi : started");
		    $content = View::make('registrasi.registrasi', compact('data'));
		} catch ( Exception $e ) {
			Log::info("RegistrationController->showFormRegistrasi exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			$content = "Something went wrong : internal service error";
		}
		return response ()->make ( $content )->header ( 'Content-Type', 'text/html' );
	}

	public function showKeuntunganAgent( Request $request ) {
		$data = null;
		$content = "";
		try {
			if (strtoupper($request->method()) == 'OPTIONS') {
			return response()->make( "200 OK" )->withHeaders( self::HEADER_API_JSON );
			}
			Log::info("RegistrationController->showKeuntunganAgent : started");
		    $content = View::make('registrasi.keuntungan_agent_paketkupos', compact('data'));
		} catch ( Exception $e ) {
			Log::info("RegistrationController->showKeuntunganAgent exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			$content = "Something went wrong : internal service error";
		}
		return response ()->make ( $content )->header ( 'Content-Type', 'text/html' );
	}


	public function submitFormRegistrasi( Request $request ) {
		Log::info("RegistrationController->submitFormRegistrasi: started");
		/*Get all inputs in multipart*/
		$nama = $request->nama;
		$ektp = $request->ektp;
		$npwp = $request->npwp;
		$telp = $request->telp;
		$email = $request->email;
		$namaGerai = $request->namagerai;
		$alamatGerai = $request->alamatgerai;
		$ukuranGerai = $request->ukurangerai;
		$jarakSesamaAgentPOS = $request->jaraksesamaagenpos;
		$dataPesaing = $request->datapesaing;
		$filenamePhotoDalamGerai = $request->filenamephotodalamgerai;
		$filenamePhotoDepanGerai = $request->filenamephotodepangerai;
		$filenamePhotoTandaTangan = $request->filenamephototandatangan;
		$idProvinsi = $request->idprovinsi;
		$namaProvinsi = $request->namaprovinsi;
		$idKota = $request->idkota;
		$namaKota = $request->namakota;
		$idKecamatan = $request->idkecamatan;
		$namaKecamatan = $request->namakecamatan;
		$idKelurahan = $request->idkelurahan;
		$namaKelurahan = $request->namakelurahan;
		$namaKantorPosKabupatenKota = $request->namakantorposkabupatenkota;
		$jarakKantorPosKabupatenKota = $request->jarakkantorposkabupatenkota; 
		$namaKantorPosKecamatanKelurahan = $request->namakantorposkecamatankelurahan;
		$jarakKantorPosKecamatanKelurahan = $request->jarakkantorposkecamatankelurahan;
		$koordinatLokasi = $request->koordinatlokasi; 

		/*Save agent*/
		//check agent duplication based on ektp
		Log::info("RegistrationController->submitFormRegistrasi: nama=".$nama);
		Log::info("RegistrationController->submitFormRegistrasi: ektp=".$ektp);
		Log::info("RegistrationController->submitFormRegistrasi: npwp=".$npwp);
		Log::info("RegistrationController->submitFormRegistrasi: telp=".$telp);
		Log::info("RegistrationController->submitFormRegistrasi: email=".$email);

		Log::info("RegistrationController->submitFormRegistrasi: nama_gerai=".$namaGerai);
		Log::info("RegistrationController->submitFormRegistrasi: alamat_gerai=".$alamatGerai);
		Log::info("RegistrationController->submitFormRegistrasi: ukuran_gerai=".$ukuranGerai);
		Log::info("RegistrationController->submitFormRegistrasi: jarak_sesama_agen=".$jarakSesamaAgentPOS);
		Log::info("RegistrationController->submitFormRegistrasi: data_pesaing=".$dataPesaing);
		Log::info("RegistrationController->submitFormRegistrasi: nama_file_photo_depan_gerai=".$filenamePhotoDepanGerai);
		Log::info("RegistrationController->submitFormRegistrasi: nama_file_photo_dalam_gerai=".$filenamePhotoDalamGerai);
		Log::info("RegistrationController->submitFormRegistrasi: nama_file_photo_tanda_tangan=".$filenamePhotoTandaTangan);
		Log::info("RegistrationController->submitFormRegistrasi: id_provinsi=".$idProvinsi);
		Log::info("RegistrationController->submitFormRegistrasi: nama_provinsi=".$namaProvinsi);
		Log::info("RegistrationController->submitFormRegistrasi: id_kota=".$idKota);
		Log::info("RegistrationController->submitFormRegistrasi: nama_kota=".$namaKota);
		Log::info("RegistrationController->submitFormRegistrasi: id_kecamatan=".$idKecamatan);
		Log::info("RegistrationController->submitFormRegistrasi: nama_kecamatan=".$namaKecamatan);
		Log::info("RegistrationController->submitFormRegistrasi: id_kelurahan=".$idKelurahan);
		Log::info("RegistrationController->submitFormRegistrasi: nama_kelurahan=".$namaKelurahan);
		Log::info("RegistrationController->submitFormRegistrasi: nama_kantor_pos_kabupaten_kota=".$namaKantorPosKabupatenKota);
		Log::info("RegistrationController->submitFormRegistrasi: jarak_kantor_pos_kabupaten_kota=".$jarakKantorPosKabupatenKota);
		Log::info("RegistrationController->submitFormRegistrasi: nama_kantor_pos_kecamatan_kelurahan=".$namaKantorPosKecamatanKelurahan);
		Log::info("RegistrationController->submitFormRegistrasi: jarak_kantor_pos_kecamatan_kelurahan=".$jarakKantorPosKecamatanKelurahan);
		Log::info("RegistrationController->submitFormRegistrasi: koordinat_lokasi=".$koordinatLokasi);

		$isAgentExist = true;
		$agentID = 0;
		$msg = "Agent sudah pernah registrasi";
		$agent = new Agent();
		$rsAgent = $agent->where('ektp',trim($ektp))->first();
		if (is_null($rsAgent)) {
			$isAgentExist = false;
			//Insert agent
			$newAgent = new Agent();
			$newAgent->ektp = $ektp;
			$newAgent->nama = $nama;
			$newAgent->npwp = $npwp;
			$newAgent->email = $email;
			$newAgent->telp = $telp;
			$newAgent->save();
			$agentID = $newAgent->id;
			Log::info("RegistrationController->submitFormRegistrasi: Agent berhasil disimpan");
		}

		/*Save gerai*/
		//Currently system support only for single gerai
		if (!$isAgentExist) {
			$fileNameDepanGerai = $request->filenamephotodepangerai.".jpg";
			$fileNameDalamGerai = $request->filenamephotodalamgerai."jpg";
			$filenameTandaTangan = $request->filenamephototandatangan.".jpg";
			
			$newGerai = new Gerai();
			$newGerai->id_agent = $agentID;
			$newGerai->nama = $namaGerai;
			$newGerai->alamat = $alamatGerai;
			$newGerai->ukuran = $ukuranGerai;
			$newGerai->jarak_sesama_agen = $jarakSesamaAgentPOS;
			$newGerai->data_pesaing = $dataPesaing;
			$newGerai->photo_gerai_depan = $fileNameDepanGerai;
			$newGerai->photo_gerai_dalam = $fileNameDalamGerai;
			$newGerai->photo_tanda_tangan = $filenameTandaTangan;
			$newGerai->photo_gerai_belakang = "not-available"; //Deprecated field
			$newGerai->photo_gerai_denah = "not-available"; //Deprecated field
			$newGerai->province_id = $idProvinsi;
			$newGerai->province_name = $namaProvinsi;
			$newGerai->city_id = $idKota;
			$newGerai->city_name = $namaKota;
			$newGerai->district_id = $idKecamatan;
			$newGerai->district_name = $namaKecamatan;
			$newGerai->village_id = $idKelurahan;
			$newGerai->village_name = $namaKelurahan;
			$newGerai->nama_kantor_pos_kota = $namaKantorPosKabupatenKota;
			$newGerai->jarak_kantor_pos_kota = $jarakKantorPosKabupatenKota;
			$newGerai->nama_kantor_pos_kecamatan_kelurahan = $namaKantorPosKecamatanKelurahan;
			$newGerai->jarak_kantor_pos_kecamatan_kelurahan = $jarakKantorPosKecamatanKelurahan;
			$newGerai->koordinat_lokasi = $koordinatLokasi;

			$newGerai->status = 'REGISTERING';
			$newGerai->save();
			$msg = "OK";

			Log::info("RegistrationController->submitFormRegistrasi: Gerai berhasil disimpan");
		}  else {
			Log::info("RegistrationController->submitFormRegistrasi: Gerai tidak disimpan karena sebelumnya pernah registrasi");
		}

		Log::info("RegistrationController->submitFormRegistrasi: done");
		return response()->make($msg)->withHeaders( self::HEADER_API_JSON ); 
	}

	public function submitImage( Request $request ) {
		//Save base64 image
		Log::info("RegistrationController->submitImage: nama_photo=".$request->filename);
		Log::info("RegistrationController->submitImage: data_photo=".$request->photo);
		$this->base64ToJpeg($request->photo,'/var/www/laravelposku/public/img/gerai/'.$request->filename.".jpg" );
		Log::info("RegistrationController->submitImage: done");
		$msg = "OK";
		return response()->make($msg)->withHeaders( self::HEADER_API_JSON ); 		
	}


	public function getProvinceCityList(Request $request) {
		$status = '';
		$cityList = null;
		try {
			$provinceID = $request->provinceid;
			$cityList = $this->_getProvinceCityList($provinceID);
			$status = 'OK';
		} catch (Exception $e) {
			$status = 'ERROR';
		}
		$result = array(
			"status" => $status,
			"cities" => $cityList
		);
		return response()->make($result)->withHeaders( self::HEADER_API_JSON );
	}

	public function getProvinceList(Request $request) {
		$status = '';
		$provinceList = null;
		try {
			$provinceList = $this->_getProvinceList();
			$status = 'OK';
		} catch (Exception $e) {
			$status = 'ERROR';
		}
		$result = array(
			"status" => $status,
			"provinces" => $provinceList
		);
		return response()->make($result)->withHeaders( self::HEADER_API_JSON );
	}
		

	public function getDistrictList(Request $request) {
		$status = '';
		$districtList = null;
		try {
			$cityID = $request->cityid;
			$districtList = $this->_getDistrictList($cityID);
			$status = 'OK';
		} catch (Exception $e) {
			Log::info("RegistrationController->getDistrictList exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			$status = 'ERROR';
		}
		$result = array(
			"status" => $status,
			"districts" => $districtList
		);
		return response()->make($result)->withHeaders( self::HEADER_API_JSON );
	}

	public function getDistrictVillageList(Request $request) {
		$status = '';
		$districtVillageList = null;
		try {
			$districtID = $request->districtid;
			$districtVillageList = $this->_getDistrictVillageList($districtID);
			$status = 'OK';
		} catch (Exception $e) {
			Log::info("RegistrationController->getDistrictVillageList exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			$status = 'ERROR';
		}
		$result = array(
			"status" => $status,
			"villages" => $districtVillageList
		);
		return response()->make($result)->withHeaders( self::HEADER_API_JSON );
	}


	private function base64ToJpeg($base64Data, $outputFile) {
	    // open the output file for writing
	    $ifp = fopen( $outputFile, 'wb' ); 

	    // split the string on commas
	    // $data[ 0 ] == "data:image/png;base64"
	    // $data[ 1 ] == <actual base64 string>
	    $data = explode( ',', $base64Data );

	    // we could add validation here with ensuring count( $data ) > 1
	    if (count($data) > 1) {
		    fwrite( $ifp, base64_decode( $data[ 1 ] ) );
	    }

	    // clean up the file resource
	    fclose( $ifp ); 
	}

	private function _getProvinceList(){
		return Province::where('active','Y')->orderBy('province_name','ASC')->get();		
	}

	private function _getProvinceCityList($provinceID) {
		return City::where('province_id',$provinceID)->where('active','Y')->orderBy('city_name','ASC')->get();
	}

	private function _getDistrictList($cityID){
		return District::where('city_id',$cityID)->where('active','Y')->orderBy('district_name','ASC')->get();		
	}

	private function _getDistrictVillageList($districtID){
		return Village::where('district_id',$districtID)->where('active','Y')->orderBy('village_name','ASC')->get();		
	}



} //End of class