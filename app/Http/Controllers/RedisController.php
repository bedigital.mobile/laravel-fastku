<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

use App\Http\Controllers\BDGBaseController;
use App\Models\PartnerModel;
use App\Models\WhiteLabelProductModel;
use App\Models\WhiteLabelProductBreakupModel;

class RedisController extends BDGBaseController
{
    function redisCommand($command) {
        if (strtolower($command) == 'del') {
            $partners = PartnerModel::all();            
            foreach ($partners as $part) {
                Redis::del($part->white_label_id.':aggregator');
            }
            
            $fees = WhiteLabelProductModel::all();
            foreach ($fees as $fee) {
                Redis::del($fee->white_label_id.':'.$fee->product_id.':fee');
            }
            
            $fees = WhiteLabelProductBreakupModel::all();
            foreach ($fees as $fee) {
                Redis::del($fee->white_label_id.':'.$fee->product_code.':fee');
            }
        }
    }
}
