<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Helpers\SessionHelper;

class AdminAuth
{
	var $session;
	
	function __construct() {
        $this->session = new SessionHelper();
	}
	
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($this->session->exist() == true) {
            return $next($request);
        }
        
        $this->session->remove();

        return redirect('/fastku/login');
    }
}
