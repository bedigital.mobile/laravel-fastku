<?php
namespace App\Core\CacheData;

use Log;
use Config;
use Exception;
use App\Core\CacheData\CacheData;

class CacheAgentDevice extends CacheData
{
	const KEY_PREFIX = "AGENTDEVICE";
	const EXPIRY_MINUTES = 0;

	public function __construct( $expiryMinutes  = -1) {
		if ($expiryMinutes < 0) {
			$this->expiryMinutes = self::EXPIRY_MINUTES;
		} else {
			$this->expiryMinutes = $expiryMinutes;
		}
		parent::__construct( self::KEY_PREFIX, $this->expiryMinutes );
	}

	public function getID( $agentID, $deviceMD5, $platform, $default = 0 ) {
		$key = $this->getKey( $agentID, (strlen($deviceMD5) == 32 ? strtolower($deviceMD5) : strtolower(md5($deviceMD5))), strtolower($platform) );
		return parent::getData($key, $default);
	}

	public function setID( $agentID, $deviceMD5, $platform, $id ) {
		$key = $this->getKey( $agentID, (strlen($deviceMD5) == 32 ? strtolower($deviceMD5) : strtolower(md5($deviceMD5))), strtolower($platform) );
		return parent::setData( $key, $id );
	}

	public function deleteID( $agentID, $deviceMD5, $platform, $force = true ) {
		$key = $this->getKey( $agentID, (strlen($deviceMD5) == 32 ? strtolower($deviceMD5) : strtolower(md5($deviceMD5))), strtolower($platform) );
		return parent::deleteData( $key, $force );
	}

}
