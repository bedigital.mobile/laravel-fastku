<?php
namespace App\Core\CacheData;

use Log;
use Config;
use Exception;
use Illuminate\Support\Facades\Cache;

class CacheUsername
{
	const KEY_PREFIX = "USER_";
	const EXPIRY_MINUTES = 0;
	
	private $expiryMinutes;
	
	public function __construct( $expiryMinutes  = -1) {
		if ($expiryMinutes < 0) {
			$this->expiryMinutes = self::EXPIRY_MINUTES;
		} else {
			$this->expiryMinutes = $expiryMinutes;
		}
	}
	
	private function getKey( $username ) {
		$username = strtolower( trim($username) );
		$key = self::KEY_PREFIX . $username;
		return $key;
	}
	
	public function getUserID( $username ) {
		$id = 0;
		try {
			$key = $this->getKey($username);
			if (Cache::has($key)) {
				$id = Cache::get($key, 0);
			}
		} catch ( Exception $e ) {
			Log::info("CacheUsername::getUserID username=".$username." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $id;
	}
	
	public function setUserID( $username, $id ) {
		$b = false;
		try {
			$key = $this->getKey($username);
			if (empty($this->expiryMinutes)) {
				Cache::forever($key, $id);
				$b = true;
			} else {
				Cache::put($key, $id, $this->expiryMinutes);
				$b = true;
			}
			
		} catch ( Exception $e ) {
			Log::info("CacheUsername::setUserID username=".$username." id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
	public function delUserID( $username ) {
		$b = false;
		try {
			$key = $this->getKey($username);
			Cache::forget($key);
			$b = true;
		} catch ( Exception $e ) {
			Log::info("CacheUsername::delUserID username=".$username." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
}
