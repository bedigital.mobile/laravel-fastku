<?php
namespace App\Core\CacheData\Simple;

use Log;
use Config;
use Exception;
use App\Core\CacheData\Simple\CacheSimple;
use App\Core\Util\UtilCommon;

class CacheDataWhiteLabelBasic extends CacheSimple
{
	const KEY_PREFIX = "WL";
	const EXPIRY_MINUTES = 15;
	
	protected $whitelabelID = 0;
	protected $group = "";
	
	public function __construct( $whitelabelID, $expiryMinutes  = -1, $group = "", $keyPrefix = "" ) {
		$this->whitelabelID = (int)$whitelabelID;
		$this->group = UtilCommon::cleanSpecialChar(strtoupper(trim($group)), false);
		$keyPrefix = UtilCommon::cleanSpecialChar(strtoupper(trim($keyPrefix)), false);
		if ($expiryMinutes < 0) {
			$this->expiryMinutes = self::EXPIRY_MINUTES;
		} else {
			$this->expiryMinutes = $expiryMinutes;
		}
		parent::__construct( (empty($keyPrefix) ? self::KEY_PREFIX : $keyPrefix ) . (!empty($this->group) ? "_" : "") . $this->group . "_" . $this->whitelabelID, $this->expiryMinutes );
	}

	protected function getData( $param, $default = "" ) {
		return parent::getData( UtilCommon::cleanSpecialChar(trim($param)), $default );
	}

	protected function setData( $param, $v ) {
		return parent::setData( UtilCommon::cleanSpecialChar(trim($param)), $v );
	}

	protected function deleteData( $parame, $force = true ) {
		return parent::deleteData( UtilCommon::cleanSpecialChar(trim($param)), $force );
	}

}
