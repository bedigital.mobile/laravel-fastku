<?php
namespace App\Core\CacheData\Simple;

use Log;
use Config;
use Exception;
use App\Core\CacheData\Simple\CacheSimple;

class CacheSignToken extends CacheSimple
{
	const KEY_PREFIX = "SIGNTKN";
	const EXPIRY_MINUTES = 60;

	public function __construct( $expiryMinutes  = -1) {
		if ($expiryMinutes < 0) {
			$this->expiryMinutes = self::EXPIRY_MINUTES;
		} else {
			$this->expiryMinutes = $expiryMinutes;
		}
		parent::__construct( self::KEY_PREFIX, $this->expiryMinutes );
	}
	
	public function isUsed( $sessionID, $token ) {
		$b = false;
		try {
			$flag = (int)parent::getData( md5($sessionID . "|" . $token), 0 );
			$b = boolval($flag);
		} catch ( Exception $e ) {
			Log::info("CacheSignToken::isUsed session_id=".$sessionID." token=".$token." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
	public function setUsed( $sessionID, $token ) {
		return parent::setData( md5($sessionID . "|" . $token), 1 );
	}
	
	public function deleteStatus(  $sessionID, $token, $force = true ) {
		return parent::deleteData( md5($sessionID . "|" . $token), $force );
	}
	
}