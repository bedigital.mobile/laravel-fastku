<?php
namespace App\Core\CacheData\Simple;

use Log;
use Config;
use Exception;
use App\Core\CacheData\Simple\CacheSimple;

class CacheAgentSendSMSDeviceToken extends CacheSimple
{
	const KEY_PREFIX = "AGTSMSTKNDVC";
	const EXPIRY_MINUTES = 3;

	public function __construct( $expiryMinutes  = -1) {
		if ($expiryMinutes < 0) {
			$this->expiryMinutes = self::EXPIRY_MINUTES;
		} else {
			$this->expiryMinutes = $expiryMinutes;
		}
		parent::__construct( self::KEY_PREFIX, $this->expiryMinutes );
	}

	public function isRecentlySMSSent( $agentID ) {
		$b = false;
		try {
			$flag = (int)parent::getData( $agentID, 0 );
			$b = boolval($flag);
		} catch ( Exception $e ) {
			Log::info("CacheAgentSendSMSDeviceToken::isRecentlyEmailSent agent_id=".$agentID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}

	public function setFlagRecentlySMSSent( $agentID ) {
		return parent::setData( $agentID, 1 );
	}

	public function deleteStatus(  $agentID, $force = true ) {
		return parent::deleteData( $agentID, $force );
	}

}
