<?php
namespace App\Core\CacheData\Simple;

use Log;
use Config;
use Exception;
use App\Core\CacheData\Simple\CacheSimple;

class CacheSMSGatewayBalance extends CacheSimple
{
	const KEY_PREFIX = "SMSGTYBAL";
	const EXPIRY_MINUTES = 15;

	public function __construct( $expiryMinutes  = -1) {
		if ($expiryMinutes < 0) {
			$this->expiryMinutes = self::EXPIRY_MINUTES;
		} else {
			$this->expiryMinutes = $expiryMinutes;
		}
		parent::__construct( self::KEY_PREFIX, $this->expiryMinutes );
	}

	public function hasGetBalance( $gatewayID ) {
		$b = false;
		try {
			$flag = (int)parent::getData( $gatewayID, 0 );
			$b = boolval($flag);
		} catch ( Exception $e ) {
			Log::info("CacheSMSGatewayBalance ::hasGetBalance gateway_id=".$gatewayID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
	public function setFlagHasGetBalance( $gatewayID ) {
		return parent::setData( $gatewayID, 1 );
	}
	
	public function deleteStatus(  $gatewayID, $force = true ) {
		return parent::deleteData( $gatewayID, $force );
	}
	
}

