<?php
namespace App\Core\CacheData\Simple;

use Log;
use Config;
use Exception;
use App\Core\CacheData\Simple\CacheSimple;

class CacheAgentEmailForgotPassword extends CacheSimple
{
	const KEY_PREFIX = "MAILFORGOTPWD";
	const EXPIRY_MINUTES = 5;

	public function __construct( $expiryMinutes  = -1) {
		if ($expiryMinutes < 0) {
			$this->expiryMinutes = self::EXPIRY_MINUTES;
		} else {
			$this->expiryMinutes = $expiryMinutes;
		}
		parent::__construct( self::KEY_PREFIX, $this->expiryMinutes );
	}

	public function isRecentlyEmailSent( $agentID ) {
		$b = false;
		try {
			$flag = (int)parent::getData( $agentID, 0 );
			$b = boolval($flag);
		} catch ( Exception $e ) {
			Log::info("CacheAgentEmailForgotPassword::isRecentlyEmailSent agent_id=".$agentID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
	public function setFlagRecentlyEmailSent( $agentID ) {
		return parent::setData( $agentID, 1 );
	}
	
	public function deleteStatus(  $agentID, $force = true ) {
		return parent::deleteData( $agentID, $force );
	}
	
}

