<?php
namespace App\Core\CacheData\Simple\Username;

use Log;
use Config;
use Exception;
use App\Core\CacheData\Simple\Username\CacheUsername;

class CacheUsernameMasterAgent extends CacheUsername
{
	const KEY_MASTER_AGENT = "MASTERAGENT";

	public function __construct( $whitelabelID, $expiryMinutes  = -1) {
		parent::__construct( $whitelabelID, $expiryMinutes, self::KEY_MASTER_AGENT );
	}

}