<?php
namespace App\Core\CacheData\Simple\Username;

use Log;
use Config;
use Exception;
use App\Core\CacheData\Simple\Username\CacheUsername;

class CacheUsernameAgent extends CacheUsername
{
	const KEY_AGENT = "AGENT";
	
	public function __construct( $whitelabelID, $expiryMinutes  = -1) {
		parent::__construct( $whitelabelID, $expiryMinutes, self::KEY_AGENT );
	}
	
}
