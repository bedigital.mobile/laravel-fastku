<?php
namespace App\Core\CacheData\Simple\Username;

use Log;
use Config;
use Exception;
use App\Core\CacheData\Simple\CacheDataWhiteLabelBasic;
use App\Core\Util\UtilCommon;

class CacheUsername extends CacheDataWhiteLabelBasic
{
	const KEY_PREFIX = "USERNAME";
	const EXPIRY_MINUTES = 0;
	
	public function __construct( $whitelabelID, $expiryMinutes  = -1, $group = "") {
		parent::__construct( $whitelabelID, $expiryMinutes, $group, self::KEY_PREFIX );
	}
	
	public function getID( $usename, $default = 0 ) {
		$res = parent::getData( $usename, (empty($default) ? 0 : (UtilCommon::isDigit($default) ? (int)$default : 0) ) );
		if (empty($res) || ! UtilCommon::isDigit($res)) {
			$res = 0;
		}
		return $res;
	}

	public function setID( $username, $v ) {
		return parent::setData( $username, (int)$v );
	}

	public function deleteID( $username, $force = true ) {
		return parent::deleteData( $username, $force );
	}

}
