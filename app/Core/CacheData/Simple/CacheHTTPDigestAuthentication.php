<?php
namespace App\Core\CacheData\Simple;

use Log;
use Config;
use Exception;
use App\Core\CacheData\Simple\CacheSimple;

class CacheHTTPDigestAuthentication extends CacheSimple
{
	const KEY_PREFIX = "HTTPNONCE";
	const EXPIRY_MINUTES = 10;
	
	public function __construct( $expiryMinutes  = -1) {
		if ($expiryMinutes < 0) {
			$this->expiryMinutes = self::EXPIRY_MINUTES;
		} else {
			$this->expiryMinutes = $expiryMinutes;
		}
		parent::__construct( self::KEY_PREFIX, $this->expiryMinutes );
	}
	
	public function getData( $nonce, $default = array() ) {
		$res = parent::getData( md5($nonce), (is_array($default) ? $default : array()) );
		if (is_null($res) || ! is_array($res)) {
			$res = array();
		}
		return $res;
	}
	
	public function setData( $nonce, $v ) {
		return parent::setData( md5($nonce), $v );
	}
	
	public function deleteData( $nonce, $force = true ) {
		return parent::deleteData( md5($nonce), $force );
	}
	
}

