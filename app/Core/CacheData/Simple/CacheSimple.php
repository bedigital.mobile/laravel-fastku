<?php
namespace App\Core\CacheData\Simple;

use Log;
use Config;
use Exception;
use App\Core\CacheData\CacheData;

class CacheSimple extends CacheData
{
	protected $expiryMinutes = 0;
	
	public function __construct( $keyPrefix, $expiryMinutes  = -1) {
		if ($expiryMinutes < 0) {
			$this->expiryMinutes = 0;
		} else {
			$this->expiryMinutes = $expiryMinutes;
		}
		parent::__construct( $keyPrefix, $this->expiryMinutes );
	}
	
	protected function getData( $param, $default = null ) {
		$v = null;
		try {
			if (! empty($param)) {
				if ($this->debug) Log::info("CacheSimple::getData param=".$param);
				$key = $this->getKey( $param );
				$v = parent::getData($key, $default);
			}
		} catch ( Exception $e ) {
			Log::info("CacheSimple::getData param=".$param." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $v;
	}
	
	protected function setData( $param, $v ) {
		$b = false;
		try {
			if (! empty($param)) {
				if ($this->debug) Log::info("CacheSimple::setData param=".$param);
				$key = $this->getKey( $param );
				$b = parent::setData($key, $v);
			}
		} catch ( Exception $e ) {
			Log::info("CacheSimple::setData param=".$param." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
	protected function deleteData( $param, $force = false ) {
		$b = false;
		try {
			if (! empty($param)) {
				if ($this->debug) Log::info("CacheSimple::deleteData param=".$param." force=".($force?"true":"false"));
				$key = $this->getKey( $param );
				$b = parent::deleteData($key);
			}
		} catch ( Exception $e ) {
			Log::info("CacheSimple::deleteData param=".$param." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
}