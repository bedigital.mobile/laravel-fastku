<?php
namespace App\Core\CacheData\Simple;

use Log;
use Config;
use Exception;
use App\Core\CacheData\Simple\CacheSimple;

class CacheAgentSendSMSDeviceTokenCounter extends CacheSimple
{
	const KEY_PREFIX = "AGTSMSTKNDVCCOUNTER";
	const EXPIRY_MINUTES = 1440;

	public function __construct( $expiryMinutes  = -1) {
		if ($expiryMinutes < 0) {
			$this->expiryMinutes = self::EXPIRY_MINUTES;
		} else {
			$this->expiryMinutes = $expiryMinutes;
		}
		parent::__construct( self::KEY_PREFIX . date('Ymd'), $this->expiryMinutes );
	}

	public function getCounter( $agentID ) {
		$num = 0;
		try {
			$num = (int)parent::getData( $agentID, 0 );
		} catch ( Exception $e ) {
			Log::info("CacheAgentSendSMSDeviceTokenCounter::getCounter agent_id=".$agentID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $num;
	}

	public function increment( $agentID ) {
		$num = $this->getCounter( $agentID );
		$num++;
		return parent::setData( $agentID, $num );
	}

	public function deleteStatus(  $agentID, $force = true ) {
		return parent::deleteData( $agentID, $force );
	}

}