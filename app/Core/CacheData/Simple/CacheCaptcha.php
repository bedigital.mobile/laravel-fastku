<?php
namespace App\Core\CacheData\Simple;

use Log;
use Config;
use Exception;
use App\Core\CacheData\Simple\CacheSimple;

class CacheCaptcha extends CacheSimple
{
	const KEY_PREFIX = "CAPTCHA";
	const EXPIRY_MINUTES = 30;
	
	public function __construct( $expiryMinutes  = -1) {
		if ($expiryMinutes < 0) {
			$this->expiryMinutes = self::EXPIRY_MINUTES;
		} else {
			$this->expiryMinutes = $expiryMinutes;
		}
		parent::__construct( self::KEY_PREFIX, $this->expiryMinutes );
	}
	
	public function getData( $keyIdentity, $default = null ) {
		return parent::getData( $keyIdentity, $default );
	}
	
	public function setData( $keyIdentity, $code ) {
		return parent::setData( $keyIdentity, $code );
	}
	
	public function deleteData( $keyIdentity, $force = true ) {
		return parent::deleteData( $keyIdentity, $force );
	}
	
}
