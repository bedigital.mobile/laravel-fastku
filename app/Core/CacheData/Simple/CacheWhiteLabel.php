<?php
namespace App\Core\CacheData\Simple;

use Log;
use Config;
use Exception;
use App\Core\CacheData\Simple\CacheSimple;
use App\Models\Database\WhiteLabel;

class CacheWhiteLabel extends CacheSimple
{
	const KEY_PREFIX = "WL";
	const EXPIRY_MINUTES = 0;

	public function __construct( $expiryMinutes  = -1) {
		if ($expiryMinutes < 0) {
			$this->expiryMinutes = self::EXPIRY_MINUTES;
		} else {
			$this->expiryMinutes = $expiryMinutes;
		}
		parent::__construct( self::KEY_PREFIX, $this->expiryMinutes );
	}
	
	public function getData( $id, $default = null ) {
		$mWL = null;
		try {
			$id = (int)$id;
			if (! empty($id)) {
				$res = parent::getData( $id, $default );
				if (! empty($res)) {
					$mWL = new WhiteLabel();
					$arrItem = array();
					if (is_string($res)) {
						$arrItem = json_decode($res, true, 512, JSON_BIGINT_AS_STRING);
					} else {
						$arrItem = json_decode(json_encode($res), true, 512, JSON_BIGINT_AS_STRING);
					}
					foreach ($arrItem as $key => $value) {
						$mWL->$key = $value;
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("CacheWhiteLabel::getData id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $mWL;
	}
	
	public function setData( $id, WhiteLabel $mWL ) {
		try {
			$id = (int)$id;
			if (! empty($id)) {
				return parent::setData( $id, $mWL->toArray() );
			}
		} catch ( Exception $e ) {
			Log::info("CacheWhiteLabel::setData id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public function deleteData( $id, $force = true ) {
		$id = (int)$id;
		if (! empty($id)) {
			return parent::deleteData( $id, $force );
		}
		return false;
	}
	
}