<?php
namespace App\Core\CacheData;

use Log;
use Config;
use Exception;
use App\Exceptions\Handler;
use App\Core\CacheData\MultipleCache;
use Illuminate\Support\Facades\Cache;


class CacheJson extends MultipleCache {

	private $isUsed;
	private $storeName;
	private $expiryMinutes;

	const KEY_PREFIX = "LIST_";
	const EXPIRY_MINUTES = 3600;

	public function __construct() {
		parent::__construct();
		$this->isUsed = false;
		$this->storeName = "";
		try {
			$this->isUsed = boolval ( Config::get ( 'webconf.cache_data.listcommission.used' ) );
			if ($this->isUsed) {
				$this->storeName = Config::get ( 'webconf.cache_data.listcommission.store_name' );
				$this->expiryMinutes = (int)Config::get ( 'webconf.cache_data.listcommission.expiry_minutes' );
				if ( empty($this->expiryMinutes) || $this->expiryMinutes <= 0 ) {
					$this->expiryMinutes = self::EXPIRY_MINUTES;
				}
			}
		} catch ( Exception $e ) {
			Log::info("CacheJson::__construct exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}

	private function getKey( $whitelabelID,$type ) {
		$key = self::KEY_PREFIX . $whitelabelID.'_'.$type;
		return $key;
	}
	 
	public function getJson( $whitelabelID,$type ) {
			$arrProfile = array();
		try {
			if ($this->isUsed && ! empty($whitelabelID)) {
				$key = $this->getKey($whitelabelID,$type);
				if (empty($this->storeName)) {
					if (Cache::has($key)) {
						$arrProfile = Cache::get($key, "");
					}
				} else {
					if (Cache::store($this->storeName)->has($key)) {
						$arrProfile = Cache::store($this->storeName)->get($key, "");
					}
				}
				if (! is_null($arrProfile) && is_string($arrProfile) && ! empty($arrProfile)) {
					$arrProfile = json_decode($arrProfile, true, 512, JSON_BIGINT_AS_STRING);
				}
			}
		} catch ( Exception $e ) {
			Log::info("CacheJson::getJson user_id=".$userID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		if (is_null($arrProfile) && ! is_array($arrProfile)) {
			$arrProfile = array();
		}
		return $arrProfile;
	}

	public function setJson( $whitelabelID,$type, $json, $curExpiryMinutes ) {
		$b = false;
		try {
			if ($this->isUsed && ! empty($whitelabelID)) {
				$key = $this->getKey($whitelabelID,$type);
				/*if (empty($curExpiryMinutes) || $curExpiryMinutes <= 0) {					
					$this->delJson( $whitelabelID );
				} else {
					if ($curExpiryMinutes > $this->expiryMinutes) {
						$curExpiryMinutes = $this->expiryMinutes;
					}*/
					Log::info("CacheJson::setJson START INPUT CACHE  key=".$key);						
					if (empty($this->storeName)) {
						Cache::put($key, $json, $curExpiryMinutes);
						$b = true;
						$this->putQueue( self::ACTION_PUT, $key, $json, $curExpiryMinutes );
						$this->setStoreOther( $key, $json, $curExpiryMinutes );
					} else {
						Cache::store($this->storeName)->put($key, $json, $curExpiryMinutes);
						$b = true;
					}
					Log::info("CacheJson::setJson END INPUT CACHE ");
						
				//}
			}
		} catch ( Exception $e ) {
			Log::info("CacheJson::setJson white_label_id=".$whitelabelID." key=".$key." expiry_minutes=".$curExpiryMinutes." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}

	public function delJson( $whitelabelID,$type ) {
		$b = false;
		try {
			if ($this->isUsed && ! empty($whitelabelID)) {
				$key = $this->getKey($whitelabelID,$type);
				if (empty($this->storeName)) {
					Cache::forget($key);
					$b = true;
					$this->putQueue( self::ACTION_DELETE, $key );
					$this->delStoreOther($key);
					// Log::info("CacheJson::delMenuJSON white_label_id=".$whitelabelID." success");
				} else {
					Cache::store($this->storeName)->forget($key);
					$b = true;
					// Log::info("CacheJson::delMenuJSON white_label_id=".$whitelabelID." success");
				}
			}
		} catch ( Exception $e ) {
			Log::info("CacheJson::delJson white_label_id=".$whitelabelID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}

}