<?php
namespace App\Core\CacheData;

use Log;
use Config;
use Exception;
use App\Exceptions\Handler;
use App\Jobs\Cache\ActionCacheJob;
use Illuminate\Support\Facades\Cache;


class MultipleCache {

	protected $defaultStore;
	protected $maxStore;
	protected $isMemcached;
	protected $isRedis;
	protected $defaultDriver;
	protected $debug;
	private $defaultQueueConnection;
	private $maxQueueConnection;
	private $idxQueue;
	private $maxQueueHighRoundRobin;
	
	const MAX_APIVERSION = 20;
	const MAX_PAGE = 50;
	const MAX_SERVER = 10;
	const MAX_LB_HIGH = 5;
	const KEY_LB_HIGH = "MULHIGHLB";
	
	const ACTION_PUT = 1;
	const ACTION_DELETE = 2;
	
	public function __construct( $idx = 0 ) {
		$this->debug = false;
		$this->isMemcached = false;
		$this->isRedis = false;
		$this->defaultDriver = "";
		$this->defaultQueueConnection = "";
		$this->maxQueueConnection = 0;
		$this->maxStore = 0;
		$this->maxQueueHighRoundRobin = 0;
		$this->idxQueue = $idx;
		$this->debug = boolval ( Config::get ( 'app.debug' ) );
		$this->checkDefaultDriver();
		$this->setMaxQueueConnection();
		$this->setMaxCacheStore();
	}
	
	private function checkDefaultDriver() {
		try {
			$this->defaultDriver = Cache::getDefaultDriver();
			if ($this->defaultDriver == 'memcached') {
				$this->isMemcached = true;
			} else if ($this->defaultDriver == 'redis' || $this->defaultDriver == 'redis-sentinel') {
				$this->isRedis = true;
			}
		} catch ( Exception $e ) {
			Log::info("MultipleCache::checkDefaultDriver exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
	private function setMaxCacheStore() {
		$n = 1;
		$defaultDriver = "";
		try {
			$defaultDriver = Cache::getDefaultDriver();
			if (! empty($defaultDriver)) {
				$locfig = "cache.stores.".$defaultDriver;
				$loop = true;
				$n = 1;
				while ($loop) {
					$strstore = $locfig . ($n + 1);
					$arrconf = Config::get ( $strstore );
					if (! is_null($arrconf) && is_array($arrconf)) {
						$n++;
						if (($n + 1) > self::MAX_SERVER) {
							$loop = false;
						}
					} else {
						$loop = false;
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("MultipleCache::setMaxCacheStore exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		$this->maxStore = $n;
	}
	
	protected function setMaxQueueConnection() {
		$n = 1;
		$defaultQueueConnection = "";
		try {
			$defaultQueueConnection = Config::get ( 'queue.default' );
			if (! empty($defaultQueueConnection)) {
				$locfig = "queue.connections.".$defaultQueueConnection;
				$loop = true;
				$n = 1;
				while ($loop) {
					$strconf = $locfig . ($n + 1);
					$arrconf = Config::get ( $strconf );
					if (! is_null($arrconf) && is_array($arrconf)) {
						$n++;
						if (($n + 1) > self::MAX_SERVER) {
							$loop = false;
						}
					} else {
						$loop = false;
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("MultipleCache::setMaxQueueConnection exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		$this->defaultQueueConnection = $defaultQueueConnection;
		$this->maxQueueConnection = $n;
	}
	
	protected function getQueueName() {
		$queuename = "";
		try {
			$queuename = Config::get ( 'queue.queue_high');
			if (! empty($queuename)) {
				$this->maxQueueHighRoundRobin = (int)Config::get ( 'queue.queue_high_'.$this->idxQueue.'_round_robin_max');
				if ($this->idxQueue >= 10 && $this->maxQueueHighRoundRobin > 1) {
					$queuename = $this->getRoundRobinMultipleQueueName( $queuename );
				} else {
					$queuename .= ($this->idxQueue >= 1 ? (string)$this->idxQueue : "");
				}
			}
		} catch ( Exception $e ) {
			Log::info("MultipleCache::getQueueName exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $queuename;
	}
	
	private function getRoundRobinMultipleQueueName( $name ) {
		try {
			$queuename = $name . $this->idxQueue;
			$num = 0;
			$key = $this->getKeyNameMultiple();
			if (Cache::has($key)) {
				$num = (int)Cache::get($key, 0);
				$num++;
				$num = $num % ($this->maxQueueHighRoundRobin > 1 ? $this->maxQueueHighRoundRobin : self::MAX_LB_HIGH);
				Cache::forever($key, $num);
			} else {
				Cache::forever($key, $num);
			}
			$queuename .= ($num == 0 ? "" : ("_".$num));
		} catch ( Exception $e ) {
			Log::info("MultipleCache::getRoundRobinMultipleQueueName exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $queuename;
	}
	
	private function getKeyNameMultiple() {
		$key = self::KEY_LB_HIGH . "_" . $this->idxQueue;
		return $key;
	}
	
	public function putQueue( $action, $key, $value = null, $expiry = 0) {
		$b = false;
		try {
			if ($this->maxQueueConnection > 1 && ! empty($this->defaultQueueConnection)) {
				if ($this->isMemcached) {
					$arrMultiInstanceIP = array();
					if ($this->idxQueue >= 10) {
						$str = Config::get( 'webconf.web_server.multi_instance_ip' );
						if (! empty($str)) {
							$arrMultiInstanceIP = explode(";", $str);
						}
					}
					$myhostname = gethostname();
					$queuename = $this->getQueueName();
					for ($n = 2; $n <= $this->maxQueueConnection; $n++) {
						try {
							$alowed = true;
							$connection = $this->defaultQueueConnection . $n;
							if ($this->idxQueue >= 20) {
								$hostcon = Config::get("queue.connections.".$connection.".host");
								for ($i = 0; $alowed && $i < count($arrMultiInstanceIP); $i++) {
									if ($hostcon == trim($arrMultiInstanceIP[$i])) {
										$alowed = false;
									}
								}
							} 
							if ($alowed) {
								if (! is_null($key) && is_array($key)) {
									for ($i = 0; $i < count($key); $i++) {
										try {
											$job = new ActionCacheJob( $myhostname, $action, $key[$i], $value, $expiry );
											if (! empty($queuename)) {
												$job->onQueue($queuename);
											}
											$job->onConnection($connection);
											dispatch( $job );
										} catch ( Exception $e ) {
											Log::info("MultipleCache::putQueue action=".$action." key=".(is_null($key)?"null":(is_array($key)?json_encode($key):$key))." value=".(is_null($value)?"null":(is_array($value)?json_encode($value):$value))." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
										}
									}
								} else {
									$job = new ActionCacheJob( $myhostname, $action, $key, $value, $expiry );
									if (! empty($queuename)) {
										$job->onQueue($queuename);
									}
									$job->onConnection($connection);
									dispatch( $job );
								}
								$b = true;
							}
						} catch ( Exception $e ) {
							Log::info("MultipleCache::putQueue action=".$action." key=".(is_null($key)?"null":(is_array($key)?json_encode($key):$key))." value=".(is_null($value)?"null":(is_array($value)?json_encode($value):$value))." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
						}
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("MultipleCache::putQueue action=".$action." key=".(is_null($key)?"null":(is_array($key)?json_encode($key):$key))." value=".(is_null($value)?"null":(is_array($value)?json_encode($value):$value))." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
	protected function setStoreOther( $key, $value, $expiry ) {
		try {
			if ($this->maxStore > 1 && ! empty($this->defaultStore)) {
				if ($this->isMemcached) {
					for ($n = 2; $n <= $this->maxStore; $n++) {
						if (empty($expiry) || $expiry <= 0) {
							Cache::store( $this->defaultStore.$n )->forever( $key, $value );
						} else {
							Cache::store( $this->defaultStore.$n )->put( $key, $value, $expiry );
						}
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("MultipleCache::setStoreOther exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
	protected function delStoreOther( $key ) {
		try {
			if ($this->maxStore > 1 && ! empty($this->defaultStore)) {
				if ($this->isMemcached) {
					for ($n = 2; $n <= $this->maxStore; $n++) {
						Cache::store( $this->defaultStore.$n )->forget($key);
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("MultipleCache::delStoreOther exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
}