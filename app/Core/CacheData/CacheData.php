<?php
namespace App\Core\CacheData;

use Log;
use Config;
use Exception;
use Illuminate\Support\Facades\Cache;
use App\Core\Util\UtilCommon;

class CacheData
{
	protected $debug = false;
	private $keyPrefix = "";
	private $expiryMinutes = 0;

	public function __construct( $keyPrefix, $expiryMinutes = 0 ) {
		$this->debug = boolval(Config::get( 'app.debug' ));
		if (is_null($keyPrefix) || (! is_string($keyPrefix) && ! is_int($keyPrefix))) {
			$this->keyPrefix = "";
		} else {
			$this->keyPrefix = UtilCommon::cleanSpecialChar(trim((string)$keyPrefix), false);
		}
		if ($expiryMinutes < 0) {
			$this->expiryMinutes = 0;
		} else {
			$this->expiryMinutes = (int)$expiryMinutes;
		}
	}

	protected function getKey() {
		$key = $this->keyPrefix;
		try {
			$arglist = func_get_args();
			$n = func_num_args();
			$argnum = count($arglist);
			if ($argnum > 0) {
				for ($i = 0; $i < $argnum && $i < $n; $i++) {
					try {
						$v = UtilCommon::cleanSpecialChar(trim((string)$arglist[$i]), false);
						if (! empty($v)) {
							$key .= "_" . $v;
						}
					} catch ( Exception $e ) {
						Log::info("CacheData::getKey exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("CacheData::getKey exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		if (empty($key)) {
			$key = "A";
		}
		if ($this->debug) Log::info("CacheData::getKey key=".$key);
		return $key;
	}

	protected function getData( $key, $default = null ) {
		$res = null;
		try {
			if (! empty($key)) {
				if (Cache::has($key)) {
					$res = Cache::get($key, $default);
				} 
			}
		} catch ( Exception $e ) {
			Log::info("CacheData::getData key=".$key." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $res;
	}

	protected function setData( $key, $v ) {
		$b = false;
		try {
			if (! empty($key)) {
				if (empty($this->expiryMinutes)) {
					Cache::forever($key, $v);
					$b = true;
				} else {
					Cache::put($key, $v, $this->expiryMinutes);
					$b = true;
				}
			}
		} catch ( Exception $e ) {
			Log::info("CacheData::setData key=".$key." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}

	protected function deleteData( $key, $force = false ) {
		$b = false;
		try {
			if (! empty($key)) {
				if ($force) {
					Cache::forget($key);
					$b = true;
				} else {
					if (Cache::has($key)) {
						Cache::forget($key);
						$b = true;
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("CacheData::deleteData key=".$key." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}

}