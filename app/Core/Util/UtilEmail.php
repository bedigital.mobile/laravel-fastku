<?php
namespace App\Core\Util;

use Log;
use Config;
use Exception;
use App\Exceptions\Handler;
use Illuminate\Support\Facades\View;
use Mail;

class UtilEmail
{
	public static function sendEmailByTemplateView( $nameBlade, $paramBlade, $subject, $toEmail, $toFullname = "", $fromEmail = "", $fromFullname = "" ) {
		$b = false;
		try {
			Log::info("UtilEmail::sendEmailByTemplateView to=".$toEmail." from=".$fromEmail." subject=".$subject." nameblade=".$nameBlade." paramblade=".(is_array($paramBlade)?json_encode($paramBlade):(is_string($paramBlade)?$paramBlade:"")));
			if (empty($toFullname)) {
				$toFullname = $toEmail;
			}
			if (empty($fromFullname) && ! empty($fromEmail)) {
				$fromFullname = $fromEmail;
			}
			$pos = strpos($toEmail, ";");
			if ($pos > 0) {
				$arrToEmail = explode(";", $toEmail);
				$arrToFullname = explode(";", $toFullname);
				$maxToFullname = count($arrToFullname);
				$arrTo = array();
				for ($i = 0; $i < count($arrToEmail); $i++) {
					$email = trim($arrToEmail[$i]);
					$name = ($i<$maxToFullname ? $arrToFullname[$i] : $email);
					$p1 = strpos($email,'<');
					$p2 = strpos($email,'>');
					if ($p1 > 0 && $p2 > 0 && $p2 > $p1) {
						$name = trim(substr($email,0,$p1));
						$email = trim(substr($email,$p1+1,$p2-$p1-1));
					}
					$arrTo[$email] = $name;
				}
				if (empty($fromEmail)) {
					Mail::send ( $nameBlade, $paramBlade, function ($message) use($subject, $arrTo) {
								 $message->subject( $subject )
										 ->to( $arrTo );
					} );
					$b = true;
				} else {
					Mail::send ( $nameBlade, $paramBlade, function ($message) use($subject, $arrTo, $fromEmail, $fromFullname) {
								 $message->subject( $subject )
										 ->to( $arrTo )
										 ->from( $fromEmail, $fromFullname );
					} );
					$b = true;
				}
			} else {
				$p1 = strpos($toEmail,'<');
				$p2 = strpos($toEmail,'>');
				if ($p1 > 0 && $p2 > 0 && $p2 > $p1) {
					$toFullname = trim(substr($toEmail,0,$p1));
					$toEmail = trim(substr($toEmail,$p1+1,$p2-$p1-1));
				}
				if (empty($fromEmail)) {
					Mail::send ( $nameBlade, $paramBlade, function ($message) use($subject, $toEmail, $toFullname) {
								 $message->subject( $subject )
										 ->to( $toEmail, $toFullname );
					} );
					$b = true;
				} else {
					Mail::send ( $nameBlade, $paramBlade, function ($message) use($subject, $toEmail, $toFullname, $fromEmail, $fromFullname) {
								 $message->subject( $subject )
										 ->to( $toEmail, $toFullname )
										 ->from( $fromEmail, $fromFullname );
					} );
					$b = true;
				}
			}
		} catch ( Exception $e ) {
			Log::info("UtilEmail::sendEmailByTemplateView to=".$toEmail." from=".$fromEmail." subject=".$subject." nameblade=".$nameBlade." paramblade=".(is_array($paramBlade)?json_encode($paramBlade):(is_string($paramBlade)?$paramBlade:""))." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}

	public static function isValidEmail($s) {
		if (!empty($s)) {
			if(preg_match("/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)*\.\w{2,8}$/", $s)) {
				return true;
			}
		}
		return false;
	}

}