<?php
namespace App\Core\Util;

use Log;
use Exception;

class UtilPhone
{
	private static $_IAC = array( '001', '007', '008', '009', '01010', '010100' );

	public static function isIAC($n) {
		$b = false;
		for ($i=0; !$b && $i < count(self::$_IAC); $i++) {
			if (self::$_IAC[$i] == $n) {
				$b = true;
			}
		}
		return $b;
	}

	public static function normalizeToMSISDN( $dn, $forsignup = true, $cc = "62", $ac = "" ) {
		$msisdn = $dn;
		$len = strlen($dn);
		if ($len > 3) {
			$d1 = substr($dn, 0, 1);
			$d2 = substr($dn, 0, 2);
			$d3 = substr($dn, 0, 3);
			$d5 = substr($dn, 0, 5);
			if ($d1 == '+') {
				$msisdn = substr($dn, 1);
			} else if ($d1 == '0' && $d2 != '00' && ! self::isIAC($d3) && ! self::isIAC($d5)) {
				$msisdn = $cc . substr($dn, 1);
			} else if ($d2 == '00' && ! self::isIAC($d3) && ! self::isIAC($d5)) {
				$msisdn = substr($dn, 2);
			} else {
				if (self::isIAC($d3)) {
					$msisdn = substr($dn, 3);
				} else {
					$d6 = substr($dn, 0, 6);
					if (self::isIAC($d6)) {
						$msisdn = $cc . substr($dn, 6);
					} else if (self::isIAC($d5)) {
						$msisdn = substr($dn, 5);
					} else if (!empty($cc) && !$forsignup) {
						$check = substr($dn, 0, strlen($cc));
						if ($check != $cc) {
							if (empty($ac)) {
								$msisdn = $cc . $dn;
							} else {
								if ($len >= 6 && $len <= 8) {
									$msisdn = $cc . $ac . $dn;
								}
							}
						}
					}
				}
			}
		}
		return $msisdn;
	}

	public static function isSC( $dn ) {
		$len = strlen($dn);
		if ($len <= 6) {
			return true;
		}
		return false;
	}
	
	public static function isPhoneValid( $contactphone ) {
		if (! empty($contactphone) && ! preg_match('/^(\+|\d)[0-9]{6,22}$/', $contactphone)) {
			return false;
		}
		return true;
	}
	
	public static function trimCharPhoneNumber( $v ) {
		return str_replace(array(" ", "-", "(", ")"), "", trim($v));
	}

}