<?php
namespace App\Core\Util;

use Log;
use Exception;
use App\Exceptions\Handler;
use DateTime;

class UtilCommon
{
	public static function intVersionAPI( $ver ) {
		$v = 1;
		try {
			if (! empty($ver) && ! is_int($ver)) {
				$ver = strtolower(trim($ver));
				if (strlen($ver) >= 2) {
					if (substr($ver, 0, 1) == "v") {
						$ver = substr($ver, 1);
					}
				}
				if (self::isDigit($ver)) {
					$v = (int)$ver;
				}
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::intVersionAPI exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		if ($v < 1) {
			$v = 1;
		} else if ($v > 127) {
			$v = 127;
		}
		return $v;
	}
	
	public static function GUID($max = 0) {
		// version 4 GUID + versi microtime (budi)
		$guid = sprintf(
				'%08x%04x%04x%02x%02x%012x',
				mt_rand(),
				mt_rand(0, 65535),
				bindec(substr_replace(
						sprintf('%016b', mt_rand(0, 65535)), '0100', 11, 4)
				),
				bindec(substr_replace(sprintf('%08b', mt_rand(0, 255)), '01', 5, 2)),
				mt_rand(0, 255),
				mt_rand()
		);
		$guid .= self::getMicrotime();
		if ($max > 0 && strlen($guid) > $max) {
			$guid = substr($guid,0,$max);
		}
		return $guid;
	}
	
	public static function getMicrotime() {
		$micro = "";
		$str = microtime(true);
		$p = strpos($str, ".");
		if ($p >= 0) {
			$depan = "";
			if ($p > 0) {
				$depan = substr($str,0,$p);
			}
			$belk = substr($str,$p+1);
			while (strlen($depan) < 12) {
				$depan = "0" . $depan;
			}
			while (strlen($belk) < 4) {
				$belk = $belk . "0";
			}
			$micro = $depan . $belk;
		}
		return $micro;
	}
	
	public static function createTransactionID() {
		list($usec, $sec) = explode(" ", microtime());
		$res = (string)($sec . str_pad(($usec * 10000000), 8, 0, STR_PAD_LEFT));
		return $res;
	}
	
	public static function isDigit($s) {
		$b = (boolean)preg_match("/^[1-9][0-9]*$/", $s);
		return $b;
	}
	
	// format datetime = YYYY-MM-DD HH24:mm:SS
	public static function stringDateTimeToTimestamp( $s ) {
		$tm = 0;
		try {
			if (strlen($s) == 19) {
				// 0123456789012345678
				// 2017-01-06 18:01:02
				$y = (int)substr($s, 0, 4);
				$m = (int)substr($s, 5, 2);
				$d = (int)substr($s, 8, 2);
				$h = (int)substr($s, 11, 2);
				$min = (int)substr($s, 14, 2);
				$sec = (int)substr($s, 17, 2);
				$tm = mktime($h, $min, $sec, $m, $d, $y);
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::stringDateTimeToTimestamp datetime=".$s." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $tm;
	}
	
	// format datetime = YYYY-MM-DD HH24:mm:SS
	public static function diffDateTime( $dt1, $dt2, $unit = "SECOND" ) {
		$d = 0;
		try {
			$diff = abs(strtotime($dt2) - strtotime($dt1));
				
			$unit = strtoupper(trim($unit));
			if ($unit == "SECOND" || $unit == "SECONDS") {
				$d = $diff;
			} else if ($unit == "MINUTE" || $unit == "MINUTES") {
				$d = floor($diff / 60);
			} else if ($unit == "HOUR" || $unit == "HOURS") {
				$d = floor($diff / (60*60));
			} else if ($unit == "DAY" || $unit == "DAYS") {
				$d = floor($diff / (60*60*24));
			} else if ($unit == "MONTH" || $unit == "MONTHS") {
				$d = floor($diff / (30*60*60*24));
			} else if ($unit == "YEAR" || $unit == "YEARS") {
				$d = floor($diff / (365*60*60*24));
			} else {
				$years = floor($diff / (365*60*60*24));
				$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
				$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
				$hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
				$minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60) / 60);
				$seconds = $diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60;
				if ($unit == "ARRAY") {
					$d = array(
							'years' => $years,
							'months' => $months,
							'days' => $days,
							'hours' => $hours,
							'minutes' => $minutes,
							'seconds' => $seconds
					);
				} else {
					$d = $years." years, ".$months." months, ".$days." days, ".$hours." hours, ".$minutes." minutes, ".$seconds." seconds";
				}
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::diffDateTime dt1=".$dt1." dt2=".$dt2." unit=".$unit." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $d;
	}
	
	// format datetime = YYYY-MM-DD HH24:mm:SS
	public static function isDateTimeExpired( $dt ) {
		$b = false;
		try {
			if (! empty($dt)) {
				$tm = strtotime($dt);
				if ( $tm < time()) {
					$b = true;
				}
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::isDateTimeExpired dt=".(is_null($dt)?"null":$dt)." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
	// format datetime = YYYY-MM-DD HH24:mm:SS
	public static function isDateTimeBetween( $dt1, $dt2 ) {
		$b = false;
		try {
			if (empty($dt1) && empty($dt2)) {
				$b = true;
			} else {
				$b = true;
				if (! empty($dt1)) {
					if (! self::isDateTimeExpired( $dt1 )) {
						$b = false;
					}
				}
				if ($b && ! empty($dt2)) {
					if (self::isDateTimeExpired( $dt2 )) {
						$b = false;
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::isDateTimeBetween dt1=".(is_null($dt1)?"null":$dt1)." dt2=".(is_null($dt2)?"null":$dt1)." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $b;
	}
	
	public static function getServerUTC() {
		$gm = 0;
		try {
			$s = date('P');
			if (! empty($s)) {
				if (strpos($s, ':') > 0) {
					$arr = explode(':', $s);
					if (count($arr) > 1) {
						$gm = (((int)$arr[0]) * 3600) + ((int)$arr[1]);
					}
				} else {
					$gm = (int)$s;
				}
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::getServerUTC exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $gm;
	}
	
	public static function isDateValid($tgl) {
		// format = 2007-01-25
		//          0123456789
		$valid = false;
		if (strlen($tgl) == 10) {
			$thn = (int)substr($tgl,0,4);
			$tag1 = (int)substr($tgl,4,1);
			$bln = (int)substr($tgl,5,2);
			$tag2 = substr($tgl,7,1);
			$day = (int)substr($tgl,8,2);
			if ($thn > 0 && $bln > 0 && $bln <= 12 && $day > 0 && $day <= 31 && $tag1=="-" && $tag2=="-") {
				$valid = true;
				if ((($bln == 4) || ($bln == 6) || ($bln == 9) || ($bln == 11)) && ($day > 30)) $valid = false;
				else if ($bln == 2)	{
					$max = 28;
					if (((($thn % 400) == 0) || (($thn % 4) == 0)) && (($thn % 100) != 0)) $max = 29;
					else if ((($thn % 100) == 0)) $max = 29;
					if ($day > $max) $valid = false;
				}
			}
		}
		return $valid;
	}
	
	public static function checkDateValid($tgl) {
		try {
			if (self::isDateValid($tgl)) {
				if (function_exists("date_parse_from_format")) {
					$cektgl = date_parse_from_format("Y-m-d", $tgl);
					if ($cektgl['error_count'] == 0 && $cektgl['warning_count'] == 0) {
						if (checkdate($cektgl['month'], $cektgl['day'], $cektgl['year'])) {
							return true;
						}
					}
				} else {
					return true;
				}
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::checkDateValid exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public static function monthIndonesia( $str ) {
		$ret = $str;
		try {
			$ret = str_ireplace( 'January', 'Januari', $ret );
			$ret = str_ireplace( 'February', 'Februari', $ret );
			$ret = str_ireplace( 'March', 'Maret', $ret );
			$ret = str_ireplace( 'May', 'Mei', $ret );
			$ret = str_ireplace( 'June', 'Juni', $ret );
			$ret = str_ireplace( 'July', 'Juli', $ret );
			$ret = str_ireplace( 'August', 'Agustus', $ret );
			$ret = str_ireplace( 'Aug', 'Agt', $ret );
			$ret = str_ireplace( 'October', 'Oktober', $ret );
			$ret = str_ireplace( 'Oct', 'Okt', $ret );
			$ret = str_ireplace( 'December', 'Desember', $ret );
			$ret = str_ireplace( 'Dec', 'Des', $ret );
		} catch ( Exception $e ) {
			Log::info("UtilCommon::monthIndonesia exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $ret;
	}
	
	public static function stringDateTimeTransaction( $datetime, $infoTimezone = "WIB", $isIndo = true ) {
		$str = "";
		try {
			// 01234567890123456789
			// 2018-11-16 16:28:29
			if (strlen($datetime) >= 15) {
				$y = (int)substr($datetime, 0, 4);
				$m = (int)substr($datetime, 5, 2);
				$d = (int)substr($datetime, 8, 2);
				$h = substr($datetime, 11, 2);
				$min = substr($datetime, 14, 2);
				// $sec = (int)substr($s, 17, 2);
				$str = (string)$d . " ";
				switch ($m) {
					case 1: $str .= ($isIndo ? "Januari" : "January"); break;
					case 2: $str .= ($isIndo ? "Februari" : "February"); break;
					case 3: $str .= ($isIndo ? "Maret" : "March"); break;
					case 4: $str .= ($isIndo ? "April" : "April"); break;
					case 5: $str .= ($isIndo ? "Mei" : "May"); break;
					case 6: $str .= ($isIndo ? "Juni" : "June"); break;
					case 7: $str .= ($isIndo ? "Juli" : "July"); break;
					case 8: $str .= ($isIndo ? "Agustus" : "August"); break;
					case 9: $str .= ($isIndo ? "September" : "September"); break;
					case 10: $str .= ($isIndo ? "Oktober" : "October"); break;
					case 11: $str .= ($isIndo ? "November" : "November"); break;
					case 12: $str .= ($isIndo ? "Desember" : "December"); break;
				}
				$str .= " " . (string)$y . ", " . $h . ":" . $min;
				if (! empty($infoTimezone)) {
					$str .= " " . $infoTimezone;
				}
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::stringDateTimeTransaction exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $str;
	}
	
	public static function cleanSpecialChar( $str, $tolower = true ) {
		try {
			$str = trim($str);
			$str = str_replace(' ', '-', $str);
			$str = preg_replace('/[^A-Za-z0-9\-]/', '', $str);
			if ($tolower) {
				$str = strtolower($str);
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::cleanSpecialChar exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $str;
	}
	
	public static function queryReplace($sql, $bindings)
	{
		$needle = '?';
		try {
			foreach ($bindings as $replace){
				try {
					$pos = strpos($sql, $needle);
					if ($pos !== false) {
						if (is_null($replace)) {
							$replace = "null";
						} else {
							if ($replace == "" || ! self::isDigit($replace) ) {
								$replace = "'" . $replace . "'";
							}
						}
						$sql = substr_replace($sql, $replace, $pos, strlen($needle));
					}
				} catch ( Exception $e ) {
					Log::info("UtilCommon::queryReplace exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::queryReplace exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $sql;
	}
	
	public static function isPasswordValid( $password ) {
		if (! preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{6,40}$/', $password) ) {
			return false;
		}
		return true;
	}
	
	public static function isUsernameValid( $username ) {
		if (! preg_match('/^[a-z0-9]{4,50}$/', $username)) {
			return false;
		}
		return true;
	}
	
	public static function randomCode( $num = 4, $onlyAbjad = false, $casesensitive = false, $uppercase = false ) {
		$code = "";
		try {
			if ($num > 0) {
				srand( time() );
				$max = 0;
				if ($onlyAbjad && $casesensitive) {
					$max = 52;
					for ($i = 0; $i < $num; $i++) {
						$n = rand(0, $max - 1);
						if ($n < 26) {
							$code .= chr( ord('a') + $n );
						} else {
							$code .= chr( ord('A') + $n - 26 );
						}
					}
				} else if ($onlyAbjad && ! $casesensitive) {
					$max = 26;
					for ($i = 0; $i < $num; $i++) {
						$n = rand(0, $max - 1);
						if ($uppercase) {
							$code .= chr( ord('A') + $n );
						} else {
							$code .= chr( ord('a') + $n );
						}
					}
				} else if (! $onlyAbjad && $casesensitive) {
					$max = 62;
					for ($i = 0; $i < $num; $i++) {
						$n = rand(0, $max - 1);
						if ($n < 10) {
							$code .= chr( ord('0') + $n );
						} else if ($n >= 10 && $n < 36) {
							$code .= chr( ord('a') + $n - 10 );
						} else {
							$code .= chr( ord('A') + $n - 36 );
						}
					}
				} else if (! $onlyAbjad && ! $casesensitive) {
					$max = 36;
					for ($i = 0; $i < $num; $i++) {
						$n = rand(0, $max - 1);
						if ($n < 10) {
							$code .= chr( ord('0') + $n );
						} else {
							if ($uppercase) {
								$code .= chr( ord('A') + $n - 10 );
							} else {
								$code .= chr( ord('a') + $n - 10 );
							}
						}
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::randomCode exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $code;
	}
	
	public static function str_baseconvert( $str, $frombase = 10, $tobase = 36 ) {
		$s = "";
		$q = 0;
		try {
			$str = trim($str);
			if (intval($frombase) != 10) {
				$len = strlen($str);
				$q = 0;
				for ($i=0; $i<$len; $i++) {
					$r = base_convert($str[$i], $frombase, 10);
					$q = bcadd(bcmul($q, $frombase), $r);
				}
			} else {
				$q = $str;
			}
			if (intval($tobase) != 10) {
				$s = '';
				while (bccomp($q, '0', 0) > 0) {
					$r = intval(bcmod($q, $tobase));
					$s = base_convert($r, 10, $tobase) . $s;
					$q = bcdiv($q, $tobase, 0);
				}
			} else {
				$s = $q;
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::str_baseconvert str=".$str." frombase=".$frombase." tobase=".$tobase." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $s;
	}
	
	public static function intToBase36( $v ) {
		return self::str_baseconvert( $v, 10, 36 );
	}
	public static function base36ToInt( $v ) {
		return self::str_baseconvert( $v, 36, 10 );
	}	
	public static function idConvertToInt( $v ) {
		$i = 0;
		try {
			if (! empty($v)) {
				if (self::isDigit($v) && strlen($v) > 6) {
					$i = (int)$v;
				} else {
					$i = (int)self::str_baseconvert( $v, 36, 10 );
				}
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::idDataConvertToInt v=".$v." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $i;
	}
	public static function idConvertToBase36( $v ) {
		$s = "0";
		try {
			if (! empty($v)) {
				if (self::isDigit($v) && strlen($v) > 6) {
					$s = self::str_baseconvert( $v, 10, 36 );
				} else {
					$s = $v;
				}
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::idDataConvertToInt v=".$v." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		while (strlen($s) < 6) {
			$s = "0" . $s;
		}
		return $s;
	}
	
	public static function intToStringRP( $n ) {
		$str = "";
		try {
			$n = abs((int)$n);
			if ($n == 0) {
				return "nol rupiah";
			}
			$rp = number_format($n);
			$arr = explode(',',$rp);
			// sample: 789.567.610.311.812
			$start = 0;
			$jmlArr = count($arr);
			if ($jmlArr > 5) {
				$start = $jmlArr - 5;
			}
			for ($j = $start; $j < $jmlArr; $j++) {
				$s = "";
				try {
					$p = $jmlArr - $j - $start;
					$i = (int)$arr[$j];
					if (strlen($arr[$j]) < 3) {
						$arr[$j] = (string)sprintf("%03d", $i);
					}
					for ($k = 0; $k <= 2; $k++) {
						$h = ($k >= 1 ? (int)substr($arr[$j],1,2): 0);
						$d = 0;
						if ($h >= 11 && $h <= 19) {
							$d = (int)substr($arr[$j],($k == 2 ? 1 : 2),1);
						} else {
							$d = (int)substr($arr[$j],$k,1);
						}
						switch ($d) {
							case 1:
								if ($k == 2 && $h >= 11 && $h <= 19) {
									$s .= ($h == 11 ? "": " ") . "belas";
								} else {
									$s .= ($k == 0 || $k == 1 || ($k == 2 && $p == 2 && $i == 1) ? " se" : " satu");
								}
								break;
							case 2: $s .= " dua"; break;
							case 3: $s .= " tiga"; break;
							case 4: $s .= " empat"; break;
							case 5: $s .= " lima"; break;
							case 6: $s .= " enam"; break;
							case 7: $s .= " tujuh"; break;
							case 8: $s .= " delapan"; break;
							case 9: $s .= " sembilan"; break;
						}
						if ($d > 0) {
							switch ($k) {
								case 0: $s .= ($d == 1?"":" ") . "ratus"; break;
								case 1: $s .= ($d == 1?"":" ") . ($h >= 11 && $h <= 19 ? "" : "puluh"); break;
							}
						}
					}
					switch ($p) {
						case 5: $s .= " tiryun"; break;
						case 4: $s .= " milyar"; break;
						case 3: $s .= " juta"; break;
						case 2: $s .= ($i == 1 ? "" : " ") . "ribu"; break;
					}
				} catch ( Exception $e ) {
					Log::info("UtilCommon::intToStringRP exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
				}
				$str .= $s;
			}
		} catch ( Exception $e ) {
			Log::info("UtilCommon::intToStringRP exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		$str .= " rupiah";
		return $str;
	}
	
	public static function definePage($page, $max) {
		if ($page < 1) {
			$page = 1;
		}
		if ($max < 1) {
			$max = 1000;
		}
		$startID = ($page - 1) * $max;
		return array(
			"startID" => $startID,
			"max" => $max
		);
	}

	public static function microtimeStamp() {
		$t = microtime(true);
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		$d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
        return $d->format("YmdHisu");		
	} 

	public static function concateStr( $existing, $new, $delimiter ) {
		if ($existing != '') {
			if ( $new != '' ) {
				return $existing . $delimiter . $new;	
			} else {
				return $existing;
			}			
		} else {
			return $new;
		}
	}

	public static function getContent($url, $post = '') {
		$usecookie = __DIR__ . "/cookie.txt";
		$header[] = 'Content-Type: application/json';
		$header[] = "Accept-Encoding: gzip, deflate";
		$header[] = "Cache-Control: max-age=0";
		$header[] = "Connection: keep-alive";
		$header[] = "Accept-Language: en-US,en;q=0.8,id;q=0.6";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_VERBOSE, false);
		// curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_ENCODING, true);
		curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36");

		if ($post)
		{
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		}

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$rs = curl_exec($ch);

		if(empty($rs)){
			var_dump($rs, curl_error($ch));
			curl_close($ch);
			return false;
		}
		curl_close($ch);
		return $rs;
	}		
	
}