<?php
namespace App\Core\Biller;

abstract class BillerAbstract {
	abstract protected function executeTrx( $trxParams = array() );
	abstract protected function getLog();
}
