<?php
namespace App\Core\Biller;

use App\Core\Biller\BillerAbstract;
use App\Core\Util\UtilCommon;
use GuzzleHttp\Client;

class BillerMPOSPAYJSON extends BillerAbstract {
	
	private $httpUserAgent = "";
	private $httpOrigin = "";
	private $httpAuthorization = "";
	private $result = null;
	private $uri = "";
	private $log = "";

	public function __construct( $uri ) {
		$this->uri = $uri;
	}

	public function executeTrx( $trxParams = array() ) {

		$client = new Client();

		
		$params = array (
			"type" => $trxParams['type'],
			"accountType" => $trxParams['accountType'],
			"account" => $trxParams['account'],
			"institutionCode" => $trxParams['institutionCode'],
			"product" => $trxParams['product'],
			"billNumber" => $trxParams['billNumber'],
			"trxId" => $trxParams['trxId'],
			"retrieval" => $trxParams['retrieval'],
			"sign" => $trxParams['sign']
		);

		$header = array(
				'Content-type' 		=> 'application/json',
				'Accept' 			=> 'application/json',
				'Authorization' 	=> $this->httpAuthorization,
				'Origin' 			=> $this->httpOrigin,
				'Content-length' 	=> strlen( json_encode($params) )	
		);

		try {
			$this->result = $client->post($this->uri, 
				[
					"headers" => $header,
					"json" => $params 
				]
			);				
		} catch (ClientErrorResponseException $exception) {
			//...
		}

		return json_decode( $this->result->getBody() );

	} // End of executeTrx

	public function getLog() {
		if (! is_null($this->result) ) {
			$log = array (
				"httpStatusCode" => $this->result->getStatusCode(),
				"httpReasonPhrase" => $this->result->getReasonPhrase()
			);
		} else {
			$log = array (
				"httpStatusCode" => '',
				"httpReasonPhrase" => ''
			);
		}
		return $log;
	}

} // End of BillerMPOSPAYJSON
