<?php
namespace App\Core\Biller;

use App\Core\Biller\BillerAbstract;
use App\Core\Util\CommonUtils;
use GuzzleHttp\Client;

class BillerMPOSPAY extends BillerAbstract {
	
	private $account = array();
	private $trx = array();
	private $signature = array();
	private $connection = array();
	private $result;

	//Account key
	const KEY_ACCOUNT_TYPE= "accountType";
	const KEY_ACCOUNT = "account";
	const KEY_INSTITUTION_CODE = "institutionCode";

	//Transaction key
	const KEY_TRANSACTION_TYPE= "type";
	const KEY_PRODUCT = "product";
	const KEY_BILLNUMBER = "billNumber";

	//Product identity key
 	const KEY_PRODUCT_ID = "productID";
	const KEY_PRODUCT_CODE = "productCode";

	//Signature key
	const KEY_TRX_ID = "trxId";
	const KEY_RETRIEVAL = "retrieval";
	const KEY_SIGN = "sign";

	//Connection key
	const KEY_URL = "url";
	const KEY_USERAGENT = "userAgent";
	const KEY_ORIGIN = "origin";
	const KEY_AUTHORIZATION = "authorization";

	//Response
	const RESPONSE_STATUS_CODE = "statusCode";
	const RESPONSE_HEADER = "header";
	const RESPONSE_JSON = "json";


	//Transaction type
	const TRANSACTION_INQUIRY = "inquiry";
	const TRANSACTION_PAYMENT = "payment";
	const TRANSACTION_PURCHASE = "purchase";
	const TRANSACTION_ADVICE = "advice";
	const TRANSACTION_BALANCEINQUIRY = "balanceInquiry";


	public function __construct( ) {

	}

	public function init() {
		$this->account = array (
			self::KEY_ACCOUNT_TYPE => 'B2B',
			self::KEY_ACCOUNT => '',
			self::KEY_INSTITUTION_CODE => ''
		);

		$this->trx = array (
			self::KEY_TRANSACTION_TYPE => self::TRANSACTION_INQUIRY,
			self::KEY_BILLNUMBER => '',
			self::KEY_PRODUCT => ''			
		);

		$this->connection = array (
			self::KEY_URL => '',
			self::KEY_USERAGENT => '',
			self::KEY_ORIGIN => '',
			self::KEY_AUTHORIZATION => ''
		);

		$this->signature = array (
			self::KEY_TRX_ID => '',
			self::KEY_RETRIEVAL => '',
			self::KEY_SIGN => ''
		);
		return $this;
	}

	public function setAccount( $params = array() ) {
		if ( array_key_exists(self::KEY_ACCOUNT_TYPE, $params) ) {
			$this->account[self::KEY_ACCOUNT_TYPE] = $params[self::KEY_ACCOUNT_TYPE];
		}
		if ( array_key_exists(self::KEY_ACCOUNT, $params) ) {
			$this->account[self::KEY_ACCOUNT] = $params[self::KEY_ACCOUNT];
		}
		if ( array_key_exists(self::KEY_INSTITUTION_CODE, $params) ) {
			$this->account[self::KEY_INSTITUTION_CODE] = $params[self::KEY_INSTITUTION_CODE];
		}
	}

	public function inquiry( $params = array() ) {	
		$this->setTransaction(self::TRANSACTION_INQUIRY, $params);
		$this->executeTrx();
	}	

	public function purchase( $params = array() ) {
		$this->setTransaction(self::TRANSACTION_PURCHASE, $params);
		$this->executeTrx();
	}

	public function payment( $params = array() ) {
		$this->setTransaction(self::TRANSACTION_PAYMENT, $params);
		$this->executeTrx();
	}

	public function advice( $params = array() ) {
		$this->setTransaction(self::TRANSACTION_ADVICE, $params);
		$this->executeTrx();
	}

	public function balanceInquiry( $params = array() ) {
		$this->setTransaction(self::TRANSACTION_BALANCEINQUIRY, $params);
		$this->executeTrx();
	}

	public function setTransaction( $type, $params = array() ) {

		$this->trx[self::KEY_TRANSACTION_TYPE] = $type;
		
		if ( array_key_exists(self::KEY_PRODUCT_CODE, $params) ) {
			if ($params[self::KEY_PRODUCT_CODE] != '') {
				$this->trx[self::KEY_PRODUCT] = $params[self::KEY_PRODUCT_CODE];
			}
		}

		if ( array_key_exists(self::KEY_PRODUCT_ID, $params) ) {
			if ($params[self::KEY_PRODUCT_ID] != '') {
				$this->trx[self::KEY_PRODUCT] = $params[self::KEY_PRODUCT_ID];
			}
		}

		if ( array_key_exists(self::KEY_BILLNUMBER, $params) ) {
			if ($params[self::KEY_BILLNUMBER] != '') {
				$this->trx[self::KEY_BILLNUMBER] = $params[self::KEY_BILLNUMBER];
			} else {
				$this->trx[self::KEY_BILLNUMBER] = '';
			}
		}		
	}

	public function setSignature( $params = array() ) {
		if ( array_key_exists(self::KEY_TRX_ID, $params) ) {
			if ($params[self::KEY_TRX_ID] != '') {
				$this->signature[self::KEY_TRX_ID] = $params[self::KEY_TRX_ID];
			}
		}
		if ( array_key_exists(self::KEY_RETRIEVAL, $params) ) {
			if ($params[self::KEY_RETRIEVAL] != '') {
				$this->signature[self::KEY_RETRIEVAL] = $params[self::KEY_RETRIEVAL];
			}
		}
		if ( array_key_exists(self::KEY_SIGN, $params) ) {
			if ($params[self::KEY_SIGN] != '') {
				$this->signature[self::KEY_SIGN] = $params[self::KEY_SIGN];
			}
		}
	} 

	public function setConnection( $params = array() ) {
		if ( array_key_exists(self::KEY_URL, $params) ) {
			if ($params[self::KEY_URL] != '') {
				$this->connection[self::KEY_URL] = $params[self::KEY_URL];
			}
		}
		if ( array_key_exists(self::KEY_USERAGENT, $params) ) {
			if ($params[self::KEY_USERAGENT] != '') {
				$this->connection[self::KEY_USERAGENT] = $params[self::KEY_USERAGENT];
			}
		}
		if ( array_key_exists(self::KEY_ORIGIN, $params) ) {
			if ($params[self::KEY_ORIGIN] != '') {
				$this->connection[self::KEY_ORIGIN] = $params[self::KEY_ORIGIN];
			}
		}
		if ( array_key_exists(self::KEY_AUTHORIZATION, $params) ) {
			if ($params[self::KEY_AUTHORIZATION] != '') {
				$this->connection[self::KEY_AUTHORIZATION] = $params[self::KEY_AUTHORIZATION];
			}
		}
	} 

	public function executeTrx() {
		$data = array (
			self::KEY_TRANSACTION_TYPE => $this->trx[self::KEY_TRANSACTION_TYPE],
			self::KEY_ACCOUNT_TYPE => $this->account[self::KEY_ACCOUNT_TYPE],
			self::KEY_ACCOUNT => $this->account[self::KEY_ACCOUNT],
			self::KEY_INSTITUTION_CODE => $this->account[self::KEY_INSTITUTION_CODE],
			self::KEY_PRODUCT => $this->trx[self::KEY_PRODUCT],
			self::KEY_BILLNUMBER => $this->trx[self::KEY_BILLNUMBER],
			self::KEY_TRX_ID => $this->signature[self::KEY_TRX_ID],
			self::KEY_RETRIEVAL => $this->signature[self::KEY_RETRIEVAL],
			self::KEY_SIGN => $this->signature[self::KEY_SIGN]
		);

		$json_data = json_encode($data, JSON_FORCE_OBJECT );


		$client = new Client();

		// Set headers
		/*
		$client->setDefaultOption('headers', 
			array(
				'Content-type' 		=> 'application/json',
				'Accept' 			=> 'application/json',
				'Authorization' 	=> $this->connection[self::KEY_AUTHORIZATION],
				'Origin' 			=> $this->connection[self::KEY_ORIGIN],
				'Content-length' 	=> strlen($json_data)	
			));
		$client->setDefaultOption('http', 
			array(
				'protocol_version' 	=> 1.1,
				'user_agent' 		=> $this->connection[self::KEY_USERAGENT],
				'content' 			=> $json_data	
			));
		*/	

	

			$x = array (
				"type" => "purchase",
				"accountType" => "B2B",
				"account" => "0000000018",
				"institutionCode" => "180309000002",
				"product" => "30099",
				"billNumber" => "01101810834",
				"trxId" => "00000001",
				"retrieval" => "20180125003025879",
				"sign" => "b3847e3a099979c1d96e3d186e0e05fa"
			);



		$uri = $this->connection[self::KEY_URL];

		$header = array(
				'Content-type' 		=> 'application/json',
				'Accept' 			=> 'application/json',
				'Authorization' 	=> $this->connection[self::KEY_AUTHORIZATION],
				'Origin' 			=> $this->connection[self::KEY_ORIGIN],
				'Content-length' 	=> strlen(json_encode($x))	
			);



			/*
			$data = array (
				"type" => "purchase",
				"accountType" => "B2B",
				"account" => "0000000018",
				"institutionCode" => "180309000002",
				"product" => "30099",
				"billNumber" => "01101810834",
				"trxId" => "00000001",
				"retrieval" => "20180125003025879",
				"sign" => "b3847e3a099979c1d96e3d186e0e05fa"
			);
			*/


		try {
	


			//$response = $client->post($uri, $args);

			//$response = $client->post($uri, $header, $json_data);
			$response = $client->post($uri, [
				"headers" => $header,
				"json" => $x 
			]);

			// Wrap result
			/*
			$result = array (
				self::RESPONSE_STATUS_CODE => $response->getStatusCode(),
				self::RESPONSE_HEADER => $response->getHeader(),
				self::RESPONSE_JSON => json_decode($response->getBody())
			);
			*/
			var_dump($data);
			//var_dump($response->getStatusCode());
			var_dump(json_decode($response->getBody()));

		} catch (ClientErrorResponseException $exception) {
			// Wrap result
			/*
			$result = array (
				self::RESPONSE_STATUS_CODE => $exception->getResponse()->getStatusCode(),
				self::RESPONSE_HEADER => $exception->getResponse()->getHeader(),
				self::RESPONSE_JSON => json_decode($exception->getResponse()->getBody())
			);
			*/
		}

		return "OK";

	}

}
