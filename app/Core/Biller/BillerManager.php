<?php
namespace App\Core\Biller;

use App\Core\Biller\BillerMPOSPAYJSON;

class BillerManager {
	
	private $billerObj = null;
	private $version;
	private $billerProvider;
	private $uri;

	const MPOSPAY_JSON = 'MPOSPAY_JSON';

	public function __construct( $billerProvider, $uri, $version = '1.0' ) {
		$this->billerProvider = $billerProvider;
		$this->version = $version;
		$this->uri = $uri;
		$this->selectBiller();
	}

	protected function getBiller() {
		return $this->billerObj;
	}
	
	private function selectBiller() {
		switch ($this->billerProvider) {
			case self::MPOSPAY_JSON :
				$this->billerObj = new BillerMPOSPAYJSON($this->uri);
				break;			
			default:
				//...
				break;
		}
	}

}
