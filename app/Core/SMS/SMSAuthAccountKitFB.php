<?php
namespace App\Core\SMS;

use Log;
use Exception;
use App\Exceptions\Handler;
use GuzzleHttp\json_encode;


class SMSAuthAccountKitFB extends SMSAuthAbstract {	
	private $logstr;
	private $config;
	private $isUseAuthCode = false;
	
	const KEY_APP_ID = 'app_id';
	const KEY_APP_SECRET = 'app_secret';
	const KEY_API_VERSION = 'default_graph_version';
	
	const API_VERSION = "v1.2";
	const APP_ID = "719952841676048";
	const APP_SECRET = "5fe427fbbee6ac74983d25c852966ad1";
	
	// === edit by mas budi
	public function __construct( $appID = "", $appSecret = "", $apiVersion = "" ) {
		$this->fb = null;
		$this->config = array(
			self::KEY_APP_ID => (! empty($appID) ? $appID : self::APP_ID),
			self::KEY_APP_SECRET => (! empty($appSecret) ? $appSecret : self::APP_SECRET),
			self::KEY_API_VERSION => (! empty($apiVersion) ? $apiVersion : self::API_VERSION) 
		);
		$this->init();
	}
	
	public function setUseAuthorizationCode( $v = true ) {
		$this->isUseAuthCode = boolval($v);
		return $this;
	}
	
	private function init() {
		$this->logstr = "";
	}
	
	public function setConfig( $key, $value ) {
		if (   $key == self::KEY_APP_ID 
			|| $key == self::KEY_APP_SECRET 
			|| $key == self::KEY_API_VERSION ) {
			$this->config[$key] = $value;
		}
		return $this;
	}

	public function setConfigAppID( $value ) {
		$this->setConfig( self::KEY_APP_ID, $value );
		return $this;
	}
	
	public function setConfigSecretID( $value ) {
		$this->setConfig( self::KEY_APP_SECRET, $value );
		return $this;
	}

	public function setConfigApiKey( $value ) {
		$this->setConfig( self::KEY_API_VERSION, $value );
		return $this;
	}
	
	public function getConfig() {
		return $this->config;
	}
	
	public function getLogString() {
		return $this->logstr;
	}

	public function verifyAccessToken( $accessToken, $phonenumber ) {
        try {
        	if ($this->isUseAuthCode) {
        		Log::info("SMSAuthAccountKitFB::verifyAccessToken authorization_code_fb=".$accessToken);
        		$accessToken = $this->getAccessTokenByAuthorization($accessToken);
        		if (empty($accessToken)) {
        			return false;
        		}
           	}
           	// Get config from database based on whiteLabel. For now we will use default. 
            $url = "https://graph.accountkit.com/".$this->config[self::KEY_API_VERSION]."/me?access_token=" . $accessToken;
            Log::info("SMSAuthAccountKitFB::verifyAccessToken access_token_fb=".$accessToken."  URL=".$url);
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($ch);
            curl_close($ch);

            /* sample response:
				{  
				   id: "1234512345123451",
				   phone: {
				     number: "+15551234567"
				     country_prefix: "1",
				     national_number: "5551234567"
				   },
				   application: {
				     id: "5432154321543210"
				   }
				}
             */
            Log::info("SMSAuthAccountKitFB::verifyAccessToken result_from_fb=".$result);
            $response = json_decode($result, true);
            Log::info("SMSAuthAccountKitFB::verifyAccessToken response_from_fb=".json_encode($response));
            $return = false;
            if ( array_key_exists('phone', $response) && is_array($response['phone']) ) {
                $phone = $response['phone'];
                if (! is_null($phone) && is_array($phone)) {
                	if (array_key_exists('number', $phone)) {
                		$valphone = $phone['number'];
                		if (substr($valphone, 0, 1) == '+') {
                			$valphone = substr($valphone, 1);
                		}
                		if ($valphone == $phonenumber) {
                			$return = true;
                		}
                	}
                	if (! $return) {
		                if ( array_key_exists('country_prefix', $phone) && array_key_exists('national_number', $phone) ) {
		                	$valphone = $phone['country_prefix'] . $phone['national_number'];
		                	if ($valphone == $phonenumber) {
		                		$return = true;
		                	}
		                }
                	}
                }
            }
			try {
	        	$this->logstr = 
	        			 "SMSAuthAccountKitFB::verifyAccessToken->"
						."accessToken=".$accessToken
						.";phonenumber=".$phonenumber
						.";verificationResult=".($return?"true":"false")
						.";verificationResponse=".json_encode($response);
        	} catch (Exception $e) {
        		Log::info("SMSAuthAccountKitFB::verifyAccessToken exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        	}
        	return $return;

        } catch (Exception $e) {
        	$this->logstr = 
        			 "SMSAuthAccountKitFB::verifyAccessToken->"
					.";exception=[L:".$e->getLine().",C:".$e->getCode().",message:".$e->getMessage()."]";
			Log::info("SMSAuthAccountKitFB::verifyAccessToken exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
        }
		return false;
	} 
	
	public function getAccessTokenByAuthorization( $code ) {
		$accessToken = "";
		try {
			$url = "https://graph.accountkit.com/".$this->config[self::KEY_API_VERSION]."/access_token?grant_type=authorization_code&code=" . $code . "&access_token=AA|".$this->config[self::KEY_APP_ID]."|".$this->config[self::KEY_APP_SECRET];
			Log::info("SMSAuthAccountKitFB::getAccessTokenByAuthorization authorization_code=".$code. " ==> URL=".$url);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$result = curl_exec($ch);
			curl_close($ch);
			
			Log::info("SMSAuthAccountKitFB::getAccessTokenByAuthorization result_data_fb=".$result);
			if (! empty($result)) {
				$data = json_decode($result, true);
				if (array_key_exists('access_token', $data)) {
					$accessToken = $data['access_token'];
				}
			}
		} catch ( Exception $e ) {
			Log::info("SMSAuthAccountKitFB::getAccessTokenByAuthorization exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $accessToken;
	}
	
	
} // End of class