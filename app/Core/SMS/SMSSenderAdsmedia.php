<?php
namespace App\Core\SMS;

use Log;
use Config;
use Exception;
use App\Exceptions\Handler;
use App\Core\Util\UtilPhone;

class SMSSenderAdsmedia extends SMSSenderAbstract {
	
	const SMS_TYPE_REGULER  = "REGULER";
	const SMS_TYPE_MASKING  = "MASKING";
	const SMS_TYPE_OTP 		= "OTP";
	
	const API_END_POINT_SMS_REGULER = "/sms/api_sms_reguler_send_json.php";
	const API_END_POINT_SMS_MASKING = "/sms/api_sms_masking_send_json.php";
	const API_END_POINT_SMS_OTP 	= "/sms/api_sms_otp_send_json.php";
	
	const API_END_POINT_CHECK_BALANCE_REGULER 	= "/sms/api_sms_reguler_balance_json.php";
	const API_END_POINT_CHECK_BALANCE_MASKING 	= "/sms/api_sms_masking_balance_json.php";
	const API_END_POINT_CHECK_BALANCE_OTP 		= "/sms/api_sms_otp_balance_json.php";
	
	private $debug = false;
	private $urlEndPoint = "";
	private $typeSMS = "";
	private $apiKey = "";
	private $urlCallback = "";
	private $errCode = 0;
	private $errMsg = "";
	private $resultOriginal = array();
	
	public function __construct( $urlEndPoint, $typeSMS, $apiKey, $urlCallback = "" ) {
		try {
			$this->debug = boolval(Config::get( 'app.debug' ));
			$this->urlEndPoint = $urlEndPoint;
			$this->typeSMS = $typeSMS;
			$this->apiKey = $apiKey;
			$this->urlCallback = $urlCallback;
		} catch ( Exception $e ) {
			Log::info("SMSSenderAdsmedia::__construct exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
	private function payloadSMSSend( $to, $msg, $sendingAt = "" ) {
		try {
			$to = (string)$to;
			$arrTo = explode(";", $to);
			$data = array(
				'apikey' 		=> $this->apiKey,  
				'callbackurl' 	=> $this->urlCallback, 
				'datapacket'	=> array()	
			);
			for ($i = 0; $i < count($arrTo); $i++) {
				$arrTo[$i] = trim($arrTo[$i]);
				if (! empty($arrTo[$i]) && UtilPhone::isPhoneValid($arrTo[$i])) {
					$arrTo[$i] = UtilPhone::normalizeToMSISDN($arrTo[$i]);
					$package = array (
							'number' 			=> $arrTo[$i],
							'message' 			=> urlencode(stripslashes(utf8_encode($msg))),
							'sendingdatetime' 	=> $sendingAt
					);
					$data['datapacket'][] = $package;
				}
			}
			return json_encode($data);
		} catch ( Exception $e ) {
			Log::info("SMSSenderAdsmedia::payloadSMSSend exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return "";
	}
	
	public function getErrorCode() {
		return $this->errCode;
	}
	public function getErrorMessage() {
		return $this->errMsg;
	}
	public function getResultOriginal( $useArray = true ) {
		if ($useArray) {
			return $this->resultOriginal;
		} else {
			return json_encode($this->resultOriginal);
		}
	}
	public function isSuccess() {
		return ($this->errCode == 10 ? true : false);
	}
	
	public function send( $to, $msg, $sendingAt = "" ) {
		try {
			$this->resultOriginal = array();
			$url = $this->urlEndPoint;
			if (substr($url, -1) == '/') {
				$url = substr($url, 0, strlen($url) - 1);
			}
			if ($this->typeSMS == self::SMS_TYPE_REGULER) {
				$url .= self::API_END_POINT_SMS_REGULER;
			} else if ($this->typeSMS == self::SMS_TYPE_MASKING) {
				$url .= self::API_END_POINT_SMS_MASKING;
			} else if ($this->typeSMS == self::SMS_TYPE_OTP) {
				$url .= self::API_END_POINT_SMS_OTP;
			} else {
				$url .= self::API_END_POINT_SMS_REGULER;
			}
			$dt = $this->payloadSMSSend($to, $msg, $sendingAt);
			if ($this->debug) Log::info("SMSSenderAdsmedia::send URL_API=".$url." data_json=".$dt);
			$curlHandle = curl_init( $url );
			curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $dt);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($dt))
			);
			curl_setopt($curlHandle, CURLOPT_TIMEOUT, 5);
			curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, 5);
			$hasil = curl_exec($curlHandle);
			$curl_errno = curl_errno($curlHandle);
			$curl_error = curl_error($curlHandle);
			$http_code  = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
			curl_close($curlHandle);
			if ($this->debug) Log::info("SMSSenderAdsmedia::send errno=".$curl_errno." error=".$curl_error." http_code=".$http_code." result=".$hasil);
			$jsonResult = array();
			if ($curl_errno > 0) {
				$jsonResult = array (
						'sending_respon' => array (
								0 => array(
									'globalstatus' 		=> 90,
									'globalstatustext' 	=> $curl_errno."|".$http_code
								)
						)
				);
			} else {
				if ($http_code<>"200") {
					$jsonResult = array(
							'sending_respon'=> array (
									0 => array (
										'globalstatus' 		=> 90,
										'globalstatustext' 	=> $curl_errno."|".$http_code
									)
							)
					);
				} else {
					$jsonResult = json_decode( $hasil, true, 512, JSON_BIGINT_AS_STRING );
				}
			} 
			if (isset($jsonResult['sending_respon'])) {
				if (! is_null($jsonResult['sending_respon']) && is_array($jsonResult['sending_respon']) && count($jsonResult['sending_respon']) > 0) {
					if (isset($jsonResult['sending_respon'][0]['globalstatus'])) {
						$this->errCode = $jsonResult['sending_respon'][0]['globalstatus'];
					}
					if (isset($jsonResult['sending_respon'][0]['globalstatustext'])) {
						$this->errMsg = $jsonResult['sending_respon'][0]['globalstatustext'];
					}
				}
			}
			$this->resultOriginal = $jsonResult;
			if ($this->isSuccess()) {
				/* sample JSON response
				 {
					  "sending_respon": [
					    {
					      "globalstatus": 10,
					      "globalstatustext": "Success",
					      "datapacket": [
					        {
					          "packet": {
					            "number": "6281380298484",
					            "sendingid": 5159489,
					            "sendingstatus": 10,
					            "sendingstatustext": "success",
					            "price": 120
					          }
					        }
					      ]
					    }
					  ]
					}
				 */
				if (isset($jsonResult['sending_respon'][0]['datapacket'])) {
					if (! is_null($jsonResult['sending_respon'][0]['datapacket']) && is_array($jsonResult['sending_respon'][0]['datapacket'])) {
						$arrResult = array();
						for ($i = 0; $i < count($jsonResult['sending_respon'][0]['datapacket']); $i++) {
							try {
								if (isset($jsonResult['sending_respon'][0]['datapacket'][$i]['packet'])) {
									if (! is_null($jsonResult['sending_respon'][0]['datapacket'][$i]['packet']) && is_array($jsonResult['sending_respon'][0]['datapacket'][$i]['packet'])) {
										$arr = self::getSMSSendArrayModel();
										if (isset($jsonResult['sending_respon'][0]['datapacket'][$i]['packet']['number'])) {
											$arr[ self::KEY_SMS_NUMBER_RECEIVER ] = (string)$jsonResult['sending_respon'][0]['datapacket'][$i]['packet']['number']; 
										}
										if (isset($jsonResult['sending_respon'][0]['datapacket'][$i]['packet']['sendingid'])) {
											$arr[ self::KEY_SMS_SENDING_ID ] = (int)$jsonResult['sending_respon'][0]['datapacket'][$i]['packet']['sendingid'];
										}
										$success = false;
										if (isset($jsonResult['sending_respon'][0]['datapacket'][$i]['packet']['sendingstatus'])) {
											$success = ($jsonResult['sending_respon'][0]['datapacket'][$i]['packet']['sendingstatus'] == 10 ? true : false);
											$arr[ self::KEY_SMS_STATUS_CODE ] = (int)$jsonResult['sending_respon'][0]['datapacket'][$i]['packet']['sendingstatus'];
										}
										$arr[ self::KEY_SMS_IS_SUCCESS ] = $success;
										if (isset($jsonResult['sending_respon'][0]['datapacket'][$i]['packet']['sendingstatustext'])) {
											$arr[ self::KEY_SMS_STATUS_DESCRIPTION ] = (string)$jsonResult['sending_respon'][0]['datapacket'][$i]['packet']['sendingstatustext'];
										}
										if (isset($jsonResult['sending_respon'][0]['datapacket'][$i]['packet']['price'])) {
											$arr[ self::KEY_SMS_PRICE ] = (int)$jsonResult['sending_respon'][0]['datapacket'][$i]['packet']['price'];
										}
										$arrResult[] = $arr;
									}
								}
							} catch ( Exception $e ) {
								Log::info("SMSSenderAdsmedia::send exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
							}
						}
						return $arrResult;
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("SMSSenderAdsmedia::send exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return array();
	}
	
	private function payloadCheckBalance() {
		try {
			$data = array(
					'apikey' 		=> $this->apiKey
			);
			return json_encode($data);
		} catch ( Exception $e ) {
			Log::info("SMSSenderAdsmedia::payloadCheckBalance exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return "";
	}
	
	public function getBalance( &$balance, &$expired ) {
		try {
			$this->resultOriginal = array();
			$url = $this->urlEndPoint;
			if (substr($url, -1) == '/') {
				$url = substr($url, 0, strlen($url) - 1);
			}
			if ($this->typeSMS == self::SMS_TYPE_REGULER) {
				$url .= self::API_END_POINT_CHECK_BALANCE_REGULER;
			} else if ($this->typeSMS == self::SMS_TYPE_MASKING) {
				$url .= self::API_END_POINT_CHECK_BALANCE_MASKING;
			} else if ($this->typeSMS == self::SMS_TYPE_OTP) {
				$url .= self::API_END_POINT_CHECK_BALANCE_OTP;
			} else {
				$url .= self::API_END_POINT_CHECK_BALANCE_REGULER;
			}
			$dt = $this->payloadCheckBalance();
			if ($this->debug) Log::info("SMSSenderAdsmedia::getBalance URL_API=".$url." data_json=".$dt);
			$curlHandle = curl_init( $url );
			curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $dt);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($dt))
			);
			curl_setopt($curlHandle, CURLOPT_TIMEOUT, 5);
			curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, 5);
			$hasil = curl_exec($curlHandle);
			$curl_errno = curl_errno($curlHandle);
			$curl_error = curl_error($curlHandle);
			$http_code  = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
			curl_close($curlHandle);
			if ($this->debug) Log::info("SMSSenderAdsmedia::getBalance errno=".$curl_errno." error=".$curl_error." http_code=".$http_code." result=".$hasil);
			$jsonResult = array();
			if ($curl_errno > 0) {
				$jsonResult = array (
						'balance_respon'		=> array (
								0 => array (
									'globalstatus' 		=> 90,
									'globalstatustext' 	=> $curl_errno."|".$http_code
								)
						)
				);
			} else {
				if ($http_code<>"200") {
					$jsonResult = array(
							'balance_respon'=> array (
									0 => array (
										'globalstatus' 		=> 90,
										'globalstatustext' 	=> $curl_errno."|".$http_code
									)
							)
					);
				} else {
					$jsonResult = json_decode( $hasil, true, 512, JSON_BIGINT_AS_STRING );
				}
			}
			$this->resultOriginal = $jsonResult;
			if (isset($jsonResult['balance_respon'])) {
				if (! is_null($jsonResult['balance_respon']) && is_array($jsonResult['balance_respon']) && count($jsonResult['balance_respon']) > 0) {
					if (isset($jsonResult['balance_respon'][0]['globalstatus'])) {
						$this->errCode = $jsonResult['balance_respon'][0]['globalstatus'];
					}
					if (isset($jsonResult['balance_respon'][0]['globalstatustext'])) {
						$this->errMsg = $jsonResult['balance_respon'][0]['globalstatustext'];
					}
				}
			}
			if ($this->isSuccess()) {
				if (isset($jsonResult['balance_respon'][0]['Balance'])) {
					$balance = $jsonResult['balance_respon'][0]['Balance'];
				}
				if (isset($jsonResult['balance_respon'][0]['Expired'])) {
					$expired = $jsonResult['balance_respon'][0]['Expired'];
				}
				return true;
			}
		} catch ( Exception $e ) {
			Log::info("SMSSenderAdsmedia::getBalance exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public static function convertDeliveryStatus( $input ) {
		try {
			if (is_null($input) || ! is_array($input)) {
				return array();
			}
			if (isset($input['status_respon'])) {
				if (! is_null($input['status_respon']) && is_array($input['status_respon'])) {
					$arrStatus = array();
					for ($i = 0; $i < count($input['status_respon']); $i++) {
						try {
							$arr = self::getSMSDeliveryArrayModel();
							if (isset($input['status_respon'][$i]['number'])) {
								$arr[ self::KEY_SMS_NUMBER_RECEIVER ] = (string)$input['status_respon'][$i]['number'];
							}
							if (isset($input['status_respon'][$i]['sendingid'])) {
								$arr[ self::KEY_SMS_SENDING_ID] = (int)$input['status_respon'][$i]['sendingid'];
							}
							if (isset($input['status_respon'][$i]['deliverystatus'])) {
								$arr[ self::KEY_SMS_DELIVERY_STATUS_CODE] = (int)$input['status_respon'][$i]['deliverystatus'];
							}
							if (isset($input['status_respon'][$i]['deliverystatustext'])) {
								$arr[ self::KEY_SMS_DELIVERY_STATUS_DESCRIPTION] = (string)$input['status_respon'][$i]['deliverystatustext'];
							}
							$arrStatus[] = $arr;
						} catch ( Exception $e ) {
							Log::info("SMSSenderAdsmedia::convertDeliveryStatus exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
						}
					}
					return $arrStatus;
				}
			}
		} catch ( Exception $e ) {
			Log::info("SMSSenderAdsmedia::convertDeliveryStatus exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return array();
	}
	
}