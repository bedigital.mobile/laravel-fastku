<?php
namespace App\Core\SMS;

abstract class SMSSenderAbstract {
	
	const KEY_SMS_NUMBER_RECEIVER 	 = "number_receiver";
	// send_response
	const KEY_SMS_SENDING_ID 		 = "sending_id";
	const KEY_SMS_IS_SUCCESS 		 = "is_success";
	const KEY_SMS_STATUS_CODE 		 = "status_code";
	const KEY_SMS_STATUS_DESCRIPTION = "status_desc";
	const KEY_SMS_PRICE 			 = "price";
	// delivery_status
	const KEY_SMS_DELIVERY_STATUS_CODE 			= "delivery_status_code";
	const KEY_SMS_DELIVERY_STATUS_DESCRIPTION   = "delivery_status_desc";
	
	protected static function getSMSSendArrayModel() {
		return array(
				self::KEY_SMS_NUMBER_RECEIVER 	 => "",
				self::KEY_SMS_SENDING_ID 		 => 0,
				self::KEY_SMS_IS_SUCCESS  	 	 => false,
				self::KEY_SMS_STATUS_CODE 		 => 0,
				self::KEY_SMS_STATUS_DESCRIPTION => "",
				self::KEY_SMS_PRICE    			 => 0
		);
	} 
	
	protected static function getSMSDeliveryArrayModel() {
		return array(
				self::KEY_SMS_NUMBER_RECEIVER 	 			=> "",
				self::KEY_SMS_SENDING_ID 		 			=> 0,
				self::KEY_SMS_DELIVERY_STATUS_CODE 			=> 0,
				self::KEY_SMS_DELIVERY_STATUS_DESCRIPTION   => ""
		);
	}
	
	abstract protected function getErrorCode();
	abstract protected function getErrorMessage();
	abstract protected function getResultOriginal( $useArray = true );
	abstract protected function isSuccess();
	abstract protected function send( $to, $msg, $sendingAt = "" );
	abstract protected function getBalance( &$balance, &$expired );
	
}