<?php
namespace App\Core\SMS;


abstract class SMSAuthAbstract {
	// === edit by mas budi ====
	// abstract protected function setConfigAppID( $value );
	// abstract protected function setConfigSecretID( $value );
	// ==== 
	abstract protected function setConfig( $key, $value );
	abstract protected function getConfig();
	abstract protected function getLogString();
	abstract protected function verifyAccessToken( $accessToken, $phonenumber );
}