<?php
namespace App\Core\Push;

class PushKey {

	//Push platform
	const PLATFORM_ANDROID = 'ANDROID';
	const PLATFORM_IOS = 'IOS';
	const PLATFORM_OTHER = 'OTHER';

	//Push protocol
	const PROTOCOL_GOOGLE_FCM = 'FCM';
	const PROTOCOL_GOOGLE_GCM = 'GCM';
	const PROTOCOL_OTHER = 'OTHER';

	//Target type
	const TARGET_TYPE_TOKEN = 1;
	const TARGET_TYPE_TOPIC = 2;

	//Target device
	const TARGET_DEVICE_ANDROID = 1;
	const TARGET_DEVICE_APPLE = 2;
	const TARGET_DEVICE_ANDROID_AND_APPLE = 3;

	//Option fields
	const OPTION_PRIORITY = 'priority';
	const OPTION_TTL = 'time_to_live';
	const OPTION_PRIORITY_ANDROID = 'priority_android';
	const OPTION_PRIORITY_APPLE = 'priority_apple';
	const OPTION_DELAY_WHILE_IDLE = 'delay_while_idle';

	//notification fields
	const FIELD_NOTIF_TITLE = 'title';
	const FIELD_NOTIF_BODY = 'body';
	const FIELD_NOTIF_SOUND = 'sound';
	const FIELD_NOTIF_BADGE = 'badge';
	const FIELD_NOTIF_SUBTITLE = 'subtitle';
	const FIELD_NOTIF_ICON = 'subtitle';
	const FIELD_NOTIF_TAG = 'tag';
	const FIELD_NOTIF_COLOR = 'color';	

	//Priority of delivery for Android
	const PRIORITY_ANDROID_HIGH = 'high';
	const PRIORITY_ANDROID_NORMAL = 'normal';

	//Priority of delivery for APPLE
	const PRIORITY_APPLE_5 = '5';
	const PRIORITY_APPLE_6 = '6';
	const PRIORITY_APPLE_7 = '7';
	const PRIORITY_APPLE_8 = '8';
	const PRIORITY_APPLE_9 = '9';

	//Target scope 
	const SCOPE_WHITELABEL = 1;
	const SCOPE_MASTER_AGENT = 2;
	const SCOPE_AGENT = 3;
	const SCOPE_DIRECT_TOKEN = 4;
	
	//Content type
	const CONTENT_TEXT = "TEXT";
	const CONTENT_URL = "URL";
	const CONTENT_IMAGE = "IMAGE";
	const CONTENT_CLICKABLE_IMAGE = "CLICKABLE_IMAGE";
	const CONTENT_REFRESH_BALANCE = "REFRESH_BALANCE";
	const CONTENT_REFRESH_SERVICE = "REFRESH_SERVICE";
	const CONTENT_REFRESH_MENU = "REFRESH_MENU";

	//Field data
	const DATA_TEXT = 'text';
	const DATA_SAVE_TO_INBOX = 'save_to_inbox';

	//User Agent
	const USER_AGENT_API_CLIENT = "PHP-curl/1.0";	

	//Proxy
	const PROXY_USE_FLAG = 'is_use_proxy';
	const PROXY_ADDRESS = 'proxy_address';
	const PROXY_PORT = 'proxy_port';
	const PROXY_USER = 'proxy_user';
	const PROXY_PASSWORD = 'proxy_password';

	//Max Token per Push
	const MAX_TOKEN_PER_PUSH = 1000;

	public function __construct() {
	
	} 

	public static function setProxy($address, $port, $user, $password) {
		$proxySetting = [];
		if (!empty($address) && !empty($port) && !empty($user) && !empty($pwd)) {
			$proxySetting[self::PROXY_USE_FLAG] = true;
			$proxySetting[self::PROXY_ADDRESS] = $address;
			$proxySetting[self::PROXY_PORT] = (int)$port;
			$proxySetting[self::PROXY_USER] = $user;
			$proxySetting[self::PROXY_PASSWORD] = $password;
		} else {
			$proxySetting[self::PROXY_USE_FLAG] = false;
			$proxySetting[self::PROXY_ADDRESS] = '';
			$proxySetting[self::PROXY_PORT] = 0;
			$proxySetting[self::PROXY_USER] = '';
			$proxySetting[self::PROXY_PASSWORD] = '';
		}
		return $proxySetting;
	} 

}