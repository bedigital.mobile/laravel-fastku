<?php
namespace App\Core\Push;

use Log;
use Exception;
use App\SystemLogic\Push\PushSL;
use App\Core\Push\PushGoogleFCM;

class PushManager {
	
	private $protocol = '';
	private $platform = '';
	private $pushModul = null;

	//Push platform
	const PLATFORM_ANDROID = 'ANDROID';
	const PLATFORM_OTHER = 'OTHER';
	//Push protocol
	const PROTOCOL_GOOGLE_FCM = 'FCM';
	const PROTOCOL_GOOGLE_GCM = 'GCM';
	const PROTOCOL_OTHER = 'OTHER';

	public function __construct( $platform = self::PLATFORM_ANDROID, $protocol = self::PROTOCOL_GOOGLE_FCM ) {
		Log::info('PushManager->__construct: constructing....');
		$this->protocol = $protocol;
		$this->platform = $platform;
		$this->selectPushModul();
	}

	public function getPushModule() {
		return $this->pushModul;
	}

	private function selectPushModul() {
		Log::info('PushManager->selectPushModul: select push modul and get config');
		$pushSL = new PushSL($this->platform, $this->protocol);
		switch ($this->platform) {
			case self::PLATFORM_ANDROID:
				 switch ($this->protocol) {
				 	case self::PROTOCOL_GOOGLE_FCM:
				 		Log::info('PushManager->selectPushModul: use GOOGLE Platform with FCM protocol');
				 		$this->pushModul = new PushGoogleFCM($pushSL->getConfig());
				 		break;
				 	case self::PROTOCOL_GOOGLE_GCM:
				 		Log::info('PushManager->selectPushModul: use GOOGLE Platform with GCM protocol');
				 		break; 	
				 }
				break;			
			case self::PLATFORM_OTHER:
				Log::info('PushManager->selectPushModul: use OTHER Platform');
				break;
		}
	}

} // End of class