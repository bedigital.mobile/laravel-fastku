<?php
namespace App\Core\Push;

use Log;
use Exception;  
use App\Models\Api\ApiError;
use App\Core\Push\PushKey;

class PushGoogleFCM {
	private $serverKey = '';
	private $targetType = 0; 
	private $target = null;
	private $targetDevice = 0; 
	private $targetPrefix = '';
	private $targetFieldName = ''; 
	private $notification = array();
	private $data = null;
	private $fields = array();
	private $proxySetting = array();

	public function __construct( $config ) {
		if (isset($config)) {
			if (isset($config['APIKEY'])) {
				$this->serverKey = $config['APIKEY'];
				$this->proxySetting = PushKey::setProxy('', '', '', '');	
			}
		}
	}

	public function sendPush($options, $notification, $data, $targetType, $target, $device) {
   	   $errorMsg = "";
	   $curlErrno = 0;	 
	   $fields = array();
	   $output = null;
	   $apiResult = null;
	   $httpStatus = 0;
	   $ch = null;
	   try {
	   	   //Check API Key
	   	   if ( empty($this->serverKey) )  {
				$output = $this->buildOutput($apiResult, "Error in push config", $httpStatus, $curlErrno);		
		   } else {
			   //create header with content_type api key
			   $headers = array(
					'Content-Type:application/json',
			        'Authorization:key='.$this->serverKey
			   );
		   	   $errorMsg = $this->setTarget($targetType, $target, $device);
		   	   if (empty($errorMsg)) {
		   	   	  $errorMsg = $this->setOptions($options);
		   	   	  if (empty($errorMsg)) {
		   	   	  	 $errorMsg = $this->setPayloads($data, $notification);
		   	   	  	 if (empty($errorMsg)) {
		   	   	  	 	//Log::info('PushGoogleFCM->sendPush: content='.json_encode($this->fields));
					    $ch = curl_init();
					   	curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
					  	curl_setopt($ch, CURLOPT_POST, true);
					   	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					   	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					   	curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
					   	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					   	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					   	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->fields));
					   	curl_setopt($ch, CURLOPT_USERAGENT, PushKey::USER_AGENT_API_CLIENT);

						if ($this->proxySetting[PushKey::PROXY_USE_FLAG]) {
							curl_setopt($ch, CURLOPT_PROXY, (
								$this->proxySetting[PushKey::PROXY_ADDRESS].":".$this->proxySetting[PushKey::PROXY_PORT]));
							curl_setopt($ch, CURLOPT_PROXYPORT, $this->proxySetting[PushKey::PROXY_PORT]);
							curl_setopt($ch, CURLOPT_PROXYUSERPWD, (
								$this->proxySetting[PushKey::PROXY_USER].":".$this->proxySetting[PushKey::PROXY_PASSWORD]));
							curl_setopt($ch, CURLOPT_PROXYAUTH, CURLAUTH_NTLM);
							curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
						}

					   	$apiResult = curl_exec($ch);
						$httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						$curlErrno = curl_errno($ch);
						if ( curl_error($ch) ) {
						    $errorMsg = curl_error($ch);
						}
						if(empty($apiResult)){
							Log::error('PushGoogleFCM->sendPush : rs='.json_encode($apiResult));
							Log::error('PushGoogleFCM->sendPush : ch='.json_encode(curl_error($ch)));
							curl_close($ch);
							$output = $this->buildOutput($apiResult, $this->checkValue($errorMsg, 0, ""), 
								$httpStatus, $curlErrno);
						} else {
							curl_close($ch);
							$output = $this->buildOutput($apiResult, $this->checkValue($errorMsg,0,""), 
								$httpStatus, $curlErrno);
						}
		   	   	  	 } else {
		   	   	  	 		$output = $this->buildOutput($apiResult, $errorMsg, $httpStatus, $curlErrno);	
		   	   	  	 }
		   	   	  } else {
		   	   	  		$output = $this->buildOutput($apiResult, $errorMsg, $httpStatus, $curlErrno);	
		   	   	  }
		   	   } else {
		   	   	 	$output = $this->buildOutput($apiResult, $errorMsg, $httpStatus, $curlErrno); 	
		   	   }	
		   }
	   } catch (Exception $e) {
			Log::error('PushGoogleFCM->sendPush: exception at [Line='.$e->getLine().',Code='.$e->getCode().'],'
				.$e->getMessage());
			$output = $this->buildOutput($apiResult, "Internal server error", $httpStatus, $curlErrno);
	   }
	   Log::info('PushGoogleFCM->sendPush: notification='.json_encode($this->notification));
	   Log::info('PushGoogleFCM->sendPush: option='.json_encode($options));
	   Log::info('PushGoogleFCM->sendPush: data='.json_encode($this->data));
	   Log::info('PushGoogleFCM->sendPush: done.');
	   //Log::info('PushGoogleFCM->sendPush: '.json_encode($output));
	   return $output;
	} // End of sendPush function

	public function subscribeTopic($topicName, $token) {
		Log::info('PushGoogleFCM->subscribeTopic: subscribe to '.$topicName.' topic');
		$apiURL = 'https://iid.googleapis.com/iid/v1/'.$token.'/rel/topics/'.$topicName;
		Log::info('PushGoogleFCM->subscribeTopic: url='.$apiURL);
		$ch = null;
		$apiResult = null;
		$httpStatus = 0;
		$curlErrno = 0;
		$errorMessage = '';
		$output = null;
		try {
		   //create header with content_type api key
		   $headers = array(
				'Content-Type:application/json',
				'Content-Length: 0',
		        'Authorization:key='.$this->serverKey
		   );
		   //CURL request to route notification to FCM connection server (provided by Google)			
		    $ch = curl_init();
		   	curl_setopt($ch, CURLOPT_URL, $apiURL);
		  	curl_setopt($ch, CURLOPT_POST, true);
		   	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		   	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		   	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		   	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			if ($this->proxySetting[PushKey::PROXY_USE_FLAG]) {
				curl_setopt($ch, CURLOPT_PROXY, (
					$this->proxySetting[PushKey::PROXY_ADDRESS].":".$this->proxySetting[PushKey::PROXY_PORT]));
				curl_setopt($ch, CURLOPT_PROXYPORT, $this->proxySetting[PushKey::PROXY_PORT]);
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, (
					$this->proxySetting[PushKey::PROXY_USER].":".$this->proxySetting[PushKey::PROXY_PASSWORD]));
				curl_setopt($ch, CURLOPT_PROXYAUTH, CURLAUTH_NTLM);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			}

		   	$apiResult = curl_exec($ch);
			$httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$curlErrno = curl_errno($ch);

			if ( curl_error($ch) ) {
			    $errorMessage = curl_error($ch);
			}
			if(empty($apiResult)){
				Log::info('PushGoogleFCM->subscribeTopic: create topic is error, no API result');
				Log::error('PushGoogleFCM->subscribeTopic : rs='.json_encode($apiResult));
				Log::error('PushGoogleFCM->subscribeTopic : ch='.json_encode(curl_error($ch)));
				curl_close($ch);
				$output = $this->buildOutput($apiResult, $this->checkValue($errorMessage, 0, ""), $httpStatus, $curlErrno);
			} else {
				Log::info('PushGoogleFCM->subscribeTopic: API result is not empty');
				Log::info('PushGoogleFCM->subscribeTopic: result='.json_encode($apiResult));
				curl_close($ch);
				$output = $this->buildOutput($apiResult, $this->checkValue($errorMessage,0,""), $httpStatus, $curlErrno);
			} 
		} catch (Exception $e) {
			Log::error('PushGoogleFCM->subscribeTopic: exception at [Line='.$e->getLine().',Code='.$e->getCode().'],'
				.$e->getMessage());
			$output = $this->buildOutput($apiResult, "Internal server error", $httpStatus, $curlErrno);			
		}
		return $output;
	}

	public function subscribeTopicBatch($topicName, $tokens=[]) {
		$headers = null;
		$apiResult = null;
		$httpStatus = 0;
		$curlErrno = 0;
		$output = null;
		$ch = null;
		$errorMsg = '';
		$fields = [];
		try {
			Log::info('PushGoogleFCM->subscribeTopicBatch : start subscribing token to '.$topicName.' topic');
			if (!$this->isSetAndNotEmpty($tokens)) {
			   $errorMsg = 'token is empty or not set';
			   $output = $this->buildOutput($apiResult, $errorMsg, $httpStatus, $curlErrno);
			} else {
				if (!$this->isSetAndNotEmpty($topicName)) {
				   $errorMsg = 'topic is not define';
				   $output = $this->buildOutput($apiResult, $errorMsg, $httpStatus, $curlErrno);
				} else {
					$fields['to'] = "/topics/".$topicName;
					$fields['registration_tokens'] = $tokens;

					$headers = array(
						'Content-Type:application/json',
				        'Authorization:key='.$this->serverKey
				    );

				    $ch = curl_init();
				   	curl_setopt($ch, CURLOPT_URL, 'https://iid.googleapis.com/iid/v1:batchAdd');
				  	curl_setopt($ch, CURLOPT_POST, true);
				   	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				   	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				   	curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
				   	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				   	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				   	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
				   	curl_setopt($ch, CURLOPT_USERAGENT, PushKey::USER_AGENT_API_CLIENT);

					if ($this->proxySetting[PushKey::PROXY_USE_FLAG]) {
						curl_setopt($ch, CURLOPT_PROXY, (
							$this->proxySetting[PushKey::PROXY_ADDRESS].":".$this->proxySetting[PushKey::PROXY_PORT]));
						curl_setopt($ch, CURLOPT_PROXYPORT, $this->proxySetting[PushKey::PROXY_PORT]);
						curl_setopt($ch, CURLOPT_PROXYUSERPWD, (
							$this->proxySetting[PushKey::PROXY_USER].":".$this->proxySetting[PushKey::PROXY_PASSWORD]));
						curl_setopt($ch, CURLOPT_PROXYAUTH, CURLAUTH_NTLM);
						curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
					}

				   	$apiResult = curl_exec($ch);
					$httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					$curlErrno = curl_errno($ch);
					if ( curl_error($ch) ) {
					    $errorMsg = curl_error($ch);
					}
					if(empty($apiResult)){
						Log::error('PushGoogleFCM->subscribeTopicBatch : apiResult='.json_encode($apiResult));
						Log::error('PushGoogleFCM->subscribeTopicBatch : ch='.json_encode(curl_error($ch)));
						curl_close($ch);
						$output = $this->buildOutput($apiResult, $this->checkValue($errorMsg, 0, ""), 
							$httpStatus, $curlErrno);
					} else {
						curl_close($ch);
						$output = $this->buildOutput($apiResult, $this->checkValue($errorMsg,0,""), 
							$httpStatus, $curlErrno);
						Log::error('PushGoogleFCM->subscribeTopicBatch : apiResult='.json_encode($apiResult));
					}
				}
			}
		} catch (Exception $e) {
			Log::error('PushGoogleFCM->subscribeTopicBatch: exception at [Line='.$e->getLine().',Code='.$e->getCode().'],'
				.$e->getMessage());
			$output = $this->buildOutput($apiResult, "Internal server error", $httpStatus, $curlErrno);			
		}
		return $output;
	}

	public function unsubscribeTopicBatch($topicName, $tokens=[]) {
		$headers = null;
		$apiResult = null;
		$httpStatus = 0;
		$curlErrno = 0;
		$output = null;
		$ch = null;
		$errorMsg = '';
		$fields = [];
		try {
			Log::info('PushGoogleFCM->unsubscribeTopicBatch : start unsubscribing token from '.$topicName. ' topic');
			if (!$this->isSetAndNotEmpty($tokens)) {
			   $errorMsg = 'token is empty or not set';
			   $output = $this->buildOutput($apiResult, $errorMsg, $httpStatus, $curlErrno);
			} else {
				if (!$this->isSetAndNotEmpty($topicName)) {
				   $errorMsg = 'topic is not define';
				   $output = $this->buildOutput($apiResult, $errorMsg, $httpStatus, $curlErrno);
				} else {
					$fields['to'] = "/topics/".$topicName;
					$fields['registration_tokens'] = $tokens;

					$headers = array(
						'Content-Type:application/json',
				        'Authorization:key='.$this->serverKey
				    );

				    $ch = curl_init();
				   	curl_setopt($ch, CURLOPT_URL, 'https://iid.googleapis.com/iid/v1:batchRemove');
				  	curl_setopt($ch, CURLOPT_POST, true);
				   	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				   	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				   	curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
				   	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				   	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				   	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
				   	curl_setopt($ch, CURLOPT_USERAGENT, PushKey::USER_AGENT_API_CLIENT);

					if ($this->proxySetting[PushKey::PROXY_USE_FLAG]) {
						curl_setopt($ch, CURLOPT_PROXY, (
							$this->proxySetting[PushKey::PROXY_ADDRESS].":".$this->proxySetting[PushKey::PROXY_PORT]));
						curl_setopt($ch, CURLOPT_PROXYPORT, $this->proxySetting[PushKey::PROXY_PORT]);
						curl_setopt($ch, CURLOPT_PROXYUSERPWD, (
							$this->proxySetting[PushKey::PROXY_USER].":".$this->proxySetting[PushKey::PROXY_PASSWORD]));
						curl_setopt($ch, CURLOPT_PROXYAUTH, CURLAUTH_NTLM);
						curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
					}

				   	$apiResult = curl_exec($ch);
					$httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					$curlErrno = curl_errno($ch);
					if ( curl_error($ch) ) {
					    $errorMsg = curl_error($ch);
					}
					if(empty($apiResult)){
						Log::error('PushGoogleFCM->unsubscribeTopicBatch : apiResult='.json_encode($apiResult));
						Log::error('PushGoogleFCM->unsubscribeTopicBatch : ch='.json_encode(curl_error($ch)));
						curl_close($ch);
						$output = $this->buildOutput($apiResult, $this->checkValue($errorMsg, 0, ""), 
							$httpStatus, $curlErrno);
					} else {
						curl_close($ch);
						$output = $this->buildOutput($apiResult, $this->checkValue($errorMsg,0,""), 
							$httpStatus, $curlErrno);
						Log::error('PushGoogleFCM->unsubscribeTopicBatch : apiResult='.json_encode($apiResult));
					}
				}
			}
		} catch (Exception $e) {
			Log::error('PushGoogleFCM->unsubscribeTopicBatch: exception at [Line='.$e->getLine().',Code='.$e->getCode().'],'
				.$e->getMessage());
			$output = $this->buildOutput($apiResult, "Internal server error", $httpStatus, $curlErrno);			
		}
		return $output;
	}

	public function setProxy($address, $port, $user, $password) {
		$this->proxySetting = PushKey::setProxy($address, $port, $user, $password);
	}
 
	private function setTarget($targetType, $target, $device) {
		$errorMsg = '';
		$this->targetType = $targetType;
		$this->target = $target;
		$this->targetDevice = $device;
		switch ($targetType) {
			case PushKey::TARGET_TYPE_TOKEN:
				$this->targetPrefix = '';
				$errorMsg = $this->setTargetFieldName($this->target);
				break;
			case PushKey::TARGET_TYPE_TOPIC:
				$this->targetPrefix = '/topics/';
				$errorMsg = $this->setTargetFieldName($this->target);
				break;
			default:
				$errorMsg = 'Target is not defined. Please choose between TOKEN or TOPIC';
				break;
		}
		if (empty($errorMsg)) {
			if (!empty($this->targetPrefix)) {
				$this->fields[$this->targetFieldName] = $this->targetPrefix.$target;	
			} else {
				$this->fields[$this->targetFieldName] = $target;
			}			
		}
		return $errorMsg;
	}

	private function setOptions($options) {
		$errorMsg = '';
		$isPrioritySet = false;
		$isTTLSet = false;
		if (isset($options)) {
			if ($this->targetDevice == PushKey::TARGET_DEVICE_ANDROID) {
				//Android
				switch ($options[PushKey::OPTION_PRIORITY_ANDROID]) {
					case PushKey::PRIORITY_ANDROID_HIGH:
					case PushKey::PRIORITY_ANDROID_NORMAL:
						$this->fields[PushKey::OPTION_PRIORITY] = $options[PushKey::OPTION_PRIORITY_ANDROID];
						$isPrioritySet = true;
						Log::info('PushGoogleFCM->setOptions: priority for ANDROID is set to '.$options[PushKey::OPTION_PRIORITY_ANDROID]);
						break;		
				}
			} else {
				//Apple
				switch ($options[PushKey::OPTION_PRIORITY_APPLE]) {
					case PushKey::PRIORITY_APPLE_5:
					case PushKey::PRIORITY_APPLE_6:
					case PushKey::PRIORITY_APPLE_7:
					case PushKey::PRIORITY_APPLE_8:
					case PushKey::PRIORITY_APPLE_9:
					case PushKey::PRIORITY_APPLE_10:
						$this->fields[PushKey::OPTION_PRIORITY] = $options[PushKey::OPTION_PRIORITY_APPLE];
						$isPrioritySet = true;
						Log::info('PushGoogleFCM->setOptions: priority for APPLE is set to '.$options[PushKey::OPTION_PRIORITY_APPLE]);
						break;		
				}
			}
			if (!empty($options[PushKey::OPTION_TTL])) {
				if ((int)$options[PushKey::OPTION_TTL] >= 0 ) {
					$this->fields[PushKey::OPTION_TTL] = $options[PushKey::OPTION_TTL];
					$isTTLSet = true;
					Log::info('PushGoogleFCM->setOptions: TTL is set to '.$options[PushKey::OPTION_TTL]);
				}
			}
			if (!empty($options[PushKey::OPTION_DELAY_WHILE_IDLE])) {
				$this->fields[PushKey::OPTION_DELAY_WHILE_IDLE] = $options[PushKey::OPTION_DELAY_WHILE_IDLE];
			}			
		} else {
			Log::info('PushGoogleFCM->setOptions: option input is undefined.');
		}
		if ( (!$isTTLSet)||(!$isPrioritySet) ) {
			if (!$isTTLSet) {
				Log::info('PushGoogleFCM->setOptions: TTL is not set.');
			}
			if (!$isPrioritySet) {
				Log::info('PushGoogleFCM->setOptions: Priority is not set.');
			}
			$errorMsg = 'Invalid options';
		}
		return $errorMsg;
	}

	private function setPayloads($data,$notification) {
		$errorMsg = '';
		$isNotificationSet = false;
		$isDataSet = false;
		//Set Notification
		if (isset($notification)) {
			switch ($this->targetDevice) {
				case PushKey::TARGET_DEVICE_ANDROID:
					if ($this->isSetAndNotEmpty($notification[PushKey::FIELD_NOTIF_TITLE])) {
						/*
						$this->notification[PushKey::FIELD_NOTIF_TITLE] = 
							$notification[PushKey::FIELD_NOTIF_TITLE];
						*/	
						$data[PushKey::FIELD_NOTIF_TITLE] = 
							$notification[PushKey::FIELD_NOTIF_TITLE];

						$this->notification[PushKey::FIELD_NOTIF_TITLE] = '';
					}
					if ($this->isSetAndNotEmpty($notification[PushKey::FIELD_NOTIF_BODY])) {
						/*
						$this->notification[PushKey::FIELD_NOTIF_BODY] = 
							$notification[PushKey::FIELD_NOTIF_BODY];
						*/
						$data[PushKey::FIELD_NOTIF_BODY] = 
							$notification[PushKey::FIELD_NOTIF_BODY];

						$this->notification[PushKey::FIELD_NOTIF_BODY] = '';	
					}
					if ($this->isSetAndNotEmpty($notification[PushKey::FIELD_NOTIF_SOUND])) {
						/*
						$this->notification[PushKey::FIELD_NOTIF_SOUND] = 
							$notification[PushKey::FIELD_NOTIF_SOUND];
						*/
						$data[PushKey::FIELD_NOTIF_SOUND] = 
							$notification[PushKey::FIELD_NOTIF_SOUND];
					}
					if ($this->isSetAndNotEmpty($notification[PushKey::FIELD_NOTIF_ICON])) {
						/*
						$this->notification[PushKey::FIELD_NOTIF_ICON] = 
							$notification[PushKey::FIELD_NOTIF_ICON];
						*/
						$data[PushKey::FIELD_NOTIF_ICON] = 
							$notification[PushKey::FIELD_NOTIF_ICON];
					}
					if ($this->isSetAndNotEmpty($notification[PushKey::FIELD_NOTIF_TAG])) {
						/*
						$this->notification[PushKey::FIELD_NOTIF_TAG] = 
							$notification[PushKey::FIELD_NOTIF_TAG];
						*/
						$data[PushKey::FIELD_NOTIF_TAG] = 
							$notification[PushKey::FIELD_NOTIF_TAG];
					}
					if ($this->isSetAndNotEmpty($notification[PushKey::FIELD_NOTIF_COLOR])) {
						/*
						$this->notification[PushKey::FIELD_NOTIF_COLOR] = 
							$notification[PushKey::FIELD_NOTIF_COLOR];
						*/
						$data[PushKey::FIELD_NOTIF_COLOR] = 
							$notification[PushKey::FIELD_NOTIF_COLOR];
					}
					break;
				case PushKey::TARGET_DEVICE_APPLE:
					if ($this->isSetAndNotEmpty($notification[PushKey::FIELD_NOTIF_TITLE])) {
						/*
						$this->notification[PushKey::FIELD_NOTIF_TITLE] = 
							$notification[PushKey::FIELD_NOTIF_TITLE];
						*/	
						$data[PushKey::FIELD_NOTIF_TITLE] = 
							$notification[PushKey::FIELD_NOTIF_TITLE];
					}
					if ($this->isSetAndNotEmpty($notification[PushKey::FIELD_NOTIF_BODY])) {
						/*
						$this->notification[PushKey::FIELD_NOTIF_BODY] = 
						$notification[PushKey::FIELD_NOTIF_BODY];
						*/
						$data[PushKey::FIELD_NOTIF_BODY] = 
						$notification[PushKey::FIELD_NOTIF_BODY];
					}
					if ($this->isSetAndNotEmpty($notification[PushKey::FIELD_NOTIF_SOUND])) {
						/*
						$this->notification[PushKey::FIELD_NOTIF_SOUND] = 
							$notification[PushKey::FIELD_NOTIF_SOUND];
						*/	
						$data[PushKey::FIELD_NOTIF_SOUND] = 
							$notification[PushKey::FIELD_NOTIF_SOUND];
					}
					if ($this->isSetAndNotEmpty($notification[PushKey::FIELD_NOTIF_BADGE])) {
						/*
						$this->notification[PushKey::FIELD_NOTIF_BADGE] = 
							$notification[PushKey::FIELD_NOTIF_BADGE];
						*/
						$data[PushKey::FIELD_NOTIF_BADGE] = 
							$notification[PushKey::FIELD_NOTIF_BADGE];
					}
					if ($this->isSetAndNotEmpty($notification[PushKey::FIELD_NOTIF_SUBTITLE])) {
						/*
						$this->notification[PushKey::FIELD_NOTIF_SUBTITLE] = 
							$notification[PushKey::FIELD_NOTIF_SUBTITLE];
						*/
						$data[PushKey::FIELD_NOTIF_SUBTITLE] = 
							$notification[PushKey::FIELD_NOTIF_SUBTITLE];
					}
					break;
			}	
		}

		if (!empty($this->notification)) {
			$this->fields['notification'] = $this->notification;
			$isNotificationSet = true;
		}

		//Set custom data
		if (isset($data)) {
			if (is_array($data)) {
				if (!empty($data)) {
					$this->data = $data;
					$this->fields['data'] =  $this->data;	
					$isDataSet = true;
				}
			}
		}

		Log::info('PushGoogleFCM->setPayloads : "'.json_encode($this->data));

		if (!$isDataSet) {
			$errorMsg = 'Data is not set';
		}
		if (!$isNotificationSet) {
			if (empty($errorMsg)) {
				$errorMsg = 'Notification is not set';
			} else {
				$errorMsg = $errorMsg.' and notification is not set';
			}
		}
		return $errorMsg;
	}

	private function buildOutput($apiResult, $errorMessage, $httpStatus, $curlErrno) {
		return array (
			"result" => $apiResult,
			"error_message" => $errorMessage,
			"http_status" => $httpStatus,
			"curl_erno" => $curlErrno 
		);					
	} 

	private function checkValue($value,$condition, $newValue) {
		$selectedValue = $value;
		if ($value == $condition) {
			$selectedValue = $newValue;
		}
		return $selectedValue;
	}

	private function isSetAndNotEmpty($x) {
		$result = false;
		if (isset($x)) {
			if (!empty($x)) {
				$result = true;
			}
		}
		return $result;
	}

	private function setTargetFieldName($target) {
	   $errorMsg = '';	
	   if (isset($target)) {
		   if (is_array($target)) {
		   		if (count($target) > 1) {
		   			$this->targetFieldName = 'registration_ids';
		   		} else {
		   			if (count($target) == 1) {
		   				$this->targetFieldName = 'to';
		   			} else {
		   				$errorMsg = 'Target is undefined or empty';	
		   			}
		   		}
		   } else {
		   		if (!empty($target)) {
		   			$this->targetFieldName = 'to';
		   		} else {
		   			$errorMsg = 'Target is undefined or empty';
		   		}
		   }
	   } else {
	   		$errorMsg = 'Target is undefined or empty';
	   }
	   Log::info('PushGoogleFCM->setTargetFieldName : "'.$this->targetFieldName.'" error:'.$errorMsg);
	   return $errorMsg;    
	}	

 } //End of class     
    