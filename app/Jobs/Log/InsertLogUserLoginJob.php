<?php
namespace App\Jobs\Log;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Config;
use Exception;
use App\BusinessLogic\Log\LogUserLoginBL;

class InsertLogUserLoginJob implements ShouldQueue {
	
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	
	private $input;
	protected $debug;
	
	public function __construct( $input ) {
		try {
			$this->input = array();
			if (! is_null($input) && is_array($input)) {
				$this->input = $input;
			} 
			$this->debug = boolval( Config::get( 'app.debug' ) );
		} catch ( Exception $e ) {
			Log::info( "InsertLogUserLoginJob::__construct exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
	public function handle() {
		$jobID = 0;
		try {
			$jobID = $this->job->getJobId();
			if ($this->debug) Log::info( "InsertLogUserLoginJob::handle #".$jobID." queue=".$this->queue." START input=".json_encode($this->input));
		} catch ( Exception $e ) {
			Log::info( "InsertLogUserLoginJob::handle exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		try {
			$logBL = new LogUserLoginBL( $this->input );
			$b = $logBL->insert();
			if ($this->debug) Log::info( "InsertLogUserLoginJob::handle #".$jobID." queue=".$this->queue." b=".($b?"true":"false"));
		} catch ( Exception $e ) {
			Log::info( "InsertLogUserLoginJob::handle #".$jobID." queue=".$this->queue." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			Log::error ( $e );
		}
		try {
			$this->job->delete();
		} catch ( Exception $e ) {
			Log::info( "InsertLogUserLoginJob::handle #".$jobID." queue=".$this->queue." delete_job exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		if ($this->debug) Log::info( "InsertLogUserLoginJob::handle #".$jobID." queue=".$this->queue." END");
	}
	
}
