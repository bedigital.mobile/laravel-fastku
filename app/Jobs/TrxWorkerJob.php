<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Http\Helpers\TrxRequestHelper;
use App\Http\Helpers\HttpClientHelper;

class TrxWorkerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    protected $workerParams;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($workerParams)
    {
        $this->workerParams = $workerParams;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        try {
            Log::info('====start job: '.$this->job->getJobId().'====');        
        
            $request = new Request();
            $request->merge([
                'ip' => $this->workerParams['ip'],
                'api_key' => $this->workerParams['api_key'],
                'token' => $this->workerParams['token'],
                'type' => $this->workerParams['type'],
                'product' => $this->workerParams['product'],
                'billNumber' => $this->workerParams['billNumber'],
                'sign' => $this->workerParams['sign'],
                'callback_url' => $this->workerParams['callback_url']
            ]);        
            $response = (new TrxRequestHelper($request))->run();

            Log::info('====end job: '.$this->job->getJobId().' with response: '.json_encode($response).'====');
            
        } catch (Exception $ex) {
            Log::info('////===== Failed to run Job '.$this->job->getJobId().': '.$ex->getMessage().'=====////');
        }
        
        try {
            
            Log::info('====delete job: '.$this->job->getJobId().'====');
            
            $this->job->delete();
            
        } catch (Exception $ex) {
            Log::info('////===== Failed to delete Job '.$this->job->getJobId().': '.$ex->getMessage().'=====////');
        }        
    }
}
