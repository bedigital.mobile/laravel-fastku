<?php
namespace App\Jobs\Push;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Config;
use Exception;
use App\BusinessLogic\Agent\AgentPushTokenBL;

class AgentInsertTokenJob implements ShouldQueue {

	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $debug = false;
	private $agentID = 0;
	private $input = array();
	
	public function __construct( $agentID, $input ) {
		try {
			$this->agentID = $agentID;
			$this->input = $input;
			if (is_null($this->input) || ! is_array($this->input)) {
				$this->input = array();
			}
			$this->debug = boolval(Config::get('app.debug'));
		} catch ( Exception $e ) {
			Log::info( "AgentInsertTokenJob::__construct exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}

	public function handle() {
		$jobID = 0;
		try {
			$jobID = $this->job->getJobId();
			if ($this->debug) Log::info( "AgentInsertTokenJob::handle #".$jobID." queue=".$this->queue." START agent_id=".$this->agentID." input=".json_encode($this->input));
		} catch ( Exception $e ) {
			Log::info( "AgentInsertTokenJob::handle exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		$b = false;
		try {
			$pushBL = new AgentPushTokenBL();
			$pushBL->setAgent( $this->agentID );
			$b = $pushBL->insertToken( $this->input );
			if ($this->debug) Log::info( "AgentInsertTokenJob::handle agent_id=".$this->agentID." result=".($b?"true":"false"));
		} catch ( Exception $e ) {
			Log::info( "AgentInsertTokenJob::handle #".$jobID." queue=".$this->queue." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			Log::error ( $e );
		}
		try {
			$this->job->delete();
		} catch ( Exception $e ) {
			Log::info( "AgentInsertTokenJob::handle #".$jobID." queue=".$this->queue." delete_job exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		if ($this->debug) Log::info( "AgentInsertTokenJob::handle #".$jobID." queue=".$this->queue." END");
	}

}