<?php
namespace App\Jobs\Push;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Config;
use Exception;
use App\BusinessLogic\Agent\AgentBL;
use App\BusinessLogic\Push\PushBL;
use App\Core\Push\PushKey;

class SendPushTextToAgentJob implements ShouldQueue {

	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $debug = false;
	protected $apiversion = 1;
	private $arrAgentID = array();
	private $whitelabelID = 0;
	private $text = "";
	private $title = "";
	
	public function __construct( $text, $title, $agentID, $whitelabelID, $apiversion = 1 ) {
		try {
			$this->text = $text;
			$this->title = $title;
			if (is_array($agentID)) {
				$this->arrAgentID = $agentID;
			} else {
				$this->arrAgentID[0] = $agentID;
			}
			$this->whitelabelID = $whitelabelID;
			$this->apiversion = (int)$apiversion;
			$this->debug = boolval(Config::get('app.debug'));
		} catch ( Exception $e ) {
			Log::info( "SendPushTextToAgentJob::__construct exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}

	public function handle() {
		$jobID = 0;
		try {
			$jobID = $this->job->getJobId();
			if ($this->debug) Log::info( "SendPushTextToAgentJob::handle #".$jobID." queue=".$this->queue." START agent_id=".json_encode($this->arrAgentID));
		} catch ( Exception $e ) {
			Log::info( "SendPushTextToAgentJob::handle exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		try {
			if (empty($this->whitelabelID)) {
				$agentBL = new AgentBL( 0, 0, $this->apiversion );
				$agentBL->setAgent( $this->arrAgentID[0] );
				$mAgent = $agentBL->getAgentModel();
				if (! is_null($mAgent)) {
					$this->whitelabelID = $mAgent->white_label_id;
				}
			}
			$body = $this->text;
			$pushBL = new PushBL( $this->whitelabelID );
			$pushBL->composeTextPush($this->text, $this->title, $body);
			$pushBL->useToken(PushKey::SCOPE_AGENT);
			$pushBL->setAgent($this->arrAgentID);
			$pushBL->useQueue( false );
			$res = $pushBL->sendPush();
			Log::info( "SendPushTextToAgentJob::handle agent_id=".json_encode($this->arrAgentID)." text=".$this->text." ==> result=".json_encode($res));
		} catch ( Exception $e ) {
			Log::info( "SendPushTextToAgentJob::handle #".$jobID." queue=".$this->queue." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			Log::error ( $e );
		}
		try {
			$this->job->delete();
		} catch ( Exception $e ) {
			Log::info( "SendPushTextToAgentJob::handle #".$jobID." queue=".$this->queue." delete_job exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		if ($this->debug) Log::info( "SendPushTextToAgentJob::handle #".$jobID." queue=".$this->queue." END");
	}

	public static function putQueueSendPush( $text, $title, $agentID, $whitelabelID = 0, $apiversion = 1 ) {
		try {
			$queuename = "fintechapi_email";
			$job = new SendPushTextToAgentJob( $text, $title, $agentID, $whitelabelID, $apiversion );
			if (! empty($queuename)) {
				$job->onQueue($queuename);
			}
			dispatch( $job );
			return true;
		} catch ( Exception $e ) {
			Log::info("AgentBalanceBL::putQueueSendPush exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
}
