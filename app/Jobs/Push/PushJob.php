<?php
namespace App\Jobs\Push;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Config;
use Exception;
use App\BusinessLogic\Push\PushBL;
use App\Core\Push\PushKey;

class PushJob implements ShouldQueue {

	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $debug = false;
	protected $apiversion = 1;
	protected $platform = PushKey::PLATFORM_ANDROID;
	protected $option = null;
	protected $notification = null;
	protected $data = null;
	protected $targetType = PushKey::TARGET_TYPE_TOKEN;
	protected $selectedTarget = null;
	protected $targetDevice = PushKey::TARGET_DEVICE_ANDROID;
	protected $whitelabelID = 0;
	protected $protocol = PushKey::PROTOCOL_GOOGLE_FCM;
	
	public function __construct($whitelabelID, $option, $notification, $data, $targetType, $selectedTarget, $targetDevice , 
		$platform = PushKey::PLATFORM_ANDROID, $protocol = PushKey::PROTOCOL_GOOGLE_FCM, $apiversion = 1 ) {
		try {
			$this->debug = boolval(Config::get('app.debug'));
			$this->option = $option;
			$this->notification = $notification;
			$this->data = $data;
			$this->targetType = $targetType;
			$this->selectedTarget = $selectedTarget;
			$this->whitelabelID = $whitelabelID;
			$this->platform = $platform;
			$this->protocol = $protocol;
			$this->apiversion = (int)$apiversion;
		} catch ( Exception $e ) {
			Log::info( "PushJob->__construct exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}

	public function handle() {
		$jobID = 0;
		$b = false;
		try {
			$jobID = $this->job->getJobId();
			Log::info( "PushJob->handle #".$jobID." queue=".$this->queue);
		} catch ( Exception $e ) {
			Log::info( "PushJob->handle exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		try {
			$pushBL = new PushBL($this->whitelabelID, $this->platform, $this->protocol);
			$rs = $pushBL->sendPushWithQueue($this->option, $this->notification, $this->data, $this->targetType, 
				   $this->selectedTarget, $this->targetDevice);
			if (!is_null($rs)) {
				$b = true;
			} 
			Log::info( "PushJob->handle: result=".($b?"true":"false"));			
		} catch ( Exception $e ) {
			Log::info( "PushJob->handle #".$jobID." queue=".$this->queue." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			Log::error ( $e );
		}
		try {
			$this->job->delete();
		} catch ( Exception $e ) {
			Log::info( "PushJob::handle #".$jobID." queue=".$this->queue." delete_job exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		Log::info( "PushJob::handle #".$jobID." queue=".$this->queue." END");
	}
	
}