<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Log;
use App\Models\TrxRequestWorkerModel;

class TrxWorkerReportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    var $workerNumber;
    
    public function __construct($workerNumber)
    {
        $this->workerNumber = $workerNumber;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $q = TrxRequestWorkerModel::where('worker_number', '=', $this->workerNumber)
                ->first();
        if ($q) {
            Log::info('==== worker '.$this->workerNumber.' done ====');
            $q->active = 'N';
            $q->access_key = '';
            $q->save();
        }
        
        $this->delete();
    }
}
