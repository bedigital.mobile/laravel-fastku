<?php
namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Config;
use Exception;
use App\BusinessLogic\Log\LogUserActivityBL;
use App\BusinessLogic\HelpdeskBL;
use App\Core\Util\UtilEmail;
class InsertLogPushMailJob implements ShouldQueue {

	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	private $toEmail;
	private $fullname;
	private $username;
	protected $debug;
// InsertLogPushMailJob( $toEmail, $fullname, $username 
	public function __construct( $toEmail, $fullname, $username ) {
		try {
			$this->toEmail = $toEmail;
			$this->fullname = $fullname;
			$this->username = $username;
			// if (! is_null($input) && is_array($input)) {
			// 	$this->input = $input;
			// }
			$this->debug = true;

 Log::info("InsertLogPushMailJob::__construct GO mail=".$this->toEmail." full=".$this->fullname." user=".$this->username);
		} catch ( Exception $e ) {
			Log::info( "InsertLogPushMailJob::__construct exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}

	public function handle() {
		$jobID = 0;
		Log::info( "InsertLogPushMailJob::__ START");
		try {
			$jobID = $this->job->getJobId();
			  Log::info( "InsertLogPushMailJob::handle #".$jobID." queue=".$this->queue." START input= mail=".$this->toEmail." full=".$this->fullname." user=".$this->username);
		} catch ( Exception $e ) {
			Log::info( "InsertLogPushMailJob::handle exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		try {
			$hdBL = new HelpdeskBL();
			$b = $hdBL->sendmailAntv($this->toEmail, $this->fullname, $this->username);
			if ($this->debug) Log::info( "InsertLogPushMailJob::handle #".$jobID." queue=".$this->queue." b=".($b?"true":"false"));
		} catch ( Exception $e ) {
			Log::info( "InsertLogPushMailJob::handle #".$jobID." queue=".$this->queue." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			Log::error ( $e );
		}
		try {
			$this->job->delete();
		} catch ( Exception $e ) {
			Log::info( "InsertLogPushMailJob::handle #".$jobID." queue=".$this->queue." delete_job exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		if ($this->debug) Log::info( "InsertLogPushMailJob::handle #".$jobID." queue=".$this->queue." END");
	}

}