<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\PartnerActivityLogModel;

class PartnerActivityLogJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    var $ipAddress;
    var $apiKey;
    var $resourceName;
    var $params;
    var $errorCode;
    var $errorMsg;
    var $response;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($ipAddress, $apiKey, $resourceName, $params='', $response='', $errorCode='', $errorMsg='')
    {
        $this->ipAddress = $ipAddress;
        $this->apiKey = $apiKey;
        $this->resourceName = $resourceName;
        $this->params = $params;
        $this->errorCode = $errorCode;
        $this->errorMsg = $errorMsg;
        $this->response = $response;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {            
            $log = new PartnerActivityLogModel();
            $log->ip_address = $this->ipAddress;
            $log->api_key = $this->apiKey;
            $log->resource_name = $this->resourceName;
            $log->params = $this->params;
            $log->response_error_code = $this->errorCode;
            $log->response_error_message = $this->errorMsg;
            $log->response = $this->response;
            $log->save();

            $this->job->delete();
        } catch (Exception $ex) {

        }
    }
}
