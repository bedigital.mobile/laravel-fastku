<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Log;
use App\Http\Helpers\HttpClientHelper;

use App\Models\PartnerTrxRequestCallbackModel;

class PartnerTrxRequestCallbackJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    var $url;
    var $params;
    
    public function __construct($url, $params)
    {
        $this->url = $url;
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            
        Log::info('===Job: '.$this->job->getJobId().' Start Calling Partner trx callback with params: '.json_encode($this->params).' ===');        
        
            $response = HttpClientHelper::invoke($this->url, 'POST', $this->params);
            Log::info('Partner callback response: '.$response['status'].' - '.$response['body']);

            if (isset($this->params['data']) && $this->params['data'] != null && !empty($this->params['data'])) {
                $q = new PartnerTrxRequestCallbackModel();
                $q->product_code = $this->params['data']['product'];
                $q->product_denom = $this->params['data']['nominal'];
                $q->admin_fee = $this->params['data']['admin'];
                $q->payment_code = $this->params['data']['paymentCode'];
                $q->bill_number = $this->params['data']['billNumber'];
                $q->trx_code = $this->params['data']['trxId'];
                $q->trx_type = $this->params['data']['type'];
                $q->service = $this->params['data']['service'];
                $q->signature = $this->params['data']['signature'];
                $q->callback_status = $response['status'];
                $q->callback_response = $response['body'];
                $q->save();
            }
        
//        Log::info('===Job: '.$this->job->getJobId().' End Calling Partner trx callback ===');
        } catch (Exception $ex) {
            Log::info('===Job: '.$this->job->getJobId().' Failed Calling Partner trx callback ===');
        }
        
        try {
            
            Log::info('===Job: '.$this->job->getJobId().' Delete Calling Partner trx callback ===');
            $this->job->delete();
            
        } catch (Exception $ex) {
            Log::info('===Job: '.$this->job->getJobId().' Failed Delete Calling Partner trx callback ===');
        }
    }
}
