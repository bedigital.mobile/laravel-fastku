<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerModel extends Model
{
    protected $table = 'partner';
    
    const PARTNER_PAYMODE_PREPAID = 'PREPAID';
    const PARTNER_PAYMODE_POSTPAID = 'POSTPAID';
        
}
