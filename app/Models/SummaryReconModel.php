<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SummaryReconModel extends Model
{
    protected $table = 'beh2hlog.SUMMARY_RECON';
}
