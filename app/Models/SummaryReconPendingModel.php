<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SummaryReconPendingModel extends Model
{
    protected $table = 'beh2hlog.SUMMARY_RECON_PENDING';
}
