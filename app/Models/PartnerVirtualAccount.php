<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerVirtualAccount extends Model
{
    protected $table = 'beh2hlog.partner_virtual_account';
}
