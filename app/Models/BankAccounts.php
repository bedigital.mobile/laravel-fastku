<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Database\Bank;
use App\Models\Database\PaymentGateway;

class BankAccounts extends Model
{
	protected $table = 'bank_accounts';

	// account_type  enum('VA','GIRO','SAVINGS','DEPOSIT')
	const ACCOUNT_TYPE_VA = "VA";
	const ACCOUNT_TYPE_GIRO = "GIRO";
	const ACCOUNT_TYPE_SAVINGS = "SAVINGS";
	const ACCOUNT_TYPE_DEPOSIT = "DEPOSIT";
	
	// owner_type  enum('AGENT','MASTER_AGENT','WHITE_LABEL')
	const OWNER_TYPE_AGENT = "AGENT";
	const OWNER_TYPE_MASTER_AGENT = "MASTER_AGENT";
	const OWNER_TYPE_WHITE_LABEL = "WHITE_LABEL";
	const OWNER_TYPE_PARTNER = "PARTNER";
	
	private $tablebank = "";
	private $tablepaymentgw = "";
	
	public function scopeListBankAccountsHasInquiry( $q, $ownerType, $ownerID, $onlyID = true ) {
		$column = array();
		if ($onlyID) {
			$column = array(
						$this->table . '.id'
					);
		} else {
			$column = array(
					DB::raw($this->table . '.*')
			);
		}
		$mTable = new Bank();
		$tablebank = $mTable->getTable();
		$this->tablebank = $tablebank;
		$mTable = new PaymentGateway();
		$tablepaymentgw = $mTable->getTable();
		$this->tablepaymentgw = $tablepaymentgw;
		$q->join( $tablebank, ($tablebank.".id"), "=", ($this->table.".bank_id") )
		  ->join( $tablepaymentgw, function ($join) {
		  		$join->on( $this->tablepaymentgw . ".id", "=", DB::raw($this->tablebank . ".payment_gateway_id") )
		  			 ->where( $this->tablepaymentgw . ".has_inquiry", "=", "Y" );
		  })
		  ->select( $column )
		  ->where( $this->table . ".owner_type", "=", $ownerType )
		  ->where( $this->table . ".owner_id", "=", $ownerID )
		  ->orderBy( $this->table . ".id", "ASC" );
		return $q;
	}
	
}