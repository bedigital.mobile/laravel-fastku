<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class BillerModel extends Model
{
    protected $table = 'biller_aggregator';
}
