<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class CityModel extends Model
{
    protected $table = 'city';
    
    function province() {
        return $this->belongsTo('App\ProvinceModel', 'province_id');
    }
    
    function districts() {
        return $this->hasMany('App\DistrictModel', 'city_id');
    }
}
