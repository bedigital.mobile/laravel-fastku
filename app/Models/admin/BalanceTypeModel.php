<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class BalanceTypeModel extends Model
{
    protected $table = 'balance_type';
    
    const BALANCE_TYPE_PREPAID = 'PREPAID';
    const BALANCE_TYPE_POSTPAID = 'POSTPAID';
    const BALANCE_TYPE_WALLET = 'WALLET';
}
