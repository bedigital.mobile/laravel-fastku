<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class AgentProfileModel extends Model
{
    protected $table = 'befintech.agent_profile';
}
