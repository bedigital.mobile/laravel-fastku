<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class AgentProfileMandatoryModel extends Model
{
    protected $table = 'agent_profile_mandatory';
    
    const MANDATORY_FIELDS = [
        ['fullname', 'Full Name'],
        ['email', 'Email Address'],
        ['district_name', 'District Name'],
        ['city_name', 'City Name'],
        ['photo_filename', 'Photo'],
        ['phone_number', 'Phone Number'],
        ['address', 'Address'],
        ['village_name', 'Village Name'],
        ['provice_name', 'Province Name'],
        ['id_card_filename', 'ID Card Captured']
    ];
}
