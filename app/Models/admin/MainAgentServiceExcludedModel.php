<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class MainAgentServiceExcludedModel extends Model
{
    protected $table = 'master_agent_service_exclude';
}
