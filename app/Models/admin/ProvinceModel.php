<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class ProvinceModel extends Model
{
    protected $table = 'province';
    
    function cities() {
        return $this->hasMany('App\CityModel', 'province_id');
    }
}
