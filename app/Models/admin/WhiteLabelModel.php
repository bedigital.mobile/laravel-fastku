<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class WhiteLabelModel extends Model
{
    protected $table = 'white_label';
}
