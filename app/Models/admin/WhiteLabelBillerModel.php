<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class WhiteLabelBillerModel extends Model
{
    protected $table = 'white_label_biller_aggregators';
}
