<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class WhiteLabelBalanceTypeModel extends Model
{
    protected $table = 'white_label_balance_types';
}
