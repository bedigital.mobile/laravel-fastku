<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class MainAgentProfileModel extends Model
{
    protected $table = 'befintech.master_agent_profile';
    
    const PROFILE_TYPE_CORPORATE = 'CORPORATE';
    const PROFILE_TYPE_PERSONAL = 'PERSONAL';
    
    function account() {
        return $this->belongsTo('App\MainAgent', 'id', 'master_agent_id');
    }
}
