<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class MainAgentModel extends Model
{
    protected $table = 'befintech.master_agent';
    
    const STATUS_REGISTER = 'REGISTERING';
    const STATUS_UNAPPROVED = 'UNAPPROVED';
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_INACTIVE = 'DEACTIVATED';
    const STATUS_SUSPENDED = 'SUSPENDED';
    
    const PAYMODE_PREPAID = 'PREPAID';
    const PAYMODE_POSTPAID = 'POSTPAID';

}
