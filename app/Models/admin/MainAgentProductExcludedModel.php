<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class MainAgentProductExcludedModel extends Model
{
    protected $table = 'master_agent_product_exclude';
    
    protected $fillable = ['master_agent_id', 'product_id', 'active', 'created_at', 'updated_at'];
            
    function masterAgent() {
        return $this->belongsTo('App\MainAgentModel', 'master_agent_id', 'id');
    }
}
