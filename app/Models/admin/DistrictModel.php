<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class DistrictModel extends Model
{
    protected $table = 'district';
    
    function city() {
        return $this->belongsTo('App\CityModel', 'city_id');
    }
    
    function villages() {
        return $this->hasMany('App\VillageModel', 'district_id');
    }
}
