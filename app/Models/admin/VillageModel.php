<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class VillageModel extends Model
{
    protected $table = 'village';
    
    function district() {
        return $this->belongsTo('App\DistrictModel', 'district_id');
    }
}
