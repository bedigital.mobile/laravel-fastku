<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class WhiteLabelAdminModel extends Model
{
    protected $table = 'white_label_admin_user';
    
    const STATUS_ACTIVE = 'Y';
    const STATUS_INACTIVE = 'N';
    
    function whiteLabel() {
        return $this->hasOne('App\Models\admin\WhiteLabelModel', 'id', 'white_label_id');
    }
}
