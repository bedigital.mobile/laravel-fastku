<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogSmsSend extends Model
{ 
    protected $table = 'befintechlog.log_sms_send';
}
