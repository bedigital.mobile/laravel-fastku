<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WhiteLabelProductBreakupModel extends Model
{
    protected $table = 'white_label_products_breakup';
}
