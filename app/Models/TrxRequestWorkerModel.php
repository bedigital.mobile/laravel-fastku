<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrxRequestWorkerModel extends Model
{
    protected $table = 'trx_request_worker';
}
