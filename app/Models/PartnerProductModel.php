<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerProductModel extends Model
{
    protected $table = 'partner_products';
}
