<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogPaymentGatewayModel extends Model
{
    protected $table = 'befintechlog.log_payment_gateway_callback';
}
