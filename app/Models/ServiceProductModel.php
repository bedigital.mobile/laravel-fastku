<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceProductModel extends Model
{
    protected $table = 'service_products';
}
