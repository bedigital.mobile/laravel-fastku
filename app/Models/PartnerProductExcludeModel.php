<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerProductExcludeModel extends Model
{
    protected $table = 'partner_products_exclude';
}
