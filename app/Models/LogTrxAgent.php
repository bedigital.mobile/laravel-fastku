<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogTrxAgent extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'befintechlog.log_trx_agent';
}
