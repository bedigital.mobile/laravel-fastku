<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerDepositModel extends Model
{
    protected $table = 'partner_deposit';
}
