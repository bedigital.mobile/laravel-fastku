<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WhiteLabelServiceModel extends Model
{
    protected $table = 'white_label_services';
}
