<?php

namespace App\Models\kurir;

use Illuminate\Database\Eloquent\Model;

class MemberProfile extends Model
{ 
    protected $table = 'belogistic.member_profile';
}
