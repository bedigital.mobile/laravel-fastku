<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WhiteLabelUserUid extends Model
{
    protected $table = 'white_label_admin_user_uid';
}
