<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerWhiteListIpModel extends Model
{
    protected $table = 'partner_whitelist_ip';
}
