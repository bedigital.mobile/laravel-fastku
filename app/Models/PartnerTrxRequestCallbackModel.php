<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerTrxRequestCallbackModel extends Model
{
    protected $table = 'beh2hlog.partner_trx_request_callback';
}
