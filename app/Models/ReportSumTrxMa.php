<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportSumTrxMa extends Model
{
    protected $table = 'befintechreport.report_summary_trx_ma';
    
    const PARTNER_PAYMODE_PREPAID = 'PREPAID';
    const PARTNER_PAYMODE_POSTPAID = 'POSTPAID';
        
}
