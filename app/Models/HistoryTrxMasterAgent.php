<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoryTrxMasterAgent extends Model
{
    protected $table = 'befintechlog.history_trx_master_agent';
}
