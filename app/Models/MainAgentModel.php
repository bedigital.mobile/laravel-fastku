<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Util\UtilCommon;
use App\Core\Util\UtilEmail;
use App\Core\Util\UtilPhone;
use App\Models\Database\MasterAgentProfile;
use App\Models\Database\MasterAgentBalances;
use App\Models\Database\WhiteLabel;

class MainAgentModel extends Model
{
	protected $table = 'master_agent';
	private $sql;

	private $tableprofile = "";
	private $searchType = "";
	private $arrParamSearch = array();
	private $tablewhitelabel = "";
	private $tablebalance = "";
	
	const COLUMN_ALIAS_PRIORITY = "priority";
	
	const FIELD_ID = 'id';
	const FIELD_WHITE_LABEL_ID = 'white_label_id';
	const FIELD_USERNAME = 'username';
	const FIELD_MASTER_AGENT_PARENT_ID = 'master_agent_parent_id';
	const FIELD_PASSWORD_ENC = 'password_enc';
	const FIELD_PASSWORD_TYPE = 'password_type';
	const FIELD_STATUS = 'status';
	const FIELD_ACTIVE_APPROVED_AT = 'active_approved_at';
	const FIELD_ACTIVE_APPROVED_BY = 'active_approved_by';
	const FIELD_PAYMODE = 'paymode';
	const FIELD_AGENT_MAX_TERMINAL_DEFAULT = 'agent_max_terminal_default';
	const FIELD_CREATED_AT = 'created_at';
	const FIELD_UPDATED_AT = 'updated_at';

	const PASSWORD_TYPE_PLAIN = 'PLAIN';
	const PASSWORD_TYPE_MD5 = 'MD5';
	const STATUS_REGISTERING = 'REGISTERING';
	const STATUS_ACTIVE = 'ACTIVE';
	const STATUS_SUSPENDED = 'SUSPENDED';
	const STATUS_DEACTIVATED = 'DEACTIVATED';
	const STATUS_UNAPPROVED = 'UNAPPROVED';

	const FLAG_ACTIVE_MASTER_AGENT = 'flag_active_agent';

	// enum('PERSONAL','CORPORATE')
	const PROFILE_TYPE_PERSONAL = "PERSONAL";
	const PROFILE_TYPE_CORPORATE = "CORPORATE";
	
	const PAYMODE_POSTPAID = "POSTPAID";
	const PAYMODE_PREPAID = "PREPAID";
	
	public function __construct( ) {

	}

	public function scopeSearch( $query, $whitelabelId, $searchFilter = array(), $page = 1, $max = 1000 ) {
		/* 
			Search agent based on 1 or more combination of the following filters :
			- WHITELABEL -> Default, mandatory.
			- [USERNAME | FULLNAME | EMAIL | DESCRIPTION] -> Optional, can be blank.
			- [MASTER AGENT && PHONE NUMBER && STATUS && ACTIVE_APPROVED_AT] -> Optional, can be blank.
		*/
		$values = array();
		//Build filter 1 : multiple LIKE query component for username, fullname, email, address and description. Combine with OR.
		$queryFilter1 = "1=2";

		if ( array_key_exists(MasterAgent::FIELD_USERNAME, $searchFilter) ) {
			if ($searchFilter[MasterAgent::FIELD_USERNAME] != '') {
				$queryFilter1 = $queryFilter1 . " or ".self::FIELD_USERNAME." like :vi".self::FIELD_USERNAME;
				$values["vi".self::FIELD_USERNAME] = "%".$searchFilter[self::FIELD_USERNAME]."%";
			}
		}

		if ( array_key_exists(MasterAgentProfile::FIELD_FULLNAME, $searchFilter) ) {
			if ($searchFilter[MasterAgentProfile::FIELD_FULLNAME] != '') {
				$queryFilter1 = $queryFilter1 . " or master_agent_profile.".MasterAgentProfile::FIELD_FULLNAME." like :vi"
					.MasterAgentProfile::FIELD_FULLNAME; 
				$values["vi".MasterAgentProfile::FIELD_FULLNAME] = "%".$searchFilter[MasterAgentProfile::FIELD_FULLNAME]."%";
			}
		}		

		if ( array_key_exists(MasterAgentProfile::FIELD_EMAIL, $searchFilter) ) {
			if ($searchFilter[MasterAgentProfile::FIELD_EMAIL] != '') {
				$queryFilter1 = $queryFilter1 . " or master_agent_profile.".MasterAgentProfile::FIELD_EMAIL." like :vi"
					.MasterAgentProfile::FIELD_EMAIL; 
				$values["vi".MasterAgentProfile::FIELD_EMAIL] = "%".$searchFilter[MasterAgentProfile::FIELD_EMAIL]."%";
			}
		}	

		if ( array_key_exists(MasterAgentProfile::FIELD_DESCRIPTION, $searchFilter) ) {
			if ($searchFilter[MasterAgentProfile::FIELD_DESCRIPTION] != '') {
				$queryFilter1 = $queryFilter1 . " or master_agent_profile.".MasterAgentProfile::FIELD_DESCRIPTION." like :vi"
					.MasterAgentProfile::FIELD_DESCRIPTION; 
				$values["vi".MasterAgentProfile::FIELD_DESCRIPTION] = "%".$searchFilter[MasterAgentProfile::FIELD_DESCRIPTION]."%";
			}
		}	

		if ( array_key_exists(MasterAgentProfile::FIELD_ADDRESS, $searchFilter) ) {
			if ($searchFilter[MasterAgentProfile::FIELD_ADDRESS] != '') {
				$queryFilter1 = $queryFilter1 . " or master_agent_profile.".MasterAgentProfile::FIELD_ADDRESS." like :vi"
					.MasterAgentProfile::FIELD_ADDRESS; 
				$values["vi".MasterAgentProfile::FIELD_ADDRESS] = "%".$searchFilter[MasterAgentProfile::FIELD_ADDRESS]."%";
			}
		}	

		//Build filter 2 : query for component phone number, faxnumber, status. Combine with AND.
		$queryFilter2 = self::FIELD_WHITE_LABEL_ID." = :vi".self::FIELD_WHITE_LABEL_ID;
		$values["vi".self::FIELD_WHITE_LABEL_ID] = $whitelabelId;

		if ( array_key_exists(self::FIELD_MASTER_AGENT_PARENT_ID, $searchFilter) ) {
			if ($searchFilter[self::FIELD_MASTER_AGENT_PARENT_ID] != '') {
				$queryFilter2 = $queryFilter2 . " and ".self::FIELD_MASTER_AGENT_PARENT_ID." = :vi".self::FIELD_MASTER_AGENT_PARENT_ID;
				$values["vi".self::FIELD_MASTER_AGENT_PARENT_ID] = $searchFilter[self::FIELD_MASTER_AGENT_PARENT_ID];
			}
		}	

		if ( array_key_exists(MasterAgentProfile::FIELD_FAX_NUMBER, $searchFilter) ) {
			if ($searchFilter[MasterAgentProfile::FIELD_FAX_NUMBER] != '') {
				$queryFilter2 = $queryFilter2 . " and master_agent_profile.".MasterAgentProfile::FIELD_FAX_NUMBER." = :vi"
					.MasterAgentProfile::FIELD_FAX_NUMBER;
				$values["vi".MasterAgentProfile::FIELD_FAX_NUMBER] = $searchFilter[MasterAgentProfile::FIELD_FAX_NUMBER];
			}
		}

		if ( array_key_exists(MasterAgentProfile::FIELD_PHONE_NUMBER, $searchFilter) ) {
			if ($searchFilter[MasterAgentProfile::FIELD_PHONE_NUMBER] != '') {
				$queryFilter2 = $queryFilter2 . " and master_agent_profile.".MasterAgentProfile::FIELD_PHONE_NUMBER." = :vi"
					.MasterAgentProfile::FIELD_PHONE_NUMBER;
				$values["vi".MasterAgentProfile::FIELD_PHONE_NUMBER] = $searchFilter[MasterAgentProfile::FIELD_PHONE_NUMBER];
			}
		}

		if ( array_key_exists(self::FIELD_STATUS, $searchFilter) ) {
			if ($searchFilter[self::FIELD_STATUS] != '') {
				$queryFilter2 = $queryFilter2 . " and ".self::FIELD_STATUS." = :vi".self::FIELD_STATUS;
				$values["vi".self::FIELD_STATUS] = $searchFilter[self::FIELD_STATUS];
			}
		}	

		if ( array_key_exists(self::FLAG_ACTIVE_MASTER_AGENT, $searchFilter) ) {
			if ($searchFilter[self::FLAG_ACTIVE_MASTER_AGENT]) {
				$queryFilter2 = $queryFilter2 . " and ".self::FIELD_ACTIVE_APPROVED_AT." is not null";
			} else {
				$queryFilter2 = $queryFilter2 . " and ".self::FIELD_ACTIVE_APPROVED_AT." is null";
			}
		}

		if ($queryFilter1 == '1=2') {
			//Query is empty for filter group 1. Reset to through because it will cancel the group 2 query result.
			$queryFilter1 = '1=1';
		}

		//Build final query
		$this->sql = "1=1 and (".$queryFilter1.") and (".$queryFilter2.")"; 	

		//Assign query and bind it's parameters with values. Get data in selected page.
		$pageSetup = $this->definePage($page,$max);
		$query
			->join('master_agent_profile', 'master_agent_profile.master_agent_id', '=', 'master_agent.id')	
			->selectRaw('master_agent.id as master_agent_id, master_agent_profile.id as master_agent_profile_id, master_agent_profile.fullname as fullname, master_agent.username as username')
			->whereRaw($this->sql,$values)
			->skip( $pageSetup["startID"] )->take( $pageSetup["max"] );

		return $query;		

	}

	public function scopeGetById($query, $masterAgentId) {
		return $query
			->where('id',$masterAgentId);
	}	

	public function getSql() {
		return $this->sql;
	}

	private function definePage($page, $max) {
		if ($page < 1) {
			$page = 1;
		}
		if ($max < 1) {
			$max = 1000;
		}
		$startID = ($page - 1) * $max;
		return array(
			"startID" => $startID,
			"max" => $max
		);
	}
	
	public function scopeGetWithProfile( $q, $id, $active = true ) {
		$id = (int)$id;
		$mTable = new MasterAgentProfile();
		$tableprofile = $mTable->getTable();
		$column = array(
				($this->table . 		'.id'),
				($this->table . 		'.username'),
				($this->table . 		'.white_label_id'),
				($this->table . 		'.master_agent_parent_id'),
				($this->table . 		'.master_agent_max_downline'),
				($this->table . 		'.default_commission_type'),
				($this->table . 		'.default_commission_value'),
				($this->table . 		'.status'),
				DB::raw('IFNULL('.$tableprofile.'.fullname,'.$this->table.'.username) AS fullname'),
				($tableprofile . 		'.photo_filename')
		);
		$q->leftJoin( $tableprofile, ($tableprofile.".master_agent_id"), "=", ($this->table.".id") )
		  ->where( $this->table . ".id", "=", $id );
		if ($active) {
			$q->where( $this->table . ".status", "=", self::STATUS_ACTIVE );
		}
		$q->select( $column );
		return $q;
	}
	
	public function scopeListByWhiteLabel( $q, $whitelabelID, $page = 1, $max = 100, $maxPlusOne = true, $active = true, $parentID = -1 ) {
		if (! is_array($parentID)) {
			$parentID = (int)$parentID;
		}
		if ($page < 1) {
			$page = 1;
		}
		if ($max < 1) {
			$max = 100;
		}
		$startID = ($page - 1) * $max;
		if ($maxPlusOne) {
			$max++;
		}
		$mTable = new MasterAgentProfile();
		$tableprofile = $mTable->getTable();
		$column = array(
				($this->table . 		'.id'),
				($this->table . 		'.username'),
				($this->table . 		'.master_agent_parent_id'),
				($this->table . 		'.master_agent_max_downline'),
				($this->table . 		'.default_commission_type'),
				($this->table . 		'.default_commission_value'),
				($this->table . 		'.status'),
				DB::raw('IFNULL('.$tableprofile.'.fullname,'.$this->table.'.username) AS fullname'),
				($tableprofile . 		'.photo_filename')
		);
		$q->leftJoin( $tableprofile, ($tableprofile.".master_agent_id"), "=", ($this->table.".id") )
		  ->where( $this->table.".white_label_id", "=", DB::raw((int)$whitelabelID));
		if (is_array($parentID)) {
			if (count($parentID) > 0) {
				$q->whereIn( $this->table . ".master_agent_parent_id", $parentID );
			}
		} else {
			if ($parentID >= 0) {
				$q->where( $this->table . ".master_agent_parent_id", "=", $parentID );
			}
		}
		if ($active) {
			$q->where( $this->table . ".status", "=", self::STATUS_ACTIVE );
		}
		$q->select( $column )
		  ->orderBy( DB::raw("fullname"), "ASC" )
		  ->orderBy( $this->table . ".username", "ASC" )
		  ->orderBy( $this->table . ".id", "ASC" )
		  ->skip( $startID )
		  ->take( $max );
		return $q;
	}
	
	public function scopeListDownline( $q, $whitelabelID, $masterAgentID, $useProfile = false, $page = 1, $max = 100, $maxPlusOne = true, $active = true, $status = "" ) {
		if ($page < 1) {
			$page = 1;
		}
		if ($max < 1) {
			$max = 100;
		}
		$startID = ($page - 1) * $max;
		if ($maxPlusOne) {
			$max++;
		}
		$column = array();
		if ($useProfile) {
			$mTable = new MasterAgentProfile();
			$tableprofile = $mTable->getTable();
			$column = array(
					($this->table . 		'.id'),
					($this->table . 		'.username'),
					($this->table . 		'.master_agent_parent_id'),
					($this->table . 		'.master_agent_max_downline'),
					($this->table . 		'.default_commission_type'),
					($this->table . 		'.default_commission_value'),
					($this->table . 		'.status'),
					DB::raw('IFNULL('.$tableprofile.'.fullname,'.$this->table.'.username) AS fullname'),
					($tableprofile . 		'.photo_filename')
			);
			$q->leftJoin( $tableprofile, ($tableprofile.".master_agent_id"), "=", ($this->table.".id") );
		} else {
			$column = array(
					($this->table . 		'.id'),
					($this->table . 		'.username'),
					($this->table . 		'.master_agent_parent_id'),
					($this->table . 		'.master_agent_max_downline'),
					($this->table . 		'.default_commission_type'),
					($this->table . 		'.default_commission_value'),
					($this->table . 		'.status')
			);
		}
		$q->where( $this->table.".white_label_id", "=", $whitelabelID);
		if (! empty($masterAgentID)) {
			$q->where( $this->table.".master_agent_parent_id", "=", $masterAgentID);
		}
		if ($active) {
			$q->where( $this->table . ".status", "=", self::STATUS_ACTIVE );
		} else {
			if (! empty($status)) {
				$status = strtoupper($status);
				$q->where( $this->table . ".status", "=", $status );
			}
		}
		$q->select( $column );
		if ($useProfile) {
			$q->orderBy( DB::raw("fullname"), "ASC" );
		}
		$q->orderBy( $this->table . ".username", "ASC" )
		  ->orderBy( $this->table . ".id", "ASC" )
		  ->skip( $startID )
		  ->take( $max );
		return $q;
	}
	
	public function scopeSearchIncludeProfile( $q, $name, $whitelabelID, $parentID = 0, $page = 1, $max = 100, $maxPlusOne = true, $active = true, $status = "" ) {
		$whitelabelID = (int)$whitelabelID;
		if (! is_array($parentID)) {
			$parentID = (int)$parentID;
		}
		if ($page < 1) {
			$page = 1;
		}
		if ($max < 1) {
			$max = 100;
		}
		$startID = ($page - 1) * $max;
		if ($maxPlusOne) {
			$max++;
		}
		$mTable = new MasterAgentProfile();
		$tableprofile = $mTable->getTable();
		$this->tableprofile = $tableprofile;
		$column = array(
				($this->table . 		'.id'),
				($this->table . 		'.username'),
				($this->table . 		'.master_agent_parent_id'),
				($this->table . 		'.master_agent_max_downline'),
				($this->table . 		'.default_commission_type'),
				($this->table . 		'.default_commission_value'),
				($this->table . 		'.status'),
				DB::raw('IFNULL('.$tableprofile.'.fullname,'.$this->table.'.username) AS fullname'),
				($tableprofile . 		'.photo_filename'),
				($tableprofile . 		'.email'),
				($tableprofile . 		'.phone_number'),
				($tableprofile . 		'.contact_number'),
				($tableprofile . 		'.fax_number'),
				($tableprofile . 		'.province_id'),
				($tableprofile . 		'.province_name'),
				($tableprofile . 		'.city_id'),
				($tableprofile . 		'.city_name')
		);
		$name = trim($name);
		if (UtilPhone::isPhoneValid($name)) {
			$this->searchType = "PHONENUMBER";
			$name = UtilPhone::normalizeToMSISDN($name);
		} else if (UtilEmail::isValidEmail($name)) {
			$this->searchType = "EMAIL";
			$name = strtolower($name);
		} else {
			$this->searchType = "GENERAL";
		}
		if ($this->searchType == "GENERAL") {
			$name = trim(str_replace(array("-","_","(",")"), " ", $name));
			$len = strlen($name);
			if ($len > 9 && strtoupper(substr($name, 0, 9)) == 'KABUPATEN') {
				$name = "KAB. " . trim(substr($name, 9));
			} else if ($len > 4 && strtoupper(substr($name, 0, 4)) == 'KAB ') {
				$name = "KAB. " . trim(substr($name, 4));
			} else if ($len > 9 && strtoupper(substr($name, 0, 9)) == 'KECAMATAN') {
				$name = trim(substr($name, 9));
			} else if ($len > 4 && strtoupper(substr($name, 0, 4)) == 'KEC.') {
				$name = trim(substr($name, 4));
			} else if ($len > 4 && strtoupper(substr($name, 0, 4)) == 'KEC ') {
				$name = trim(substr($name, 4));
			}
			if ($len > 12) {
				$name = trim(str_replace("ADMINISTRASI", " ADM. ", $name));
			}
			if ($len > 5) {
				$name = trim(str_replace(" ADM ", " ADM. ", $name));
			}
			$name = trim(str_replace(array("\t","\r","\n"), " ", $name));
			while ( strpos($name, "  ") > 0 ) {
				$name = trim(str_replace("  ", " ", $name));
			}
		}
		if (! empty($name)) {
			if (strpos($name, " ") > 0) {
				$arr = explode(" ", $name);
				for ($i = 0; $i < count($arr); $i++) {
					$s = trim($arr[$i]);
					if (! empty($s)) {
						$this->arrParamSearch[] = $s;
					}
				}
			} else {
				$this->arrParamSearch[] = $name;
			}
		}
		if (count($this->arrParamSearch) <= 0) {
			$name = "A";
			$this->arrParamSearch[] = "A";
		}
		$searchprm = str_replace("'", "\\'", $name);
		$lensearch = strlen($name);
		$sqlPriority = "";
		$sqlPriority .= "IF(".$this->table.".username='".$searchprm."',1,";
		if ($this->searchType == "PHONENUMBER") {
			$sqlPriority .= "IF(".$tableprofile.".phone_number=".$searchprm." OR ".$tableprofile.".contact_number=".$searchprm." OR ".$tableprofile.".fax_number=".$searchprm.",2,3";
			$sqlPriority .= "))";
		} else if ($this->searchType == "EMAIL") {
			$sqlPriority .= "IF(".$tableprofile.".email='".$searchprm."',2,";
			$sqlPriority .= "IF(SUBSTRING(".$this->table.".username,1,".$lensearch.")='".$searchprm."' OR SUBSTRING(".$tableprofile.".email,1,".$lensearch.")='".$searchprm."',3,";
			$sqlPriority .= "IF(INSTR(".$this->table.".username,'".$searchprm."')>0 OR INSTR(".$tableprofile.".email,'".$searchprm."')>0,4,5";
			$sqlPriority .= "))))";
		} else {
			$sqlPriority .= "IF(TRIM(".$tableprofile.".fullname)='".$searchprm."' OR TRIM(".$tableprofile.".province_name)='".$searchprm."' OR TRIM(".$tableprofile.".city_name)='".$searchprm."',2,";
			$sqlPriority .= "IF(SUBSTRING(LTRIM(".$this->table.".username),1,".$lensearch.")='".$searchprm."' OR SUBSTRING(LTRIM(".$tableprofile.".fullname),1,".$lensearch.")='".$searchprm."' OR SUBSTRING(LTRIM(".$tableprofile.".province_name),1,".$lensearch.")='".$searchprm."' OR SUBSTRING(LTRIM(".$tableprofile.".city_name),1,".$lensearch.")='".$searchprm."',3,";
			$sqlPriority .= "IF(INSTR(".$this->table.".username,'".$searchprm."')>0 OR INSTR(".$tableprofile.".fullname,'".$searchprm."')>0 OR INSTR(".$tableprofile.".province_name,'".$searchprm."')>0 OR INSTR(".$tableprofile.".city_name,'".$searchprm."')>0,4,5";
			for ($i = 0; $i < count($this->arrParamSearch); $i++) {
				$sqlPriority .= " + IF(INSTR(".$this->table.".username,'".str_replace("'", "\\'", $this->arrParamSearch[$i])."')>0 OR INSTR(".$tableprofile.".fullname,'".str_replace("'", "\\'", $this->arrParamSearch[$i])."')>0 OR INSTR(".$tableprofile.".province_name,'".str_replace("'", "\\'", $this->arrParamSearch[$i])."')>0 OR INSTR(".$tableprofile.".city_name,'".str_replace("'", "\\'", $this->arrParamSearch[$i])."')>0,0,1)";
			}
			$sqlPriority .= "))))";
		}
		$sqlPriority .= " AS " . self::COLUMN_ALIAS_PRIORITY;
		$column[] = DB::raw($sqlPriority);
		$q->leftJoin( $tableprofile, ($tableprofile.".master_agent_id"), "=", ($this->table.".id") )
		  ->where( ($this->table .'.white_label_id'), '=', $whitelabelID );
		if (is_array($parentID)) {
			if (count($parentID) > 0) {
				$q->whereIn( ($this->table .'.master_agent_parent_id'), $parentID );
			}
		} else {
			if ($parentID >= 0) {
				$q->where( ($this->table .'.master_agent_parent_id'), '=', $parentID );
			}
		}
		$q->where( function ($q)  {
			$strname = "";
			for ($i = 0; $i < count($this->arrParamSearch); $i++) {
				$q->orWhere( ($this->table.".username"), "like", ("%".$this->arrParamSearch[$i]."%") );
				if ($this->searchType == "PHONENUMBER") {
					$q->orWhere( ($this->tableprofile.".phone_number"), "=", DB::raw($this->arrParamSearch[$i]) )
					->orWhere( ($this->tableprofile.".contact_number"), "=", DB::raw($this->arrParamSearch[$i]) )
					->orWhere( ($this->tableprofile.".fax_number"), "=", DB::raw($this->arrParamSearch[$i]) );
				} else if ($this->searchType == "EMAIL") {
					$q->orWhere( ($this->tableprofile.".email"), "like", ("%".$this->arrParamSearch[$i]."%") );
				} else {
					$q->orWhere( ($this->tableprofile.".fullname"), "like", ("%".$this->arrParamSearch[$i]."%") )
					->orWhere( ($this->tableprofile.".province_name"), "like", ("%".$this->arrParamSearch[$i]."%") )
					->orWhere( ($this->tableprofile.".city_name"), "like", ("%".$this->arrParamSearch[$i]."%") );
					$strname .= $this->arrParamSearch[$i];
				}
			}
			if ($this->searchType == "GENERAL") {
				$q->orWhere( DB::raw("REPLACE(".($this->table.".username").",' ', '')"), "like", ("%".$strname."%") )
				->orWhere( DB::raw("REPLACE(".($this->tableprofile.".fullname").",' ', '')"), "like", ("%".$strname."%") )
				->orWhere( DB::raw("REPLACE(".($this->tableprofile.".province_name").",' ', '')"), "like", ("%".$strname."%") )
				->orWhere( DB::raw("REPLACE(".($this->tableprofile.".city_name").",' ', '')"), "like", ("%".$strname."%") );
			}
		});
		if ($active) {
			$q->where( $this->table . ".status", "=", self::STATUS_ACTIVE );
		} else {
			if (! empty($status)) {
				$status = strtoupper($status);
				$q->where( $this->table . ".status", "=", $status );
			}
		}
		$q->select( $column )
		  ->orderBy( DB::raw(self::COLUMN_ALIAS_PRIORITY), 'ASC' )
		  ->orderBy( DB::raw("fullname"), "ASC" )
		  ->orderBy( $this->table . ".username", "ASC" )
		  ->orderBy( $this->table . ".id", "ASC" )
		  ->skip( $startID )
		  ->take( $max );
		return $q;
	}
	
	public function scopeGetByUsername($q, $whitelabelID, $username, $active = false) {
		$username = strtolower(trim($username));
		$q->where('white_label_id', '=', $whitelabelID)->where('username', '=', $username);
		if ($active) {
			$q->where('status', '=', self::STATUS_ACTIVE);
		}
		return $q;
	}
	
	public function scopeListBalanceByWhiteLabel( $q, $whitelabelID, $status = "", $arrParent = array(), $page = 1, $max = 1000000, $maxPlusOne = true ) {
		if ($page < 1) {
			$page = 1;
		}
		if ($max < 1) {
			$max = 100;
		}
		$startID = ($page - 1) * $max;
		if ($maxPlusOne) {
			$max++;
		}
		if (is_null($arrParent) || ! is_array($arrParent)) {
			$arrParent = array();
		}
		$mTable = new WhiteLabel();
		$tablewhitelabel = $mTable->getTable();
		$this->tablewhitelabel = $tablewhitelabel;
		$mTable = new MasterAgentBalances();
		$tablebalance = $mTable->getTable();
		$this->tablebalance = $tablebalance;
		$mTable = new MasterAgentProfile();
		$tableprofile = $mTable->getTable();
		$tableprofileAlias = $tableprofile . "_alias";
		$column = array(
				($this->table . 		'.id'),
				($this->table . 		'.username'),
				($tableprofile . 		'.fullname'),
				($this->table . 		'.status'),
				DB::raw("IF(".$tablebalance.".balance_type_id IS NULL,'N','Y') AS ever_login"),
				($this->table . 		'.paymode'),
				DB::raw("IFNULL(".$tablebalance.".balance_type_id,IF(".$this->table.".paymode = '".self::PAYMODE_PREPAID."', ".$tablewhitelabel.".default_prepaid_balance_type_id, ".$tablewhitelabel.".default_postpaid_balance_type_id)) AS balance_type_id"),
				DB::raw("IFNULL(".$tablebalance.".balance_value,0) AS balance_value"),
				DB::raw("IFNULL(".$tablebalance.".is_blocked,'N') AS is_blocked"),
				($this->table . 		'.master_agent_parent_id'),
				DB::raw($tableprofileAlias.".fullname AS master_agent_fullname"),
		);
		$q->join( $tablewhitelabel, $tablewhitelabel.".id", "=", $this->table.".white_label_id" )
		  ->leftJoin( $tablebalance, function ($join) {
			$join->on( $this->tablebalance. ".master_agent_id", "=", DB::raw($this->table . ".id") )
			 	 ->where( $this->tablebalance . ".balance_type_id", "=", DB::raw("IF(".$this->table.".paymode = '".self::PAYMODE_PREPAID."', ".$this->tablewhitelabel.".default_prepaid_balance_type_id, ".$this->tablewhitelabel.".default_postpaid_balance_type_id)") );
		  } )
		  ->leftJoin( $tableprofile, $tableprofile.".master_agent_id", "=", $this->table.".id" )
		  ->leftJoin( DB::raw($tableprofile." AS ".$tableprofileAlias), DB::raw($tableprofileAlias.".master_agent_id"), "=", $this->table.".master_agent_parent_id" )
		  ->where( $this->table.".white_label_id", "=", $whitelabelID);
		if (! empty($status)) {
			if (is_string($status)) {
				$q->where( $this->table . '.status', '=', $status );
			} else if (is_array($status)) {
				$q->whereIn( $this->table . '.status', $status );
			}
		}
		if (count($arrParent) > 0) {
			$q->whereIn( $this->table . '.master_agent_parent_id', $arrParent );
		}
		$q->select( $column )
		  ->orderBy( $this->table . '.master_agent_parent_id', 'ASC' )
		  ->orderBy( $this->table . '.id', 'ASC' )
		  ->skip( $startID )
		  ->take( $max );
		return $q;
	}
	
}