<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportCommissionMonthly extends Model
{
    protected $table = 'befintechreport.report_commission_master_agent_monthly';
    
    const PARTNER_PAYMODE_PREPAID = 'PREPAID';
    const PARTNER_PAYMODE_POSTPAID = 'POSTPAID';
        
}
