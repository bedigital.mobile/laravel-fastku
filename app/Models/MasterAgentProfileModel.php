<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterAgentProfileModel extends Model
{
    protected $table = 'master_agent_profile';
}
