<?php
namespace App\Models\Database;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Util\CommonUtils;

class PushConfig extends Model
{
	protected $table = 'befintech.push_config';
	private $filter;	

	public function __construct( ) {

	}

	public function scopeList( $query, $active = true ) {
		if ( $active ) {
			return $query->where('active','Y');
		} else {
			return $query->where('active','N');
		}		    
	}

	public function scopeGetById($query, $id) {
		return $query
			->where('id',$id);
	}
	
}