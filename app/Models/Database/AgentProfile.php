<?php
namespace App\Models\Database;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Database\Agent;

class AgentProfile extends Model
{
	protected $table = 'agent_profile';

	const FIELD_AGENT_ID = 'agent_id';
	const FIELD_FULLNAME = 'fullname';
	const FIELD_FIRSTNAME = 'firstname';
	const FIELD_MIDDLENAME = 'middlename';
	const FIELD_LASTNAME = 'lastname';
	const FIELD_DESCRIPTION = 'description';
	const FIELD_PHOTO_FILENAME = 'photo_filename';
	const FIELD_EMAIL = 'email';
	const FIELD_EMAIL_VERIFIED_AT = 'email_verified_at';
	const FIELD_EMAIL_VERIFIED_BY = 'email_verified_by';
	const FIELD_PHONE_NUMBER = 'phone_number';
	const FIELD_PHONE_NUMBER_VERIFIED_AT = 'phone_number_verified_at';
	const FIELD_PHONE_NUMBER_VERIFIED_BY = 'phone_number_verified_by';
	const FIELD_FAX_NUMBER = 'fax_number';
	const FIELD_FAX_NUMBER_VERIFIED_AT = 'fax_number_verified_at';
	const FIELD_FAX_NUMBER_VERIFIED_BY = 'fax_number_verified_by';
	const FIELD_PLACE_BIRTH = 'place_birth';
	const FIELD_BIRTH_DATE = 'birth_date';
	const FIELD_GENDER = 'gender';
	const FIELD_NPWP_NUMBER = 'npwp_number';
	const FIELD_ADDRESS = 'address';
	const FIELD_VILLAGE = 'village';
	const FIELD_DISTRICT = 'district';
	const FIELD_CITY = 'city';
	const FIELD_PROVINCE = 'province';
	const FIELD_POSTAL_CODE = 'postal_code';
	const FIELD_JOB_TITLE = 'job_title';
	const FIELD_ID_CARD_TYPE = 'id_card_type';
	const FIELD_ID_CARD_NUMBER = 'id_card_number';
	const FIELD_ID_CARD_FILENAME = 'id_card_filename';
	const FIELD_ID_CARD_ORIGINALNAME = 'id_card_originalname';
	const FIELD_ID_CARD_UPLOADED_AT = 'id_card_uploaded_at';
	const FIELD_ID_CARD_UPLOADED_BY = 'id_card_uploaded_by';
	const FIELD_VERIFIED_AT = 'verified_at';
	const FIELD_VERIFIED_BY = 'verified_by';
	const FIELD_CREATED_BY = 'created_by';
	const FIELD_CREATED_AT = 'created_at';
	const FIELD_UPDATED_AT = 'updated_at';

	private $whitelabelID = 0;
	private $tableagent = "";
	
	public function scopeGetByAgentId($query, $agentId) {
		return $query
			->where('agent_id',$agentId);
	}
	
	public function scopeSearchByEmail( $q, $whitelabelID, $email ) {
		$this->whitelabelID = (int)$whitelabelID;
		$mTable = new Agent();
		$tableagent = $mTable->getTable();
		$this->tableagent = $tableagent;
		$column = array(
				$tableagent .	'.id',
				$this->table . 	'.agent_id',
				$tableagent . 	'.white_label_id',
				$tableagent . 	'.status',
				$this->table . 	'.email',
				$this->table . 	'.email_verified_at',
				$this->table . 	'.phone_number',
				$this->table . 	'.phone_number_verified_at'
		);
		$q->join( $tableagent, function ($join) {
			$join->on( $this->tableagent . ".id", "=", DB::raw($this->table.'.agent_id') )
			->where( $this->tableagent . ".white_label_id", "=", DB::raw($this->whitelabelID) );
		  })
		  ->where( $this->table . ".email", "=", $email )
		  ->select( $column )
		  ->orderBy( $tableagent . ".id", "ASC" );
	}
	
	public function scopeSearchByPhone( $q, $whitelabelID, $phone) {
		$this->whitelabelID = (int)$whitelabelID;
		$mTable = new Agent();
		$tableagent = $mTable->getTable();
		$this->tableagent = $tableagent;
		$column = array(
				$tableagent .	'.id',
				$this->table . 	'.agent_id',
				$tableagent . 	'.white_label_id',
				$tableagent . 	'.status',
				$this->table . 	'.email',
				$this->table . 	'.email_verified_at',
				$this->table . 	'.phone_number',
				$this->table . 	'.phone_number_verified_at'
		);
		$q->join( $tableagent, function ($join) {
			$join->on( $this->tableagent . ".id", "=", DB::raw($this->table.'.agent_id') )
				 ->where( $this->tableagent . ".white_label_id", "=", DB::raw($this->whitelabelID) );
		  })
		  ->where( $this->table . ".phone_number", "=", $phone )
		  ->select( $column )
		  ->orderBy( $tableagent . ".id", "ASC" );
	}
	
}