<?php
namespace App\Models\Database;

use Illuminate\Database\Eloquent\Model;

class WhitelabelSMSGateways  extends Model
{
	protected $table = 'white_label_sms_gateways';

}