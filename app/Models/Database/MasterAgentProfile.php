<?php
namespace App\Models\Database;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Database\MasterAgent;

class MasterAgentProfile extends Model
{
	protected $table = 'master_agent_profile';

	const FIELD_MASTER_AGENT_ID = 'master_agent_id';
	const FIELD_PROFILE_TYPE = 'profile_type';	
	const FIELD_FULLNAME = 'fullname';
	const FIELD_FIRSTNAME = 'firstname';
	const FIELD_MIDDLENAME = 'middlename';
	const FIELD_LASTNAME = 'lastname';
	const FIELD_DESCRIPTION = 'description';
	const FIELD_PHOTO_FILENAME = 'photo_filename';
	const FIELD_EMAIL = 'email';
	const FIELD_EMAIL_VERIFIED_AT = 'email_verified_at';
	const FIELD_EMAIL_VERIFIED_BY = 'email_verified_by';
	const FIELD_PHONE_NUMBER = 'phone_number';
	const FIELD_PHONE_NUMBER_VERIFIED_AT = 'phone_number_verified_at';
	const FIELD_PHONE_NUMBER_VERIFIED_BY = 'phone_number_verified_by';
	const FIELD_FAX_NUMBER = 'fax_number';
	const FIELD_FAX_NUMBER_VERIFIED_AT = 'fax_number_verified_at';
	const FIELD_FAX_NUMBER_VERIFIED_BY = 'fax_number_verified_by';
	const FIELD_PLACE_BIRTH = 'place_birth';
	const FIELD_BIRTH_DATE = 'birth_date';
	const FIELD_GENDER = 'gender';
	const FIELD_NPWP_NUMBER = 'npwp_number';
	const FIELD_ADDRESS = 'address';
	const FIELD_VILLAGE = 'village';
	const FIELD_DISTRICT = 'district';
	const FIELD_CITY = 'city';
	const FIELD_PROVINCE = 'province';
	const FIELD_POSTAL_CODE = 'postal_code';
	const FIELD_COMPANY_NAME = 'company_name';
	const FIELD_DEPARTMENT = 'department';
	const FIELD_CONTACT_PERSON = 'contact_person';
	const FIELD_BRANCH_OFFICE_LEVEL = 'branch_office_level';
	const FIELD_VERIFIED_AT = 'verified_at';
	const FIELD_VERIFIED_BY = 'verified_by';
	const FIELD_CREATED_BY = 'created_by';
	const FIELD_CREATED_AT = 'created_at';
	const FIELD_UPDATED_AT = 'updated_at';

	const PROFILE_TYPE_PERSONAL = "PERSONAL";
	const PROFILE_TYPE_CORPORATE = "CORPORATE";
	
	private $whitelabelID = 0;
	private $tablemasteragent = "";
	
	public function scopeGetByMasterAgentId($query, $masterAgentId) {
		return $query
			->where('master_agent_id',$masterAgentId);
	}

	public function scopeSearchByEmail( $q, $whitelabelID, $email ) {
		$this->whitelabelID = (int)$whitelabelID;
		$mTable = new MasterAgent();
		$tableagent = $mTable->getTable();
		$this->tablemasteragent = $tablemasteragent;
		$column = array(
				$tablemasteragent .	'.id',
				$this->table . 		'.master_agent_id',
				$tablemasteragent .	'.white_label_id',
				$tablemasteragent .	'.status',
				$this->table . 		'.email',
				$this->table . 		'.email_verified_at',
				$this->table . 		'.phone_number',
				$this->table . 		'.phone_number_verified_at'
		);
		$q->join( $tablemasteragent, function ($join) {
			$join->on( $this->tablemasteragent . ".id", "=", DB::raw($this->table.'.master_agent_id') )
				 ->where( $this->tablemasteragent . ".white_label_id", "=", DB::raw($this->whitelabelID) );
		})
		->where( $this->table . ".email", "=", $email )
		->select( $column )
		->orderBy( $tableagent . ".id", "ASC" );
	}
	
	public function scopeSearchByPhone( $q, $whitelabelID, $phone) {
		$this->whitelabelID = (int)$whitelabelID;
		$mTable = new MasterAgent();
		$tablemasteragent = $mTable->getTable();
		$this->tablemasteragent = $tablemasteragent;
		$column = array(
				$tablemasteragent .	'.id',
				$this->table . 		'.master_agent_id',
				$tablemasteragent .	'.white_label_id',
				$tablemasteragent . '.status',
				$this->table . 		'.email',
				$this->table . 		'.email_verified_at',
				$this->table . 		'.phone_number',
				$this->table . 		'.phone_number_verified_at'
		);
		$q->join( $tablemasteragent, function ($join) {
			$join->on( $this->tablemasteragent . ".id", "=", DB::raw($this->table.'.agent_id') )
				 ->where( $this->tablemasteragent . ".white_label_id", "=", DB::raw($this->whitelabelID) );
		})
		->where( $this->table . ".phone_number", "=", $phone )
		->select( $column )
		->orderBy( $tableagent . ".id", "ASC" );
	}
	
}