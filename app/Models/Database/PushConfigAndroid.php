<?php
namespace App\Models\Database;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Util\CommonUtils;

class PushConfigAndroid  extends Model
{
	protected $table = 'push_config_android';
	private $filter;	

	public function __construct( ) {

	}

	public function scopeList( $query, $active = true ) {
		if ( $active ) {
			return $query->where('active','Y');
		} else {
			return $query->where('active','N');
		}		    
	}

	public function scopeGetById($query, $id) {
		return $query
			->where('id',$id);
	}
	
}