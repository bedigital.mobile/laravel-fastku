<?php
namespace App\Models\Database;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Database\Agent;

class AgentWorkstation extends Model
{
	protected $table = 'agent_workstations'; 
}