<?php
namespace App\Models\Database;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Database\WhitelabelSMSGateways;

class SMSGateway extends Model
{
	const PROVIDER_ADSMEDIA = 'ADSMEDIA';

	protected $table = 'sms_gateway';

	private $tablewlsmsgty = "";
	private $whitelabelID = 0;
	private $active = false;
		
	public function scopeListByWhiteLabel( $q, $whitelabelID, $mode = "SENDER", $active = true) {
		$this->whitelabelID = $whitelabelID;
		$this->active = $active;
		$mTable = new WhitelabelSMSGateways();
		$tablewlsmsgty = $mTable->getTable();
		$this->tablewlsmsgty = $tablewlsmsgty;
		$column = array(
				DB::raw($tablewlsmsgty . '.id AS row_id'),
				($this->table . 		'.id'),
				($tablewlsmsgty . 		'.sms_gateway_id'),
				($this->table . 		'.gateway_name'),
				($this->table . 		'.gateway_provider'),
				($this->table . 		'.is_sender'),
				($this->table . 		'.is_receiver'),
				($this->table . 		'.protocol'),
				($this->table . 		'.ip_hostname'),
				($this->table . 		'.port'),
				($this->table . 		'.url_endpoint'),
				($this->table . 		'.is_production'),
				($this->table . 		'.total_balance_latest'),
				($this->table . 		'.description'),
				($tablewlsmsgty . 		'.white_label_id'),
				($tablewlsmsgty . 		'.priority'),
				($tablewlsmsgty . 		'.active')
		);
		$q->join( $tablewlsmsgty, function ($join) {
				$join->on( $this->tablewlsmsgty . ".white_label_id", "=", DB::raw($this->whitelabelID) )
					 ->where( $this->tablewlsmsgty . ".sms_gateway_id", "=", DB::raw($this->table . ".id") );
				if ($this->active) {
					$join->where( $this->tablewlsmsgty . ".active", "=", "Y" );
				}
		  });
		if ($mode == "SENDER") {
			$q->where( $this->table . '.is_sender', '=', 'Y' );
		} else if ($mode == "RECEIVER") {
			$q->where( $this->table . '.is_receiver', '=', 'Y' );
		}
		$q->where( $this->table . '.active', '=', 'Y' )
		  ->orderBy(  $tablewlsmsgty .'.priority', 'ASC' )
		  ->orderBy(  $this->table .'.id', 'ASC' );
	} 
	
}