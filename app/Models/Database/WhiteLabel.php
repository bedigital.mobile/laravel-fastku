<?php
namespace App\Models\Database;

use Illuminate\Database\Eloquent\Model;

class WhiteLabel extends Model
{
	protected $table = 'white_label';

	public function scopeActive($q, $id) {
		return $q->where( "id", "=", $id )
				->where( "active", "=", "Y");
	}

}