<?php
namespace App\Models\Database;

use Illuminate\Database\Eloquent\Model;

class SMSGatewayConfig extends Model
{
	protected $table = 'sms_gateway_config';

}