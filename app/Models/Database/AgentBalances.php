<?php
namespace App\Models\Database;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Database\BalanceType;
use App\Models\Database\WhitelabelBalanceTypes;

class AgentBalances extends Model
{
	protected $table = 'agent_balances';
	protected $fillable = [ 'agent_id', 'balance_type_id' ];
	
	private $tablebalctype = "";
	private $tablewlbalctype = "";
	private $whitelabelID = 0;
	private $payMode = "";
	private $active = false;
	
	public function scopeListByAgent( $q, $whitelabelID, $agentID, $payMode, $defaultTypeID, $balcTypeID = 0, $page = 1, $max = 100, $maxPlusOne = true, $active = true ) {
		if ($page < 1) {
			$page = 1;
		}
		if ($max < 1) {
			$max = 100;
		}
		$startID = ($page - 1) * $max;
		if ($maxPlusOne) {
			$max++;
		}
		$defaultTypeID = (int)$defaultTypeID;
		$this->whitelabelID = $whitelabelID;
		$this->payMode = $payMode;
		$this->active = $active;
		$mTable = new BalanceType();
		$tablebalctype = $mTable->getTable();
		$this->tablebalctype = $tablebalctype;
		$mTable = new WhitelabelBalanceTypes();
		$tablewlbalctype = $mTable->getTable();
		$this->tablewlbalctype = $tablewlbalctype;
		$column = array(
				DB::raw($this->table . '.id AS row_id'),
				($this->table . 		'.agent_id'),
				($tablebalctype . 		'.id'),
				($this->table . 		'.balance_type_id'),
				($tablebalctype . 		'.balance_name'),
				($tablewlbalctype .		'.balance_name_alias'),
				($tablebalctype . 		'.paymode'),
				($tablebalctype . 		'.currency_code'),
				($this->table . 		'.balance_value'),
				($this->table . 		'.is_blocked'),
				($tablebalctype . 		'.description'),
				($tablewlbalctype .		'.active'),
				DB::raw('IF('.$this->table . '.balance_type_id = '.$defaultTypeID.', 1, 2) AS priority')
		);
		$q->join( $tablebalctype, function ($join) {
				$join->on( $this->tablebalctype . ".id", "=", DB::raw($this->table. ".balance_type_id") );
				if ($this->payMode == BalanceType::PAYMODE_POSTPAID) {
					$join->where( $this->tablebalctype . ".id", ">=", DB::raw(BalanceType::TYPE_POSTPAID_MIN) )
						 ->where( $this->tablebalctype . ".id", "<=", DB::raw(BalanceType::TYPE_POSTPAID_MAX) )
						 ->where( $this->tablebalctype . ".paymode", "=", BalanceType::PAYMODE_POSTPAID );
				} else if ($this->payMode == BalanceType::PAYMODE_PREPAID) {
					$join->where( $this->tablebalctype . ".id", ">=", DB::raw(BalanceType::TYPE_PREPAID_MIN) )
						 ->where( $this->tablebalctype . ".id", "<=", DB::raw(BalanceType::TYPE_PREPAID_MAX) )
						 ->where( $this->tablebalctype . ".paymode", "=", BalanceType::PAYMODE_PREPAID );
				} else if ($this->payMode == BalanceType::PAYMODE_WALLET) {
					$join->where( $this->tablebalctype . ".id", ">=", DB::raw(BalanceType::TYPE_WALLET_MIN) )
						 ->where( $this->tablebalctype . ".id", "<=", DB::raw(BalanceType::TYPE_WALLET_MAX) )
						 ->where( $this->tablebalctype . ".paymode", "=", BalanceType::PAYMODE_WALLET );
				}
				$join->where( $this->tablebalctype . ".active", "=", "Y" );
		  })
		  ->join( $tablewlbalctype, function ($join) {
				$join->on( $this->tablewlbalctype . ".white_label_id", "=", DB::raw((int)$this->whitelabelID) )
					 ->where( $this->tablewlbalctype . ".balance_type_id", "=", DB::raw($this->table . ".balance_type_id") );
				if ($this->active) {
					$join->where( $this->tablewlbalctype. ".active", "=", "Y" );
				}
		  })
		  ->where( $this->table . ".agent_id", "=", DB::raw((int)$agentID) );
		if (! empty($balcTypeID)) {
			$q->where( $this->table . ".balance_type_id", "=", DB::raw((int)$balcTypeID) );
		}
		$q->select( $column )
		  ->orderBy( DB::raw("priority"), "ASC" )
		  ->orderBy( $tablewlbalctype . ".show_order", "ASC" )
		  ->orderBy( $this->table . ".balance_type_id", "ASC" )
		  ->skip( $startID )
		  ->take( $max );
		return $q;
	}
	
}