<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Util\UtilCommon;
use App\Core\Util\UtilEmail;
use App\Core\Util\UtilPhone;
use App\Models\Database\AgentProfile;
use App\Models\Database\AgentBalances;
use App\Models\Database\MasterAgentProfile;
use App\Models\Database\WhiteLabel;

class AgentAntv extends Model
{
    protected $table = 'befintechlog.data_username_antv';
}
