<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerServiceModel extends Model
{
    protected $table = 'partner_services';
}
