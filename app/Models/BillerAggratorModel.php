<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillerAggratorModel extends Model
{
    protected $table = 'biller_aggregator';
}
