<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogUserLogin extends Model
{ 
    protected $table = 'befintechlog.log_user_login';
}
