<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WhiteLabelUserIP extends Model
{
    protected $table = 'white_label_admin_user_ip_whitelist';
}
