<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WhiteLabelProductModel extends Model
{
    protected $table = 'white_label_products';
}
