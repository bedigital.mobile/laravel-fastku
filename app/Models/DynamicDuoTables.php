<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\BindsDynamically;

class DynamicDuoTables extends Model
{
   use BindsDynamically;
}

 