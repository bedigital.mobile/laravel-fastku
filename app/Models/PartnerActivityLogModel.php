<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerActivityLogModel extends Model
{
    protected $table = 'beh2hlog.partner_activities';
}
