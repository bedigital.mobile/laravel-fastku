<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerProductBreakupModel extends Model
{
    protected $table = 'partner_products_breakup';
}
