<?php
namespace App\SystemLogic\Push;

use Log;
use Exception;
use App\Models\Database\Push;
use App\Models\Database\PushConfig;

class PushSL {

	private $platform = '';
	private $protocol = '';

	public function __construct( $platform, $protocol) {
		Log::info('PushSL->__construct: constructing....');
		$this->platform = $platform;
		$this->protocol = $protocol;
	}

	public function getConfig() {
		$config = array();
		try {
			Log::info('PushSL->getConfig: looking for config for '.$this->platform.' platform with '.
				$this->protocol.' protocol.');
			$push = Push::where('platform',$this->platform)->where('protocol',$this->protocol)->where('active','Y')->first();
			if (!is_null($push)) {
				$pushConfig = PushConfig::where('push_id',$push->id)->where('active','Y')->get();
				foreach ($pushConfig as $configItem) {
					$config[$configItem->config_name] = $configItem->config_value;
				}
			} else {
				//Push platform is undefined
				Log::info('PushSL->getConfig: config not found.');
			}
		} catch(Exception $e) {
			Log::error("PushSL->getConfig: exception at [Line=".$e->getLine().",Code=".$e->getCode()."]:".$e->getMessage());
		}
		Log::info('PushSL->getConfig: config='.json_encode($config));
		return $config;
	}

} // End of class