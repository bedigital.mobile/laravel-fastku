<?php
namespace App\SystemLogic\SMS;

use Log;
use DB;
use Config;
use Exception;
use App\BusinessLogic\Log\LogSMSSendBL;
use App\Core\CacheData\Simple\CacheSMSGatewayBalance;
use App\Core\SMS\SMSSenderAbstract;
use App\Core\SMS\SMSSenderAdsmedia;
use App\Jobs\Log\InsertLogSMSSendJob;
use App\Jobs\Log\UpdateLogSMSDeliveryStatusJob;
use App\Models\Database\SMSGateway;
use App\Models\Database\SMSGatewayConfig;
use App\Models\Database\LogSMSSend;
use App\Models\Database\WhitelabelSMSGateways;
use App\BusinessLogic\helpdeskBL;

class SMSSenderManager extends SMSSenderAbstract
{
	private $smsSender = null;
	private $mGateway = null;
	private $numberSerder = null;
	private $purposeType = "";
	private $username = "";

	public function __construct( $whitelabelID ) {
		try {
			$this->initSMSSender($whitelabelID);
		} catch ( Exception $e ) {
			Log::info("SMSSenderManager::__construct white_label_id=".$whitelabelID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
	private function initSMSSender( $whitelabelID ) {
		try {
			$this->mGateway = SMSGateway::ListByWhiteLabel( $whitelabelID, "SENDER" )->firstOrFail();
			if (! is_null($this->mGateway) && ! empty($this->mGateway->id)) {
				if ($this->mGateway->gateway_provider == SMSGateway::PROVIDER_ADSMEDIA) {
					$typeSMS = "";
					$apiKey = "";
					$urlCallback = "";
					$this->getConfigAdsmedia( (! empty($this->mGateway->sms_gateway_id) ? $this->mGateway->sms_gateway_id : $this->mGateway->id ), $typeSMS, $apiKey, $urlCallback, $this->numberSerder );
					$this->smsSender = new SMSSenderAdsmedia( $this->mGateway->url_endpoint, $typeSMS, $apiKey, $urlCallback );
				}
			}
		} catch ( Exception $e ) {
			$this->mGateway = null;
			Log::info("SMSSenderManager::initSMSSender white_label_id=".$whitelabelID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
	private function getConfigAdsmedia( $id, &$typeSMS, &$apiKey, &$urlCallback, &$numberSerder ) {
		try {
			$mList = SMSGatewayConfig::where( 'sms_gateway_id', '=', $id )->where( 'active', '=', 'Y' )->get();
			foreach ($mList as $row) {
				if ($row->config_name == "APIKEY") {
					$apiKey = $row->config_value;
				} else if ($row->config_name == "SMSTYPE") {
					$typeSMS = $row->config_value;
				} else if ($row->config_name == "URLCALLBACKSTATUS") {
					$urlCallback = $row->config_value;
				} else if ($row->config_name == "NUMBERSENDER") {
					$numberSerder = $row->config_value;
				}
			}
		} catch ( Exception $e ) {
			Log::info("SMSSenderManager::getConfigAdsmedia id=".$id." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
	public function setPurposeType( $v ) {
		$this->purposeType = $v;
		return $this;
	}
	
	public function getErrorCode() {
		return (! is_null($this->smsSender) ? $this->smsSender->getErrorCode() : 9999);
	}
	
	public function getErrorMessage() {
		return (! is_null($this->smsSender) ? $this->smsSender->getErrorMessage() : "");
	}
	
	public function getResultOriginal( $useArray = true ) {
		return (! is_null($this->smsSender) ? $this->smsSender->getResultOriginal( $useArray ) : ($useArray ? array() : ""));
	}
	
	public function isSuccess() {
		return (! is_null($this->smsSender) ? $this->smsSender->isSuccess() : false);
	}
	
	public function send( $to, $msg, $sendingAt = "" ) {
		$result = array();
		if (! is_null($this->smsSender)) {
			try {
				$allowed = true;
				if (! is_null($this->mGateway) && ! empty($this->mGateway->id)) {
					$chace = new CacheSMSGatewayBalance();
					if (! $chace->hasGetBalance( ! empty($this->mGateway->sms_gateway_id) ? $this->mGateway->sms_gateway_id : $this->mGateway->id) ) {
						$balance = 0;
						$expired = "";
						if ($this->getBalance($balance, $expired)) {
							if ($balance > 0 && (empty($expired) || $expired >= date('Y-m-d'))) {
								$allowed = true;
							}
							try {
								$mTable = new SMSGateway();
								DB::table( $mTable->getTable() )->where('id', '=', ! empty($this->mGateway->sms_gateway_id) ? $this->mGateway->sms_gateway_id : $this->mGateway->id)
										->update( [
											'total_balance_latest' => $balance,
											'balance_expired' => (! empty($expired) ? $expired : null)
										] );
							} catch ( Exception $e ) {
								Log::info("MSSenderManager::send do_update_balance exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
							}
						}
					}
				}
				if ($allowed) {
					$result = $this->smsSender->send($to, $msg, $sendingAt);
							log::info(" send sms to".$to);
					if ($this->isSuccess() && ! is_null($result) && is_array($result)) {
						if (count($result) > 0) {
							$this->putQueueLog( $msg, $result );


						}
					}
				}
			} catch ( Exception $e ) {
				Log::info("SMSSenderManager::send exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
		}
		return $result;
	}
	
	public function getBalance( &$balance, &$expired ) {
		$ret = (! is_null($this->smsSender) ? $this->smsSender->getBalance( $balance, $expired ) : false);
		if ($ret) {
			try {
				$chace = new CacheSMSGatewayBalance();
				$chace->setFlagHasGetBalance( ! empty($this->mGateway->sms_gateway_id) ? $this->mGateway->sms_gateway_id : $this->mGateway->id );
			} catch ( Exception $e ) {
				Log::info("SMSSenderManager::getBalance exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
			}
		}
		return $ret;
	}
	
	private function putQueueLog( $msg, $result ) {
		log::info(" putQueueLog sms send START");
		try {
			$gtwID = (! empty($this->mGateway->sms_gateway_id) ? $this->mGateway->sms_gateway_id : $this->mGateway->id);
			log::info("gtwID = ".json_encode($gtwID));
			$input = LogSMSSendBL::createInput( $gtwID, $this->username, $msg, $result, $this->purposeType );

			$logBL = new LogSMSSendBL( $input );
			$logBL->setLogDateTime();
			$queuename = "deskma-worker0";
			$input = $logBL->getInput();
			log::info(" putQueueLog input =".json_encode($input));
			$job = new InsertLogSMSSendJob( $input );
			if (! empty($queuename)) {
				$job->onQueue($queuename);
							log::info(" InsertLogSMSSendJob quee");

			}
			dispatch( $job );
			return true;
		} catch ( Exception $e ) {
			Log::info("SMSSenderManager::putQueueLog exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public static function putQueueUpdateSMSStatus( $provider, $input ) {
		try {
			$job = new UpdateLogSMSDeliveryStatusJob( $provider, $input );
			$queuename = "deskma-worker1";
			if (! empty($queuename)) {
				$job->onQueue($queuename);
			}
			dispatch( $job );
			return true;
		} catch ( Exception $e ) {
			Log::info("SMSSenderManager::putQueueUpdateSMSStatus exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
	public static function updateSMSStatus( $provider, $input ) {
		try {
			if ($provider == SMSGateway::PROVIDER_ADSMEDIA) {
				$arrStatus = SMSSenderAdsmedia::convertDeliveryStatus( $input );
				if (count($arrStatus) > 0) {
					$arrSMSGtyID = array();
					try {
						$mList = SMSGateway::where( 'gateway_provider', '=', SMSGateway::PROVIDER_ADSMEDIA )->where( 'is_sender', '=', 'Y' )->where( 'active', '=', 'Y' )->select( 'id' )->get();
						foreach ($mList as $row) {
							$arrSMSGtyID[] = $row->id;
						}
					} catch ( Exception $e ) {
						Log::info("SMSSenderManager::updateSMSStatus exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
					}
					return LogSMSSendBL::updateDeliveryStatus($arrSMSGtyID, $arrStatus);
				}
			}
		} catch ( Exception $e ) {
			Log::info("SMSSenderManager::updateSMSStatus exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return false;
	}
	
}