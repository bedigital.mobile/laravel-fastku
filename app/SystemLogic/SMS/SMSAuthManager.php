<?php
namespace App\SystemLogic\SMS;

use Log;
use Config;
use Exception;
use App\Core\SMS\SMSAuthAbstract;
use App\Core\SMS\SMSAuthAccountKitFB;
use App\Models\Database\SmsGatewayConfigFacebook;

class SMSAuthManager extends SMSAuthAbstract
{
	private $smsKit = null;
	
	public function __construct( $whitelabelID, $type = "FACEBOOK" ) {
		try {
			if ($type == "FACEBOOK" || $type == "FACEBOOK_AUTHORIZATION" ) {
				$this->smsKit = new SMSAuthAccountKitFB();
				if ($type == "FACEBOOK_AUTHORIZATION") {
					$this->smsKit->setUseAuthorizationCode();
				}
				$mConfig = SmsGatewayConfigFacebook::where( 'white_label_id', '=', $whitelabelID )->where( 'active', '=', 'Y' )->firstOrFail();
				if (! is_null($mConfig) && ! empty($mConfig->id)) {
					$this->smsKit = new SMSAuthAccountKitFB( $mConfig->api_id, $mConfig->api_secret, $mConfig->api_version );
					if ($type == "FACEBOOK_AUTHORIZATION") {
						$this->smsKit->setUseAuthorizationCode();
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("SMSAuthManager::__construct white_label_id=".$whitelabelID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
	public function setConfig( $key, $value ) {
		return ( ! is_null($this->smsKit) ? $this->smsKit->setConfig($key, $value) : null );
	}
	public function getConfig() {
		return ( ! is_null($this->smsKit) ? $this->smsKit->getConfig() : null );
	}
	
	public function getLogString() {
		return ( ! is_null($this->smsKit) ? $this->smsKit->getLogString() : "" );
	}
	
	public function verifyAccessToken( $accessToken, $phonenumber ) {
		return ( ! is_null($this->smsKit) ? $this->smsKit->verifyAccessToken($accessToken, $phonenumber) : false );
	}
	
}