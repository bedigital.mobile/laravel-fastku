<?php
namespace App\SystemLogic\Auth;

use Log;
use Config;
use Exception;
use App\Core\Util\UtilCommon;
use App\Core\CacheData\Simple\CacheSignToken;

class TokenLogic
{
	protected $debug = false;
	protected $saltPrefix = "";
	protected $saltSuffix = "";
	protected $sessionID = "";
	
	private $data1 = "";
	private $data2 = "";
	private $data3 = "";
	
	const GRANUALITY_SECONDS = 10;
	const TOLERANCE_MINUTES = 60;
	
	public function __construct( $whitelabelID, $userID, $sessionID ) {
		try {
			$this->debug = boolval(Config::get( 'app.debug' ));
			$whilalabelIDBase36 = UtilCommon::idConvertToBase36( $whitelabelID );
			$userIDBase36 = UtilCommon::idConvertToBase36( $userID );
			$this->saltPrefix = strtoupper(substr($whilalabelIDBase36, -3));
			$this->saltSuffix = strtoupper(substr($userIDBase36, -3));
			$this->sessionID =  $sessionID;
			if ($this->debug) Log::info("TokenLogic::__construct white_label_id=".$whilalabelIDBase36." user_id=".$userIDBase36." salt_prefix=".$this->saltPrefix." salt_suffix=".$this->saltSuffix." session_id=".$this->sessionID);
		} catch ( Exception $e ) {
			Log::info("TokenLogic::__construct white_label_id=".$whitelabelID." user_id=".$userID." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
	}
	
	public function getSaltPrefix() {
		return $this->saltPrefix;
	}
	public function getSaltSuffix() {
		return $this->saltSuffix;
	}
	public function getData1() {
		return $this->data1;
	}
	public function getData2() {
		return $this->data2;
	}
	public function getData3() {
		return $this->data3;
	}
	
	public function generateSignToken() {
		return $this->createToken( $this->getCurrentTimeEpoch() );
	}
	
	public function generateSMSToken( $formatDigit = true ) {
		return $this->createToken( $this->getCurrentTimeEpoch(), true, $formatDigit );
	}
	
	public function isSignTokenValid( $token ) {
		return $this->isTokenValid( $token );
	}
	
	public function isSMSTokenValid( $token, $formatDigit = true ) {
		return $this->isTokenValid( $token, true, $formatDigit );
	}
	
	private function isTokenValid( $token, $isSMStoken = false, $formatDigit = true ) {
		try {
			$cache = new CacheSignToken();
			if ($cache->isUsed( $this->sessionID, $token )) {
				return false;
			}
			$curTM = $this->getCurrentTimeEpoch();
			if ($this->debug) Log::info("TokenLogic::isTokenValid sms=".($isSMStoken?"true":"false")." is_digit=".($formatDigit?"true":"false")." token=".$token." current_time_epoch=".$curTM);
			$granualityMinute = (int)(60 / self::GRANUALITY_SECONDS);
			for ($i = 0; $i < self::TOLERANCE_MINUTES; $i++) {
				if ($i == 0) {
					for ($tm = $curTM; $tm <= $curTM + ($granualityMinute * self::GRANUALITY_SECONDS); $tm += self::GRANUALITY_SECONDS) {
						// if ($this->debug) Log::info("TokenLogic::isTokenValid i=".$i." tm=".$tm);
						if ($token == $this->createToken($tm, $isSMStoken, $formatDigit)) {
							$cache->setUsed( $this->sessionID, $token );
							if ($this->debug) Log::info("TokenLogic::isTokenValid i=".$i." tm=".$tm." token=".$token." ==> VALID");
							return true;
						}
					}
				} else {
					for ($tm = $curTM - ($i * $granualityMinute * self::GRANUALITY_SECONDS); $tm < $curTM - (($i - 1) * $granualityMinute * self::GRANUALITY_SECONDS); $tm += self::GRANUALITY_SECONDS) {
						// if ($this->debug) Log::info("TokenLogic::isTokenValid A i=".$i." tm=".$tm);
						if ($token == $this->createToken($tm, $isSMStoken, $formatDigit)) {
							$cache->setUsed( $this->sessionID, $token );
							if ($this->debug) Log::info("TokenLogic::isTokenValid A i=".$i." tm=".$tm." token=".$token." ==> VALID");
							return true;
						}
					}
					for ($tm = $curTM + ($i * $granualityMinute * self::GRANUALITY_SECONDS) + self::GRANUALITY_SECONDS; $tm <= $curTM + (($i + 1) * $granualityMinute * self::GRANUALITY_SECONDS); $tm += self::GRANUALITY_SECONDS) {
						// if ($this->debug) Log::info("TokenLogic::isTokenValid B i=".$i." tm=".$tm);
						if ($token == $this->createToken($tm, $isSMStoken, $formatDigit)) {
							$cache->setUsed( $this->sessionID, $token );
							if ($this->debug) Log::info("TokenLogic::isTokenValid B i=".$i." tm=".$tm." token=".$token." ==> VALID");
							return true;
						}
					}
				}
			}
		} catch ( Exception $e ) {
			Log::info("TokenLogic::isTokenValid token=".$token." exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		if ($this->debug) Log::info("TokenLogic::isTokenValid token=".$token." ==> NOT VALID");
		return false;
	}
	
	private function getCurrentTimeEpoch() {
		$tm = 0;
		try {
			$tm = gmdate('U');
			$diffSecond = 0;
			if ( ($diffSecond = ($tm % self::GRANUALITY_SECONDS)) > 0) {
				$tm -= $diffSecond;
			}
		} catch ( Exception $e ) {
			Log::info("TokenLogic::getCurrentTimeEpoch exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $tm;
	}
	
	private function createToken( $tmEpoch, $isSMStoken = false, $formatDigit = true ) {
		$token = "";
		try {
			if ($isSMStoken) {
				$data = md5( $this->saltPrefix . $tmEpoch . $this->sessionID . $this->saltSuffix );
				if ($formatDigit) {
					$data = strtoupper(substr($data, 0, 5));
					$token = sprintf("%06d", UtilCommon::idConvertToInt($data));
					if (strlen($token) > 6) {
						$token = substr($token, -6);
					}
				} else {
					$token = strtoupper(substr($data, 0, 6));
				}
			} else {
				if ($this->debug) {
					$this->data1 = $tmEpoch;
					$this->data2 = $this->saltPrefix . $tmEpoch . $this->sessionID . $this->saltSuffix;
					$this->data3 = md5( $this->saltPrefix . $tmEpoch . $this->sessionID . $this->saltSuffix );
				}
				$token = hash('sha256', md5( $this->saltPrefix . $tmEpoch . $this->sessionID . $this->saltSuffix ));
			}
		} catch ( Exception $e ) {
			Log::info("TokenLogic::createSignToken exception=[L=".$e->getLine().",C=".$e->getCode()."]:".$e->getMessage());
		}
		return $token;
	}
	
}