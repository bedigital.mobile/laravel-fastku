<?php
return array (
	'cms' => [
		'production' => true, 
 		'api_web' => 'https://apiweb.posfin.id/api/helpdesk/trx/',
 
		'cms_type' => 'FASTKU',
		'status_list' => 'SUSPENDED;DEACTIVATED;UNAPPROVED;ACTIVE',
 
		'connectionalibaba' => 'mysql', 
		'connectionkurir' => 'mysqlkurir', 
		'connectiondata_stg' => 'mysql', 
		'connectionreport' => 'mysql', 
		'connectiondata' => 'mysql', 
		'connectionreport_prod' => 'mysql', 
		'connectiondata_prod' => 'mysql', 
		'connection111' => 'mysql',   
		'whitelabel' => 'KEZXFW',
		'whitelabelID' => '1234514444',
		'platform' => 'WEB',
		'appid' => '1.0',
		'hostcms' => 'cms.paketkupos.co.id',
	], 
);
 