var filenamephotobelakanggerai = "" ;
var filenamephotodepangerai = "";
var filenamephotodenahgerai = "";
var currentForm = "form1";
var isKeuntunganDisplayed = false;

window.onresize = showContent();
showContent();
showForm('form1');

function postData(viServiceURL, viData) {
	hideElm("action");
	$("infoproses").innerHTML = "Mohon menunggu...";
	var xhr = new XMLHttpRequest();
	xhr.open('POST', viServiceURL);
	xhr.onload = function() {
	    if (xhr.status === 200) {
	        var result = xhr.responseText;
	        console.log(result);
	        if (result === "OK") {

	        	var imgUploadServiceURL = '/paketkupos/submit-image';
	        	//First attempt : gagal 
	        	photodepangerai = $("lphotodepangerai").files[0];
				imgResizeAndUpload(photodepangerai, filenamephotodepangerai, imgUploadServiceURL);

	        	photobelakanggerai = $("lphotobelakanggerai").files[0];
				imgResizeAndUpload(photobelakanggerai, filenamephotobelakanggerai, imgUploadServiceURL);

				photodenahgerai = $("lphotodenahgerai").files[0];
				imgResizeAndUpload(photodenahgerai, filenamephotodenahgerai, imgUploadServiceURL);


				//Second attempt : biasanya berhasil, hah ini memang cara koboy :)	
	        	photodepangerai = $("lphotodepangerai").files[0];
				imgResizeAndUpload(photodepangerai, filenamephotodepangerai, imgUploadServiceURL);

	        	photobelakanggerai = $("lphotobelakanggerai").files[0];
				imgResizeAndUpload(photobelakanggerai, filenamephotobelakanggerai, imgUploadServiceURL);

				photodenahgerai = $("lphotodenahgerai").files[0];
				imgResizeAndUpload(photodenahgerai, filenamephotodenahgerai, imgUploadServiceURL);

				//alert("Pendaftaran telah berhasil.\nPaketkuPOS akan menghubungi Anda lebih lanjut.");	
				$("info").innerHTML = "Pendaftaran Paketku POS<br>BERHASIL<br><br>Admin kami akan segera<br>Menghubungi Anda";
				//hideElm("back"); 	        	
	        } else {
	        	//alert("Pendaftaran gagal.\n" + result);
	        	$("info").innerHTML = "Pendaftaran gagal.<br>" + result;
	        }
	        showElm("action");
	        $("infoproses").innerHTML = "&nbsp;";
	        showForm('formselesai');
	    }
	};
	xhr.send(viData);
}

function submitData() {
	var nama = $("lnama").value;
	var ektp = $("lektp").value;
	var npwp = $("lnpwp").value;
	var telp = $("ltelp").value;
	var email = $("lemail").value;
	var namagerai = $("lnamagerai").value;
	var alamatgerai = $("lalamatgerai").value;
	var ukurangerai = $("lukurangerai").value;
	var jaraksesamaagenpos = $("ljaraksesamaagenpos").value;
	var datapesaing = $("ldatapesaing").value;

	//Cek input
	if (nama.trim()==="") {
		alert ("Nama belum diisi");
		showForm('form1');
		$("lnama").focus();
		return false;
	}

	if (ektp.trim()==="") {
		alert ("EKTP belum diisi");
		showForm('form1');
		$("lektp").focus();
		return false;
	} else {
		if (!isNumeric(ektp)) {
			alert("EKTP harus numerik");
			showForm('form1');
			$("lektp").focus();
			return false;
		} else {
			if (ektp.length !== 16) {
				alert("EKTP harus numerik 16 digit");
				showForm('form1');
				$("lektp").focus();
				return false;
			}
		}
	}

	if (npwp.trim()==="") {
		alert ("NPWP belum diisi");
		showForm('form1');
		$("lnpwp").focus();
		return false;
	} else {
		if (!isNumeric(npwp)) {
			alert("NPWP harus numerik");
			showForm('form1');
			$("lnpwp").focus();
			return false;
		} else {
			if (npwp.length !== 15) {
				alert("NPWP harus numerik 15 digit");
				showForm('form1');
				$("lnpwp").focus();
				return false;
			}
		}
	}

	if (telp.trim()==="") {
		alert ("Telp belum diisi");
		showForm('form1');
		$("ltelp").focus();
		return false;
	}

	if (email.trim()==="") {
		alert ("Email belum diisi");
		showForm('form1');
		$("lemail").focus();
		return false;
	} else {
		if (!isValidEmail(email)) {
			alert("Email tidak valid");
			showForm('form1');
			$("lemail").focus();
			return false;
		}
	}

	if (namagerai.trim()==="") {
		alert ("Nama Gerai belum diisi");
		showForm('form2');
		$("lnamagerai").focus();
		return false;
	}

	if (alamatgerai.trim()==="") {
		alert ("Alamat Gerai belum diisi");
		showForm('form2');
		$("lalamatgerai").focus();
		return false;
	}

	if (ukurangerai.trim()==="") {
		alert ("Ukuran gerai belum diisi");
		showForm('form2');
		$("lukurangerai").focus();
		return false;
	}


	var photodepangerai = null;
	if ( $("lphotodepangerai").files.length > 0 ) {
		photodepangerai = $("lphotodepangerai").files[0];
	} else {
		alert("Photo depan gerai belum ada");
		showForm('form2');
		$("lphotodepangerai").focus();
		return false;
	}


	var photobelakanggerai = null;
	if ( $("lphotobelakanggerai").files.length > 0 ) {
		photobelakanggerai = $("lphotobelakanggerai").files[0];
	} else {
		alert("Photo belakang gerai belum ada");
		showForm('form2');
		$("lphotobelakanggerai").focus();
		return false;
	}

	var photodenahgerai = null;
	if ( $("lphotodenahgerai").files.length > 0 ) {
		photodenahgerai = $("lphotodenahgerai").files[0];
	} else {
		alert("Photo denah gerai belum ada");
		showForm('form2');
		$("lphotodenahgerai").focus();
		return false;
	}

	filenamephotobelakanggerai = "pblk_" + getCurrentDateTimeString() + '_' + makeID(5);
	filenamephotodepangerai = "pdpn_" + getCurrentDateTimeString() + '_' + makeID(5);
	filenamephotodenahgerai = "pdnh_" + getCurrentDateTimeString() + '_' + makeID(5);

	var formData = new FormData();
	formData.append('nama',dbFriendly(nama));
	formData.append('ektp',dbFriendly(ektp));
	formData.append('npwp',dbFriendly(npwp));
	formData.append('telp',dbFriendly(telp));
	formData.append('email',dbFriendly(email));
	formData.append('namagerai',dbFriendly(namagerai));
	formData.append('alamatgerai',dbFriendly(alamatgerai));
	formData.append('ukurangerai',dbFriendly(ukurangerai));
	formData.append("jaraksesamaagenpos", dbFriendly(jaraksesamaagenpos));
	formData.append('datapesaing',dbFriendly(datapesaing));
	formData.append('filenamephotobelakanggerai', dbFriendly(filenamephotobelakanggerai));
	formData.append('filenamephotodepangerai', dbFriendly(filenamephotodepangerai));
	formData.append('filenamephotodenahgerai', dbFriendly(filenamephotodenahgerai));

	console.log(formData);

	var serviceURL = "/paketkupos/submit-registrasi";
	postData(serviceURL, formData);
}

function showContent() {
	var windowWidth = getWindowWidth();
	var contentWidth = getDivWidth('content');
	
	if (contentWidth > 600) {
		contentWidth = 520; //500px (see max width limit in css) + 20 px padding 
	}
	var left = Math.round((windowWidth - contentWidth)/2);
	console.log("content width :" + contentWidth);
	if (windowWidth <= 600) {
		left = 0;
	}
	left = left + 'px';
	$('content').style.left = left;
	$('content').style.top = "10px";
	$('content').style.position = 'absolute';
	//console.log('window width:'+windowWidth);
	//console.log('content width:'+contentWidth);
	console.log('content left:'+left);
}

function hideAllForm() {
	try {
		hideElm('form1');
		hideElm('form2');
		hideElm('formselesai');
		hideElm('headerform1');
		hideElm('headerform2');
		hideElm('keuntungan');
	} catch(err) {
		//...
	}
}

function showForm(viID) {
	hideAllForm();
	try {
		showElm(viID);
		showElm('header'+viID);
	} catch(err) {
		console.log("Error displaying :"+viID);
	}
	if (viID !== "keuntungan") {
		currentForm = viID;	
	}
}

function showKeuntungan(){
	if (!isKeuntunganDisplayed) {
		var keuntunganWidth = getDivWidth('imgKeuntungan');
		keuntunganLeft = getLeft($('content')) + Math.round(( getDivWidth('content')- keuntunganWidth)/2);
		keuntunganLeft = keuntunganLeft - 10;
		keuntunganLeft = keuntunganLeft + 'px';

		console.log('displayed keuntungan left:' + keuntunganLeft);
		$('keuntungan').style.left = keuntunganLeft;
		$('keuntungan').style.position = 'absolute';
		showElm('keuntungan');
		isKeuntunganDisplayed = true;
		$('btnBerikutnya').disabled = true;
		$('btnKembali').disabled = true;
		$('btnDaftar').disabled = true;
	}
}

function hideKeuntungan(){
	hideElm('keuntungan');
    isKeuntunganDisplayed = false;
	$('btnBerikutnya').disabled = false;
	$('btnKembali').disabled = false;
	$('btnDaftar').disabled = false;

}



function showElm(viID) {
	$(viID).style.display = "block";
}

function hideElm(viID) {
	$(viID).style.display = "none";
}

function $(viID) {
	return document.getElementById(viID);
}

function getCurrentDateTimeString() {
	var date = new Date();
	return (
		date.getFullYear().toString() + 
		pad(date.getMonth() + 1) + 
		pad(date.getDate()) + 
		pad(date.getHours()) + 
		pad(date.getMinutes()) + 
		pad(date.getSeconds())
	);
}

function pad(n) { return n < 10 ? '0' + n : n }

function imgResizeAndUpload(file, filename, serviceURL) {
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        if (file) {

            var reader = new FileReader();
            // Set the image once loaded into file reader
            reader.onload = function(e) {

                var img = document.createElement("img");
                img.src = e.target.result;

                var canvas = document.createElement("canvas");
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);

                var MAX_WIDTH = 400;
                var MAX_HEIGHT = 400;
            var width = img.width;
                var height = img.height;

                if (width > height) {
                    if (width > MAX_WIDTH) {
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                    }
                } else {
                    if (height > MAX_HEIGHT) {
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                    }
                }
                canvas.width = width;
                canvas.height = height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0, width, height);

                var dataurl = canvas.toDataURL(file.type);
                console.log(dataurl);
                
                var formData = new FormData();
				formData.append('photo',dataurl);
				formData.append('filename', filename);
				genericPostData(serviceURL, formData);
                
                /*
                var resultBlob = blobCreationFromURL(dataurl);
                var resultFile = blobToFile(resultBlob, file.name);
                console.log(resultFile);
                return resultFile;
                return dataurl;
                */

            }
            reader.readAsDataURL(file);
        }

    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
}

function blobCreationFromURL(inputURI) { 

	var binaryVal; 

	// mime extension extraction 
	var inputMIME = inputURI.split(',')[0].split(':')[1].split(';')[0]; 

	// Extract remaining part of URL and convert it to binary value 
	if (inputURI.split(',')[0].indexOf('base64') >= 0) 
		binaryVal = atob(inputURI.split(',')[1]); 

	// Decoding of base64 encoded string 
	else
		binaryVal = unescape(inputURI.split(',')[1]); 

	// Computation of new string in which hexadecimal 
	// escape sequences are replaced by the character 
	// it represents 

	// Store the bytes of the string to a typed array 
	var blobArray = []; 
	for (var index = 0; index < binaryVal.length; index++) { 
		blobArray.push(binaryVal.charCodeAt(index)); 
	} 

	return new Blob([blobArray], { 
		type: inputMIME 
	}); 
} 

function blobToFile(theBlob, fileName){
    theBlob.lastModifiedDate = new Date();
    theBlob.name = fileName;
    return theBlob;
}

function genericPostData(viServiceURL, viData) {
	var xhr = new XMLHttpRequest();
	xhr.open('POST', viServiceURL);
	xhr.onload = function() {
	    if (xhr.status === 200) {
	        var result = xhr.responseText;
	        console.log(result);
	    }
	};
	xhr.send(viData);
}

function makeID(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

function getWindowWidth() {
  return Math.max(
    document.body.scrollWidth,
    document.documentElement.scrollWidth,
    document.body.offsetWidth,
    document.documentElement.offsetWidth,
    document.documentElement.clientWidth
  );
}

function getWindowHeight() {
  return Math.max(
    document.body.scrollHeight,
    document.documentElement.scrollHeight,
    document.body.offsetHeight,
    document.documentElement.offsetHeight,
    document.documentElement.clientHeight
  );
}

function getDivWidth(viID) {
  return $(viID).offsetWidth;	
}

function getLeft(el) {
    // yay readability
    for (var lx=0, ly=0;
         el != null;
         lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
    return ly;	
}

function getTop(el) {
    // yay readability
    for (var lx=0, ly=0;
         el != null;
         lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
    return lx;	
}


