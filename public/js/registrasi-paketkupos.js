var gformData = null;
var gnamaFilePhotoDepanGerai = '';
var gnamaFilePhotoDalamGerai = '';
var gnamaFilePhotoTandaTangan = '';
var gisDisplayKeuntungan = false;
var gisDisplayPetaLokasi = false;
var gLat = "0";
var gLong = "0";

const API_REG_URL = '/paketkupos/submit-registrasi';
const API_SAVE_IMG_URL = '/paketkupos/submit-image';
const API_GET_PROVINCE = '/paketkupos/get-province';
const API_GET_CITY = '/paketkupos/get-city';
const API_GET_DISTRICT = '/paketkupos/get-district';
const API_GET_VILLAGE = '/paketkupos/get-village';

window.addEventListener("resize",function(){
	//Repositioning popup
	repositionKeuntungan();
	repositionPetaLokasi();
},false);

document.addEventListener("DOMContentLoaded", function(){
	initPage();
});

function initPage() {
	hide('form2');
	hide('form2-info-upload');
	addEvent('btn-berikutnya','click',showForm2);
	addEvent('btn-kembali','click',showForm1);
	addEvent('btn-daftar','click',submitForm);
	addEvent('keuntungan','click',showHideKeuntungan);
	addEvent('footer','click',showHideKeuntungan);
	addEvent('lbl-perbaikan','click',perbaikan);
	addEvent('i-koordinat-lokasi','click',showPosition);
	getProvince();
	$('i-provinsi').onchange =  function() {
 		 getCity();
	}
	$('i-kota').onchange =  function() {
 		 getDistrict();
	}
	$('i-kecamatan').onchange =  function() {
 		 getVillage();
	}
	showHideKeuntungan();
	addMapEvent();
	showForm1();
}

function showForm2() {
	hide('form1');
	show('form2');
	$('content').style.height = "900px";
}

function showForm1() {
	hide('form2');
	$('content').style.height = "440px";
	show('form1');
}

function submitForm() {
	persetujuan = $('i-persetujuan').checked;	
	if (persetujuan) {
		if (getFormData()) {
			if (gformData !== null) {
				hide('form2-action-button');
				show('form2-info-upload');
				console.log('submiting form data');
				postData(API_REG_URL, gformData, uploadPhotoGerai);
			}
		}
	} else {
		alert("Anda harus memberikan persetujuan");
		showForm2();
		$('i-persetujuan').focus();
	}
}

function getFormData() {
	gformData = null;
	var nama = $("i-nama").value;
	var ektp = $("i-ektp").value;
	var npwp = $("i-npwp").value;
	var telp = $("i-telp").value;
	var email = $("i-email").value;
	var namaGerai = $("i-nama-gerai").value;
	var alamatGerai = $("i-alamat-gerai").value;
	var ukuranGerai = $("i-ukuran-gerai").value;
	var jarakSesamaAgenPos = $("i-jarak-sesama-agen-pos").value;
	var dataPesaing = $("i-data-pesaing").value;
	var namaKantorPosKabupatenKota = $('i-nama-kantor-pos-kabupaten-kota').value;
	var jarakKantorPosKabupatenKota = $('i-jarak-kantor-pos-kabupaten-kota').value;
	var namaKantorPosKecamatanKelurahan = $('i-nama-kantor-pos-kecamatan-kelurahan').value;
	var jarakKantorPosKecamatanKelurahan = $('i-jarak-kantor-pos-kecamatan-kelurahan').value;
	var koordinatLokasi = $('i-koordinat-lokasi').value;

	//Cek input
	if (nama.trim()==="") {
		alert ("Nama belum diisi");
		showForm1();
		$("i-nama").focus();
		return false;
	}

	if (ektp.trim()==="") {
		alert ("EKTP belum diisi");
		showForm1();
		$("i-ektp").focus();
		return false;
	} else {
		if (!isNumeric(ektp)) {
			alert("EKTP harus numerik");
			showForm1();
			$("i-ektp").focus();
			return false;
		} else {
			if (ektp.length !== 16) {
				alert("EKTP harus numerik 16 digit");
				showForm1();
				$("i-ektp").focus();
				return false;
			}
		}
	}

	if (npwp.trim()==="") {
		alert ("NPWP belum diisi");
		showForm1();
		$("i-npwp").focus();
		return false;
	} else {
		if (!isNumeric(npwp)) {
			alert("NPWP harus numerik");
			showForm1();
			$("i-npwp").focus();
			return false;
		} else {
			if (npwp.length !== 15) {
				alert("NPWP harus numerik 15 digit");
				showForm1();
				$("i-npwp").focus();
				return false;
			}
		}
	}

	if (telp.trim()==="") {
		alert ("Telp belum diisi");
		showForm1();
		$("i-telp").focus();
		return false;
	}

	if (email.trim()==="") {
		alert ("Email belum diisi");
		showForm1();
		$("i-email").focus();
		return false;
	} else {
		if (!isValidEmail(email)) {
			alert("Email tidak valid");
			showForm1();
			$("i-email").focus();
			return false;
		}
	}

	if (getSelectedOptionValue($('i-provinsi')) === '0' ) {
		alert('Provinsi belum dipilih');
		showForm2();
		$('i-provinsi').focus();
		return false;
	}

	if (getSelectedOptionValue($('i-kota')) === '0' ) {
		alert('Kota belum dipilih');
		showForm2();
		$('i-kota').focus();
		return false;
	}

	if (getSelectedOptionValue($('i-kecamatan')) === '0' ) {
		alert('Kecamatan belum dipilih');
		showForm2();
		$('i-kecamatan').focus();
		return false;
	}

	if (getSelectedOptionValue($('i-kelurahan')) === '0' ) {
		alert('Kelurahan belum dipilih');
		showForm2();
		$('i-kelurahan').focus();
		return false;
	}

	if (namaGerai.trim()==="") {
		alert ("Nama Gerai belum diisi");
		showForm2();
		$("i-nama-gerai").focus();
		return false;
	}

	if (alamatGerai.trim()==="") {
		alert ("Alamat Gerai belum diisi");
		showForm2();
		$("i-alamat-gerai").focus();
		return false;
	}

	if (ukuranGerai.trim()==="") {
		alert ("Ukuran gerai belum diisi");
		showForm2()
		$("i-ukuran-gerai").focus();
		return false;
	}

	if (namaKantorPosKabupatenKota.trim()==="") {
		alert ("Nama kantor Pos kabupaten/kota belum diisi");
		showForm2()
		$("i-nama-kantor-pos-kabupaten-kota").focus();
		return false;
	}

	if (jarakKantorPosKabupatenKota.trim()==="") {
		alert ("Jarak kantor Pos kabupaten/kota belum diisi");
		showForm2()
		$("i-jarak-kantor-pos-kabupaten-kota").focus();
		return false;
	}

	if (namaKantorPosKecamatanKelurahan.trim()==="") {
		alert ("Nama kantor Pos kecamatan/kelurahan belum diisi");
		showForm2()
		$("i-nama-kantor-pos-kecamatan-kelurahan").focus();
		return false;
	}

	if (jarakKantorPosKecamatanKelurahan.trim()==="") {
		alert ("Jarak kantor Pos kecamatan/kelurahan belum diisi");
		showForm2()
		$("i-jarak-kantor-pos-kecamatan-kelurahan").focus();
		return false;
	}

	if (koordinatLokasi.trim()==="") {
		alert ("Koordinat lokasi");
		showForm2()
		$("i-koordinat-lokasi").focus();
		return false;
	}

	var photodepangerai = null;
	if ( $("i-photo-depan-gerai").files.length == 0 ) {
		alert("Photo depan gerai belum ada");
		showForm2();
		$("i-photo-depan-gerai").focus();
		return false;
	}

	var photodalamgerai = null;
	if ( $("i-photo-dalam-gerai").files.length == 0 ) {
		alert("Photo dalam gerai belum ada");
		showForm2();
		$("i-photo-dalam-gerai").focus();
		return false;
	}

	var phototandatangan = null;
	if ( $("i-photo-tanda-tangan").files.length == 0 ) {
		alert("Photo tanda tangan belum ada");
		showForm2();
		$("i-photo-tanda-tangan").focus();
		return false;
	}

	gnamaFilePhotoDepanGerai = "pdpn_" + getCurrentDateTimeString() + '_' + makeID(5);
	gnamaFilePhotoDalamGerai = "pdlm_" + getCurrentDateTimeString() + '_' + makeID(5);
	gnamaFilePhotoTandaTangan = "pttg_" + getCurrentDateTimeString() + '_' + makeID(5);

	gformData = new FormData();
	gformData.append('nama',dbFriendly(nama));
	gformData.append('ektp',dbFriendly(ektp));
	gformData.append('npwp',dbFriendly(npwp));
	gformData.append('telp',dbFriendly(telp));
	gformData.append('email',dbFriendly(email));
	gformData.append('namagerai',dbFriendly(namaGerai));
	gformData.append('alamatgerai',dbFriendly(alamatGerai));
	gformData.append('ukurangerai',dbFriendly(ukuranGerai));
	gformData.append("jaraksesamaagenpos", dbFriendly(jarakSesamaAgenPos));
	gformData.append('datapesaing',dbFriendly(dataPesaing));
	gformData.append('filenamephotodalamgerai', dbFriendly(gnamaFilePhotoDalamGerai));
	gformData.append('filenamephotodepangerai', dbFriendly(gnamaFilePhotoDepanGerai));
	gformData.append('filenamephototandatangan', dbFriendly(gnamaFilePhotoTandaTangan));
	gformData.append('idprovinsi', dbFriendly(getSelectedOptionValue($('i-provinsi'))));
	gformData.append('namaprovinsi', dbFriendly(getSelectedOptionText($('i-provinsi'))));
	gformData.append('idkota', dbFriendly(getSelectedOptionValue($('i-kota'))));
	gformData.append('namakota', dbFriendly(getSelectedOptionText($('i-kota'))));
	gformData.append('idkecamatan', dbFriendly(getSelectedOptionValue($('i-kecamatan'))));
	gformData.append('namakecamatan', dbFriendly(getSelectedOptionText($('i-kecamatan'))));
	gformData.append('idkelurahan', dbFriendly(getSelectedOptionValue($('i-kelurahan'))));
	gformData.append('namakelurahan', dbFriendly(getSelectedOptionText($('i-kelurahan'))));
	gformData.append('namakantorposkabupatenkota', dbFriendly(namaKantorPosKabupatenKota));
	gformData.append('jarakkantorposkabupatenkota', dbFriendly(jarakKantorPosKabupatenKota));
	gformData.append('namakantorposkecamatankelurahan', dbFriendly(namaKantorPosKecamatanKelurahan));
	gformData.append('jarakkantorposkecamatankelurahan', dbFriendly(jarakKantorPosKecamatanKelurahan));
	gformData.append('koordinatlokasi', dbFriendly(koordinatLokasi));

	return true;
}

function uploadPhotoGerai(viHasilProses) {
	
	show('form2-action-button');
	hide('form2-info-upload');

	if (viHasilProses === "OK") {
		console.log(viHasilProses);

		//First attempt : gagal 
		var photo = $("i-photo-depan-gerai").files[0];
		resizeImage(photo, uploadImage,500,500,"depan");

		var photo = $("i-photo-dalam-gerai").files[0];
		resizeImage(photo, uploadImage,500,500,"dalam");

		var photo = $("i-photo-tanda-tangan").files[0];
		resizeImage(photo, uploadImage,500,500,"tandatangan");

		//Second attempt : biasanya berhasil, hah ini memang cara koboy :)	
		var photo = $("i-photo-depan-gerai").files[0];
		resizeImage(photo, uploadImage,500,500,"depan");

		var photo = $("i-photo-dalam-gerai").files[0];
		resizeImage(photo, uploadImage,500,500,"dalam");

		var photo = $("i-photo-tanda-tangan").files[0];
		resizeImage(photo, uploadImage,500,500,"tandatangan");

		infoSelesai();

	} else {
		infoPerbaikan(viHasilProses);
	}
}


function uploadImage(viBase64Img, viInfo) {
	var fileName = "";
	if (viInfo === "depan") {
		fileName = gnamaFilePhotoDepanGerai;
	}
	if (viInfo === "dalam") {
		fileName = gnamaFilePhotoDalamGerai;
	}
	if (viInfo === "tandatangan") {
		fileName = gnamaFilePhotoTandaTangan;
	}
	var formData = new FormData();
	formData.append('photo',viBase64Img);
	formData.append('filename', fileName);
	postData(API_SAVE_IMG_URL, formData, null);
}

function showHideKeuntungan() {
	if (gisDisplayKeuntungan) {
		hide('keuntungan');
		gisDisplayKeuntungan = false;
	} else {
		show('keuntungan');
		$('keuntungan').style.top = "120px";
		widthHeader = getDivWidth('header');
		widthKeuntungan = getDivWidth('keuntungan');
		selisih = widthHeader - widthKeuntungan;
		adjustment = Math.round(selisih/2);
		console.log("width header :" + widthHeader);
		console.log("width keuntungan :" + widthKeuntungan);
		console.log("width selisih :" + selisih);

		hide('keuntungan');

		left = getLeft($('header'));
		console.log("left =" + left);
		left = left + adjustment + 14;
		left = left + "px";
		$('keuntungan').style.left = left;
		show('keuntungan');
		gisDisplayKeuntungan = true;
	}	
}

function showPetaLokasi() {
	if (gisDisplayPetaLokasi) {
		hide('peta-lokasi');
		gisDisplayPetaLokasi = false;
		$('i-koordinat-lokasi').value = gLat + ";" + gLong;
	} else {
		show('peta-lokasi');
		$('peta-lokasi').style.top = "170px";
		widthHeader = getDivWidth('header');
		widthPetaLokasi = getDivWidth('peta-lokasi');
		selisih = widthHeader - widthPetaLokasi;
		adjustment = Math.round(selisih/2);
		console.log("width header :" + widthHeader);
		console.log("width peta-lokasi :" + widthPetaLokasi);
		console.log("width selisih :" + selisih);

		hide('peta-lokasi');

		left = getLeft($('header'));
		console.log("left =" + left);
		left = left + adjustment;
		left = left + "px";
		$('peta-lokasi').style.left = left;
		show('peta-lokasi');
		gisDisplayPetaLokasi = true;
	}	
}

function repositionPetaLokasi() {
	if (gisDisplayPetaLokasi) {
		showPetaLokasi(); //Close it
		showPetaLokasi(); //Reopen it
	}	
}


function repositionKeuntungan() {
	if (gisDisplayKeuntungan) {
		showHideKeuntungan(); //Close it
		showHideKeuntungan(); //Reopen it
	}	
}

function perbaikan() {
	hide("info");
	showForm1();
}

function infoPerbaikan(viHasilProses) {
	hide('form1');
	hide('form2');
	hide('keuntungan');
	$('info-text-content').innerHTML = "Pendaftaran Paketku Pos<br><br><strong>GAGAL</strong><br><br>" + viHasilProses + "<br><br>";
	show('info');
	hide('info-selesai');
	show('info-perbaikan');
}

function infoSelesai() {
	hide('form1');
	hide('form2');
	hide('keuntungan');
	$('info-text-content').innerHTML = "Pendaftaran Paketku Pos<br><br><strong>BERHASIL</strong><br><br>Admin kami akan segera<br>menghubungi Anda";
	show('info');
	show('info-selesai');
	hide('info-perbaikan');
}

function getProvince() {
	var formData = new FormData();
	formData.append('nothing','');
	postData(API_GET_PROVINCE, formData, showProvince);
}

function showProvince(viProvince) {
	var province = JSON.parse(viProvince);
	provinceSelect = $('i-provinsi');
	console.log(province);
	for(var i = 0; i < province.provinces.length; i++) {
	    var theProvince = province.provinces[i];
	    addOption($('i-provinsi'), theProvince.province_name.toUpperCase(), theProvince.id);
	}
}

function getCity() {
	removeAllOptions($('i-kota'));
	addOption($('i-kota'), 'Loading kota...', '0');
	var provinceID = $("i-provinsi").value;
	var formData = new FormData();
	formData.append('provinceid',provinceID);
	postData(API_GET_CITY, formData, showCity);
}

function getDistrict() {
	removeAllOptions($('i-kecamatan'));
	addOption($('i-kecamatan'), 'Loading kecamatan...', '0');
	var cityID = $("i-kota").value;
	var formData = new FormData();
	formData.append('cityid',cityID);
	postData(API_GET_DISTRICT, formData, showDistrict);
}

function getVillage() {
	removeAllOptions($('i-kelurahan'));
	addOption($('i-kelurahan'), 'Loading kelurahan...', '0');
	var districtID = $("i-kecamatan").value;
	var formData = new FormData();
	formData.append('districtid',districtID);
	postData(API_GET_VILLAGE, formData, showVillage);
}

function showVillage(viVillage) {
	var village = JSON.parse(viVillage);
	console.log(village);
	removeAllOptions($('i-kelurahan'));
	addOption($('i-kelurahan'), 'Pilih Kelurahan', '0');
	for(var i = 0; i < village.villages.length; i++) {
	    var theVillage = village.villages[i];
	    addOption($('i-kelurahan'), theVillage.village_name.toUpperCase(), theVillage.id);  
	}
}

function showDistrict(viDistrict) {
	var district = JSON.parse(viDistrict);
	console.log(district);
	removeAllOptions($('i-kecamatan'));
	addOption($('i-kecamatan'), 'Pilih Kecamatan', '0');
	for(var i = 0; i < district.districts.length; i++) {
	    var theDistrict = district.districts[i];
	    addOption($('i-kecamatan'), theDistrict.district_name.toUpperCase(), theDistrict.id);  
	}
}

function showCity(viCity) {
	var city = JSON.parse(viCity);
	console.log(city);
	removeAllOptions($('i-kota'));
	addOption($('i-kota'), 'Pilih Kota', '0');
	for(var i = 0; i < city.cities.length; i++) {
	    var theCity = city.cities[i];
	    addOption($('i-kota'), theCity.city_name.toUpperCase(), theCity.id);  
	}
}

function adjustForm(v) {
   //Not implemented
}

//*** MAP
function showPosition() {
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showMap, showError);
    } else {
        alert("Browser yang digunakan tidak mendukung HTML5 geolocation.");
    }
}
 
// Define callback function for successful attempt
function showMap(position) {
    // Get location data
    lat = position.coords.latitude;
    long = position.coords.longitude;
    
    gLat = lat;
    gLong = long;

    var latlong = new google.maps.LatLng(lat, long);
    
    var myOptions = {
        center: latlong,
        zoom: 16,
        mapTypeControl: true,
        navigationControlOptions: {
            style:google.maps.NavigationControlStyle.SMALL
        }
    }

    showPetaLokasi();
    
    var map = new google.maps.Map($("peta-lokasi"), myOptions);
    var marker = new google.maps.Marker({ position:latlong, map:map, title:"Anda di sini!" });

    //Add close button to map
	var mapCloseBtn = document.createElement("BUTTON");   
	mapCloseBtn.innerHTML = "Close";   
	mapCloseBtn.onclick = function() {
		gisDisplayPetaLokasi = true;
        showPosition();  		
	}  
    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(mapCloseBtn);
}
 
// Define callback function for failed attempt
function showError(error) {
	gLat = "0";
	gLong = "0";
    if(error.code == 1) {
        result.innerHTML = "Anda tidak mengijinkan berbagi lokasi";
    } else if(error.code == 2) {
        result.innerHTML = "Koneksi internet terputus atau layanan Google Map sedang terganggu";
    } else if(error.code == 3) {
        result.innerHTML = "Koneksi timeout data peta belum selesai diambil";
    } else {
        result.innerHTML = "Load peta gagal karena error yang tidak diketahui";
    }
}

function addMapEvent() {
   google.maps.event.addDomListener(document, 'keyup', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if ((code === 120) || (code === 88)) {
            //when 'x' or 'X' is pressed. Set as if previously shown. So it will be closed
            gisDisplayPetaLokasi = true;
            showPosition();
        }
    });

}






