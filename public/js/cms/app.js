/*
  File          : app.js
  Purpose       : simple javascript for simple web app
  Creator       : amin.nurahman@gmail.com
  License       : Free to use without warranty from the creator
  Created  		: October, 14th, 2020           
*/

/* Global variables */
var gMenuIsDisplayed = false;

/* On app-ready events */
document.addEventListener("DOMContentLoaded", function(){
	appInit();
});


/* Show app menu */
function showHideMenu(viDisplayStatus) {
	if (!viDisplayStatus) {
		//Currently it is not shown => show it.
		setTop('menu',67);
		setLeft('menu',0);
		show('menu-holder');
		setHeight('menu-content', (getDivHeight('menu')-getDivHeight('menu-caption')));
	} else {
		//Currently it is shown => hide it.
		hide('menu-holder');
	}
	viDisplayStatus = !viDisplayStatus;
	return viDisplayStatus;
}

function displayDateTime(viID) {
	var x = new Date()
	var x1 = x.getFullYear() + "-" + pad(x.getMonth() + 1) + "-" + pad(x.getDate()); 
	x1 = x1 + "  " +  pad(x.getHours())+ ":" +  pad(x.getMinutes()) + ":" +  pad(x.getSeconds());
	setContent(viID, x1);
}

/* ***************************************** Modify Below Here ************************************************* */

/* App initialization */
function appInit() {
	
	window.setInterval(function(){
	  	displayDateTime('footer-datetime');
	}, 1000);

	document.addEventListener("click", function(evnt){
	    appClickCtrl(evnt.target.id);
	});
}

/* App click event controller */
function appClickCtrl(viID) {
	//console.log(viID + ' is clicked');
	switch (viID) {
		//Hide open menu
		case 'app-menu-icon':
			gMenuIsDisplayed = showHideMenu(gMenuIsDisplayed);
			routeModule(viID);//Just tell to off any sub menu which is displayed
			break;
		case 'menu-upload-video':
		case 'icon-menu-upload-video':
		case 'caption-menu-upload-video':
			location.href = "/paketkupos/video-upload";
			gMenuIsDisplayed = showHideMenu(gMenuIsDisplayed);
			break;
		default:
			if (gMenuIsDisplayed) {
				gMenuIsDisplayed = showHideMenu(gMenuIsDisplayed);
			}
			routeModule(viID); //routeModule will be implemented specifically in each module script
			break;
	}
}

/* App menu item click action */ 
function menuClick(viMenuID, viMenuItemID) {
	 markList(viMenuID,viMenuItemID);
	 //Item menu action
}


