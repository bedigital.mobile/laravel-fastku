const INPROGRESS = 'In Progress';
const FAILED = 'Failed';
const DONE = 'Done';

var gVideoUploadView = INPROGRESS;
var gUploadMenuIsDisplayed = false;
var gInStatusView = false;
var gInFormView = false;
var gInSearchView = false;

videoUploadInitPage();

function routeModule(viID) {
	switch (viID) { 
		case 'vus-btn-inprogress' :
			gVideoUploadView = INPROGRESS;
			markViewBtn(viID);
			break;
		case 'vus-btn-failed':
			gVideoUploadView = FAILED;
			markViewBtn(viID);
			break;
		case 'vus-btn-done' :
			gVideoUploadView = DONE;
			markViewBtn(viID);
			break;		
		case 'module-upload-menu' :
		case 'module-upload-title' :
			gUploadMenuIsDisplayed = showHideUploadMenu(gUploadMenuIsDisplayed);
			break;
		case 'menu-upload-video-new':
		case 'icon-menu-upload-video-new':
		case 'caption-menu-upload-video-new':
			showNewUploadForm();
			break;
		case 'menu-upload-video-search':
		case 'icon-menu-upload-video-search':
		case 'caption-menu-upload-video-search':
			showUploadVideoStatus();
			break;	
		case 'menu-upload-video-status':
		case 'icon-menu-upload-video-status':
		case 'caption-menu-upload-video-status':
			showUploadVideoStatus();
			break;	

		default :
			if (gUploadMenuIsDisplayed) {
				gUploadMenuIsDisplayed = showHideUploadMenu(gUploadMenuIsDisplayed);
			}
			break;	
	}
}

function markViewBtn(viID) {
	$('vus-btn-inprogress').style.backgroundColor = "#7F8C8D";
	$('vus-btn-failed').style.backgroundColor = "#7F8C8D";
	$('vus-btn-done').style.backgroundColor = "#7F8C8D";
	
	$('vus-btn-inprogress').value = "In Progress";
	$('vus-btn-failed').value = "Failed";
	$('vus-btn-done').value = "Done";

	$('vus-list-title').innerHTML = "List of "+ $(viID).value + " Upload"; 
	$(viID).style.backgroundColor = "#37CA41";
	$(viID).value = $(viID).value;
}

function videoUploadInitPage() {
	markViewBtn('vus-btn-inprogress');
	markZebraList('item-list','');
}

function showHideUploadMenu(viDisplayStatus) {
	if (!viDisplayStatus) {
		//Currently it is not shown => show it.
		setTop('module-menu-upload',95);
		setLeft('module-menu-upload',10);
		//Adjust menu title based on current video upload view
		switch (gVideoUploadView) {
			case INPROGRESS :
				setContent('caption-menu-upload-video-search','Search In Progress Upload');
				break;
			case FAILED :
				setContent('caption-menu-upload-video-search','Search Failed Upload');
				break;
			case DONE :
				setContent('caption-menu-upload-video-search','Search Done Upload');
				break;
			default :
				break;		
		}
		show('module-menu-upload-holder');
		//setHeight('module-menu-upload-content', (getDivHeight('module-menu-upload')-getDivHeight('module-menu-upload-caption')));
		setHeight('module-menu-upload-content', 125);
		setHeight('module-menu-upload',125); 
	} else {
		//Currently it is shown => hide it.
		hide('module-menu-upload-holder');
	}
	viDisplayStatus = !viDisplayStatus;
	return viDisplayStatus;
}

function showNewUploadForm(){
	hide('video-upload-status');
	show('video-upload-form');
	/*
	resizeDialog(500,450); 
	setDialogContent(getContent('video-upload-form'));
	setDialogCaption('New Video Upload');
	showDialog(true);
	setFullCenter('dialog','');
	setFullCenter('dialog','');
	*/

	gUploadMenuIsDisplayed = showHideUploadMenu(gUploadMenuIsDisplayed); 
	gInFormView = true;
	gVideoUploadView = INPROGRESS;
	gInStatusView = false;
}

function showUploadVideoStatus(){
	hide('video-upload-form');
	show('video-upload-status');
	markViewBtn('vus-btn-inprogress');
	gUploadMenuIsDisplayed = showHideUploadMenu(gUploadMenuIsDisplayed);
	gInStatusView = true;
	gInFormView = false;
	gInSearchView = false;
}