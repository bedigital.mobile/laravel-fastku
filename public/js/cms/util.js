/*
  File          : util.js
  Purpose       : simple functional javascript library
  Creator       : amin.nurahman@gmail.com
  Notes         : This little library consist of several functions taken from internet.
                  Little modifications was added to meet my requirement. 
  License       : Free to use without warranty from the creator
  Created       : October, 14th, 2020           
*/

// Ajax  
function postData(viServiceURL, viData, viPostAction) {
	var vlXhr = new XMLHttpRequest();
	vlXhr.open('POST', viServiceURL);
	vlXhr.onload = function() {
	    if (vlXhr.status === 200) {
	        var vlResult = vlXhr.responseText;
            if (isFunction(viPostAction)) {
	        	viPostAction(vlResult);
	        }
	    }
	};
	vlXhr.send(viData);
}

// DOM Access 
function $(viID) {
	var vlElm = null;
	try {
		vlElm = document.getElementById(viID);
	} catch (vlErr) {
		console.log("util.js->$: " + viID + " is not defined");
	}
	return vlElm;
}

function show(viID,viShowType = "block") {
	$(viID).style.display = viShowType;
}

function hide(viID) {
	$(viID).style.display = "none";
}

function getFileInput(viID) {
	var vlFileInput = null; 
	if ( $(viID).files.length > 0 ) {
		vlFileInput = $(viID).files[0];
	}
	return vlFileInput;
}

function focus(viID) {
	$(viID).focus();
}

function getDivWidth(viID) {
  return $(viID).offsetWidth;   
}

function getDivHeight(viID) {
  return $(viID).offsetHeight;   
}

function getWindowWidth() {
  return Math.max(
    document.body.scrollWidth,
    document.documentElement.scrollWidth,
    document.body.offsetWidth,
    document.documentElement.offsetWidth,
    document.documentElement.clientWidth
  );
}

function getWindowHeight() {
  return Math.max(
    document.body.scrollHeight,
    document.documentElement.scrollHeight,
    document.body.offsetHeight,
    document.documentElement.offsetHeight,
    document.documentElement.clientHeight
  );
}

function addEvent(viID,viEventType, viAction) {
    $(viID).addEventListener(viEventType,viAction);
}

function getLeft(viID) {
    var vlElm = $(viID);
    var vlRect = vlElm.getBoundingClientRect(),
    vlScrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    vlScrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return (vlRect.left + vlScrollLeft);
}    

function getTop(viID) {
    var vlElm = $(viID);
    var vlRect = vlElm.getBoundingClientRect(),
    vlScrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    vlScrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return (vlRect.top + vlScrollTop);
}    

function isHidden(viID) {
    var vlElm = $(viID);
    return (vlElm.offsetParent === null)
}

function addOption(viSelectID, viText, viValue){
    var vlSelect = $(viSelectID);
    vlSelect.options[vlSelect.options.length] = new Option(viText, viValue, false, false);
}

function removeOption(viSelectID,viIndex){
    var vlSelect = $(viSelectID);
    vlSelect.options[viIndex] = null;
}

function removeAllOptions(viSelectID){
    var vlSelect = $(viSelectID);
    vlSelect.options.length = 0;
}

function getSelectedOptionValue(viSelectID) {
    var vlSelect = $(viSelectID);
    return vlSelect.options[vlSelect.selectedIndex].value;    
}

function getSelectedOptionText(viSelectID) {
    var vlSelect = $(viSelectID);
    return vlSelect.options[vlSelect.selectedIndex].text;    
}

// Other tools
function makeID(length) {
   var vlResult = '';
   var vlCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var vlCharactersLength = vlCharacters.length;
   for ( var vlI = 0; vlI < length; vlI++ ) {
      vlCharacters += vlCharacters.charAt(Math.floor(Math.random() * vlCharactersLength));
   }
   return vlResult;
}

function getCurrentDateTimeString() {
	var vlDate = new Date();
	return (
		vlDate.getFullYear().toString() + 
		pad(vlDate.getMonth() + 1) + 
		pad(vlDate.getDate()) + 
		pad(vlDate.getHours()) + 
		pad(vlDate.getMinutes()) + 
		pad(vlDate.getSeconds())
	);
}

function pad(viN) { return viN < 10 ? '0' + viN : viN }

function dbFriendly (viStr) {
    if (typeof viStr != 'string')
        return viStr;

    return viStr.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (viChar) {
        switch (viChar) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                // prepends a backslash to backslash, percent,
                // and double/single quotes
                return "\\"+viChar; 
        }
    });
}

// Image Manipulation
function resizeImage(viFile, viPostAction, viMaxWidth = 500, viMaxHeight = 500, viInfo= "" ) {
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        if (viFile) {
            var vlReader = new FileReader();
            // Set the image once loaded into file reader
            reader.onload = function(viElm) {

                var vlImg = document.createElement("img");
                vlImg.src = vlElm.target.result;

                var vlCanvas = document.createElement("canvas");
                var vlCtx = vlCanvas.getContext("2d");
                vlCtx.drawImage(vlImg, 0, 0);

                var vlMAX_WIDTH = viMaxWidth;
                var vlMAX_HEIGHT = viMaxHeight;
            	var vlWidth = vlImg.width;
                var vlHeight = vlImg.height;

                if (vlWidth > vlHeight) {
                    if (vlWidth > vlMAX_WIDTH) {
                        vlHeight *= vlMAX_WIDTH / vlWidth;
                        vlWidth = vlMAX_WIDTH;
                    }
                } else {
                    if (vlHeight > vlMAX_HEIGHT) {
                        vlWidth *= vlMAX_HEIGHT / vlHeight;
                        vlHeight = vlMAX_HEIGHT;
                    }
                }
                vlCanvas.width = vlWidth;
                vlCanvas.height = vlHeight;
                var vlCtx = vlCanvas.getContext("2d");
                vlCtx.drawImage(vlImg, 0, 0, vlWidth, vlHeight);
                var vlDataURL = vlCanvas.toDataURL(viFile.type);
                if (isFunction(viPostAction)) {
                	viPostAction(vlDataURL,viInfo);
                }
            }
            vlReader.readAsDataURL(viFile);
        }
    } else {
        if(isFunction(viPostAction)) {
        	viPostAction(null);
        }
    }
}

function isNumeric(viStr){
    return /^[0-9]+$/.test(viStr);
}

function isNumber(viStr) {
    return !isNaN(parseFloat(viStr)) && isFinite(viStr);
}

function isValidEmail(viEmail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(viEmail)) {
        return (true)
    }
    return (false)
}

function eventFire(viID, viEventType = 'click'){
  var vlElm = $(viID);
  if (vlElm.fireEvent) {
    vlElm.fireEvent('on' + viEventType);
  } else {
    var vlEventObj = document.createEvent('Events');
    vlEventObj.initEvent(viEventType, true, false);
    vlElm.dispatchEvent(vlEventObj);
  }
}

function isValidURL(viStr) {
  var vlPattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  return !!vlPattern.test(viStr);
}

function isFunction(viFunctionToCheck) {
    return viFunctionToCheck && {}.toString.call(viFunctionToCheck) === '[object Function]';
}

function setWidth(viID, viWidth) {
    $(viID).style.width = viWidth + "px";
}

function setHeight(viID, viHeight) {
    $(viID).style.height = viHeight + "px";
}

function setHCenter(viChildID, viParentID) {
    var vlParent = $(viParentID);
    var vlParentWidth = 0;
    var vlChildWidth = 0;
    var vlDeltaWidth = 0;
    var vlChildLeft = 0;

    if (vlParent !== null) {
        vlParentWidth = getDivWidth(viParentID);
        vlChildWidth = getDivWidth(viChildID);
        vlDeltaWidth = vlParentWidth - vlChildWidth;
        vlChildLeft = getLeft(viParentID) + (Math.round(vlDeltaWidth/2));
        $(viChildID).style.left = vlChildLeft + "px";
        $(viChildID).style.position = 'absolute';
    } else {
        vlParentWidth = getWindowWidth();
        vlChildWidth = getDivWidth(viChildID);
        vlDeltaWidth = vlParentWidth - vlChildWidth;
        vlChildLeft = -5 + (Math.round(vlDeltaWidth/2));
        $(viChildID).style.left = vlChildLeft + "px";
        $(viChildID).style.position = 'absolute';
    }

}

function setLeft(viID, viPosition) {
    $(viID).style.position = 'absolute';
    $(viID).style.left = viPosition + 'px';
}

function setTop(viID, viPosition) {
    $(viID).style.position = 'absolute';
    $(viID).style.top = viPosition + 'px';
}

function setRight(viID, viPosition) {
    $(viID).style.position = 'absolute';
    $(viID).style.right = viPosition + 'px';
}

function setBottom(viID, viPosition) {
    $(viID).style.position = 'absolute';
    $(viID).style.left = viPosition + 'px';
}

function setVCenter(viChildID, viParentID) {
    var vlParent = $(viParentID);
    var vlParentHeight = 0;
    var vlChildHeight = 0;
    var vlDeltaHeight = 0;
    var vlChildTop = 0;
    if (vlParent !== null) {
        vlParentHeight = getDivHeight(viParentID);
        vlChildHeight = getDivHeight(viChildID);
        vlDeltaHeight = vlParentHeight - vlChildHeight;
        vlChildTop = getTop(viParentID) + (Math.round(vlDeltaHeight/2));
        $(viChildID).style.top = vlChildTop + "px";
        $(viChildID).style.position = 'fixed';
    } else {
        vlParentHeight = getWindowHeight();
        vlChildHeight = getDivHeight(viChildID);
        vlDeltaHeight = vlParentHeight - vlChildHeight;
        vlChildTop = 0 + (Math.round(vlDeltaHeight/2));
        $(viChildID).style.top = vlChildTop + "px";
        $(viChildID).style.position = 'fixed';
    }
}

function setFullCenter(viChildID, viParentID) {
    setHCenter(viChildID, viParentID);
    setVCenter(viChildID, viParentID);
}

function setValue(viID,viValue) {
    try {
        $(viID).value = viValue;
    }
    catch(vlErr) {
        console.log('util.js->setValue-> '+ vlErr.message + ' on element :' + viID);
    }   
}

function setContent(viID,viContent) {
    try {
        $(viID).innerHTML = viContent;
    } catch(vlErr){
        console.log('util.js->setContent-> '+ vlErr.message + ' on element :' + viID);
    }
}

function getContent(viID) {
    try {
        return $(viID).innerHTML;
    } catch(vlErr){
        console.log('util.js->getContent-> '+ vlErr.message + ' on element :' + viID);
    }
}

function zebraList(viULID) {
    var vlUL = $(viULID);
    if (!vlUL.childNodes || vlUL.childNodes.length == 0) return;
    // Iterate LIs
    var vlItems = vlUL.getElementsByTagName("li");
    for (var vlIndex=0;vlIndex<vlItems.length;vlIndex++) {
        var vlItem = vlItems[vlIndex];
         vlItem.classList.remove('selected-color');
        if (vlIndex % 2 == 0) {
            vlItem.classList.remove('odd-color');
            vlItem.classList.remove('event-color');
            vlItem.classList.add("even-color");
        } else {
            vlItem.classList.remove('odd-color');
            vlItem.classList.remove('event-color');
            vlItem.classList.add("odd-color");
        }            
    }
}

function clearZebraList(viULID) {
    var vlUL = $(viULID);
    if (!vlUL.childNodes || vlUL.childNodes.length == 0) return;
    // Iterate LIs
    var vlItems = vlUL.getElementsByTagName("li");
    for (var vlIndex=0;vlIndex<vlItems.length;vlIndex++) {
        var vlItem = vlItems[vlIndex];
         vlItem.classList.remove('selected-color');
         vlItem.classList.remove('odd-color');
         vlItem.classList.remove('event-color');
    }
}


function markZebraList(viULID,viSelectedLIID) {
    zebraList(viULID);
    var vlUL = $(viULID);
    if (!vlUL.childNodes || vlUL.childNodes.length == 0) return;
    // Iterate LIs
    var vlItems = vlUL.getElementsByTagName("li");
    for (var vlIndex=0;vlIndex<vlItems.length;vlIndex++) {
        var vlItem = vlItems[vlIndex];
        if (vlItem.id === viSelectedLIID) {
            vlItem.classList.remove('odd-color');
            vlItem.classList.remove('event-color');
            vlItem.classList.add("selected-color");
        }        
    }
}

function markList(viULID,viSelectedLIID) {
    clearZebraList(viULID);
    var vlUL = $(viULID);
    if (!vlUL.childNodes || vlUL.childNodes.length == 0) return;
    // Iterate LIs
    var vlItems = vlUL.getElementsByTagName("li");
    for (var vlIndex=0;vlIndex<vlItems.length;vlIndex++) {
        var vlItem = vlItems[vlIndex];
        if (vlItem.id === viSelectedLIID) {
            vlItem.classList.remove('odd-color');
            vlItem.classList.remove('event-color');
            vlItem.classList.add("selected-color");
        }        
    }
}

function createDragableDialog(viElm) {
    var vlPos1 = 0, vlPos2 = 0, vlPos3 = 0, vlPos4 = 0;

    $(viElm.id + "-toggle").onclick = closeDialog;

    if ($(viElm.id + "-header")) {
        /* if present, the header is where you move the DIV from:*/
        $(viElm.id + "-header").onmousedown = dragMouseDown;
    } else {
        /* otherwise, move the DIV from anywhere inside the DIV:*/
        viElm.onmousedown = dragMouseDown;
    }

    function closeDialog(viE, viOverlayID='the-overlay'){
        viE = viE || window.event;
        viE.preventDefault();
        $(viElm.id).style.display = 'none'; 
        try {
            $('overlay-'+viOverlayID).remove();
        } catch (err) {

        }
    }

  function dragMouseDown(viE) {
    viE = viE || window.event;
    viE.preventDefault();
    // get the mouse cursor position at startup:
    vlPos3 = viE.clientX;
    vlPos4 = viE.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(viE) {
    viE = viE || window.event;
    viE.preventDefault();
    // calculate the new cursor position:
    vlPos1 = vlPos3 - viE.clientX;
    vlPos2 = vlPos4 - viE.clientY;
    vlPos3 = viE.clientX;
    vlPos4 = viE.clientY;
    // set the element's new position:
    viElm.style.top = (viElm.offsetTop - vlPos2) + "px";
    viElm.style.left = (viElm.offsetLeft - vlPos1) + "px";
  }

  function closeDragElement() {
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

/* Dialog Helper */
function showDialog(viModal=false) {
    if (viModal) {
        createOverlay();                
        $('dialog').style.zIndex = 9999;
    }
    show('dialog-holder');
    show('dialog');
    createDragableDialog($('dialog'));
}
function resizeDialog(viWidth, viHeight){
    //Add more 10 pixels to viWidth for preventing 'X' from wrapping 
    var vlWindowWidth = getWindowWidth();
    var vlWindowHeight = getWindowHeight();
    setWidth('dialog',viWidth + 10);
    setHeight('dialog',viHeight);
}
function setDialogContent(viContent) {
    setContent('dialog-content',viContent);
}
function setDialogCaption(viCaption) {
    setContent('dialog-caption',viCaption);
}

/* Create overlay for modal dialog */
function createOverlay(viOverlayID = 'the-overlay') {
    var vlOverlay = document.createElement('div');
    vlOverlay.style.width = getWindowWidth();
    vlOverlay.style.height = getWindowHeight();
    vlOverlay.classList.add('overlay');
    vlOverlay.style.display = 'block';
    vlOverlay.id = 'overlay-' + viOverlayID;
    document.body.appendChild(vlOverlay);
}


