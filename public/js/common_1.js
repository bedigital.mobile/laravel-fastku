var daysOfMonth = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var monthMaxDay = [31, febMaxDay(), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
var months = ["January", "February", "March", "April", "May", "June", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

function febMaxDay() {
    var d = new Date();
    var y = d.getYear();
    
    if ((y % 4) == 0 || (y % 100) == 0 || (y % 400) == 0) {
        return 29;
    }
    
    return 28;
}

function sendForm3(config) {
    if(config.beforeSubmit !== null) {
        config.beforeSubmit();
    } 
    
    if (config.form !== null) {
        var frm = $("#" + config.form).serialize();
        $("#" + config.form).children().prop("disabled", true);
        $('input:button').addClass('disabled');
    }
    
                           console.log("config"+config);

    
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('input[name=_token]').val()
        },
        type: (config.method !== null ? config.method : 'POST'),
        url: config.url,
        dataType: 'html',
        data: frm,
        success: function(resp) {
             console.log("reponse: " + resp);
            var result = jQuery.parseJSON(resp);
            if(typeof(result) !== 'undefined' && typeof(result.error) !== 'undefined' && typeof(result.error.code) !== 'undefined' && result.error.code === 200) {
                if(config.redirect !== null && config.redirect !== "") {
                    if(config.afterSubmit !== null) {
                        config.afterSubmit();
                    }
                    
                    if(typeof config.redirect === 'function') {
                        var resp = typeof(result.data) !== 'undefined' ? result.data : null;
                        config.redirect(result.data);
                       // console.log("result data"+result.data); 
                    } else {  
                        getView(config.redirect);
                        new PNotify({
                                  title: config.messageSuccess,
                                   type: 'success',
                                  styling: 'bootstrap3'
                              }); 
                        //console.log("config-redirect"+config.redirect);
                    }
                }
            }
            else {
                if (config.form !== null) {
                    $("#" + config.form).children().prop("disabled", false);
                    $('input:button').removeClass('disabled');
                }
                
                if (config.messageView != null) {
                    var msg = "<ul>";                
                    if (typeof(result) !== 'undefined' && typeof(result.error) !== 'undefined') {
                        if (typeof(result.error.messages) !== 'undefined') {
                            for (var i in result.error.messages) {
                                msg += "<li>" + result.error.messages[i] + "</li>";
                            };
                        }
                    } else {
                        msg += "<li style='color: red;'>Ooops!!! Something went wrong, please contact your administrator.</li>";
                    }
                    msg += "</ul>"; 
                   // $("#" + config.messageView).html("<div class='alert alert-danger'><strong>Invalid Data Sent:</strong>"+msg+"</div>");
                    $("#" + config.messageView).html("<div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>"+msg+"</strong></div>");

                    $(window).scrollTop(0); 
                }
                
                if(config.afterSubmit !== null) {
                    config.afterSubmit();
                }
            }
        },
        error: function(resp) {
            if (config.form !== null) {
                $("#" + config.form).children().prop("disabled", false);
                $('input:button').removeClass('disabled');
            }
            
            if(config.afterSubmit !== null)
                config.afterSubmit();
                new PNotify({ 
                                  title: 'Invalid Data Sent',
                                  text: msg,
                                   type: 'error',
                                  styling: 'bootstrap3'
                              });          
           // $("#" + config.messageView).html("<div class='alert alert-danger'><strong>Invalid Data Sent: &nbsp;</strong>Unable to connect to server. Please try again.</div>");
            $(window).scrollTop(0);

            return false;
        }
    });
}

function sendForm2(config) {
    if(config.beforeSubmit !== null) {
        config.beforeSubmit();
    }
    
    if (config.form !== null) {
        var frm = $("#" + config.form).serialize();
        $("#" + config.form).children().prop("disabled", true);
        $('input:button').addClass('disabled');
    }
    
                           console.log("config"+config);

    
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('input[name=_token]').val()
        },
        type: (config.method !== null ? config.method : 'POST'),
        url: config.url,
        dataType: 'html',
        data: frm,
        success: function(resp) {
             console.log("reponse: " + resp);
            var result = jQuery.parseJSON(resp);
            if(typeof(result) !== 'undefined' && typeof(result.error) !== 'undefined' && typeof(result.error.code) !== 'undefined' && result.error.code === 200) {
                if(config.redirect !== null && config.redirect !== "") {
                    if(config.afterSubmit !== null) {
                        config.afterSubmit();
                    }
                    
                    if(typeof config.redirect === 'function') {
                        var resp = typeof(result.data) !== 'undefined' ? result.data : null;
                        config.redirect(result.data);
                       // console.log("result data"+result.data); 
                    } else {  
                        getPage(config.redirect);
                        //console.log("config-redirect"+config.redirect);
                    } 
                }
            }
            else {
                if (config.form !== null) {
                    $("#" + config.form).children().prop("disabled", false);
                    $('input:button').removeClass('disabled');
                }
                
                if (config.messageView != null) {
                    var msg = "<ul>";                
                    if (typeof(result) !== 'undefined' && typeof(result.error) !== 'undefined') {
                        if (typeof(result.error.messages) !== 'undefined') {
                            for (var i in result.error.messages) {
                                msg += "<li>" + result.error.messages[i] + "</li>";
                            };
                        }
                    } else {
                        msg += "<li style='color: red;'>Ooops!!! Something went wrong, please contact your administrator.</li>";
                    }
                    msg += "</ul>";
                    $("#" + config.messageView).html("<div class='alert alert-danger'><strong>Invalid Data Sent:</strong>"+msg+"</div>");
                    $(window).scrollTop(0);
                }
                
                if(config.afterSubmit !== null) {
                    config.afterSubmit();
                }
            }
        },
        error: function(resp) {
            if (config.form !== null) {
                $("#" + config.form).children().prop("disabled", false);
                $('input:button').removeClass('disabled');
            }
            
            if(config.afterSubmit !== null)
                config.afterSubmit();
                        
            $("#" + config.messageView).html("<div class='alert alert-danger'><strong>Invalid Data Sent: &nbsp;</strong>Unable to connect to server. Please try again.</div>");
            $(window).scrollTop(0);

            return false;
        }
    });
}

function sendJson(config) {
    if(config.beforeSubmit !== null) {
        config.beforeSubmit();
    }
    
//    var frm = $("#" + config.form).serialize();
//    $("#" + config.form).children().prop("disabled", true);
//    $('input:button').addClass('disabled');
    
//    console.log($('input[name=_token]').val());
    
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('input[name=_token]').val()
        },
        type: (config.method !== null ? config.method : 'POST'),
        url: config.url,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(config.data),
        success: function(resp) {
            console.log("reponse: " + resp);
            var result = jQuery.parseJSON(resp);
            if(typeof(result) !== 'undefined' && typeof(result.error) !== 'undefined' && typeof(result.error.code) !== 'undefined' && result.error.code === 200) {
                if(config.redirect !== null && config.redirect !== "") {
                    if(typeof config.redirect === 'function') {
                        var resp = typeof(result.data) !== 'undefined' ? result.data : null;
                        config.redirect(result.data);
                    } else {
                        getView(config.redirect);
                    }
                }
            }
            else {
//                $("#" + config.form).children().prop("disabled", false);
//                $('input:button').removeClass('disabled');
                
                if (config.messageView !== null) {
                    var msg = "<ul>";                
                    if (typeof(result) !== 'undefined' && typeof(result.error) !== 'undefined') {
                        if (typeof(result.error.messages) !== 'undefined') {
                            for (var i in result.error.messages) {
                                msg += "<li>" + result.error.messages[i] + "</li>";
                            };
                        }
                    } else {
                        msg += "<li style='color: red;'>Ooops!!! Something went wrong, please contact your administrator.</li>";
                    }
                    msg += "</ul>";
                    $("#" + config.messageView).html("<div class='alert alert-danger'><strong>Invalid Data Sent:</strong>"+msg+"</div>");
                    $(window).scrollTop(0);
                }
                
                if(config.afterSubmit !== null)
                    config.afterSubmit();
            }
        },
        error: function(resp) {
//            $("#" + config.form).children().prop("disabled", false);
//            $('input:button').removeClass('disabled');
            if(config.afterSubmit !== null)
                config.afterSubmit();
            
            $("#" + config.messageView).html("<div class='alert alert-danger'><strong>Invalid Data Sent: </strong>Unable to connect to server. Please try again.</div>");
            $(window).scrollTop(0);

            return false;
        }
    });
}

function pageLoadProgress(message) {
    var msg = message == null ? 'Loading ...' : message;
    var load = '<div id="loading-bar" class="row">';
    load += '<p><strong>' + msg + '</strong></p>';
    load += '<img src="https://fintech-stg.bedigital.co.id/admin/img/ajax-loaders/ajax-loader-7.gif" title="img/ajax-loaders/ajax-loader-7.gif" ></div>';
    
    return load;
}

function getView(address, callback) {
    $('#content').html("");
    $('#content').append(pageLoadProgress('Loading...'));
    if (callback !== null) {
        $('#content').load(address, callback);
    } else {
        $('#content').load(address);
    }
} 

function getPage(address) {
    $('#content').html(pageLoadProgress('Loading...'));
    window.location.replace(address);
}

function getContentForView(elemId, address, callback) {
    $('#' + elemId).html(pageLoadProgress('Loading ...'));
    if (callback !== null) {
        $('#' + elemId).load(address, callback);
    } else {
        $('#' + elemId).load(address);
    }
}

function getContentInBox(address) {
    $('.box-content').html(pageLoadProgress('Loading ...'));
    window.location.replace(address);
}

function getContentInTable(address) {
    var tdCount = $('#x-table thead tr th').length;console.log(tdCount);
    $('#x-table tbody').html('<tr><td colspan="' + tdCount + '">Loading...</td></tr>');
    window.location.replace(address);
}

function goSearch(address) {
    var url = address + '?q=' + escape($('#frm-search').find('input[id="q"]').val());
    
    getContentInTable(url);

    return false;
}

function goSearchInBox(address) {
    var url = address + '?q=' + escape($('#frm-search').find('input[id="q"]').val());
    
    $('.box-content').html(pageLoadProgress('Loading ...'));    
    window.location.replace(url);

    return false;
}

function slugify(text)
{
  if (typeof text === 'undefined') {
      return '';
  }
  
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}

var callbackFileUpload = null;
function openFileUpload(callback) {
    this.callbackFileUpload = callback;
    
    var url = '/file_upload_image.html';
    var w = 375;
    var h = 300;
    var left = (screen.width-w)/2;
    var top = (screen.height-h)/2;
    var win = null;
    if (win === null) {
        win = window.open(url, 'Upload File', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    } else {
        win.focus();
    }
}

function callbackUploadedFile(data) {
    if(typeof this.callbackFileUpload === 'function') {
        this.callbackFileUpload(data);
    }
}

function print_barcode(text) {
    if ( text === '' ) {
        alert( 'Barcode text cannot be empty' );
        
        return;
    }
    
    var url = '/barcode.html?text=' + escape(text);
    var w = 375;
    var h = 200;
    var left = (screen.width-w)/2;
    var top = (screen.height-h)/2;
    var win = null;
    if (win === null) {
        win = window.open(url, 'Upload File', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    } else {
        win.focus();
    }
}

function autoComplete(config) {
    var $class = $('.' + config.className);
    
    if (typeof config.data !== 'undefined' && config.data !== null) {
        $class.typeahead({
            source: config.data,
            autoSelect: true
        });
    } else {
        $.get(config.resource, function (data) {
            $class.typeahead({
                source: data,
                autoSelect: true
            });
        }, 'json');
    }
    
    $class.change(function() {
        var current = $class.typeahead('getActive');
        if (current) {
            config.callback();
        }
    });
}

function imageOnSubmit() {
    $('#button-close-upload').attr('disabled', 'disabled');
    $('#button-upload').attr('disabled', 'disabled');
    $('#button-upload').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Saving...');
}

function imageOnSuccessSubmit(image) {
    imageOnErrorSubmit();
    $('#file-name').val(null);
    $('#button-close-upload').click();
    
    callbackImageUpload(image);
}

function imageOnErrorSubmit() {
    $('#button-close-upload').removeAttr('disabled');
    $('#button-upload').removeAttr('disabled');
    $('#button-upload').html('<i class="glyphicon glyphicon-save"></i> Save');
}



 

 

$(document).ready(function() {
    $('#frame-image-upload').on('load', function() {
        var response = $(this).contents().find('pre').html();
        if(response !== '') {
            var json = jQuery.parseJSON(response);
            if(json.error.code == 200 && typeof(json.data) !== 'undefined' && json.data !== null) {
                imageOnSuccessSubmit(json.data);
            } else {
                imageOnErrorSubmit();

                var msg = '<div class="alert alert-danger" role="alert">Invalid Data Sent:<ul>';
                for (var i=0; i<json.error.messages.length; i++) {
                    msg += '<li>' + json.error.messages[i] + '</li>';
                }
                msg += '</ul></div>';
                $('#msg').html(msg);
            }
        }
    });
});