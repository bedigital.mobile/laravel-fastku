// Ajax  
function postData(viServiceURL, viData, viPostAction) {
	var xhr = new XMLHttpRequest();
	xhr.open('POST', viServiceURL);
	xhr.onload = function() {
	    if (xhr.status === 200) {
	        var result = xhr.responseText;
            if (isFunction(viPostAction)) {
	        	viPostAction(result);
	        }
	    }
	};
	xhr.send(viData);
}

// DOM Access 
function $(viID) {
	var elm = null;
	try {
		elm = document.getElementById(viID);
	} catch (err) {
		console.log("util.js->$: " + viID + " is not defined");
	}
	return elm;
}

function show(viID,viShowType = "block") {
	$(viID).style.display = viShowType;
}

function hide(viID) {
	$(viID).style.display = "none";
}

function getFileInput(viID) {
	var fileInput = null; 
	if ( $(viID).files.length > 0 ) {
		fileInput = $(viID).files[0];
	}
	return fileInput;
}

function focus(viID) {
	$(viID).focus();
}

function getDivWidth(viID) {
  return $(viID).offsetWidth;   
}

function getWindowWidth() {
  return Math.max(
    document.body.scrollWidth,
    document.documentElement.scrollWidth,
    document.body.offsetWidth,
    document.documentElement.offsetWidth,
    document.documentElement.clientWidth
  );
}

function getWindowHeight() {
  return Math.max(
    document.body.scrollHeight,
    document.documentElement.scrollHeight,
    document.body.offsetHeight,
    document.documentElement.offsetHeight,
    document.documentElement.clientHeight
  );
}

function addEvent(viID,eventType, action) {
    $(viID).addEventListener(eventType,action);
}

function getLeft(el) {
    var rect = el.getBoundingClientRect(),
    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return (rect.left + scrollLeft);
}    

function getTop(el) {
    var rect = el.getBoundingClientRect(),
    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return (rect.top + scrollTop);
}    

function isHidden(el) {
    return (el.offsetParent === null)
}

function addOption(viSelect, viText, viValue){
    viSelect.options[viSelect.options.length] = new Option(viText, viValue, false, false);
}

function removeOption(viSelect,viIndex){
    viSelect.options[viIndex] = null;
}

function removeAllOptions(viSelect){
    viSelect.options.length = 0;
}

function getSelectedOptionValue(viSelect) {
    return viSelect.options[viSelect.selectedIndex].value;    
}

function getSelectedOptionText(viSelect) {
    return viSelect.options[viSelect.selectedIndex].text;    
}

// Other tools
function makeID(length) {
   var result = '';
   var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

function getCurrentDateTimeString() {
	var date = new Date();
	return (
		date.getFullYear().toString() + 
		pad(date.getMonth() + 1) + 
		pad(date.getDate()) + 
		pad(date.getHours()) + 
		pad(date.getMinutes()) + 
		pad(date.getSeconds())
	);
}

function pad(n) { return n < 10 ? '0' + n : n }

function dbFriendly (viStr) {
    if (typeof viStr != 'string')
        return viStr;

    return viStr.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\"+char; // prepends a backslash to backslash, percent,
                                  // and double/single quotes
        }
    });
}

// Image Manipulation
function resizeImage(viFile, viPostAction, viMaxWidth = 500, viMaxHeight = 500, viInfo= "" ) {
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        if (viFile) {
            var reader = new FileReader();
            // Set the image once loaded into file reader
            reader.onload = function(e) {

                var img = document.createElement("img");
                img.src = e.target.result;

                var canvas = document.createElement("canvas");
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);

                var MAX_WIDTH = viMaxWidth;
                var MAX_HEIGHT = viMaxHeight;
            	var width = img.width;
                var height = img.height;

                if (width > height) {
                    if (width > MAX_WIDTH) {
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                    }
                } else {
                    if (height > MAX_HEIGHT) {
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                    }
                }
                canvas.width = width;
                canvas.height = height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0, width, height);
                var dataURL = canvas.toDataURL(viFile.type);
                if (isFunction(viPostAction)) {
                	viPostAction(dataURL,viInfo);
                }
            }
            reader.readAsDataURL(viFile);
        }
    } else {
        if(isFunction(viPostAction)) {
        	viPostAction(null);
        }
    }
}

function isNumeric(viStr){
    return /^[0-9]+$/.test(viStr);
}

function isNumber(viStr) {
    return !isNaN(parseFloat(viStr)) && isFinite(viStr);
}

function isValidEmail(viEmail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(viEmail)) {
        return (true)
    }
    return (false)
}

function isValidURL(viStr) {
  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  return !!pattern.test(viStr);
}

function isFunction(viFunctionToCheck) {
    return viFunctionToCheck && {}.toString.call(viFunctionToCheck) === '[object Function]';
}

