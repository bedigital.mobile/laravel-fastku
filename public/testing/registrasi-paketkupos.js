var gformData = null;
var gnamaFilePhotoBelakangGerai = '';
var gnamaFilePhotoDepanGerai = '';
var gnamaFilePhotoDenahGerai = '';
var gisDisplayKeuntungan = false;

const API_REG_URL = '/paketkupos/submit-registrasi';
const API_SAVE_IMG_URL = '/paketkupos/submit-image';
const API_GET_PROVINCE = '/paketkupos/get-province';
const API_GET_CITY = '/paketkupos/get-city';

window.addEventListener("resize",function(){
	//Repositioning popup
	repositionKeuntungan();
},false);

document.addEventListener("DOMContentLoaded", function(){
	initPage();
});

function initPage() {
	hide('form2');
	hide('form2-info-upload');
	addEvent('btn-berikutnya','click',showForm2);
	addEvent('btn-kembali','click',showForm1);
	addEvent('btn-daftar','click',submitForm);
	addEvent('keuntungan','click',showHideKeuntungan);
	addEvent('footer','click',showHideKeuntungan);
	addEvent('lbl-perbaikan','click',perbaikan);
	getProvince();
	$('i-provinsi').onchange =  function() {
 		 getCity();
	}
	showHideKeuntungan();
	showForm1();
}

function showForm2() {
	hide('form1');
	show('form2');
	$('content').style.height = "535px";
}

function showForm1() {
	hide('form2');
	$('content').style.height = "440px";
	show('form1');
}

function submitForm() {
	if (getFormData()) {
		if (gformData !== null) {
			hide('form2-action-button');
			show('form2-info-upload');
			console.log('submiting form data');
			postData(API_REG_URL, gformData, uploadPhotoGerai);
		}
	}
}

function getFormData() {
	gformData = null;
	var nama = $("i-nama").value;
	var ektp = $("i-ektp").value;
	var npwp = $("i-npwp").value;
	var telp = $("i-telp").value;
	var email = $("i-email").value;
	var namaGerai = $("i-nama-gerai").value;
	var alamatGerai = $("i-alamat-gerai").value;
	var ukuranGerai = $("i-ukuran-gerai").value;
	var jarakSesamaAgenPos = $("i-jarak-sesama-agen-pos").value;
	var dataPesaing = $("i-data-pesaing").value;

	//Cek input
	if (nama.trim()==="") {
		alert ("Nama belum diisi");
		showForm1();
		$("i-nama").focus();
		return false;
	}

	if (ektp.trim()==="") {
		alert ("EKTP belum diisi");
		showForm1();
		$("i-ektp").focus();
		return false;
	} else {
		if (!isNumeric(ektp)) {
			alert("EKTP harus numerik");
			showForm1();
			$("i-ektp").focus();
			return false;
		} else {
			if (ektp.length !== 16) {
				alert("EKTP harus numerik 16 digit");
				showForm1();
				$("i-ektp").focus();
				return false;
			}
		}
	}

	if (npwp.trim()==="") {
		alert ("NPWP belum diisi");
		showForm1();
		$("i-npwp").focus();
		return false;
	} else {
		if (!isNumeric(npwp)) {
			alert("NPWP harus numerik");
			showForm1();
			$("i-npwp").focus();
			return false;
		} else {
			if (npwp.length !== 15) {
				alert("NPWP harus numerik 15 digit");
				showForm1();
				$("i-npwp").focus();
				return false;
			}
		}
	}

	if (telp.trim()==="") {
		alert ("Telp belum diisi");
		showForm1();
		$("i-telp").focus();
		return false;
	}

	if (email.trim()==="") {
		alert ("Email belum diisi");
		showForm1();
		$("i-email").focus();
		return false;
	} else {
		if (!isValidEmail(email)) {
			alert("Email tidak valid");
			showForm1();
			$("i-email").focus();
			return false;
		}
	}

	if (getSelectedOptionValue($('i-provinsi')) === '0' ) {
		alert('Provinsi belum dipilih');
		showForm2();
		$('i-provinsi').focus();
		return false;
	}

	if (getSelectedOptionValue($('i-kota')) === '0' ) {
		alert('Kota belum dipilih');
		showForm2();
		$('i-kota').focus();
		return false;
	}

	if (namaGerai.trim()==="") {
		alert ("Nama Gerai belum diisi");
		showForm2();
		$("i-nama-gerai").focus();
		return false;
	}

	if (alamatGerai.trim()==="") {
		alert ("Alamat Gerai belum diisi");
		showForm2();
		$("i-alamat-gerai").focus();
		return false;
	}

	if (ukuranGerai.trim()==="") {
		alert ("Ukuran gerai belum diisi");
		showForm2()
		$("i-ukuran-gerai").focus();
		return false;
	}

	var photodepangerai = null;
	if ( $("i-photo-depan-gerai").files.length == 0 ) {
		alert("Photo depan gerai belum ada");
		showForm2();
		$("i-photo-depan-gerai").focus();
		return false;
	}

	var photodepangerai = null;
	if ( $("i-photo-belakang-gerai").files.length == 0 ) {
		alert("Photo dalam gerai belum ada");
		showForm2();
		$("i-photo-belakang-gerai").focus();
		return false;
	}

	var photodepangerai = null;
	if ( $("i-photo-denah-gerai").files.length == 0 ) {
		alert("Photo denah gerai belum ada");
		showForm2();
		$("i-photo-denah-gerai").focus();
		return false;
	}

	gnamaFilePhotoBelakangGerai = "pblk_" + getCurrentDateTimeString() + '_' + makeID(5);
	gnamaFilePhotoDepanGerai = "pdpn_" + getCurrentDateTimeString() + '_' + makeID(5);
	gnamaFilePhotoDenahGerai = "pdnh_" + getCurrentDateTimeString() + '_' + makeID(5);

	gformData = new FormData();
	gformData.append('nama',dbFriendly(nama));
	gformData.append('ektp',dbFriendly(ektp));
	gformData.append('npwp',dbFriendly(npwp));
	gformData.append('telp',dbFriendly(telp));
	gformData.append('email',dbFriendly(email));
	gformData.append('namagerai',dbFriendly(namaGerai));
	gformData.append('alamatgerai',dbFriendly(alamatGerai));
	gformData.append('ukurangerai',dbFriendly(ukuranGerai));
	gformData.append("jaraksesamaagenpos", dbFriendly(jarakSesamaAgenPos));
	gformData.append('datapesaing',dbFriendly(dataPesaing));
	gformData.append('filenamephotobelakanggerai', dbFriendly(gnamaFilePhotoBelakangGerai));
	gformData.append('filenamephotodepangerai', dbFriendly(gnamaFilePhotoDepanGerai));
	gformData.append('filenamephotodenahgerai', dbFriendly(gnamaFilePhotoDenahGerai));
	gformData.append('idprovinsi', dbFriendly(getSelectedOptionValue($('i-provinsi'))));
	gformData.append('namaprovinsi', dbFriendly(getSelectedOptionText($('i-provinsi'))));
	gformData.append('idkota', dbFriendly(getSelectedOptionValue($('i-kota'))));
	gformData.append('namakota', dbFriendly(getSelectedOptionText($('i-kota'))));

	return true;
}

function uploadPhotoGerai(viHasilProses) {
	
	show('form2-action-button');
	hide('form2-info-upload');

	if (viHasilProses === "OK") {
		console.log(viHasilProses);

		//First attempt : gagal 
		var photo = $("i-photo-depan-gerai").files[0];
		resizeImage(photo, uploadImage,500,500,"depan");

		var photo = $("i-photo-belakang-gerai").files[0];
		resizeImage(photo, uploadImage,500,500,"belakang");

		var photo = $("i-photo-denah-gerai").files[0];
		resizeImage(photo, uploadImage,500,500,"denah");

		//Second attempt : biasanya berhasil, hah ini memang cara koboy :)	
		var photo = $("i-photo-depan-gerai").files[0];
		resizeImage(photo, uploadImage,500,500,"depan");

		var photo = $("i-photo-belakang-gerai").files[0];
		resizeImage(photo, uploadImage,500,500,"belakang");

		var photo = $("i-photo-denah-gerai").files[0];
		resizeImage(photo, uploadImage,500,500,"denah");

		infoSelesai();

	} else {
		infoPerbaikan(viHasilProses);
	}
}


function uploadImage(viBase64Img, viInfo) {
	var fileName = "";
	if (viInfo === "depan") {
		fileName = gnamaFilePhotoDepanGerai;
	}
	if (viInfo === "belakang") {
		fileName = gnamaFilePhotoBelakangGerai;
	}
	if (viInfo === "denah") {
		fileName = gnamaFilePhotoDenahGerai
	}
	var formData = new FormData();
	formData.append('photo',viBase64Img);
	formData.append('filename', fileName);
	postData(API_SAVE_IMG_URL, formData, null);
}

function showHideKeuntungan() {
	if (gisDisplayKeuntungan) {
		hide('keuntungan');
		gisDisplayKeuntungan = false;
	} else {
		show('keuntungan');
		$('keuntungan').style.top = "120px";
		widthHeader = getDivWidth('header');
		widthKeuntungan = getDivWidth('keuntungan');
		selisih = widthHeader - widthKeuntungan;
		adjustment = Math.round(selisih/2);
		console.log("width header :" + widthHeader);
		console.log("width keuntungan :" + widthKeuntungan);
		console.log("width selisih :" + selisih);

		hide('keuntungan');

		left = getLeft($('header'));
		console.log("left =" + left);
		left = left + adjustment + 14;
		left = left + "px";
		$('keuntungan').style.left = left;
		show('keuntungan');
		gisDisplayKeuntungan = true;
	}	
}

function repositionKeuntungan() {
	if (gisDisplayKeuntungan) {
		showHideKeuntungan(); //Close it
		showHideKeuntungan(); //Reopen it
	}	
}

function perbaikan() {
	hide("info");
	showForm1();
}

function infoPerbaikan(viHasilProses) {
	hide('form1');
	hide('form2');
	hide('keuntungan');
	$('info-text-content').innerHTML = "Pendaftaran Paketku Pos<br><br><strong>GAGAL</strong><br><br>" + viHasilProses + "<br><br>";
	show('info');
	hide('info-selesai');
	show('info-perbaikan');
}

function infoSelesai() {
	hide('form1');
	hide('form2');
	hide('keuntungan');
	$('info-text-content').innerHTML = "Pendaftaran Paketku Pos<br><br><strong>BERHASIL</strong><br><br>Admin kami akan segera<br>menghubungi Anda";
	show('info');
	show('info-selesai');
	hide('info-perbaikan');
}

function getProvince() {
	var formData = new FormData();
	formData.append('nothing','');
	postData(API_GET_PROVINCE, formData, showProvince);
}

function showProvince(viProvince) {
	var province = JSON.parse(viProvince);
	provinceSelect = $('i-provinsi');
	console.log(province);
	for(var i = 0; i < province.provinces.length; i++) {
	    var theProvince = province.provinces[i];
	    addOption($('i-provinsi'), theProvince.province_name.toUpperCase(), theProvince.id);
	}
}

function getCity() {
	removeAllOptions($('i-kota'));
	addOption($('i-kota'), 'Loading kota...', '0');
	var provinceID = $("i-provinsi").value;
	var formData = new FormData();
	formData.append('provinceid',provinceID);
	postData(API_GET_CITY, formData, showCity);
}

function showCity(viCity) {
	var city = JSON.parse(viCity);
	console.log(city);
	removeAllOptions($('i-kota'));
	addOption($('i-kota'), 'Pilih Kota', '0');
	for(var i = 0; i < city.cities.length; i++) {
	    var theCity = city.cities[i];
	    addOption($('i-kota'), theCity.city_name.toUpperCase(), theCity.id);  
	}
}

function adjustForm(v) {

}