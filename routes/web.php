<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;
 

/*
|--------------------------------------------------------------------------
| CMS fastku
|--------------------------------------------------------------------------

*/

//graphql
        Route::post('/getlistkurir', 'admin\cms\UserController@getkurirfromcity');
        Route::post('/cekuser', 'admin\cms\UserController@cekuseraktif');
        Route::post('/cekkodepos', 'admin\cms\UserController@getlocationfrompostcode');
        Route::post('/refcap', 'admin\cms\UserController@getrefreshcaptcha');
        Route::post('/test', 'admin\cms\UserController@testlogin');
        Route::post('/guzzletest', 'admin\cms\UserController@testguzzle');
        Route::get('/test1', 'admin\cms\TestController@testresult');
         Route::get('/testpay', 'admin\cms\TestController@getpayment');

        //CAPTCHA & password
        Route::get('/ambilcaptcha', 'admin\cms\LoginController@getcaptch');
        Route::get('/reloadcaptcha', 'admin\cms\LoginController@refreshcaptch');
        Route::post('/daftar', 'admin\cms\LoginController@register');
        Route::post('/sendotp', 'admin\cms\LoginController@verifyotp');
        Route::post('/resendotp', 'admin\cms\LoginController@resendotp');
        Route::post('/forgot', 'admin\cms\LoginController@forgotpass');


        //get kategori
        Route::get('/getkategori', 'admin\cms\UserController@getchoicecat');
        Route::get('/getpackage', 'admin\cms\UserController@getchoicepackage');
        Route::get('/getpayment', 'admin\cms\UserController@getpaymentchannel');
        Route::get('/getid', 'admin\cms\UserController@getshipmentid');
        Route::post('/setkurir', 'admin\cms\UserController@setkurirforshipment');





Route::prefix('/')->group(function() {
    // Route::get('login', function() {
    //        return view('admin.login.index');
    // });
    Route::get('login', 'admin\cms\LoginController@getlogin'); 
    // Route::post('/login', 'admin\cms\UserController@userlogin');
    Route::post('/login', 'admin\cms\LoginController@login');
    // Route::get('/logout', 'admin\cms\LoginController@logoutuser');

    Route::get('/logout', function() {
        $session = new App\Http\Helpers\SessionHelper();
        log::info("session = ".$session->get('session_id'));
        // $session->remove();
        $helpdeskbl = new App\BusinessLogic\HelpdeskBL();
        $helpdeskbl->logoutjwt($session->get('session_id'));
 
        return redirect('/fastku/login');  
    });

    Route::middleware(['admin_auth'])->group(function () {



 //dashboard fastku
         
         Route::get('/', 'admin\cms\UserController@dash2');
         Route::get('/homeindex', 'admin\cms\UserController@dash2');
         Route::get('/dashboard', 'admin\cms\UserController@dash');
         Route::get('/inputtrx', 'admin\cms\UserController@getkirim');
         Route::get('/pengaturan', 'admin\cms\UserController@getpengaturan');
         Route::get('/pengaturanalamat', 'admin\cms\UserController@getpengaturanalm');
         Route::post('/savedatatrx', 'admin\cms\UserController@savetrxkurir');


         Route::get('/report', 'admin\cms\ReportController@reportamanah');
         Route::get('/exportreport', 'admin\cms\ReportController@expreportamanah');




Route::get('/sumreport', 'admin\cms\SummaryController@sumreportamanah');
Route::get('/exportsumreport', 'admin\cms\SummaryController@expsumreportamanah');

        //list agent

        Route::get('/verifikasi' , 'admin\cms\AgentController@verified');
        Route::get('/listuser' , 'admin\cms\AgentController@index');
         Route::get('/gerai_view_{agentId}', 'admin\cms\AgentController@view');
         Route::post('/process_verified', 'admin\cms\AgentController@updateVerified');
         Route::post('/update_agent_save.html', 'admin\cms\AgentController@updateProfile');
         Route::get('/agent_export.html', 'admin\cms\AgentController@exportAgen');
         

//report
         Route::get('/summary', 'admin\cms\SummaryController@sumreportpaketku');


//----end of paketkupos
 
    });
 });
