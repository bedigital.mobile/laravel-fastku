<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Keuntungan Agent PaketKuPOS</title>
<link rel="stylesheet" type="text/css" href="css/registrasi.css">
</head>
<body>
	<div class="top-logo, parent">
		<div class="row,child" style="background-color: #A3C2DD; max-height:190px;">
			<img src="https://helpdesk.posfin.id/paketkupos/img/Logo_paketku_koper.png" style="width:100%; object-fit: contain;">
		</div>
	</div>
	<div class="container" id="form1" style="background-color: #0656A2;">
    	<!-- form data --> 
    	<div class="row" style="margin-bottom: 30px;">
      		<div style="font-size:125%; color:#EEE;">
        		<div>Gabung agen PaketKUPOS</div>
            <div>dari PT Pos Indonesia,</div>
            <div>banyak untungnya! </div>
      		</div>
    	</div>
    	<div class="row" style="color:#DEDEDE;font-size:80%;">
      		<div >
            <div style="margin-bottom: 5px;">- Jaringan terluas hingga pelosok daerah</div> 
            <div style="margin-bottom: 5px;">- Syarat dan proses mudah</div>
            <div style="margin-bottom: 5px;">- Modal kecil</div>
            <div style="margin-bottom: 5px;">- Tarif lebih murah</div>
            <div style="margin-bottom: 5px;">- Target ringan</div>
            <div style="margin-bottom: 5px;">- Bisa gabung dengan bisnis lain</div>        		
      		</div>
    	</div>
    	<div class="row">
    		&nbsp;
    	</div>   	
    	<div class="row">
    		&nbsp;
    	</div>   	
    	<div class="row, parent">
    		<div class="child">
	      		<input type="button" class="action-button" value="DAFTAR SEKARANG" onclick="location.href='https://helpdesk.posfin.id/paketkupos/registrasi';">
			</div>
    	</div>
    	<div class="row">
    		&nbsp;
    	</div>   	
  </div>
</body>
</html> 