<!DOCTYPE html>
<html>
<head>
	<title>TVONENEWS CMS</title>	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="css/cms/base.css">
	<link rel="stylesheet" href="css/cms/reset.css">
	<link rel="stylesheet" href="css/cms/app.css">
	<!-- javascript and css holder for descendant blades -->
	<!-- ... -->
	@yield('module_head_library');
</head>
<body>
	<!-- Layout -->
	<div class="header">
		<img id="app-menu-icon" src="img/cms/app/logo.png" class="header-menu-icon">
		<div id="header-title" class="header-title">
			TVONENEWS CMS
		</div>
		<div id="header-user" class="header-user">
			Amin Nurahman
		</div>
	</div>
	<div class="content">
		<!-- Content Below Here -->
		@yield('module_content')
	</div>
	<div class="footer">
		<div id="footer-info" class="footer-info">Ready</div>
		<div id="footer-datetime" class="footer-datetime"></div>
	</div>
	<!-- /Layout -->

	<!-- UI Component -->
	<!-- Vertical Menu -->
	<div id="menu-holder" style="display: none;">
		<div id="menu" class="menu">
			<div id="menu-caption" class="menu-caption">
				<div class="menu-title"> &nbsp;&nbsp;Administrator Menu </div>
			</div>
			<div id="menu-content" class ="menu-content">
				<ul id="menu-list">
					<li id="menu-upload-video">
						<img id="icon-menu-upload-video" src="img/cms/app/upload.png" width="24px" height="24px">
						<div id="caption-menu-upload-video" class='menu-item-text'>Upload Video</div>
					</li>
					<li id="menu-vod-list">
						<img id="icon-menu-vod-list" src="img/cms/app/video-list.png" width="24px" height="24px">
						<div id="caption-menu-vod-list" class='menu-item-text'>VOD List</div>
					</li>
					<li id="menu-video-category">
						<img id="icon-menu-video-category" src="img/cms/app/catagory.png" width="24px" height="24px">
						<div id="caption-menu-video-category" class='menu-item-text'>Video Category</div>
					</li>
					<li id="menu-ads">
						<img id="icon-menu-ads" src="img/cms/app/ads.png" width="24px" height="24px">
						<div id="caption-menu-ads" class='menu-item-text'>Advertisement</div>
					</li>
					<li id="menu-video-tag">
						<img id="icon-menu-video-tag" src="img/cms/app/tag.png" width="24px" height="24px">
						<div id="caption-menu-video-tag" class='menu-item-text'>Video Tag</div>
					</li>
					<li id="menu-dashboard">
						<img id="icon-menu-dashboard" src="img/cms/app/dashboard.png" width="24px" height="24px">
						<div id="caption-menu-dashboard" class='menu-item-text'>Dashboard</div>
					</li>
					<li id="menu-report">
						<img id="icon-menu-report" src="img/cms/app/report.png" width="24px" height="24px">
						<div id="caption-menu-report" class='menu-item-text'>Report</div>
					</li>
					<li id="menu-activity-monitoring">
						<img id="icon-menu-activity-monitoring" src="img/cms/app/log.png" width="24px" height="24px">
						<div id="caption-menu-activity-monitoring" class='menu-item-text'>Activity Monitoring</div>
					</li>
					<li id="menu-thirdparty_api">
						<img id="icon-menu-thirdparty_api" src="img/cms/app/thirdparty.png" width="24px" height="24px">
						<div id="caption-menu-thirdparty_api" class='menu-item-text'>Third Party API Settings</div>
					</li>
					<li id="menu-user-access">
						<img id="icon-menu-user-access" src="img/cms/app/users.png" width="24px" height="24px">
						<div id="caption-menu-user-access" class='menu-item-text'>User & Access</div>
					</li>
					<li id="menu-setting">
						<img id="icon-menu-setting" src="img/cms/app/settings.png" width="24px" height="24px">
						<div id="caption-menu-setting" class='menu-item-text'>Setting</div>
					</li>
					<li id="menu-logout">
						<img id="icon-menu-logout" src="img/cms/app/logout.jpg" width="24px" height="24px">
						<div id="caption-menu-logout" class='menu-item-text'>Logout</div>
					</li>
				</ul> <!-- end : menu-list -->
			</div> <!-- end : menu-content -->
		</div> <!-- end : menu --> 
	</div> <!-- end : menu-holder -->	

	<!-- Moveable Dialog -->
	<div id="dialog-holder" style="display: none;">
		<div id="dialog" class="dialog">
		  	<div id="dialog-header" class="dialog-header">
		    	<div id="dialog-caption" class="dialog-caption">Click here to move</div>
		    	<div id="dialog-toggle" class="dialog-toggle">&#x2715;</div>
		  	</div>
		  	<div id="dialog-content" class="dialog-content">
		    	  <!-- dialog content will be here -->   
		  	</div>
		</div>
	</div>	
	<!-- Other UI Component Below Here -->
	<!-- ... -->
	@yield('module_ui')
	@php
	   $pdt = date('Ymd').time();
	@endphp
	<script src="js/cms/util.js?v={{$pdt}}"></script>
	<script src="js/cms/app.js?v={{$pdt}}"></script>
	<!-- Other Javascript Includes Below Here -->
	@yield('module_script')
</body>	
</html>