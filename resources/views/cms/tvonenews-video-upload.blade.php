@extends('cms.tvonenews-main')

@section('module_head_library')
	<!-- Nothing to add -->
@endsection

@section('module_content')
	<!-- Module Header --> 
	<div id="module-header" style="width:100%; height:34px; background-color: #c1c1c1; top:52px;">
		<img id="module-upload-menu" src="img/cms/app/menu.svg" style="width: 24px; height: 24px; margin-top: 5px; float: left; padding-left: 10px; cursor: pointer;">
		<div id="module-upload-title" style="padding-top: 10px; padding-left: 5px; position: relative; float: left; font-size: 85%;">Upload Video</div>
	</div>

	<!-- Form Upload -->
	<div id="video-upload-form" style="padding-left:10px; padding-top: 10px; font-size: 85%; display: none;">
		<div style="border-bottom: 1px solid #CDCDCD; margin-bottom: 10px; padding-bottom: 5px; font-weight: bold;">Video Upload Form</div>
		<div style="overflow:auto;" class="hide-scroll">
			<table style="margin-left: 10px; width: 725px; font-size: 85%;">
				<tr>
					<td style="width:100px;">File</td>
					<td style="width:625px;">
						<input id ="vuf-video-file" type="file" accept="video/*"/>
						<div style="margin-bottom: 15px;"></div>	
					</td>
				</tr>
				<tr>
					<td colspan="2"><div style="margin-bottom: 10px;">Meta Data (Can be defined later)</div></td>
				</tr>
				<tr>
					<td style="width:100px;padding-left: 10px;">Image Small</td>
					<td style="width:625px;">
						<input id ="vuf-video-image-small" type="file" accept="image/*"/>
						<div style="margin-bottom: 5px;"></div>	
					</td>
				</tr>
				<tr>
					<td style="width:100px;padding-left: 10px;">Image Medium</td>
					<td style="width:625px;">
						<input id ="vuf-video-image-medium" type="file" accept="image/*"/>
						<div style="margin-bottom: 5px;"></div>	
					</td>
				</tr>
				<tr>
					<td style="width:100px;padding-left: 10px;">Image Large</td>
					<td style="width:625px;">
						<input id ="vuf-video-image-besar" type="file" accept="image/*"/>
						<div style="margin-bottom: 5px;"></div>	
					</td>
				</tr>
				<tr>
					<td style="width: 100px;padding-left: 10px;">Title</td>
					<td style="width: 625px;">
						<input type="text" id="vuf-i-title" style="width:300px;height:22px;padding-left:5px;font-size:9pt;">
						<div style="margin-bottom: 5px;"></div>
					</td>
				</tr>
				<tr>
					<td style="vertical-align: top; width: 100px;padding-left: 10px;">Short Description</td>
					<td style="width: 625px;">
						<textarea style="width:375px;height:85px;font-size:9pt;padding-left:5px;"></textarea>
						<div style="margin-bottom: 5px;"></div>
					</td>
				</tr>
				<tr>
					<td style="vertical-align: top; width: 100px; padding-left: 10px;">Long Description</td>
					<td style="width: 625px;">
						<textarea style="width:375px;height:85px;font-size:9pt;padding-left:5px;"></textarea>
						<div style="margin-bottom: 5px;"></div>
					</td>
				</tr>
				<tr>
					<td style="width: 100px; padding-left: 10px;">Category</td>
					<td style="width: 625px;">
						<select id="video-category">
							<option value="">Please select</option>
							<option value="1">Indonesia Lawyer Club</option>
							<option value="2">Apa Kabar Indonesia Pagi </option>
							<option value="3">Apa Kabar Indonesia Siang </option>
							<option value="4">Apa Kabar Indonesia Malam </option>
							<option value="5">Dua Sisi </option>
						</select>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<div style="margin-top: 20px;">
							<input class="button" type="button" value="Upload" id="vuf-btn-upload" style="background-color: #37CA41;">&nbsp;
							<input class="button" type="button" value="Clear" id="vuf-btn-clear">
						</div>
					</td>
				</tr>
			</table>				
		</div>

	</div>

	<!-- Video Upload Status -->
	<div id="video-upload-status" style="padding-top:5px; padding-left: 10px;">
		<div id="module-upload-title" style="padding-top: 5px; padding-left: 5px; position: relative; font-size: 85%; border-bottom: 1px solid #CDCDCD; margin-bottom: 5px; padding-bottom: 5px; font-weight: bold;">Video Upload Status</div>	
		<div style="margin-left:5px;">		
			<input class="button" type="button" value="In Progress" id="vus-btn-inprogress" style="font-size: 80%;">&nbsp;
			<input class="button" type="button" value="Failed" id="vus-btn-failed" style="font-size: 80%;">&nbsp;
			<input class="button" type="button" value="Done" id="vus-btn-done" style="font-size: 80%;">&nbsp;
		</div>	
		<div id="vus-list-title" style="margin-top:12px; font-size: 80%; font-weight:bold; border-bottom: 1px dotted #DADEE4;padding-bottom: 7px; margin-left: 5px;">List of In Progress Upload Video</div>

		<table style="margin-top: 10px;margin-left: 15px; font-size: 80%; height: 100px; display: block;">
			<tr>
				<td>Filter</td>
				<td>&nbsp;:&nbsp;</td>
				<td>
					<input type="text" id="vusf-filter" style="width:180px;height:22px;padding-left:5px;font-size:9pt;">
					<div style="margin-bottom:5px;"></div>
				</td>
			</tr>
			<tr>
				<td>Category</td>
				<td>&nbsp;:&nbsp;</td>
				<td>
					<select id="video-category">
						<option value="">Please select</option>
						<option value="1">Indonesia Lawyer Club</option>
						<option value="2">Apa Kabar Indonesia Pagi </option>
						<option value="3">Apa Kabar Indonesia Siang </option>
						<option value="4">Apa Kabar Indonesia Malam </option>
						<option value="5">Dua Sisi </option>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<div style="padding-top: 10px;"></div>
					<input class="button" type="button" value="Search" id="vusf-btn-search">
				</td>
			</tr>
		</table>
		<div id="video-status-list" style="margin-top:5px;height: 350px;width: calc(100%-20px); margin-right: 5px; overflow-x: hidden;
		 overflow-y: auto; overflow-x: hidden;">
			<ul id="item-list" class="item_list" style="font-size: 85%;">

				<li class="data-list-item" id="data1" onclick="markZebraList('item-list','data1');" style="margin-right:5px;">  
					<div style="font-weight: bold; width: 400px; margin-bottom: 7px;">Indonesia Lawyer Club Episode 1</div>
					<div style="overflow: auto;" class="hide-scroll">
						<table style="width:760px;" >
							<tr>
								<td style="width:150px; vertical-align: top; padding-right: 10px;">
									<div style="margin-top:5px;"></div>
									<img src="" style="width:150px; height:90px;">
								</td>
								<td style="width:600px; vertical-align: top;">
									<table style="font-size: 75%;width:600px;">
										<tr>
											<td style="width:100px;">
											<div style="margin-top:5px;"></div>
											Short Desc
											</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; padding-left: 5px;word-wrap: break-word;"><p>Indonesia Lawyers Club (ILC) Episode Perdana.</p></td>
										</tr>
										<tr>
											<td style="width:100px;">
											<div style="margin-top:3px;"></div>
											Long Desc
											</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; padding-left: 5px;word-wrap: break-word;"><p>Membahas tentang Pilpress 2024. Permasalahan, konflik, cerita menarik dan hal-hal kontroversi yang terjadi selama Pilpress 2024. Disertai tanggapan dan sanggahan para panelis terpilih dengan latar belakang keilmuan yang mumpuni.</p></td>
										</tr>
										<tr>
											<td style="width:100px;">
											Original
											</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all;padding-left: 5px;" >
												<div style="margin-top:3px;"></div>
												ILC-Karni-Ilyas-Episode-1.r3d
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Stored</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												20201115092300abc.r3d
											</td>
										</tr>
										<tr>
											<td style="width:100px;">File Size</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												1.5 GB
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Uploaded At</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												2020-11-15 09:23:00
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Uploaded By</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												Amin Nurahman
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Upload Status</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												Done
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Encoded Status</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												Done
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Encoded File</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												https://aswsdfasf/asfasfssf/12344324/adfasfas/sdfsafsasdf
											</td>
										</tr>										
									</table>
								</td>
							</tr>
						</table>							
					</div>
				</li>


				<li class="data-list-item" id="data2" onclick="markZebraList('item-list','data2');" style="margin-right:5px;">  
					<div style="font-weight: bold; width: 400px; margin-bottom: 7px;">Indonesia Lawyer Club Episode 2</div>
					<div style="overflow: auto;" class="hide-scroll">
						<table style="width:760px;" >
							<tr>
								<td style="width:150px; vertical-align: top; padding-right: 10px;">
									<div style="margin-top:5px;"></div>
									<img src="" style="width:150px; height:90px;">
								</td>
								<td style="width:600px; vertical-align: top;">
									<table style="font-size: 75%;width:600px;">
										<tr>
											<td style="width:100px;">
											<div style="margin-top:5px;"></div>
											Short Desc
											</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; padding-left: 5px;word-wrap: break-word;"><p>Indonesia Lawyers Club (ILC) Episode Perdana.</p></td>
										</tr>
										<tr>
											<td style="width:100px;">
											<div style="margin-top:3px;"></div>
											Long Desc
											</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; padding-left: 5px;word-wrap: break-word;"><p>Membahas tentang Pilpress 2024. Permasalahan, konflik, cerita menarik dan hal-hal kontroversi yang terjadi selama Pilpress 2024. Disertai tanggapan dan sanggahan para panelis terpilih dengan latar belakang keilmuan yang mumpuni.</p></td>
										</tr>
										<tr>
											<td style="width:100px;">
											Original
											</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all;padding-left: 5px;" >
												<div style="margin-top:3px;"></div>
												ILC-Karni-Ilyas-Episode-1.r3d
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Stored</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												20201115092300abc.r3d
											</td>
										</tr>
										<tr>
											<td style="width:100px;">File Size</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												1.5 GB
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Uploaded At</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												2020-11-15 09:23:00
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Uploaded By</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												Amin Nurahman
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Upload Status</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												Done
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Encoded Status</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												Done
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Encoded File</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												https://aswsdfasf/asfasfssf/12344324/adfasfas/sdfsafsasdf
											</td>
										</tr>										
									</table>
								</td>
							</tr>
						</table>							
					</div>
				</li>

				<li class="data-list-item" id="data3" onclick="markZebraList('item-list','data3');" style="margin-right:5px;">  
					<div style="font-weight: bold; width: 400px; margin-bottom: 7px;">Indonesia Lawyer Club Episode 3</div>
					<div style="overflow: auto;" class="hide-scroll">
						<table style="width:760px;" >
							<tr>
								<td style="width:150px; vertical-align: top; padding-right: 10px;">
									<div style="margin-top:5px;"></div>
									<img src="" style="width:150px; height:90px;">
								</td>
								<td style="width:600px; vertical-align: top;">
									<table style="font-size: 75%;width:600px;">
										<tr>
											<td style="width:100px;">
											<div style="margin-top:5px;"></div>
											Short Desc
											</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; padding-left: 5px;word-wrap: break-word;"><p>Indonesia Lawyers Club (ILC) Episode Perdana.</p></td>
										</tr>
										<tr>
											<td style="width:100px;">
											<div style="margin-top:3px;"></div>
											Long Desc
											</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; padding-left: 5px;word-wrap: break-word;"><p>Membahas tentang Pilpress 2024. Permasalahan, konflik, cerita menarik dan hal-hal kontroversi yang terjadi selama Pilpress 2024. Disertai tanggapan dan sanggahan para panelis terpilih dengan latar belakang keilmuan yang mumpuni.</p></td>
										</tr>
										<tr>
											<td style="width:100px;">
											Original
											</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all;padding-left: 5px;" >
												<div style="margin-top:3px;"></div>
												ILC-Karni-Ilyas-Episode-1.r3d
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Stored</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												20201115092300abc.r3d
											</td>
										</tr>
										<tr>
											<td style="width:100px;">File Size</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												1.5 GB
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Uploaded At</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												2020-11-15 09:23:00
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Uploaded By</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												Amin Nurahman
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Upload Status</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												Done
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Encoded Status</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												Done
											</td>
										</tr>
										<tr>
											<td style="width:100px;">Encoded File</td>
											<td style="width:5px;">&nbsp;:&nbsp;</td>
											<td style="width:495px; word-wrap: break-all; padding-left: 5px;">
												<div style="margin-top:3px;"></div>
												https://aswsdfasf/asfasfssf/12344324/adfasfas/sdfsafsasdf
											</td>
										</tr>										
									</table>
								</td>
							</tr>
						</table>							
					</div>
				</li>					
			</ul>
		</div>
	</div>
@endsection

@section('module_ui')
	<!-- Module Menu Upload-->
	<div id="module-menu-upload-holder" style="display: none;">
		<div id="module-menu-upload" class="menu">
			<div id="module-menu-upload-caption" class="menu-caption">
				<div class="menu-title"> &nbsp;&nbsp;Upload Video Menu </div>
			</div>
			<div id="module-menu-upload-content" class ="menu-content">
				<ul id="module-menu-upload-list">
					<li id="menu-upload-video-new">
						<img id="icon-menu-upload-video-new" src="img/cms/app/upload.png" width="24px" height="24px">
						<div id="caption-menu-upload-video-new" class='menu-item-text'>New Upload Video</div>
					</li>
					<li id="menu-upload-video-search" style="display: none;">
						<img id="icon-menu-upload-video-search" src="img/cms/app/video-list.png" width="24px" height="24px">
						<div id="caption-menu-upload-video-search" class='menu-item-text'>Search Upload</div>
					</li>
					<li id="menu-upload-video-status">
						<img id="icon-menu-upload-video-status" src="img/cms/app/video-list.png" width="24px" height="24px">
						<div id="caption-menu-upload-video-status" class='menu-item-text'>View Upload Status</div>
					</li>					
				</ul> 
			</div> 
		</div> 
	</div> 	
@endsection

@section('module_script')
	@php
	   $pdt = date('Ymd').time();
	@endphp
	<script src="js/cms/video-upload.js?v={{$pdt}}"></script>
@endsection
