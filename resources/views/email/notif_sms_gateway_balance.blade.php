<p>
Diberitahukan bahwa status SMS GATEWAY <b>{{$gateway_provider}}</b> dengan Gateway-ID : <b>{{$gateway_id}}</b>,  
memiliki balance hampir/sudah habis atau tanggal expiry yang hampir/sudah kadaluarsa.<br/>
Detail sebagai berikut:<br/>
<ul>
<li>Balance: <b>Rp {{number_format($balance,2,',','.')}}</b></li>
<li>Expiry: <b>{{$expiry}}</b></li>
</ul>
<br/>
Segera cek di Dashboard SMS GATEWAY <b>{{$gateway_provider}}</b> dan segera topup deposit ke <b>{{$gateway_provider}}</b>.
</p>
