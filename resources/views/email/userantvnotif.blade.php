@extends('email.layoutposfin')

@section('email_title', 'Posfin User Information')

@section('email_content') 

  
 Hi {{$fullname}}, <br>
<p>Selamat! Akun Posfin Anda sudah aktif dengan ID Pengguna: <b>{{$username}}</b>.   
Langkah mudah untuk mengakses/menggunakan akun Anda: </p>
<ol type="1">
	<li>Download aplikasi POSFIN pada Google Playstore atau mengakses melalui web : <a href="https://agen.posfin.id" target="_blank">https://agen.posfin.id</a> </li>
	<li>Klik tombol "Lupa Kata Sandi?" di aplikasi Android atau "Lupa Password" di web POSFIN.</li>
	<li>Masukkan ID Pengguna : [Nomer HP Karyawan] misalnya : 62811100999 dan ikuti langkah selanjutnya. </li>
	<li>Akun POSFIN Anda siap digunakan</li>
</ol>

<p>
Video panduan untuk mengakses POSFIN bisa dilihat di: <a href=" https://www.youtube.com/watch?v=QinIQHQSMN0&feature=youtu.be"> link video </a></p>
<br>
Panduan lengkap lainnya ada di Web POSFIN, menu Beranda – “Support” – “Document Support”.
<p>Terima kasih sudah mendaftar dan gunakan terus POSFIN sebagai solusi pembayaran Anda.</p> 
<br>
Salam sukses. 
@endsection  

 	