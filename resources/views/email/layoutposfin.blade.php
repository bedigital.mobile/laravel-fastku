<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, user-scalable=no" />
<title></title>
<style type="text/css">
a { color: #0085c3; text-decoration: none;}
p { margin: 1em 0;}
#outlook a { padding: 0;} 
body { width: 100% !important;}
.ReadMsgBody { width: 100%;}
.ExternalClass { width: 100%;}
body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none;}
body { margin: 0; padding: 0;}
img { font-family: "Trebuchet MS", Helvetica, sans-serif;}

@media only screen and (max-width: 480px) {
table[class=per100] { position: relative; top: 0; left: 0; right: 0; width: 100% !important; height: auto !important;}
table[class=borderPerTab] { position: relative; top: 0; left: 0px; right: 0px; width: 98% !important; height: auto !important;}
table[class=borderPerTab90] { position: relative; top: 0; left: 0px; right: 0px; width: 90% !important; height: auto !important;}
td[class=tdp5] { padding: 0px 5px !important;}
td[class=td_pad] { width: 10px !important;}
td[class=colsplit] { width: 100% !important; float: left !important;}
td[class=colsplitAL] { text-align: left; width: 100% !important; float: left !important;}
td[class=per50] { width: 50% !important; float: left !important;}
img[class=imgPer50] { width: 50% !important;}
td[class=menuTd] { width: 100% !important; float: left !important; font-size: 14px !important; height: 24px !important; line-height: 24px !important; border-bottom: 1px solid #ffffff !important;}
td[class=menuTdlast] { width: 100% !important; float: left !important; font-size: 14px !important; height: 24px !important; line-height: 24px !important; border-bottom: noe !important;}
td[class=texC] {
  text-align: center !important;
  clear: both !important;
}
td[class=email_box] {
  width: 100% !important;
  float: left !important;
}
td[class=emailTd] {
  width: 100% !important;
  float: left !important;
  text-align: center !important;
  padding: 0px;
}
td[class=emailTdW] {
  width: 100% !important;
  float: left !important;
  border-bottom: 1px solid #ffffff !important;
}
td[class=emailTdLast] {
  width: 100% !important;
  float: left !important;
  padding: 10px 0px !important;
  text-align:center !important;
}
img[class=tel] {
  width: 100% !important;
  height: auto !important;
}
img[class=emailImg] {
  width: 100% !important;
  height: 100px !important;
  display: block !important;
  text-align: center !important;

}
img[class=footerMenu] {
  width: 100% !important;
  height: auto !important;
  display: block !important;
}
table[class=noneMobile], tr[class=noneMobile], td[class=noneMobile], img[class=noneMobile], span[class=noneMobile] {
  display: none !important;
}
table[class=showMobile], tr[class=showMobile], td[class=showMobile], img[class=showMobile], span[class=showMobile] {
  display: block !important;
}
td[class=vspacer15], img[class=vspacer15] {
  width: 15px !important;
}
td[class=vspacer10], img[class=vspacer10] {
  width: 10px !important;
}
td[class=vspacer5], img[class=vspacer5] {
  width: 5px !important;
}
img[class=imgReplaceBox01] {
  width: 100% !important;
  height: 7px !important;
  column-span: all !important;
}
td[class=colsplit] {
  width: 100% !important;
  float: left !important;
}
td[class=colsplitSpacer] {
  width: 100% !important;
  height: 10px !important;
  float: left !important;
}
table[class=tabCenter] {
  width: 100% !important;
}
/*---*/
} 
</style>
</head>
<body style="-webkit-text-size-adjust:none; font-family: 'Helvetica', Arial, sans-serif">

<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td align="center" valign="top"><table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="borderPerTab" style="border:1px solid #EDEDED">
        <tr>         
          <td bgcolor="#dfe0e2"><!--height 25--><!--height 20-->
<!----Logo---->
<table width="100%" border="0" cellpadding="0" cellspacing="0"> 
    <tr>
      <td bgcolor="#20699e" height="15px">         
      </td> 
    </tr>  
    <tr>
      <td align="left" bgcolor="#fff">
         <img class="emailImg" src="https://helpdesk.posfin.id/ma/img/header_posfin.jpg" style="border:0;" />
      </td>
    </tr> 
</table>
 
<!----Receipt---->

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="5"> </td>
  </tr>
</table>

<!----Receipt---->
<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
              <tr>
                <td valign="top" class="vspacer15" width="45"> </td>

                <td align="center" valign="top">
                
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr> 
                      <td align="left" height="20"></td>
                    </tr>
                  </table>
                       
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr> 
                      
                      <td width="55%" align="left" style="font-size:14px; font-weight:bold; color:#00af41">
                        
                      </td>
                    </tr>
                  </table> 
                
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="200" class="emailTd">    
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
      
  </table>
  </td> 
    <td valign="top" width="100%" class="emailTd">    
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top" class="tdp5" style="font-size:14px; font-weight:bold; color:#20699e">@yield('email_title')</td>
    </tr>
    <tr>
    <td align="center" valign="middle" height="10" class="img_1"> </td>
    </tr>

  <tr>
    <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="border:1px solid #dddddd">
        <tr>
          <td align="left" valign="top" style="padding:0cm 0cm 0cm 0cm;">
            
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td class="t3_1" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
    
        <tr>
              <td align="left" valign="top" class="tdp5">
                  <table border="0" cellspacing="0" cellpadding="0" width="100%">
                  <tr>
                      <td height="10px" align="left" class="tdp5"></td>
                      <td height="10px" colspan="2" align="left" class="tdp5"></td>
                      <td height="10px" align="left" class="tdp5"></td>
                    </tr>
                  <tr>
                      <td height="5px" align="left" class="tdp5"></td>
                      <td height="5px" colspan="2" align="left" class="tdp5" style="font-size:11px; line-height:20px; font-family:''Helvetica', Arial, sans-serif;-webkit-text-size-adjust:none; font-weight:bold; color:#9E9E9E">
                 		@yield('email_content')   
                      </td>
                      <td height="5px" align="left" class="tdp5"></td>
                    </tr>
                    <tr> 
                      <td align="left" class="tdp5" width="15"></td>
                      <td align="right" class="tdp5" style=" font-family:'Helvetica, 'Arial', sans-serif;-webkit-text-size-adjust:none;font-weight:normal;color:#000000;">
                      <span style="font-size:12px; font-weight:bolder; color:#000000; line-height:28px;"> &nbsp;&nbsp;&nbsp;&nbsp;</span>
                      </td>
                      <td align="left" class="tdp5" style=" font-family:'Helvetica, 'Arial', sans-serif;-webkit-text-size-adjust:none;font-weight:normal;color:#000000;">
                      <span style="font-size:12px; font-weight:bolder; color:#000000; line-height:28px;"> </span>
                      </td>
                      <td align="left" class="tdp5" width="15"></td>
                    </tr>
                    
                  </table>
              </td>
            </tr>
          <tr>
              <td align="left" valign="top" class="tdp5">
                  <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr> 
                      <td align="left" class="tdp5" style=" font-family:'Helvetica, 'Arial', sans-serif;-webkit-text-size-adjust:none;font-weight:normal;color:#000000;"> </td>
                    </tr>
                  </table>
              </td>
            </tr> 
  </table>
            </td>
              </tr>
            </table></td>
        </tr>
    </table></td>
  </tr>
  </table>
  </td>

  </tr>
</table>

 <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody><tr>
          <td height="20"></td>
        </tr>
      </tbody></table>

        </td>
                <td valign="top" class="vspacer15" width="45"></td>
  </tr> </table>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="20"></td>
  </tr>
</table>


<!--Footer-->
<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" >
    <tr>
      <td valign="top" class="vspacer15" width="45"></td>
      <td valign="top" style="font-family:  'Helvetica', Arial, sans-serif; color:#000000; font-size:11px;-webkit-text-size-adjust:none;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" style=" font-family:'Helvetica', Arial, sans-serif;-webkit-text-size-adjust:none; text-align:center;">
            
            </td>
           </tr>
           
           <tr>
            <td align="left" style=" font-family:  'Helvetica', Arial, sans-serif;-webkit-text-size-adjust:none; text-align:center;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="55%" align="left" valign="top" class="emailTdLast" style="font-family:'Helvetica', Arial, sans-serif;-webkit-text-size-adjust:none; color:#666666; padding-right:10%;">
  
            <br> <span style="color:#666666;">
<b><i>Customer Service</i></b></span><br>
 Email: <a href="mailto:helpdesk@posfin.id?Subject=Contact%20to%20BWN%20Customer%20Service" target="_blank">helpdesk@posfin.id</a>
<br>
Call Centre : <a href="tel:+62227275001" target="_blank">+62-22-7275001</a><br>
</p>
                     </td>
                      <td width="35%" align="left" valign="top" class="emailTdLast" style="font-family:'Helvetica', Arial, sans-serif;-webkit-text-size-adjust:none; color:#666666">
                         <span style="font-size:10px; line-height:12px; font-weight:bold;"></span><br />
                      </td>
                    </tr>
                  </table>
            </td>
           </tr>
           <tr>
            <td height="30"></td>
           </tr>
                        
 </table></td>
           <td valign="top" class="vspacer15" width="45"></td>
     </tr>
</table>

</td>
 </tr>
</table></td>
  </tr>
</table>

</body>
</html>