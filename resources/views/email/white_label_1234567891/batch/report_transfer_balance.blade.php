@extends('email.white_label_1234567891.layout')

@section('email_title', 'Report Batch Transfer Balance')

@section('email_content')
<p>Hai,</p>
<p>
Sesuai permintaan batch <b>TRANSFER BALANCE KE {{$name}}</b> pada tanggal {{$date}} jam {{$hour}} WIB dengan upload file excel <b>{{$original_filename}}</b>,
maka berikut ini hasilnya:<br/>
<ul>
<li>Total data: {{$total_data}}</li>
<li>Total sukses: {{$total_success}}</li>
<li>Total gagal: {{$total_failed}}</li>
<li>Total terkirim: Rp. {{number_format($total_transferred,2,',','.')}}</li>
</ul>
</p>
<p>Detail dari hasil batch ada di attchment.</p>
@endsection