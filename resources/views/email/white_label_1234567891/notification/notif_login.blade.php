@extends('email.white_label_1234567891.layout')

@section('email_title', 'Login Notification')

@section('email_content')
<p>Hai <b>{{$fullname}}</b>,</p>
<p>
Terima kasih Anda telah menggunakan fasilitas aplikasi <b>POSFIN</b>. Berikut ini adalah informasi transaksi yang telah Anda lakukan:
</p><p>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<td width="20" align="left">Waktu</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$datetime}}</td>
</tr>
<tr>
	<td width="20" align="left">Type</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$user_type}}</td>
</tr>
<tr>
	<td width="20" align="left">ID</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$id}}</td>
</tr>
<tr>
	<td width="20" align="left">Aktivitas</td>
	<td width="1" align="center">:</td>
	<td align="left">Login</td>
</tr>
<tr>
	<td width="20" align="left">Status</td>
	<td width="1" align="center">:</td>
	<td align="left">Sukses</td>
</tr>
<tr>
	<td width="20" align="left">Platform</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$platform}}</td>
</tr>
<tr>
	<td width="20" align="left">Informasi</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$info}}</td>
</tr>
@if (! empty($ip_address))
	<tr>
		<td width="20" align="left">Alamat_IP</td>
		<td width="1" align="center">:</td>
		<td align="left">{{$ip_address}} 
		@if (! empty($ip_info))
			 ({{$ip_info}})
		@endif
		</td>
	</tr>
@endif
</table>
</p><p>
Semoga informasi ini bermanfaat bagi Anda.
</p><p>
Jika Anda merasa tidak melakukan aktivitas ini atau memiliki pertanyaan mengenai transaksi Anda, silahkan hubungi Customer Care kami.
</p>
@endsection 