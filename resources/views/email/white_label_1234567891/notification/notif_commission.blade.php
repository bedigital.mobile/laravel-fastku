@extends('email.white_label_1234567891.layout')

@section('email_title', 'Realize Commission Notification')

@section('email_content')
<p>Hai <b>{{$fullname}}</b>,</p>
<p>
Kami informasikan bahwa akun <b>{{$owner_type}}</b> <b>POSFIN</b> dengan ID <b>{{$owner_id}}</b> 
telah mendapatkan realisasi komisi untuk bulan <b>{{$month_name}}</b> tahun <b>{{$year}}</b>. 
Berikut ini detail informasinya:
</p><p>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<td width="20" align="left">Waktu</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$datetime}}</td>
</tr>
<tr>
	<td width="20" align="left">Aktivitas</td>
	<td width="1" align="center">:</td>
	<td align="left">Realisasi Komisi {{$owner_type}}</td>
</tr>
<tr>
	<td width="20" align="left">Bln/thn</td>
	<td width="1" align="center">:</td>
	<td align="left">{{ sprintf('%02d', $month) }} - {{$month_name}} / {{$year}}</td>
</tr>
<tr>
	<td width="20" align="left">Komisi</td>
	<td width="1" align="center">:</td>
	<td align="left">Rp {{ number_format($total_commission_received, 2, ',', '.') }}</td>
</tr>
<tr>
	<td width="20" align="left">Potongan</td>
	<td width="1" align="center">:</td>
	<td align="left">Rp {{ number_format($realize_deduction_value, 2, ',', '.') }}</td>
</tr>
<tr>
	<td width="20" align="left">Keterangan</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$realize_deduction_name}}</td>
</tr>
<tr>
	<td width="20" align="left">Diterima</td>
	<td width="1" align="center">:</td>
	<td align="left"><b>Rp {{ number_format($realize_commission_received, 2, ',', '.') }}</b></td>
</tr>
<tr>
	<td width="20" align="left">Mekanisme</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$realize_status_name}}</td>
</tr>
@if ($realize_status == 'ADDEDDEPOSIT')
<tr>
	<td width="20" align="left">Deposit</td>
	<td width="1" align="center">:</td>
	<td align="left">dari <b>Rp {{ number_format($balance_value_before, 2, ',', '.') }}</b> menjadi <b>Rp {{ number_format($balance_value_after, 2, ',', '.') }}</b></td>
</tr>
@elseif ($realize_status == 'ADDEDWALLET')
<tr>
	<td width="20" align="left">Wallet</td>
	<td width="1" align="center">:</td>
	<td align="left">dari <b>Rp {{ number_format($balance_value_before, 2, ',', '.') }}</b> menjadi <b>Rp {{ number_format($balance_value_after, 2, ',', '.') }}</b></td>
</tr>
@endif
</table>
</p><p>
Semoga informasi ini bermanfaat bagi Anda.
</p>
@endsection 