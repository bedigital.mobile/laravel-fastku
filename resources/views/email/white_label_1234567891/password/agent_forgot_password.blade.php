@extends('email.white_label_1234567891.layout')

@section('email_title', 'Agent Forgot Password')

@section('email_content')
<p>Hai <b>{{$fullname}}</b>,</p>
<p>
Kami menerima notifikasi forgot password aplikasi AGENT <b>POSFIN</b> dengan username <b>{{$username}}</b>. 
Silakan klik link dibawah dan ikuti petunjuknya (link ini aktif selama {{$expiry_hours}} jam dan hanya untuk satu kali permintaan penggantian password)
<br/>
<a href="{{$url}}" target="_blank">{{$url}}</a>
</p>
<p>Jika terdapat masalah pada link tersebut, silakan copy-paste pada browser.</p>
<p>Abaikan email ini jika Anda tidak meminta perubahan password.</p>
<p>
@endsection 