@extends('email.white_label_1234567891.layout')

@section('email_title', 'Wellcome Join POSFIN')

@section('email_content')
<p>Hai <b>{{$fullname}}</b>,</p>
<p>
Selamat bergabung dengan layanan <b>POSFIN</b> : Agen Pulsa & Pembayaran.<br/> 
Berikut ini data akun Anda :<br/>
<ul>
<li>ID: <b>{{$agent_id}}</b></li>
<li>Username: <b>{{$agent_username}}</b></li>
</ul><br>
@if (! empty($master_agent_id))
	Berikut ini data koordinator / Master Agent sebagai upline Anda :<br/>
	<ul>
	<li>Nama: {{$master_agent_fullname}}</li>
	<li>Alamat: {{$master_agent_address}}</li>
	<li>Telp: {{$master_agent_phone_number}}</li>
	<li>Email: {{$master_agent_email}}</li>
	</ul><br>
@endif
</p>
<p>
Untuk mengetahui lebih detail tentang POSFIN, Anda bisa baca dengan klik <a href="https://apiweb.posfin.id/fintech/legal/faq" target="_blank">FAQ POSFIN</a>.
</p>
<p>Anda bisa login di aplikasi POSFIN atau dari web agen POSFIN ( <a href="https://agen.posfin.id" target="_blank">agen.posfin.id</a> ) 
dengan akun username di atas. Jika lupa password, gunakan fasilitas <i>forgot password</i>.<br/>
Aplikasi POSFIN bisa Anda download di Google Play ( <a href="https://play.google.com/store/apps/details?id=com.bedigital.fintech" target="_blank">play.google.com/store/apps/details?id=com.bedigital.fintech</a> ).
</p>
@endsection 