@extends('email.white_label_1234567890.layout')

@section('email_title', 'Agent Request Approval')

@section('email_content')
<p>Hai <b>{{$master_agent_fullname}}</b>,</p>
<p>
Berikut ini notifikasi bahwa ada Agent baru yang akan bergabung dengan data sebagai berikut: <br>
<ul>
<li>ID: <b>{{$agent_id}}</b></li>
<li>Username: <b>{{$agent_username}}</b></li>
<li>Nama: <b>{{$agent_fullname}}</b></li>
<li>Telp.: {{$agent_phone_number}}</li>
<li>Email: {{$agent_email}}</li> 
</ul><br>
</p>
<p>Silahkan login ke Web Koordinator / Master Agent ( <a href="https://master-agen.posfin.id" target="_blank">master-agen.posfin.id</a> ) dan melakukan verifikasi data profil dan dilanjutkan persetujuan (approval) untuk Agent baru ini.</p> 
@endsection 