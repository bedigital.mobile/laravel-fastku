@extends('email.white_label_1234567890.layout')

@section('email_title', 'Report Batch Inquiry')

@section('email_content')
<p>Hai <b>{{$fullname}}</b>,</p>
<p>
Sesuai permintaan batch inquiry pada tanggal {{$date}} jam {{$hour}} WIB dengan upload file text <b>{{$original_filename}}</b>, 
maka berikut ini hasilnya:<br/>
<ul>
   <li>Total data: {{$total_data}}</li>
   <li>Total sukses: {{$total_success}}</li>
   <li>Total gagal: {{$total_failed}}</li>
   <li>Cummulative Nominal: Rp. {{number_format($cummulative_price_nominal,2,',','.')}}</li>
   <li>Cummulative Admin: Rp. {{number_format($cummulative_price_admin,2,',','.')}}</li>
   <li>Cummulative Total Harga: Rp. {{number_format($cummulative_price_total,2,',','.')}}</li>
</ul>
</p>
<p>Detail dari hasil batch inquiry ada di attachment.</p>
<p>Anda juga bisa melakukan print-out hasil inquiry ini di aplikasi POSFIN atau dari web agen POSFIN ( <a href="https://agen.posfin.id" target="_blank">agen.posfin.id</a> ).</p>
@endsection 