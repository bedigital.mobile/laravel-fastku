@extends('email.white_label_1234567891.layout')

@section('email_title', 'Agent Balance Topup')

@section('email_content')
<p>Hai <b>{{$full_name}}</b>,</p>
<p>
Kami telah menerima topup untuk akun nomor <b>{{$account_number}}</b> pada <b>{{$bank_name}}</b> senilai <b>{{$amount}}</b> rupiah. Dengan biaya admin senilai <b>{{$admin_fee}}</b> rupiah. Balance sebelumnya adalah <b>{{$balance_before}}</b> rupiah. Sehingga balance saat ini menjadi <b>{{$balance_after}}</b> rupiah. 
<br/>
</p>
@endsection 