@extends('email.white_label_1234567890.layout')

@section('email_title', 'Transfer Balance Notification')

@section('email_content')
<p>Hai <b>{{$fullname}}</b>,</p>
<p>
Kami informasikan bahwa akun <b>{{$owner_type}}</b> <b>POSFIN</b> dengan ID <b>{{$owner_id}}</b> telah 
@if ($activity_type == 'SENDER')
	melakukan pengiriman saldo.  
@else
	menerima tambahan saldo. 
@endif
Berikut ini detail informasinya:
</p><p>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<td width="40" align="left">Waktu</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$datetime}}</td>
</tr>
<tr>
	<td width="40" align="left">Aktivitas</td>
	<td width="1" align="center">:</td>
	<td align="left">
	@if ($activity_type == 'SENDER')
		Mengirim Saldo
	@else
		Menerima Saldo
	@endif
	</td>
</tr>
<tr>
	<td width="40" align="left">Nominal</td>
	<td width="1" align="center">:</td>
	<td align="left"><b>Rp {{ number_format($transfer_value, 2, ',', '.') }}</b></td>
</tr>
<tr>
	<td width="40" align="left">Saldo</td>
	<td width="1" align="center">:</td>
	<td align="left">dari <b>Rp {{ number_format($balance_value_before, 2, ',', '.') }}</b> menjadi <b>Rp {{ number_format($balance_value_after, 2, ',', '.') }}</b></td>
</tr>
<tr>
	<td width="40" align="left">
	@if ($activity_type == 'SENDER')
		Tipe_Penerima
	@else
		Tipe_Pengirim
	@endif
	</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$partner_type}}</td>
</tr>
<tr>
	<td width="40" align="left">
	@if ($activity_type == 'SENDER')
		ID_Penerima
	@else
		ID_Pengirim
	@endif
	</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$partner_id}}</td>
</tr>
<tr>
	<td width="40" align="left">
	@if ($activity_type == 'SENDER')
		Nama_Penerima
	@else
		Nama_Pengirim
	@endif
	</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$partner_name}}</td>
</tr>
</table>
</p><p>
Semoga informasi ini bermanfaat bagi Anda.
</p><p>
Jika Anda merasa tidak melakukan aktivitas ini atau memiliki pertanyaan mengenai transaksi Anda, silahkan hubungi Customer Care kami.
</p>
@endsection 