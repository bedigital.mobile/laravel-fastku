@extends('email.white_label_1234567890.layout')

@section('email_title', 'Transaction Notification')

@section('email_content')
<p>Hai <b>{{$fullname}}</b>,</p>
<p>
Kami informasikan bahwa akun <b>Agen</b> <b>POSFIN</b> dengan ID <b>{{$agent_id}}</b> telah melakukan transaksi pembelian/pembayaran.   
Berikut ini detail informasinya:
</p><p>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<td width="40" align="left">Waktu</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$datetime}}</td>
</tr>
<tr>
	<td width="40" align="left">Aktivitas</td>
	<td width="1" align="center">:</td>
	<td align="left">Transaksi 
	@if ($trx_type == 'PAYMENT')
		Pembayaran
	@else
		Pembelian
	@endif
	</td>
</tr>
<tr>
	<td width="40" align="left">Produk</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$description}}</td>
</tr>
<tr>
	<td width="40" align="left">Nomor_Biller</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$trx_bill_number}}</td>
</tr>
<tr>
	<td width="40" align="left">Total_Harga</td>
	<td width="1" align="center">:</td>
	<td align="left"><b>Rp {{ number_format($amount, 2, ',', '.') }}</b></td>
</tr>
<tr>
	<td width="40" align="left">Terbilang</td>
	<td width="1" align="center">:</td>
	<td align="left">{{$amount_rupiah}}</td>
</tr>
<tr>
	<td width="40" align="left">
	@if ($balance_type_id >= 1000 && $balance_type_id <= 2999)
		Tagihan
	@elseif ($balance_type_id >= 3000 && $balance_type_id <= 4999)
		Deposit
	@else
		Wallet
	@endif
	</td>
	<td width="1" align="center">:</td>
	<td align="left">dari <b>Rp {{ number_format($balance_value_before, 2, ',', '.') }}</b> menjadi <b>Rp {{ number_format($balance_value_after, 2, ',', '.') }}</b></td>
</tr>
@if ($trx_commission_received > 0)
	<tr>
		<td width="40" align="left">Komisi</td>
		<td width="1" align="center">:</td>
		<td align="left">Rp {{ number_format($trx_commission_received, 2, ',', '.') }}</td>
	</tr>
@endif
@if ($trx_bonus_received > 0)
	<tr>
		<td width="40" align="left">Bonus</td>
		<td width="1" align="center">:</td>
		<td align="left">Rp {{ number_format($trx_bonus_received, 2, ',', '.') }}</td>
	</tr>
@endif
</table>
</p>
@if ($has_attachment)
	<p>Detail dari transaksi ada di attachment.</p>
@endif
<p>Anda juga bisa melakukan print-out hasil transaksi ini di aplikasi POSFIN atau dari web agen POSFIN ( <a href="https://agen.posfin.id" target="_blank">agen.posfin.id</a> ).</p>
<p>
Semoga informasi ini bermanfaat bagi Anda.
</p><p>
Jika Anda merasa tidak melakukan aktivitas ini atau memiliki pertanyaan mengenai transaksi Anda, silahkan hubungi Customer Care kami.
</p>
@endsection 