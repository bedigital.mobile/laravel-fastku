@extends('email.layoutposfin')

@section('email_title', 'Transaction Information')

@section('email_content') 

 
	<p> Berikut ini adalah informasi transaksi terhadap akun anda:</p>
<p>
Waktu	: {{$waktu}}<br>
username	: {{$username}}<br>
Aktivitas	: UPDATE-AGENT<br>
<p>
	 Telah dilakukan perubahan untuk AgenID= {{$userid}} / {{$agentid}}
	 <br> Master Agent ID : {{$malama}} menjadi {{$mabaru}}
	 <br> No HP : {{$hplama}}  menjadi  {{$hpbru}}
	 <br> Alamat email : {{$maillama}} menjadi {{$mailbaru}}
	 <br> Status Akun : {{$stslama}} menjadi {{$stsbaru}}
	 <br>  Daily Total Max Trx : {{$dlama}} menjadi {{$dbaru}}
	 
</p> 
User Agent	: {{$useragent}}<br>
Status	: Sukses<br> 


<p>
Semoga informasi ini bermanfaat bagi Anda.</p>
<p>
Jika Anda merasa tidak melakukan aktivitas ini silahkan hubungi kami melalui email: info@posfin.id
</p>
@endsection 