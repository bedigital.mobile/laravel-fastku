@extends('email.layout')

@section('email_title', 'Transaction Information')

@section('email_content')
<p>Hai <b>{{$fullname}}</b>,</p>
<p>
	Terima kasih Anda telah menggunakan {{$msg}}.</p><p> Berikut ini adalah informasi transaksi yang telah Anda lakukan:</p>
<p>
Waktu	: {{$waktu}}<br>
username	: {{$username}}<br>
Aktivitas	: Login<br>
User Agent	: {{$useragent}}<br>
Status	: Sukses<br>
</p>
<p>
Semoga informasi ini bermanfaat bagi Anda.</p>
<p>
Jika Anda merasa tidak melakukan aktivitas ini silahkan hubungi kami melalui email: info@posfin.id
</p>
@endsection 