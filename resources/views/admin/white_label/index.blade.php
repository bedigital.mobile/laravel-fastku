@extends('admin.main_agent.layout')

@section('content')
    <div>
        <ul class="breadcrumb">
            <li><a href="{{ secure_url('/h2h/admin/home.html') }}">Home</a></li>
            <li>Main Agents</li>
        </ul>
    </div>

    <div class="row-fluid">
        <form id="frm-search" onsubmit="goSearch('{{ secure_url('/h2h/admin/mainagent.html') }}');return false;">
        <div class="input-group col-md-4">
            <input type="text" class="form-control" id="q" placeholder="Search...">
            <span class="input-group-addon"><a href="javascript:goSearch('{{ secure_url('/h2h/admin/mainagent.html') }}');"><i class="glyphicon glyphicon-search red"></i></a></span>
            <span class="input-group-addon"><a href="javascript:getPage('{{ secure_url('/h2h/admin/mainagent.html') }}');"><i class="glyphicon glyphicon-refresh green"></i></a></span>
            <span class="input-group-addon"><a href="javascript:getPage('{{ secure_url('/h2h/admin/mainagent_new.html') }}');"><i class="glyphicon glyphicon-plus green"></i> New</a></span>
        </div>
        </form>
    </div>

    <div class="row-fluid">
        <!-- PAGE CONTENT BEGINS -->
        &nbsp;
        
        @if ($sorter->searchFor() != '')
        <div class="row">
            <div class="col-xs-12">Search For: <strong>'{{ $sorter->searchFor() }}'</strong></div>
        </div>
        @endif
        
        @if ($sorter->pagination() != '')
        <div class="row">
            <div class="col-xs-12">{!! $sorter->pagination() !!}</div>
        </div>
        @endif

        <table class="table table-striped table-bordered">  
            <thead>
                <tr>
                    <th class="center" width="1%">No.</th>
                    <th width="63%">{!! $sorter->field(0) !!}</th>
                    <th width="20%">{!! $sorter->field(1) !!}</th>
                    <th width="15%">&nbsp;</th>
                </tr>
            </thead>

            <tbody>
                @if ($sorter->rowCount() == 0)
                <tr>
                    <td colspan="4" align="center">No Records Found...</td>
                </tr>
                @else
                    @foreach ($sorter->pageRows() as $row)
                <tr>
                    <td class="center">{{ $sorter->skippedRows = $sorter->skippedRows + 1 }}.</td>
                    <td>{{ ($row->fullname != '' ? $row->fullname : ($row->company_name != '' ? $row->company_name : $row->username)) }}</td>
                    <td>
                        @if ($row->status == 'REGISTERING')
                        <span class="label label-info">
                        @elseif ($row->status == 'ACTIVE')
                        <span class="label label-success">
                        @elseif ($row->status == 'SUSPENDED')
                        <span class="label label-warning">
                        @elseif ($row->status == 'DEACTIVATED')
                        <span class="label label-danger">
                        @elseif ($row->status == 'UNAPPROVED')
                        <span class="label label-danger">
                        @endif
                        {{ $row->status }}
                        </span>
                    </td>
                    <td class="center">
                        <div class="hidden-sm hidden-xs btn-group">                            
                            <a class="btn btn-success" href="javascript:getPage('{{ url('/mainagent_view_'.$row->agent_id.'.html') }}');">
                                <i class="glyphicon glyphicon-eye-open icon-white"></i>
                            </a>
                            
                            <a class="btn btn-info" href="javascript:getPage('{{ url('/mainagent_edit_'.$row->agent_id.'.html') }}');">
                                <i class="glyphicon glyphicon-edit icon-white"></i>
                            </a>
                            
                            <a class="btn btn-danger" href="javascript:getPage('{{ url('/mainagent_delete_'.$row->agent_id.'.html') }}');">
                                <i class="glyphicon glyphicon-trash icon-white"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>

@endsection