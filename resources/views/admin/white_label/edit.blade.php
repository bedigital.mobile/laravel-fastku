@extends('main_agent.layout')

@section('content')
    <div>
        <ul class="breadcrumb">
            <li><a href="javascript:getPage('{{ url('/home.html') }}');">Home</a></li>
            <li><a href="javascript:getPage('{{ url('/mainagent.html') }}');">Main Agents</a></li>
            <li>Edit</li>
        </ul><!-- /.breadcrumb -->      
    </div>

    <div class="row-fluid">
        <div class="box col-md-12">
            <div class="box-inner">                
                <div class="box-content">
                        <div id="msg"></div>            
                        <form role="form" id="frm" onsubmit="doEdit();return false;">
                        @csrf
                        <input type="hidden" name="id" value="{{ $rs->id }}" />

                        <div class="clearfix"></div>
                        <h4 class="blue">General</h4>
                        <hr/>

                        <div class="form-group col-md-12">
                            <label for="profile-type"> Profile Type </label>

                            <div class="clearfix"></div>

                            <label class="radio-inline blue">
                                <input name="profile-type" id="profile-type1" value="{{ $PROFILE_TYPE_CORPORATE }}" type="radio" class="ace" {!! ($rs->profile->profile_type == $PROFILE_TYPE_CORPORATE ? 'checked="checked"' : '') !!} />
                                <span class="lbl"> Corporate</span>
                            </label>
                            <label class="radio-inline blue">
                                <input name="profile-type" id="profile-type2" value="{{ $PROFILE_TYPE_PERSONAL }}" type="radio" class="ace" {!! ($rs->profile->profile_type == $PROFILE_TYPE_PERSONAL ? 'checked="checked"' : '') !!} />
                                <span class="lbl"> Personal</span>
                            </label>
                        </div>

                        <div class="form-group" id="view-corporate">
                            @include('main_agent.edit_profile_corporate')
                        </div>

                        <div class="form-group" id="view-personal">
                            @include('main_agent.edit_profile_personal')
                        </div>

                        <div class="form-group col-md-12">
                            <label for="description"> Description </label>

                            <textarea id="description" name="description" class="form-control">{{ $rs->profile->description }}</textarea>
                        </div>
                        
                        &nbsp;
                        <div class="clearfix"></div>
                        <h4 class="blue">Account</h4>
                        <hr/>

                        <div class="form-group col-md-6">
                            <label for="username"> User Name </label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                                <span class="form-control">{{ $rs->username }}</span>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="status"> Status </label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-flag red"></i></span>
                                <div class="form-control" id="status">
                                    @if ($rs->status == $STATUS_REGISTER || $rs->status == $STATUS_UNAPPROVED)
                                    <label class="radio-inline blue">
                                        <input name="status" id="status1" value="{{ $STATUS_REGISTER }}" type="radio" class="ace" {!! ($rs->status == $STATUS_REGISTER ? 'checked="checked"' : '') !!} />
                                        <span class="lbl"> {{ $STATUS_REGISTER }}</span>
                                    </label>
                                    <label class="radio-inline blue">
                                        <input name="status" id="status2" value="{{ $STATUS_UNAPPROVED }}" type="radio" class="ace" {!! ($rs->status == $STATUS_UNAPPROVED ? 'checked="checked"' : '') !!} />
                                        <span class="lbl"> {{ $STATUS_UNAPPROVED }}</span>
                                    </label>
                                    <label class="radio-inline blue">
                                        <input name="status" id="status3" value="{{ $STATUS_ACTIVE }}" type="radio" class="ace" {!! ($rs->status == $STATUS_ACTIVE ? 'checked="checked"' : '') !!} />
                                        <span class="lbl"> {{ $STATUS_ACTIVE }}</span>
                                    </label>
                                    @else
                                    <label class="radio-inline blue">
                                        <input name="status" id="status4" value="{{ $STATUS_ACTIVE }}" type="radio" class="ace" {!! ($rs->status == $STATUS_ACTIVE ? 'checked="checked"' : '') !!} />
                                        <span class="lbl"> {{ $STATUS_ACTIVE }}</span>
                                    </label>
                                    <label class="radio-inline blue">
                                        <input name="status" id="status5" value="{{ $STATUS_INACTIVE }}" type="radio" class="ace" {!! ($rs->status == $STATUS_INACTIVE ? 'checked="checked"' : '') !!} />
                                        <span class="lbl"> {{ $STATUS_INACTIVE }}</span>
                                    </label>
                                    <label class="radio-inline blue">
                                        <input name="status" id="status6" value="{{ $STATUS_SUSPENDED }}" type="radio" class="ace" {!! ($rs->status == $STATUS_SUSPENDED ? 'checked="checked"' : '') !!} />
                                        <span class="lbl"> {{ $STATUS_SUSPENDED }}</span>
                                    </label>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="password"> Password </label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                                <input type="password" id="password" name="password" placeholder="Password" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="cpassword"> Confirm Password </label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                                <input type="password" id="cpassword" name="cpassword" placeholder="Confirm Password" class="form-control" />
                            </div>
                        </div>

                        &nbsp;
                        <div class="clearfix"></div>
                        <h4 class="blue">Agent</h4>
                        <hr/>

                        <div class="form-group col-md-12">
                            <label for="uplink"> Uplink Agent </label>

                            <select class="form-control" id="uplink" name="uplink" data-rel="chosen">
                                <option value="0"> --- </option>
                                @foreach ($uplinks as $up)
                                <option value="{{ $up->agent_id }}" {{ ($rs->master_agent_parent_id == $up->agent_id ? 'selected' : '') }}>{{ ($up->fullname != '' ? $up->fullname : ($up->company_name != '' ? $up->company_name : $up->username)) }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="paymode"> Payment Mode </label>

                            <div class="clearfix"></div>

                            <div class="col-md-4">
                                <label class="radio blue">
                                    <input name="paymode" id="paymode1" value="PREPAID" type="radio" class="ace" checked="checked" />
                                    <span class="lbl"> Prepaid</span>
                                </label>
                                <label class="radio blue">
                                    <input name="paymode" id="paymode2" value="POSTPAID" type="radio" class="ace" />
                                    <span class="lbl"> Postpaid</span>
                                </label>
                                <label>Max. Transaction/day</label>
                                <div class="input-group">
                                    <span class="input-group-addon">Rp.</span>
                                    <input type="text" id="max-trx" name="max-trx" maxlength="11" placeholder="Max. Transactions per day" class="form-control" />
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group col-md-2">
                            <label for="maxterminal"> Max. Terminal </label>

                            <input type="text" id="maxterminal" name="maxterminal" placeholder="Maximum number of Terminal" class="form-control" value="{{ $rs->agent_max_terminal_default }}" />
                        </div>

                        <div class="clearfix"></div>                            
                        <hr/>

                        <div class="form-actions">
                            <a href="javascript:getPage('{{ url('/mainagent.html') }}');" class="btn btn-warning btn-sm" id="button-close"><i class="glyphicon glyphicon-arrow-left"></i> Close</a>
                            &nbsp;
                            <button class="btn btn-info btn-sm" type="submit" id="button-save"><i class="glyphicon glyphicon-save"></i> Save</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('utils.image_upload')

@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        $('#view-corporate').hide();
        $('#view-personal').hide();
        $('input[name=profile-type]').on('click', function() {
            if ($(this).val() == '{{ $PROFILE_TYPE_CORPORATE }}') {
                $('#view-corporate').show();
                $('#view-personal').hide();
            } else {
                $('#view-corporate').hide();
                $('#view-personal').show();
            }
        });
        @if ($rs->profile->profile_type === $PROFILE_TYPE_PERSONAL)
        $('input[name=profile-type]')[1].click();
        @else
        $('input[name=profile-type]')[0].click();
        @endif
        
        $('input[name=paymode]').on('click', function() {
            if ($(this).val() === '{{ $PAYMODE_POSTPAID }}') {
                $('#max-trx').removeAttr('disabled');
            } else {
                $('#max-trx').attr('disabled', 'disabled');
            }
        });
        
        @if ($rs->paymode == $PAYMODE_POSTPAID)
        $('input[name=paymode]')[1].click();
        @else
        $('input[name=paymode]')[0].click();
        @endif
    });
</script>
@endpush
