@extends('admin.white_label.layout')

@section('content')
    <div>
        <ul class="breadcrumb">
            <li><a href="javascript:getPage('{{ url('/h2h/admin/home.html') }}');">Home</a></li>
            <li>My Profile</li>
        </ul><!-- /.breadcrumb -->      
    </div>

    <div class="row-fluid">
        <div class="box col-md-12">
            <div class="box-inner">                
                <div class="box-content">
                    <div id="msg"></div>            
                    <form role="form" id="frm" onsubmit="doEditProfile();return false;">
                        @csrf
                        <input type="hidden" name="id" value="{{ $rs->id }}" />

                        <div class="clearfix"></div>

                        <div class="form-group col-md-2 pull-right">
                            <a href="#" class="btn btn-setting">
                                <div class="crop center" id="avatar">
                                    @if ($rs->photo_filename != '')
                                    <img src="{{ url('/asset_avatar/'.$rs->photo_filename) }}" />
                                    @else
                                    <img src="img/user.png" />
                                    @endif
                                </div>
                            </a>
                            <input type="hidden" name="avatar-file" id="avatar-file" value="" />
                        </div>

                        <div class="form-group col-md-10 pull-left">
                            <label for="fullname"> Full Name </label>
                            <input type="text" id="fullname" name="fullname" placeholder="Full name" class="form-control" value="{{ $rs->fullname }}" />
                        </div>

                        <div class="form-group col-md-10 pull-left">
                            <label for="description"> Description </label>

                            <textarea id="description" name="description" class="form-control">{{ $rs->description }}</textarea>
                        </div>
                        
                        <div class="clearfix"></div>
                        <h4 class="blue">Contact</h4>
                        <hr/>

                        <div class="form-group col-md-6">
                            <label for="email"> Email Address </label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Valid Email Address" value="{{ $rs->email }}" />
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="phone"> Phone Number </label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="Valid Phone Number" value="{{ $rs->phone_number }}" />
                            </div>
                        </div>
                                
                        &nbsp;
                        <div class="clearfix"></div>
                        <h4 class="blue">Account</h4>
                        <hr/>

                        <div class="form-group col-md-6">
                            <label for="username"> User Name </label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                                <span class="form-control">{{ $rs->username }}</span>
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="status"> Status </label>

                            <div class="input-group {{ ($rs->active == $STATUS_ACTIVE ? 'has-success' : 'has-error') }}">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-flag {{ ($rs->active == $STATUS_ACTIVE ? 'green' : 'red') }}"></i></span>
                                <div class="form-control" id="status">
                                    {{ ($rs->active == $STATUS_ACTIVE ? 'Active' : 'Inactive') }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="password"> Password </label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                                <input type="password" id="password" name="password" placeholder="Password" class="form-control" />
                                <input type="hidden" id="xpwd" name="xpwd" value="" />
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="cpassword"> Confirm Password </label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                                <input type="password" id="cpassword" name="cpassword" placeholder="Confirm Password" class="form-control" />
                                <input type="hidden" id="ypwd" name="ypwd" value="" />
                            </div>
                        </div>

                        <div class="clearfix"></div>                            
                        <hr/>

                        <div class="form-actions">
                            <button class="btn btn-info btn-sm" type="submit" id="button-save"><i class="glyphicon glyphicon-save"></i> Save</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('admin.utils.image_upload')

@endsection
