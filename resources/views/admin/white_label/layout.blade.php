@extends('admin.layout')

@push('styles')
<style>
    .crop {
        width: 128px;
        height: 128px;
        border: 1px #000 dotted;
        overflow: hidden;
    }
    
    .crop img {
        background-position: center;
        background-size: cover;
        height: 128px;
        display: block;
    }
</style>
@endpush

@prepend('scripts')
<script src="js/md5.js"></script>
<script>
    function beforeSubmit() {
        $('#button-close').attr('disabled', 'disabled');

        $('#button-save').attr('disabled', 'disabled');
        $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Saving...');
    }

    function afterSubmit() {
        $('#button-close').removeAttr('disabled');

        $('#button-save').removeAttr('disabled');
        $('#button-save').html('<i class="glyphicon glyphicon-save"></i> Save');
    }
    
    function beforeSubmitProfile() {
        var pwd = $('#password').val();
        if (pwd !== '') {
            $('#password').val('');
            $('#xpwd').val(hex_md5(pwd));
        }
        
        var cpwd = $('#cpassword').val();
        if (cpwd !== '') {
            $('#cpassword').val('');
            $('#ypwd').val(hex_md5(cpwd));
        }
        
        $('#button-save').attr('disabled', 'disabled');
        $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Saving...');
    }

    function afterSubmitProfile() {
        $('#xpwd').val('');
        $('#ypwd').val('');
        $('#button-save').removeAttr('disabled');
        $('#button-save').html('<i class="glyphicon glyphicon-save"></i> Save');
    }
    
    function doSave() {
        sendForm2({
            method: 'PUT',
            form: 'frm',
            messageView: 'msg',
            url: '{{ url('/mainagent_new.html') }}',
            redirect:  '{{ url('/mainagent.html') }}',
            beforeSubmit: beforeSubmit,
            afterSubmit: afterSubmit
        });
    }

    function doEdit() {
        sendForm2({
            method: 'POST',
            form: 'frm',
            messageView: 'msg',
            url: '{{ url('/mainagent_edit.html') }}',
            redirect:  '{{ url('/mainagent.html') }}',
            beforeSubmit: beforeSubmit,
            afterSubmit: afterSubmit
        });
    }
    
    function doEditProfile() {
        sendForm2({
            method: 'POST',
            form: 'frm',
            messageView: 'msg',
            url: '{{ url('/profile.html') }}',
            redirect:  '{{ url('/profile.html') }}',
            beforeSubmit: beforeSubmitProfile,
            afterSubmit: afterSubmitProfile
        });
    }
    
    function doDelete() {
        if (confirm('Are you sure you want to remove this Main Agent?')) {
            sendForm2({
                method: 'POST',
                form: 'frm',
                messageView: 'msg',
                url: '{{ url('/mainagent_delete.html') }}',
                redirect:  '{{ url('/mainagent.html') }}',
                beforeSubmit: function() {
                    $('#button-close').attr('disabled', 'disabled');
                    $('#button-save').attr('disabled', 'disabled');
                    $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Removing...');
                },
                afterSubmit: function() {
                    $('#button-close').removeAttr('disabled');
                    $('#button-save').removeAttr('disabled');
                    $('#button-save').html('<i class="glyphicon glyphicon-remove"></i> Remove');                }
            });
        }
    }
    
    function callbackImageUpload(data) {
        $('#avatar').html('<img src="{{ url('/asset_avatar/') }}/' + data + '" />');
        $('#avatar-file').val(data);
    }    
</script>
@endprepend
