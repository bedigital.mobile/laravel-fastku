@extends('admin.trx.layout') 
@section('style') 
<style type="text/css">
    main.c-main {
    padding: 15px 0 0 0;
}
 .lku {
    /*overflow: scroll;*/
    /*height: 500px;*/ 
    padding-inline-start: 0px !important;
}
    
section {
  display: flex;
  flex-flow: row wrap;
}

section > div {
  flex: 1;
  padding: 0.5rem;
} 
.nav-items { 
    width: 100%;
}
.text-warning {
    color: #e5ae67!important;
    text-align: right;
}
.hide
{
    display: none;
}
 
</style>
@endsection

@section('content')
<main class="c-main">	   
<div class="container-fluid">
<div class="fade show" style="">
<div class="row hide" id="formtrxpayment">
    <div class="col-12">
        <div class="card">
        <header class="card-header">Pembayaran Pengiriman</header>
            <div class="card-body">                 
                <div id="infokur">
                    <div class="block block-rounded">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Detail Alamat</h3>
                        </div>
                        <div class="block-content">
                            <div class="row">
                                <div class="col-lg-6">
                                    <!-- Billing Address -->
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header border-bottom">
                                            <h3 class="block-title">Alamat Pengirim</h3>
                                        </div>
                                        <div class="block-content">
                                            <div class="font-size-h4 mb-1">John Parker</div>
                                            <address class="font-size-sm">
                                                Sunrise Str 620<br>
                                                Melbourne<br>
                                                Australia, 11-587<br><br>
                                                <i class="fa fa-phone"></i> (999) 888-55555<br>
                                                <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)">company@example.com</a>
                                            </address>
                                        </div>
                                    </div>
                                    <!-- END Billing Address -->
                                </div>
                                <div class="col-lg-6">
                                    <!-- Shipping Address -->
                                    <div class="block block-rounded block-bordered">
                                        <div class="block-header border-bottom">
                                            <h3 class="block-title">Alamat Penerima</h3>
                                        </div>
                                        <div class="block-content">
                                            <div class="font-size-h4 mb-1">John Parker</div>
                                            <address class="font-size-sm">
                                                Sunrise Str 620<br>
                                                Melbourne<br>
                                                Australia, 11-587<br><br>
                                                <i class="fa fa-phone"></i> (999) 888-55555<br>
                                                <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)">company@example.com</a>
                                            </address>
                                        </div>
                                    </div>
                                    <!-- END Shipping Address -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block block-rounded">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Detail Barang</h3>
                        </div>
                        <div class="block-content">
                            <div class="table-responsive">
                                <table class="table table-borderless table-striped table-vcenter">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 100px;">ID</th>
                                            <th class="d-none d-md-table-cell">Product</th>
                                            <th class="d-none d-sm-table-cell text-center">Added</th>
                                            <th>Status</th>
                                            <th class="d-none d-sm-table-cell text-right">Value</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center font-size-sm">
                                                <a class="font-w600" href="be_pages_ecom_product_edit.html">
                                                    <strong>PID.0154</strong>
                                                </a>
                                            </td>
                                            <td class="d-none d-md-table-cell font-size-sm">
                                                <a href="be_pages_ecom_product_edit.html">Product #4</a>
                                            </td>
                                            <td class="d-none d-sm-table-cell text-center font-size-sm">19/12/2019</td>
                                            <td>
                                                <span class="badge badge-danger">Out of Stock</span>
                                            </td>
                                            <td class="text-right d-none d-sm-table-cell font-size-sm">
                                                <strong>$35,00</strong>
                                            </td>
                                            <td class="text-center font-size-sm">
                                                <a class="btn btn-sm btn-alt-secondary js-tooltip-enabled" href="be_pages_ecom_product_edit.html" data-toggle="tooltip" title="" data-original-title="View">
                                                    <i class="fa fa-fw fa-eye"></i>
                                                </a>
                                                <a class="btn btn-sm btn-alt-danger js-tooltip-enabled" href="javascript:void(0)" data-toggle="tooltip" title="" data-original-title="Delete">
                                                    <i class="fa fa-fw fa-times text-danger"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center font-size-sm">
                                                <a class="font-w600" href="be_pages_ecom_product_edit.html">
                                                    <strong>PID.0153</strong>
                                                </a>
                                            </td>
                                            <td class="d-none d-md-table-cell font-size-sm">
                                                <a href="be_pages_ecom_product_edit.html">Product #3</a>
                                            </td>
                                            <td class="d-none d-sm-table-cell text-center font-size-sm">16/10/2019</td>
                                            <td>
                                                <span class="badge badge-danger">Out of Stock</span>
                                            </td>
                                            <td class="text-right d-none d-sm-table-cell font-size-sm">
                                                <strong>$87,00</strong>
                                            </td>
                                            <td class="text-center font-size-sm">
                                                <a class="btn btn-sm btn-alt-secondary js-tooltip-enabled" href="be_pages_ecom_product_edit.html" data-toggle="tooltip" title="" data-original-title="View">
                                                    <i class="fa fa-fw fa-eye"></i>
                                                </a>
                                                <a class="btn btn-sm btn-alt-danger js-tooltip-enabled" href="javascript:void(0)" data-toggle="tooltip" title="" data-original-title="Delete">
                                                    <i class="fa fa-fw fa-times text-danger"></i>
                                                </a>
                                            </td>
                                        </tr> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- notes !-->

                    <div class="block block-rounded">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Notes</h3>
                        </div>
                        <div class="block-content">
                            <p class="alert alert-info font-size-sm">
                                <i class="fa fa-fw fa-info mr-1"></i> This note will not be displayed to the customer.
                            </p>
                            <form action="be_pages_ecom_customer.html" onsubmit="return false;">
                                <div class="form-group">
                                    <label for="one-ecom-customer-note">Note</label>
                                    <textarea class="form-control" id="one-ecom-customer-note" name="one-ecom-customer-note" rows="4" placeholder="Maybe a special request?"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-alt-success">Add Note</button>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
                <form class="form-horizontal" name="frmtrxpay" id="frmtrxpay" method="post" enctype="multipart/form-data" >
                <span class="" for="prependedInput">
                <strong>Informasi Pembayaran</strong>
                </span>                
                <select name="selectpayment" id="selectpayment"  class="form-control"  required></select>
                <hr>
                <div class="col-md-8">
                    <!-- Referred User -->
                    <a class="block block-rounded block-bordered block-link-shadow" href="javascript:void(0)">
                        <div class="block-content block-content-full d-flex align-items-center justify-content-between">
                            <div>
                                <div class="font-w600 mb-1" id="bankname">Susan Day</div>
                                <div class="font-size-sm text-muted" id="bankdetail">4 Orders</div>
                            </div>
                            <div class="ml-3">
                                <img   id="imagecontainer" src="assets/media/avatars/avatar3.jpg" alt="" height="64px">

                            </div>
                        </div>
                    </a>
                    <!-- END Referred User -->
                </div>
                <div class="form-actions">
                    
                    <input type="submit" class="btn btn-danger" name="savetrx" id="savetrx" value="Simpan Transaksi">
                    <input type="button" class="btn btn-secondary" name="backtrx" id="backtrx" value="Kembali">
                 </div>

               </form>
            </div>
        </div>
    </div>
</div>
<div class="row" id="formtrxdata">
<div class="col-8">
<div class="card">
<header class="card-header">Pengiriman Terjadwal</header>
<div class="card-body"> 

<form class="form-horizontal" name="frmtrxkurir" id="frmtrxkurir" method="post" enctype="multipart/form-data" >
           

<span class="" for="prependedInput">
<i class="si si-drop"></i>
<strong>Asal Pengiriman</strong>
</span>
<div class="row form-group my-0">
<div class="col-5">
<div class="form-group">
<input class="form-control" type="text" id="namapengirim" placeholder="Nama Pengirim" required>
</div>
</div>
<div class="col-4">
<div class="form-group">
<input class="form-control" type="text" id="hppengirim" placeholder="No HP" required>
</div>
</div>
<div class="col-3">
<div class="form-group">
<!-- <select class="custom-select" name="select" id="select">
<option value="0">Pilih</option>
<option value="1">Option #1</option>
<option value="2">Option #2</option>
<option value="3">Option #3</option>
</select> -->
</div>
</div>
</div>
<div class="row form-group my-0">
<div class="col-9">
<div class="form-group">
<input class="form-control" type="text" id="alamatpengirim" placeholder="Alamat Pengirim" required>
</div>
</div>
<div class="col-3">
<div class="form-group">
<input class="form-control" type="text" id="postpengirim" name="postpengirim" placeholder="Kode POS"  maxlength="5" onKeyPress="if(this.value.length==5) return false;" >
</div>
</div>
</div>
<div class="row form-group my-0">
	<div class="col-4">
			<div class="form-group">
				<div id="load_prov" style="display: none;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
				   <select class="custom-select" name="selectprov" id="selectprov"  required>
                                <option value ="">Pilih Provinsi</option>
                                
                            </select>
				
			</div>
	</div>
	<div class="col-4">
			<div class="form-group">
                    <div id="load_kota" style="display: none;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>

				   <select class="custom-select" name="selectkota" id="selectkota"  required>
                                <option value ="">Pilih Kota</option>                                
                            </select>
			</div>
	</div>
	<div class="col-4">
			<div class="form-group">
                    <div id="load_kec" style="display: none;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>

				   <select class="custom-select" name="selectkec" id="selectkec"  required>
                                <option value ="">Pilih Kec</option>                                
                            </select>
			</div>
	</div>

</div>
<div class="row form-group my-0">
<div class="col-12">
<div class="form-group">
<input class="form-control" type="text" id="detailpengirim" placeholder="Detail Alamat, nama apartemen, lantai apartemen, dll">
</div>
</div>
</div>
<hr>
<span class="" for="prependedInput">
<i class="si si-drop"></i>
<strong>Destinasi Pengiriman</strong>
</span>
<div class="row form-group my-0">
<div class="col-5">
<div class="form-group">
<input class="form-control" type="text" id="namapenerima" placeholder="Nama Penerima" required>
</div>
</div>
<div class="col-4">
<div class="form-group">
<input class="form-control" type="text" id="hppenerima" placeholder="No HP Penerima" required>
</div>
</div>
<div class="col-3">
<div class="form-group">
<!-- <select class="custom-select" name="select2" id="select2">
<option value="0">Pilih</option>
<option value="1">Option #1</option>
<option value="2">Option #2</option>
<option value="3">Option #3</option>
</select> -->
</div>
</div>
</div>
<div class="row form-group my-0">
<div class="col-9">
<div class="form-group">
<input class="form-control" type="text" id="alamatpenerima" placeholder="Alamat Penerima" required>
</div>
</div>
<div class="col-3">
<div class="form-group">
<input class="form-control" type="text" id="postcode2" name="postcode2" placeholder="Kode POS" maxlength="5" onKeyPress="if(this.value.length==5) return false;" >
 
</div>
</div>
</div>
<div class="row form-group my-0">
	<div class="col-4">
			<div class="form-group">
			<div id="load_prov2" style="display: none;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
				   <select class="custom-select" name="selectprov2" id="selectprov2"  required>
                                <option value ="">Pilih Provinsi</option>
                                
                            </select>
				
			</div>
	</div>
	<div class="col-4">
			<div class="form-group">
                    <div id="load_kota2" style="display: none;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
				   <select class="custom-select" name="selectkota2" id="selectkota2"  required>
                                <option value ="">Pilih Kota</option>                                
                            </select>
			</div>
	</div>
	<div class="col-4">
			<div class="form-group">
                    <div id="load_kec2" style="display: none;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>

				   <select class="custom-select" name="selectkec2" id="selectkec2"  required>
                                <option value ="">Pilih Kec</option>                                
                            </select>
			</div>
	</div>

</div>
<div class="row form-group my-0">
<div class="col-12">
<div class="form-group">
<input class="form-control" type="text" id="detailpenerima" placeholder="Detail Alamat Penerima, nama apartemen, lantai apartemen, dll">
</div>
</div>
</div>

<button type="button" class="btnbuat btn btn-success" id="cekongkir" name="cekongkir">
    Cek Ongkir
</button>
<div id="kurirpilihan"></div>


<!-- 
<hr>
<span class="" for="prependedInput">
<strong>Fitur Tambahan</strong>
</span>
<div class="row form-group my-0">
<div class="col-12">
<div class="form-group">
<div class="input-group">
<input class="form-control" type="text" id="cod" placeholder="Masukkan nilai barang yang akan ditagih" disabled="">
<div class="input-group-append">
<div class="input-group-text">Cash On Delivered (COD) </div>
</div>
</div>
</div>
</div>
</div>
<div class="row form-group my-0">
<div class="col-12">
<div class="form-group">
<div class="input-group">
<input class="form-control" type="text" id="insurance" placeholder="Nilai Asuransi berdasarkan harga barang" disabled="">
<div class="input-group-append">
    <div class="input-group-text">Pakai Asuransi </div>
</div>
</div>
</div>
</div>
</div> -->
<hr>
<span class="" for="prependedInput">
<strong>Pilih Waktu Penjemputan Barang</strong>
</span>
<div class="row form-group my-0">
<div class="col-9">
<input class="form-control" type="date" id="date-input" name="date-input" placeholder="date" required>
</div>
<div class="col-3">
<div class="form-group">
    <select class="custom-select" name="select2" id="select2">
        <option value="0">Pilih</option>
        <option value="08:00">08:00</option>
        <option value="09:00">09:00</option>
        <option value="10:00">10:00</option>
        <option value="11:00">11:00</option>
        <option value="12:00">12:00</option>
        <option value="13:00">13:00</option>
        <option value="14:00">14:00</option>
        <option value="15:00">15:00</option>
        <option value="16:00">16:00</option>
        <option value="17:00">17:00</option>
    </select>
</div>
</div>
</div>
<div class="form-group">
<span class="" for="prependedInput">Interval Penjemputan Barang = 30 - 60 menit dari waktu yang dipilih</span>
</div>

<hr>

<div id="brg_parent">
    <br>

<div class="frm_brg">
<br><hr>
    
    <div class="row form-group my-0">
        <div class="col-6">
            <div class="form-group">
                <input class="form-control" type="text" name="name-0" data-id="0" id="name-0" placeholder="Nama Barang"  required>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <input class="form-control" type="text" name="harga-0" data-id="0" id="harga-0" placeholder="Harga Barang" >
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <select class="custom-select" name="selcategory[]" id="selcategory-0"  required>
                            <option value="">Kategori</option> 
                        </select>
                    </div>
                </div>
            </div>
            <div class="row form-group my-0">
                <div class="col-2">
                    <div class="form-group">
                        <input class="form-control" type="text" name="pjg-0" data-id="0" id="pjg-0" placeholder="Panjang">
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <input class="form-control" type="text" name="lbr-0" data-id="0" id="lbr-0" placeholder="Lebar">
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <input class="form-control" type="text" name="tng-0" data-id="0" id="tng-0" placeholder="Tinggi">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <div class="controls">
                                        <div class="input-group">
                                            <input class="form-control form-control-16" type="text" name="grm-0" data-id="0" id="grm-0">
                                                <div class="input-group-append">
                                                    <div class="input-group-text">Kg</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">                                        
                                         <select class="custom-select" name="selpaket[]" id="selpaket-0">
                                            <option value="">Kemasan</option>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p></p>
                        </div>
                        <button class="btnbuat btn btn-info" type="button" id="tambahbarang">Tambah Barang</button>
                        <br>
          


<hr> 

                <div class="form-actions">
                    <input type="hidden" id="idshipment" name="idshipment"> 
                    <input type="submit" class="btn btn-danger" name="sbmt_upd" id="sbmt_upd" value="Lanjutkan Pembayaran">
                    <a class="btn btn-secondary" href="#/listpending">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>
                <div class="col-4" id="listkur">
                    <div class="card">
                        <header class="card-header">Opsi Pengiriman Kurir</header>
                        <div class="card-body">
                                

                        	<div id="load_kurir" style="display: none;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
                                  
                              <div id="kurirlistarray"></div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="assets/js/core/jquery.min.js"></script>
<!-- <script src="assets/js/jquery/jquery-1.9.0.min.js"></script>  -->
<script src="assets/js/msdropdown/jquery.dd.js"></script> 
<link rel="stylesheet" type="text/css" href="assets/css/msdropdown/dd.css" />
  

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
       $(document).ready(function() {
         // getdatakota();
                 let selprov = $('#selectprov');
                 let selprov2 = $('#selectprov2');
                 let selkota = $('#selectkota');
                 let selkota2 = $('#selectkota2');
                 let selkec = $('#selectkec');
                 let selkec2 = $('#selectkec2');
 

                 let loadprov = $('#load_prov');
                 let loadprov2 = $('#load_prov2');
                 let loadcity = $('#load_kota');
                 let loadcity2 = $('#load_kota2');
                 let loadkec = $('#load_kec');
                 let loadkec2 = $('#load_kec2');
                 let loadkurir = $('#load_kurir');

                 let namapengirim = $('#namapengirim');
                 let alamatpengirim = $('#alamatpengirim');
                 let postal_code = $('#postpengirim');
                 let hppengirim = $('#hppengirim');
                 let notes = $('#detailpengirim');

                 let namapenerima = $('#namapenerima');
                 let alamatpenerima = $('#alamatpenerima');
                 let postal_code2 = $('#postcode2');
                 let hppenerima  = $('#hppenerima');
                 let notes2 = $('#detailpenerima');
 

                 var optpaket ='';
 		             var optkategori = '';
                 var optpayment = '';
		//loading provinsi1
		getprovince(loadprov,selprov);
		getprovince(loadprov2,selprov2);
        // getdatakurir(31,33,3173,3304,317301,330406);
 // 33 , 3173 , 3304 , 317301 , 330406
    getkategori("selcategory-0");
     
    getpaket("selpaket-0");
   getpaymentlist("selectpayment");
   getshipid("idshipment");


	  	selprov.change(function(){
            getCitys($(this).val(),loadcity,selkota);
        });
		selprov2.change(function(){
		            getCitys($(this).val(),loadcity2,selkota2);
		        });
		
		selkota.change(function(){
            getKec($(this).val(),loadkec,selkec);
        });
        selkota2.change(function(){
            getKec($(this).val(),loadkec2,selkec2);
        });

         $('#cekongkir').click(function() {  
         	console.log("cekongkir");
            $('#cekongkir').prop( "disabled", true );
         	if((namapengirim.val() == '' ) ||(alamatpengirim.val() == '' ) ||(hppengirim.val() == '' ) ||(selprov.val() == '' ) || (selkota.val() == '' ) || (selkec.val() == '' ))
         	{
                  Swal.fire(
                  'Alert!',
                  'Lengkapi Data Pengirim: Nama, Alamat, No HP, provinsi , kota dan kecamatan pengirim',
                  'error'
                );
                 $('#cekongkir').prop( "disabled", false );
         	}
         	else if((namapenerima.val() == '' ) ||(alamatpenerima.val() == '' ) ||(hppenerima.val() == '' ) ||(selprov2.val() == '' ) || (selkota2.val() == '' ) || (selkec2.val() == '' ))
         	{
         		// alert("Lengkapi data provinsi , kota dan kecamatan penerima");
                 Swal.fire(
                  'Alert!',
                  'Lengkapi Data Penerima: Nama, Alamat, No HP, provinsi , kota dan kecamatan Penerima',
                  'error'
                );
                  $('#cekongkir').prop( "disabled", false );
         	} else {
         		 
                const argspengirim = [namapengirim.val(),alamatpengirim.val(),postal_code.val(),hppengirim.val(),notes.val()];
                const argspenerima = [namapenerima.val(),alamatpenerima.val(),postal_code2.val(),hppenerima.val(),notes2.val()];
         		getdatakurir(selprov.val(),selprov2.val(),selkota.val(),selkota2.val(),selkec.val(),selkec2.val(),argspengirim,argspenerima);
                console.log(argspengirim);
                console.log(argspenerima);
                $('#cekongkir').prop( "disabled", false );
         	}

         });			 

 
         $('#postcode').keyup(function() {  			 
 			if(this.value.length >= 5)
 			{
  				loadpost1.show();
  				cityName.val('Mengambil data kota....');
  				provName.val('Mengambil data provinsi....');
 				getdatakota(this.value);
 			}
		 });

          $('#selectpayment').on('change', function () {
            var isi = $('option:selected', this).data("image");
            var bn = $('option:selected', this).text();
                        // alert(isi);
                        $("#imagecontainer").attr('src', isi);
                        $("#bankname").html(bn);
                        // .css("background-image", ($('option:selected', this).data("image")));


                    });



 // $('#selectpayment').msDropDown();

//  var resultData=["Mumbai","Delhi","Chennai","Goa"]
// $(document).ready(function(){
//    var myselect = $('<select>');
//      $.each(resultData, function(index, key) {
//        myselect.append( $('<option></option>').val(key).html(key) );
//        });
//       $('#selectCity').append(myselect.html());
// });



    $("#tambahbarang").click(function(){
      // var count=1;
  // console.log($("#brg_parent > div").length);

        // $("#inputtext").append("<input type='text' value='photo_" + count + "' name='photo_" + (count++) +"'/><br />");



var hit = $("#brg_parent > div").length + 1;
        $("#brg_parent").append('<div class=\"frm_brg\"><br><hr>     <div class=\"row form-group my-0\">        <div class=\"col-6\">            <div class=\"form-group\">                <input class=\"form-control\" type=\"text\" name=\"namebarang[]\"  id=\"namebarang-'+hit+'\" placeholder=\"Nama Barang\">                </div>            </div>            <div class=\"col-3\">                <div class=\"form-group\">                    <input class=\"form-control\" type=\"text\" name=\"harga[]\" id=\"harga-'+hit+'\" placeholder=\"Harga Barang\">                    </div>                </div>                <div class=\"col-3\">                    <div class=\"form-group\">                        <select class=\"custom-select\" name=\"selcategory[]\" id=\"selcategory-'+hit+'\" >                            <option value=\"\">Kategori</option></select>                    </div>                </div>            </div>            <div class=\"row form-group my-0\">                <div class=\"col-2\">                    <div class=\"form-group\">                        <input class=\"form-control\" type=\"text\" name=\"pjg[]\"  id=\"pjg-'+hit+'\" placeholder=\"Panjang\">                        </div>                    </div>                    <div class=\"col-2\">                        <div class=\"form-group\">                            <input class=\"form-control\" type=\"text\" name=\"lbr[]\" id=\"lbr-'+hit+'\" placeholder=\"Lebar\">                            </div>                        </div>                        <div class=\"col-2\">                            <div class=\"form-group\">                                <input class=\"form-control\" type=\"text\" name=\"tng[]\" id=\"tng-'+hit+'\" placeholder=\"Tinggi\">                                </div>                            </div>                            <div class=\"col-3\">                                <div class=\"form-group\">                                    <div class=\"controls\">                                        <div class=\"input-group\">                                            <input class=\"form-control form-control-16\" type=\"text\" name=\"brt[]\"  id=\"brt-'+hit+'\">                                                <div class=\"input-group-append\">                                                    <div class=\"input-group-text\">Kg</div>                                                </div>                                            </div>                                        </div>                                    </div>                                </div>                                <div class=\"col-3\">                                    <div class=\"form-group\">                                        <select class=\"custom-select\" name=\"selpaket[]\" id=\"selpaket-'+hit+'\">  <option value=\"\">Type Paket</option></select>                                       </div>                                    </div>                                </div>                           <button class=\"btnbuat btn btn-danger\" type=\"button\" onclick=\"remove_me(this)\" style=\"float:right;\" >remove</button> <div class=\"minibox\" </div>');

getkategori("selcategory-"+hit);
getpaket("selpaket-"+hit);


}); 
 

	   
     function getpaymentlist(sel)
        {        	
        	
            if(optpayment  === '')
          {
            var url = `https://helpdesk.posfin.id/fastku/getpayment`;
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'GET',
                    async: true,
                    contentType: "application/json",
                    success: function(res) {
                      // console.log(res);
                        optpayment = `<option value="">Pilih Pembayaran</option>`;    
  // $("#"+sel).msDropDown({byJson:{data:res['choice_payment_channels'], name:'payments2'}}).data("dd");

                        $.each(res['choice_payment_channels'], function(key, value) {
                         optpayment +=`<option value=\"${value.id}\" class=\"test\" data-image=\"${value.icon_filename}\">${value.name}</option>`;
                          // console.log(optpayment);
                        });
 
                        

 
                            $('#'+sel).html(optpayment); 
                            // $('#'+sel).msDropDown();

                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    }
                });
            }, 1000);
          }
          else
          {
             // console.log("ada data"+optpayment);
            $('#'+sel).html(optpayment); 
            $('#'+sel).msDropDown();
            // $(optpayment).select2({
            //  templateResult: formatState
            // });
  
          }

          

        } 
 

        function getpaket(sel)
        {         
          
            if(optpaket  === '')
          {
            var url = `https://helpdesk.posfin.id/fastku/getpackage`;
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'GET',
                    async: true,
                    contentType: "application/json",
                    success: function(res) {
                      // console.log(res);
                        optpaket = `<option value="">Type Paket</option>`;    

                        $.each(res['choice_packagings'], function(key, value) {
                         optpaket +=`<option value="${value.id}">${value.name}</option>`;
                          // console.log(optpaket);
                        });
 
                            $('#'+sel).html(optpaket); 
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    }
                });
            }, 1000);
          }
          else
          {
             // console.log("ada data"+optpaket);
            $('#'+sel).html(optpaket); 
          }

        } function getshipid(sel)
        {         
          
            
            var url = `https://helpdesk.posfin.id/fastku/getid`;
            
                $.ajax({
                    url:url,
                    type: 'GET',
                    async: true,
                    contentType: "application/json",
                    success: function(res) {
                       
                            $('#'+sel).val(res.shipment_id); 
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    }
                });
             

        }

        function getkategori(sel)
        {         
          if(optkategori  === '')
          {
            
            // console.log("kosong");
            var url = `https://helpdesk.posfin.id/fastku/getkategori`;
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'GET',
                    async: true,
                    contentType: "application/json",
                    success: function(res) {
                      // console.log(res);
                        optkategori = `<option value="">Kategori Barang</option>`;    

                        $.each(res['choice_categories'], function(key, value) {
                         optkategori +=`<option value="${value.id}">${value.name}</option>`;
                         
                        });
                            console.log(optkategori);
                            $('#'+sel).html(optkategori); 
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    }
                });
            }, 1000);
          }
          else 
          {
            // console.log("ada data"+optkategori);
            $('#'+sel).html(optkategori); 
          }
            
                            

        }
 
     function getprovince(loads,sel)
        {         
          loads.show();
          sel.hide();
            var url = `https://fintech-stg.bedigital.co.id/elog/graph/geoname?query={provincies{id,name}}`;
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'GET',
                    async: true,
                    contentType: "application/json",
                    success: function(res) {
                        let opt = `<option value="">Pilih Provinsi</option>`;    
                            res.data.provincies.forEach(function(i) {
                                opt += `<option value="${i.id}">${i.name}</option>`;

                            });
                            sel.html(opt);
                            sel.show();
                            loads.hide();
                     
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    }
                });
            }, 1000);

        }
        function getCitys(idProv,loads,sel) {
            // console.log("get city , id="+idProv);
 			loads.show();
        	sel.hide();
        	var url = `https://fintech-stg.bedigital.co.id/elog/graph/geoname?query={cities(province_id:`+idProv+`){id,name}}`;
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'GET',
                    async: true,
                     contentType: "application/json",
                    success: function(res) {
                    	// console.log("get city success , data ="+res);
                        let opt = `<option value="">Pilih Kota</option>`;
                         res.data.cities.forEach(function(i) {
                                opt += `<option value="${i.id}">${i.name}</option>`;

                            });
                            sel.html(opt);
                            sel.show();
                            loads.hide();
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    } 
                });
            }, 1000);
        }
         function getKec(idCity,loads,sel) {
            // console.log("get kec , id="+idCity);
 			loads.show();
        	sel.hide();
        	var url = `https://fintech-stg.bedigital.co.id/elog/graph/geoname?query={subdistricts(city_id:`+idCity+`){id,name}}`;
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'GET',
                    async: true,
                     contentType: "application/json",
                    success: function(res) {
                    	// console.log("get city success , data ="+res);
                        let opt = `<option value="">Pilih Kecamatan</option>`;
                         res.data.subdistricts.forEach(function(i) {
                                opt += `<option value="${i.id}">${i.name}</option>`;

                            });
                            sel.html(opt);
                            sel.show();
                            loads.hide();
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    }
                });
            }, 1000);
        }

 function getdatakurir(idprov,idprov2,idkota,idkota2,idkec,idkec2,argspengirim,argspenerima)
        {           
  console.log('array = '+argspenerima);
  // console.log('nama = '+argspengirim[0]);
  // console.log('alamat = '+argspengirim[1]);
        $("#kurirlistarray").html('');
           loadkurir.show();
           var postForm = { 
            'idprov_from'     : idprov ,
            'idprov_to'     : idprov2 ,
            'city_from'     : idkota ,
            'city_to'     : idkota2,
            'kec_from'     : idkec,
            'kec_to'     : idkec2,
            'namapengirim'     : argspengirim[0],
            'alamatpengirim'     : argspengirim[1],
            'postal_code'     : argspengirim[2],
            'hppengirim'     : argspengirim[3],
            'notes'     : argspengirim[4],
            'namapenerima'     : argspenerima[0],
            'alamatpenerima'     : argspenerima[1],
            'postal_code2'     : argspenerima[2],
            'hppenerima'     : argspenerima[3],
            'notes2'     : argspenerima[4],

        	};
 			$('#postcode').prop( "disabled", true );
            var url = `https://helpdesk.posfin.id/fastku/getlistkurir`; 
            var ars = 0;
            var grp = 0;
            var sgrp = 0;
            let isimain;
            let isihead;
            let isimain2;
            let isimain3;
            isimain='';
            isimain2='';
            isimain3='';
            isihead='';
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'POST', 
                     data: postForm,
                    success: function(res) {
                          console.log(res);
                          
                          loadkurir.hide();
                            $.each(res, function(i,row){    
                          	   	$.each(row, function(j,rows){
                             	console.log(" rootgroup = "+rows['rootgroup']);
                             	console.log('ars = '+ars);
                             	console.log('j ='+j); 
                              	var hitung=1;
                              	var l = 0;
                                isimain +="<li class=\"nav-item\">            <a class=\"nav-link\" href=\"#"+rows['rootgroup']+"\">                <i class=\"fa fa-fw fa-box-open text-gray mr-1\"></i> "+rows['rootgroup']+"          </a>        </li>";

                                  isimain2 +=" <div class=\"tab-pane pull-x fade fade-left show \" id=\""+rows['rootgroup']+"\" >                                  <ul class=\"nav nav-tabs nav-tabs-alt nav-justified\">";
                                   	$.each(rows['subgroups'], function(l,rowsgr){
                                    console.log("array 2, subgroups = "+rowsgr['group']+"-"+j+"-"+l);  
//javascript:pilihkurir(\""+srvc['courier_id']+"\",\""+srvc['service_code']+"\")\"
//onclick=\"' + obj.func(\'"+srvc['courier_id']+"\',\'"+srvc['service_code']+"\') + '\"
                                  isimain2 +="<li class=\"nav-item\"><a class=\"nav-link\" href=\"#"+j+"-"+l+"\">"+rowsgr['group']+"</a> </li>";
                                    isimain3 +=" <div class=\"tab-pane pull-x fade fade-left show\" id=\""+j+"-"+l+"\"  role=\"tabpanel\" >                                  <ul class=\"nav-items mb-0\"  data-toggle=\"tabs\" role=\"tablist\" >"; 
                                        $.each(rowsgr['choices'], function(m,srvc){   
                                          isimain3 +="<li>                                                                <a onclick=\"popup(\'"+srvc['courier_id']+"\',\'"+srvc['service_code']+"\',\'"+srvc['service_name']+"\',\'"+srvc['info']+"\',\'"+srvc['info2']+"\',\'"+srvc['icon_filename']+"\')\"  href=\"#\" class=\"text-dark media py-2\" > <div class=\"mr-3 ml-2\">                                                                                                                                                <img width=\"46px\" src=\""+srvc['icon_filename']+"\"/>                                                                    </div>                                                                    <div class=\"media-body\">                                                                        <div class=\"font-size-sm font-w600\">"+srvc['info']+"</div>                                                                        <div class=\"text-success\">"+srvc['service_name']+"</div>                                                                        <small class=\"font-size-sm text-muted\">"+srvc['info2']+"</small>                                                                    </div>                                                                 </a>                                                           </li>";
                                        console.log("array 3, choices = "+srvc['service_name']);
                                        console.log('sgrp = '+sgrp);     	          				        
                                        sgrp++; 
                                 	    });
                                           isimain3 +="</ul></div>";  
 
                                 	    grp++; 
                                    }); isimain2 +="</ul></div>";  
                                    
                                    ars++;
                                }); isimain3 +="</div>";   
                         	});
                             
                     // $("#kurirlist #menu1").html('');

                        $("#kurirlistarray").append("<div class=\"content-side\"><div class=\"block block-transparent pull-x pull-t\">    <ul class=\"nav nav-tabs nav-tabs-alt nav-justified\">"+isimain+"</ul><div class=\"block-content tab-content overflow-hidden\"> "+isimain2+" "+isimain3+"</div></div></div> "); 
                                       

                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                        loadkurir.hide();
                    }
                });
            }, 1000);

        }

         

          function getdatakota(kodep)
        {           
           var postForm = { 
            'postalcode'     : kodep 
        	};
 			$('#postcode').prop( "disabled", true );
            var url = `https://helpdesk.posfin.id/fastku/cekkodepos`; 
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'POST', 
                     data: postForm,
                    success: function(res) {
                          console.log(res);
                          if(res.city_id == 0 )
                          {
							alert("kodepos tidak dikenal");
							cityName.val('');
							provName.val('');
							$('#postcode').prop( "disabled", false );
							$('#postcode').val('');
							$('#postcode').focus();
                          }
                          else
                          {
                          	cityName.val(res.city_name);
							cityId.val(res.city_id);
							provId.val(res.province_id);
							provName.val(res.province_name);
                          }
					      loadpost1.hide();
                           $('#postcode').prop( "disabled", false );
                     
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                        loadpost1.hide();
                    }
                });
            }, 1000);

        }
        $('#postcode2').keyup(function() {  			 
 			if(this.value.length >= 5)
 			{
  				loadpost2.show();
  				cityName2.val('Mengambil data kota tujuan....');
  				provName2.val('Mengambil data provinsi tujuan....');
 				getdatakota2(this.value);
 			}
		 });


        

          function getdatakota2(kodep)
        {           
           var postForm = { 
            'postalcode'     : kodep 
        	};
        	$('#postcode2').prop( "disabled", true ); 
            var url = `https://helpdesk.posfin.id/fastku/cekkodepos`; 
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'POST', 
                     data: postForm,
                    success: function(res) {
                          console.log(res);
                      if(res.city_id == 0 )
                          {
							alert("kodepos tujuan tidak dikenal");
							cityName2.val('');
							provName2.val('');
							$('#postcode2').prop( "disabled", false );
							$('#postcode2').val('');
							$('#postcode2').focus();
                          }
                          else
                          {     
							cityName2.val(res.city_name);
							cityId2.val(res.city_id);
							provId2.val(res.province_id);
							provName2.val(res.province_name);
		                          loadpost2.hide();
		                           $('#postcode2').prop( "disabled", false );
		                  }
                     
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                        loadpost1.hide();
                    }
                });
            }, 1000);

        }


});

 
$(function() {
$('body').on('click', '.nav-link', function (e) {
     var id=$(this).attr('href');
         // alert("0 "+id);
         console.log("running click active "+id);
         $('body #kurirlistarray .tab-pane').removeClass("active");
         $('body #kurirlistarray '+id).removeClass("tab-pane pull-x fade fade-left show");
         $('body #kurirlistarray '+id).addClass(["tab-pane","show","pull-x","fade","fade-left"]);
         $('body #kurirlistarray '+id).addClass(["active"]);
         // $('body #kurirlistarray '+id).addClass(["show"]);
         // , "pull-x" , "fade" , "fade-left" , "show", "active"]);
         // $('body #kurirlistarray '+id).hide(); 
         
         // $('body #kurirlistarray '+id).filter(function(){return this.href==location.href}).parent().addClass('active').siblings().removeClass('active')
        $('body #kurirlistarray '+id).click(function(){
            $('body #kurirlistarray '+id).parent().addClass('active').siblings().removeClass('active')  

        });
            e.preventDefault();


}); 


  


}); 

function popup(a,b,c,d,e,f)
          {
           // alert("Kurir terpilih");
$("#kurirpilihan").html('<br><span class="" for="prependedInput"><strong>Kurir Pilihan</strong></span><input type="hidden" id="courier_id" name="courier_id" value="'+a+'"><input type="hidden" id="courier_name" name="courier_name" value="'+b+'"><div class="">                                                                     <a class="block block-rounded block-bordered block-link-shadow" href="javascript:void(0)">                                        <div class="block-content block-content-full d-flex align-items-center justify-content-between">                                            <div>                                                <div class="font-w600 mb-1">'+c+'</div>                                                <div class="font-size-sm text-muted">'+e+' , '+d+'</div>                                            </div>                                            <div class="ml-3">                                                <img class="img-avatar" src="'+f+'" alt="">                                            </div>                                        </div>                                    </a>                              </div>');

var url = `https://helpdesk.posfin.id/fastku/setkurir`;
var postForm = { 
            'idkurir'     : a ,
            'kurirname'     : b 
        	};
	$.ajax({
	    url:url,
	    type: 'POST',
     	data: postForm, 
	    success: function(res) {
	        let opt = ` `;
	         res.data.courier.forEach(function(i) {
	                opt += `<option value="${i.id}">${i.name}</option>`;

	            });
	           console.log(opt);
	    },
	    error: function(xhr, stat, err) {
	        console.log(err);
	    }
	});
            

// "mutation {
//     draftShipmentSetCourier (
//         courier_id : 14,
//         service_code : ""economy""
//     ) {
//       shipment_id, 
//       courier { courier_id, courier_code, courier_name, service_code, service_name, etd, valid, message },
//       cost { delivery_fee, grand_total }
//     }
// }
// "

// <div class="row form-group my-0"><div class="col-12"><div class="form-group"><div class="input-group"><input class="form-control" type="text" value="'+c+'" disabled=""><div class="input-group-append"><div class="input-group-text">Service Kurir</div></div></div></div></div></div><div class="row form-group my-0"><div class="col-12"><div class="form-group"><div class="input-group"><input class="form-control" type="text" value="'+d+'" disabled=""><div class="input-group-append"><div class="input-group-text">Harga </div></div></div></div></div></div>


//
//            Swal.fire({
//   title: 'Are you sure?'+a,
//   text: "You won't be able to revert this!"+b,
//   icon: 'warning',
//   showCancelButton: true,
//   confirmButtonColor: '#3085d6',
//   cancelButtonColor: '#d33',
//   confirmButtonText: 'Yes, delete it!'
// }).then((result) => {
//   if (result.isConfirmed) {
//     Swal.fire(
//       'Deleted!',
//       'Your file has been deleted.',
//       'success'
//     )
//   }
// })

          }
 function remove_me(elm){
 Swal.fire({
    title: 'Anda Yakin menghapus barang ini?',
    text: "Data akan hilang!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus!'
}).then((result) => {
  if (result.isConfirmed) {
    $(elm).parent().remove();
    Swal.fire(
      'Deleted!',
      'List Barang Telah Terhapus.',
      'success'
    )
  }
})
   
}
 
  $("#frmtrxkurir").submit(function(e) {
  e.preventDefault(); 
  var urlsave = `https://helpdesk.posfin.id/fastku/savedatatrx`; 
  var postForm = $("#frmtrxkurir").serialize();
  Swal.fire({
    title: 'Anda Yakin Menyimpan Data Transaksi?',
    text: "Data akan disimpan!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Simpan!'
  }).then((result) => {
  if (result.isConfirmed) {
    // $(elm).parent().remove();

     $.ajax({
                    url:urlsave,
                    type: 'POST', 
                     data: postForm,
                    success: function(res) {
                          console.log(res);
                            
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                        // loadpost1.hide();
                    }
                });


    Swal.fire(
      'Saves!',
      'List Barang Telah Tersimpan.',
      'success'
    )
  }
  });
});

 
              
            // $.post('{{ secure_url('/ma/update_agent_save.html') }}', $("#frmupdateAgentHD").serialize())
            // // Serialization looks good: name=textInNameInput&&telefon=textInPhoneInput etc
            // .done(function(data) {
            //     if (data.error.code == 200) {
            //         $("#msginfopush").removeClass("hide");   
            //         $("#msginfopushchild").html("<button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button><strong>Update Success</strong>");   
            //         $("#msginfopushchild").addClass("alert-success");   
            //         $("#msginfopushchild").removeClass("alert-danger");   
            //         // $("#frmsendpushagen").[0].reset();
            //         // $("#frmupdateAgentHD")[0].reset()

            //     } else {
            //         // console.log("error");
            //         $("#msginfopush").removeClass("hide");   
            //         $("#msginfopushchild").html("<strong>Update Failed - "+ data.error.messages +"</strong> ");  
            //         $("#msginfopushchild").addClass("alert-danger");                    
            //         $("#msginfopushchild").removeClass("alert-success");   
   

            //     }
            // });
          

</script>
@endsection