@extends('admin.trx.layout') 
@section('style')
<style type="text/css">
    main.c-main {
    padding: 15px 0 0 0;
}
 #mapdiv {
        width:100%; height:100%; margin:0;
    }
</style>
@endsection
@section('content') 
               
                <div class="content">
                    <!-- Block Tabs -->
                    <h2 class="content-heading">Pengaturan Akun</h2>
                    <div class="row">
                        <div class="col-lg-12">


                            <!-- Block Tabs Alternative Style -->
                            <div class="block block-rounded">
                                <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#btabs-alt-static-home">Akun</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#btabs-alt-static-profile">Email & No.Hp</a>
                                    </li> <li class="nav-item">
                                        <a class="nav-link" href="#btabs-alt-static-sandi">Kata Sandi</a>
                                    </li> <li class="nav-item">
                                        <a class="nav-link" href="#btabs-alt-static-alamat">Alamat</a>
                                    </li><!-- 
                                    <li class="nav-item ml-auto">
                                        <a class="nav-link" href="#btabs-alt-static-settings"><i class="si si-settings"></i></a>
                                    </li> -->
                                </ul>
                                <div class="block-content tab-content">
                                    <div class="tab-pane active" id="btabs-alt-static-home" role="tabpanel">
                                       
                                       <div class="form-group"><label class="" for="company">Nama Lengkap</label><input class="form-control" type="text" id="nama" placeholder="Nama Lengkap"></div>

                                    </div>
                                    <div class="tab-pane" id="btabs-alt-static-profile" role="tabpanel">
                                        
                                        <div class="card-body"><div class="form-group"><label class="" for="email">Alamat Email</label><input class="form-control" type="text" id="email" placeholder="email" disabled=""></div><div class="form-group"><label class="" for="email">Nomor HP</label><input class="form-control" type="text" id="hp" placeholder="No HP" disabled=""></div><span class="text-info"> <i class="si si-phone"></i> Harap hubungi CS untuk melakukan perubahan email dan no.HP</span></div>

                                    </div>
                                    <div class="tab-pane" id="btabs-alt-static-sandi" role="tabpanel">
                                       
                                       <div class="card-body"><div class="form-group"><label class="" for="email">Kata Sandi Lama</label><input class="form-control" type="text" id="email" placeholder="email"></div><div class="form-group"><label class="" for="email">Kata Sandi Baru</label><input class="form-control" type="text" id="hp" placeholder="Masukkan kata sandi baru"></div><div class="form-group"><label class="" for="email">Ulangi Kata Sandi Baru</label><input class="form-control" type="text" id="hp" placeholder="Konfirmasi kata sandi baru"></div></div>

                                    </div> 
                                    <div class="tab-pane" id="btabs-alt-static-alamat" role="tabpanel">
                                       
                                       <div id="contentalamat">
                                           <ul class="list-group" role="list-items"><li class="list-group-item list-group-item-action active"><h5 class="d-flex w-100 justify-content-between">Murtopo<small><span class="badge badge-info">Alamat utama</span></small></h5><div class="mb-1">Taman Alfa Indah Blok 4, Petukangan, Pesanggrahan Jakarta Selatan 122665<p></p><a href="/fastku/pengaturanalamat" class="text-info">ubah</a></div></li></ul>
                                        </div>

                                </div>
                            </div>
                            <!-- END Block Tabs Alternative Style -->
                        </div>
                        
                    </div>
                    <!-- END Block Tabs -->

                  
                           
                    </div>  

@endsection