
        <!-- page content -->
        <div class="right_col" role="main">
          <div id="tablefrmedit"></div>
          <div id="divdetailmainagent">
            <div class="page-title">
              <div class="title_left">
                <h3>Master Agent Detail Profile</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  
                </div>
              </div>
            </div>
             

            <div class="clearfix"></div>
            <div id="msg"></div>    

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">  
                   <ul class="nav navbar-right panel_toolbox">
                      <li><a class="red bold" href="javascript:getView('/admin/listmainagent.html');"  class="collapse-link"><i class="fa fa-chevron-left"></i> <b> List Main Agent</b></a> 
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="img/wiro.jpg" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      @if($rs->profile_type == "CORPORATE")
                      <h3> {{$rs->company_name}}</h3>
                      @else
                      <h3> {{$rs->fullname}}</h3>
                      @endif
                      <small>( {{$rs->master_agent_id}} )</small>
                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-map-marker user-profile-icon"></i>  {{$rs->address}}  {{$rs->village_name}} {{$rs->district_name}}  {{$rs->city_name}},  {{$rs->province_name}}
                        </li>

                        <li>
                          <i class="fa fa-envelope-o user-profile-icon"></i> {{$rs->email}} 
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-phone user-profile-icon"></i> {{$rs->phone_number}}  
                        </li>
                      </ul>


                      <a class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm"  ><i class="fa fa-user m-right-xs"></i> Edit Status</a>
                    
  <!--

                         <form role="form" id="frm" onsubmit="doDelete();return false;">
                          @csrf
                          <input type="hidden" name="id" value="{{ $rs->master_agent_id }}" />
                          <button class="btn btn-danger" type="submit" id="button-remove-MA"><i class="fa fa-remove m-right-xs"></i> Remove </button>
                          </form>

                  

                      start skills 
                      <h4>Skills</h4>
                      <ul class="list-unstyled user_data">
                        <li>
                          <p>Web Applications</p>
                          <div class="progress progress_sm">
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                          </div>
                        </li>
                        <li>
                          <p>Website Design</p>
                          <div class="progress progress_sm">
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="70"></div>
                          </div>
                        </li>
                        <li>
                          <p>Automation & Testing</p>
                          <div class="progress progress_sm">
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="30"></div>
                          </div>
                        </li>
                        <li>
                          <p>UI / UX</p>
                          <div class="progress progress_sm">
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                          </div>
                        </li>
                      </ul>
                        end of skills -->

                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">


                      <ul class="stats-overview">
                        <li>
                          <span class="name"> Total Agent </span>
                          <span class="value text-success"> 2300 </span>
                        </li>
                        <li>
                          <span class="name"> Total SubMaster Agent</span>
                          <span class="value text-success"> 20 </span>
                        </li> 
                        <li>
                          <span class="name"> Total Product Available</span>
                          <span class="value text-success"> 200 </span>
                        </li>
                       
                      </ul>
                         <div class="clearfix"></div>
                       <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Master Agen Profile</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Service & Product</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_contentcommisi" role="tab" id="profile-tabcms" data-toggle="tab" aria-expanded="false">Commision</a>
                          </li> 
                          <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Agent List</a>
                          </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                         
                          <ul class="nav navbar-right panel_toolbox">
                                          <li><button id="addnew" onclick="showEditForm('{{$rs->master_agent_id}}');" class="btn btn-sm btn-success btn-block" type="button">Edit Profile</button>
                                          </li> 
                         </ul>
                         <div class="profile-user-info">
                                

                                <div class="profile-info-row">
                                  <div class="profile-info-name">Master Agent Type </div>

                                  <div class="profile-info-value">
                                    <span> {{$rs->profile_type}}</span>
                                  </div>
                                </div>
                                
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Master Agent ID </div>

                                  <div class="profile-info-value">
                                    <span> {{$rs->master_agent_id}}</span>
                                  </div>
                                </div>
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Username </div>

                                  <div class="profile-info-value">
                                    <span> {{$rs->username}}</span>
                                  </div>
                                </div>

                                  @if($rs->profile_type == "CORPORATE")
                          
                                  <div class="profile-info-row">
                                      <div class="profile-info-name"> Company Name </div>
                                      <div class="profile-info-value">
                                        <span>{{$rs->company_name}}</span>
                                      </div>
                                  </div>
                                  <div class="profile-info-row">
                                      <div class="profile-info-name"> Departemen </div>
                                      <div class="profile-info-value">
                                        <span>{{$rs->department}}</span>
                                      </div>
                                  </div>
                                  <div class="profile-info-row">
                                      <div class="profile-info-name"> Contact Person </div>
                                      <div class="profile-info-value">
                                        <span>{{$rs->contact_person}}</span>
                                      </div>
                                  </div>
                                  <div class="profile-info-row">
                                      <div class="profile-info-name"> Branch Office </div>
                                      <div class="profile-info-value">
                                        <span>{{$rs->branch_office_level}}</span>
                                      </div>
                                  </div>

                                  @else
                                 
                                  <div class="profile-info-row">
                                      <div class="profile-info-name"> Fullname </div>
                                      <div class="profile-info-value">
                                        <span>{{$rs->fullname}}</span>
                                      </div>
                                  </div>
                                  <div class="profile-info-row">
                                      <div class="profile-info-name"> Place Birth </div>
                                      <div class="profile-info-value">
                                        <span>{{$rs->place_birth}}</span>
                                      </div>
                                  </div>
                                  <div class="profile-info-row">
                                      <div class="profile-info-name"> Birth Date </div>
                                      <div class="profile-info-value">
                                        <span>{{$rs->birth_date}}</span>
                                      </div>
                                  </div>
                                  <div class="profile-info-row">
                                      <div class="profile-info-name"> Gender </div>
                                      <div class="profile-info-value">
                                        <span>{{$rs->gender}}</span>
                                      </div>
                                  </div>

                                  @endif
  
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Location </div>

                                  <div class="profile-info-value">
                                    <i class="fa fa-map-marker light-orange bigger-110"></i>
                                    <span>  {{$rs->address}}  {{$rs->village_name}} {{$rs->district_name}}  {{$rs->city_name}} {{$rs->postal_code}} </span>
                                    <span>{{$rs->province_name}}</span>
                                    <span>{{$rs->description}}</span>
                                  </div>
                                </div>
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Description </div>

                                  <div class="profile-info-value">
  
                                    <span>{{$rs->description}}</span>
                                  </div>
                                </div>


                                <div class="profile-info-row">
                                  <div class="profile-info-name"> NPWP </div>

                                  <div class="profile-info-value">
                                    <span>{{$rs->npwp_number}}</span>
                                  </div>
                                </div>


                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Email </div>

                                  <div class="profile-info-value">
                                    <span>{{$rs->email}}</span>
                                  </div>
                                </div>

                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Phone No </div>

                                  <div class="profile-info-value">
                                    <span>{{$rs->phone_number}} </span>
                                  </div>
                                </div>

                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Fax No </div>

                                  <div class="profile-info-value">
                                    <span>{{$rs->fax_number}} </span>
                                  </div>
                                </div>

                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Balance Name </div>

                                  <div class="profile-info-value">
                                    <span>{{$rs->balance_name}} </span>
                                  </div>
                                </div> 

                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Balance  </div>

                                  <div class="profile-info-value">
                                    <span>{{$rs->balance_value}} </span>
                                  </div>
                                </div>
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Max SubLevel  </div>

                                  <div class="profile-info-value">
                                    <span>{{$rs->master_agent_max_downline}} </span>
                                  </div>
                                </div>

                                 <div class="profile-info-row">
                                  <div class="profile-info-name"> Status  </div>

                                  <div class="profile-info-value">



                          @if ($rs->status == 'REGISTERING')
                        <span class="btn label label-info"><i class="fa fa-circle-thin m-right-xs"></i>
                        @elseif ($rs->status == 'ACTIVE')
                        <span class="btn  label label-success"><i class="fa fa-check m-right-xs"></i>
                        @elseif ($rs->status == 'SUSPENDED')
                        <span class="btn label label-warning"><i class="fa fa-eye-slash m-right-xs"></i>
                        @elseif ($rs->status == 'DEACTIVATED')
                        <span class="btn label label-danger"><i class="fa fa-ban m-right-xs"></i>
                        @elseif ($rs->status == 'UNAPPROVED')
                        <span class="btn label label-danger"><i class="fa fa-check m-right-xs"></i>
                        @endif

                                    {{$rs->status}} </span>

 

                                  </div>
                                </div>
 
                              </div> 
  
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                            <div id="listService"></div>
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_contentcommisi" aria-labelledby="profile-tab">
                            <div id="listKomisi"></div>
                          </div> 
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            <p>List Agen </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> 
                  <div class="row">

                     <div class="x_title">  
                   
                    <div class="clearfix"></div>
                  </div>

                  </div>
        </div>
        <!-- /page content -->




                        <!-- Small modal -->  

                  <div class="modal fade bs-example-modal-sm" id='modalStatus' tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content"> 
                       <form role="form" id="frmstatus" onsubmit="doUpdateStatus();return false;">
                          @csrf
                          <input type="hidden" name="id" value="{{ $rs->master_agent_id }}" />
 
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"> Update Master Agent Status </h4>
                        </div>                           
                      

                        <div class="modal-body">This user current status is  : 
                          <h4> {{$rs->status}} </h4>

                          
                          Update :
                         

                          <select id="status" name="status" class="form-control">
                            <option value="">Choose..</option>
                            <option value="REGISTERING">REGISTERING</option>
                            <option value="ACTIVE">ACTIVE</option>
                            <option value="SUSPENDED">SUSPENDED</option>
                            <option value="DEACTIVATED">DEACTIVATED</option>
                            <option value="UNAPPROVED">UNAPPROVED</option> 
                          </select>   
                          Reason : <textarea id="reason" name="reason"></textarea> 
                        </div> 
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Save changes</button>
                        </div> 
                      </form>

                      </div>
                    </div>
                  </div>
                  <!-- /modals -->

        <script type="text/javascript">
          

        // list_service
        load_service('{{$rs->master_agent_id}}');
        load_fee('{{$rs->master_agent_id}}');
        function load_service(id)
        {
          $("#listService").append("<img src='img/ajax-loaders/ajax-loader-8.gif'><br> Loading.....");
         $.ajax({
                      type: "GET",
                      url: "/admin/listservice/"+id+".html",                     
                      success: function(msg) {
                       $("#listService").html(msg);                                                            
                      }
                  });
        } 
        function load_fee(id)
        {
          $("#listKomisi").append("<img src='img/ajax-loaders/ajax-loader-8.gif'><br> Loading.....");
         $.ajax({
                      type: "GET",
                      url: "/admin/listfee/"+id+".html",                     
                      success: function(msg) {
                       $("#listKomisi").html(msg);                                                            
                      }
                  });
        }

 




   function doUpdateStatus() {
        if (confirm('Are you sure you want to change  this Main Agent status?')) {
            sendForm3({
                method: 'POST',
                form: 'frmstatus',
                messageView: 'msg',
                messageSuccess: 'Master Agent Status Saved',
                url: '{{ secure_url('/mainagent_status.html') }}',
                redirect:  '{{ secure_url('/listmainagent.html') }}',
                beforeSubmit: function() {
                    $('#button-close').attr('disabled', 'disabled');
                    $('#button-save').attr('disabled', 'disabled');
                    $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Removing...');
                },
                afterSubmit: function() {
                 //  modal.remove();
                     $('.modal-backdrop').hide(); 
                    //$('.modal-backdrop').removeClass('show');
                   // $('#modalStatus').modal('hide');
                 } 
            }); 
        }
       // modal.remove();
    }
 
 
   function doDelete() {
        if (confirm('Are you sure you want to remove this Main Agent?')) {
            sendForm3({
                method: 'POST',
                form: 'frm',
                messageView: 'msg',
                messageSuccess: 'Master Agent Removed',
                url: '{{ secure_url('/mainagent_delete.html') }}',
                redirect:  '{{ secure_url('/listmainagent.html') }}',
                beforeSubmit: function() {
                    $('#button-close').attr('disabled', 'disabled');
                    $('#button-save').attr('disabled', 'disabled');
                    $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Removing...');
                },
                afterSubmit: function() {
                    $('#button-close').removeAttr('disabled');
                    $('#button-save').removeAttr('disabled');
                    $('#button-save').html('<i class="glyphicon glyphicon-remove"></i> Remove');                }
            });
        }
    }
 


      function showEditForm(id)
      {
       // alert("test");
        $("#tablefrmedit").removeClass('hideform');
        $("#divdetailmainagent").addClass('hideform');
        $.ajax({
                      type: "GET",
                      url: "editform/"+id+".html",                     
                      success: function(msg) {
                       $("#tablefrmedit").html(msg);                                                            
                      }
                  });


       }
        </script>