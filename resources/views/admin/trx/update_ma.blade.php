@extends('admin.main_agent.layout')

@section('content')


    <div class="row-fluid">
       
        
        <div class="box col-md-6">
            <div class="box-inner">                
                <div class="box-content"> 
                  
                    <div class="clearfix"></div>
                    <h4 class="blue">Update Main Agent</h4>
                    <hr/>

                    <div class="form-group col-md-6">
                        <label for="email">Main Agent ID </label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <span class="form-control" id="master_agent_id">{{ $rs->master_agent_id }}</span>
                        </div>
                    </div>


                    <div class="form-group col-md-6">
                        <label for="email">Main Agent Name </label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>                            
                            <input type="text"  class="form-control" id="master_agent_name" value="{{ $rs->master_agent_name }}">
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="email"> Master Agent Upline</label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-tags"></i></span>                            
                            <select class="form-control" id="master_agent_id">
                                <option value="{{ $rs->master_agent_id }}">{{ $rs->master_agent_id }}-{{ $rs->master_agent_name }}</option>
                                    @foreach( $ma_list as $rowma)
                                        <option value="{{ $rowma->master_agent_id }}">{{ $rowma->master_agent_id }}-{{ $rowma->master_agent_name }}</option>
                                    @endforeach

                            </select>
                        </div>
                    </div>   
                  
 

                   <div class="form-group col-md-6">
                        <label for="email"> Email</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span> 
                            <input type="text"  class="form-control" id="email" value="{{ $rs->email }}">
                        </div>
                    </div> 

                   <div class="form-group col-md-6">
                        <label for="email"> Phone Number</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                            <input type="text"  class="form-control" id="phone_number" value="{{ $rs->phone_number }}">                            
                        </div>
                    </div> 
 
<!-- 
                    <div class="form-group col-md-2">
                        <label for="maxterminal"> Max. Terminal </label>

                        <div class="clearfix"></div>
                        <span class="form-control">{{ $rs->max_terminal }}</span>
                    </div> -->

                    <div class="clearfix"></div>                            
                    <hr/>

                    <div class="form-actions">
                        <a href="javascript:getContentInBox('{{ url('/agency/master_agent.html') }}');" class="btn btn-warning btn-sm" id="button-close"><i class="glyphicon glyphicon-arrow-left"></i> Close</a>
                    </div>
                </div>
            </div> 
        </div>
    </div>

    <!-- @include('admin.utils.image_upload') -->
 
@endsection