
<div id="formagent" class="hideform">

</div> 
<div id="tablemainagent">
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <!--  <h3>Users <small>Some examples to get you started</small></h3>!-->
              </div>
 
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Master Agent <small></small></h2>
                       <ul class="nav navbar-right panel_toolbox">
                                          <li><button id="addnew" onclick="showAgentForm();" class="btn btn-sm btn-success btn-block" type="button">Add New</button>
                                          </li> 
                      </ul>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                       
                    </p>
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Master ID</th>
                          <th>Master Name</th>
                          <th>Status</th> 
                        </tr>
                      </thead> 
                      <tbody>


                      @if (count($listagent) > 0)
                    

                       @foreach ($listagent  as $row)
                          <tr>
                              <td><a class="red bold" href="javascript:getView('/admin/detailmainagent/{{ $row->agent_id }}.html');">{{ $row->agent_id }}</a></td>
                              <td>{{ $row->agent_name }}</td>
                              <td> 
                              @if ($row->status == 'REGISTERING')
                              <span class="label label-info">
                              @elseif ($row->status == 'ACTIVE')
                              <span class="label label-success">
                              @elseif ($row->status == 'SUSPENDED')
                              <span class="label label-warning">
                              @elseif ($row->status == 'DEACTIVATED')
                              <span class="label label-danger">
                              @elseif ($row->status == 'UNAPPROVED')
                              <span class="label label-danger">
                              @endif
                              {{ $row->status }}
                              </span>
                              </td> 
                        </tr> 
                        @endforeach

                      @endif

                      </tbody>
                    </table>
                  </div></div>
 
 </div>
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });

      function showAgentForm()
      {
       // alert("test");
        $("#formagent").removeClass('hideform');
        $.ajax({
                      type: "GET",
                      url: "{{ secure_url('/inputform.html') }}",                     
                      success: function(msg) {
                       $("#formagent").html(msg);                                                            
                      }
                  });


        $("#tablemainagent").addClass('hideform');
      }
    </script>
    <!-- /Datatables -->