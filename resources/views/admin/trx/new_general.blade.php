                        <div class="clearfix"></div>
                        <h4 class="blue">General</h4>
                        <hr/>

                        <div class="form-group col-md-12">
                            <label for="profile-type"> Profile Type </label>

                            <div class="clearfix"></div>

                            <label class="radio-inline blue">
                                <input name="profile-type" id="profile-type1" value="{{ $PROFILE_TYPE_CORPORATE }}" type="radio" class="ace" checked="checked" />
                                <span class="lbl"> Corporate</span>
                            </label>
                            <label class="radio-inline blue">
                                <input name="profile-type" id="profile-type2" value="{{ $PROFILE_TYPE_PERSONAL }}" type="radio" class="ace" />
                                <span class="lbl"> Personal</span>
                            </label>
                        </div>

                        <div class="form-group" id="view-corporate">
                            @include('main_agent.new_profile_corporate')
                        </div>

                        <div class="form-group" id="view-personal">
                            @include('main_agent.new_profile_personal')
                        </div>

                        <div class="form-group col-md-12">
                            <label for="description"> Description </label>

                            <textarea id="description" name="description" class="form-control"></textarea>
                        </div>
                        
                        &nbsp;
                        <div class="clearfix"></div>
                        <h4 class="blue">Account</h4>
                        <hr/>

                        <div class="form-group col-md-4">
                            <label for="username"> User Name  <small class="small red"><i>(Required)</i></small></label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                                <input type="text" id="username" name="username" placeholder="Agent User Name" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="password"> Password  <small class="small red"><i>(Required)</i></small></label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                                <input type="password" id="password" name="password" placeholder="Password" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="cpassword"> Confirm Password  <small class="small red"><i>(Required)</i></small></label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                                <input type="password" id="cpassword" name="cpassword" placeholder="Confirm Password" class="form-control" />
                            </div>
                        </div>

                        &nbsp;
                        <div class="clearfix"></div>
                        <h4 class="blue">Balance</h4>
                        <hr/>

                        <div class="form-group col-md-6" id="balance-type">
                            <label for="balance-type"> Balance Type <small class="small red"><i>(Required)</i></small></label>

                            <div class="clearfix"></div>

                            <select class="form-control" name="balance-type" id="balance-type" data-rel="chosen">
                                @foreach ($balances as $bal)
                                <option value="{{ $bal->id }}">{{ $bal->balance_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6" id="balance-val">
                            <label for="balance"> Balance Amount <small class="small red"><i>(Required)</i></small></label>

                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="text" class="form-control" name="balance" id="balance" placeholder="Amount of balance" value="" />
                            </div>
                        </div>

                        &nbsp;
                        <div class="clearfix"></div>
                        <h4 class="blue">Agents</h4>
                        <hr/>

                        <div class="form-group col-md-12">
                            <label for="uplink"> Upline Main Agent </label>
                            
                            <div class="clearfix"></div>
                            
                            <select class="form-control" id="uplink" name="uplink" data-rel="chosen">
                                <option value="0"> --- </option>
                                @foreach ($uplinks as $up)
                                <option value="{{ $up->agent_id }}">{{ ($up->fullname != '' ? $up->fullname : ($up->company_name != '' ? $up->company_name : $up->username)) }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="paymode"> Payment Mode  <small class="small red"><i>(Required)</i></small></label>

                            <div class="clearfix"></div>

                            <div class="col-md-4">
                                <label class="radio blue">
                                    <input name="paymode" id="paymode1" value="PREPAID" type="radio" checked="checked" />
                                    <span class="lbl"> Prepaid</span>
                                </label>
                                <label class="radio blue">
                                    <input name="paymode" id="paymode2" value="POSTPAID" type="radio" />
                                    <span class="lbl"> Postpaid</span>
                                </label>
                                <label>Max. Transaction/day</label>
                                <div class="input-group">
                                    <span class="input-group-addon">Rp.</span>
                                    <input type="text" id="max-trx" name="max-trx" maxlength="11" placeholder="Max. Transactions per day" class="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="maxterminal"> Max. Terminal Default <small class="small red"><i>(Required)</i></small></label>

                            <input type="text" id="maxterminal" name="maxterminal" placeholder="Maximum number of Terminal" class="form-control" value="1" />
                        </div>

                        <div class="form-group col-md-12">
                            <label for="auto-approve"> Agents Approval</label>

                            <div class="clearfix"></div>
                            
                            <div class="controls col-md-10">
                                <label class="checkbox blue">
                                    <input name="auto-approve" id="auto-approve" value="1" type="checkbox" />
                                    <span class="lbl"> Auto Approval </span>
                                </label>                                
                            </div>
                        </div>
