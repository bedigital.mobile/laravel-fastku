<div class="" role="main">
    <div class="">
    <div class="page-title">
        <div class="title_left">
        <h3>Update Service & Product Master Agent</h3>
        <br>
        </div>
        <div class="title_right"></div>
    </div>
    <div class="clearfix"></div> 
    <div id="msg"></div>   

<form role="form" id="frm" name="frm" onsubmit="doEdit();return false;"> 
  @csrf

    <input type="hidden" name="id" id="id" value="{{ $rs->ma_id }}" />
<div class="row"> 
    <div class="form-group col-md-12">
        @foreach ($services as $srv)
        <div class="form-group col-md-12">
                 <input type="checkbox" name="service[]" id="service{{ $srv['service'][0] }}" value="{{ $srv['service'][0] }}" onclick="checkService(this)" {!! ($srv['service'][2] == 1 ? 'checked=""' : '') !!} />
                {{ $srv['service'][1] }}
 
            <div class="clearfix"></div>

            @foreach ($srv['product'] as $prd)
            <div class="col-md-12">
                     <input name="product[]" id="{{ $srv['service'][0] }}product{{ $prd[1] }}" value="{{ $prd[1] }}" type="checkbox" onclick="checkProduct(this);" {!! ($prd[7] == 1 ? 'checked=""' : '') !!} />
                    <span class="lbl"> {{ $prd[2]}} </span>

                                    <div class="clearfix"></div>
                                    <table  class="table table-bordered">
                                    <tr>
                                 
                                     <td width="50">Kode Produk</td> 
                                    <td width="200">Produk Name</td>
                                     </tr>
                                     @if($prd[8] == 'Y')
                                      @foreach ($prd[9] as $brk)
                                            <tr>
                                            <td> {{ $brk[0] }} </td> 
                                            <td> {{ $brk[1] }} </td>
                                            </tr>
                                      @endforeach

                                     @else
                                        <tr>
                                            <td> {{ $prd[1] }} </td> 
                                            <td> {{ $prd[2] }} </td>
                                        </tr>

                                     @endif

                                           
                                     </table>
 
            </div>
            @endforeach
        </div>
        @endforeach
    </div>
        <div id="frm-product">
            <input type="hidden" name="service-list" id="service-list" value="" />
            <input type="hidden" name="product-list" id="product-list" value="" />
        </div>
</div>


<div class="row">
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button type="button" id="canceledit"  onclick="javascript:getView('/admin/detailmainagent/{{ $rs->ma_id }}.html');"  class="btn btn-primary">Cancel</button>
        <button type="submit" class="btn btn-success">Submit</button>
        </div>
    </div>
</div> 


</form>
</div>
</div> 
<script type="text/javascript">
    
  
    function beforeSubmit() {
        $('#button-close').attr('disabled', 'disabled');

        $('#button-save').attr('disabled', 'disabled');
        $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Saving...');
    }

    function afterSubmit() {
        $('#button-close').removeAttr('disabled');

        $('#button-save').removeAttr('disabled');
        $('#button-save').html('<i class="glyphicon glyphicon-save"></i> Save');
    }



    function doEdit(id) {        
        sendForm3({
            method: 'POST',
            form: 'frm',
            messageView: 'msg',            
            messageSuccess: 'Service Update Saved',
            url: '{{ secure_url('/servicemagent_edit.html') }}',
            redirect:  '/admin/detailmainagent/{{ $rs->ma_id }}.html',
            beforeSubmit: function () {
                serviceBuild();
                productBuild(); 
                beforeSubmit();
            },
            afterSubmit: afterSubmit
        });
    }

///admin/detailmainagent/1537531119.html
 

    var services = [];
    var products = [];
    
    function checkService(cb) {
        if ($(cb).is(':checked')) {
            var tmp = [];
            for (var i=0; i<this.services.length; i++) {
                if (this.services[i] != $(cb).val()) {
                    var ln = tmp.length;
                    tmp[ln] = this.services[i];
                }
            }
            this.services = tmp;
        } else {
            var ln = this.services.length;
            this.services[ln] = $(cb).val();
        }
        console.log(this.services);
        $("input[id^='" + $(cb).val() + "product']").each(function (i, el) {
            $(el).prop('checked', $(cb).is(':checked'));
            checkProduct(el);
        });
    } 
    function checkProduct(cb) {
        if ($(cb).is(':checked')) {
            var tmp = [];
            for (var i=0; i<this.products.length; i++) {
                if (this.products[i] != $(cb).val()) {
                    var ln = tmp.length;
                    tmp[ln] = this.products[i];
                }
            }
            this.products = tmp;
        } else {
            var ln = this.products.length;
            this.products[ln] = $(cb).val();
        }
        console.log(this.products);
        $("input[id^='" + $(cb).val() + "break']").each(function (i, el) {
            $(el).prop('checked', $(cb).is(':checked'));
            checkProduct(el);
        });
    }
    
    function serviceBuild() {
        var srv = this.services.length > 0 ? this.services.join(',') : '';
        $('#service-list').val(srv);
    } 
    
    function productBuild() {
        var prod = this.products.length > 0 ? this.products.join(',') : '';
        $('#product-list').val(prod);
    }
    
</script>