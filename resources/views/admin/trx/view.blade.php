@extends('admin.trx.layout')

@section('content')
    <div>
        <ul class="breadcrumb">
            <li><a href="javascript:getPage('{{ secure_url('/home.html') }}');">Home</a></li>
            <li><a href="javascript:getPage('{{ secure_url('/mainagent.html') }}');">Main Agents</a></li>
            <li>View</li>
        </ul><!-- /.breadcrumb -->      
    </div>

    <div class="row-fluid">
        <div class="btn-group">
            <a href="javascript:getContentInBox('{{ secure_url('/mainagent_view_'.$rs->id.'.html') }}');" style="font-weight: bold;text-decoration: underline;">General Profile</a>
            &nbsp;&nbsp;
            <a href="javascript:getContentInBox('{{ secure_url('/mainagent_sub_'.$rs->id.'_service.html') }}');">Services &amp; Products</a>
            &nbsp;&nbsp;
            <a href="javascript:getContentInBox('{{ secure_url('/mainagent_sub_'.$rs->id.'_agent.html') }}');">Agent List</a>
        </div>
        
        <div class="box col-md-12">
            <div class="box-inner">                
                <div class="box-content">
                    <div class="clearfix"></div>
                    <h4 class="blue">General</h4>
                    <hr/>

                    <div class="form-group col-md-3">
                        <label for="profile-type"> Profile Type </label>

                        <div class="clearfix"></div>
                        <span id="profile-type" class="form-control"></span>
                    </div>

                    <div class="form-group">
                      
                    </div>

                    <div class="form-group col-md-12">
                        <label for="description"> Description </label>

                        <span id="description" class="form-control"></span>
                    </div>

                    &nbsp;
                    <div class="clearfix"></div>
                    <h4 class="blue">Account</h4>
                    <hr/>

                    <div class="form-group col-md-6">
                        <label for="username"> User Name </label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                            <span class="form-control" id="username">{{ $rs->username }}</span>
                        </div>
                    </div>

                    <div class="form-group {{ ($rs->status == $STATUS_ACTIVE ? 'has-success' : 'has-error') }} col-md-6">
                        <label for="status"> Status </label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-flag red"></i></span>
                            <div class="form-control" id="status">{{ $rs->status }}</div>
                        </div>
                        @if ($rs->status == $STATUS_ACTIVE && $rs->active_approved_at != null)
                        <p class="help-block">
                            Verified By: {{ $rs->active_approved_by }} @ {{ @date('m/d/Y H:i', @strtotime($rs->active_approved_at)) }}
                        </p>
                        @endif
                    </div>

                    &nbsp;
                    <div class="clearfix"></div>
                    <h4 class="blue">Agent</h4>
                    <hr/>

                    <div class="form-group col-md-12">
                        <label for="uplink"> Uplink Agent </label>

                        <div class="clearfix"></div>
                        <span class="form-control">{{ ($rs->uplink != null ? $rs->uplink->username : '--') }}</span>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="paymode"> Payment Mode</label>

                        <div class="clearfix"></div>
                        <span class="form-control"> {{ $rs->paymode }}</span>                        
                    </div>
                    
                    @if ($rs->paymode == $PAYMODE_POSTPAID)
                    <div class="form-group col-md-4">
                        <label for="max-trx">Max. Transaction/day</label>
                        <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <span id="max-trx" class="form-control"></span>
                        </div>
                    </div>
                    @endif
                    
                    <div class="clearfix"></div>

                    <div class="form-group col-md-3">
                        <label for="maxterminal"> Max. Terminal Default</label>

                        <div class="clearfix"></div>
                        <span class="form-control">{{ $rs->agent_max_terminal_default }}</span>
                    </div>
                    
                    <div class="clearfix"></div>
                    
                    <div class="form-group col-md-4">
                        <label for="auto-approve"> Agents Approval </label>

                        <div class="clearfix"></div>
                        <span class="form-control"></span>
                    </div>

                    <div class="clearfix"></div>
                    <h4 class="blue">Verification</h4>
                    <hr/>

                    <div class="form-group col-md-6">
                        <label for="verif-by"> By </label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-thumbs-up"></i></span>
                            <span id="verif-by" class="form-control"></span>
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="verif-at"> Time </label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            <span id="verif-at" class="form-control">{{ @date('m/d/Y H:i', @strtotime($rs->profile->verified_at)) }}</span>
                        </div>
                    </div>
                        
                    <div class="clearfix"></div>                            
                    <hr/>

                    <div class="form-actions">
                        <a href="javascript:getPage('{{ secure_url('/mainagent.html') }}');" class="btn btn-warning btn-sm" id="button-close"><i class="glyphicon glyphicon-arrow-left"></i> Close</a>
                        &nbsp;&nbsp;                        
                        <a href="javascript:getPage('{{ secure_url('/mainagent_edit_'.$rs->id.'.html') }}');" class="btn btn-info btn-sm" id="button-edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.utils.image_upload')

@endsection
