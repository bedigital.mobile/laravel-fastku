                                <div class="clearfix"></div>
                                
                                <div class="form-group col-md-2 pull-right">
                                    <a href="#" class="btn btn-upload-image">
                                        <div class="crop center" id="avatar">
                                            <img src="img/user.png" />
                                        </div>
                                    </a>
                                    <input type="hidden" name="avatar-file" id="avatar-file" value="" />
                                </div>

                                <div class="form-group col-md-10 pull-left">
                                    <label for="fullname"> Full Name <small class="small red"><i>(Required)</i></small></label>
                                    <input type="text" id="fullname" name="fullname" placeholder="Full name" class="form-control" />
                                </div>
                                
                                <div class="form-group col-md-3 pull-left">
                                    <label for="birth-date"> Birth Date <small class="small red"><i>(Required)</i></small></label>
                                    
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                        <input type="date" id="birth-date" name="birth-date" class="form-control" />
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-4 pull-left">
                                    <label for="birth-place"> Birth Place <small class="small red"><i>(Required)</i></small></label>
                                    <input type="text" id="birth-place" name="birth-place" placeholder="Birth Place" class="form-control" />
                                </div>
                                
                                <div class="form-group col-md-3">
                                    <label for="gender"> Gender <small class="small red"><i>(Required)</i></small></label>

                                    <div class="clearfix"></div>

                                    <label class="radio-inline blue">
                                        <input name="gender" id="gender1" value="M" type="radio" class="ace" checked="checked" />
                                        <span class="lbl"> Male</span>
                                    </label>
                                    <label class="radio-inline blue">
                                        <input name="gender" id="gender2" value="F" type="radio" class="ace" />
                                        <span class="lbl"> Female</span>
                                    </label>
                                </div>
                                
                                <div class="form-group col-md-10 pull-left">
                                    <label for="corp-npwp"> NPWP <small class="small red"><i>(Required)</i></small></label>
                                    <input type="text" id="npwp" name="npwp" placeholder="NPWP" class="form-control" />
                                </div>
                                                                
                                <div class="clearfix"></div>
                                <h4 class="blue">Contact</h4>
                                <hr/>
                                
                                <div class="form-group col-md-4">
                                    <label for="email"> Email Address <small class="small red"><i>(Required)</i></small></label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Valid Email Address" value="" />
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="phone"> Phone Number <small class="small red"><i>(Required)</i></small></label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Valid Phone Number" value="" />
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="fax"> Fax Number </label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-print"></i></span>
                                        <input type="text" class="form-control" id="fax" name="fax" placeholder="Valid Fax. Number" value="" />
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label for="address"> Address <small class="small red"><i>(Required)</i></small></label>

                                    <textarea class="form-control" id="address" name="address"></textarea>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="village"> Village Name </label>

                                    <input type="text" class="form-control" id="village" name="village" placeholder="Name of village" value="" />
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="district"> District Name <small class="small red"><i>(Required)</i></small></label>

                                    <input type="text" class="form-control" id="district" name="district" placeholder="District Name" value="" />
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="city"> City <small class="small red"><i>(Required)</i></small></label>

                                    <input type="text" class="form-control" id="city" name="city" placeholder="City Name" value="" />
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="province"> Province <small class="small red"><i>(Required)</i></small></label>

                                    <input type="text" class="form-control" id="province" name="province" placeholder="Province Name" value="" />
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="postal"> Postal Code <small class="small red"><i>(Required)</i></small></label>

                                    <input type="text" class="form-control" id="postal" maxlength="5" name="postal" placeholder="Postal Code" value="" />
                                </div>

