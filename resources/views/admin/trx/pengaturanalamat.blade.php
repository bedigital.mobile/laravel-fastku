@extends('admin.trx.layout') 
@section('style') 
<link rel="stylesheet" href="https://npmcdn.com/leaflet@0.7.7/dist/leaflet.css" />

<style type="text/css">
    main.c-main {
    padding: 15px 0 0 0;
}
 #mapalamat {
        width:100%; height:350px; margin:0;
    }
</style>
@endsection
@section('content') 
               
                <div class="content">
                    <!-- Block Tabs -->
                    <h2 class="content-heading">Pengaturan Akun</h2>
                    
                    <div class="row">
                        <div class="col-lg-12">


                            <!-- Block Tabs Alternative Style -->
                            <div class="block block-rounded">
                                <ul class="nav nav-tabs nav-tabs-alt" >
                                    <li class="nav-item">
                                        <a class="nav-link " href="/fastku/pengaturan#btabs-alt-static-home">Akun</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="/fastku/pengaturan#btabs-alt-static-profile">Email & No.Hp</a>
                                    </li> <li class="nav-item">
                                        <a class="nav-link" href="/fastku/pengaturan#btabs-alt-static-sandi">Kata Sandi</a>
                                    </li> <li class="nav-item">
                                        <a class="nav-link active" href="#btabs-alt-static-alamat">Alamat</a>
                                    </li><!-- 
                                    <li class="nav-item ml-auto">
                                        <a class="nav-link" href="#btabs-alt-static-settings"><i class="si si-settings"></i></a>
                                    </li> -->
                                </ul>
                            <br>


<div class="row form-group" id="frmupdatealamat">
    <div class="col-sm-12 col-xl-12">
        <div class="card">
            <header class="card-header">Form Alamat</header>
            <div class="card-body">
                <div class="form-group">
                    <label class="" for="email">Nama Alamat</label>
                    <input class="form-control" type="text" id="namaalamat" placeholder="Nama Alamat">
                    </div>
                    <div class="form-group">
                        <label class="" for="textarea-input">Alamat</label>
                        <textarea class="form-control" name="textarea-input" id="textarea-input" rows="3" placeholder="Content..."></textarea>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            <label class="" for="selectkota">Provinsi</label>                            
                            <div id="load_prov" style="display: none;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>

                             <select class="custom-select" name="selectprov" id="selectprov">
                                <option value ="">Pilih Provinsi</option>
                                
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="" for="ccyear">Kota</label>                            
                            <div id="load_city" style="display: none;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
                            <select class="custom-select" name="selectcity" id="selectcity">
                                <option value="">Pilih Kota</option>
                            </select>
                        </div>
                    </div>
					<div class="row form-group">
                        <div class="col-md-6">
                            <label class="" for="ccyear">Kecamatan</label>
                            <div id="load_kec" style="display: none;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>

                            <select class="custom-select" name="selectkec" id="selectkec">
                               
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="" for="ccyear">Kelurahan</label>
                            <div id="load_kel" style="display: none;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
                            <select class="custom-select" name="selectkel" id="selectkel">
                               
                            </select>
                        </div>

                    </div>
                    <div class="row form-group">
                    	 <div class="col-md-6">
                            <label class="" for="ccyear">Kode pos</label>
                            <input class="form-control" type="text" id="kodepos" placeholder="Kode Pos">
                        </div>
                    </div>
                        <div class="row form-group">
                            <label class="" for="textarea-input">Peta</label> 
                               <div id="mapalamat"></div>
                               <div class='pointer'><< Click search button</div>
                                    <div class="form-group">
                                        <label class="" for="email">Alamat by Map</label>
                                        <input class="form-control" type="text" id="address" placeholder="Nama Alamat" value="Alamat Kordinat Peta">
                                            <input type="text" id="latitude" placeholder="LatMap" value="">
                                                <input type="text" id="longitude" placeholder="LgtMap" value="">
                                                    <input type="text" id="postcode" placeholder="PostCode" value="">
                                                    <input type="text" id="city" placeholder="City" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    </div>
                            <!-- END Block Tabs Alternative Style -->
                        </div>
                        
                    </div>
                    <!-- END Block Tabs -->

                  
                           
                    </div> 
<script src="assets/js/core/jquery.min.js"></script>
<!--<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/openlayers/2.11/lib/OpenLayers.js"></script>!-->
<script src="https://npmcdn.com/leaflet@0.7.7/dist/leaflet.js"></script>
<script src="https://cdn.jsdelivr.net/npm/leaflet.locatecontrol@0.71.1/src/L.Control.Locate.js"></script>


<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.css" />
<!-- <script src="http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.js"></script> -->
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet/0.0.1-beta.5/esri-leaflet.js"></script>
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.css">





<script language="javascript">
   	  /* 
   	 $( window ).on( "load", function() {
   	 	init();
        console.log( "window loaded" );
        getLocationLeaflet();
    });
    var map;
      var popup = L.popup();
         
      function init() {
                       
         map = new L.Map('map');
         popup = new L.Popup();
         
         L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
            maxZoom: 18
         }).addTo(map);
         map.attributionControl.setPrefix(''); // Don't show the 'Powered by Leaflet' text.

         var london = new L.LatLng(51.505, -0.09); 
         map.setView(london, 13);
         
         
         map.on('click', onMapClick);
      }
      
      //Listener function taking an event object 
      function onMapClick(e) {
         //map click event object (e) has latlng property which is a location at which the click occured.
         popup
           .setLatLng(e.latlng)
           .setContent("You clicked the map at " + e.latlng.toString())
           .openOn(map);
      }
       
 
   map = new OpenLayers.Map("mapalamat");
    map.addLayer(new OpenLayers.Layer.OSM());

    var lonLat = new OpenLayers.LonLat( -0.1279688 ,51.5077286 )
          .transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
            map.getProjectionObject() // to Spherical Mercator Projection
          );
          
    var zoom=16;

    var markers = new OpenLayers.Layer.Markers( "Markers" );
    map.addLayer(markers);
    
    markers.addMarker(new OpenLayers.Marker(lonLat));
    
    map.setCenter (lonLat, zoom); 
 
    //leaflet
     var map;

      function init() {
         map = new L.Map('mapalamat');
         popup = new L.Popup();

         L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
            maxZoom: 18
         }).addTo(map);
         map.attributionControl.setPrefix(''); // Don't show the 'Powered by Leaflet' text.

         // map view before we get the location
         map.setView(new L.LatLng(51.505, -0.09), 13);

         map.on('click', onMapClick);
      }

      function onLocationFound(e) {
         var radius = e.accuracy / 2;
         var location = e.latlng
         L.marker(location).addTo(map)
         L.circle(location, radius).addTo(map);


      }

      function onLocationError(e) {
         alert(e.message);
      }

      function getLocationLeaflet() {
         map.on('locationfound', onLocationFound);
         map.on('locationerror', onLocationError);

         map.locate({setView: true, maxZoom: 16});
      }
       function onMapClick(e) {
       	         let address = $('#address');
       	             console.log("onclick map");
         //map click event object (e) has latlng property which is a location at which the click occured.
         popup
           .setLatLng(e.latlng)
           .setContent("You clicked the map at " + e.latlng.toString())
           .openOn(map);
            console.log("var latitude "+e.latlng.lat);
            console.log("var longitude "+e.latlng.lng);
            var location = e.latlng
            var radius = e.accuracy / 2;
            var lat = e.latlng.lat;
            var lng = e.latlng.lng;
         	L.marker(location).addTo(map)
        	L.circle(location, radius).addTo(map);


    	

             let getAddress = function(lat, lng) {
            let url = `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${lat}&lon=${lng}`;
            address.val(captionTakeAddress);
            setTimeout(() => {
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'json',
                    beforeSend: function(request) {
                        request.setRequestHeader('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36');
                    },
                    success: function(res) {
                        if (res.error == null && res.display_name != null) {
                            address.val(res.display_name);
                        } else {
                            address.val(null);
                        }
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                        address.val(null);
                    }
                });
            }, 1000);
        }


      }

  */


    let onLocationCallback = {
        onGetLocation: null
    }
    let onGetCurrentLocation = function(lat, lng) {
        if (onLocationCallback.onGetLocation != null) onLocationCallback.onGetLocation(lat, lng);
    }
    $(document).ready(function() {
    	console.log("begin leaflet maps");
        let mapCenter = [-0.789275, 113.9213257];
        let map = L.map('mapalamat', {
            center: mapCenter,
            zoom: 3
        });


        let currentLocMarker = L.marker(mapCenter).addTo(map);
        let name = $('#name');
        let latitude = $('#latitude');
        let longitude = $('#longitude');
        let address = $('#address');
        let postcode = $('#postcode');
        let city = $('#city');

        let selectprov = $('#selectprov');
        let load_prov = $('#load_prov');
        let selectcity = $('#selectcity');
        let load_city = $('#load_city');
        let selectkec = $('#selectkec');
        let load_kec = $('#load_kec');
        let selectkel = $('#selectkel');
        let load_kel = $('#load_kel');
        
        let loadCity = $('#load_city');
        let div_city = $('#div_city');

        var loadKec=$('#load_kec');
        var divKec=$('#div_kec');
        var kec=$('#kec');
        // loadKec.hide();
        getprovince();
        selectprov.change(function(){
            getCitys($(this).val());
        });
        selectcity.change(function(){
            getKec($(this).val());
        });  
        selectkec.change(function(){
            getKel($(this).val());
        });
        function getprovince()
        {        	
        	load_prov.show();
        	selectprov.hide();
            var url = `https://fintech-stg.bedigital.co.id/elog/graph/geoname?query={provincies{id,name}}`;
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'GET',
                    async: true,
                    contentType: "application/json",
                    success: function(res) {
                        let opt = `<option value="">Pilih Provinsi</option>`;    
                            res.data.provincies.forEach(function(i) {
                                opt += `<option value="${i.id}">${i.name}</option>`;

                            });
                            selectprov.html(opt);
                            selectprov.show();
                            load_prov.hide();
                     
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    }
                });
            }, 1000);

        }
        function getCitys(idProv) {
            console.log("get city , id="+idProv);
 			load_city.show();
        	selectcity.hide();
        	var url = `https://fintech-stg.bedigital.co.id/elog/graph/geoname?query={cities(province_id:`+idProv+`){id,name}}`;
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'GET',
                    async: true,
                     contentType: "application/json",
                    success: function(res) {
                    	console.log("get city success , data ="+res);
                        let opt = `<option value="">Pilih Kota</option>`;
                         res.data.cities.forEach(function(i) {
                                opt += `<option value="${i.id}">${i.name}</option>`;

                            });
                            selectcity.html(opt);
                            selectcity.show();
                            load_city.hide();
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    }
                });
            }, 1000);
        }
         function getKec(idCity) {
            console.log("get kec , id="+idCity);
 			load_kec.show();
        	selectkec.hide();
        	var url = `https://fintech-stg.bedigital.co.id/elog/graph/geoname?query={subdistricts(city_id:`+idCity+`){id,name}}`;
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'GET',
                    async: true,
                     contentType: "application/json",
                    success: function(res) {
                    	console.log("get city success , data ="+res);
                        let opt = `<option value="">Pilih Kecamatan</option>`;
                         res.data.subdistricts.forEach(function(i) {
                                opt += `<option value="${i.id}">${i.name}</option>`;

                            });
                            selectkec.html(opt);
                            selectkec.show();
                            load_kec.hide();
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    }
                });
            }, 1000);
        }
 		
 		function getKel(idKec) {
            console.log("get kec , id="+idKec);
 			load_kel.show();
        	selectkel.hide();
        	var url = `https://fintech-stg.bedigital.co.id/elog/graph/geoname?query={villages(subdistrict_id:`+idKec+`){id,name}}`;
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'GET',
                    async: true,
                     contentType: "application/json",
                    success: function(res) {
                    	console.log("get city success , data ="+res);
                        let opt = `<option value="">Pilih Kelurahan</option>`;
                         res.data.villages.forEach(function(i) {
                                opt += `<option value="${i.id}">${i.name}</option>`;

                            });
                            selectkel.html(opt);
                            selectkel.show();
                            load_kel.hide();
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    }
                });
            }, 1000);
        }
        function getbycordinate(lat,lng) {
            console.log("get kec , id="+idKec);
 			load_kel.show();
        	selectkel.hide();        	
        	var url = `?query={coordinate(latitude:`+lat+`,longitude:`+lng+`){province_id,province_name,city_id,city_name,subdistrict_id,subdistrict_name,village_id,village_name,postal_code,address_suggestion,line_address}}`;
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'GET',
                    async: true,
                     contentType: "application/json",
                    success: function(res) {
                    	console.log("get city success , data ="+res);
                        let opt = `<option value="">Pilih Kelurahan</option>`;
                         res.data.villages.forEach(function(i) {
                                opt += `<option value="${i.id}">${i.name}</option>`;

                            });
                            selectkel.html(opt);
                            selectkel.show();
                            load_kel.hide();
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    }
                });
            }, 1000);
        }

        let captionTakeAddress = "Sedang mengambil alamat...";

        loadCity.hide();
        let getAddress = function(lat, lng) {
            let url = `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${lat}&lon=${lng}`;
            address.val(captionTakeAddress);
            setTimeout(() => {
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'json',
                    beforeSend: function(request) {
                        request.setRequestHeader('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36');
                    },
                    success: function(res) {
                    	console.log("info alamat="+JSON.stringify(res))
                        if (res.error == null && res.display_name != null) {
                            address.val(res.display_name);
                            postcode.val(res.address.postcode);
                            city.val(res.address.city_district);
                            
                        } else {
                            address.val(null);
                        }
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                        address.val(null);
                    }
                });
            }, 1000);
        }
        let updateCurrentLoc = function(lat, lng, withAddress = true) {
            currentLocMarker.setLatLng([lat, lng]).bindPopup('Lokasi Anda');
            map.setView([lat, lng], 18);
            latitude.val(lat);
            longitude.val(lng);
            if (withAddress) getAddress(lat, lng);
        };


        
        let doSubmit = function() { 
            var fd = new FormData();
            // fd.append('file', $('#file')[0].files[0]);
            fd.append('latitude', latitude.val());
            fd.append('longitude', longitude.val());
            fd.append('address', address.val());
            fd.append('phone', '62' + phone.val());
            fd.append('name', name.val());
            fd.append('buka', $("#buka").val());
            fd.append('tutup', $("#tutup").val());
            fd.append('province', $("#province").val());
            fd.append('city', city.val());
            fd.append('fullname', $("#fullname").val());
            fd.append('canvaser', $("#canvaser").val());
            fd.append('referal_code', $("#referal_code").val());
            fd.append('kec',$('#kec').val());
            fd.append('jenis_usaha',$('#jenis_usaha').val());
            
            // fd.append('nik', $("#nik").val());
            // fd.append('tgl_lahir', $("#tgl_lahir").val());
            if (address.val() == captionTakeAddress){
                alert("Alamat belum dilengkapi!")
            }else{   
                $('.loader').show();
                $.ajax({
                    url: site_url + '/auth/post_daftar',
                    type: 'POST',
                    data: fd,
                    processData: false,
                    contentType: false,
                    cache: false,
                    async: true,
                    dataType: 'json',
                    success: function(data, text) {
                        if (data.success) {
                            // alert('Terima kasih, data Anda berhasil terkirim. Akun anda akan di tinjau untuk kemudian di verifikasi kurang lebih dalam 1x24 jam kerja!');
                            alert('Terima kasih, data Anda berhasil terkirim & kami telah mengirim link verifikasi ke akun whatsapp anda. Segera lakukan verifikasi sebelum waktu verifikasi habis!');
                            window.location.href = 'fastku.co.id';
                        } else {
                            alert(data.message != null ? data.message : "Gagal, Silahkan coba lagi!");
                            $('.loader').css('display', 'none');
                        }
                    },
                    error: function(stat, res, err) {
                        alert("Gagal, Silahkan coba lagi!");
                        $('.loader').css('display', 'none');
                    }
                });
            }
        }

        $("#province").change(function() {
            getCity($(this).val());
            getProvince(null);
            let lat = $("#province :selected").attr('data-lat');
            let lng = $("#province :selected").attr('data-lng');
            if (lat == null && lng == null) {
                updateCurrentLoc(mapCenter[0], mapCenter[1], 4);
            } else {
                updateCurrentLoc(parseFloat(lat), parseFloat(lng), 9);
            }
        });


        let getCity = function(prov, isAsync = true) {
            loadCity.show();
            div_city.hide();
            let url = `fastku.co.id`;
            setTimeout(() => {
                $.ajax({
                    url: url,
                    type: 'POST',
                    async: isAsync,
                    data: {
                        prov: prov
                    },
                    dataType: 'json',
                    success: function(res) {
                        let opt = `<option value="">Pilih Kabupaten/Kota</option>`;
                        res.forEach(function(i) {
                            opt += `<option value="${i.tlc}">${i.kota}</option>`;
                        });
                        city.html(opt);
                        loadCity.hide();
                        div_city.show();
                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    }
                });
            }, 1000);
        }

        // $.validate({
        //     form: '#form',
        //     modules: 'location, date, security, file',
        //     onModulesLoaded: function() {},
        //     onError: function($form) {},
        //     onSuccess: function($form) {
        //         event.preventDefault();
        //         doSubmit($form);
        //         return false;
        //     }
        // });
//=======================================================
//map
//=======================================================
        
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            noWrap: true
        }).addTo(map);
        map.on('click', function(e) {
          console.log(e)
            updateCurrentLoc(e.latlng.lat, e.latlng.lng);
        });


L.control.scale().addTo(map);


  var searchControl = new L.esri.Controls.Geosearch().addTo(map);

  var results = new L.LayerGroup().addTo(map);

  searchControl.on('results', function(data){
    results.clearLayers();
    for (var i = data.results.length - 1; i >= 0; i--) {
      results.addLayer(L.marker(data.results[i].latlng));
      console.log(data.results[i].latlng)
      updateCurrentLoc(data.results[i].latlng.lat, data.results[i].latlng.lng);
    }
  });

setTimeout(function(){$('.pointer').fadeOut('slow');},3400);






        // updateCurrentLoc(mapCenter[0],mapCenter[1]); 

        //Get user data

        $('#mobile-loc').hide();
        if (typeof(RynxJSAndroidBridge) != 'undefined') {
            $('#mobile-loc').show();
            $('#mobile-loc-btn').click(function() {
                RynxJSAndroidBridge.getCurrentLocation();
                onLocationCallback.onGetLocation = function(lat, lng) {
                    updateCurrentLoc(lat, lng);
                }
            });
        } else {
            function onLocationFound(e) {
                var radius = e.accuracy;
                updateCurrentLoc(e.latlng.lat, e.latlng.lng);
            }

            function onLocationError(e) {
                alert(e.message);
            }

            map.on('locationerror', onLocationError);
            map.on('locationfound', onLocationFound);

            var lc = L.control.locate({
                strings: {
                    title: "Dapatkan Lokasi Terkini"
                },
                initialZoomLevel: 10,
                locateOptions: {
                    enableHighAccuracy: true
                }
            }).addTo(map);
        }
        $('#phone').on('change keyup', function() {
            var sanitized = $(this).val().replace(/[^0-9]/g, '');
            $(this).val(sanitized);
        });
    });

/*

    $.ajax({
  url: "https://nominatim.openstreetmap.org/reverse",
  data: {
    lat: 38.748666,
    lon: -9.103002,
    format: "json"
  },
  beforeSend: function (xhr) {
    xhr.setRequestHeader(
      'User-Agent',
      'ID of your APP/service/website/etc. v0.1'
    )
  },
  dataType: "json",
  type: "GET",
  async: true,
  crossDomain: true
}).done(function (res) {
  console.log(res.address)
}).fail(function (error) {
  console.error(error)
})
*/
  </script>

@endsection