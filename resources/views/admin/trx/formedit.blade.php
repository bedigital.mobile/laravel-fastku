<div class="" role="main">
    <div class="">
    <div class="page-title">
        <div class="title_left">
        <h3>Update Master Agent</h3>
        <br>
        </div>
        <div class="title_right"></div>
    </div>
    <div class="clearfix"></div> 
    <div id="msg"></div>    
<!--<form id="frmMainAgent" data-parsley-validate class="form-horizontal form-label-left">!-->
<form role="form" id="frm" name="frm" onsubmit="doEdit('{{ $rs->ma_id }}');return false;"> 
  @csrf
                        <input type="hidden" name="id" value="{{ $rs->ma_id }}" />

                       
<div class="row"> 
<div class="col-md-6 col-xs-12">       

    <div class="x_panel">
        <div class="x_title">
            <h2>General <small></small></h2>
            <ul class="nav navbar-right panel_toolbox"></ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content"> 
            <div class="form-group col-md-12">
            <label class="control-label">Profile Type </label>
                <div class="">
                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                <input type="radio" name="profile-type" value="CORPORATE" checked> &nbsp;  Corporate &nbsp;
                </label>
                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                <input type="radio" name="profile-type" value="PERSONAL">  Personal
                </label>  
                </div>
            </div>  

            <div id="view-personal">
                <div class="clearfix"></div>

                <div class="form-group col-md-12">
                <label for="fullname"> Full Name <small class="small red"><i>*</i></small></label>
                <input type="text" id="fullname" name="fullname" placeholder="Full name" class="form-control" value="{{ isset($rs->fullname) ? $rs->fullname : '' }}"    required/>
                </div>

                <div class="form-group col-md-6">
                <label for="birth-date"> Birth Date <small class="small red"><i>*</i></small></label>

                <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                <input type="date" id="birth-date" name="birth-date" class="form-control" value="{{ isset($rs->birth_date) ? $rs->birth_date : '' }}"  required/>
                </div>
                </div>

                <div class="form-group col-md-6">
                <label for="birth-place"> Birth Place <small class="small red"><i>*</i></small></label>
                <input type="text" id="birth-place" name="birth-place"  value="{{ isset($rs->place_birth) ? $rs->place_birth : '' }}" placeholder="Birth Place" class="form-control"  required/>
                </div>

                <div class="form-group col-md-12">
                <label for="gender"> Gender <small class="small red"><i>*</i></small></label>

                <div class="clearfix"></div>

                <label class="radio-inline blue">
                <input name="gender" id="gender1" value="M" type="radio" class="ace" checked="checked" />
                <span class="lbl"> Male</span>
                </label>
                <label class="radio-inline blue">
                <input name="gender" id="gender2" value="F" type="radio" class="ace" />
                <span class="lbl"> Female</span>
                </label>
                </div> 
            </div>

            <div id="view-corporate">
                <div class="clearfix"></div>
                <div class="form-group col-md-12">
                <label for="company-name"> Company Name <small class="small red"><i>*</i></small></label>
                <input type="text" id="company-name" name="company-name" placeholder="Company name" class="form-control"  value="{{ isset($rs->company_name) ? $rs->company_name : '' }}" required/>
                </div>
                <div class="form-group col-md-6">
                <label for="department"> Department <small class="small red"><i>*</i></small></label>
                <input type="text" id="department" name="department" placeholder="Department name" class="form-control" value="{{ isset($rs->department) ? $rs->department : '' }}"   required/>
                </div>
                <div class="form-group col-md-6">
                <label for="branch-office"> Branch Office <small class="small red"><i>*</i></small></label>
                <input type="text" id="branch-office" name="branch-office" placeholder="Branch Office Level" value="{{ isset($rs->branch_office_level) ? $rs->branch_office_level : '' }}"    class="form-control" required/>
                </div>
                 
            </div>
                <div class="form-group col-md-12">
                <label for="corp-npwp"> NPWP <small class="small red"><i>*</i></small></label> 
                <input type="text" id="corp-npwp" value="{{ isset($rs->npwp_number) ? $rs->npwp_number : '' }}"   name="corp-npwp" placeholder="NPWP" class="form-control" required/>
                </div>
        </div>
    </div><!-- end general!--> 

<div class="x_panel">
    <div class="x_title">
        <h2>Account <small></small></h2>                   
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="form-group ">
        <label for="username"> User Name  <small class="small red"><i>*</i></small></label>
            <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
            <input type="text" id="username" name="username" value="{{ isset($rs->username) ? $rs->username : '' }}" placeholder="Agent User Name" class="form-control" disabled="" />
            </div>
        </div>
      
     
    </div>
</div><!-- end account!-->


<div class="x_panel">
    <div class="x_title">
        <h2>Agent <small></small></h2>                   
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
<!--
    <div class="form-group">
    <label for="uplink"> Upline Main Agent </label>

    <div class="clearfix"></div>

    <select class="form-control" id="uplink" name="uplink" data-rel="chosen">
    <option value="0"> --- </option>
    @foreach ($uplinks as $up)
    <option value="{{ $up->agent_id }}">{{ ($up->fullname != '' ? $up->fullname : ($up->company_name != '' ? $up->company_name : $up->username)) }}</option>
    @endforeach
    </select>
    </div>
!-->
    <div class="form-group">
    <label for="paymode"> Payment Mode  <small class="small red"><i>*</i></small></label>

 
     <div class="input-group">
        <label>
        <input name="paymode" id="paymode1" value="PREPAID" type="radio" checked="checked" />
        <span class="lbl"> Prepaid</span>
        </label><br>
 
        <label> 
        <input name="paymode" id="paymode2" value="POSTPAID" type="radio" />
        <span class="lbl"> Postpaid</span>
        </label>
      </div> 
      <div id="maxtrx"> 
        <label>Max. Transaction/day</label>
            <div class="input-group col-md-6">
                <span class="input-group-addon">Rp.</span>
                <input type="text" id="max-trx" name="max-trx" maxlength="11" placeholder="Max. Transactions per day" value="{{ isset($rs->postpaid_balance_value_max) ? $rs->postpaid_balance_value_max : '' }}"  class="form-control" />
            </div>
    </div>
    </div>

    <div class="form-group">
    <label for="maxterminal"> Max. Terminal Default <small class="small red"><i>*</i></small></label>
    <div class="input-group col-md-3"><input type="text" id="maxterminal" name="maxterminal" placeholder="Maximum number of Terminal" class="form-control" value="{{ isset($rs->agent_max_terminal_default) ? $rs->agent_max_terminal_default : '1' }}"  /></div>
    </div> 

    <div class="form-group col-md-12">
    <label for="auto-approve"> Agents Approval</label>
       <div class="controls">
            <label class="checkbox blue">
            <input name="auto-approve" id="auto-approve" value="1" type="checkbox" />
            <span class="lbl"> Auto Approval </span>
            </label>                                
        </div>
    </div> 
    </div>
</div> <!-- end trx type!-->



 
</div>

   
<div class="col-md-6 col-xs-12">
    <div class="x_panel"><!-- contact!-->
        <div class="x_title">
        <h2>Contact <small></small></h2>                   
        <div class="clearfix"></div>
        </div>
    <div class="x_content">

        <div class="form-group col-md-6" id="cp-corporate">
        <label for="contact"> Contact Person <small class="small red"><i>*</i></small></label>
        <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        <input type="text" id="contact" name="contact" placeholder="Contact person's name" class="form-control" value="{{ isset($rs->contact_person) ? $rs->contact_person : '' }}"  required/>
        </div>
        </div>
        <div class="form-group col-md-6">
        <label for="corp-email"> Email Address <small class="small red"><i>*</i></small></label>
            <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
            <input type="email"  value="{{ isset($rs->email) ? $rs->email : '' }}"  class="form-control" id="corp-email" name="corp-email" placeholder="Valid Email Address" value="" required/>
            </div>
        </div>

        <div class="form-group col-md-6">
        <label for="corp-phone"> Phone Number <small class="small red"><i>*</i></small></label>
            <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
            <input type="text"  value="{{ isset($rs->phone_number) ? $rs->phone_number : '' }}"  class="form-control" id="corp-phone" name="corp-phone" placeholder="Valid Phone Number" value="" required/>
            </div>
        </div>
        <div class="form-group col-md-6">
        <label for="corp-fax"> Fax Number </label>
            <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-print"></i></span>
            <input type="text" value="{{ isset($rs->fax_number) ? $rs->fax_number : '' }}"  class="form-control" id="corp-fax" name="corp-fax" placeholder="Valid Fax. Number" value="" required/>
            </div>
        </div>
        <div class="form-group col-md-12">
        <label for="corp-address"> Address <small class="small red"><i>*</i></small></label>
        <textarea class="form-control" id="corp-address" name="corp-address"> {{ isset($rs->address) ? $rs->address : '' }} </textarea>
        </div>
        <div class="form-group col-md-4">
        <label for="corp-village"> Village Name </label>
        <input type="text" value="{{ isset($rs->village_name) ? $rs->village_name : '' }}"  class="form-control" id="corp-village" name="corp-village" placeholder="Name of village" value="" required/>
        </div>
        <div class="form-group col-md-4">
        <label for="corp-district"> District Name <small class="small red"><i>*</i></small></label>
        <input type="text" value="{{ isset($rs->district_name) ? $rs->district_name : '' }}"  class="form-control" id="corp-district" name="corp-district" placeholder="District Name" value="" required/>
        </div>
        <div class="form-group col-md-4">
            <label for="corp-city"> City <small class="small red"><i>*</i></small></label>
            <input type="text" value="{{ isset($rs->city_name) ? $rs->city_name : '' }}"  class="form-control" id="corp-city" name="corp-city" placeholder="City Name" value="" required/>
        </div>
        <div class="form-group col-md-4">
            <label for="corp-province"> Province <small class="small red"><i>*</i></small></label>
            <input type="text" value="{{ isset($rs->province_name) ? $rs->province_name : '' }}"  class="form-control" id="corp-province" name="corp-province" placeholder="Province Name" value="" required/>
        </div>
        <div class="form-group col-md-4">
            <label for="corp-postal"> Postal Code <small class="small red"><i>*</i></small></label>
            <input type="text" value="{{ isset($rs->postal_code) ? $rs->postal_code : '' }}"  class="form-control" id="corp-postal" maxlength="5" name="corp-postal" placeholder="Postal Code" value="" required/>
        </div>
        <div class="form-group col-md-12">
            <label for="description"> Description </label>
            <textarea id="description" name="description" class="form-control"> {{ isset($rs->description) ? $rs->description : '' }} </textarea>
        </div>  
    </div>
    </div><!-- end contact!-->

      <div class="x_panel">
        <div class="x_title">
        <h2>Balance <small></small></h2>                   
        <div class="clearfix"></div>
        </div>
        <div class="x_content">

        <div class="form-group col-md-6" id="balance-type">
            <label for="balance-type"> Balance Type <small class="small red"><i>*</i></small></label>
            <div class="clearfix"></div>
            <select class="form-control" name="balance-type" id="balance-type" data-rel="chosen">
            @foreach ($rsBalType as $bal)
            <option value="{{ $bal->id }}">{{ $bal->balance_name }}</option>
            @endforeach
            </select>
        </div>

        <div class="form-group col-md-6" id="balance-val">
        <label for="balance"> Balance Amount <small class="small red"><i>*</i></small></label>
            <div class="input-group">
            <span class="input-group-addon">Rp.</span>
            <input type="text" class="form-control" name="balance" id="balance" placeholder="Amount of balance"  value="{{ isset($rs->balance_value) ? $rs->balance_value : '' }}"  required/>
            </div>
        </div> 

    </div>
    </div> <!-- end Balance -->



<div class="x_panel">
    <div class="x_title">
        <h2>Downline<small></small></h2>                   
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
         <div class="form-group">
        <label for="uplink"> Max Downline Main Agent </label>

        <div class="clearfix"></div>

        <select class="form-control" id="dline" name="dline" data-rel="chosen"> 
        <option value="{{ isset($rs->max_downline) ? $rs->max_downline : '0' }}"> {{ isset($rs->max_downline) ? $rs->max_downline : '---' }} </option>
        @for ($i = 1; $i < 10; $i++)
                     <option value="{{ $i }}">{{ $i }}</option>
        @endfor
         </select>
        </div>


    </div>
</div><!-- end downline !-->



</div><!-- end right -->


</div><!-- end row!-->

 

<div class="row">
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button type="button" id="canceledit"  onclick="javascript:getView('/admin/listmainagent.html');"  class="btn btn-primary">Cancel</button>
        <button type="submit" class="btn btn-success">Submit</button>
        </div>
    </div>
</div> 
 
 
</form> 

</div>
</div>

<script type="text/javascript"> 
    

        $('#maxtrx').hide();
        $('#view-personal').hide();
       hidepersonal();

           $('#canceledit').on('click', function() {
            $('#divdetailmainagent').removeClass('hideform');
           });

           $('input[name=paymode]').on('click', function() {
            if ($(this).val() === '{{ $PAYMODE_POSTPAID }}') {
                $('#maxtrx').show();
                $("#frm #max-trx").attr('required', 'true');
            } else {
                $('#maxtrx').hide();
                $("#frm #max-trx").removeAttr("required");
            }
        });



 

function hidepersonal()
{

                $("#view-personal :input").removeAttr("required");
                 $("#frm #view-corporate :input").attr('required', 'true');
                 $("#frm #company-name ").attr('required', 'true');

                 $("#cp-corporate :input").attr('required', 'true');

                $('#view-corporate').show(); 
                $('#cp-corporate').show();
                $('#view-personal').hide();
}


        $('input[name=profile-type]').on('click', function() { 
            if ($(this).val() === '{{ $PROFILE_TYPE_CORPORATE }}') {
                hidepersonal();
            } else {
                $("#view-corporate :input").removeAttr("required");
                $("#cp-corporate :input").removeAttr("required");
                $("#view-personal :input").attr('required', 'true');
                $('#view-corporate').hide();
                $('#cp-corporate').hide();
                $('#view-personal').show();
            }
        });



    $("#frmMainAgent").on('submit',function(e){
            e.preventDefault();
            bootbox.confirm("Are you sure want to save?", function(result) {
            if(result) {   
                   //  alert("test"); 
                    //ajax code here frmEditProfile
                   var form_data = $(this).serialize();
                   console.log(form_data);  
                   
                    $.ajax({
                            type: "PUT",  
                            headers: {
                                'X-CSRF-TOKEN': $('input[name=_token]').val()
                            },
                            url: "{{ secure_url('/mainagent_new.html') }}",           
                            data : form_data,//only input   
                            beforeSubmit: function() {
                            serviceBuild();
                            productBuild();
                            beforeSubmit();
                            },      
                            success: function(msg) {
                                  console.log(msg); 
                                
                            },
                            error: function(jqXHR, textStatus, errorThrown) 
                            {
                              alert("Error: "+errorThrown+" , Please try again");   
                            }
                        }).done(function( response) {
                            alert(response); console.log(response);  
                            //$('#modal-form-profile').modal('hide');
                              //  reloadpage( '{{ url('app/customer/profile/view') }}' ,'PROFILE');});
                             /*   */
                }); 
             }
        }); 
           
    }); 
  
    function beforeSubmit() {
        $('#button-close').attr('disabled', 'disabled');

        $('#button-save').attr('disabled', 'disabled');
        $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Saving...');
    }

    function afterSubmit() {
        $('#button-close').removeAttr('disabled');

        $('#button-save').removeAttr('disabled');
        $('#button-save').html('<i class="glyphicon glyphicon-save"></i> Save');
    }



    function doEdit(id) {        
        sendForm3({
            method: 'POST',
            form: 'frm',
            messageView: 'msg',            
            messageSuccess: 'Update Master Agent Saved',
            url: '{{ secure_url('/mainagent_edit.html') }}',
            redirect:  '/admin/detailmainagent/'+id+'.html',
            beforeSubmit: function () {
                serviceBuild();
                productBuild(); 
                beforeSubmit();
            },
            afterSubmit: afterSubmit
        });
    }

///admin/detailmainagent/1537531119.html
 

    var services = [];
    var products = [];
    
    function checkService(cb) {
        if ($(cb).is(':checked')) {
            var tmp = [];
            for (var i=0; i<this.services.length; i++) {
                if (this.services[i] != $(cb).val()) {
                    var ln = tmp.length;
                    tmp[ln] = this.services[i];
                }
            }
            this.services = tmp;
        } else {
            var ln = this.services.length;
            this.services[ln] = $(cb).val();
        }
        console.log(this.services);
        $("input[id^='" + $(cb).val() + "product']").each(function (i, el) {
            $(el).prop('checked', $(cb).is(':checked'));
            checkProduct(el);
        });
    }
    
    function serviceBuild() {
        var srv = this.services.length > 0 ? this.services.join(',') : '';
        $('#service-list').val(srv);
    }
    
    function checkProduct(cb) {
        if ($(cb).is(':checked')) {
            var tmp = [];
            for (var i=0; i<this.products.length; i++) {
                if (this.products[i] != $(cb).val()) {
                    var ln = tmp.length;
                    tmp[ln] = this.products[i];
                }
            }
            this.products = tmp;
        } else {
            var ln = this.products.length;
            this.products[ln] = $(cb).val();
        }
//        console.log(this.products);
    }
    
    function productBuild() {
        var prod = this.products.length > 0 ? this.products.join(',') : '';
        $('#product-list').val(prod);
    }
    
</script>



