
 <div role="main">
          <div class="">
             
<div id="formupdtSrv" class="hideform"></div> 
<div id="tablemainagentSrv">         
     <div class="row">
      <h2>Master Agent Commision</h2>
          <ul class="nav navbar-right panel_toolbox">
              <li><!--<button id="updatesrv" onclick="showSrvForm('{{ $mainId }}');" class="btn btn-sm btn-success btn-block" type="button">Edit Commision</button>!-->
              </li> 
          </ul>
            <div class="box-inner">                
                <div class="box-content"> 
                     @foreach ($services as $srv)
                    <?php $no=1; ?>  
                    <div class="form-group col-md-12">
                        <label for="{{ $srv['service'][0] }}">{{ $srv['service'][1] }}</label>

                        <div class="clearfix"></div>
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                                <td width="50">Kode Produk</td> 
                                <td width="200">Produk Name</td>
                                <td width="50">Fee type</td>
                                <td width="50">End user fee</td>
                                <td width="50">Whitelabel Commission</td>
                                <td width="50">Total Commision Master Agent Include Downlines</td>
                            </tr> 
                          </thead>
                        @foreach ($srv['product'] as $prd)
                                                    
                            <tr>
                            <td> {{ $prd[1] }} </td> 
                            <td> {{ $prd[2] }} </td>
                            <td> {{ $prd[0] }} </td>
                            <td> {{ number_format($prd[9],0) }} </td>
                            <td> {{($prd[3] == 'PERCENTAGE' ?  $prd[4].' % ' : ' Rp.'. $prd[4])}} </td>
                            <td> {{($prd[5] == 'PERCENTAGE' ?  $prd[6].' % ' : ' Rp.'. $prd[6])}} </td>
                            </tr> 
                             

                                      @foreach ($prd[10] as $brk)
                                            <tr>
                                            <td width="50"><b> {{ $brk[0] }} </b></td> 
                                            <td width="200"> {{ $brk[1] }} </td>
                                            <td>  {{ $brk[6] }} </td>
                                            <td>{{ number_format($brk[7], 0) }} </td>
                                            <td> {{($brk[2] == 'PERCENTAGE' ?  $brk[3].' % ' : ' Rp.'. $brk[3])}} </td>
                                            <td> {{($brk[4] == 'PERCENTAGE' ?  $brk[5].' % ' : ' Rp.'. $brk[5])}} </td>
                                            </tr>
                                      @endforeach 
                                   


                                             
                        @endforeach  
                    </table>
                   
                    </div>                            
                    @endforeach
                    
                    <div class="clearfix"></div>                            
                    <hr/>

                  
                </div>
            </div>
        </div>
        </div>
        </div>
</div>
<script type="text/javascript">
    

      function showSrvForm(id)
      {
       // alert("test");
        $("#formupdtSrv").removeClass('hideform');
        $("#tablemainagentSrv").addClass('hideform');
        $.ajax({
                      type: "GET",
                      url: "editserviceform/"+id+".html",                     
                      success: function(msg) {
                       $("#formupdtSrv").html(msg);                                                            
                      }
                  });


       } 
</script>