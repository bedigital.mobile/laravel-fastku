                                <div class="clearfix"></div>
                                
                                <div class="form-group col-md-2 pull-right">
                                    <a href="#" class="btn btn-upload-image">
                                        <div class="crop center" id="avatar">
                                            @if ($rs->photo_filename != '')
                                            <img src="{{ url('asset_avatar/'.$rs->photo_filename) }}" />
                                            @else
                                            <img src="img/user.png" />
                                            @endif
                                        </div>
                                    </a>
                                    <input type="hidden" name="avatar-file" id="avatar-file" value="{{ $rs->photo_filename }}" />
                                </div>

                                <div class="form-group col-md-10 pull-left">
                                    <label for="fullname"> Full Name <small class="small red"><i>(Required)</i></small></label>
                                    <input type="text" id="fullname" name="fullname" placeholder="Full name" class="form-control" value="{{ $rs->fullname }}" />
                                </div>
                                
                                <div class="form-group col-md-3 pull-left">
                                    <label for="birth-date"> Birth Date <small class="small red"><i>(Required)</i></small></label>
                                    
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                        <input type="date" id="birth-date" name="birth-date" class="form-control" value="{{ @date('Y-m-d', @strtotime($rs->birth_date)) }}" />
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-4 pull-left">
                                    <label for="birth-place"> Birth Place <small class="small red"><i>(Required)</i></small></label>
                                    <input type="text" id="birth-place" name="birth-place" placeholder="Birth Place" class="form-control" value="{{ $rs->place_birth }}" />
                                </div>
                                
                                <div class="form-group col-md-3">
                                    <label for="gender"> Gender <small class="small red"><i>(Required)</i></small></label>

                                    <div class="clearfix"></div>

                                    <label class="radio-inline blue">
                                        <input name="gender" id="gender1" value="M" type="radio" class="ace" {!! ($rs->gender == 'M' ? 'checked="checked"' : '') !!} />
                                        <span class="lbl"> Male</span>
                                    </label>
                                    <label class="radio-inline blue">
                                        <input name="gender" id="gender2" value="F" type="radio" class="ace" {!! ($rs->gender == 'F' ? 'checked="checked"' : '') !!} />
                                        <span class="lbl"> Female</span>
                                    </label>
                                </div>
                                
                                <div class="form-group col-md-10 pull-left">
                                    <label for="corp-npwp"> NPWP <small class="small red"><i>(Required)</i></small></label>
                                    <input type="text" id="npwp" name="npwp" placeholder="NPWP" class="form-control" value="{{ $rs->npwp_number }}" />
                                </div>
                                                                
                                <div class="clearfix"></div>
                                <h4 class="blue">Contact</h4>
                                <hr/>
                                
                                <div class="form-group {{ ($rs->email_verified_at != null ? 'has-success' : '') }} col-md-4">
                                    <label for="email"> Email Address <small class="small red"><i>(Required)</i></small></label>                                  

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Valid Email Address" value="{{ $rs->email }}" />
                                    </div>
                                    @if ($rs->email_verified_at != null)
                                    <p class="help-block">
                                        Verified By: {{ $rs->email_verified_by }} <br/>
                                        @ {{ @date('m/d/Y H:i', @strtotime($rs->email_verified_at)) }}
                                    </p>
                                    @endif
                                </div>

                                <div class="form-group {{ ($rs->phone_number_verified_at != null ? 'has-success' : '') }} col-md-4">
                                    <label for="phone"> Phone Number <small class="small red"><i>(Required)</i></small></label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Valid Phone Number" value="{{ $rs->phone_number }}" />
                                    </div>
                                    @if ($rs->phone_number_verified_at != null)
                                    <p class="help-block">
                                        Verified By: {{ $rs->phone_number_verified_by }} <br/>
                                        @ {{ @date('m/d/Y H:i', @strtotime($rs->phone_number_verified_at)) }}
                                    </p>
                                    @endif
                                </div>
                                
                                <div class="form-group {{ ($rs->fax_number_verified_at != null ? 'has-success' : '') }} col-md-4">
                                    <label for="fax"> Fax Number </label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-print"></i></span>
                                        <input type="text" class="form-control" id="fax" name="fax" placeholder="Valid Fax. Number" value="{{ $rs->fax_number }}" />
                                    </div>
                                    @if ($rs->fax_number_verified_at != null)
                                    <p class="help-block">
                                        Verified By: {{ $rs->fax_number_verified_by }} <br/>
                                        @ {{ @date('m/d/Y H:i', @strtotime($rs->fax_number_verified_at)) }}
                                    </p>
                                    @endif
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label for="address"> Address <small class="small red"><i>(Required)</i></small></label>

                                    <textarea class="form-control" id="address" name="address">{{ $rs->address }}</textarea>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="village"> Village Name </label>

                                    <input type="text" class="form-control" id="village" name="village" placeholder="Name of village" value="{{ $rs->village_name }}" />
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="district"> District Name <small class="small red"><i>(Required)</i></small></label>

                                    <input type="text" class="form-control" id="district" name="district" placeholder="District Name" value="{{ $rs->district_name }}" />
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="city"> City <small class="small red"><i>(Required)</i></small></label>

                                    <input type="text" class="form-control" id="city" name="city" placeholder="City Name" value="{{ $rs->city_name }}" />
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="province"> Province <small class="small red"><i>(Required)</i></small></label>

                                    <input type="text" class="form-control" id="province" name="province" placeholder="Province Name" value="{{ $rs->province_name }}" />
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="postal"> Postal Code <small class="small red"><i>(Required)</i></small></label>

                                    <input type="text" class="form-control" id="postal" maxlength="5" name="postal" placeholder="Postal Code" value="{{ $rs->postal_code }}" />
                                </div>

