
 <div role="main">
          <div class="">
            
<div id="formupdtSrv" class="hideform"></div> 
<div id="tablemainagentSrv">         
     <div class="row"><h2>Service Master Agen</h2>
          <ul class="nav navbar-right panel_toolbox">
              <li><button id="updatesrv" onclick="showSrvForm('{{ $mainId }}');" class="btn btn-sm btn-success btn-block" type="button">Edit Service</button>
              </li> 
          </ul>
            <div class="box-inner">                 
                <div class="box-content"> 
                    @foreach ($services as $srv)
                    <?php $no=1; ?>  
                    <div class="form-group col-md-12">
                        <label for="{{ $srv['service'][0] }}">{{ $srv['service'][1] }}</label>

                        <div class="clearfix"></div>
                        <table class="table table-bordered">
                            <tr>
                                 <td width="50">Kode Produk</td> 
                                <td width="200">Produk Name</td>
                                <td width="50">Available</td>
                            </tr>

                  
                        @foreach ($srv['product'] as $prd)
                                                    
                            <tr>
                            <td> {{ $prd[1] }} </td> 
                            <td> {{ $prd[2] }} </td>
                            <td><i class="glyphicon glyphicon-{{ ($prd[7] == 1 ? 'ok green' : 'remove red') }}"></i></td>
                            </tr> 
                              @if($prd[8] == 'Y')
                              <tr><td colspan="3">
                                 <table class="table">
                                

                                      @foreach ($prd[9] as $brk)
                                            <tr>
                                            <td width="50"><b> {{ $brk[0] }} </b></td> 
                                            <td width="200"> {{ $brk[1] }} </td>
                                            <td><i class="glyphicon glyphicon-{{ ($prd[7] == 1 ? 'ok green' : 'remove red') }}"></i></td>
                                            </tr>
                                      @endforeach 
                                    </table>
                              </td></tr>
                               @endif


                                             
                        @endforeach  
                    </table>
                   
                    </div>                            
                    @endforeach
                    
                    <div class="clearfix"></div>                            
                    <hr/>

                  
                </div>
            </div>
        </div>
        </div>
        </div>
</div>
<script type="text/javascript">
    

      function showSrvForm(id)
      {
       // alert("test");
        $("#formupdtSrv").removeClass('hideform');
        $("#tablemainagentSrv").addClass('hideform');
        $.ajax({
                      type: "GET",
                      url: "editserviceform/"+id+".html",                     
                      success: function(msg) {
                       $("#formupdtSrv").html(msg);                                                            
                      }
                  });


       } 
</script>