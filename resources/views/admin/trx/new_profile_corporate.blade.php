                                <div class="clearfix"></div>
                                
                                <div class="form-group col-md-12">
                                    <label for="company-name"> Company Name <small class="small red"><i>(Required)</i></small></label>
                                    <input type="text" id="company-name" name="company-name" placeholder="Company name" class="form-control" />
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="department"> Department <small class="small red"><i>(Required)</i></small></label>
                                    <input type="text" id="department" name="department" placeholder="Department name" class="form-control" />
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="branch-office"> Branch Office <small class="small red"><i>(Required)</i></small></label>
                                    <input type="text" id="branch-office" name="branch-office" placeholder="Branch Office Level" class="form-control" />
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label for="corp-npwp"> NPWP <small class="small red"><i>(Required)</i></small></label>
                                    <input type="text" id="corp-npwp" name="corp-npwp" placeholder="NPWP" class="form-control" />
                                </div>
                                
                                <div class="clearfix"></div>
                                <h4 class="blue">Contact</h4>
                                <hr />

                                <div class="form-group col-md-6">
                                    <label for="contact"> Contact Person <small class="small red"><i>(Required)</i></small></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input type="text" id="contact" name="contact" placeholder="Contact person's name" class="form-control" />
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="corp-email"> Email Address <small class="small red"><i>(Required)</i></small></label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                        <input type="email" class="form-control" id="corp-email" name="corp-email" placeholder="Valid Email Address" value="" />
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="corp-phone"> Phone Number <small class="small red"><i>(Required)</i></small></label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                        <input type="text" class="form-control" id="corp-phone" name="corp-phone" placeholder="Valid Phone Number" value="" />
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label for="corp-fax"> Fax Number </label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-print"></i></span>
                                        <input type="text" class="form-control" id="corp-fax" name="corp-fax" placeholder="Valid Fax. Number" value="" />
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label for="corp-address"> Address <small class="small red"><i>(Required)</i></small></label>

                                    <textarea class="form-control" id="corp-address" name="corp-address"></textarea>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="corp-village"> Village Name </label>

                                    <input type="text" class="form-control" id="corp-village" name="corp-village" placeholder="Name of village" value="" />
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="corp-district"> District Name <small class="small red"><i>(Required)</i></small></label>

                                    <input type="text" class="form-control" id="corp-district" name="corp-district" placeholder="District Name" value="" />
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="corp-city"> City <small class="small red"><i>(Required)</i></small></label>

                                    <input type="text" class="form-control" id="corp-city" name="corp-city" placeholder="City Name" value="" />
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="corp-province"> Province <small class="small red"><i>(Required)</i></small></label>

                                    <input type="text" class="form-control" id="corp-province" name="corp-province" placeholder="Province Name" value="" />
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="corp-postal"> Postal Code <small class="small red"><i>(Required)</i></small></label>

                                    <input type="text" class="form-control" id="corp-postal" maxlength="5" name="corp-postal" placeholder="Postal Code" value="" />
                                </div>
                                