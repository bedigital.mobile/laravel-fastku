                                <div class="clearfix"></div>
                                
                                <div class="form-group col-md-2 pull-right">
                                    <div class="crop center" id="avatar">
                                        @if ($rs->profile->photo_filename != '')
                                        <img src="{{ url('asset_avatar/'.$rs->profile->photo_filename) }}" />
                                        @else
                                        <img src="img/user.png" />
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group col-md-10 pull-left">
                                    <label for="fullname"> Full Name </label>
                                    <span id="fullname" class="form-control">{{ $rs->profile->fullname }}</span>
                                </div>
                                
                                <div class="form-group col-md-3 pull-left">
                                    <label for="birth-date"> Birth Date </label>
                                    
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                        <span id="birth-date" class="form-control">{{ @date('m/d/Y', @strtotime($rs->profile->birth_date)) }}</span>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-4 pull-left">
                                    <label for="birth-place"> Birth Place </label>
                                    <span id="birth-place" class="form-control">{{ $rs->profile->place_birth }}</span>
                                </div>
                                
                                <div class="form-group col-md-3">
                                    <label for="gender"> Gender </label>

                                    <div class="clearfix"></div>
                                    <span class="form-control">{{ ($rs->profile->gender == 'F' ? 'Female' : 'Male') }}</span>
                                </div>
                                
                                <div class="form-group col-md-10 pull-left">
                                    <label for="corp-npwp"> NPWP </label>
                                    <span id="npwp" class="form-control">{{ $rs->profile->npwp_number }}</span>
                                </div>
                                                                
                                <div class="clearfix"></div>
                                <h4 class="blue">Contact</h4>
                                <hr/>
                                
                                <div class="form-group col-md-4">
                                    <label for="email"> Email Address </label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                        <span class="form-control" id="email">{{ $rs->profile->email }}</span>
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="phone"> Phone Number </label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                        <span class="form-control" id="phone">{{ $rs->profile->phone_number }}</span>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="fax"> Fax Number </label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-print"></i></span>
                                        <span class="form-control" id="fax">{{ $rs->profile->fax_number }}</span>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label for="address"> Address </label>

                                    <span class="form-control" id="address">{{ $rs->profile->address }}</span>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="village"> Village Name </label>

                                    <input type="text" class="form-control" id="village" name="village" placeholder="Name of village" value="{{ $rs->profile->village }}" />
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="district"> District Name </label>

                                    <input type="text" class="form-control" id="district" name="district" placeholder="District Name" value="{{ $rs->profile->district }}" />
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="city"> City </label>

                                    <input type="text" class="form-control" id="city" name="city" placeholder="City Name" value="{{ $rs->profile->city }}" />
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="province"> Province </label>

                                    <input type="text" class="form-control" id="province" name="province" placeholder="Province Name" value="{{ $rs->profile->province }}" />
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="postal"> Postal Code </label>

                                    <input type="text" class="form-control" id="postal" maxlength="5" name="postal" placeholder="Postal Code" value="{{ $rs->profile->postal_code }}" />
                                </div>

