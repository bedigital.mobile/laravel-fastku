@extends('admin.layoutkurir')

@prepend('scripts')
<script>
    function beforeSubmit() {
        $('#button-close').attr('disabled', 'disabled');

        $('#button-save').attr('disabled', 'disabled');
        $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Saving...');
    }

    function afterSubmit() {
        $('#button-close').removeAttr('disabled');

        $('#button-save').removeAttr('disabled');
        $('#button-save').html('<i class="glyphicon glyphicon-save"></i> Save');
    }

    function doSave() {
        sendForm2({
            method: 'PUT',
            form: 'frm',
            messageView: 'msg',
            url: '{{ secure_url('/mainagent_new.html') }}',
            redirect:  '{{ secure_url('/mainagent.html') }}',
            beforeSubmit: function() {
                serviceBuild();
                productBuild();
                beforeSubmit();
            },
            afterSubmit: afterSubmit
        });
    }

    function doEdit() {        
        sendForm2({
            method: 'POST',
            form: 'frm',
            messageView: 'msg',
            url: '{{ secure_url('/mainagent_edit.html') }}',
            redirect:  '{{ secure_url('/mainagent.html') }}',
            beforeSubmit: function () {
                serviceBuild();
                productBuild();
                beforeSubmit();
            },
            afterSubmit: afterSubmit
        });
    }
    
    function doDelete() {
        if (confirm('Are you sure you want to remove this Main Agent?')) {
            sendForm2({
                method: 'POST',
                form: 'frm',
                messageView: 'msg',
                url: '{{ secure_url('/mainagent_delete.html') }}',
                redirect:  '{{ secure_url('/mainagent.html') }}',
                beforeSubmit: function() {
                    $('#button-close').attr('disabled', 'disabled');
                    $('#button-save').attr('disabled', 'disabled');
                    $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Removing...');
                },
                afterSubmit: function() {
                    $('#button-close').removeAttr('disabled');
                    $('#button-save').removeAttr('disabled');
                    $('#button-save').html('<i class="glyphicon glyphicon-remove"></i> Remove');                }
            });
        }
    }
    
    function resetPassword() {
        if (confirm('Are you sure you want to reset this Main Agent\'s password?')) {
            sendForm2({
                method: 'GET',
                form: 'frm',
                messageView: null,
                url: 'http://twitter.com',
                redirect:  function() {},
                beforeSubmit: function() {
                    $('#reset-pwd').attr('disabled', 'disabled');
                    $('#reset-pwd').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Resetting password...');
                    $('#button-close').attr('disabled', 'disabled');
                    $('#button-save').attr('disabled', 'disabled');
                },
                afterSubmit: function() {
                    $('#reset-pwd').removeAttr('disabled');
                    $('#reset-pwd').html('Reset Password');
                    $('#button-close').removeAttr('disabled');
                    $('#button-save').removeAttr('disabled');
                }
            });
        }
    }
    
    function callbackImageUpload(data) {
        $('#avatar').html('<img src="{{ secure_url('/asset_avatar/') }}/' + data + '" />');
        $('#avatar-file').val(data);
    }
    
    var services = [];
    var products = [];
    
    function checkService(cb) {
        if ($(cb).is(':checked')) {
            var tmp = [];
            for (var i=0; i<this.services.length; i++) {
                if (this.services[i] != $(cb).val()) {
                    var ln = tmp.length;
                    tmp[ln] = this.services[i];
                }
            }
            this.services = tmp;
        } else {
            var ln = this.services.length;
            this.services[ln] = $(cb).val();
        }
        console.log(this.services);
        $("input[id^='" + $(cb).val() + "product']").each(function (i, el) {
            $(el).prop('checked', $(cb).is(':checked'));
            checkProduct(el);
        });
    }
    
    function serviceBuild() {
        var srv = this.services.length > 0 ? this.services.join(',') : '';
        $('#service-list').val(srv);
    }
    
    function checkProduct(cb) {
        if ($(cb).is(':checked')) {
            var tmp = [];
            for (var i=0; i<this.products.length; i++) {
                if (this.products[i] != $(cb).val()) {
                    var ln = tmp.length;
                    tmp[ln] = this.products[i];
                }
            }
            this.products = tmp;
        } else {
            var ln = this.products.length;
            this.products[ln] = $(cb).val();
        }
//        console.log(this.products);
    }
    
    function productBuild() {
        var prod = this.products.length > 0 ? this.products.join(',') : '';
        $('#product-list').val(prod);
    }
    
</script>
@endprepend
