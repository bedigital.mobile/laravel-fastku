@extends('admin.main_agent.layout')

@section('content')
 



<ul class="breadcrumb">
     <!-- <li><a href="{{ secure_url('/ma/report_agency.html') }}">Log Report Agent</a></li> -->
    <li>List Komisi Main Agent Monthly </li>
    <!-- <li style="float: right;">{{ $session->get('email') }}</li> -->
</ul>

        <div id="msginfo"></div>

 <div class="row">   
<div class="box col-md-6">
<div class="box-inner"  style="background-color: #fff">
<div class="box-header well" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
<h2><i class="glyphicon glyphicon-tasks"></i> Search</h2>
<div class="box-icon"> 
  <a href="#" class="btn btn-minimize btn-round btn-default" ><i class="fa fa-angle-down"></i></a>
</div>
</div>
<div class="collapse box-content" id="collapseExample">
<form action="/ma/komisi_ma.html" method="get" name="frms">
<div class="row">
  <div class="form-group col-md-4">  
          
    <label style="font-size: 15px" for="from">Year</label> 
    <select class="form-control" style="font-size: 15px" name="sf" id="sf">
    <option value="" selected="selected"> </option>
                            @foreach ($listyear as $yr)
                            <option value="{{$yr['report_year']}}">{{$yr['report_year']}}</option>
                            @endforeach

    </select>


    <label style="font-size: 15px" for="from">Option</label>
    <select class="form-control" style="font-size: 15px" name="tableselect" id="tableselect">
    <option value="" selected="selected"> </option>
    <option value="report_commission_master_agent_monthly.master_agent_id">Master Agent Id</option>
      <option value="master_agent_profile.fullname"> Master Agent Name</option> 
    </select>
</div> 
<div class="form-group col-md-4">
           
    <label style="font-size: 15px" for="from">Month</label> 
    <select class="form-control" style="font-size: 15px" name="sm" id="sm">
    <option value="" selected="selected"> </option>
                            @foreach ($listmonth as $mnt)
                            <option value="{{$mnt['report_month']}}">{{date('F', mktime(0, 0, 0, $mnt['report_month'], 10))}}</option>
                            @endforeach

    </select>


            <label style="font-size: 15px" for="from">Text</label>
            <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="search" id="search" placeholder="search by selected">
</div>
</div>     

<div class="row">
      <div class=" col-md-6">
          <button style="padding: 5px" type="submit" class="btn btn-primary">Search</button>
         &nbsp; <a class="btn btn-default" href="/ma/komisi_ma.html"><i class="fa fa-sync-alt"></i> Refresh</a>
       </div>
</div>



      </form> 
    </div>
</div>
</div>
</div> 
 <table id="x-table" class="table table-striped table-bordered">  
                <thead>
                    <tr style="height: 10%">
                        <th class="center" width="1%">No.</th>
                        <th width="15%">{!! $sorter->field(0) !!}</th>
                        <th width="15%">{!! $sorter->field(1) !!}</th>
                        <th width="10%">{!! $sorter->field(2) !!}</th>
                        <th width="10%">{!! $sorter->field(3) !!}</th>
                        <th width="10%">{!! $sorter->field(4) !!}</th> 
                        <th width="10%">{!! $sorter->field(5) !!}</th>  
                        <th width="10%"></th>
                     </tr>
                </thead>
                <tbody>
                    @if ($sorter->rowCount() == 0)
                    <tr>
                        <td colspan="5" align="center">No Records Found...</td>
                    </tr>
                    @else
                        @foreach ($sorter->pageRows() as $row)
                    <tr>
                      <td class="center">{{ $sorter->skippedRows = $sorter->skippedRows + 1 }}.</td>
                        <!-- <td>{{$row['customer_phone_number']}}</td>  -->
                        <!-- <td>{{$row['customer_name']}}</td>  -->
                        <td>{{$row['master_agent_id']}}                         </td>  
                        <td>{{$row['fullname']}}</td>  
                        <td>{{$row['report_year']}}</td>  
                        <td>{{date('F', mktime(0, 0, 0, $row['report_month'], 10))}}</td>  
                        <td>{{number_format($row['total_commission_received'])}}</td>  
                        <td>{{$row['status']}}</td>   
                        <td>  <a href="/ma/komisi_{{$row['white_label_id']}}_{{$row['master_agent_id']}}_{{$row['report_year']}}_{{str_pad($row['report_month'], 2, "0", STR_PAD_LEFT)}}.html">detail</a> | <a href="/ma/komisisum_{{$row['white_label_id']}}_{{$row['master_agent_id']}}_{{$row['report_year']}}_{{str_pad($row['report_month'], 2, "0", STR_PAD_LEFT)}}.html">summary</a></td>
                        
                    </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        @php
            $t = microtime(true);
            $micro = sprintf("%06d",($t - floor($t)) * 1000000);
            $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
            $time = $d->format("YmdHisu");
        @endphp
            <div class="row-fluid col-md-12 pull-right">{!! $sorter->pagination() !!}</div> 
              <a href="komisi_ma_export.html?tableselect={{$tableselect}}&search={{$search}}&sf={{$sf}}&sm={{$sm}}&t={{$time}}" target="_blank">export to excel </a>

 
<script type="text/javascript"> 
    
$(".use-address").click(function () {
  var id = $(this).closest("tr").find(".use-address").text();
  alert(id);
});
    
</script>
<script src="js/charisma.js"></script>
@endsection