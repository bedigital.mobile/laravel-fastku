                            <div class="clearfix"></div>
                            <h4 class="blue">Services &amp; Products</h4>
                            <hr/>

                            <div class="form-group col-md-12">
                                @foreach ($services as $srv)
                                <div class="form-group col-md-12">
                                    <label for="{{ $srv['service'][0] }}" class="checkbox blue">
                                        <input type="checkbox" name="service[]" id="service{{ $srv['service'][0] }}" value="{{ $srv['service'][0] }}" onclick="checkService(this)" {!! ($srv['service'][2] == 1 ? 'checked=""' : '') !!} />
                                        {{ $srv['service'][1] }}
                                    </label>

                                    <div class="clearfix"></div>

                                    @foreach ($srv['product'] as $prd)
                                    <div class="col-md-3">
                                        <label class="checkbox blue">
                                            <input name="product[]" id="{{ $srv['service'][0] }}product{{ $prd[1] }}" value="{{ $prd[1] }}" type="checkbox" onclick="checkProduct(this);" {!! ($prd[3] == 1 ? 'checked=""' : '') !!} />
                                            <span class="lbl"> {{ $prd[2] }} </span>
                                        </label>
                                            <!--@foreach ($prd[4] as $bu)
                                        <div class="col-md-4">{{ $bu[2] }}</div>
                                        <div class="clearfix"></div>
                                            @endforeach-->
                                    </div>
                                    @endforeach
                                </div>
                                @endforeach
                            </div>
                            <div id="frm-product">
                                <input type="hidden" name="service-list" id="service-list" value="" />
                                <input type="hidden" name="product-list" id="product-list" value="" />
                            </div>

