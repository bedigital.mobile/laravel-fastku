@extends('admin.main_agent.layout')

@section('content')
    <div>
        <ul class="breadcrumb">
            <li><a href="javascript:getPage('{{ url('/home.html') }}');">Home</a></li>
            <li><a href="javascript:getPage('{{ url('/mainagent.html') }}');">Main Agents</a></li>
            <li>Remove</li>
        </ul><!-- /.breadcrumb -->      
    </div>

    <div class="row-fluid">
        <div class="box col-md-12">
            <div class="box-inner">                
                <div class="box-content">
                        <div id="msg"></div>            
                        <form role="form" id="frm" onsubmit="doDelete();return false;">
                        @csrf
                        <input type="hidden" name="id" value="{{ $rs->id }}" />

                        <div class="clearfix"></div>
                        <h4 class="blue">General</h4>
                        <hr/>

                        <div class="form-group col-md-3">
                            <label for="profile-type"> Profile Type </label>

                            <div class="clearfix"></div>
                            <span id="profile-type" class="form-control"></span>
                        </div>

                        <div class="form-group">
                          
                        </div>

                        <div class="form-group col-md-12">
                            <label for="description"> Description </label>

                            <span id="description" class="form-control"></span>
                        </div>
                        
                        &nbsp;
                        <div class="clearfix"></div>
                        <h4 class="blue">Account</h4>
                        <hr/>

                        <div class="form-group col-md-6">
                            <label for="username"> User Name </label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                                <span class="form-control" id="username">{{ $rs->username }}</span>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="status"> Status </label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-flag red"></i></span>
                                <div class="form-control" id="status">{{ $rs->status }}</div>
                            </div>
                        </div>                        

                        &nbsp;
                        <div class="clearfix"></div>
                        <h4 class="blue">Agent</h4>
                        <hr/>

                        <div class="form-group col-md-12">
                            <label for="uplink"> Uplink Agent </label>

                            <div class="clearfix"></div>
                            <span class="form-control">{{ ($rs->uplink != null ? $rs->uplink->username : '--') }}</span>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="paymode"> Payment Mode</label>

                            <div class="clearfix"></div>
                            <span class="form-control"> {{ $rs->paymode }}</span>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="maxterminal"> Max. Terminal </label>

                            <div class="clearfix"></div>
                            <span class="form-control">{{ $rs->agent_max_terminal_default }}</span>
                        </div>

                        <div class="clearfix"></div>                            
                        <hr/>

                        <div class="form-actions">
                            <a href="javascript:getPage('{{ url('/mainagent.html') }}');" class="btn btn-warning btn-sm" id="button-close"><i class="glyphicon glyphicon-arrow-left"></i> Close</a>
                            &nbsp;
                            <button class="btn btn-danger btn-sm" type="submit" id="button-save"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('admin.utils.image_upload')

@endsection
