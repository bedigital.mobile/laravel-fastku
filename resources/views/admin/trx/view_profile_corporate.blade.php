                                <div class="clearfix"></div>
                                
                                <div class="form-group col-md-12">
                                    <label for="company-name"> Company Name </label>
                                    <span id="company-name" class="form-control">{{ $rs->profile->company_name }}</span>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="department"> Department </label>
                                    <span id="department" class="form-control">{{ $rs->profile->department }}</span>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="branch-office"> Branch Office </label>
                                    <span id="branch-office" class="form-control">{{ $rs->profile->branch_office_level }}</span>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label for="corp-npwp"> NPWP </label>
                                    <span id="corp-npwp" class="form-control">{{ $rs->profile->npwp_number }}</span>
                                </div>
                                
                                <div class="clearfix"></div>
                                <h4 class="blue">Contact</h4>
                                <hr />

                                <div class="form-group col-md-6">
                                    <label for="contact"> Contact Person </label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <span id="contact" class="form-control">{{ $rs->profile->contact_person }}</span>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="corp-email"> Email Address </label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                        <span class="form-control" id="corp-email">{{ $rs->profile->email }}</span>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="corp-phone"> Phone Number </label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                        <span class="form-control" id="corp-phone">{{ $rs->profile->phone_number }}</span>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label for="corp-fax"> Fax Number </label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-print"></i></span>
                                        <span class="form-control" id="corp-fax">{{ $rs->profile->fax_number }}</span>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <label for="corp-address"> Address </label>

                                    <span class="form-control" id="corp-address">{{ $rs->profile->address }}</span>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="corp-village"> Village Name </label>

                                    <span class="form-control" id="corp-village">{{ $rs->profile->village }}</span>
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="corp-district"> District Name </label>

                                    <span class="form-control" id="corp-district">{{ $rs->profile->district }}</span>
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="corp-city"> City </label>

                                    <span class="form-control" id="corp-city">{{ $rs->profile->city }}</span>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="corp-province"> Province </label>

                                    <span class="form-control" id="corp-province">{{ $rs->profile->province }}</span>
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label for="corp-postal"> Postal Code </label>

                                    <span class="form-control" id="corp-postal">{{ $rs->profile->postal_code }}</span>
                                </div>
                                