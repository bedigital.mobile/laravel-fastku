@extends('admin.main_agent.layout')

@section('content')
  
<style>

a.red.bold.hand {
    color: #858796;
}
 /* Remove default bullets */
ul, #myUL {
  list-style-type: none;
}

/*

.sidebar .nav-item .nav-link[data-toggle=collapse]::after {
    width: 1rem;
    text-align: center;
    float: right;
    vertical-align: 0;
    border: 0;
    font-weight: 900;
    content: '\f107';
    font-family: 'Font Awesome 5 Free';
}



.sidebar .nav-item .nav-link[data-toggle=collapse].collapsed::after {
    content: '\f105';
}
*/
/* Remove margins and padding from the parent ul */
#myUL {
  margin: 0;
  padding: 0;
}

/* Style the caret/arrow */
.caret {
  cursor: pointer;
  user-select: none; /* Prevent text selection */
}

/* Create the caret/arrow with a unicode, and style it */
.caret::before {
  content: '\f107';
    font-family: 'Font Awesome 5 Free';    font-weight: 900;
  color: #4e73df;
  display: inline-block;
  margin-right: 6px;
}

/* Rotate the caret/arrow icon when clicked on (using JavaScript) */
.caret-down::before {
  transform: rotate(180deg);
}

/* Hide the nested list */
.nested {
  display: none;
}

/* Show the nested list when the user clicks on the caret/arrow (with JavaScript) */
.active {
  display: block;
} 
 
</style> 
  <link rel="stylesheet" href="tree/themes/default/style.min.css" />

<ul class="breadcrumb">
     <!-- <li><a href="{{ secure_url('/ma/report_agency.html') }}">Log Report Agent</a></li> -->
    <li>List Tree Master Agent  </li>
    <!-- <li style="float: right;">{{ $session->get('email') }}</li> -->
</ul>
    <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12"> 
   
            
            
</div> 
</div>   
 
                   

<div id="html" class="demo">
    <ul>
          <li><a href="#">MA {!!$id!!}</a>
                    {!!$data!!} 
                    </li>
        
    </ul>
  </div>

    <script src="js/jquery-1.11.1.min.js"></script>

  <script src="tree/jstree.min.js"></script>


<script> 
     function updateWindow(a,b){
      if(b=="agen")
      {
           window.open("/ma/agent_update.html?tableselect=username&search="+a); 
      }
      else
      {
           window.open("/ma/m_agent.html?tableselect=ma&search="+a); 
      }
     

     }

$('#html').jstree();
</script>



@endsection