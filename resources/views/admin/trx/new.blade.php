@extends('main_agent.layout')

@section('content')
    <div>
        <ul class="breadcrumb">
            <li><a href="javascript:getPage('{{ url('/home.html') }}');">Home</a></li>
            <li><a href="javascript:getPage('{{ url('/mainagent.html') }}');">Main Agents</a></li>
            <li>New</li>
        </ul><!-- /.breadcrumb -->      
    </div>

    <div class="row-fluid">
        <div class="btn-group">
            <a href="#" id="tab-general">General</a>
            &nbsp;&nbsp;
            <a href="#" id="tab-service">Services &amp; Products</a>
        </div>
        
        <div class="box col-md-12">
            <div class="box-inner">                
                <div class="box-content">
                    <div id="msg"></div>            
                    <form role="form" id="frm" onsubmit="doSave();return false;">
                        @csrf

                        <div id="panel-general">
                            @include('main_agent.new_general')
                        </div>
                        <div id="panel-service">
                            @include('main_agent.new_service')
                        </div>

                        <div class="clearfix"></div>                            
                        <hr/>

                        <div class="form-actions">
                            <a href="javascript:getPage('{{ url('/mainagent.html') }}');" class="btn btn-warning btn-sm" id="button-close"><i class="glyphicon glyphicon-arrow-left"></i> Close</a>
                            &nbsp;
                            <button class="btn btn-info btn-sm" type="submit" id="button-save"><i class="glyphicon glyphicon-save"></i> Save</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('utils.image_upload')

@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        $('#view-corporate').hide();
        $('#view-personal').hide();
        $('input[name=profile-type]').on('click', function() {
            if ($(this).val() === '{{ $PROFILE_TYPE_CORPORATE }}') {
                $('#view-corporate').show();
                $('#view-personal').hide();
            } else {
                $('#view-corporate').hide();
                $('#view-personal').show();
            }
        });
        $('input[name=profile-type]')[0].click();
        
        $('input[name=paymode]').on('click', function() {
            if ($(this).val() === '{{ $PAYMODE_POSTPAID }}') {
                $('#max-trx').removeAttr('disabled');
            } else {
                $('#max-trx').attr('disabled', 'disabled');
            }
        });
        $('input[name=paymode]')[0].click();
        
        $('#auto-approve').on('click', function() {
            if ($(this).is(':checked')) {
                $('#field-approve').removeAttr('disabled');
            } else {
                $('#field-approve').attr('disabled', 'disabled');
            }
        });
        $('#field-approve').attr('disabled', 'disabled');
        
        $('#tab-general').on('click', function() {
            $('#tab-general').css({'font-weight': 'bold', 'text-decoration': 'underline'});
            $('#panel-general').show();
            
            $('#tab-service').css({'font-weight': 'normal', 'text-decoration': 'none'});
            $('#panel-service').hide();
        });
        
        $('#tab-service').on('click', function() {
            $('#tab-general').css({'font-weight': 'normal', 'text-decoration': 'none'});
            $('#panel-general').hide();
            
            $('#tab-service').css({'font-weight': 'bold', 'text-decoration': 'underline'});
            $('#panel-service').show();
        });        
        $('#tab-general').click();
    });
</script>
@endpush
