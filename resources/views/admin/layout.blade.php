
<!DOCTYPE html>
<html lang="en">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title>Admin POSFIN Agent Helpdesk</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Muhammad Usman">



    <!-- The styles -->
    <link id="bs-css" href="../admin/admin/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="../admin/admin/css/charisma-app.css" rel="stylesheet">
    <link href='../admin/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='../admin/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='../admin/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='../admin/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='../admin/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='../admin/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='../admin/admin/css/jquery.noty.css' rel='stylesheet'>
    <link href='../admin/admin/css/noty_theme_default.css' rel='stylesheet'>
    <link href='../admin/admin/css/elfinder.min.css' rel='stylesheet'>
    <link href='../admin/admin/css/elfinder.theme.css' rel='stylesheet'>
    <link href='../admin/admin/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='../admin/admin/css/uploadify.css' rel='stylesheet'>
    <link href='../admin/admin/css/animate.min.css' rel='stylesheet'>
    <!-- <link href="../admin/css/bootstrap-datetimepicker.min.css" rel="stylesheet"> -->

    <!-- jQuery -->
    <script src="../admin/bower_components/jquery/jquery.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>   
     <script src="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"></script>
    <!-- <script src="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"></script> -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js"></script>     -->


    <!-- Datatables -->
    <!-- <link href="../helpdesk/gen/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"> -->
    <!-- <link href="../helpdesk/gen/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="../admin/gen/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../admin/gen/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../admin/gen/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

   <!-- Datatables -->
    <!-- <script src="../gen/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../gen/datatables.net-bs/js/dataTables.bootstrap.min.js"></script> -->
    <!-- <script src="../gen/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../gen/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../gen/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../gen/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../gen/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../gen/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../gen/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../gen/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../gen/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>s
    <script src="../gen/datatables.net-scroller/js/dataTables.scroller.min.js"></script>  -->
    
   <!-- <script src="../gen/js/bootstrap-multiselect.min.js"></script>
   <script src="../gen/js/jquery.bootstrap-duallistbox.min.js"></script>
    <link rel="stylesheet" href="../gen/css/bootstrap-multiselect.min.css" />
    <link rel="stylesheet" href="../gen/css/bootstrap-duallistbox.min.css" /> -->

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="img/favicon.ico">

    <style>
        .crop {
            width: 128px;
            height: 128px;
            border: 1px #000 dotted;
            overflow: hidden;
        }

        .crop img {
            background-position: center;
            background-size: cover;
            height: 128px;
            display: block;
        }
    </style>
    
    @stack('styles')
</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- 
            <a class="navbar-brand" href="{{ secure_url('/admin/report_admin.html') }}">
                <span>{{ $session->get('group') }}</span>
            </a>

            user dropdown starts -->   
           
            <div class="btn-group pull-right">

                <button class="btn btn-default">
                    <a href="{{ secure_url('/admin/logout.html') }}">Logout</a>
                </button>
            </div> <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span class="hidden-sm hidden-xs">{{ $session->get('username') }} - {{ $session->get('group') }}</span>
                    
                </button>
                 
            </div>
            <!-- user dropdown ends -->         

            <ul class="collapse navbar-collapse nav navbar-nav top-menu">
                <li><a href="javascript:getPage('{{ secure_url('/admin/report_admin.html') }}');"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="  glyphicon glyphicon-list-alt"></i> History <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ secure_url('/admin/history_agent.html') }}">History Agent</a></li>
                        <li><a href="{{ secure_url('/admin/history_deposit.html') }}">History Deposit</a></li>
                    </ul>
                </li> 
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-hdd"></i> Transaction <span class="caret"></span></a>  
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ secure_url('/admin/pending_trx.html') }}">List Pending Trx</a></li>
                        <li><a href="{{ secure_url('/admin/refund_pending_trx.html') }}">Refund Pending Trx</a></li>
                                                <li><a href="{{ secure_url('/admin/refund_trx.html') }}">List Refund Trx</a></li>
                        <li><a href="{{ secure_url('/admin/refund_info.html') }}">Pending to Success Status</a></li>
                        <li><a href="{{ secure_url('/admin/force_trx.html') }}">Force Transaction</a></li>
                    </ul>
                </li> 
             
                 <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class=" glyphicon glyphicon-user"></i> Agent <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ secure_url('/admin/master_agent.html') }}">List Master Agent</a></li> 
                        <li><a href="{{ secure_url('/admin/agent_update.html') }}">List Agent</a></li> 
                    </ul>
                </li>  
                 <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-exclamation-sign"></i> Info <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ secure_url('/admin/recon_info.html') }}">Recon Info</a></li> 
                    </ul>
                </li> 
                <!-- <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-list-alt"></i> Reports <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Transactions</a></li>
                        <li><a href="#">Settlements</a></li>
                    </ul>
                </li>!-->
            </ul>
            
        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
        
        <!-- left menu starts -->
    <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">
                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <!--<li class="nav-header">Main</li>
                        <li><a class="ajax-link" href="javascript:getPage('{{ secure_url('/home.html') }}');"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a></li>
                        <li><a class="ajax-link" href="javascript:getPage('{{ secure_url('/mainagent.html') }}');"><i class="glyphicon glyphicon-eye-open"></i><span> Main Agents</span></a></li>
                        <li><a class="ajax-link" href="javascript:getPage('{{ secure_url('/report.html') }}');"><i class="glyphicon glyphicon-file"></i><span> Reports</span></a></li>!-->
                    </ul>
                </div>
            </div>
        </div>   
        <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">Warning!</h4>

                <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                    enabled to use this site.</p>
            </div>
        </noscript>
<div id="msginfo"></div>
        <div id="content" class="col-md-12 col-xs-12">        
            
            <!-- content starts -->
            @yield('content')
            <!-- content ends -->
        </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->

    <hr>

    <!--div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div-->

</div><!--/.fluid-container-->

<!-- external javascript -->

<!-- <script src="../helpdesk/bower_components/bootstrap/dist/js/bootstrap.min.js"></script> -->

<!-- library for cookie management -->
<!-- <script src="../helpdesk/js/jquery.cookie.js"></script> -->
<!-- calender plugin -->
<!-- <script src='../helpdesk/bower_components/moment/min/moment.min.js'></script> -->
<!-- <script src='../bower_components/fullcalendar/dist/fullcalendar.min.js'></script> -->
<!-- data table plugin -->

<!-- select or dropdown enhancer -->
<!-- <script src="../helpdesk/bower_components/chosen/chosen.jquery.min.js"></script> -->
<!-- plugin for gallery image view -->
<!-- <script src="../helpdesk/bower_components/colorbox/jquery.colorbox-min.js"></script> -->
<!-- notification plugin -->
<!-- <script src="../helpdesk/js/jquery.noty.js"></script> -->
<!-- library for making tables responsive -->
<!-- <script src="../helpdesk/bower_components/responsive-tables/responsive-tables.js"></script> -->
<!-- tour plugin -->
<!-- <script src="../helpdesk/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script> -->
<!-- star rating plugin -->
<script src="../admin/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="../admin/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="../admin/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="../admin/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="../admin/js/jquery.history.js"></script>
<!-- <script src="../js/bootstrap-datetimepicker.min.js"></script> -->

<!-- application script for Charisma demo -->
<script src="../admin/js/common_1.js"></script>






<!-- external javascript -->

<script src="../admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
 
<!-- calender plugin -->
<script src='../admin/bower_components/moment/min/moment.min.js'></script>
<script src='../admin/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
 
<!-- select or dropdown enhancer -->
<script src="../admin/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view --> 
<!-- notification plugin -->
<script src="../admin/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="../admin/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin --> 
<!-- star rating plugin --> 
<script src="../admin/js/charisma.js"></script>



@stack('scripts')
</body>
</html>
