@extends('white_label_biller.layout')

@section('content')
    <div>
        <ul class="breadcrumb">
            <li><a href="{{ url('/home.html') }}">Home</a></li>
            <li><a href="javascript:getPage('{{ url('/wlbiller.html') }}');">Biller Aggregators</a></li>
            <li>New</li>
        </ul><!-- /.breadcrumb -->      
    </div>

    <div class="row-fluid">
        <div class="box col-md-12">
            <div class="box-inner">                
                <div class="box-content">
                    <div id="msg"></div>            
                    <form role="form" id="frm" onsubmit="doSave();return false;">
                        @csrf

                        <div class="form-group col-md-12">
                            <label for="biller"> Biller <small class="small red"><i>(Required)</i></small></label>

                            <div class="clearfix"></div>

                            <select class="form-control" name="biller" id="biller">
                                @foreach ($rsbill as $bill)
                                <option value="{{ $bill->id }}">{{ $bill->aggregator_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="priority"> Priority <small class="small red"><i>(Required)</i></small></label>

                            <input class="form-control" type="text" name="priority" id="priority" maxlength="3" placeholder="Smaller priority is higher" value="" />
                        </div>

                        <div class="clearfix"></div>                            
                        
                        <div class="checkbox col-md-4">
                            <label>
                                <input type="checkbox" id="status" name="status" value="Y" checked=""> Active
                            </label>
                        </div>

                        <div class="clearfix"></div>                            
                        <hr/>

                        <div class="form-actions">
                            <a href="javascript:getPage('{{ url('/wlbiller.html') }}');" class="btn btn-warning btn-sm" id="button-close"><i class="glyphicon glyphicon-arrow-left"></i> Close</a>
                            &nbsp;
                            <button class="btn btn-info btn-sm" type="submit" id="button-save"><i class="glyphicon glyphicon-save"></i> Save</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
