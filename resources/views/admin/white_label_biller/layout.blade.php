@extends('layout')

@prepend('scripts')
<script>
    function beforeSubmit() {
        $('#button-close').attr('disabled', 'disabled');

        $('#button-save').attr('disabled', 'disabled');
        $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Saving...');
    }

    function afterSubmit() {
        $('#button-close').removeAttr('disabled');

        $('#button-save').removeAttr('disabled');
        $('#button-save').html('<i class="glyphicon glyphicon-save"></i> Save');
    }

    function doSave() {
        sendForm2({
            method: 'PUT',
            form: 'frm',
            messageView: 'msg',
            url: '{{ url('/wlbiller_new.html') }}',
            redirect:  '{{ url('/wlbiller_new.html') }}',
            beforeSubmit: beforeSubmit,
            afterSubmit: afterSubmit
        });
    }

    function doEdit() {
        sendForm2({
            method: 'POST',
            form: 'frm',
            messageView: 'msg',
            url: '{{ url('/wlbiller_edit.html') }}',
            redirect:  '{{ url('/wlbiller.html') }}',
            beforeSubmit: beforeSubmit,
            afterSubmit: afterSubmit
        });
    }
    
    function doDelete() {
        if (confirm('Are you sure you want to remove this Biller Aggregator?')) {
            sendForm2({
                method: 'POST',
                form: 'frm',
                messageView: 'msg',
                url: '{{ url('/wlbiller_delete.html') }}',
                redirect:  '{{ url('/wlbiller.html') }}',
                beforeSubmit: function() {
                    $('#button-close').attr('disabled', 'disabled');
                    $('#button-save').attr('disabled', 'disabled');
                    $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Removing...');
                },
                afterSubmit: function() {
                    $('#button-close').removeAttr('disabled');
                    $('#button-save').removeAttr('disabled');
                    $('#button-save').html('<i class="glyphicon glyphicon-remove"></i> Remove');                }
            });
        }
    }    
</script>
@endprepend
