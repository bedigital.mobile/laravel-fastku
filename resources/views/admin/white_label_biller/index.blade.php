@extends('white_label_biller.layout')

@section('content')
    <div>
        <ul class="breadcrumb">
            <li><a href="{{ url('/home.html') }}">Home</a></li>
            <li>Biller Aggregators</li>
        </ul>
    </div>

    <div class="row-fluid">
        <div class="row-fluid">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row-fluid col-md-12 pull-left">
                <form id="frm-search" onsubmit="goSearch('{{ url('/wlbiller.html') }}');return false;">
                <div class="input-group col-md-4 pull-left">
                    <input type="text" class="form-control" id="q" placeholder="{{ ($sorter->searchFor() != '' ? 'Search For: \''.$sorter->searchFor().'\'' : 'Search...')}}">
                    <span class="input-group-addon"><a href="javascript:goSearch('{{ url('/wlbiller.html') }}');"><i class="glyphicon glyphicon-search red"></i></a></span>
                    <span class="input-group-addon"><a href="javascript:getContentInTable('{{ url('/wlbiller.html') }}');"><i class="glyphicon glyphicon-refresh green"></i></a></span>
                </div>
                </form>
            </div>                        
            &nbsp;
            <div class="clearfix"></div>

            <table id="x-table" class="table table-striped table-bordered">  
                <thead>
                    <tr>
                        <th class="center" width="1%">No.</th>
                        <th width="53%">{!! $sorter->field(0) !!}</th>
                        <th width="10%">{!! $sorter->field(1) !!}</th>
                        <th width="20%">{!! $sorter->field(2) !!}</th>
                        <th width="15%">&nbsp;</th>
                    </tr>
                </thead>

                <tbody>
                    @if ($sorter->rowCount() == 0)
                    <tr>
                        <td colspan="5" align="center">No Records Found...</td>
                    </tr>
                    @else
                        @foreach ($sorter->pageRows() as $row)
                    <tr>
                        <td class="center">{{ $sorter->skippedRows = $sorter->skippedRows + 1 }}.</td>
                        <td>{{ $row->aggregator_name }}</td>
                        <td>{{ $row->priority }}</td>
                        <td>
                            @if ($row->is_active == 1)
                            <span class="label label-success">
                                Active
                            @else
                            <span class="label label-danger">
                                Inactive
                            @endif                                        
                            </span>
                        </td>
                        <td class="center">
                            <div class="hidden-sm hidden-xs btn-group">                            
                                <a class="btn btn-info" href="javascript:getContentInBox('{{ url('/wlbiller_edit_'.$row->wlb_id.'.html') }}');">
                                    <i class="glyphicon glyphicon-edit icon-white"></i>
                                </a>

                                <!--a class="btn btn-danger" href="javascript:getContentInBox('{{ url('/wlbiller_delete_'.$row->wlb_id.'.html') }}');">
                                    <i class="glyphicon glyphicon-trash icon-white"></i>
                                </a-->
                            </div>
                        </td>
                    </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>

            <div class="row-fluid col-md-12 pull-right">{!! $sorter->pagination() !!}</div>
        </div>
    </div>

@endsection