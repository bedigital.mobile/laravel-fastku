@extends('white_label_biller.layout')

@section('content')
    <div>
        <ul class="breadcrumb">
            <li><a href="{{ url('/home.html') }}">Home</a></li>
            <li><a href="javascript:getPage('{{ url('/wlbiller.html') }}');">Biller Aggregators</a></li>
            <li>Edit</li>
        </ul><!-- /.breadcrumb -->      
    </div>

    <div class="row-fluid">
        <div class="box col-md-12">
            <div class="box-inner">                
                <div class="box-content">
                    <div id="msg"></div>            
                    <form role="form" id="frm" onsubmit="doEdit();return false;">
                        @csrf
                        <input type="hidden" name="id" value="{{ $rs->id }}" />

                        <div class="form-group col-md-12">
                            <label for="biller"> Biller Aggregator <small class="small red"><i>(Required)</i></small></label>

                            <div class="clearfix"></div>

                            <span class="form-control">{{ $rs->aggregator_name }}</span>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="priority"> Priority <small class="small red"><i>(Required)</i></small></label>

                            <input class="form-control" type="text" name="priority" id="priority" maxlength="3" placeholder="Smaller priority is higher" value="{{ $rs->priority }}" />
                        </div>

                        <div class="clearfix"></div>                            
                        
                        <div class="form-group col-md-3 {{ ($rs->is_active == 1 ? 'has-success' : 'has-error') }}">
                            <label for="status">Status</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-flag"></i></span>
                                <span class="form-control" id="status" {{ ($rs->is_active == 1 ? 'green' : 'red') }}">{{ ($rs->is_active == 1 ? 'Active' : 'Inactive') }}</span>
                            </div>
                        </div>

                        <div class="clearfix"></div>                            
                        <hr/>

                        <div class="form-actions">
                            <a href="javascript:getPage('{{ url('/wlbiller.html') }}');" class="btn btn-warning btn-sm" id="button-close"><i class="glyphicon glyphicon-arrow-left"></i> Close</a>
                            &nbsp;
                            <button class="btn btn-info btn-sm" type="submit" id="button-save"><i class="glyphicon glyphicon-save"></i> Save</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
