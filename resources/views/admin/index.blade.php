@extends('admin.trx.layout')

@section('content')

<div class="content">
                    <!-- Statistics -->
                    <div class="row">
                        <div class="col-xl-8 d-flex flex-column">
                            <!-- Earnings Summary -->
                            <div class="block block-rounded flex-grow-1 d-flex flex-column">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Statistik Pesanan</h3><br>
                                    <small>Data pesanan anda dalam 30 hari terakhir</small>
                                    <div class="block-options">
                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                            <i class="si si-refresh"></i>
                                        </button>
                                       
                                    </div>
                                </div>
                                <div class="block-content block-content-full flex-grow-1 d-flex align-items-center"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                    <!-- Earnings Chart Container -->
                                    <!-- Chart.js Chart is initialized in js/pages/be_pages_dashboard.min.js which was auto compiled from _js/pages/be_pages_dashboard.js -->
                                    <!-- For more info and examples you can check out http://www.chartjs.org/docs/ -->
                                    <canvas class="js-chartjs-earnings chartjs-render-monitor" style="display: block; width: 591px; height: 295px;" width="591" height="295"></canvas>
                                </div>
                                <div class="block-content bg-body-light">
                                    <div class="row items-push text-center w-100">
                                        <div class="col-sm-4">
                                            <dl class="mb-0">
                                                <dt class="font-size-h3 font-w700">
                                                    <i class="fa fa-arrow-up font-size-lg text-success"></i> 2.5%
                                                </dt>
                                                <dd class="text-muted mb-0">Customer Growth</dd>
                                            </dl>
                                        </div>
                                        <div class="col-sm-4">
                                            <dl class="mb-0">
                                                <dt class="font-size-h3 font-w700">
                                                    <i class="fa fa-arrow-up font-size-lg text-success"></i> 3.8%
                                                </dt>
                                                <dd class="text-muted mb-0">Page Views</dd>
                                            </dl>
                                        </div>
                                        <div class="col-sm-4">
                                            <dl class="mb-0">
                                                <dt class="font-size-h3 font-w700">
                                                    <i class="fa fa-arrow-up font-size-lg text-success"></i> 1.7%
                                                </dt>
                                                <dd class="text-muted mb-0">New Products</dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Earnings Summary -->
                        </div>
                        <div class="col-xl-4 d-flex flex-column">
                            <!-- Last 2 Weeks -->
                            <!-- Sparkline Charts (.js-sparkline class is initialized in Helpers.sparkline() -->
                            <!-- For more info and examples you can check out http://omnipotent.net/jquery.sparkline/#s-about -->
                            <div class="row row-deck flex-grow-1">
                                <div class="col-md-6 col-xl-12">
                                    <div class="block block-rounded d-flex flex-column">
                                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between">
                                            <dl class="mb-0">
                                                <dt class="font-size-h2 font-w700">570</dt>
                                                <dd class="text-muted mb-0">Total Orders</dd>
                                            </dl>
                                            <div>
                                                <div class="d-inline-block px-2 py-1 rounded-lg font-size-sm font-w600 text-danger bg-danger-light">
                                                    <i class="fa fa-caret-down mr-1"></i>
                                                    2.2%
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block-content p-1 text-center overflow-hidden">
                                            <!-- Sparkline Line: Orders -->
                                            <span class="js-sparkline" data-type="line" data-points="[33,29,32,37,38,30,34,28,43,45,26,45,49,39]" data-width="100%" data-height="70px" data-chart-range-min="20" data-line-color="rgba(210, 108, 122, .4)" data-fill-color="rgba(210, 108, 122, .15)" data-spot-color="transparent" data-min-spot-color="transparent" data-max-spot-color="transparent" data-highlight-spot-color="#D26C7A" data-highlight-line-color="#D26C7A" data-tooltip-suffix="Orders"><canvas width="293" height="70" style="display: inline-block; width: 293.422px; height: 70px; vertical-align: top;"></canvas></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-12">
                                    <div class="block block-rounded d-flex flex-column">
                                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between">
                                            <dl class="mb-0">
                                                <dt class="font-size-h2 font-w700">$5,234.21</dt>
                                                <dd class="text-muted mb-0">Total Earnings</dd>
                                            </dl>
                                            <div>
                                                <div class="d-inline-block px-2 py-1 rounded-lg font-size-sm font-w600 text-success bg-success-light">
                                                    <i class="fa fa-caret-up mr-1"></i>
                                                    4.2%
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block-content p-1 text-center oveflow-hidden">
                                            <!-- Sparkline Line: Earnings -->
                                            <span class="js-sparkline" data-type="line" data-points="[716,1185,750,1365,956,890,1200,968,1158,1025,920,1190,720,1352]" data-width="100%" data-height="70px" data-chart-range-min="300" data-line-color="rgba(70,195,123, .4)" data-fill-color="rgba(70,195,123, .15)" data-spot-color="transparent" data-min-spot-color="transparent" data-max-spot-color="transparent" data-highlight-spot-color="#46C37B" data-highlight-line-color="#46C37B" data-tooltip-prefix="$"><canvas width="293" height="70" style="display: inline-block; width: 293.422px; height: 70px; vertical-align: top;"></canvas></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="block block-rounded d-flex flex-column">
                                        <div class="block-content block-content-full flex-grow-1 d-flex justify-content-between">
                                            <dl class="mb-0">
                                                <dt class="font-size-h2 font-w700">264</dt>
                                                <dd class="text-muted mb-0">New Customers</dd>
                                            </dl>
                                            <div>
                                                <div class="d-inline-block px-2 py-1 rounded-lg font-size-sm font-w600 text-success bg-success-light">
                                                    <i class="fa fa-caret-up mr-1"></i>
                                                    9.3%
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block-content p-1 text-center oveflow-hidden">
                                            <!-- Sparkline Line: New Customers -->
                                            <span class="js-sparkline" data-type="line" data-points="[25,15,36,14,29,19,36,41,28,26,29,33,23,41]" data-width="100%" data-height="70px" data-chart-range-min="0" data-line-color="rgba(70,195,123, .4)" data-fill-color="rgba(70,195,123, .15)" data-spot-color="transparent" data-min-spot-color="transparent" data-max-spot-color="transparent" data-highlight-spot-color="#46C37B" data-highlight-line-color="#46C37B" data-tooltip-prefix="$"><canvas width="293" height="70" style="display: inline-block; width: 293.422px; height: 70px; vertical-align: top;"></canvas></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Last 2 Weeks -->
                        </div>
                    </div>
                    <!-- END Statistics -->
                 </div>
@endsection