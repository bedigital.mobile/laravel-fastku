<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Register Paketkupos </title>

    <!-- Bootstrap -->
    <link href="gen/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="gen/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="gen/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="gen/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">

    <script src='https://www.google.com/recaptcha/api.js'></script>
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
                              <form class="user" id="frm" name="frm" onsubmit="doLogin(); return false;">

              <h1>Form Registrasi</h1>
              <div>
                <input type="text" id="username" name="username" class="form-control" placeholder="Nama" required="" />
              </div>
              <div>
                <input type="text" id="username" name="username" class="form-control" placeholder="Alamat" required="" />
              </div>
             <input type="hidden" name="uid" id="uid"/><br>
             <input type="hidden" name="plugin" id="plugin"/>
              <div class="form-group col-md-12 hide" id="otpfield">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-phone red"></i></span>
                    <input type="text" name="otp" id="otp" class="form-control" placeholder="OTP">
                </div>
             </div>
      <div class="form-group col-md-12">
<div class="g-recaptcha" data-sitekey="6LduhoEUAAAAAAZ7ZWwdC2egamtlNIbdhEuK1hUd"></div>
</div>
 
                 <input type="submit" value="Simpan"  class="btn btn-default submit">   

              <div class="clearfix"></div>

              <div class="separator">
                 <div id="msg">
                     
                </div>

                <div class="clearfix"></div>
                <br />

                <div>
                   
                  <p>©2020 Paketkupos</p>
                </div>
              </div>
            </form>
          </section>
        </div>
 
      </div>
    </div>
 
  </body>
</html>
