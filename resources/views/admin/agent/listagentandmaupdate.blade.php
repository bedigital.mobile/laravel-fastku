@extends('admin.main_agent.layout')
@section('content') 
<style>
/* Styles go here */

.tree, .tree ul {
    margin:0;
    padding:0;
    list-style:none
}
.tree ul {
    margin-left:1em;
    position:relative
}
.tree ul ul {
    margin-left:.5em
}
.tree ul:before {
    content:"";
    display:block;
    width:0;
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    border-left:1px solid
}
.tree li {
    margin:0;
    padding:0 1em;
    line-height:2em;
    color:#369;
    font-weight:700;
    position:relative
}
.tree ul li:before {
    content:"";
    display:block;
    width:10px;
    height:0;
    border-top:1px solid;
    margin-top:-1px;
    position:absolute;
    top:1em;
    left:0
}
.tree ul li:last-child:before {
    background:#fff;
    height:auto;
    top:1em;
    bottom:0
}
.indicator {
    margin-right:5px;
}
.tree li a {
    text-decoration: none;
    color:#369;
}
.tree li button, .tree li button:active, .tree li button:focus {
    text-decoration: none;
    color:#369;
    border:none;
    background:transparent;
    margin:0px 0px 0px 0px;
    padding:0px 0px 0px 0px;
    outline: 0;
}
li.branch {
    cursor: pointer;
}
</style>  
  <script data-require="jquery@2.1.3" data-semver="2.1.3" src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">  
          <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">List Tree Master Agent </h6>
                </div>
                <div class="card-body">

                     <div id="listtree">
                         
                        <div class="x_content"> 
                            <ul id="tree3" class="tree">
                            <li>  MA {!!$id!!} 
                            {!!$data!!} 
                            </li>
                            </ul>
                        </div> 
                    </div>  
                </div> 
        </div>   
    </div> 
    </div>    
 
 <script>
    $.fn.extend({
      treed: function(o) {

        var openedClass = 'glyphicon-minus-sign';
        var closedClass = 'glyphicon-plus-sign';

        if (typeof o != 'undefined') {
          if (typeof o.openedClass != 'undefined') {
            openedClass = o.openedClass;
          }
          if (typeof o.closedClass != 'undefined') {
            closedClass = o.closedClass;
          }
        };

        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function() {
          var branch = $(this); //li with children ul
          branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
          branch.addClass('branch');
          branch.on('click', function(e) {
            if (this == e.target) {
              var icon = $(this).children('i:first');
              icon.toggleClass(openedClass + " " + closedClass);
              $(this).children().children().toggle();
            }
          })
          branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
        tree.find('.branch .indicator').each(function() {
          $(this).on('click', function() {
            $(this).closest('li').click();
          });
        });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function() {
          $(this).on('click', function(e) {
            $(this).closest('li').click();
            e.preventDefault();
          });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function() {
          $(this).on('click', function(e) {
            $(this).closest('li').click();
            e.preventDefault();
          });
        });
      }
    });

     //Initialization of treeviews

    $('#tree1').treed();

    $('#tree2').treed({
      openedClass: 'glyphicon-folder-open',
      closedClass: 'glyphicon-folder-close'
    });

    $('#tree3').treed({
      openedClass: 'glyphicon-chevron-right',
      closedClass: 'glyphicon-chevron-down'
    });
  </script>
@endsection