@extends('admin.main_agent.layout')

@section('content')
 


<ul class="breadcrumb">
     <!-- <li><a href="{{ secure_url('/ma/report_agency.html') }}">Log Report Agent</a></li> -->
    <li>List SMS OTP Agent </li>
    <!-- <li style="float: right;">{{ $session->get('email') }}</li> -->
</ul>

<div id="msginfo"></div>
<div id="msg"></div>
 <div class="row">   
<div class="box col-md-6">
<div class="box-inner"  style="background-color: #fff">
<div class="box-header well" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
<h2><i class="glyphicon glyphicon-tasks"></i> Search</h2>
<div class="box-icon"> 
  <a href="#" class="btn btn-minimize btn-round btn-default" ><i class="fa fa-angle-down"></i></a>
</div>
</div>
<div class="collapse box-content" id="collapseExample">
<form action="/ma/agent_sms.html" method="get" name="frms">
<div class="row">
  <div class="form-group col-md-4">  
          
    <label style="font-size: 15px" for="from">Option</label>
    <select class="form-control" style="font-size: 15px" name="tableselect" id="tableselect">
    <option value="" selected="selected"> </option>
    <option value="id">Agent Id</option>
    <option value="name">Agent Name</option>
      <option value="email">Email</option>
    <option value="phone">Phone Number</option>  
    <option value="detail">SMS Message</option>  
    </select>
</div> 
<div class="form-group col-md-4">
           
            <label style="font-size: 15px" for="from">Text</label>
            <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="search" id="search" placeholder="search by selected">
</div>
</div>     

<div class="row">
      <div class=" col-md-6">
          <button style="padding: 5px" type="submit" class="btn btn-primary">Search</button>
         &nbsp; <a class="btn btn-default" href="/ma/agent_sms.html"><i class="fa fa-sync-alt"></i> Refresh</a>
       </div>
</div>



      </form> 
    </div>
</div>
</div>
</div>   
   <table id="x-table" class="table table-striped table-bordered gray-dark">  
                           <!--  <thead>
                                <tr style="background-color: #fff">
                                    <th class="center" width="1%">No.</th>
                                    <th width="10%">{!! $sorter->field(0) !!}</th>
                                    <th width="10%">{!! $sorter->field(1) !!}</th>
                                    <th width="10%">{!! $sorter->field(2) !!}</th>
                                    <th width="10%">{!! $sorter->field(3) !!}</th>
                                    <th width="10%">{!! $sorter->field(4) !!}</th>
                                    <th width="10%">{!! $sorter->field(5) !!}</th>  
                                    <th width="10%">{!! $sorter->field(6) !!}</th>  
                                </tr>
                            </thead> -->

                            <tbody>
                                @if ($sorter->rowCount() == 0)
                                <tr>
                                    <td colspan="4" align="center">No Records Found...</td>
                                </tr>
                                @else
                                    @foreach ($sorter->pageRows() as $row)
                                <tr>
                                    <td class="center" rowspan="4">{{ $sorter->skippedRows = $sorter->skippedRows + 1 }}.</td>
                                    <td width="10%">{!! $sorter->field(0) !!} </td><td>{{ $row->log_datetime }}</td>
                                  </tr><tr>
                                    <td>{!! $sorter->field(1) !!} </td><td>{{ $row->agent_id }}</td>                                
                                    <td>{!! $sorter->field(2) !!} </td><td>{{ $row->fullname }}</td>
                                  </tr><tr>
                                   <td>{!! $sorter->field(3) !!} </td> <td>{{ $row->number_receiver }}</td>                                 
                                   <td>{!! $sorter->field(4) !!} </td><td>{{ $row->email }}</td>  
                                   </tr><tr>
                                    <td height="100px">{!! $sorter->field(5) !!} </td><td colspan ="3" style="word-break: break-all;">{{ $row->sms_message }}</td> 
                                    <!-- <td>{{ $row->delivery_status_desc }}</td>  -->
                                    <tr>
                                      <td colspan="5" style="background-color: #fff"></td>
                                    </tr>
                                </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        
            <div class="row-fluid col-md-12 pull-right">{!! $sorter->pagination() !!}</div>
            <!-- <a href="agent_export.html?tableselect={{$tableselect}}&search={{$search}}" target="_blank">export to excel</a> -->
             @if(!empty($min)||!empty($search)||!empty($tableselect))
              <a href="agent_export.html?tableselect={{$tableselect}}&search={{$search}}" target="_blank">export to excels</a>
              @endif



<script type="text/javascript"> 
    
$(".use-address").click(function () {
  var id = $(this).closest("tr").find(".use-address").text();
  alert(id);
});
    
</script>
<script src="js/charisma.js"></script>
@endsection