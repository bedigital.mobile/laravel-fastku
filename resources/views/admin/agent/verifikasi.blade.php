@extends('admin.main_agent.layout')

@section('content')
 
<style>
  .titless a{
    color:#858796!important;
    font-weight: bold;
  }
</style>


<ul class="breadcrumb">
     <!-- <li><a href="{{ secure_url('/ma/report_agency.html') }}">Log Report Agent</a></li> -->
    <li>Verifikasi Gerai </li>
    <!-- <li style="float: right;">{{ $session->get('email') }}</li> -->
</ul>

<div id="msginfo"></div>
<div id="msg"></div>
 <div class="row">   
<div class="box col-md-6">
<div class="box-inner"  style="background-color: #fff">
<div class="box-header well" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
<h2><i class="glyphicon glyphicon-tasks"></i> Search</h2>
<div class="box-icon"> 
  <a href="#" class="btn btn-minimize btn-round btn-default" ><i class="fa fa-angle-down"></i></a>
</div>
</div>
<div class="collapse box-content" id="collapseExample">
<form action="/admin/verifikasi" method="get" name="frms">
<div class="row">
  <div class="form-group col-md-4">  
          
    <label style="font-size: 15px" for="from">Option</label>
    <select class="form-control" style="font-size: 15px" name="tableselect" id="tableselect">
    <option value="" selected="selected"> </option>
    <option value="nomordaftar">Nomor Daftar</option>
    <option value="nama">Nama Gerai</option>
    <option value="alamat">Alamat Gerai</option> 
    </select>
</div> 
<div class="form-group col-md-4">
           
            <label style="font-size: 15px" for="from">Text</label>
            <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="search" id="search" placeholder="search by selected">
</div>
</div>     

<div class="row">
      <div class=" col-md-6">
          <button style="padding: 5px" type="submit" class="btn btn-primary">Search</button>
         &nbsp; <a class="btn btn-default" href="/admin/verifikasi"><i class="fa fa-sync-alt"></i> Refresh</a>
       </div>
</div>



      </form> 
    </div>
</div>
</div>
</div> 

   <table id="x-table" class="table table-bordered">  
             
                <tbody>
                    @if ($sorter->rowCount() == 0)
                    <tr>
                        <td colspan="5" align="center">No Records Found...</td>
                    </tr>
                    @else
                        @foreach ($sorter->pageRows() as $row)
                    <tr>
                        <td class="center" rowspan="4">{{ $sorter->skippedRows = $sorter->skippedRows +1 }}.</td>
                        <td class="titless">{!! $sorter->field(0) !!}</td> <td style="background-color: transparent;" >{{$row['id']}}</td>                           
                    </tr>
                    <tr>                   
                      <td class="titless">{!! $sorter->field(1) !!}</td> <td>{{$row['nama']}}</td>                            
                     </tr><tr>
                        <td class="titless">{!! $sorter->field(2) !!}</td> <td>{{$row['alamat']}}</td>
 
                      </tr><tr>  
                        <td class="titless">{!! $sorter->field(3) !!}</td> <td >
                   @if($row['status'] == "ACTIVE")

                    <span class="btn btn-success btn-sm upd">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">VERIFIED</span>
                  </span>
                  @elseif(($row['status'] == "REGISTERING"))

                    <span class="btn btn-danger btn-sm upd">
                    <span class="icon text-white-50">
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span class="text">UNVERIFIED</span>
                  </span>
                  @endif

                      

                    </td> 
 

                    </tr><tr><td colspan="9" style="background-color:#3334;text-align: right;" height="20px" align="left">
                       @if($session->get('group') == "ADMIN")
                      <a class="btn btn-info btn-sm upd" href="javascript:getContentInBox('gerai_view_{{$row['id']}}');"><i class="fa fa-edit"></i> view </a>     
                    @endif
                    <!-- --></td>
                    </tr>
                        @endforeach 
                    @endif
                </tbody>
            </table>
            <div class="row-fluid col-md-12 pull-right">{!! $sorter->pagination() !!}</div>
                        <!-- 
                        <table id="x-table" class="table table-striped table-bordered">  
                            <thead>
                                <tr style="background-color: #fff">
                                    <th class="center" width="1%">No.</th>
                                    <th width="25%">{!! $sorter->field(0) !!}</th>
                                    <th width="20%">{!! $sorter->field(1) !!}</th>
                                    <th width="20%">{!! $sorter->field(2) !!}</th>
                                    <th width="20%">{!! $sorter->field(3) !!}</th>
                                    <th width="10%">{!! $sorter->field(4) !!}</th>
                                    <th width="10%">{!! $sorter->field(5) !!}</th>
                                    <th width="20%">{!! $sorter->field(6) !!}</th>
                                    <th width="25%">{!! $sorter->field(7) !!}</th>
                                    <th width="25%">{!! $sorter->field(8) !!}</th>
                                    <th width="25%">{!! $sorter->field(9) !!}</th>
                                </tr>
                            </thead>

                            <tbody>
                                @if ($sorter->rowCount() == 0)
                                <tr>
                                    <td colspan="4" align="center">No Records Found...</td>
                                </tr>
                                @else
                                    @foreach ($sorter->pageRows() as $row)
                                <tr>
                                    <td class="center">{{ $sorter->skippedRows = $sorter->skippedRows + 1 }}.</td>
                                    <td>{{ $row->agent_id }}</td>
                                    <td>{{ $row->agent_name }}</td>
                                    <td>{{ $row->master_agent_id }} - {{ $row->m_agent_name }}</td>
                                    <td>{{ $row->email }}</td>
                                    <td>{{ $row->phone_number }}</td>                                    
                                    <td>{{ number_format($row->daily_trx_total_price_max,0) }}</td>
                                    <td>{{ $row->npwp_number }}</td>
                                    <td>{{ $row->id_card_type }}</td>
                                    <td>{{ $row->id_card_number }}</td>
                                    <td>{{ $row->address }}</td>
                                </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        
            <div class="row-fluid col-md-12 pull-right">{!! $sorter->pagination() !!}</div> -->
           
     @php 
            $tims = time();
            @endphp
            <!-- <a href="agent_export.html?tableselect={{$tableselect}}&search={{$search}}&tims={{$tims}}" target="_blank">export to excel</a> -->
 

<script type="text/javascript"> 
    
$(".use-address").click(function () {
  var id = $(this).closest("tr").find(".use-address").text();
  alert(id);
});
    
</script>
<script src="js/charisma.js"></script>
@endsection