@extends('admin.main_agent.layout')

@section('content')
 
 <style type="text/css">
   /* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #4e73df;
}

input:focus + .slider {
  box-shadow: 0 0 1px #4e73df;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
 </style>



<ul class="breadcrumb">
     <!-- <li><a href="{{ secure_url('/ma/report_agency.html') }}">Log Report Agent</a></li> -->
    <li>List Operator Agent </li>
    <!-- <li style="float: right;">{{ $session->get('email') }}</li> -->
</ul>

<div id="msginfo"></div>
<div id="msg"></div>
 <div class="row">   
<div class="box col-md-6">
<div class="box-inner"  style="background-color: #fff">
<div class="box-header well" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
<h2><i class="glyphicon glyphicon-tasks"></i> Search</h2>
<div class="box-icon"> 
  <a href="#" class="btn btn-minimize btn-round btn-default" ><i class="fa fa-angle-down"></i></a>
</div>
</div>
<div class="collapse box-content" id="collapseExample">
<form action="/ma/list_operator.html" method="get" name="frms">
<div class="row">
  <div class="form-group col-md-4">  
          
    <label style="font-size: 15px" for="from">Option</label>
    <select class="form-control" style="font-size: 15px" name="tableselect" id="tableselect">
    <option value="" selected="selected"> </option>
    <option value="agent_id">Agent Id</option>
    <option value="name">Agent Name</option>
    <option value="operator">Operator</option> 
    </select>
</div> 
<div class="form-group col-md-4">
           
            <label style="font-size: 15px" for="from">Text</label>
            <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="search" id="search" placeholder="search by selected">
</div>
</div>     

<div class="row">
      <div class=" col-md-6">
          <button style="padding: 5px" type="submit" class="btn btn-primary">Search</button>
         &nbsp; <a class="btn btn-default" href="/ma/list_operator.html"><i class="fa fa-sync-alt"></i> Refresh</a>
       </div>
</div>



      </form> 
    </div>
</div>
</div>
</div> 

   <table id="x-table" class="table table-striped table-bordered gray-dark">  
             <thead>
                <tr>
                      <td>No.</td>
                      <td>{!! $sorter->field(0) !!}</td>
                      <td>{!! $sorter->field(1) !!}</td>
                      <td>{!! $sorter->field(2) !!}</td>
                      <td>{!! $sorter->field(3) !!}</td>
                      <td colspan="2">{!! $sorter->field(4) !!}</td> 
                    </tr> 
             </thead>
                <tbody>
                    @if ($sorter->rowCount() == 0)
                    <tr>
                        <td colspan="5" align="center">No Records Found...</td>
                    </tr>
                    @else
                        @foreach ($sorter->pageRows() as $row)
                   
                    <tr>           
                        <td class="center">{{ $sorter->skippedRows = $sorter->skippedRows + 1 }}.</td>            
                        <td>{{$row['agent_id']}}</td>                            
                        <td>{{$row['fullname']}}</td>                            
                        <td>{{$row['workstation_code']}}</td>                            
                        <td>{{ $row->workstation_name }}</td> 
                        <td> @if ($row->active == 'Y')                           
                              <span class="btn btn-success btn-circle btn-sm"><i class="fas fa-check"></i></span>
                              @else
                              <span class="btn btn-danger btn-circle btn-sm"><i class="fas fa-times"></i></span>
                              @endif

                        </td> @if($session->get('group') == "ADMIN")
                        <td  align="center">   
                            <label class="switch"> 
                          @if ($row->active == 'Y')                           
                             <input type="checkbox" name="updopr" id="updopr" onclick="return updateOprator('N','{{$row['workstation_id']}}');" checked="true">
                              @else
                             <input type="checkbox" name="updopr" id="updopr" onclick="return updateOprator('Y','{{$row['workstation_id']}}');">
                              @endif

                            <span class="slider round"></span>
                            </label> 
   
                         
                    <!-- --></td>  @endif
                    </tr> 
                     <tr>
                    </tr>
                        @endforeach 
                    @endif
                </tbody> 
            </table>
            <div class="row-fluid col-md-12 pull-right">{!! $sorter->pagination() !!}</div>
                        <!-- 
                        <table id="x-table" class="table table-striped table-bordered">  
                            <thead>
                                <tr style="background-color: #fff">
                                    <th class="center" width="1%">No.</th>
                                    <th width="25%">{!! $sorter->field(0) !!}</th>
                                    <th width="20%">{!! $sorter->field(1) !!}</th>
                                    <th width="20%">{!! $sorter->field(2) !!}</th>
                                    <th width="20%">{!! $sorter->field(3) !!}</th>
                                    <th width="10%">{!! $sorter->field(4) !!}</th>
                                    <th width="10%">{!! $sorter->field(5) !!}</th>
                                    <th width="20%">{!! $sorter->field(6) !!}</th>
                                    <th width="25%">{!! $sorter->field(7) !!}</th>
                                    <th width="25%">{!! $sorter->field(8) !!}</th>
                                    <th width="25%">{!! $sorter->field(9) !!}</th>
                                </tr>
                            </thead>

                            <tbody>
                                @if ($sorter->rowCount() == 0)
                                <tr>
                                    <td colspan="4" align="center">No Records Found...</td>
                                </tr>
                                @else
                                    @foreach ($sorter->pageRows() as $row)
                                <tr>
                                    <td class="center">{{ $sorter->skippedRows = $sorter->skippedRows + 1 }}.</td>
                                    <td>{{ $row->agent_id }}</td>
                                    <td>{{ $row->agent_name }}</td>
                                    <td>{{ $row->master_agent_id }} - {{ $row->m_agent_name }}</td>
                                    <td>{{ $row->email }}</td>
                                    <td>{{ $row->phone_number }}</td>                                    
                                    <td>{{ number_format($row->daily_trx_total_price_max,0) }}</td>
                                    <td>{{ $row->npwp_number }}</td>
                                    <td>{{ $row->id_card_type }}</td>
                                    <td>{{ $row->id_card_number }}</td>
                                    <td>{{ $row->address }}</td>
                                </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        
            <div class="row-fluid col-md-12 pull-right">{!! $sorter->pagination() !!}</div> --> 
 

 
<script type="text/javascript" src="js/jquery-1.11.1.min.js" charset="UTF-8"></script>


<script type="text/javascript"> 
    function updateOprator(values,ids ) { 
        if (confirm('Anda Yakin Akan menyimpan?')) {
             
            $.post('{{ secure_url('/ma/update_operator.html') }}', {id:ids,val:values })
            // Serialization looks good: name=textInNameInput&&telefon=textInPhoneInput etc
            .done(function(data) {
                if (data.error.code == 200) {
                    alert("Tersimpan");
                    window.location.href = "list_operator.html";

                } else {
                    $("#msginfopush").removeClass("hide");   
                    $("#msginfo").html("<strong>Send Push Failed</strong> ");  
                    $("#msginfo").addClass("alert-danger");                    
                    $("#msginfo").removeClass("alert-success");   
   

                }
            });
           } 
           // alert(values);
            return false;
        }


$(".use-address").click(function () {
  var id = $(this).closest("tr").find(".use-address").text();
  alert(id);
});
    

    // updopr
</script>
<script src="js/charisma.js"></script>
@endsection