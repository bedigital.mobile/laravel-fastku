@extends('admin.trx.layout')

@section('content')
<style type="text/css">
  
  .ntab {              
     text-indent: 1em;
    line-height: 10pt;  /* try also a bit smaller line-height */
}
</style> 
  <link href="admin_template/css/input.css" rel="stylesheet"> 

    <div class="row-fluid">
        <!-- <div id="msginfo"></div>? -->
        <div id="msginfopush" class="hide"><div id="msginfopushchild" class="alert "></div></div>

         <form name="frmupdateAgentHD" id="frmupdateAgentHD" method="post" enctype="multipart/form-data"  > 

        <input type="hidden" name="id_gerai" id="id_gerai" value="{{ $rs->id_gerai }}" />

 @csrf 

<div class="row">
    <div class="col-lg-6 mb-4">
         <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-black">Data Pribadi</h6>
                </div>
                <div class="card-body">
                  <h4 class="small font-weight-bold">Nama Agent:   <p class="ntab">{{ $rs->nama_agent }}</p></h4> 
                  <hr>
                  <h4 class="small font-weight-bold">No HP Agent:   <p class="ntab">{{ $rs->telp }} </p></h4>
                  <hr>
                  <h4 class="small font-weight-bold">eKTP:   <p class="ntab">{{ $rs->ektp }}</p></h4>
                  <hr>
                  <h4 class="small font-weight-bold">NPWP:   <p class="ntab">{{ $rs->npwp }}</p></h4>
                  <hr>
                  <h4 class="small font-weight-bold">Email:   <p class="ntab">{{ $rs->email }}</p></h4>
                  <hr> 
                </div>
              </div>
    </div> <div class="col-lg-6 mb-4">
         <div class="card shadow mb-4"> 
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-black">Profile Gerai</h6>
                </div>
                <div class="card-body">
                  <h4 class="small font-weight-bold">Nama Gerai:   <p class="ntab">{{ $rs->nama }}</p></h4> 
                  <hr>
                  <h4 class="small font-weight-bold">Alamat Gerai:   <p class="ntab">{{ $rs->alamat }}</p></h4>
                  <hr>
                  <div class="row">
                    <div class="col-lg-6 mb-4"> <h4 class="small font-weight-bold">Kota:
                        <p class="ntab">{{ $rs->city_name }}</p></h4> 
                    </div>
                    <div class="col-lg-6 mb-4"> <h4 class="small font-weight-bold">Provinsi:
                        <p class="ntab">{{ $rs->province_name }}</p></h4> 
                    </div>
                  </div>


                  <h4 class="small font-weight-bold">Ukuran Gerai:   <p class="ntab">{{ $rs->ukuran }}</p></h4>
                  <hr>
                  <h4 class="small font-weight-bold">Jarak Sesama Gerai:   <p class="ntab">{{ $rs->jarak_sesama_agent }}</p></h4>
                  <hr> 
                  <h4 class="small font-weight-bold">Data Pesaing Gerai:   <p class="ntab">{{ $rs->data_pesaing }}</p></h4>
                  <hr> 
<!-- 
                   Tampak Gerai Depan:  <br>
                  <img class="img-fluid" src="img/gerai/{{ $rs->photo_gerai_depan }}">
                  Tampak Gerai Belakang:  <br>
                  <img class="img-fluid" src="img/gerai/{{ $rs->photo_gerai_belakang }}">
                  Tampak Denah Gerai:  <br>
                  <img class="img-fluid" src="img/gerai/{{ $rs->photo_gerai_denah }}"> -->
                  Tampak Gerai :  <br>
 <div class="row">
  <div class="col-md-4">
    <div class="thumbnail">
      <a href="img/gerai/{{ $rs->photo_gerai_depan }}" target="_blank">
        <img src="img/gerai/{{ $rs->photo_gerai_depan }}" alt="" style="width:100%">
        <div class="caption"> 
        </div>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="thumbnail">
      <a href="img/gerai/{{ $rs->photo_gerai_belakang }}"  target="_blank">
        <img src="img/gerai/{{ $rs->photo_gerai_belakang }}" alt="" style="width:100%">
        <div class="caption"> 
        </div>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="thumbnail">
      <a href="img/gerai/{{ $rs->photo_gerai_denah }}"  target="_blank">
        <img src="img/gerai/{{ $rs->photo_gerai_denah }}" alt="" style="width:100%">
        <div class="caption">      
        </div>
      </a>
    </div>
  </div>
</div>
                </div>
              </div>
    </div>
  
  </div>
 
    <div class="clearfix"></div>      
    <div class="form-actions">       

 

@if($rs->status == "REGISTERING")
        <input type="submit" class="btn btn-primary btn-lg" name="sbmt_upd" id="sbmt_upd" value="VERIFIKASI">
         <a href="javascript:getContentInBox('{{ secure_url('/admin/verifikasi') }}');" class="btn btn-warning btn-lg" id="button-close">  Close</a>
@else
         <a href="javascript:getContentInBox('{{ secure_url('/admin/listgerai') }}');" class="btn btn-warning btn-lg" id="button-close">  Close</a>
@endif    </div>                        
    <hr/>

  


 
         </form>


    </div>
<script type="text/javascript" src="js/jquery-1.11.1.min.js" charset="UTF-8"></script>

<script type="text/javascript">

        $("#frmupdateAgentHD").submit(function( ) { 

        if (confirm('Anda Yakin Memproses verifikasi Gerai ini?')) { 
              
            $.post('{{ secure_url('/admin/process_verified') }}', $("#frmupdateAgentHD").serialize())
            // Serialization looks good: name=textInNameInput&&telefon=textInPhoneInput etc
            .done(function(data) {
                if (data.error.code == 200) {
                    $("#msginfopush").removeClass("hide");   
                    $("#msginfopushchild").html("<button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button><strong>Update Success</strong>");   
                    $("#msginfopushchild").addClass("alert-success");   
                    $("#msginfopushchild").removeClass("alert-danger");   
                    // $("#frmsendpushagen").[0].reset();
                    // $("#frmupdateAgentHD")[0].reset()

                } else {
                    // console.log("error");
                    $("#msginfopush").removeClass("hide");   
                    $("#msginfopushchild").html("<strong>Update Failed - "+ data.error.messages +"</strong> ");  
                    $("#msginfopushchild").addClass("alert-danger");                    
                    $("#msginfopushchild").removeClass("alert-success");   
   

                }
            });
           } 
            return false;
        });
  
</script>
  
@endsection
