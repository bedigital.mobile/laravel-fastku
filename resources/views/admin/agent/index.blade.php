@extends('admin.trx.layout')

@section('content')
 
<style>
  
</style>

 
<div id="msginfo"></div>
<div id="msg"></div>
<div class="block block-rounded">
                        <div class="block-header">
                            <h3 class="block-title">List User</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si si-magnifier"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                           
                            <table class="table table-bordered table-striped table-vcenter font-size-sm">
                                <tbody>
                    @if ($sorter->rowCount() == 0)
                    <tr>
                        <td colspan="5" align="center">No Records Found...</td>
                    </tr>
                    @else
                        @foreach ($sorter->pageRows() as $row)
                    <tr>
                        <td class="center" rowspan="6" width="5%">{{ $sorter->skippedRows = $sorter->skippedRows +1 }}.</td>
                        <td class="titless">{!! $sorter->field(0) !!}</td> <td style="background-color: transparent;" >{{$row['id']}}</td>                           
                    </tr>
                    <tr>                   
                      <td class="titless">{!! $sorter->field(1) !!}</td> <td>{{$row['username']}}</td>                            
                     </tr><tr>
                        <td class="titless">{!! $sorter->field(2) !!}</td> <td>{{$row['fullname']}}</td>
                     </tr><tr>
                        <td class="titless">{!! $sorter->field(3) !!}</td> <td>{{$row['email']}}</td>
                     </tr><tr>
                        <td class="titless">{!! $sorter->field(4) !!}</td> <td>{{$row['description']}}</td>
                        
 
                      </tr><tr>  
                        <td class="titless">{!! $sorter->field(5) !!}</td> <td >
                   @if($row['user_status'] == "ACTIVE")

                    <span class="btn btn-success btn-sm upd">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">ACTIVE</span>
                  </span>
                  @elseif(($row['user_status'] == "REGISTERING"))

                    <span class="btn btn-danger btn-sm upd">
                    <span class="icon text-white-50">
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span class="text">UNVERIFIED</span>
                  </span>
                  @endif

                      

                    </td> 
 

                    </tr>
                    <tr> <td colspan="5" style="background-color:#e1e6e9;"></td>
                    </tr>
                        @endforeach 
                    @endif
                </tbody>
           
                                                          </table>
                        </div>
                    </div>
            <div class="row-fluid col-md-12 pull-right">{!! $sorter->pagination() !!}</div>
                  
     @php 
            $tims = time();
            @endphp
        

<script type="text/javascript"> 
    
$(".use-address").click(function () {
  var id = $(this).closest("tr").find(".use-address").text();
  alert(id);
});
    
</script>
<script src="js/charisma.js"></script>
@endsection
