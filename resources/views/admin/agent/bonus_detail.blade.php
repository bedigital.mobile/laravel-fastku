@extends('admin.main_agent.layout')

@section('content')
 



<ul class="breadcrumb">
     <li style="padding-right: 20px"><a href="{{ secure_url('/ma/bonus_agen.html') }}">List Bonus Agen Monthly </a></li> <i  style="padding-right: 20px" class="fa fa-arrow-left "></i> 
    <li>List Detail Agen {{$agents}}</li>
    <!-- <li style="float: right;">{{ $session->get('email') }}</li> -->
</ul>

        <div id="msginfo"></div>
<!-- 
 <div class="row">   
<div class="box col-md-6">
<div class="box-inner"  style="background-color: #fff">
<div class="box-header well" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
<h2><i class="glyphicon glyphicon-tasks"></i> Search</h2>
<div class="box-icon"> 
  <a href="#" class="btn btn-minimize btn-round btn-default" ><i class="fa fa-angle-down"></i></a>
</div>
</div>
<div class="collapse box-content" id="collapseExample">
<form action="/ma/m_agent.html" method="get" name="frms">
<div class="row">
  <div class="form-group col-md-4">  
          
    <label style="font-size: 15px" for="from">Option</label>
    <select class="form-control" style="font-size: 15px" name="tableselect" id="tableselect">
    <option value="" selected="selected"> </option>
    <option value="master_agent_id">Master Agent Id</option>
    <option value="master_agent_profile.fullname">Agent Name</option>
     <option value="master_agent_parent_id">Parent Master Agent Id</option>
     <option value="email">Email</option>
    <option value="phone_number">Phone Number</option>  
    </select>
</div> 
<div class="form-group col-md-4">
           
            <label style="font-size: 15px" for="from">Text</label>
            <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="search" id="search" placeholder="search by selected">
</div>
</div>     

<div class="row">
      <div class=" col-md-6">
          <button style="padding: 5px" type="submit" class="btn btn-primary">Search</button>
         &nbsp; <a class="btn btn-default" href="/ma/m_agent.html"><i class="fa fa-sync-alt"></i> Refresh</a>
       </div>
</div>



      </form> 
    </div>
</div>
</div>
</div>  -->
 <table id="x-table" class="table table-striped table-bordered">  
                <thead>
                    <tr style="height: 10%">
                        <th class="center" width="1%">No.</th>
                        <th width="5%">{!! $sorter->field(0) !!}</th>
                        <th width="8%">{!! $sorter->field(1) !!}</th>
                        <th width="8%">{!! $sorter->field(2) !!}</th>
                        <th width="8%">{!! $sorter->field(3) !!}</th>
                        <th width="5%">{!! $sorter->field(4) !!}</th> 
                        <th width="5%">{!! $sorter->field(5) !!}</th> 
                     </tr>
                </thead> 
                <tbody>
                    @if ($sorter->rowCount() == 0)
                    <tr>
                        <td colspan="5" align="center">No Records Found...</td>
                    </tr>
                    @else
                        @foreach ($sorter->pageRows() as $row)
                    <tr>
                      <td class="center">{{ $sorter->skippedRows = $sorter->skippedRows + 1 }}.</td>  
                        <td>{{$row['log_trx_agent_id']}}</td>  
                        <td>{{$row['product_id']}}</td>   
                        <td>{{$row['product_code']}}</td>   
                        <td>{{$row['product_name']}}</td>   
                        <td>{{$row['trx_datetime']}}</td>   
                        <td>{{$row['trx_bonus_received']}}</td>   
                    </tr>
                        @endforeach
                    @endif
                </tbody> 
            </table>
            <div class="row-fluid col-md-12 pull-right">{!! $sorter->pagination() !!}</div> 
  <a href="{{$export}}" target="_blank">export to excel</a>


 
<script type="text/javascript"> 
    
$(".use-address").click(function () {
  var id = $(this).closest("tr").find(".use-address").text();
  alert(id);
});
    
</script>
<script src="js/charisma.js"></script>
@endsection