  <?php $no=1;?>

<div id="tableservices"> 
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <!--  <h3>Users <small>Some examples to get you started</small></h3>!-->
              </div>
 
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Service & Product <small></small></h2>
                   
                                     
                    <div class="clearfix"></div>

                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                       <a class="btn btn-success" href="javascript:getView('{{ secure_url('/service.html') }}');">List Product by Service</a>
                       <a class="btn btn-primary" href="javascript:getView('{{ secure_url('/listallproduct.html') }}');">List All Product</a>
                    </p>

                         


                        <table id="allproducttable" class="table table-striped table-bordered">  
                            <thead>
                                <tr>
                                    <th class="center" width="1%">No.</th>
                                    <th width="5%">Kode</th>
                                    <th width="35%"> Name</th>
                                    <th width="15%"> Alias</th>                                    
                                    <th width="15%">Service Name</th>

                                    <th width="10%">Status</th>
                                    
                                </tr>
                            </thead>

                            <tbody>


                 @if (count($listproduct) > 0)
                   
                       @foreach ($listproduct  as $row)
                       <tr>
                              <td>{{ $no }}</td>
                              <td>{{ $row->product_id }}</td>
                              <td>{{ $row->product_name }}</td>
                              <td>{{ $row->product_name_alias }}</td>
                              <td>{{ $row->service_name }}</td>
                               <td> 
                                 @if ($row->is_active == 1)
                                        <span class="label label-success">
                                            Active
                                        @else
                                        <span class="label label-danger">
                                            Inactive
                                        @endif
                                        </span>&nbsp;&nbsp; <a class="collapse-link"><i class="fa fa-pencil"></i></a>
                              </td> 
                        </tr>  




                        
                        <?php $no++;?>
                        @endforeach

                  @endif



                            </tbody>
                        </table>
                         </div>
                         </div>
                         </div>
                         </div>


    <script>
      $(document).ready(function() {
       
        var handleDataTableButtons = function() {
          if ($("#allproducttable").length) {
            $("#allproducttable").DataTable({
              dom: "Bfrtip",
              buttons: [
                
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };
 /**/
        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });

      function showServiceForm()
      {
       // alert("test");
        $("#formservices").removeClass('hideform');
        $.ajax({
                      type: "GET",
                      url: "{{ secure_url('/inputservice.html') }}",                     
                      success: function(msg) {
                       $("#formservices").html(msg);                                                            
                      }
                  });


        $("#tableservices").addClass('hideform');
      }
    </script>