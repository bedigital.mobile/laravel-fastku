@extends('layout')

@prepend('scripts')
<script>
    function beforeSubmit() {
        $('#button-close').attr('disabled', 'disabled');

        $('#button-save').attr('disabled', 'disabled');
        $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Saving...');
    }

    function afterSubmit() {
        $('#button-close').removeAttr('disabled');

        $('#button-save').removeAttr('disabled');
        $('#button-save').html('<i class="glyphicon glyphicon-save"></i> Save');
    }

    function doSave() {
        sendForm2({
            method: 'PUT',
            form: 'frm',
            messageView: 'msg',
            url: '{{ url('/wlservice_sub_'.$serviceId.'_product_new.html') }}',
            redirect:  '{{ url('/wlservice_sub_'.$serviceId.'_product.html') }}',
            beforeSubmit: function() {
                if (typeof buildFormBreakup === 'function') {
                    buildFormBreakup();
                }
                beforeSubmit();
            },
            afterSubmit: afterSubmit
        });
    }

    function doEdit() {
        sendForm2({
            method: 'POST',
            form: 'frm',
            messageView: 'msg',
            url: '{{ url('/wlservice_sub_'.$serviceId.'_product_edit.html') }}',
            redirect:  '{{ url('/wlservice_sub_'.$serviceId.'_product.html') }}',
            beforeSubmit: function() {
                if (typeof buildFormBreakup === 'function') {
                    buildFormBreakup();
                }
                beforeSubmit();
            },
            afterSubmit: afterSubmit
        });
    }
    
    function doDelete() {
        if (confirm('Are you sure you want to remove this Product?')) {
            sendForm2({
                method: 'POST',
                form: 'frm',
                messageView: 'msg',
                url: '{{ url('/wlservice_sub_'.$serviceId.'_product_delete.html') }}',
                redirect:  '{{ url('/wlservice_sub_'.$serviceId.'_product.html') }}',
                beforeSubmit: function() {
                    $('#button-close').attr('disabled', 'disabled');
                    $('#button-save').attr('disabled', 'disabled');
                    $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Removing...');
                },
                afterSubmit: function() {
                    $('#button-close').removeAttr('disabled');
                    $('#button-save').removeAttr('disabled');
                    $('#button-save').html('<i class="glyphicon glyphicon-remove"></i> Remove');                }
            });
        }
    }
    
    function callbackImageUpload(data) {
        $('#avatar').html('<img src="{{ url('/asset_avatar/') }}/' + data + '" />');
        $('#avatar-file').val(data);
    }
    
</script>
@endprepend
