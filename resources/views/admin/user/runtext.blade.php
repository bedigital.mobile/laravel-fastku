@extends('admin.main_agent.layout')

@section('content')
 



<ul class="breadcrumb">
     <!-- <li><a href="{{ secure_url('/ma/report_agency.html') }}">Log Report Agent</a></li> -->
    <li>Running Text </li>
    <!-- <li style="float: right;">{{ $session->get('email') }}</li> -->
</ul>

<div id="msginfo"></div>
<div id="msg"></div>
  <div class="row">   
<div class="box col-md-6">
<div class="box-inner"  style="background-color: #fff">
<div class="box-header well" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
<h2><i class="glyphicon glyphicon-tasks"></i> Form Input</h2>
<div class="box-icon"> 
  <a href="#" class="btn btn-minimize btn-round btn-default" ><i class="fa fa-angle-down"></i></a>
</div>
</div>
<div class="collapse box-content show" id="collapseExample">
<form  name="frmrunningtxt" id="frmrunningtxt">

    <input type="hidden" id="opsi" name="opsi" value="input" class="form-control">     
    <input type="hidden" id="idrun" name="idrun"  class="form-control"> 

   


<div class="row">
  <div class="form-group col-md-5">  
          
    <label style="font-size: 15px" for="from">Date Start</label>
    <input type="date" id="min" name="min"  class="form-control" required="" width="350px"> 
</div> 
<div class="form-group col-md-5">
           

    <label style="font-size: 15px" for="from">Date Stop</label>
    <input type="date" id="max" name="max"  class="form-control" required="">
</div>
 
</div>    

 <div class="row">
<div class="form-group col-md-6">
    <label style="font-size: 15px" for="from">Apply to</label>
    <select  name="sendto" id="sendto"   class="form-control">
      <option value="MASTER_AGENT">All Agen Under this MA</option>
      <!-- <option value="AGENT"> Only Agen</option> -->
    </select>
  <!--   <label style="font-size: 15px" for="from">Agen ID</label>
    <input type="text" name="agentid" id="agentid"   class="form-control"> -->
</div>
</div> 


  
<div class="row">
<div class="form-group col-md-12">
           

    <label style="font-size: 15px" for="from">Text Message</label>
    <textarea cols="10" rows="5" name="rtext" id="rtext"   class="form-control" required=""></textarea>
</div>

</div> 

<div class="row">
      <div class=" col-md-6">
          
          <input type="submit" value="Save">
         &nbsp; <a class="btn btn-default" href="/ma/running_text.html"><i class="fa fa-sync-alt"></i> Refresh</a>
       </div>
</div>



      </form> 
    </div>
</div>
</div>
</div>  
   <table id="x-table" class="table table-striped table-bordered gray-dark">  
    <thead>
       <tr>
        <th width="2%">No</th>
        <th width="5%">{!! $sorter->field(0) !!}</th>
        <th width="5%">{!! $sorter->field(1) !!}</th>
        <th width="15%">{!! $sorter->field(2) !!}</th>
        <th width="4%">{!! $sorter->field(3) !!}</th> 
        <th width="4%">{!! $sorter->field(4) !!}</th> 
        <th width="7%">{!! $sorter->field(5) !!}</th> 
       </tr>
    </thead>
             
                <tbody>
                    @if ($sorter->rowCount() == 0)
                    <tr>
                        <td colspan="5" align="center">No Records Found...</td>
                    </tr>
                    @else
                        @foreach ($sorter->pageRows() as $row)
                    <tr>
                        <td>{{ $sorter->skippedRows = $sorter->skippedRows +1 }}.</td>
                        
                                                  
                        <td>{{$row['show_date_start']}}</td>                            
                        <td>{{$row['show_date_stop']}}</td>
                        <td>{{$row['text_message']}}</td> 
                        <td>{{$row['owner_type']}}</td>                            
                        <td>{{$row['owner_id']}}</td>
                        <td>
                            
                    @if($row['active'] == "Y")

                    <span class="btn btn-success btn-sm upd">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">ACTIVE</span>
                  </span>
                  @else 

                    <span class="btn btn-danger btn-sm upd">
                    <span class="icon text-white-50">
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span class="text">NoT ACTIVE</span>
                  </span>
                  
                  @endif
 
                 
                  &nbsp;

                  <a href='#' onClick = "delcon({{$row['id']}});"  class=\"red bold hand btn-sm\"  >
                    <i class='fas fa-trash-alt'></i>
                  </a> 
                        </td> 
                    </tr>  
                   
                        @endforeach 
                    @endif
                </tbody>
            </table>
            <div class="row-fluid col-md-12 pull-right">{!! $sorter->pagination() !!}</div>
                       
 
<script type="text/javascript" src="js/jquery-1.11.1.min.js" charset="UTF-8"></script>


<script type="text/javascript">     
     $("#frmrunningtxt").submit(function( ) { 
        if (confirm('Anda Yakin Akan menyimpan?')) {
             
            $.post('{{ secure_url('/ma/saverunning_text.html') }}', $("#frmrunningtxt").serialize())
            // Serialization looks good: name=textInNameInput&&telefon=textInPhoneInput etc
            .done(function(data) {
                if (data.error.code == 200) {
                    alert("Tersimpan");
                    window.location.href = "running_text.html";

                } else {
                    $("#msginfopush").removeClass("hide");   
                    $("#msginfo").html("<strong>Send Push Failed</strong> ");  
                    $("#msginfo").addClass("alert-danger");                    
                    $("#msginfo").removeClass("alert-success");   
   

                }
            });
           } 
            return false;
        });  

   function delcon(vars) { 
        if (confirm('Anda Yakin Akan menghapus?')) {
             
            $.post('{{ secure_url('/ma/delrunning_text.html') }}', 
                { id:vars }
                )
            .done(function(data) {
                if (data.error.code == 200) {
                    alert("Terhapus");
                    window.location.href = "running_text.html";

                } else {
                    $("#msginfopush").removeClass("hide");   
                    $("#msginfo").html("<strong>Send Push Failed</strong> ");  
                    $("#msginfo").addClass("alert-danger");                    
                    $("#msginfo").removeClass("alert-success");   
   

                }
            });
           } 
            return false;
        } 
$(".use-address").click(function () {
  var id = $(this).closest("tr").find(".use-address").text();
  alert(id);
});
    
</script>
<script src="js/charisma.js"></script>
@endsection