<style type="text/css">
    .well {
    border-radius: 0;
}
.well-lg {
    padding: 24px;
    border-radius: 6px;
}
</style>
<div id="formupdtComs" class="hideform">

</div> 
<div id="tablelistfee">
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <!--  <h3>Users <small>Some examples to get you started</small></h3>!-->
              </div>
 
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Commision Fee WhiteLabel <small></small></h2>
                       <ul class="nav navbar-right panel_toolbox">
                                          <li><button id="addnew" onclick="showCommisionForm();" class="btn btn-sm btn-success btn-block" type="button"> Update Fee</button>
                                          </li> 
                      </ul>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                       
                    </p>
    @foreach ($services as $srv)                  
<div class="widget-box">
    <div class="widget-header widget-header-flat">
        <h4 class="widget-title"> {{ $srv['service'][1] }} </h4>
    </div>
 @foreach ($srv['product'] as $prd)
    <div class="widget-body">
        <div class="widget-main"> 
            <div class="row" style="padding-left: 45px;">
                <div class="col-xs-12">
                    <div class="well"> <table>
                      <tr>
                        <td width="55">Produk</td><td>: {{ $prd[2] }}</td>
                      </tr> <tr>
                        <td width="55">ID Produk</td><td>: {{ $prd[1] }}</td>
                      </tr>
                    <tr>
                      <td width="85">Label Fee </td><td>: {{($prd[3] == 'PERCENTAGE' ?  $prd[4].' % ' : ' Rp.'. $prd[4])}}</td>
                    </tr> <tr>
                      <td width="85">MA Fee </td><td>: {{($prd[5] == 'PERCENTAGE' ?  $prd[6].' % ' : ' Rp.'. $prd[6])}}</td>
                    </tr>
                    </table>
                    </div>
                      
                       <table id="datatable-buttons" class="table table-striped table-bordered">
                                          <thead>
                                            <tr>
                                              <th>Produk code</th>
                                              <th>Produk Name</th>
                                              <th>Whitelabel</th> 
                                              <th>MasterAgent</th> 
                                            </tr>
                                          </thead> 
                                          <tbody>
                                            @foreach ($prd[8] as $brk)
                                            <tr>
                                                <td width="35"> {{ $brk[2] }} </td> 
                                                <td width="100"> {{ $brk[3] }} </td>
                                                <td width="35">   {{($brk[4] == 'PERCENTAGE' ?  $brk[5].' % ' : ' Rp.'.$brk[5])}} </td>
                                                <td width="35">{{($brk[6] == 'PERCENTAGE' ?  $brk[7].' % ' : ' Rp.'.$brk[7])}} </td>
                                            </tr>
                                            @endforeach  
                                        </tbody>
                                    </table>
                </div>
            </div>
        </div>
    </div>
@endforeach 
@endforeach     
</div>
  
<script type="text/javascript">
    

      function showCommisionForm(id)
      {
       // alert("test");
        $("#formupdtComs").removeClass('hideform');
        $("#tablelistfee").addClass('hideform');
        $.ajax({
                      type: "GET",
                      url: "editcommision.html",                     
                      success: function(msg) {
                       $("#formupdtComs").html(msg);                                                            
                      }
                  });


       } 
</script>