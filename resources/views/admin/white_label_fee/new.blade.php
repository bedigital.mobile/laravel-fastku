@extends('white_label_product.layout')

@section('content')
    <div>
        <ul class="breadcrumb">
            <li><a href="{{ url('/home.html') }}">Home</a></li>
            <li><a href="javascript:getPage('{{ url('/wlservice.html') }}');">Services &amp; Products</a></li>
            <li><a href="javascript:getPage('{{ url('/wlservice_sub_'.$serviceId.'_product.html') }}');">Products</a></li>
            <li>New</li>
        </ul><!-- /.breadcrumb -->      
    </div>

    <div class="row-fluid">
        <div class="btn-group">
            <a href="{{ url('/wlservice_edit_'.$serviceId.'.html') }}">Service</a>
            &nbsp;&nbsp;
            <a href="{{ url('/wlservice_sub_'.$serviceId.'_product.html') }}" style="font-weight: bold;text-decoration: underline;">Products</a>
        </div>
        
        <form role="form" id="frm" onsubmit="doSave();return false;">
            @csrf
            <input type="hidden" name="wlservice" value="{{ $serviceId }}" />
            <input type="hidden" name="has-breakup" value="{{ $rs->has_breakup }}" />

            <div class="box col-md-12">
                <div class="box-inner">                
                    <div class="box-content">
                        <div id="msg"></div>            
                        <div class="clearfix"></div>
                        <h4 class="blue">New Product</h4>
                        <hr/>

                        <div class="form-group col-md-12">
                            <label for="service"> Service </label>

                            <div class="clearfix"></div>

                            <span class="form-control" id="service">{{ $rsserv->service_name }}</span>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="biller"> Biller Aggregator</label>

                            <div class="clearfix"></div>

                            <select class="form-control" name="biller" id="biller" data-rel="chosen">
                                <option value="">--</option>
                                @foreach ($rsbill as $bill)
                                <option value="{{ $bill->biller_id }}">{{ $bill->aggregator_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="product"> Product <small class="small red"><i>(Required)</i></small></label>

                            <div class="clearfix"></div>

                            <select class="form-control" name="product" id="product" data-rel="chosen" onchange="changeProduct(this);">
                                <option value="" selected="">--</option>
                                @foreach ($rsprod as $prd)
                                <option value="{{ $prd->product_id }}" {!! ($productId == $prd->product_id ? 'selected' : '') !!}>{{ $prd->product_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-12" id="box-alias">
                            <label for="product"> Alias</label>

                            <input class="form-control" type="text" name="alias" id="alias" placeholder="Alias name for product" value="" />
                        </div>

                        <div class="form-group col-md-3" id="box-curcode">
                            <label for="currency"> Currency Code <small class="small red"><i>(Required)</i></small></label>

                            <!--input class="form-control" type="text" name="currency" id="currency" maxlength="3" placeholder="Currency code" value="IDR" /-->
                            <span class="form-control">IDR</span>
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-6" id="box-fee">
                            <label for="fee"> Fee Type <small class="small red"><i>(Required)</i></small></label>

                            <div class="clearfix"></div>

                            <div class="radio-inline blue">
                                <input type="radio" name="fee-type" id="fee-type1" value="{{ $TRX_FEE_TYPE_FIXED }}" checked="" />
                                <label class="lbl">{{ $TRX_FEE_TYPE_FIXED }}</label>
                            </div>

                            <div class="radio-inline blue">
                                <input type="radio" name="fee-type" id="fee-type2" value="{{ $TRX_FEE_TYPE_PERCENTAGE }}" />
                                <label class="lbl">{{ $TRX_FEE_TYPE_PERCENTAGE }}</label>
                            </div>

                            <input class="form-control" type="text" name="fee" id="fee" maxlength="20" placeholder="Value" value="" />
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-6" id="box-discount">
                            <label for="discount"> Discount Type <small class="small red"><i>(Required)</i></small></label>

                            <div class="clearfix"></div>

                            <div class="radio-inline blue">
                                <input type="radio" name="discount-type" id="discount-type1" value="{{ $DISCOUNT_TYPE_FIXED }}" checked="" />
                                <label class="lbl">{{ $DISCOUNT_TYPE_FIXED }}</label>
                            </div>

                            <div class="radio-inline blue">
                                <input type="radio" name="discount-type" id="discount-type2" value="{{ $DISCOUNT_TYPE_PERCENTAGE }}" />
                                <label class="lbl">{{ $DISCOUNT_TYPE_PERCENTAGE }}</label>
                            </div>

                            <input class="form-control" type="text" name="discount" id="discount" maxlength="20" placeholder="Value" value="" />
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="box col-md-12" id="box-breakup" style="display: none;">
                <div class="box-inner">                
                    <div class="box-content">
                        <div class="clearfix"></div>
                        <h4 class="blue">Product Breakup</h4>
                        <hr/>
                        
                        <span class="btn-setting"></span>
                        <table id="bu-table" class="table table-striped table-bordered">  
                            <thead>
                                <tr>
                                    <th class="center" width="1%">No.</th>
                                    <th width="31%">Product Name</th>
                                    <th width="30%">Alias</th>
                                    <th width="12%">Trx Fee</th>
                                    <th width="12%">Discount</th>
                                    <th width="14%">Status</th>
                                    <th width="1%">&nbsp;</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td colspan="7">No Records Found...</td>
                                </tr>
                            </tbody>
                        </table>
                        <div id="frm-breakup"></div>
                    </div>
                </div>
            </div>
        
            <div class="box col-md-12">
                <div class="box-inner">                
                    <div class="box-content">
                        <div class="form-actions">
                            <a href="javascript:getPage('{{ url('/wlservice_sub_'.$serviceId.'_product.html') }}');" class="btn btn-warning btn-sm pull-left" id="button-close"><i class="glyphicon glyphicon-arrow-left"></i> Close</a>
                            &nbsp;
                            <button class="btn btn-info btn-sm pull-right" type="submit" id="button-save"><i class="glyphicon glyphicon-save"></i> Save</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
                
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Edit Product Breakup</h3>
                </div>
                <form action="" method="post" onsubmit="updateBreakup();return false;">
                    <input type="hidden" name="bu-index" id="bu-index" value="" />
                    <div class="modal-body">
                        <div class="form-group col-md-12">
                            <label for="bu-status">Status</label>
                            
                            <div class="clearfix"></div>

                            <div class="checkbox-inline blue">
                                <input type="checkbox" name="bu-status" id="bu-status" value="Y" />
                                <label class="lbl">Active</label>
                            </div>
                        </div>
                        
                        <div class="form-group col-md-12" id="fbu-code">
                            <label for="bu-name">Product Name <small class="small red"><i>(Required)</i></small></label>
                            
                            <span class="form-control" id="bu-name"></span>
                        </div>
                        
                        <div class="form-group col-md-12" id="fbu-alias">
                            <label for="bu-alias">Alias Name</label>
                            
                            <input class="form-control" name="bu-alias" id="bu-alias" placeholder="Product Alias Name" value="" />
                            <p id="pbu-alias" class="help-block"></p>
                        </div>
                        
                        <div class="form-group col-md-6" id="fbu-fee">
                            <label for="bu-fee"> Fee Type <small class="small red"><i>(Required)</i></small></label>

                            <div class="clearfix"></div>

                            <div class="radio-inline blue">
                                <input type="radio" name="bu-fee-type" id="bu-fee-type1" value="{{ $TRX_FEE_TYPE_FIXED }}" />
                                <label class="lbl">{{ $TRX_FEE_TYPE_FIXED }}</label>
                            </div>

                            <div class="radio-inline blue">
                                <input type="radio" name="bu-fee-type" id="bu-fee-type2" value="{{ $TRX_FEE_TYPE_PERCENTAGE }}" />
                                <label class="lbl">{{ $TRX_FEE_TYPE_PERCENTAGE }}</label>
                            </div>

                            <input class="form-control" type="text" name="bu-fee" id="bu-fee" maxlength="20" placeholder="Value" value="" />
                            <p id="pbu-fee" class="help-block"></p>
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-6" id="fbu-discount">
                            <label for="bu-discount"> Discount Type <small class="small red"><i>(Required)</i></small></label>

                            <div class="clearfix"></div>

                            <div class="radio-inline blue">
                                <input type="radio" name="bu-discount-type" id="bu-discount-type1" value="{{ $DISCOUNT_TYPE_FIXED }}" />
                                <label class="lbl">{{ $DISCOUNT_TYPE_FIXED }}</label>
                            </div>

                            <div class="radio-inline blue">
                                <input type="radio" name="bu-discount-type" id="bu-discount-type2" value="{{ $DISCOUNT_TYPE_PERCENTAGE }}" />
                                <label class="lbl">{{ $DISCOUNT_TYPE_PERCENTAGE }}</label>
                            </div>

                            <input class="form-control" type="text" name="bu-discount" id="bu-discount" maxlength="20" placeholder="Value" value="" />
                            <p id="pbu-discount" class="help-block"></p>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" id="button-bu-close" class="btn btn-sm btn-danger pull-left" data-dismiss="modal" onclick="clearFormBreakup();"><i class="glyphicon glyphicon-remove"></i> Close</a>
                        <button type="submit" id="button-bu-save" class="btn btn-primary btn-sm pull-right"><i class="glyphicon glyphicon-save"></i> Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    function changeProduct(obj) {
        var addr = '{{ url('wlservice_sub_'.$serviceId.'_product_new_') }}' + obj.options[obj.selectedIndex].value + '.html';
        console.log(addr);
        self.location.replace(addr);
    }
</script>
<script>
    var breakups = [];
    
    function updateBreakup() {
        if (validateBreakup() === true) {
            var i = $('#bu-index').val() === '' ? -1 : parseInt($('#bu-index').val());
            if (i > -1 && i < this.breakups.length) {
                this.breakups[i].status = ($('input[id=bu-status]:checked').length == 1 ? 'Y' : 'N');
                this.breakups[i].alias = $('#bu-alias').val();
                this.breakups[i].fee.type = $('input[name=bu-fee-type]:checked').val();
                this.breakups[i].fee.value = parseInt($('#bu-fee').val());
                this.breakups[i].discount.type = $('input[name=bu-discount-type]:checked').val();
                this.breakups[i].discount.value = parseInt($('#bu-discount').val());
                buildTableBreakup();
                $('#button-bu-close').click();
            }
        }
    }
    
    function editBreakup(i) {
        if (i < this.breakups.length) {
            var bu = this.breakups[i];
            $('#bu-index').val(i);
            $('#bu-status').prop('checked', (bu.status === 'Y' ? true : false)),
            $('#bu-name').html(bu.name);
            $('#bu-alias').val(bu.alias);
            if (bu.fee.type === '{{ $TRX_FEE_TYPE_PERCENTAGE }}') {
                $('input[name=bu-fee-type]')[1].click();
            } else {
                $('input[name=bu-fee-type]')[0].click();
            }
            $('#bu-fee').val(bu.fee.value);
            if (bu.discount.type === '{{ $DISCOUNT_TYPE_PERCENTAGE }}') {
                $('input[name=bu-discount-type]')[1].click();
            } else {
                $('input[name=bu-discount-type]')[0].click();
            }
            $('#bu-discount').val(bu.discount.value);
            $('.btn-setting').click();
        }
    }
    
    function buildTableBreakup() {
        var tbl = $('#bu-table tbody');
        var row = '';
        var no = 0;
        
        if (this.breakups.length > 0) {
            for (var i=0; i<this.breakups.length; i++) {
                no++;
                row += '<tr>';
                row += '<td>' + no + '.</td>';
                row += '<td>' + this.breakups[i].name + '</td>';
                row += '<td>' + this.breakups[i].alias + '</td>';
                row += '<td>' + this.breakups[i].fee.value + (this.breakups[i].fee.type === '{{ $TRX_FEE_TYPE_PERCENTAGE }}' ? '%' : '') + '</td>';
                row += '<td>' + this.breakups[i].discount.value + (this.breakups[i].discount.type === '{{ $DISCOUNT_TYPE_PERCENTAGE }}' ? '%' : '') + '</td>';
                row += '<td><span class="label label-' + (this.breakups[i].status === 'Y' ? 'success">Active' : 'danger">Inactive') + '</span></td>';
                row += '<td><a href="javascript:editBreakup(' + i + ')"><i class="glyphicon glyphicon-pencil"></i></a></td>';
                row += '</tr>';
            }
        } else {
            row += '<tr>';
            row += '<td colspan="7">No records found...</td>';
            row += '</tr>';
        }
        tbl.html(row);
    }
    
    function buildFormBreakup() {
        var frm = $('#frm-breakup');
        var ctn = '<input type="hidden" name="bu-count" value="' + this.breakups.length + '" />';
        
        if (this.breakups.length > 0) {
            for (var i=0; i<this.breakups.length; i++) {
                ctn += '<input type="hidden" name="bu-code-' + i + '" value="' + this.breakups[i].code + '" />';
                ctn += '<input type="hidden" name="bu-alias-' + i + '" value="' + this.breakups[i].alias + '" />';
                ctn += '<input type="hidden" name="bu-fee-type-' + i + '" value="' + this.breakups[i].fee.type + '" />';
                ctn += '<input type="hidden" name="bu-fee-' + i + '" value="' + this.breakups[i].fee.value + '" />';
                ctn += '<input type="hidden" name="bu-discount-type-' + i + '" value="' + this.breakups[i].discount.type + '" />';
                ctn += '<input type="hidden" name="bu-discount-' + i + '" value="' + this.breakups[i].discount.value + '" />';
                ctn += '<input type="hidden" name="bu-status-' + i + '" value="' + this.breakups[i].status + '" />';
            }
        }
        
        frm.html(ctn);
    }
    
    function checkFieldBreakup(field, val) {
        var isTrue = true;
        if (this.breakups.length > 0) {
            for (var i=0; i<this.breakups.length; i++) {
                if (i !== parseInt($('#bu-index').val())) {
                    console.log(i + ' - ' + $('#bu-index').val());
                    var c = eval('this.breakups[' + i + '].' + field);
                    if (c === val) {
                        isTrue = false;
                        break;
                    }
                }
            }
        }
        
        return isTrue;
    }
    
    function validateBreakup() {
        var isTrue = true;
        
        if (checkFieldBreakup('alias', $('#bu-alias').val()) === false) {
            $('#fbu-alias').addClass('has-error');
            $('#pbu-alias').html('Product alias name exists. Please input another.');
            isTrue = false;
        }
        
        if ($('#bu-fee').val() === '' || isNaN($('#bu-fee').val()) === true || parseInt($('#bu-fee').val()) < 0) {
            $('#fbu-fee').addClass('has-error');
            $('#pbu-fee').html('Fee value must be number and greater than or equal zero.');
            isTrue = false;
        }
        
        if ($('#bu-discount').val() === '' || isNaN($('#bu-discount').val()) === true || parseInt($('#bu-discount').val()) < 0) {
            $('#fbu-discount').addClass('has-error');
            $('#pbu-discount').html('Discount value must be number and greater than or equal zero.');
            isTrue = false;
        }
        
        return isTrue;
    }
    
    function clearFormBreakup() {
        $('#bu-status').prop('checked', false);
        
        $('#bu-code').val('');
        $('#fbu-code').removeClass('has-error');
        $('#pbu-code').html('');
        
        $('#bu-alias').val('');
        
//        $('#bu-curcode').val('');
//        $('#fbu-curcode').removeClass('has-error');
//        $('#pbu-curcode').html('');
        
        $('input[name=bu-fee-type]')[0].click();
        $('#bu-fee').val('');
        $('#fbu-fee').removeClass('has-error');
        $('#pbu-fee').html('');
        
        $('input[name=bu-discount-type]')[0].click();
        $('#bu-discount').val('');
        $('#fbu-discount').removeClass('has-error');
        $('#pbu-discount').html('');
    }
    
    function triggerBreakup() {
        @if ($rs->has_breakup == 'Y' && isset($rsbu) && $rsbu)
            @foreach ($rsbu as $bu)
        var ln = this.breakups.length;
        this.breakups[ln] = {
            'status': '{{ $bu->active }}',
            'code': '{{ $bu->prod_code }}',
            'name': '{{ $bu->product_name }}',
            'alias': '{{ $bu->product_name_alias }}',
            'fee': {
                'type': '{{ $bu->trx_fee_type }}',
                'value': '{{ $bu->trx_fee_value }}'
            },
            'discount': {
                'type': '{{ $bu->discount_type }}',
                'value': '{{ $bu->discount_value }}'
            }
        };
            @endforeach
        @endif
        buildTableBreakup();
    }
    
    $(document).ready(function() {
        @if ($rs->has_breakup == 'Y' && isset($rsbu) && $rsbu)
        $('#box-fee').hide();
        $('#box-discount').hide();
        $('#box-breakup').show();
        triggerBreakup();
        @else
        $('#box-fee').show();
        $('#box-discount').show();
        $('#box-breakup').hide();
        @endif
    });
</script>
@endpush