 
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <!--  <h3>Users <small>Some examples to get you started</small></h3>!-->
              </div>
 
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Update Fee WhiteLabel <small></small></h2>
                       <ul class="nav navbar-right panel_toolbox">
                                          <li><button id="addnew" onclick="showCommisionList();" class="btn btn-sm btn-success btn-block" type="button"> Cancel Update</button>
                                          </li> 
                      </ul>
                   
                    <div class="clearfix"></div>
                  </div>
                  <form role="form" id="frmChangeCommision" name="frmChangeCommision" > 
  @csrf
<input type="hidden" value="{{ $session->get('white_label_id') }}" name="white_label_id">
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                       
                    </p>
    @foreach ($services as $srv)                  
<div class="widget-box">
    <div class="widget-header widget-header-flat">
        <h4 class="widget-title"> {{ $srv['service'][1] }} </h4>
    </div>
 @foreach ($srv['product'] as $prd)
    <div class="widget-body">
        <div class="widget-main"> 
            <div class="row" style="padding-left: 45px;">
                <div class="col-xs-12">
                    <div class="well"> <table>
                      <tr>
                        <td width="55">Produk</td><td>: {{ $prd[2] }}</td>
                      </tr> <tr>
                        <td width="55">ID Produk</td><td>: {{ $prd[1] }}</td>
                      </tr>
                    <tr>
                      <td width="85">Label Fee </td><td>: 
                        <select name="labeltype[]" id="labeltype[]" >
                            <option value="{{$prd[3]}}">{{$prd[3]}}</option>
                            <option value="FIXED">FIXED</option>
                            <option value="PERCENTAGE">PERCENTAGE</option>
                        </select> <input type="text" name="labelvalue[]"  value="{{$prd[4]}}"> 
                    <input type="text" name="labelcode[]"  value="{{$prd[1]}}">
                </td>
                    </tr> <tr>
                      <td width="85">MA Fee </td><td>:  <select name="matype[]" id="matype[]" >
                            <option value="{{$prd[5]}}">{{$prd[5]}}</option>
                            <option value="FIXED">FIXED</option>
                            <option value="PERCENTAGE">PERCENTAGE</option>
                        </select> <input type="text" name="mavalue[]" id="mavalue[]"  value="{{$prd[6]}}"></td>
                    </tr>
                    </table>
                    </div>
                      
                       <table id="datatable-buttons" class="table table-striped table-bordered">
                                          <thead>
                                            <tr>
                                              <th>Produk code</th>
                                              <th>Produk Name</th>
                                              <th>Whitelabel</th> 
                                              <th>MasterAgent</th> 
                                            </tr>
                                          </thead> 
                                          <tbody>
                                            @foreach ($prd[8] as $brk)
                                            <tr>
                                                <td width="35"> {{ $brk[2] }} </td> 
                                                <td width="100"> {{ $brk[3] }} </td>
                                                <td width="35"><select name="brklabeltype[]" id="brklabeltype[{!!$brk[2]!!}][]" >
                            <option value="{{$brk[4]}}">{{$brk[4]}}</option>
                            <option value="FIXED">FIXED</option>
                            <option value="PERCENTAGE">PERCENTAGE</option>
                        </select> <input type="text" name="brklabelval[]"  value="{{$brk[5]}}"><input type="text" name="brklabelcode[]"  value="{{$brk[2]}}">
                           </td>
                                                <td width="35"><select name="brkmatype[{!!$brk[2]!!}][]" id="brkmatype[{!!$brk[2]!!}][]" >
                            <option value="{{$brk[6]}}">{{$brk[6]}}</option>
                            <option value="FIXED">FIXED</option>
                            <option value="PERCENTAGE">PERCENTAGE</option>
                        </select> <input type="text" name="brkmaval[]"  value="{{$brk[7]}}"><input type="text" name="brkmacode[]"  value="{{$brk[2]}}"></td>
                                            </tr>
                                            @endforeach  
                                        </tbody>
                                    </table>
                </div>
            </div>
        </div> 
    </div> 
@endforeach 
@endforeach     
</div>
</div>

<div class="row">
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <button type="button" id="canceledit"  onclick="javascript:showCommisionList();"  class="btn btn-primary">Cancel</button>
        <button type="submit" class="btn btn-success">Submit</button>
        </div>
    </div>
</div> 

</form>
  
<script type="text/javascript">
    

      function showCommisionList(id)
      {
       // alert("test");
        $("#formupdtComs").addClass('hideform');
        $("#tablelistfee").removeClass('hideform');
        $.ajax({
                      type: "GET",
                      url: "commision.html",                     
                      success: function(msg) {
                       $("#tablelistfee").html(msg);                                                            
                      }
                  });


       } 

   $("#frmChangeCommision").on('submit', function(e) {
    e.preventDefault();
    var data = $("#frmChangeCommision").serialize();
    $.ajax({
        type: "post",
        url: "{{ secure_url('/saveupdatecommision.html') }}",
        data: data,
        dataType: "json",
        success: function(data) {
            console.log('success');
        },
        error: function(error) {
            console.log('error');
        }
    });
});

    function doEdit() {        
        sendForm3({
            method: 'POST',
            form: 'frm',
            messageView: 'msg',            
            messageSuccess: 'Service Update Saved',
            url: '{{ secure_url('/saveupdatecommision.html') }}',
            redirect:  '/admin/commision.html',
            beforeSubmit: function () {
                
                beforeSubmit();
            },
            afterSubmit: afterSubmit
        });
    }
 
    function beforeSubmit() {
        $('#button-close').attr('disabled', 'disabled');

        $('#button-save').attr('disabled', 'disabled');
        $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Saving...');
    }

    function afterSubmit() {
        $('#button-close').removeAttr('disabled');

        $('#button-save').removeAttr('disabled');
        $('#button-save').html('<i class="glyphicon glyphicon-save"></i> Save');
    }


</script>