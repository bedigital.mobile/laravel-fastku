@extends('admin.main_agent.layout')

@section('content')


<script src="https://cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>


  
      <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css">
     


<style type="text/css">

html,body{
           
            background-size: cover;
            background-repeat: repeat;
            height: 100%;
            }

td{
  max-width: 100px;
  max-height: 50px;
  overflow: hidden;
}

th {
  font-size: 10px;
}

.sticky-col {
    position: sticky;
    position: -webkit-sticky;    
    background-color: white;
}

.first-col {
  width: 100px;
  left: 0px;    
}

.table-filter {
  position: relative;
  font-family: Arial;
}
.table-filter select {
  display: none; /*hide original SELECT element:*/
}
.select-selected {
  background-color: DodgerBlue;
}
/*style the arrow inside the select element:*/
.select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 10px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
}
/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
}
/*style the items (options), including the selected item:*/
.select-items div,.select-selected {
  color: #ffffff;
  padding: 8px 16px;
  border: 1px solid transparent;
  border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
  cursor: pointer;
}
/*style items (options):*/
.select-items {
  position: absolute;
  background-color: DodgerBlue;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}
/*hide the items when the select box is closed:*/
.select-hide {
  display: none;
}
.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
}

 .btn_downlaod {
    background-color: DodgerBlue;
    border: none;
    color: white;
    padding: 5px 10px;
    cursor: pointer;
    font-size: 15px;
    width: 10%;
    margin-left: 40%;
    margin-right: 40%;
    margin-bottom: 15px;
    }

    .btnClick{
    background-color: rgb(7, 55, 99);
    color: white;
    border: none;
    cursor: pointer;
    padding: 2px 12px 3px 12px;
    text-decoration: none;
    }

    /* Darker background on mouse-over */
    .btn_downlaod:hover {
    background-color: RoyalBlue;

    .btn-group button {
    background-color: #RoyalBlue; /* Green background */
    border: 1px solid green; /* Green border */
    color: white; /* White text */
    padding: 10px 24px; /* Some padding */
    cursor: pointer; /* Pointer/hand icon */
    float: left; /* Float the buttons side by side */
    }

    /* Clear floats (clearfix hack) */
    .btn-group:after {
        content: "";
        clear: both;
        display: table;
    }

    .btn-group button:not(:last-child) {
        border-right: none; /* Prevent double borders */
    }

    /* Add a background color on hover */
    .btn-group button:hover {
        background-color: #3e8e41;
    }
        }

    .dataTables_filter, .dataTables_info { display: none; }
    legendfieldset{

    display: block;
    /* width: 100%; */
    max-width: 100%;
    padding: 2px;
    margin-bottom: .5rem;
    /* font-size: 1.5rem; */
    line-height: inherit;
    color: inherit;
    white-space: normal;
    border:1px solid #DEDEDE;
    }
    fieldset
    {

    border:1px solid #DEDEDE;
    }
    legend{
    display: block;
    width: auto;
    margin-left: .5rem;
    max-width: 100%;
    /* padding: 0 0 0 15px; */
    margin-bottom: .5rem;
    font-size: 1rem;
    line-height: inherit;
    color: inherit;
    white-space: normal;
    padding: 5px;
    }
</style>
    <div class="row-fluid">
        <div class="clearfix"></div>    
        <ul class="breadcrumb">
            <li>Log Report Amanah </li> 
        </ul> 


<div class="row">  
<div class="box col-md-12">
<div class="box-inner"  style="background-color: #fff">
<div class="box-header well" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
<h2><i class="glyphicon glyphicon-tasks"></i> Search</h2>
<div class="box-icon"> 
  <a href="#" class="btn btn-minimize btn-round btn-default" ><i class="fa fa-angle-down "></i></a>
</div>
</div> 

<div class="collapse box-content" id="collapseExample">
<form name="frmhistorydagen" id="frmhistorydagen"  method="get" action="/amanah/report"  onSubmit="return submitcheck(this);" >

    <input type="hidden" id="pg" name="pg" value="1">      
 

<div class="row">
  <div class="col-sm-6">
    <fieldset>
    <legend>Time Range</legend>
      <div style="padding:0.50rem 0 0.25rem 1.25rem" >   
        <input type="radio" name="opsidates" value="month" checked >               <label style="font-size: 15px" for="from"></label>
          <select   name="mnt" id="mnt" style="width: 150px;color: #6e707e;background-color: #fff;background-clip: padding-box;border: 1px solid #d1d3e2;border-radius: .35rem;">
          @foreach ($listmnth as $key => $rowdatess)
          <option value="{{ $key }}">{{ $rowdatess }}</option>
          @endforeach
          </select>
          <select  name="yr" id="yr" 
          style="width: 100px;color: #6e707e;background-color: #fff;background-clip: padding-box;border: 1px solid #d1d3e2;border-radius: .35rem;">
          @foreach ($listyear as $rowdatess)
          <option value="{{$rowdatess}}">{{$rowdatess}}</option>
          @endforeach
        </select> 
      </div>
       <div style="padding:1.50rem 0 1.25rem 1.25rem"> 

          <input type="radio" name="opsidates" value="range"> 

           <select  name="fromdates" id="fromdates" 
          style="width: 45px;color: #6e707e;background-color: #fff;background-clip: padding-box;border: 1px solid #d1d3e2;border-radius: .35rem;">
          @foreach ($listfrom as $rowdatess)
          <option value="{{$rowdatess}}">{{$rowdatess}}</option>
          @endforeach

          </select>

          to 
          <select  name="todates" id="todates" 
          style="width: 45px;color: #6e707e;background-color: #fff;background-clip: padding-box;border: 1px solid #d1d3e2;border-radius: .35rem;">

          @foreach ($listfrom as $rowdatess)
          <option value="{{$rowdatess}}">{{$rowdatess}}</option>
          @endforeach

          </select>
          {{$months}} {{$years}} 
 

       </div>



    </fieldset>
  </div>
  <div class="col-sm-6">
   <fieldset>
    <legend>Search</legend>
      <div style="padding:0.50rem 0 0.25rem 1.25rem"> 

              <div class="row">
              <div class="form-group col-md-4">  

              <label style="font-size: 15px" for="from">Option</label>
              <select class="form-control" style="font-size: 15px" name="tableselect" id="tableselect">
              <option value="" selected="selected"> </option>
              <option value="trx_id">Transaction Id</option>
              <option value="biller_name">Biller Name</option>
              <option value="product_id"> Product Id</option>
              <option value="product_code"> Product Code</option>
              <option value="product_name"> Product Name</option>
              <option value="product_denom"> Denom</option>
              <option value="nominal">Nominal</option>
              <option value="info1">Info 1</option> 
              <option value="info2">Info 2</option> 
              <option value="info3">Info 3</option>  
              </select>

              <label style="font-size: 15px" for="from">Option2</label>
              <select class="form-control" style="font-size: 15px" name="tableselect2" id="tableselect2">
              <option value="" selected="selected"> </option>
              <option value="trx_id">Transaction Id</option>
              <option value="biller_name">Biller Name</option>
              <option value="product_id"> Product Id</option>
              <option value="product_code"> Product Code</option>
              <option value="product_name"> Product Name</option>
              <option value="product_denom"> Denom</option>
              <option value="nominal">Nominal</option>
              <option value="info1">Info 1</option> 
              <option value="info2">Info 2</option> 
              <option value="info3">Info 3</option>   
              </select>
              </div> 
              <div class="form-group col-md-4"> 
              <label style="font-size: 15px" for="from">Text</label>
              <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="search" id="search" placeholder="search by selected">

              <label style="font-size: 15px" for="from">Text2</label>
              <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="search2" id="search2" placeholder="search by selected">

              </div>


              </div>  
      </div>
    </fieldset> 
      </div>
</div> 

<div class="row">
      <div class=" col-md-6">
          <input style="padding: 5px" type="submit" class="btn btn-primary" value="Search">
         &nbsp; <a class="btn btn-default" href="/amanah/report"><i class="fa fa-sync-alt"></i> Refresh</a>
       </div>
</div> 

      </form>
    </div>






</div>
</div>
</div>

<br>
<br>
        <div style="overflow-x:auto;">

        <table id="x-table" class="table table-striped table-bordered">  
          <tr>
            <td align="center">
          No Records Found...
        </td>
      </tr>
    </table>
          </div>
          </div>

            











@endsection