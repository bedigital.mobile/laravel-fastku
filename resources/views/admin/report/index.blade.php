@extends('admin.report.layout')

@section('content')
    <div>
        <ul class="breadcrumb">
            <li><a href="{{ url('/h2h//home.html') }}">Home</a></li>
            <li>Reports</li>
        </ul>
    </div>

    <div class="row-fluid">
        <div class="col-md-3 col-sm-3 col-xs-6">
            <a class="well top-block" href="{{ url('/report_trx.html') }}">
                <i class="glyphicon glyphicon-file blue"></i>

                <div>Transaction Report</div>
                <div>&nbsp;</div>
            </a>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-6">
            <a class="well top-block" href="{{ url('/report_trx.html') }}">
                <i class="glyphicon glyphicon-file green"></i>

                <div>Balance Report</div>
                <div>&nbsp;</div>
            </a>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-6">
            <a class="well top-block" href="{{ url('/report_trx.html') }}">
                <i class="glyphicon glyphicon-file yellow"></i>

                <div>Commission Report</div>
                <div>&nbsp;</div>
            </a>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-6">
            <a class="well top-block" href="{{ url('/report_trx.html') }}">
                <i class="glyphicon glyphicon-file orange"></i>

                <div>Settlement Report</div>
                <div>&nbsp;</div>
            </a>
        </div>
    </div>
@endsection