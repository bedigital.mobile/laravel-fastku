@extends('admin.main_agent.layout')

@section('content')


  <link href="admin_template/css/input.css" rel="stylesheet"> 
    <div class="row-fluid">
       
        <div id="msginfo"></div>
       
      <form name="frmupdateAgentHD" role="form" id="frmupdateAgentHD"  onsubmit="doEditMAgent();return false;" >
        


          <!-- Content Row -->
       

              <!-- Overflow Hidden -->
              <div class="card mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Update Main Agent</h6>
                </div>
                <div class="card-body">
          
<div class="row">
    <div class="col">
        <div class="form-group ">
                        <label for="email">Main Agent ID </label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <span class="form-control" id="master_agent_id" name="master_agent_id">{{ $rs->master_agent_id }}
                                <input type="hidden"  class="form-control" id="master_agent_id" name="master_agent_id" value="{{ $rs->master_agent_id }}"></span>

                        </div>
        </div>
    </div>
    <div class="col">
        <div class="form-group ">
                        <label for="email">Main Agent Name </label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>                            
                            <input type="text"  class="form-control" id="fullname" name="fullname" value="{{ $rs->fullname }}">
                        </div>
        </div>
    </div>

    <div class="w-100"></div>
 <div class="col">
    <div class="form-group ">
                        <label for="email"> Master Agent Upline</label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-tags"></i></span>                            
                            <select class="form-control" id="upline_master_agent_id" name="upline_master_agent_id">
                                <option value="{{ $rs->master_agent_parent_id }}">{{ $rs->master_agent_parent_id }} </option>
                                    @foreach( $ma_list as $rowma)
                                        <option value="{{ $rowma->master_agent_id }}">{{ $rowma->master_agent_id }} - {{ $rowma->m_agent_name }} </option>
                                    @endforeach

                            </select>
                        </div>
                    </div>
    </div>
 <div class="col">
      <div class="form-group">
                        <label for="email"> Email</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span> 
                            <input type="text"  class="form-control" id="email" name="email" value="{{ $rs->email }}">
                        </div>
                    </div>
 </div>
 <div class="w-100"></div>
 <div class="col">
      <div class="form-group">
                        <label for="email"> Phone Number</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                            <input type="text"  class="form-control" id="phone_number" name="phone_number" value="{{ $rs->phone_number }}">                            
                        </div>
                    </div>                     
 </div>
 <div class="col">
      <div class="form-group">
                        <label for="email"> MA Status</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-cog"></i></span>
                            <select class="form-control" name="status" id="status" required="">
                                <option value="{{ $rs->status }}">{{ $rs->status }} </option>
                                    @foreach( $status_list as $rowma)
                                        <option value="{{ $rowma }}">{{ $rowma }} </option>
                                    @endforeach

                            </select>
                         </div>
                    </div> 
 </div> 
 <div class="w-100"></div>
 <div class="col"></div>
 <div class="col">
                     <div class="form-actions">

                        <a href="javascript:getContentInBox('{{ url('/ma/m_agent.html') }}');" class="btn btn-warning btn-sm" id="button-close"><i class="glyphicon glyphicon-arrow-left"></i> Close</a>
                                <input type="submit" class="btn btn-info btn-sm" name="sbmt_upd" id="sbmt_upd" value="Save">

                    </div>
</div>
<div class="col"></div>
                </div>
                </div>
              </div>
 
    </form>
    </div>

<script type="text/javascript">
    
  
    function beforeSubmit() {
        $('#button-close').attr('disabled', 'disabled');
        $('#sbmt_upd').attr('disabled', 'disabled');
        $('#sbmt_upd').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Saving...');
    }

    function afterSubmit() {
        $('#button-close').removeAttr('disabled');

        $('#sbmt_upd').removeAttr('disabled');
        $('#sbmt_upd').html('<i class="glyphicon glyphicon-save"></i> Save');
    }



    function doEditMAgent() {     
    if (confirm('Are you sure to update?')) {
     
        sendForm2({
            method: 'POST',
            form: 'frmupdateAgentHD',
            messageView: 'msginfo',            
            messageSuccess: 'Master Agent Update Saved',
            url: '{{ secure_url('/ma/update_m_agent_save.html') }}',
            redirect:  '/ma/m_agent.html',
            beforeSubmit: function () {                
                beforeSubmit();
            },
            afterSubmit: afterSubmit
        });
    }
    }
</script>
    <!-- @include('admin.utils.image_upload') -->
 
@endsection