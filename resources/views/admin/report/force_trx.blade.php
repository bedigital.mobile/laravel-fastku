@extends('admin.main_agent.layout')

@section('content')
<?php $no=1;?>
<style type="text/css">

html,body{
            background-image: url('http://getwallpapers.com/wallpaper/full/c/1/4/60124.jpg');
            background-size: cover;
            background-repeat: repeat;
            height: 100%;
            }

td{
  max-width: 100px;
  max-height: 50px;
  overflow: hidden;
  background-color: #fff;
}

th {
  font-size: 10px;
}

#x-table th {
  font-size: 10px;
  text-align: center;
    vertical-align: middle;
}

.sticky-col {
    position: sticky;
    position: -webkit-sticky;    
    background-color: white;
}

.first-col {
  width: 100px;
  left: 0px;    
}

.table-filter {
  position: relative;
  font-family: Arial;
}
.table-filter select {
  display: none; /*hide original SELECT element:*/
}
.select-selected {
  background-color: DodgerBlue;
}
/*style the arrow inside the select element:*/
.select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 10px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
}
/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
}
/*style the items (options), including the selected item:*/
.select-items div,.select-selected {
  color: #ffffff;
  padding: 8px 16px;
  border: 1px solid transparent;
  border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
  cursor: pointer;
}
/*style items (options):*/
.select-items {
  position: absolute;
  background-color: DodgerBlue;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}
/*hide the items when the select box is closed:*/
.select-hide {
  display: none;
}
.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
}

 .btn_downlaod {
    background-color: DodgerBlue;
    border: none;
    color: white;
    padding: 5px 10px;
    cursor: pointer;
    font-size: 15px;
    width: 10%;
    margin-left: 40%;
    margin-right: 40%;
    margin-bottom: 15px;
    }

    .btnClick{
    background-color: rgb(7, 55, 99);
    color: white;
    border: none;
    cursor: pointer;
    padding: 2px 12px 3px 12px;
    text-decoration: none;
    }

    /* Darker background on mouse-over */
    .btn_downlaod:hover {
    background-color: RoyalBlue;

    .btn-group button {
    background-color: #RoyalBlue; /* Green background */
    border: 1px solid green; /* Green border */
    color: white; /* White text */
    padding: 10px 24px; /* Some padding */
    cursor: pointer; /* Pointer/hand icon */
    float: left; /* Float the buttons side by side */
    }

    /* Clear floats (clearfix hack) */
    .btn-group:after {
        content: "";
        clear: both;
        display: table;
    }

    .btn-group button:not(:last-child) {
        border-right: none; /* Prevent double borders */
    }

    /* Add a background color on hover */
    .btn-group button:hover {
        background-color: #3e8e41;
    }
        }

    .dataTables_filter, .dataTables_info { display: none; }
</style>
    <div class="row-fluid">
        <div class="clearfix"></div>    
        <ul class="breadcrumb">
            <li><a href="{{ secure_url('/agency/report_agency.html') }}">Log Report Agent</a></li> 
            <li>List Force Transaction</li>
            <!-- <li><a href="{{ secure_url('/agency/refund_pending_trx.html') }}">Refund Pending</a></li>
            <li><a href="{{ secure_url('/agency/recon_info.html') }}">Recon Info</a></li>
            <li><a href="{{ secure_url('/agency/agent_id.html') }}">Agent Id</a></li> -->
            
            <li style="float: right;">{{ $session->get('email') }}</li>
        </ul>
<div id="info"></div>


<div class="row">  
<div class="box col-md-6">
<div class="box-inner"  style="background-color: #fff">
<div class="box-header well" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
<h2><i class="glyphicon glyphicon-tasks"></i> Search</h2>
<div class="box-icon"> 
  <a href="#" class="btn btn-minimize btn-round btn-default" ><i class="glyphicon glyphicon-chevron-down"></i></a>
</div>
</div>
<div class="collapse box-content" id="collapseExample">
<form action="/agency/pending_trx.html" method="get" name="frms">
<div class="row">
  <div class="form-group col-md-4">  
    <label style="font-size: 15px" for="from">From</label>
    <input type="hidden" id="pg" name="pg" value="1">
    <input style="font-size: 15px" class="form-control" type="date" id="min" name="min">         
    <label style="font-size: 15px" for="from">Option</label>
    <select class="form-control" style="font-size: 15px" name="tableselect" id="tableselect">
    <option value="" selected="selected"> </option>
    <option value="partner_id">Partner Id</option>
    <option value="agent_id">Agent Id</option>
    <option value="trx_type">Transaction Type</option>
    <option value="recon_code">Recon Code</option>
    <option value="product_billnumber_paymentcode_denom">Pending Product Trx</option>
    </select>
</div> 
<div class="form-group col-md-4">
            <label style="font-size: 15px" for="to">to</label>
            <input style="font-size: 15px" class="form-control" type="date" id="max" name="max">
            <label style="font-size: 15px" for="from">Text</label>
            <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="search" id="search" placeholder="search by selected">
</div>
</div>     

<div class="row">
      <div class=" col-md-6">
          <button style="padding: 5px" type="submit" class="btn btn-primary">Search</button>
         &nbsp; <a class="btn btn-default" href="/agency/pending_trx.html"><i class="glyphicon glyphicon-refresh"></i> Refresh</a>
       </div>
</div>



      </form>
    </div>
</div>
</div>
</div>
 <br>
 <br>

        <table id="x-table" class="table table-striped table-bordered">  
              <!--   <thead>
                    <tr>
                        <th class="center" width="1%">No.</th>
                        <th width="8%">{!! $sorter->field(0) !!}</th>
                        <th width="8%">{!! $sorter->field(1) !!}</th>
                        <th width="8%">{!! $sorter->field(2) !!}</th>
                        <th width="8%">{!! $sorter->field(3) !!}</th>
                        <th width="8%">{!! $sorter->field(4) !!}</th>
                        <th width="8%">{!! $sorter->field(5) !!}</th>
                        <th width="8%">{!! $sorter->field(6) !!}</th>
                        <th width="8%">{!! $sorter->field(7) !!}</th>
                        <th width="8%">{!! $sorter->field(8) !!}</th>
                        <th width="8%">{!! $sorter->field(9) !!}</th>
                        <th width="8%">{!! $sorter->field(10) !!}</th>
                        <th width="8%">{!! $sorter->field(11) !!}</th>
                        <th width="8%">{!! $sorter->field(12) !!}</th>
                        <th width="8%">{!! $sorter->field(13) !!}</th>
                        <th width="8%">{!! $sorter->field(14) !!}</th>
                        <th width="8%">{!! $sorter->field(15) !!}</th>
                        <th width="8%">{!! $sorter->field(16) !!}</th>
                     </tr>
                </thead> -->
                <tbody>
                    @if ($sorter->rowCount() == 0)
                    <tr>
                        <td colspan="6" align="center">No Records Found...</td>
                    </tr>
                    @else
                        @foreach ($sorter->pageRows() as $row)
                        <input type="hidden" id="alldata{{$no}}" value="{{$row['product_billnumber_paymentcode']}}">
                    <tr>
                        <td class="center" rowspan="5">{{ $sorter->skippedRows = $sorter->skippedRows + 1 }}.</td>
                        <td>{!! $sorter->field(0) !!}</td> <td>{{$row['created_datetime']}}</td>   
                    </tr>
                    <tr>
                        <td>{!! $sorter->field(3) !!}</td>  <td id="product{{$no}}"></td>

                        <td>{!! $sorter->field(4) !!}</td>  <td id="bill{{$no}}"></td>
                        <td>{!! $sorter->field(5) !!}</td>  <td colspan="3" id="paymentcode{{$no}}"></td>
                       
                    </tr>
                    <tr>                        
                          <td>{!! $sorter->field(6) !!}</td>  <td id="denom{{$no}}"></td>
                         <td>{!! $sorter->field(9) !!}</td>  <td>{{$row['trx_price_fee_type']}}</td> 
                         <td>{!! $sorter->field(10) !!}</td> <td>{{$row['trx_type']}}</td>
                       
                         <td>{!! $sorter->field(15) !!}</td>  <td>{{$row['summary_status']}}</td>

                    </tr>
                    <tr>
                       
                        <td>{!! $sorter->field(11) !!}</td>  <td>{{$row['recon_code']}}</td>
                        <td>{!! $sorter->field(12) !!}</td>  <td>{{$row['recon_desc']}}</td>
                         <td>{!! $sorter->field(13) !!}</td> <td>{{$row['recon_status']}}</td>
                         <td>{!! $sorter->field(14) !!}</td> <td>{{$row['confirmation_status']}}</td>
                   </tr>
                   <tr>
                         <td>{!! $sorter->field(1) !!}</td> <td colspan="3">{{$row['product_billnumber_paymentcode']}}</td> 
                         <td>{!! $sorter->field(16) !!}</td> <td>{{$row['notes']}}</td>
                    </tr>
                    <tr>   
                      <td colspan="9" style="background-color:#3334;text-align: right;" height="20px" align="left">
                       <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"  onclick="setID('{{$row['id']}}');">Duplicate Trx</a>  
                       <a href="#"  class="btn btn-warning btn-sm" onclick="allocateAct('{{$row['id']}}' );">Allocate to H2H</a>  
                       <a href="#"  class="btn btn-danger  btn-sm" onclick="forceAct('{{$row['id']}}','{{$row['created_datetime']}}','{{$row['product_billnumber_paymentcode']}}' );">Force Trx (Agency)</a>
                      </td>
                    </tr>
                    <script type="text/javascript">
                      
                      $(document).ready(function () {
                              var result = $("#alldata{{$no}}").val().split('#');
                             
                              $("#product{{$no}}").text(result[0])
                              $("#bill{{$no}}").text(result[1])
                              $("#paymentcode{{$no}}").text(result[2])
                              $("#denom{{$no}}").text(result[3])
                              });

                    </script>
                    <?php $no++;?>
                        @endforeach
                    @endif
                </tbody>
            </table>
            <div class="row-fluid col-md-12 pull-right">{!! $sorter->pagination() !!}</div>
        </div>                           

      <a href="pending_help.html?min={{$min}}&max={{$max}}&tableselect={{$tableselect}}&search={{$search}}" target="_blank">export to excel</a>
 

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <label style="font-size: 15px" for="to">Notes</label><textarea class="form-control autogrow" id="notes_trx" name="notes_trx" ></textarea> 
            
                     <input type="hidden" name="id_trx" id="id_trx" >
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal" onclick="dupliAct();">Save changes</a>
                </div>
            </div>
        </div>
    </div>



    </div>
</div>

    <script>
    
$(".use-address").click(function () {
  var id = $(this).closest("tr").find(".use-address").text();
  alert(id);
});
    
    function setID(id)
    {
      $("#id_trx").val(id); $("#notes_trx").val('');
    }
    function dupliAct()
    {
      var id = $("#id_trx").val();
      var notes = $("#notes_trx").val();
// console.log('id = '+id+' notes='+notes);

      if (confirm('Are you sure to Process Duplicate?')) { 
         $.ajax({
            type: "post",
            url: "{{ secure_url('/agency/act_duplicate') }}",
            data:  {id:id,notes:notes }, 
            dataType: "json", cache    : false,
            success: function(data) {
                // console.log(data); 
                alert("Duplicate Success");
                  window.location.assign('force_trx.html'); 

            },
            error: function(error) { 
                console.log('error');alert("Error");
            }
        });  
     } 
  
 
    }

     function allocateAct(id)
    {
      

      if (confirm('Are you sure to Process Allocate to H2H?')) { 
         $.ajax({ 
            type: "post",
            url: "{{ secure_url('/agency/act_allocate') }}",
            data:  {id:id  }, 
            dataType: "json", cache    : false,
            success: function(data) {
                // console.log(data);
                alert("Allocate to H2H Success");
                  window.location.assign('force_trx.html');
             },
            error: function(error) {
                console.log('error');alert("Error");
            }
        }); 
       } 
    } function forceAct(id,time,code)
    {
      

      if (confirm('Are you sure to Process Force Trx?')) { 
         $.ajax({ 
            type: "post",
            url: "{{ secure_url('/agency/act_force') }}",
            data:  {id:id, time:time, code:code  }, 
            dataType: "json", cache    : false,
            success: function(data) {
                $("#info").html("<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'>×</button><strong>"+data.status+"</strong><br>"+data.code+" - "+data.message+"</div>");                
 setTimeout(function(){
                      window.location.assign('force_trx.html');

},10);
             },
            error: function(error) {
                console.log('error');alert("Error");
            }
        }); 
       } 
    }



</script>
@endsection