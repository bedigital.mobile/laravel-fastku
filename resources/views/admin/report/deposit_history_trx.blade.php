@extends('admin.main_agent.layout')

@section('content')

<style type="text/css">
html,body{
            background-image: url('http://getwallpapers.com/wallpaper/full/c/1/4/60124.jpg');
            background-size: cover;
            background-repeat: repeat;
            height: 100%;
            }

td{
  max-width: 100px;
  max-height: 50px;
  overflow: hidden;
}

th {
  font-size: 10px;
}

.sticky-col {
    position: sticky;
    position: -webkit-sticky;    
    background-color: white;
}

.first-col {
  width: 100px;
  left: 0px;    
}

.table-filter {
  position: relative;
  font-family: Arial;
}
.table-filter select {
  display: none; /*hide original SELECT element:*/
}
.select-selected {
  background-color: DodgerBlue;
}
/*style the arrow inside the select element:*/
.select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 10px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
}
/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
}
/*style the items (options), including the selected item:*/
.select-items div,.select-selected {
  color: #ffffff;
  padding: 8px 16px;
  border: 1px solid transparent;
  border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
  cursor: pointer;
}
/*style items (options):*/
.select-items {
  position: absolute;
  background-color: DodgerBlue;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}
/*hide the items when the select box is closed:*/
.select-hide {
  display: none;
}
.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
}

 .btn_downlaod {
    background-color: DodgerBlue;
    border: none;
    color: white;
    padding: 5px 10px;
    cursor: pointer;
    font-size: 15px;
    width: 10%;
    margin-left: 40%;
    margin-right: 40%;
    margin-bottom: 15px;
    }

    .btnClick{
    background-color: rgb(7, 55, 99);
    color: white;
    border: none;
    cursor: pointer;
    padding: 2px 12px 3px 12px;
    text-decoration: none;
    }

    /* Darker background on mouse-over */
    .btn_downlaod:hover {
    background-color: RoyalBlue;

    .btn-group button {
    background-color: #RoyalBlue; /* Green background */
    border: 1px solid green; /* Green border */
    color: white; /* White text */
    padding: 10px 24px; /* Some padding */
    cursor: pointer; /* Pointer/hand icon */
    float: left; /* Float the buttons side by side */
    }

    /* Clear floats (clearfix hack) */
    .btn-group:after {
        content: "";
        clear: both;
        display: table;
    }

    .btn-group button:not(:last-child) {
        border-right: none; /* Prevent double borders */
    }

    /* Add a background color on hover */
    .btn-group button:hover {
        background-color: #3e8e41;
    }
        }

    .dataTables_filter, .dataTables_info { display: none; }
</style>
    <div class="row-fluid">
        <div class="clearfix"></div>    
        <ul class="breadcrumb">
            <!-- <li><a href="{{ secure_url('/ma/report_agency.html') }}">Log Report Agent</a></li> -->
            <li>History Master Agent</li> 
            <!-- <li style="float: right;">{{ $session->get('email') }}</li> -->
        </ul>


 <div class="row">   
<div class="box col-md-6">
<div class="box-inner"  style="background-color: #fff">
<div class="box-header well" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
<h2><i class="glyphicon glyphicon-tasks"></i> Search</h2>
<div class="box-icon"> 
  <a href="#" class="btn btn-minimize btn-round btn-default" ><i class="fa fa-angle-down"></i></a>
</div>
</div>
<div class="collapse box-content" id="collapseExample">
<form name="frmhistorydagen" id="frmhistorydagen"  method="get" action="/ma/history_trx.html" >
<div class="row">
  <div class="form-group col-md-4">  
    <label style="font-size: 15px" for="from">From</label>
    <input type="hidden" id="pg" name="pg" value="1">
    <input style="font-size: 15px" class="form-control" type="date" id="min" name="min" max="{{$today}}">         
    <label style="font-size: 15px" for="from">Option</label>
    <select class="form-control" style="font-size: 15px" name="tableselect" id="tableselect">
     <option value="" selected="selected"> </option>
            <option value="id">Master Agent Id</option>
             <option value="fullname">Fullname</option>
             <option value="code">Trx Code</option>
             <option value="mode">Trx Mode</option>
            <option value="type">Trx Type</option>
            <option value="description">Deskripsi</option> 
     </select>
</div> 
<div class="form-group col-md-4">
            <label style="font-size: 15px" for="to">to</label>
            <input style="font-size: 15px" class="form-control" type="date" id="max" name="max" max="{{$today}}">
            <label style="font-size: 15px" for="from">Text</label>
            <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="search" id="search" placeholder="search by selected">
</div>
</div>     

<div class="row">
      <div class=" col-md-6">
          <input style="padding: 5px" type="submit" class="btn btn-primary" value="Search">
         &nbsp; <a class="btn btn-default" href="/ma/history_trx.html"><i class="fa fa-sync-alt"></i> Refresh</a>
       </div>
</div>



      </form>
    </div>
</div>
</div>
</div>
 <br>
 <br>
<div style="overflow-x:auto;">
        <table id="x-table" class="table table-striped table-bordered">  
              <thead>
                    <tr style="height: 10%">
                        <th class="center" width="1%">No.</th>
                        <th width="8%">{!! $sorter->field(0) !!}</th>
                        <th width="8%">{!! $sorter->field(1) !!}</th>
                        <th width="8%">{!! $sorter->field(2) !!}</th>
                        <th width="8%">{!! $sorter->field(3) !!}</th>
                        <th width="8%">{!! $sorter->field(4) !!}</th>
                        <th width="8%">{!! $sorter->field(5) !!}</th> 
                        <th width="8%">{!! $sorter->field(6) !!}</th> 
                        <th width="8%">{!! $sorter->field(7) !!}</th> 
                        <th width="8%">{!! $sorter->field(8) !!}</th> 
                        <!-- <th width="8%">{!! $sorter->field(6) !!}</th>  -->
                     </tr>
                </thead>   <!-- -->
                <tbody>
                    @if ($sorter->rowCount() == 0)
                    <tr>
                        <td colspan="5" align="center">No Records Found...</td>
                    </tr>
                    @else
                        @foreach ($sorter->pageRows() as $row)
                    <tr>
                      <td class="center">{{ $sorter->skippedRows = $sorter->skippedRows + 1 }}.</td>                       
                      <td>{{$row['trx_datetime']}}</td>  
                      <td>{{$row['master_agent_id']}}</td>                         
                      <td>{{$row['fullname']}}</td>                         
                      <td>{{$row['trx_code']}}</td>  
                      <td>{{$row['trx_mode']}}</td>  
                      <td>{{$row['trx_type']}}</td>  
                      <td>{{$row['description']}}</td>   
                      <td>{{number_format($row['balance_value_before'],0,'.','.')}}</td>  
                      <td>{{number_format($row['balance_value_after'],0,'.','.')}}</td>  
                         <!-- <td>{{$row['amount']}}</td>   -->

                      <tr>
                        <td colspan="10" style="background-color: #ddd;height: 10px"></td> 
                      </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            <div class="row-fluid col-md-12 pull-right">{!! $sorter->pagination() !!}</div>
        </div>
    </div> 
    @if(!empty($min)||!empty($search)||!empty($tableselect))
      <a href="history_trx_help.html?min={{$min}}&max={{$max}}&tableselect={{$tableselect}}&search={{$search}}" target="_blank">export to excel</a>
    @endif
    </div>
</div> 
    <script> 
       // action="/ma/history_agent.html" method="get" 
function submitcheck(element)
{
   
 

if (confirm('Are You Sure')) {
    alert('Thanks for confirming');
    return true;
} else {
alert("The action to execute is " + element.action);
   return false;
}
 }    
 // min="2001-01-02"

     // $('#min').attr('max' , '{{$today}}' );


// $("#frmhistorydagen").submit(function () {
//   alert("oke");
//   return true;
// }


// $(".use-address").click(function () {
//   var id = $(this).closest("tr").find(".use-address").text();
//   alert(id);
// });
    
</script>
@endsection