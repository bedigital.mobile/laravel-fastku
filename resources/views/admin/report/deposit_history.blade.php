@extends('admin.main_agent.layout')

@section('content')

<style type="text/css">
html,body{
            background-image: url('http://getwallpapers.com/wallpaper/full/c/1/4/60124.jpg');
            background-size: cover;
            background-repeat: repeat;
            height: 100%;
            }

td{
  max-width: 100px;
  max-height: 50px;
  overflow: hidden;
}

th {
  font-size: 10px;
}

.sticky-col {
    position: sticky;
    position: -webkit-sticky;    
    background-color: white;
}

.first-col {
  width: 100px;
  left: 0px;    
}

.table-filter {
  position: relative;
  font-family: Arial;
}
.table-filter select {
  display: none; /*hide original SELECT element:*/
}
.select-selected {
  background-color: DodgerBlue;
}
/*style the arrow inside the select element:*/
.select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 10px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
}
/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
}
/*style the items (options), including the selected item:*/
.select-items div,.select-selected {
  color: #ffffff;
  padding: 8px 16px;
  border: 1px solid transparent;
  border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
  cursor: pointer;
}
/*style items (options):*/
.select-items {
  position: absolute;
  background-color: DodgerBlue;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}
/*hide the items when the select box is closed:*/
.select-hide {
  display: none;
}
.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
}

 .btn_downlaod {
    background-color: DodgerBlue;
    border: none;
    color: white;
    padding: 5px 10px;
    cursor: pointer;
    font-size: 15px;
    width: 10%;
    margin-left: 40%;
    margin-right: 40%;
    margin-bottom: 15px;
    }

    .btnClick{
    background-color: rgb(7, 55, 99);
    color: white;
    border: none;
    cursor: pointer;
    padding: 2px 12px 3px 12px;
    text-decoration: none;
    }

    /* Darker background on mouse-over */
    .btn_downlaod:hover {
    background-color: RoyalBlue;

    .btn-group button {
    background-color: #RoyalBlue; /* Green background */
    border: 1px solid green; /* Green border */
    color: white; /* White text */
    padding: 10px 24px; /* Some padding */
    cursor: pointer; /* Pointer/hand icon */
    float: left; /* Float the buttons side by side */
    }

    /* Clear floats (clearfix hack) */
    .btn-group:after {
        content: "";
        clear: both;
        display: table;
    }

    .btn-group button:not(:last-child) {
        border-right: none; /* Prevent double borders */
    }

    /* Add a background color on hover */
    .btn-group button:hover {
        background-color: #3e8e41;
    }
        }

    .dataTables_filter, .dataTables_info { display: none; }

     fieldset
    {

    border:1px solid #DEDEDE;
    }
    legend{
    display: block;
    width: auto;
    margin-left: .5rem;
    max-width: 100%;
    /* padding: 0 0 0 15px; */
    margin-bottom: .5rem;
    font-size: 1rem;
    line-height: inherit;
    color: inherit;
    white-space: normal;
    padding: 5px;
    }
</style>
    <div class="row-fluid">
        <div class="clearfix"></div>    
        <ul class="breadcrumb">
            <!-- <li><a href="{{ secure_url('/ma/report_agency.html') }}">Log Report Agent</a></li> -->
            <li>History Agent</li> 
            <!-- <li style="float: right;">{{ $session->get('email') }}</li> -->
        </ul>


 <div class="row">   
<div class="box col-md-12">
<div class="box-inner"  style="background-color: #fff">
<div class="box-header well" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
<h2><i class="glyphicon glyphicon-tasks"></i> Search</h2>
<div class="box-icon"> 
  <a href="#" class="btn btn-minimize btn-round btn-default" ><i class="fa fa-angle-down"></i></a>
</div>
</div>
<div class="collapse box-content" id="collapseExample">
<form name="frmhistorydagen" id="frmhistorydagen"  method="get" action="/ma/history_agent.html"  onSubmit="return submitcheck(this);" >

    <input type="hidden" id="pg" name="pg" value="1">      
 

<div class="row">
  <div class="col-sm-6">
    <fieldset>
    <legend>Time Range</legend>
      <div style="padding:0.50rem 0 0.25rem 1.25rem" >   
        <input type="radio" name="opsidates" value="month" checked >               <label style="font-size: 15px" for="from"></label>
          <select   name="mnt" id="mnt" style="width: 150px;color: #6e707e;background-color: #fff;background-clip: padding-box;border: 1px solid #d1d3e2;border-radius: .35rem;">
          @foreach ($listmnth as $key => $rowdatess)
          <option value="{{ $key }}">{{ $rowdatess }}</option>
          @endforeach
          </select>
          <select  name="yr" id="yr" 
          style="width: 100px;color: #6e707e;background-color: #fff;background-clip: padding-box;border: 1px solid #d1d3e2;border-radius: .35rem;">
          @foreach ($listyear as $rowdatess)
          <option value="{{$rowdatess}}">{{$rowdatess}}</option>
          @endforeach
        </select> 
      </div>
       <div style="padding:1.50rem 0 1.25rem 1.25rem"> 

          <input type="radio" name="opsidates" value="range"> 

           <select  name="fromdates" id="fromdates" 
          style="width: 45px;color: #6e707e;background-color: #fff;background-clip: padding-box;border: 1px solid #d1d3e2;border-radius: .35rem;">
          @foreach ($listfrom as $rowdatess)
          <option value="{{$rowdatess}}">{{$rowdatess}}</option>
          @endforeach

          </select>

          to 
          <select  name="todates" id="todates" 
          style="width: 45px;color: #6e707e;background-color: #fff;background-clip: padding-box;border: 1px solid #d1d3e2;border-radius: .35rem;">

          @foreach ($listfrom as $rowdatess)
          <option value="{{$rowdatess}}">{{$rowdatess}}</option>
          @endforeach

          </select>
          {{$months}} {{$years}} 
 

       </div>



    </fieldset>
  </div>
  <div class="col-sm-6">
   <fieldset>
    <legend>Search</legend>
      <div style="padding:0.50rem 0 0.25rem 1.25rem"> 

              <div class="row">
              <div class="form-group col-md-4">  

              <label style="font-size: 15px" for="from">Option</label>
              <select class="form-control" style="font-size: 15px" name="tableselect" id="tableselect">
                      <option value="" selected="selected"> </option>
                      <option value="id">Agent Id</option>
                      <option value="username">Username</option>
                      <option value="mode">Trx Mode</option>
                      <option value="type">Trx Type</option>
                      <option value="product_id">Trx Product Id</option>
                      <option value="product_code">Trx Product Code</option>
                      <option value="bill_no">Trx Bill Number</option>
                      <option value="trx_code">Trx Code</option>
                      <option value="desc">Description</option>
              </select>

              <label style="font-size: 15px" for="from">Option2</label>
              <select class="form-control" style="font-size: 15px" name="tableselect2" id="tableselect2">
                      <option value="" selected="selected"> </option>
                      <option value="id">Agent Id</option>
                      <option value="username">Username</option>
                      <option value="mode">Trx Mode</option>
                      <option value="type">Trx Type</option>
                      <option value="product_id">Trx Product Id</option> 
                      <option value="product_code">Trx Product Code</option>
                      <option value="bill_no">Trx Bill Number</option>
                      <option value="trx_code">Trx Code</option>
                      <option value="desc">Description</option>
              </select>
              </div> 
              <div class="form-group col-md-4"> 
              <label style="font-size: 15px" for="from">Text</label>
              <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="search" id="search" placeholder="search by selected">

              <label style="font-size: 15px" for="from">Text2</label>
              <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="search2" id="search2" placeholder="search by selected">

              </div>


              </div>  
      </div>
    </fieldset> 
      </div>
</div> 

<div class="row">
      <div class=" col-md-6">
          <input style="padding: 5px" type="submit" class="btn btn-primary" value="Search">
         &nbsp; <a class="btn btn-default" href="/ma/history_agent.html"><i class="fa fa-sync-alt"></i> Refresh</a>
       </div>
</div> 

      </form> 
    </div>
</div>
</div>
</div>
 <br>
 <br>
<div style="overflow-x:auto;">
        <table id="x-table" class="table table-striped table-bordered">  
               <!--  <thead>
                    <tr style="height: 10%">
                        <th class="center" width="1%">No.</th>
                        <th width="8%">{!! $sorter->field(0) !!}</th>
                        <th width="8%">{!! $sorter->field(1) !!}</th>
                        <th width="8%">{!! $sorter->field(2) !!}</th>
                        <th width="8%">{!! $sorter->field(3) !!}</th>
                        <th width="8%">{!! $sorter->field(4) !!}</th>
                        <th width="8%">{!! $sorter->field(5) !!}</th>
                        <th width="8%">{!! $sorter->field(6) !!}</th>
                        <th width="8%">{!! $sorter->field(7) !!}</th>
                        <th width="8%">{!! $sorter->field(8) !!}</th>
                        <th width="8%">{!! $sorter->field(9) !!}</th>
                        <th width="8%">{!! $sorter->field(10) !!}</th>
                        <th width="8%">{!! $sorter->field(11) !!}</th>
                        <th width="8%">{!! $sorter->field(12) !!}</th>
                        <th width="8%">{!! $sorter->field(13) !!}</th>
                        <th width="8%">{!! $sorter->field(14) !!}</th>
                        <th width="8%">{!! $sorter->field(15) !!}</th>
                     </tr>
                </thead> -->
                <tbody>
                    @if ($sorter->rowCount() == 0)
                    <tr>
                        <td colspan="5" align="center">No Records Found...</td>
                    </tr>
                    @else
                        @foreach ($sorter->pageRows() as $row)
                    <tr>
                      <td class="center" rowspan="7">{{ $sorter->skippedRows = $sorter->skippedRows + 1 }}.</td>
                        
                       <tr> 
                        <td>{!! $sorter->field(2) !!}</td> <td>{{$row['trx_datetime']}}</td> <td>{!! $sorter->field(0) !!}</td> <td>{{$row['agent_id']}}</td>  
                       </tr>
                       <tr> 
                                             
                         <td>{!! $sorter->field(1) !!}</td> <td>{{$row['username']}}</td>                         
                          
                        <td>{!! $sorter->field(3) !!}</td> <td>{{$row['trx_mode']}}</td>                         
                        <td>{!! $sorter->field(4) !!}</td> <td>{{$row['trx_type']}}</td>
                        
                      </tr>
                      <tr> 
                         <td>{!! $sorter->field(8) !!}</td> <td>{{$row['trx_product_id']}}</td>
                         <td>{!! $sorter->field(7) !!}</td> <td>{{$row['trx_product_code']}}</td>   
                         <!-- <td>{!! $sorter->field(14) !!}</td> <td>{{$row['trx_sheet_number']}}</td> -->
                      </tr> 
                      <tr>
                        <td>{!! $sorter->field(10) !!}</td>  <td>Rp. {{number_format ($row['amount'])}}</td>
                        <td>{!! $sorter->field(11) !!}</td>  <td>Rp. {{number_format ($row['balance_value_before'])}}</td>
                        <td>{!! $sorter->field(12) !!}</td>  <td>Rp. {{number_format ($row['balance_value_after'])}}</td>
                      </tr>
                      <tr>
                        <td>{!! $sorter->field(6) !!}</td> <td>{{$row['trx_bill_number']}}</td> 
                        <td>{!! $sorter->field(5) !!}</td>  <td colspan="3">{{$row['trx_code']}}</td> 
                      </tr>
                      <tr>
                       <td>{!! $sorter->field(9) !!}</td> <td colspan="2">{{$row['description']}}</td>
                        
                        <td>{!! $sorter->field(13) !!}</td>  <td colspan="2">{{$row['app_info']}}</td>
                      </tr>
                      <tr>
                        <td colspan="7" style="background-color: #ddd;height: 10px"></td> 
                      </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            <div class="row-fluid col-md-12 pull-right">
              @if($opsidates =='month') 
                Displayed week <span style="color:blue;">{!! $week !!}</span>. 
                Select week :
                  <a href="history_agent.html?opsidates={{$opsidates}}&mnt={{$mnt}}&yr={{$yr}}&fromdates={{$fromdates}}&todates={{$todates}}&tableselect2={{$tableselect2}}&tableselect={{$tableselect}}&search={{$search}}&search2={{$search2}}&week=1">1</a>&nbsp;
                  <a href="history_agent.html?opsidates={{$opsidates}}&mnt={{$mnt}}&yr={{$yr}}&fromdates={{$fromdates}}&todates={{$todates}}&tableselect2={{$tableselect2}}&tableselect={{$tableselect}}&search={{$search}}&search2={{$search2}}&week=2">2</a>&nbsp;
                  <a href="history_agent.html?opsidates={{$opsidates}}&mnt={{$mnt}}&yr={{$yr}}&fromdates={{$fromdates}}&todates={{$todates}}&tableselect2={{$tableselect2}}&tableselect={{$tableselect}}&search={{$search}}&search2={{$search2}}&week=3">3</a>&nbsp;
                  <a href="history_agent.html?opsidates={{$opsidates}}&mnt={{$mnt}}&yr={{$yr}}&fromdates={{$fromdates}}&todates={{$todates}}&tableselect2={{$tableselect2}}&tableselect={{$tableselect}}&search={{$search}}&search2={{$search2}}&week=4">4</a>&nbsp;              
                <br>
              @endif  

              {!! $sorter->pagination() !!}
            </div>
        </div>
    </div> 
  
      @php
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
        $time = $d->format("YmdHisu");
      @endphp

      @if(!empty($opsidates)||!empty($search)||!empty($search2))
          @if($opsidates=="range") 
            <a href="agenthistory_help.html?opsidates={{$opsidates}}&mnt={{$mnt}}&yr={{$yr}}&fromdates={{$fromdates}}&todates={{$todates}}&tableselect2={{$tableselect2}}&tableselect={{$tableselect}}&search={{$search}}&search2={{$search2}}&week={{$week}}&t={{$time}}" target="_blank">export data to excel</a>
          @else
            <a href="agenthistory_help.html?opsidates={{$opsidates}}&mnt={{$mnt}}&yr={{$yr}}&fromdates={{$fromdates}}&todates={{$todates}}&tableselect2={{$tableselect2}}&tableselect={{$tableselect}}&search={{$search}}&search2={{$search2}}&week={{$week}}&t={{$time}}" target="_blank">export week {{$week}} data to excel</a>
          @endif  
      @endif
    </div>
</div> 
    <script> 
       // action="/ma/history_agent.html" method="get" 
function submitcheck(element)
{
   
 

// if (confirm('Are You Sure')) {
//     alert('Thanks for confirming');
//     return true;
// } else {
// alert("The action to execute is " + element.action);
//    return false;
// }
//  }    // min="2001-01-02"

     // $('#min').attr('max' , '{{$today}}' );


// $("#frmhistorydagen").submit(function () {
//   alert("oke");
//   return true;
// }


// $(".use-address").click(function () {
//   var id = $(this).closest("tr").find(".use-address").text();
//   alert(id);
// });
    
</script>
@endsection