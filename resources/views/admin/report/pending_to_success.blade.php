@extends('admin.layout')

@section('content')

<style type="text/css">

html,body{
                        background-image: url('http://getwallpapers.com/wallpaper/full/c/1/4/60124.jpg');

            background-size: cover;
            background-repeat: repeat;
            height: 100%;
            }

td{
  max-width: 100px;
  max-height: 50px;
  overflow: hidden;
  background-color: #fff;
}

#x-table th {
  font-size: 10px;
  text-align: center;
    vertical-align: middle;
}

.sticky-col {
    position: sticky;
    position: -webkit-sticky;    
    background-color: white;
}

.first-col {
  width: 100px;
  left: 0px;    
}

.table-filter {
  position: relative;
  font-family: Arial;
}
.table-filter select {
  display: none; /*hide original SELECT element:*/
}
.select-selected {
  background-color: DodgerBlue;
}
/*style the arrow inside the select element:*/
.select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 10px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
}
/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
}
/*style the items (options), including the selected item:*/
.select-items div,.select-selected {
  color: #ffffff;
  padding: 8px 16px;
  border: 1px solid transparent;
  border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
  cursor: pointer;
}
/*style items (options):*/
.select-items {
  position: absolute;
  background-color: DodgerBlue;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}
/*hide the items when the select box is closed:*/
.select-hide {
  display: none;
}
.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
}

 .btn_downlaod {
    background-color: DodgerBlue;
    border: none;
    color: white;
    padding: 5px 10px;
    cursor: pointer;
    font-size: 15px;
    width: 10%;
    margin-left: 40%;
    margin-right: 40%;
    margin-bottom: 15px;
    }

    .btnClick{
    background-color: rgb(7, 55, 99);
    color: white;
    border: none;
    cursor: pointer;
    padding: 2px 12px 3px 12px;
    text-decoration: none;
    }

    /* Darker background on mouse-over */
    .btn_downlaod:hover {
    background-color: RoyalBlue;

    .btn-group button {
    background-color: #RoyalBlue; /* Green background */
    border: 1px solid green; /* Green border */
    color: white; /* White text */
    padding: 10px 24px; /* Some padding */
    cursor: pointer; /* Pointer/hand icon */
    float: left; /* Float the buttons side by side */
    }

    /* Clear floats (clearfix hack) */
    .btn-group:after {
        content: "";
        clear: both;
        display: table;
    }

    .btn-group button:not(:last-child) {
        border-right: none; /* Prevent double borders */
    }

    /* Add a background color on hover */
    .btn-group button:hover {
        background-color: #3e8e41;
    }
        }

    .dataTables_filter, .dataTables_info { display: none; }

    #x-table
    {
      font-size: 11px;
    }
</style>


    <div class="row-fluid">   
        <ul class="breadcrumb">
             <li><a href="{{ secure_url('/agency/report_agency.html') }}">Log Report Agent</a></li> 

            <li>Force Pending to Success Status</li><!-- 
            <li><a href="{{ secure_url('/agency/agen_update.html') }}">Agen Profile Update</a></li> -->
            <li style="float: right;">{{ $session->get('email') }}</li>
        </ul>
 <div id="inforefundtrx"></div>
 <div class="row"> 
 


<div class="box col-md-6">
<div class="box-inner"  style="background-color: #fff">
<div class="box-header well" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
<h2><i class="glyphicon glyphicon-tasks"></i> Search</h2>
<div class="box-icon"> 
  <a href="#" class="btn btn-minimize btn-round btn-default" ><i class="glyphicon glyphicon-chevron-down"></i></a>
</div>
</div>
<div class="collapse box-content" id="collapseExample">
<form action="/agency/refund_info.html" method="get" name="frms">
<div class="row">
  <div class="form-group col-md-4">  
    <label style="font-size: 15px" for="from">From</label>
    <input type="hidden" id="pg" name="pg" value="1">
    <input style="font-size: 15px" class="form-control" type="date" id="min" name="min">         
    <label style="font-size: 15px" for="from">Option</label>
    <select class="form-control" style="font-size: 15px" name="tableselect" id="tableselect">
    <option value="" selected="selected"> </option>
    <option value="id">Trx Id</option>
    <option value="agent_id">Agent Id</option>
    <option value="master_agent_id">Master Agent Id</option>
    <option value="product_id">Product Id</option>
    <option value="product_code">Product Code</option> 
    <option value="trx_type">Transaction Type</option>
    <option value="trx_payment_code">Payment Code</option>
    <option value="trx_bill_number">Bill Number</option>
    <option value="app_platform">App Platform</option>
    </select>
</div> 
<div class="form-group col-md-4">
            <label style="font-size: 15px" for="to">to</label>
            <input style="font-size: 15px" class="form-control" type="date" id="max" name="max">
            <label style="font-size: 15px" for="from">Text</label>
            <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="search" id="search" placeholder="search by selected">
</div>
</div>     

<div class="row">
      <div class=" col-md-6">
          <button style="padding: 5px" type="submit" class="btn btn-primary">Search</button>
         &nbsp; <a class="btn btn-default" href="/agency/refund_info.html"><i class="glyphicon glyphicon-refresh"></i> Refresh</a>
       </div>
</div>



      </form>
    </div>
</div>
</div>
</div>
</div>
<br><br>
        <table id="x-table" class="table table-bordered">  
                <!--  <tr>
                        <th class="center" width="1%" rowspan="5">No.</th>
                        <th>{!! $sorter->field(0) !!}</th>
                        <th>{!! $sorter->field(1) !!}</th>
                        <th>{!! $sorter->field(2) !!}</th>
                        <th>{!! $sorter->field(3) !!}</th>
                        <th>{!! $sorter->field(4) !!}</th>
                        <th>{!! $sorter->field(5) !!}</th>
                        <th>{!! $sorter->field(6) !!}</th>
                        <th>{!! $sorter->field(7) !!}</th>
                        <th>{!! $sorter->field(8) !!}</th>
                        <th>{!! $sorter->field(9) !!}</th>
                        <th>{!! $sorter->field(10) !!}</th>
                        <th>{!! $sorter->field(11) !!}</th>
                        <th>{!! $sorter->field(12) !!}</th>
                        <th>{!! $sorter->field(13) !!}</th>
                        <th>{!! $sorter->field(14) !!}</th>
                        <th>{!! $sorter->field(15) !!}</th>
                        <th>{!! $sorter->field(16) !!}</th>
                        <th>{!! $sorter->field(17) !!}</th>
                        <th>{!! $sorter->field(18) !!}</th>
                        <th>{!! $sorter->field(19) !!}</th> 
                     </tr> -->
                <tbody>
                  <?php $rownum=1; ?>
                    @if ($sorter->rowCount() == 0)
                    <tr>
                        <td colspan="5" align="center">No Pending Trx Records Found...</td>
                    </tr>
                    @else
                        @foreach ($sorter->pageRows() as $row)
                    <tr>
                        <td class="center" rowspan="9" valign="middle">{{ $sorter->skippedRows = $sorter->skippedRows + 1 }}.</td>
                        <td>{!! $sorter->field(0) !!}</td>
                        <td nowrap width="20%">{{ date('d M Y  H:i:s', strtotime($row['log_datetime'])) }}</td> 
                         <td>{!! $sorter->field(5) !!}</td>
                         <td width="20%">{{$row['trx_type']}}</td> 
                         <td>{!! $sorter->field(10) !!}</td>
                         <td width="20%">{{$row['trx_payment_code']}}</td>

                      </tr>
                      <tr> 
                        <td>{!! $sorter->field(24) !!} / {!! $sorter->field(1) !!}</td>
                        <td>{{$row['id']}} / {{$row['agent_id']}}</td> 
                        <td>{!! $sorter->field(6) !!}</td>  
                        <td>{{$row['trx_bill_number']}}</td>
                        <td>{!! $sorter->field(11) !!}</td> 
                        <td>{{$row['trx_signature']}}</td>
                      </tr>                      
                      <tr>
                        <td>{!! $sorter->field(2) !!}</td>
                        <td>{{$row['master_agent_id']}}</td>
                         <td>{!! $sorter->field(7) !!}</td> 
                         <td>{{$row['trx_result_code']}}</td>
                         <td>{!! $sorter->field(12) !!}</td>
                         <td>{{$row['trx_info1']}}</td>  
                      </tr>                      
                      <tr>
                        <td>{!! $sorter->field(3) !!}</td>                      
                        <td>{{$row['product_id']}}</td> 
                         <td>{!! $sorter->field(8) !!}</td> 
                         <td>{{$row['trx_result_desc']}}</td> 
                         <td>{!! $sorter->field(13) !!}</td> 
                         <td>{{$row['trx_info2']}}</td>

                      </tr>                      
                      <tr>
                        <td>{!! $sorter->field(4) !!}</td>                      
                        <td>{{$row['product_code']}}</td> 
                         <td>{!! $sorter->field(9) !!}</td>                          
                         <td>{{$row['trx_retrieval']}}</td> 
                         <td>{!! $sorter->field(14) !!}</td> 
                         <td>{{$row['trx_info3']}}</td>

                      </tr>                          
                      <td>{!! $sorter->field(15) !!}</td> 
                      <td>{{$row['trx_sheet_number']}}</td>
 
                        <td>{!! $sorter->field(16) !!}</td>
                        <td>{{$row['api_error_code']}}</td>
                       
                        <td>{!! $sorter->field(17) !!}</td>
                        <td>{{$row['api_error_message']}}</td>
                      </tr>                      
                      <tr>                      
                        <td>{!! $sorter->field(18) !!}</td> 
                        <td>{{ ($row['is_pending'] ='Y' ? 'true' : 'false') }}</td>
                        <td>{!! $sorter->field(19) !!}</td> 
                        <td>{{$row['advice_retry']}}</td> 
                        <td>{!! $sorter->field(20) !!}</td> 
                        <td>{{$row['advice_datetime']}}</td>
                      </tr> 
                      <tr>                      
                        <td>{!! $sorter->field(21) !!}</td> 
                        <td>{{$row['advice_result_code']}}</td>
                        <td>{!! $sorter->field(22) !!}</td> 
                        <td>{{$row['advice_result_desc']}}</td> 
                        <td>{!! $sorter->field(23) !!}</td> 
                        <td>{{$row['created_at']}}</td>
                        </tr>
                        <tr> 
                        <td>{!! $sorter->field(25) !!}</td> 
                        <td colspan="3">{{$row['product_billnumber_paymentcode']}} <div id="infoid{{$rownum}}"></div></td><td>{!! $sorter->field(26) !!}</td> 
                        <td>{{$row['trx_internal_id']}} </td>
                      </tr>
                     <tr>
                      <td colspan="7" style="background-color:#3334;text-align: right;" height="20px" align="left">
                       <button class="btn btn-primary btn-sm" onclick="javascript:recekpend('{{$rownum}}','{{$row['id']}}');">Force Success</button>
                       <!-- <button class="btn btn-warning btn-sm" onclick="javascript:refundpend('{{$rownum}}','{{$row['id']}}');">Refund Pending</button> -->
                      </td>
                    </tr>
                    <?php $rownum++; ?>
                        @endforeach
                    @endif
                </tbody>
            </table>
         
            <div class="row-fluid col-md-12 pull-right">{!! $sorter->pagination() !!}</div>
       
      <!-- <a href="report_help.html?min={{$min}}&max={{$max}}&tableselect={{$tableselect}}&search={{$search}}" target="_blank">export to excel</a> -->
    </div> 
<script type="text/javascript"> 
    
$(".use-address").click(function () {
  var id = $(this).closest("tr").find(".use-address").text();
  alert(id);
});
    
    function recekpend(id,cek)
    {
      

      if (confirm('Are you sure to Process Pending?')) {
         // alert('Thanks for confirming');



      $("#infoid"+id).html("<span class='red'>processing...</span>");

     // e.preventDefault();
         $.ajax({
            type: "post",
            url: "{{ secure_url('/agency/force_pending') }}",
            data:  {id:cek},
            dataType: "json", cache    : false,
            success: function(data) {
                console.log(data); 
                alert(data);
                // console.log(data);
                // alert("Force Pending Success");
                //   window.location.assign('refund_info.html');

                 // if (data.error.code == 200) { 
 // alert(data.status);
 $("#inforefundtrx").html("<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'>×</button><strong>"+data.status+"</strong><br>"+data.error.code+" - "+data.error.message+"</div>");  
 // window.location.assign('refund_info.html');
 
 //                              }
 //                              else
 //                              {
 //                                  console.log("runing step 2");
 // $("#inforefundtrx").html("<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'>×</button><strong>"+data.status+"</strong><br>"+data.code+" - "+data.message+"</div>");  
 //                              }



            },
            error: function(error) {
                console.log('error');
            }
        }); 

      } else {
              //alert('Why did you press cancel? You should have confirmed');
              return false;
          }
 
    }

    function refundpend(id,cek)
    {
      

      if (confirm('Are you sure to Process Refund?')) {
         // alert('Thanks for confirming');



      $("#infoid"+id).html("<span class='red'>processing...</span>");

     // e.preventDefault();
         $.ajax({
            type: "post",
            url: "{{ secure_url('/agency/refund_pending') }}",
            data:  {id:cek},
            dataType: "json", cache    : false,
            success: function(data) {
                console.log(data);
                alert(data.status);
                // alert("Refund Success"); 
                  // window.location.assign('refund_info.html');

            },
            error: function(error) {
                console.log('error');
            }
        }); 

      } else {
              //alert('Why did you press cancel? You should have confirmed');
              return false;
          }
 
    }
</script>
<script src="js/charisma.js"></script>
@endsection