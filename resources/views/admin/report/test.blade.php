@extends('admin.main_agent.layout')

@section('content')
<style>

html,body{
            background-image: url('http://getwallpapers.com/wallpaper/full/c/1/4/60124.jpg');
            background-size: cover;
            background-repeat: repeat;
            height: 100%;
            }

.container {
    height:100px;
    width:100%;
  }

.box{
    background-color: #F7F7F9;
    width: 80%;
    border: 2px solid grey;
}

table, td, th {
  border: 1px solid black;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th {
  height: 50px;
}

</style>
    <div>
        <ul class="breadcrumb">
            <li><a href="{{ secure_url('/agency/report_agency.html') }}">Log Report Agent</a></li>
            
            <li>Recon Info</li>
            <li style="float: right;">{{ $session->get('email') }}</li>
        </ul>
    </div>
  <h4>Recon Info</h4>
    </br>
      <h4>Contoh info recon_code : [process_type]-[init_status]-[status_code]-[biller_aggregator_id]</h4>
      <table>
          <tr>
            <th>Recon Code</th>
            <th>Info</th>
          </tr>
          <tr>
            <td>0-3-0-x</td>
            <td>Application Error.</td>
          </tr>
          <tr>
            <td>1-1-1-*</td>
            <td>Internal Application Error</td>
          </tr>
          <tr>
            <td>1-1-3-*</td>
            <td>Data only available in BWN.</td>
          <tr>
            <td>1-1-3-x</td>
            <td>Data only available in BWN.</td>
          </tr>
          <tr>
            <td>1-2-1-2</td>
            <td>Different admin fee</td>
          </tr>
          <tr>
            <td>2-1-3-2</td>
            <td>Data only available in BWN</td>
          </tr>
          <tr>
            <td>2-4-2-2</td>
            <td>Pending Success</td>
          </tr>
          <tr>
            <td>1-4-2-2</td>
            <td>OK : Pending Success</td>
          </tr>
          <tr>
            <td>2-1-2-2</td>
            <td>Pending failed need refund</td>
          </tr>
      </table>
      Contoh info recon_code :
      <div>
      <br>
          info untuk process_type :
            <table>
          <tr>
            <th>Recon Code</th>
            <th>Info</th>
          </tr>
          <tr>
            <td>0</td>
            <td>unknown</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Agency</td>
          </tr>
          <tr>
            <td>2</td>
            <td>H2H</td>
          <tr>  
              </table>
              <br>
          info untuk init_status :
              <table>
          <tr>
            <th>Recon Code</th>
            <th>Info</th>
          </tr>
          <tr>
            <td>1</td>
            <td>Bedigital data is existed & POS data is not existed. </td>
          </tr>
          <tr>
            <td>2</td>
            <td>Bedigital & POS data is existed, but there are differences of biller fee.</td>
          </tr>
          <tr>
            <td>3</td>
            <td>Bedigital data is not existed & POS data is existed.</td>
          <tr>
          <tr>
            <td>4</td>
            <td>Bedigital & POS data is existed, pending transaction status from partner_trx_request with exclude condition init_status 1,2,3.</td>
          <tr>  
              </table>
              <br>
          info untuk status_code, data based on bedigital transaction code :
              <table>
          <tr>
            <th>Recon Code</th>
            <th>Info</th>
          </tr>
          <tr>
            <td>0</td>
            <td>unknown</td>
          </tr>
          <tr>
            <td>1</td>
            <td>SUCCESS</td>
          </tr>
          <tr>
            <td>2</td>
            <td>PENDING</td>
          <tr>  
            <tr>
            <td>3</td>
            <td>FAILED</td>
          <tr> 
              </table>
      </div>
      <br>
      
@endsection