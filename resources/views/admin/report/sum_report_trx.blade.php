@extends('admin.main_agent.layout')

@section('content')


<script src="https://cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>


  
      <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css">
     


<style type="text/css">

html,body{
           
            background-size: cover;
            background-repeat: repeat;
            height: 100%;
            }

td{
  max-width: 100px;
  max-height: 50px;
  overflow: hidden;
}

th {
  font-size: 10px;
}

.sticky-col {
    position: sticky;
    position: -webkit-sticky;    
    background-color: white;
}

.first-col {
  width: 100px;
  left: 0px;    
}

.table-filter {
  position: relative;
  font-family: Arial;
}
.table-filter select {
  display: none; /*hide original SELECT element:*/
}
.select-selected {
  background-color: DodgerBlue;
}
/*style the arrow inside the select element:*/
.select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 10px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
}
/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
}
/*style the items (options), including the selected item:*/
.select-items div,.select-selected {
  color: #ffffff;
  padding: 8px 16px;
  border: 1px solid transparent;
  border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
  cursor: pointer;
}
/*style items (options):*/
.select-items {
  position: absolute;
  background-color: DodgerBlue;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}
/*hide the items when the select box is closed:*/
.select-hide {
  display: none;
}
.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
}

 .btn_downlaod {
    background-color: DodgerBlue;
    border: none;
    color: white;
    padding: 5px 10px;
    cursor: pointer;
    font-size: 15px;
    width: 10%;
    margin-left: 40%;
    margin-right: 40%;
    margin-bottom: 15px;
    }

    .btnClick{
    background-color: rgb(7, 55, 99);
    color: white;
    border: none;
    cursor: pointer;
    padding: 2px 12px 3px 12px;
    text-decoration: none;
    }

    /* Darker background on mouse-over */
    .btn_downlaod:hover {
    background-color: RoyalBlue;

    .btn-group button {
    background-color: #RoyalBlue; /* Green background */
    border: 1px solid green; /* Green border */
    color: white; /* White text */
    padding: 10px 24px; /* Some padding */
    cursor: pointer; /* Pointer/hand icon */
    float: left; /* Float the buttons side by side */
    }

    /* Clear floats (clearfix hack) */
    .btn-group:after {
        content: "";
        clear: both;
        display: table;
    }

    .btn-group button:not(:last-child) {
        border-right: none; /* Prevent double borders */
    }

    /* Add a background color on hover */
    .btn-group button:hover {
        background-color: #3e8e41;
    }
        }

    .dataTables_filter, .dataTables_info { display: none; }
    legendfieldset{

    display: block;
    /* width: 100%; */
    max-width: 100%;
    padding: 2px;
    margin-bottom: .5rem;
    /* font-size: 1.5rem; */
    line-height: inherit;
    color: inherit;
    white-space: normal;
    border:1px solid #DEDEDE;
    }
    fieldset
    {

    border:1px solid #DEDEDE;
    }
    legend{
    display: block;
    width: auto;
    margin-left: .5rem;
    max-width: 100%;
    /* padding: 0 0 0 15px; */
    margin-bottom: .5rem;
    font-size: 1rem;
    line-height: inherit;
    color: inherit;
    white-space: normal;
    padding: 5px;
    }
</style>
    <div class="row-fluid">
        <div class="clearfix"></div>    
        <ul class="breadcrumb">
            <li>Summary Report </li> 
        </ul> 

 



<div class="row wrapper border-bottom white-bg page-heading"><div class="col-lg-12">
   

 <table class="table">
                                  <thead>  
                                   <tr>
                                             
                                        <td ><b>Month Year </b></td>   
                                        <td ><b>Daftar Gerai</b></td>   
                                        <td ><b>Verified</b></td>    
                                     
                                    </tr>
                                    </thead>
                                   <tbody> 
                                    
                                       @foreach ($listdata  as $row)
                                                   <tr>
                                                    <td>{{ date("F", mktime(null, null, null, $row['month'])) }} {{ $row['year'] }}</td> 
                                                    <td>{{ number_format($row['total'] , 0) }}</td> 
                                                    <td>{{ number_format($row['verified'] , 0) }}</td> 
                                                    
                                                    </tr>
                                                  @endforeach 
 
                                            </tbody>
                                </table> 
</div>
 
</div>


 
          </div>

            
  
 












@endsection