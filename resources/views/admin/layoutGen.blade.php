<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin Site</title>
 
    <!-- Bootstrap -->
    <link href="../gen/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../gen/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../gen/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="../gen/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../gen/css/custom.min.css" rel="stylesheet">


      <!-- PNotify -->
    <link href="../gen/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../gen/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../gen/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <style type="text/css">
    .hideform{
      display: none;
    }
    .showform{
      display: block;
    }
    #customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
    }

    #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #customers tr:nth-child(even){background-color: #f2f2f2;}

    #customers tr:hover {background-color: #ddd;}

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }

   @yield('styles')    
    </style>

  </head>   
 
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{ secure_url('/h2h/admin/home.html') }}" class="site_title"><i class="fa fa-cogs"></i></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="img/andah.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
              
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="{{ secure_url('/h2h/admin/home.html') }}"><i class="fa fa-home"></i> Home </a></li>
                  <li><a><i class="fa fa-edit"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
 
                        <li><a href="javascript:getView('{{ secure_url('/h2h/admin/listallproduct.html') }}');">Services &amp; Products</a></li> 
                        <li><a href="javascript:getView('{{ secure_url('/h2h/admin/fee.html') }}');">Fee</a></li> 
                       <!--  <li><a href="javascript:insidePage('{{ secure_url('/admin/wlbiller.html') }}');">Billers</a></li>
                       <li><a href="javascript:insidePage('{{ secure_url('/admin/wlbalancetype.html') }}');">Balance Types</a></li>! -->


                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> Reporting <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="javascript:getView('{{ secure_url('/h2h/admin/report_trx.html') }}');">Transactions</a></li>
                        <li><a href="javascript:getView('{{ secure_url('/h2h/admin/report.html') }}');">Settlements</a></li>
                    </ul>
                  </li> 
                </ul>
              </div>
              

            </div>
            <!-- /sidebar menu --> 
            
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="img/andah.jpg" alt="">
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="{{ secure_url('/h2h/admin/profile.html') }}">Profile</a></li>
                    <li><a href="{{ secure_url('/h2h/admin/logout.html') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
         <!-- page content -->
         <div id="content">
              <div id="infomessage"></div>

         @yield('content')
         </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
           </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../gen/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../gen/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../gen/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../gen/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../gen/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="../gen/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
   
    <!-- bootstrap-daterangepicker -->
    <script src="../gen/moment/min/moment.min.js"></script> 
    
    <!-- Custom Theme Scripts -->
    <script src="../gen/js/custom.min.js"></script>     
    <script src="../gen/js/bootbox.min.js"></script>  

  <!-- PNotify -->
    <script src="../gen/pnotify/dist/pnotify.js"></script>
    <script src="../gen/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../gen/pnotify/dist/pnotify.nonblock.js"></script>

 <!-- Datatables -->
    <script src="../gen/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../gen/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../gen/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../gen/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../gen/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../gen/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../gen/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../gen/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../gen/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../gen/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../gen/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../gen/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
 
    <!-- Datatables --> 
    <script src="../admin/js/common_1.js"></script>

@yield('scripts')
  
    <!-- /bootstrap-daterangepicker -->
  </body>
</html>