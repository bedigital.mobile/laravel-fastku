@extends('admin.main_agent.layout')

@section('content')

<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/jquery-1.11.1.min.js" charset="UTF-8"></script>
  <script src="wysiwyg/ckeditor/ckeditor.js"></script>
  <script src="wysiwyg/ckeditor/samples/js/sample.js"></script>
  <link rel="stylesheet" href="wysiwyg/ckeditor/samples/css/samples.css">
  <link rel="stylesheet" href="wysiwyg/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css">

<style>
  body > #standalone-container {
    margin: 50px auto;
    max-width: 720px;
  }
  #editor-container {
    height: 350px;
  }
 
 

.hide {

    display: none;

}

 
</style>
<ul class="breadcrumb">
     <!-- <li><a href="{{ secure_url('/ma/report_agency.html') }}">Log Report Agent</a></li> -->
    <li>Push Notification to Agent  </li>
    <!-- <li style="float: right;">{{ $session->get('email') }}</li> -->
</ul>
    <div class="row-fluid">   
       
 <div id="msginfopush" class="hide"><div id="msginfopushchild" class="alert "></div></div>
<div class="well">
  
<form name="frmsendpushagency" id="frmsendpushagen" method="post" enctype="multipart/form-data"  > 
    <div class="row">
        <div class="form-group col-md-6">        
              <label style="font-size: 15px" for="from">Agen Name</label>
              <select   class="form-control" name="agent_id" class="w3-input w3-border">
              @foreach ($agen as $r)
              <option value="{{$r['id']}}">{{$r['fullname']}} -- {{$r['username']}}</option>
              @endforeach
              </select> 
        </div>
    </div> 
    <div class="row">
        <div class="form-group col-md-6">  
              <label style="font-size: 15px" for="from">Title</label>
              <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="title" id="title" placeholder="Push Title" required="">
        </div>    
    </div>   <div class="row">
        <div class="form-group col-md-6">  
              <label style="font-size: 15px" for="from">Notification Text </label>
              <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="body" id="body" placeholder="Notification Text" required="">
        </div>    
    </div>     
    <input type="hidden" name="textquill" id="textquill">
              <label style="font-size: 15px" for="from">Message text</label>
    <textarea name="editor" id="editor"></textarea>
 


    <div class="radio">
        <label>
        <input type="radio" name="optionsRadios" id="optionsRadios1" value="now" checked="">
        Send Now
        </label>
    </div>
  <!--   <div class="radio">
        <label>
        <input type="radio" name="optionsRadios" id="optionsRadios2" value="later">
        Send Later
        </label>
    </div> -->
    <div class="row hide">
        <div class=" col-md-4"> 
              <input type="text" name="send_datetime" id="send_datetime" value="" class="form-control w3-input w3-border hide" /> 
        </div>
    </div>
    <div class="row">
        <div class=" col-md-6">
              <button style="padding: 5px" type="submit" class="btn btn-primary">Send</button>
              <button style="padding: 5px" type="reset" class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i> Reset</button> 
        </div>
    </div>  
 
  
 </form>
 </div> 

    <script type="text/javascript">  
    initSample();


      $(function(){
 
    $('#optionsRadios2').click(function(){
      $('#send_datetime').removeClass("hide");
    });
    $('#optionsRadios1').click(function(){
      $('#send_datetime').addClass("hide");
    });

        
      });
      
        $("#frmsendpushagen").submit(function( ) {

for (instance in CKEDITOR.instances) {
              CKEDITOR.instances[instance].updateElement();
              }

        if (confirm('Anda Yakin Akan Mengirim?')) {
              
            $.post('{{ secure_url('/ma/send_push_agen.html') }}', $("#frmsendpushagen").serialize())
            // Serialization looks good: name=textInNameInput&&telefon=textInPhoneInput etc
            .done(function(data) {
                if (data.error.code == 200) {
                    $("#msginfopush").removeClass("hide");   
                    $("#msginfopushchild").html("<button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button><strong>Send Push Success</strong>");   
                    $("#msginfopushchild").addClass("alert-success");   
                    $("#msginfopushchild").removeClass("alert-danger");   
                    // $("#frmsendpushagen").[0].reset();
                    $("#frmsendpushagen")[0].reset()

                } else {
                    $("#msginfopush").removeClass("hide");   
                    $("#msginfopushchild").html("<strong>Send Push Failed</strong> ");  
                    $("#msginfopushchild").addClass("alert-danger");                    
                    $("#msginfopushchild").removeClass("alert-success");   
   

                }
            });
           } 
            return false;
        });
        

 
    </script> 

  
@endsection