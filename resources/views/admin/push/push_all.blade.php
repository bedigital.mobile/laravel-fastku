@extends('admin.main_agent.layout')

@section('content')

 
<style type="text/css">

html,body{
                        background-image: url('http://getwallpapers.com/wallpaper/full/c/1/4/60124.jpg');

            background-size: cover;
            background-repeat: repeat;
            height: 100%;
            }

td{
  max-width: 100px;
  max-height: 50px;
  overflow: hidden;
  background-color: #fff;
}

#x-table th {
  font-size: 10px;
  text-align: center;
    vertical-align: middle;
}

.sticky-col {
    position: sticky;
    position: -webkit-sticky;    
    background-color: white;
}

.first-col {
  width: 100px;
  left: 0px;    
}

.table-filter {
  position: relative;
  font-family: Arial;
}
.table-filter select {
  display: none; /*hide original SELECT element:*/
}
.select-selected {
  background-color: DodgerBlue;
}
/*style the arrow inside the select element:*/
.select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 10px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
}
/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
}
/*style the items (options), including the selected item:*/
.select-items div,.select-selected {
  color: #ffffff;
  padding: 8px 16px;
  border: 1px solid transparent;
  border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
  cursor: pointer;
}
/*style items (options):*/
.select-items {
  position: absolute;
  background-color: DodgerBlue;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}
/*hide the items when the select box is closed:*/
.select-hide , .hide {
  display: none;
}
.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
}

 .btn_downlaod {
    background-color: DodgerBlue;
    border: none;
    color: white;
    padding: 5px 10px;
    cursor: pointer;
    font-size: 15px;
    width: 10%;
    margin-left: 40%;
    margin-right: 40%;
    margin-bottom: 15px;
    }

    .btnClick{
    background-color: rgb(7, 55, 99);
    color: white;
    border: none;
    cursor: pointer;
    padding: 2px 12px 3px 12px;
    text-decoration: none;
    }

    /* Darker background on mouse-over */
    .btn_downlaod:hover {
    background-color: RoyalBlue;

    .btn-group button {
    background-color: #RoyalBlue; /* Green background */
    border: 1px solid green; /* Green border */
    color: white; /* White text */
    padding: 10px 24px; /* Some padding */
    cursor: pointer; /* Pointer/hand icon */
    float: left; /* Float the buttons side by side */
    }

    /* Clear floats (clearfix hack) */
    .btn-group:after {
        content: "";
        clear: both;
        display: table;
    }

    .btn-group button:not(:last-child) {
        border-right: none; /* Prevent double borders */
    }

    /* Add a background color on hover */
    .btn-group button:hover {
        background-color: #3e8e41;
    }
        }

    .dataTables_filter, .dataTables_info { display: none; }

    #x-table , .upd
    {
      font-size: 11px;
    }
</style>
<style type="text/css">
      
      dl.defs{ margin: 10px 0 10px 40px; }
      dl.defs dt{ font-weight: bold; line-height: 20px; margin: 10px 0 0 0; }
      dl.defs dd{ margin: -20px 0 10px 160px; padding-bottom: 10px; border-bottom: solid 1px #eee;}
      pre{ font-size: 12px; line-height: 16px; padding: 5px 5px 5px 10px; margin: 10px 0; background-color: #e4f4d4; border-left: solid 5px #9EC45F; overflow: auto; tab-size: 4; -moz-tab-size: 4; -o-tab-size: 4; -webkit-tab-size: 4; }

      .wrapper{ background-color: #ffffff; width: 800px; border: solid 1px #eeeeee; padding: 20px; margin: 0 auto; }
      #tabs{ margin: 20px -20px; border: none; }
      #tabs, #ui-datepicker-div, .ui-datepicker{ font-size: 85%; }
      .clear{ clear: both; }

      .example-container{ background-color: #f4f4f4; border-bottom: solid 2px #777777; margin: 0 0 20px 40px; padding: 20px; }
      .example-container input{ border: solid 1px #aaa; padding: 4px; width: 175px; }
      .ebook{}
      .ebook img.ebookimg{ float: left; margin: 0 15px 15px 0; width: 100px; }
      .ebook .buyp a iframe{ margin-bottom: -5px; }
    </style>

    <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" media="all" type="text/css" href="jquery-ui-timepicker-addon.css" />

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="i18n/jquery-ui-timepicker-addon-i18n.min.js"></script>
    <script type="text/javascript" src="jquery-ui-sliderAccess.js"></script>

    <div class="row-fluid">   
        <ul class="breadcrumb">
             <!-- <li><a href="{{ secure_url('/agency/report_agency.html') }}">SMS Info Agent</a></li> -->
            <li>Push Notification to ALL Agent</li>
            <li style="float: right;">{{ $session->get('email') }}</li>
        </ul>
 <div id="msginfopush" class="hide"><div id="msginfopushchild" class="alert "></div></div>
<div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab">Text Notification</a></li>
      <!-- <li><a href="#profile" data-toggle="tab" >Image Notification</a></li> -->
    </ul>    
    <!-- <form action="/agency/send_push_whitelabel.html" method="post" name="frmsendpushWl"> -->
      <form name="frmsendpushWl" id="frmsendpushWl" method="post" >

    <div id="myTabContent" class="tab-content">        

    <div class="tab-pane active in" id="home">
        <br><br>
        <div class="row">
      <div class="form-group col-md-6">  
        <label style="font-size: 15px" for="from">Title</label>
        <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="title" id="title" placeholder="Push Title" required="">
        </div>    <!-- -->
        </div>     
        <div class="row">
        <div class="form-group col-md-6">
        <label style="font-size: 15px" for="from">Push text</label>
        <textarea class="form-control" class="w3-input w3-border"  name="txtbody" id="txtbody" placeholder="Push Body" required=""></textarea>
        </div>
        </div>
        <!-- <div class="row">
        <div class="form-group col-md-6">
        <label style="font-size: 15px" for="from">Text</label>
        <textarea class="form-control w3-input w3-border"  name="txttext" id="txttext" placeholder="Push Test"></textarea>
        </div>   
        </div> --> 
    </div>
    <div class="tab-pane fade" id="profile"><br>
        Under Construction
    </div>
        <div class="radio">
            <label>
            <input type="radio" name="optionsRadios" id="optionsRadios1" value="now" checked="">
            Send Now
            </label>
        </div>
        <div class="radio">
            <label>
            <input type="radio" name="optionsRadios" id="optionsRadios2" value="later">
            Send Later
            </label>
        </div>
         <div class="row">
          <div class=" col-md-4">

 
                  <input type="text" name="send_datetime" id="send_datetime" value="" class="form-control w3-input w3-border hide" />

          </div>
        </div>
        <br>
        <div class="row">
          <div class=" col-md-6">
          <button style="padding: 5px" type="submit" class="btn btn-primary">Send</button>
          <button style="padding: 5px" type="reset" class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i> Reset</button> 
          </div>
        </div> 
  </div> 
 


        </form>

 
     </div> 
 

    <script type="text/javascript">
      
      $(function(){

        // $('#tabs').tabs();
    $('#send_datetime').datetimepicker();
    $('#optionsRadios2').click(function(){
      $('#send_datetime').removeClass("hide");
    });
    $('#optionsRadios1').click(function(){
      $('#send_datetime').addClass("hide");
    });

        
      });
      



  
    function beforeSubmit() {
        $('#button-close').attr('disabled', 'disabled');
        $('#sbmt_upd').attr('disabled', 'disabled');
        $('#sbmt_upd').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Saving...');
    }

    function afterSubmit() {
        $('#button-close').removeAttr('disabled');

        $('#sbmt_upd').removeAttr('disabled');
        $('#sbmt_upd').html('<i class="glyphicon glyphicon-save"></i> Save');
    }

// $(document).on("submit", "form", function(event)
// {
//     event.preventDefault();

//     var url=$(this).attr("action");
//     $.ajax({
//         url: '{{ secure_url('/agency/send_push_whitelabel.html') }}',
//         type: 'POST',      
//         data: $('#frmsendpushWl').serialize(),
//         processData: false,
//         contentType: false,
//         success: function (data, status)
//         {
//           console.log(data);
//           console.log(status);

//         },
//         error: function (xhr, desc, err)
//         {
//             console.log("error");

//         }
//     });        

// });

 $(document).ready(function(e) {

        $("#frmsendpushWl").submit(function() {
          if (confirm('Anda Yakin Akan Mengirim?')) {
            $.post('{{ secure_url('/agency/send_push_whitelabel.html') }}', $("#frmsendpushWl").serialize())
            // Serialization looks good: name=textInNameInput&&telefon=textInPhoneInput etc
            .done(function(data) {
                if (data.error.code == 200) {
                    $("#msginfopush").removeClass("hide");   
                    $("#msginfopushchild").html("<button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button><strong>Send Push Success</strong>");   
                    $("#msginfopushchild").addClass("alert-success");   
                    $("#msginfopushchild").removeClass("alert-danger");   
                    // $("#frmsendpushWl").[0].reset();
                    $("#frmsendpushWl")[0].reset()

                } else {
                    $("#msginfopush").removeClass("hide");   
                    $("#msginfopushchild").html("<strong>Send Push Failed</strong> ");  
                    $("#msginfopushchild").addClass("alert-danger");                    
                    $("#msginfopushchild").removeClass("alert-success");   
   

                }
            });
          }

            return false;
        })
    });



    // function sendPushWL() {        
    //     sendForm2({
    //         method: 'POST',
    //         form: 'frmsendpushWl',
    //         messageView: 'msginfo',            
    //         messageSuccess: 'Push Send',
    //         url: '{{ secure_url('/agency/send_push_whitelabel.html') }}',
    //         redirect:  '/agency/push_whitelabel.html',
    //         beforeSubmit: function () {                
    //             beforeSubmit();
    //         },
    //         afterSubmit: afterSubmit
    //     });
    // }
    </script>
<script src="js/charisma.js"></script>
@endsection