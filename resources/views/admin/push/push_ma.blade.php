@extends('admin.main_agent.layout')


@section('content')
 

    <link rel="stylesheet" media="all" type="text/css" href="jquery-ui.css" />
    <link rel="stylesheet" media="all" type="text/css" href="jquery-ui-timepicker-addon.css" />

    <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery-ui-timepicker-addon.js"></script>


      <script src="wysiwyg/ckeditor/ckeditor.js"></script>
  <script src="wysiwyg/ckeditor/samples/js/sample.js"></script>
  <link rel="stylesheet" href="wysiwyg/ckeditor/samples/css/samples.css">
  <link rel="stylesheet" href="wysiwyg/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css">




<ul class="breadcrumb">
     <!-- <li><a href="{{ secure_url('/ma/report_agency.html') }}">Log Report Agent</a></li> -->
    <li>Push Notification to Agent by MA</li>
    <!-- <li style="float: right;">{{ $session->get('email') }}</li> -->
</ul>

<div id="msginfo"></div>
<div id="msg"></div>

 
       <form name="frmsendpushMa" id="frmsendpushMa" method="post" >

    <div id="myTabContent" class="tab-content">        

    <div class="tab-pane active in" id="home">
         <div class="row">
        <div class="form-group col-md-6"> 
          <label style="font-size: 15px" for="from">MA</label>

              <div class="controls">
                  <select name="user_ma" id="user_ma" data-rel="chosen" class="form-control">
                    @foreach($ma as $row)
                    <option value="{{$row['master_agent_id']}}">{{$row['username']}}</option>

                    @endforeach 
                  </select>
              </div>
        </div>
        </div>

<div class="row"> 
        <div class="form-group col-md-6">  
              <label style="font-size: 15px" for="from">Title</label>
              <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="title" id="title" placeholder="Push Title" required="">
        </div>    
    </div>   
    <div class="row">
        <div class="form-group col-md-6">  
              <label style="font-size: 15px" for="from">Notification Text </label>
              <input class="form-control"  style="font-size: 15px" class="w3-input w3-border" type="text" name="body" id="body" placeholder="Notification Text" required="">
        </div>    
    </div>     
    <input type="hidden" name="textquill" id="textquill">
              <label style="font-size: 15px" for="from">Message text</label>
    <textarea name="editor" id="editor"></textarea>
        <!-- <div class="row">
        <div class="form-group col-md-6">
        <label style="font-size: 15px" for="from">Text</label>
        <textarea class="form-control w3-input w3-border"  name="txttext" id="txttext" placeholder="Push Test"></textarea>
        </div>   
        </div> --> 
    </div>
    <div class="tab-pane fade" id="profile"><br>
        Under Construction
    </div>
        <div class="radio">
            <label>
            <input type="radio" name="optionsRadios" id="optionsRadios1" value="now" checked="">
            Send Now
            </label>
        </div>
      <!--   <div class="radio">
            <label>
            <input type="radio" name="optionsRadios" id="optionsRadios2" value="later">
            Send Later
            </label>
        </div>
         <div class="row">
          <div class=" col-md-4">

 
                  <input type="text" name="send_datetime" id="send_datetime" value="" class="form-control w3-input w3-border hide" />

          </div>
        </div> -->
        <br>
        <div class="row">
          <div class=" col-md-6">
          <button style="padding: 5px" type="submit" class="btn btn-primary">Send</button>
          <button style="padding: 5px" type="reset" class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i> Reset</button> 
          </div>
        </div> 
  </div> 
 


        </form>
 
 

    <script type="text/javascript">
            initSample();

 // $(document).ready(function(e) {

        $("#frmsendpushMa").submit(function() {
   if (confirm('Anda Yakin Akan Mengirim?')) {
     
            $.post('{{ secure_url('/ma/send_push_ma.html') }}', $("#frmsendpushMa").serialize())
            // Serialization looks good: name=textInNameInput&&telefon=textInPhoneInput etc
            .done(function(data) {
                if (data.error.code == 200) {
                    $("#msginfopush").removeClass("hide");   
                    $("#msginfopushchild").html("<button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button><strong>Send Push Success</strong>");   
                    $("#msginfopushchild").addClass("alert-success");   
                    $("#msginfopushchild").removeClass("alert-danger");   
                    // $("#frmsendpushMa").[0].reset();
                    $("#frmsendpushMa")[0].reset()

                } else {
                    $("#msginfopush").removeClass("hide");   
                    $("#msginfopushchild").html("<strong>Send Push Failed</strong> ");  
                    $("#msginfopushchild").addClass("alert-danger");                    
                    $("#msginfopushchild").removeClass("alert-success");   
   

                }
            });
           } 
            return false;
        });
       
    // });
 
    </script>  

@endsection