    <div class="modal fade" id="uploadImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Upload Image</h3>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/asset_image.html') }}" method="post" onsubmit="imageOnSubmit();" enctype="multipart/form-data" target="frame-image-upload">
                        @csrf
                        <input type="hidden" name="folder" value="avatars" />
                        <div class="form-group">
                            <input type="file" id="file-name" name="file-name" />
                            <p class="help-block">
                                *.jpg, *.png
                            </p>
                            <br/>
                            <button type="submit" id="button-upload" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-save"></i> Save</button>
                        </div>
                    </form>
                    <iframe name="frame-image-upload" id="frame-image-upload" style="display: none;"></iframe>
                </div>
                <div class="modal-footer">
                    <a href="#" id="button-close-upload" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Close</a>
                </div>
            </div>
        </div>
    </div>
