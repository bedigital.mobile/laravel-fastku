<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>File Uploader</title>

    <!-- Bootstrap -->
    <link href="/master/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/master/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/master/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/master/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="/master/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="/master/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="/master/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
	<!-- NProgress -->
    <link href="/master/vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- Dropzone.js -->
	<link href="/master/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">

	@yield('custom_css')

    <!-- Custom Theme Style -->
    <link href="/master/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

		<!-- page content -->
        <div id="content" class="right_col" role="main">
        @yield('content')
        </div>
        <!-- /page content -->

      </div>
    </div>

    <!-- jQuery -->
    <script src="/master/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/master/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/master/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/master/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="/master/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="/master/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="/master/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="/master/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="/master/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="/master/vendors/Flot/jquery.flot.js"></script>
    <script src="/master/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="/master/vendors/Flot/jquery.flot.time.js"></script>
    <script src="/master/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="/master/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="/master/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="/master/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="/master/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="/master/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="/master/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="/master/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="/master/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/master/vendors/moment/min/moment.min.js"></script>
    <script src="/master/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	<!-- FastClick -->
    <script src="/master/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/master/vendors/nprogress/nprogress.js"></script>	<!-- Dropzone.js -->
    <script src="/master/vendors/dropzone/dist/min/dropzone.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="/master/build/js/custom.min.js"></script>
	
	<script src="/master/js/common_1.js"></script>
	<script src="/master/js/common.js"></script>
	<script src="/master/js/md5.js"></script>
	
	@yield('custom_js')
	
  </body>
</html>
