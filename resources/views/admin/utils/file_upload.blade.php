@extends('admin.utils.layout')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>New Upload</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div id="msg"></div>
				<form action="/master_keys/file_upload/upload" method="post" onsubmit="formSubmit();" enctype="multipart/form-data" target="frame-upload">
					<input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}" />
					<input type="hidden" name="folder" value="{{ $folder }}" />
					<input type="hidden" name="title" value="{{ str_slug($title) }}" />
					<div class="form-group">
						<label class="control-label" for="title">Choose File</label>
						<input class="form-control" type="file" name="file-name" id="file-name" />
						<p class="help-block">(*.pdf, *.doc, max. 100MB)</p>
					</div>
					
					<button class="btn btn-primary" id="button-active"><i class="fa fa-check"></i> Save</button>
					<div class="btn btn-primary" id="button-deactive"><i class="fa fa-spin fa-spinner"></i> Saving..</div>
				</form>
				<iframe name="frame-upload" id="frame-upload" style="display:none;"></iframe>
			</div>
		</div>
	</div>
</div>
@endsection

@section('custom_js')
<script>
$(function(){
	$('#button-deactive').hide();
});
function formSubmit() {
//	$('#file-name').attr('disabled', 'disabled');
	$('#button-active').hide();
	$('#button-deactive').show();
}

function successSubmit(fileName) {
//	$('#file-name').removeAttr('disabled');
	$('#button-active').show();
	$('#button-deactive').hide();
	top.opener.callbackUpload(fileName);
	window.close();
}

function errorSubmit() {
	$('#button-active').show();
	$('#button-deactive').hide();
}

$('#frame-upload').on('load', function(){
	var response = $(this).contents().find('pre').html();
	if(response !== '')
	{
		var json = jQuery.parseJSON(response);
		if(json.error.code == 200 && json.data !== null) {
			successSubmit(json.data);
		} else {
			errorSubmit();
			
			var msg = '<div class="alert alert-danger" role="alert">Invalid Data Sent:<ul>';
			for (var i=0; i<json.error.messages.length; i++) {
				msg += '<li>' + json.error.messages[i] + '</li>';
			}
            msg += '</ul></div>';
			$('#msg').html(msg);
		}
	}
	
	$('#frame-upload').html('');
});
</script>
@endsection