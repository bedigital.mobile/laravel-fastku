 <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
              <!--  <h3>Users <small>Some examples to get you started</small></h3>!-->
              </div>
 
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
 
    <div class="">
        <div class="btn-group">
            <a  href="#" onclick="javascript:getView('/admin/wlservice_edit_{{$rs->service_id}}.html');" style="font-weight: bold;text-decoration: underline;">Service</a>
            &nbsp;&nbsp;
            <a  href="#" onclick="javascript:getView('/admin/wlservice_sub_{{$rs->service_id}}_product.html');"   >Products</a>
        </div>
         
                    <div id="msg"></div>
                    <form role="form" id="frm" onsubmit="doEditServ();return false;">
                        @csrf
                        <input type="hidden" name="service-id" value="{{ $rs->service_id }}" />
                        
                        <div class="form-group col-md-2 pull-right">
                            <a href="#" class="btn btn-upload-image">
                                <div class="crop center" id="avatar">
                                    @if ($rs->logo_filename_alias != '')
                                    <img src="{{ url('/asset_avatar/'.$rs->logo_filename_alias) }}" />
                                    @else
                                    <img src="img/user.png" />
                                    @endif
                                </div>
                            </a>
                            <input type="hidden" name="avatar-file" id="avatar-file" value="{{ $rs->logo_filename_alias }}" />
                        </div>

                        <div class="form-group col-md-10 pull-left">
                            <label for="service"> Service Name &nbsp;<small class="red"><i>(Required)</i></small></label>

                            <div class="clearfix"></div>

                            <span class="form-control">{{ $rs->service_name }}</span>
                        </div>
                        
                        <div class="clearfix"></div>

                        <div class="form-group col-md-10 pull-left">
                            <label for="alias"> Alias Name </label>
                            <input type="text" id="alias" name="alias" placeholder="Alias service name" class="form-control" value="{{ $rs->service_name_alias }}" />
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-10 pull-left">
                            <label for="description"> Description </label>
                            <input type="text" id="description" name="description" placeholder="Service description" class="form-control" value="{{ $rs->description }}" />
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-3 pull-left">
                            <label for="priority"> Show Order <small class="red"><i>(Required)</i></small></label>
                            <input type="text" id="priority" name="priority" placeholder="Service priority" class="form-control" value="{{ $rs->show_order }}" />
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-3 {{ ($rs->active == 'Y' ? ($rs->service_active == 'Y' ? 'has-success' : 'has-error') : 'has-error') }}">
                            <label for="status"> Status</label>

                            <div class="clearfix"></div>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-flag"></i></span>
                                <span class="form-control">{{ ($rs->active == 'Y' ? ($rs->service_active == 'Y' ? 'Active' : 'Inactive') : 'Inactive') }}</span>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr/>

                        <div class="form-group col-md-12">
                            <a href="#" onclick="javascript:getView('/admin/service.html');"  class="btn btn-warning btn-sm" id="button-close"><i class="glyphicon glyphicon-arrow-left"></i> Close</a>
                            &nbsp;
                            <button class="btn btn-info btn-sm pull-right" type="submit" id="button-save"><i class="glyphicon glyphicon-save"></i> Save</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
           
    @include('utils.image_upload')
  </div>
    </div>  </div>
    </div>