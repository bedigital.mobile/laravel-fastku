@extends('layout')

@prepend('scripts')
<script>
    function beforeSubmit() {
        $('#button-close').attr('disabled', 'disabled');

        $('#button-save').attr('disabled', 'disabled');
        $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Saving...');
    }

    function afterSubmit() {
        $('#button-close').removeAttr('disabled');

        $('#button-save').removeAttr('disabled');
        $('#button-save').html('<i class="glyphicon glyphicon-save"></i> Save');
    }
    
    function didSave(id) {
        self.location = '{{ url('/service') }}_edit_' + id + '.html';
    }

    function doSave() {
        sendForm3({
            method: 'PUT',
            form: 'frm',
            messageView: 'msg',
            url: '{{ url('/service_new.html') }}',
            redirect:  didSave,
            beforeSubmit: beforeSubmit,
            afterSubmit: afterSubmit
        });
    }

    function doEditServ() {        
        sendForm3({
            method: 'POST',
            form: 'frm',
            messageView: 'msg',
            url: '{{ url('/service_edit.html') }}',
            redirect:  '{{ url('/service.html') }}',
            beforeSubmit: beforeSubmit,
            afterSubmit: afterSubmit
        });
    }
    
    function doDelete() {
        if (confirm('Are you sure you want to remove this Main Agent?')) {
            sendForm3({
                method: 'POST',
                form: 'frm',
                messageView: 'msg',
                url: '{{ url('/service_delete.html') }}',
                redirect:  '{{ url('/service.html') }}',
                beforeSubmit: function() {
                    $('#button-close').attr('disabled', 'disabled');
                    $('#button-save').attr('disabled', 'disabled');
                    $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Removing...');
                },
                afterSubmit: function() {
                    $('#button-close').removeAttr('disabled');
                    $('#button-save').removeAttr('disabled');
                    $('#button-save').html('<i class="glyphicon glyphicon-remove"></i> Remove');                }
            });
        }
    }
    
    function callbackImageUpload(data) {
        $('#avatar').html('<img src="{{ url('/asset_avatar/') }}/' + data + '" />');
        $('#avatar-file').val(data);
    }
    
    var products = []
    function checkProduct(id) {
        if ($('#product-' + id).is(':checked') == false) {
            var ln = this.products.length;
            this.products[ln] = $('#product-' + id).val();
        } else {
            var tmp = [];
            for (var i=0; i<this.products.length; i++) {
                if (this.products[i] != $('#product-' + id).val()) {
                    var ln = tmp.length;
                    tmp[ln] = this.products[i];
                }
            }
            this.products = tmp;
        }
    }
    
    function productBuild() {
        var lst = this.products.join(',');
        $('#product-list').val(lst);
    }
    
</script>
@endprepend
