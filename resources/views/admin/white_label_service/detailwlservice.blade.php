
        <!-- page content -->
        <div class="right_col" role="main">
          <div id="tablefrmedit"></div>
          <div id="divdetailmainagent">
            <div class="page-title">
              <div class="title_left">
                <h3>Master Agent Service & Product</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  
                </div>
              </div>
            </div>
             

            <div class="clearfix"></div>
            <div id="msg"></div>    

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">  
                   <ul class="nav navbar-right panel_toolbox">
                      <li><a class="red bold" href="javascript:getView('/admin/service.html');"  class="collapse-link"><i class="fa fa-chevron-left"></i> <b> List Service</b></a> 
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-pencil"></i></a>
                      </li> 
                    </ul>


                    <div class="col-md-9 col-sm-9 col-xs-12">

                       <div class="profile-user-info">
                                <div class="profile_img">
                                  <div id="crop-avatar">
                                    <!-- Current avatar -->
                                    <img class="img-responsive avatar-view" src="{{$rs->logo_filename_alias}}" alt="Avatar" title="Change the avatar">
                                  </div>
                                </div>

                                <div class="profile-info-row">
                                  <div class="profile-info-name">Service Name </div>

                                  <div class="profile-info-value">
                                    <span> {{$rs->service_name}}</span>
                                  </div>
                                </div>
                                
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Service Name Alias </div>

                                  <div class="profile-info-value">
                                    <span> {{$rs->service_name_alias}}</span>
                                  </div>
                                </div> 
                                <div class="profile-info-row">
                                  <div class="profile-info-name"> Description </div>

                                  <div class="profile-info-value">
                                    <span> {{$rs->description}}</span>
                                  </div>
                                </div>
                                 <div class="profile-info-row">
                                  <div class="profile-info-name"> Show Order </div>

                                  <div class="profile-info-value">
                                    <span> {{$rs->show_order}}</span>
                                  </div>
                                </div>
                                 <div class="profile-info-row">
                                  <div class="profile-info-name"> Status </div>

                                  <div class="profile-info-value">
                                    <span>{{ ($rs->active == 'Y' ? ($rs->service_active == 'Y' ? 'Active' : 'Inactive') : 'Inactive') }}</span>
                                  </div>
                                </div>
                        </div>
                      <div class="clearfix"></div>
                         <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Produk List</a>
                          </li>
                        
                        </ul>
                        <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                          <div id="listproduks"></div> 

  
                        </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> 
                 
        </div>
        <!-- /page content -->


 

        <script type="text/javascript">
          
showDetailProduct('{{$rs->service_id}}'); 

   function doUpdateStatus() {
        if (confirm('Are you sure you want to change  this Main Agent status?')) {
            sendForm3({
                method: 'POST',
                form: 'frmstatus',
                messageView: 'msg',
                messageSuccess: 'Master Agent Status Saved',
                url: '{{ secure_url('/mainagent_status.html') }}',
                redirect:  '{{ secure_url('/listmainagent.html') }}',
                beforeSubmit: function() {
                    $('#button-close').attr('disabled', 'disabled');
                    $('#button-save').attr('disabled', 'disabled');
                    $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Removing...');
                },
                afterSubmit: function() {
                 //  modal.remove();
                     $('.modal-backdrop').hide(); 
                    //$('.modal-backdrop').removeClass('show');
                   // $('#modalStatus').modal('hide');
                 } 
            }); 
        }
       // modal.remove();
    }
 
 
   function doDelete() {
        if (confirm('Are you sure you want to remove this Main Agent?')) {
            sendForm3({
                method: 'POST',
                form: 'frm',
                messageView: 'msg',
                messageSuccess: 'Master Agent Removed',
                url: '{{ secure_url('/mainagent_delete.html') }}',
                redirect:  '{{ secure_url('/listmainagent.html') }}',
                beforeSubmit: function() {
                    $('#button-close').attr('disabled', 'disabled');
                    $('#button-save').attr('disabled', 'disabled');
                    $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Removing...');
                },
                afterSubmit: function() {
                    $('#button-close').removeAttr('disabled');
                    $('#button-save').removeAttr('disabled');
                    $('#button-save').html('<i class="glyphicon glyphicon-remove"></i> Remove');                }
            });
        }
    }
 
 
      function showDetailProduct(id)
      {
       // alert("test");
        $("#listproduks").html(''); 
        $.ajax({
                      type: "GET",
                      url: "wlproductservice_sub_"+id+"_product.html",                     
                      success: function(msg) {
                       $("#listproduks").html(msg);                                                            
                      }
                  });


       } 
       function showEditForm(id)
      {
       // alert("test");
        $("#tablefrmedit").removeClass('hideform');
        $("#divdetailmainagent").addClass('hideform');
        $.ajax({
                      type: "GET",
                      url: "editform/"+id+".html",                     
                      success: function(msg) {
                       $("#tablefrmedit").html(msg);                                                            
                      }
                  });


       }
        </script>