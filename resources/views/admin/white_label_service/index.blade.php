@extends('white_label_service.layout')

@section('content')
    <div>
        <ul class="breadcrumb">
            <li><a href="{{ url('/admin/home.html') }}">Home</a></li>
            <li>Services &amp; Products</li>
        </ul>
    </div>

    <div class="row-fluid">
        <!-- PAGE CONTENT BEGINS -->
        <div class="row-fluid col-md-12 pull-left">
            <form id="frm-search" onsubmit="goSearch('{{ url('/wlservice.html') }}');return false;">
            <div class="input-group col-md-4 pull-left">
                <input type="text" class="form-control" iid="myInput" onkeyup="myFunction()" placeholder="Search For Names Product">
                <span class="input-group-addon"><a href="javascript:goSearch('{{ url('/admin/wlservice.html') }}');"><i class="glyphicon glyphicon-search red"></i></a></span>
                <span class="input-group-addon"><a href="javascript:getContentInTable('{{ url('/admin/wlservice.html') }}');"><i class="glyphicon glyphicon-refresh green"></i></a></span>
            </div>
            </form>            
        </div>
        &nbsp;
        <div class="clearfix"></div>

        <table class="table table-striped table-bordered" id="x-table">  
            <thead>
                <tr>
                    <td>Kode Product</td>
                    <td>Daftar Product</td>
                </tr>
                @foreach ($products as $dataProduct)
                  <tr>
                    <td>{{ $dataProduct->prodid }}</td>
                    <td>{{ $dataProduct->product_name }}</td>
                  </tr>
                @endforeach
            </thead>
        </table>
        
        <script>
    function myFunction() {
      var input, filter, table, tr, td, i;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("x-table");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
}
</script>
    </div>

@endsection