@extends('white_label_service.layout')

@section('content')
    <div>
        <ul class="breadcrumb">
            <li><a href="javascript:getPage('{{ url('/home.html') }}');">Home</a></li>
            <li><a href="javascript:getPage('{{ url('/wlservice.html') }}');">Services &amp; Products</a></li>
            <li><a href="javascript:getPage('{{ url('/wlservice.html') }}');">Services</a></li>
            <li>New</li>
        </ul><!-- /.breadcrumb -->      
    </div>

    <div class="row-fluid">
        <div class="box col-md-12">
            <div class="box-inner">                
                <div class="box-content">
                    <div id="msg"></div>            
                    <form role="form" id="frm" onsubmit="doSave();return false;">
                        @csrf

                        <div class="form-group col-md-2 pull-right">
                            <a href="#" class="btn btn-setting">
                                <div class="crop center" id="avatar">
                                    <img src="img/user.png" />
                                </div>
                            </a>
                            <input type="hidden" name="avatar-file" id="avatar-file" value="" />
                        </div>
                        
                        <div class="form-group col-md-10 pull-left">
                            <label for="service"> Service Name &nbsp;<small class="red"><i>(Required)</i></small></label>
                            
                            <div class="clearfix"></div>
                            
                            <select class="form-control" id="service" name="service" data-rel="chosen">
                                @foreach ($rsserv as $srv)
                                <option value="{{ $srv->id }}">{{ $srv->service_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-10 pull-left">
                            <label for="alias"> Alias Name </label>
                            <input type="text" id="alias" name="alias" placeholder="Alias service name" class="form-control" />
                        </div>
                        
                        <div class="clearfix"></div>
                        
                        <div class="form-group col-md-10 pull-left">
                            <label for="description"> Description </label>
                            <input type="text" id="description" name="description" placeholder="Service description" class="form-control" />
                        </div>
                        
                        <div class="clearfix"></div>
                        
                        <div class="form-group col-md-3 pull-left">
                            <label for="priority"> Show Order <small class="red"><i>(Required)</i></small></label>
                            <input type="text" id="priority" name="priority" placeholder="Service priority" class="form-control" />
                        </div>
                        
                        <div class="clearfix"></div>
                        
                        <div class="checkbox col-md-4 pull-left">
                            <label>
                                <input type="checkbox" id="status" name="status" value="Y" checked=""> Active
                            </label>
                        </div>

                        <div class="clearfix"></div>
                        <hr/>

                        <div class="form-actions">
                            <a href="javascript:getPage('{{ url('/wlservice.html') }}');" class="btn btn-warning btn-sm" id="button-close"><i class="glyphicon glyphicon-arrow-left"></i> Close</a>
                            &nbsp;
                            <button class="btn btn-info btn-sm" type="submit" id="button-save"><i class="glyphicon glyphicon-save"></i> Save</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('utils.image_upload')

@endsection
