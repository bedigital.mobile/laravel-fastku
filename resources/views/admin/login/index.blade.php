<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
       
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>FastKu - Perfect match for your  &amp; Logistic</title> 
        <link rel="shortcut icon" href="assets/media/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="assets/media/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="assets/media/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->
        <!-- Stylesheets -->
        <!-- Fonts and OneUI framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap">
        <link rel="stylesheet" id="css-main" href="assets/css/oneui.min.css"> 
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
          <script src='https://www.google.com/recaptcha/api.js'></script>
          <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <!-- jQuery -->
<style type="text/css">
   
.hide 
{
  display: none!important;
}
 
 #reloadcaptchabtn {
    cursor: pointer;
 }
#content-login { display: block; position: relative; width: 100%; }

/*#content-register { display: none!important;width: 100%; }*/
.btn-primaryfastku {
    color: #fff;
    background-color: #F04A41;
}
#infoimg
{
    font-size: 8px;
}

 
.pin-wrapper{
  display:flex;
  justify-content:center;
  align-items:center;
  height:50vh;
}
.pin-wrapper input{
    width: 44px;
    height: 44px;
    border: 1px solid #d7d7d7;
    border-radius: 6px;
    outline: none;
    margin: 1px;
    text-align:center;
    margin:0 5px;
    background:#eeeeee;
    font-size:20px;
}
.pin{
  color:#ffffff;
  font-size:20px;
  text-align:center;
}
.text-otp
{
	color:#ffffff;
}
.input-group-text
{
     border: 0px !important; 
    
}
</style>
    </head>
    <body> 
         <div id="page-container" class="side-trans-enabled">

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="bg-image" style="background-image: url('img/bg1.jpg');">
                    <div class="row no-gutters bg-primary-dark-op">
                        <!-- Meta Info Section -->
                        <div class="hero-static col-lg-4 d-none d-lg-flex flex-column justify-content-center">
                            <div class="p-4 p-xl-5 flex-grow-1 d-flex align-items-center">
                                <div class="w-100">
                                    <img src="logo.png" height="50px">
                                     <p class="text-white-75 mr-xl-8 mt-2">
                                        Perfect match for your Logistic
                                    </p>
                                </div>
                            </div>
                            <div class="p-4 p-xl-5 d-xl-flex justify-content-between align-items-center font-size-sm">
                                <p class="font-w500 text-white-50 mb-0">
                                    
                                </p>
                                
                            </div>
                        </div>
                        <!-- END Meta Info Section -->

                        <!-- Main Section -->
                        <div id="content-login" class="hero-static col-lg-8 d-flex flex-column align-items-center bg-white" >
                            <div class="font-w300 p-3 w-100 text-right"> Belum punya Akun? &nbsp;&nbsp;&nbsp;
                                 <a href="#" class="btn btn-lg btn-primaryfastku" id="showregister">
                                                                  Daftar Sekarang
                                                            </a>
                                
                            </div>
                            <div class="p-4 w-100 flex-grow-1 d-flex align-items-center">

                                <div class="w-100">
                                  <div class="row no-gutters justify-content-center">
                                         <div class="text-center col-sm-8 col-xl-12">
                                             <div class="    mb-5">
                                           
                                            <h1 class="font-w700 mb-2">
                                                Selamat Datang di Fastku
                                            </h1>
                                            <h2 class="font-size-base text-muted" id="txthead2">
                                               Masuk ke Akun Anda 
                                            </h2>
                                            </div>
                                        </div>
                                            <form class="user hide" id="formforgot" name="formforgot" method="post">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control form-control-lg form-control-alt py-4" id="usernameforgot" name="usernameforgot" placeholder="Username" required="">
                                                    </div>
                                                    <button type="submit" class="btn btn-lg btn-primaryfastku" id="submitforgot">
                                                                          Kirim
                                                   	</button>    
                                                   	<button type="button" class="btn btn-lg btn-primaryfastku" id="batallupa">
                                                                          Login
                                                   	</button>   

                                            </form>
                                           
                                            <form class="user" id="formlogin" name="formlogin" method="post">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control form-control-lg form-control-alt py-4" id="username" name="username" placeholder="Username" required="">
                                                    </div>
                                                    <div class="form-group">
                                                    	<div class="input-group">
				                                                <input type="password" class="form-control form-control-lg form-control-alt py-4" id="password" name="password" placeholder="PIN" maxlength="6">
				                                                <div class="input-group-append">
				                                                    <span class="input-group-text">
				                                                        <i class="far fa-eye" id="pinpswd"></i>
				                                                    </span>
				                                                </div>
				                                            </div>
 
                                                    </div>  
                                                    <div class="form-group text-align-right text-right">
                                                        <a href="#" id="lupapin" name="lupapin" style="color:#F04A41">Lupa PIN</a>

                                                    </div>
                                                    <div class="form-group">                                                        
                                                        <div class="block-content-full d-flex align-items-center justify-content-between">
                                                        <div>
                                                           <div id="imagecaptcha" /></div>
                                                           <div id="load_captcha" style="display: none">
                                                                         <div class="spinner-border text-dark" role="status">
                                                            <span class="sr-only">Loading...</span>
                                                            </div>
                                                           </div>
                                                           <br><span id="infoimg"></span>
                                                        </div>
                                                        <div class="ml-3">
                                                           <div class="item item-circle bg-warning-light mx-auto" id="reloadcaptchabtn">
                                                                <i class="fa fa-sync text-warning"></i>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                       <input type="text" class="form-control form-control-lg form-control-alt py-4" id="textcapthca" name="textcapthca" placeholder="Capthca" required="">
                                                    </div>
                                                    <input type="hidden" name="uid" id="uid"/><br>
                                                    <input type="hidden" name="plugin" id="plugin"/>
                                                        <div class="form-group d-flex justify-content-between align-items-center">
                                                          <div id="submitbtninfo"></div>
                                                            <button type="submit" class="btn btn-lg btn-primaryfastku" id="submitbtn">
                                                                          Masuk
                                                                    </button>                                                        
                                                        </div> 
                                                    <div class="form-group">
                                                        <div id="msg"></div>
                                                    </div>
                                                </form>
                                        </div>
                                     
                                    </div>
                                </div>
                            </div>
                           
        <!-- Register Section -->
      <div  id="content-register" class="hide hero-static col-lg-8 d-flex flex-column align-items-center bg-white" >
                            <div class="font-w300 p-3 w-100 text-right"> Sudah punya Akun? &nbsp;&nbsp;&nbsp;
                                 <a href="#" class="btn btn-lg btn-primaryfastku" id="showlogin">
                                                                  Masuk
                                                            </a>
                                
                            </div>
                            <div class="p-4 w-100 flex-grow-1 d-flex align-items-center">

                                <div class="w-100">
                                      <div class="row no-gutters justify-content-center">
                                         <div class="text-center col-sm-8 col-xl-12">
                                             <div class="    mb-5">
                                           
                                            <h1 class="font-w700 mb-2">
                                                Mulai Membuat Akun Fastku 
                                            </h1>
                                            <h2 class="font-size-base text-muted">
                                               Lengkapi Data Anda 
                                            </h2>
                                            </div>
                                        </div>
                                        <div class="col-sm-8 col-xl-6 hide" id="frmpin">
                                        	 <form class="user" id="frmotp" name="frmotp" method="post">
                                        	 <input type="hidden" id="useridotp" name="useridotp">
                                        	 <input type="hidden" id="isiotp" name="isiotp">
                                        	 <div class="row  btn-primaryfastku">                                        	 	
      	                                        <div class="col-md-12">
      	                                        	<br>
      	                                        	<span style="display: flex;justify-content: center;align-items: center;">Masukkan OTP</span>  
      	                                        	<br/>
      	                                        	<div class="pin-wrapper">
													<input type="text" data-role="pin" maxlength="1" class="pin-input">
													<input type="text" data-role="pin" maxlength="1" class="pin-input">
													<input type="text" data-role="pin" maxlength="1" class="pin-input">
													<input type="text" data-role="pin" maxlength="1" class="pin-input">
													<input type="text" data-role="pin" maxlength="1" class="pin-input">
													<input type="text" data-role="pin" maxlength="1" class="pin-input">
													</div>
													<div class="pin"></div>
													<span style="display: flex;justify-content: center;align-items: center;color:#ffffff;"><i class="fa fa-sync text-otp"></i><a href="#" id="kirimulangotp" name="kirimulangotp" style="color:#ffffff;margin-bottom:15px;">                                                                Kirim ulang OTP</a><span id="textresendotp" class="hide">Processing Resend OTP.....</span></span><br/>
													
      	                                        </div>
      	                                        <br>
      	                                        
      	                                    </div>
      	                                    <br><br>

      	                                    <div style="display: flex;justify-content: center;align-items: center;">
														<button type="submit" class="btn btn-lg btn-info" id="sendpin" > SEND </button>
													</div>
											</form>
                                        </div>
                                        <div class="col-sm-8 col-xl-6 " id="frmregis">
                                            <form class="user" id="frmsignup" name="frmsignup" method="post">
      	                                        <div class="row">
      	                                        <div class="col-md-6">
                                                
                                                    <div class="form-group">
                                                        <input type="text" class="form-control form-control-lg form-control-alt py-4" id="namaawal" name="namaawal" placeholder="Nama Awal" required="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control form-control-lg form-control-alt py-4" id="namaakhir" name="namaakhir" placeholder="Nama Akhir" required="">
                                                    </div>                                                 
                                                </div>
                                                </div>
                                                    <div class="form-group"> 
                                                        <input type="text" class="form-control form-control-lg form-control-alt py-4" value="" id="nohp" name="nohp" onkeypress="return isNumber(event)" onpaste="return false;" placeholder="No HP" required/>

                                                    </div> 
                                                    <div class="form-group">
                                                        <input type="text" class="form-control form-control-lg form-control-alt py-4" id="email" name="email" placeholder="Email" required="">
                                                    </div>
                                                 <div class="row">
	      	                                        <div class="col-md-6">
	                                                
	                                                    <div class="form-group">

	                                                    	<div class="input-group">
				                                                <input type="password" class="form-control form-control-lg form-control-alt py-4" id="pin" name="pin" placeholder="PIN" maxlength="6">
				                                                <div class="input-group-append">
				                                                    <span class="input-group-text">
				                                                        <i class="far fa-eye" id="pinnewbtn"></i>
				                                                    </span>
				                                                </div>
				                                            </div>
 
	                                                    </div>
	                                                </div>
	                                                <div class="col-md-6">
	                                                    <div class="form-group">
	                                                    	<div class="input-group">
				                                                <input type="password" class="form-control form-control-lg form-control-alt py-4" id="pin2" name="pin2"  placeholder="Konfirmasi PIN" maxlength="6">
				                                                <div class="input-group-append">
				                                                    <span class="input-group-text">
				                                                        <i class="far fa-eye" id="pinconfirmbtn"></i>
				                                                    </span>
				                                                </div>
				                                            </div>
 
	                                                    </div>                                                 
	                                                </div>
                                                </div>
                                                    <div class="form-group">                                                        
                                                        <div class="block-content-full d-flex align-items-center justify-content-between">
                                                        <div>
                                                           <div id="imagecaptchas" /></div>
                                                           <div id="load_captchas" style="display: none">
                                                                         <div class="spinner-border text-dark" role="status">
                                                            <span class="sr-only">Loading...</span>
                                                            </div>
                                                           </div>
                                                           <br><span id="infoimgs"></span>
                                                        </div>
                                                        <div class="ml-3">
                                                           <div class="item item-circle bg-warning-light mx-auto" id="reloadcaptchabtns">
                                                                <i class="fa fa-sync text-warning"></i>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                       <input type="text" class="form-control form-control-lg form-control-alt py-4" id="textcapthcas" name="textcapthcas" placeholder="Capthca" required="">
                                                    </div>  
                                                   
                                                    <div class="form-group d-flex justify-content-between align-items-center">
													<div id="registerload" style="display: none">
											         	<div class="spinner-border text-dark" role="status">
														<span class="sr-only">Loading...</span>
														</div>
													</div>
                                                            <button type="submit" class="btn btn-lg btn-primaryfastku" id="registerbtn">
                                                                  Daftar
                                                            </button>
                                                     </div> 
                                                    <div class="form-group">
                                                        <div id="msg"></div>
                                                    </div>
                                                </form>
                                        </div>
                                     
                                    </div>
                                    <!-- END Sign In Form -->
                                </div>
                            </div>
                           
                        </div> 
                        <!-- END Register Section -->
                    </div>
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->
        </div> 

  <!-- Bootstrap core JavaScript-->
  <script src="admin_template/vendor/jquery/jquery.min.js"></script>
  <script src="admin_template/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/js/pages/be_ui_animations.min.js"></script>
  <script src="assets/js/fastku.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="admin_template/vendor/jquery-easing/jquery.easing.min.js"></script> 
<script src="js/common_1.js"></script>
<script src="js/md5.js"></script>  
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

  <script>
$(document).ready(function(){
    
  var clogin = $("#content-login");
  var cregister = $("#content-register");
  var frmpin = $("#frmpin");
  var frmregis = $("#frmregis");
  var formforgot = $("#formforgot");
  var formlogin = $("#formlogin");

  let loadcap = $("#load_captcha");
  let imgcap = $("#imagecaptcha");
  let reloadC = $("#reloadcaptchabtn");
  let reloadSignup = $("#reloadcaptchabtns");
  let infoimg = $("#infoimg");
  let whitelabelId = "KEZXFW";
  
  //load capthca
  loadCaptcha('infoimg','load_captcha','imagecaptcha');
  loadCaptcha('infoimgs','load_captchas','imagecaptchas');
//  Swal.fire(
//   'The Internet?',
//   'That thing is still around?',
//   'question'
// )
   infoimg.text("fetching new captcha image...");
   // imgcap.html("https://helpdesk.posfin.id/fastku/ambilcaptcha");
   infoimg.text("");
  
  /* display the register page */
  reloadC.on("click", function(e){ 
    // Swal.fire(
    //   'The Internet?',
    //   'That thing is still around?',
    //   'question' 
    // );
    getCaptcha(whitelabelId,'infoimg','reloadcaptchabtn','load_captcha','imagecaptcha');
    // getCaptchasignup(whitelabelId);
  });
  reloadSignup.on("click", function(e){  
    getCaptcha(whitelabelId,'infoimgs','reloadcaptchabtns','load_captchas','imagecaptchas');
  });


  //https://fintech-stg.bedigital.co.id/elog/graph/v1/member?mutation={refreshCaptcha(whitelabel_id:%22KEZXFW%22){code,message}}

  $("#showregister").on("click", function(e){ 
    e.preventDefault();  
    // alert("regis");
    $(clogin).slideUp();
    $(clogin).addClass("hide");
    $(cregister).removeClass("hide"); 
    $(cregister).show(); 

  });
  
  $("#lupapin").on("click", function(e){ 
    e.preventDefault();  
    // alert("regis");
    // $(formlogin).slideUp();
    $("#txthead2").text("Lupa PIN? Silahkan Masukan username Anda");
    $(formlogin).addClass("hide");
    $(formforgot).removeClass("hide"); 
    // $(formforgot).show(); 

  });

  $("#batallupa").on("click", function(e){ 
    e.preventDefault();  
    // alert("regis");
    // $(formforgot).slideUp();
    $("#txthead2").text("Masuk ke Akun Anda");
    $(formforgot).addClass("hide");
    $(formlogin).removeClass("hide"); 
    // $(cregister).show(); 

  });
  


// fa-eye fa-eye-slash
const pinnewbtn = document.querySelector('#pinnewbtn');
const passwordNew = document.querySelector('#pin');

pinnewbtn.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = passwordNew.getAttribute('type') === 'password' ? 'text' : 'password';
    passwordNew.setAttribute('type', type);
    // toggle the icon
    this.classList.toggle('fa-eye-slash');
});

const pinconfirmbtn = document.querySelector('#pinconfirmbtn');
const passwordConfirm = document.querySelector('#pin2');

pinconfirmbtn.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = passwordConfirm.getAttribute('type') === 'password' ? 'text' : 'password';
    passwordConfirm.setAttribute('type', type);
    // toggle the icon
    this.classList.toggle('fa-eye-slash');
});
const pinpswd = document.querySelector('#pinpswd');
const password = document.querySelector('#password');

pinpswd.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the icon
    this.classList.toggle('fa-eye-slash');
});
                                        
                                           
                                           




  //==========================================================
  /* resend OTP */
  //==========================================================
  $("#kirimulangotp").on("click", function(e){
	$("#kirimulangotp").hide();
	$("#textresendotp").removeClass("hide");
  	 let userid = $("#useridotp").val();
 
            console.log("resend otp userid="+userid);
            var url = `https://helpdesk.posfin.id/fastku/resendotp`;
            // setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'POST',
                      data: { useridotp: userid },                     
                     async: true,                     
                    success: function(data) {
	                    console.log('resend otp was successful.');
						const obj = JSON.parse(data); 
						let hasil = JSON.stringify(data);
						let hasil2 = JSON.parse(data); 
						var result = '';
						//{"data":{"resendPIN":{"message":"Kode token OTP/PIN untuk verifikasi dikirim ke nomor telpon +6285311223*** melalui SMS","user_status":"REGISTERING","username":"6285311223131"}}}
						if(typeof(hasil2.errors) === 'undefined'){
							let msgotp = obj.data.resendPIN.message;
							// let txtuser_id = obj.data.signup.user_id;
								Swal.fire({
									title: 'RESEND OTP',
									text: msgotp,
									icon: 'success',
									confirmButtonText: 'Close'
								});
								$("#kirimulangotp").show();
								$("#textresendotp").addClass("hide");
						} else { 
							console.log('null, data errors');
							hasil2.errors.forEach(function(i) {
							    msg = `"${i.message}"`; 
							});
							if (msg != null) {

							        Swal.fire({
							        title: 'Error!',
							        text: msg,
							        icon: 'error',
							        confirmButtonText: 'Close'
							        });
							}
							$("#kirimulangotp").show();
							$("#textresendotp").addClass("hide");

						} 
                        

                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    } 
                });
            // }, 10);



  });
  $("#showlogin").on("click", function(e){
    e.preventDefault(); 

     $(cregister).slideUp("slow"); 
     $(cregister).addClass("hide"); 
     $(clogin).slideDown("slow"); 
     $(clogin).removeClass("hide"); 
  });
});

     // $('#uid').val(get_uuid());
     // $('#plugin').val(get_plugin());


  
    function doLogin() {
        sendFormlogins({
            method: 'POST',
            form: 'frm',
            messageView: 'msg',
            url: '{{ secure_url('fastku/login') }}',
            redirect:  '{{ secure_url('fastku/homeindex') }}',
            beforeSubmit: beforeSubmit,
            afterSubmit: afterSubmit
  
        });
    }
    function doSignup() {
        sendFormlogins({
            method: 'POST',
            form: 'formsignup',
            messageView: 'msg',
            url: '{{ secure_url('fastku/register') }}',
            redirect:  '{{ secure_url('fastku/login') }}',
            beforeSubmit: beforeSubmit,
            afterSubmit: afterSubmit
  
        });
    } 
    function loadCaptcha(infoid,loadid,imgid) {
            let infoimg = $("#"+infoid);
            let loadcaptchas = $("#"+loadid);

            console.log("load capthca");
            loadcaptchas.show();
            infoimg.text("fetching new captcha image...");
            var url = `https://helpdesk.posfin.id/fastku/ambilcaptcha`;
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'GET',
                    async: true,
                     contentType: "application/json",
                    success: function(res) {
                        console.log("refresh captcha success , data ="+JSON.stringify(res));
                         $("#"+imgid).html(res);
                         infoimg.html("");
                         loadcaptchas.hide();

                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    } 
                });
            }, 10);
        }

    function getCaptcha(whitelabelId,info,reloadid,loadid,imgid) {
            let infoimg = $("#"+info);
            let reloadcaptchabtn = $("#"+reloadid);
            reloadcaptchabtn.hide();
            console.log("get capthca , id="+whitelabelId);
            infoimg.text("fetching new captcha image...");
            var url = `https://helpdesk.posfin.id/fastku/reloadcaptcha`;
            setTimeout(function(){
                $.ajax({
                    url:url,
                    type: 'GET',
                    async: true,
                     contentType: "application/json",
                    success: function(res) {
                        console.log("refresh captcha success , data ="+JSON.stringify(res));
                         // $("#imagecaptcha").attr("src", "https://fintech-stg.bedigital.co.id/elog/graph/v1/member/captcha/"+whitelabelId+"/image.png");
                         loadCaptcha(info,loadid,imgid);
                         infoimg.html("");
                         reloadcaptchabtn.show();

                    },
                    error: function(xhr, stat, err) {
                        console.log(err);
                    } 
                });
            }, 10);
        }
 

function sendFormlogins(config) {
    if(config.beforeSubmit !== null) {
        config.beforeSubmit();
    }
    
    if (config.form !== null) {
        var frm = $("#" + config.form).serialize();
        $("#" + config.form).children().prop("disabled", true);
        $("#submitbtn").prop("disabled", true);
        $('input:button').addClass('disabled');
    }
    
                           // console.log("config"+config);

    
    $.ajax({
        // headers: {
        //     'X-CSRF-TOKEN': $('input[name=_token]').val(),
        //      'WWW-Authenticate' : 'Digest',
        //     'User-Agent' : navigator.userAgent,
        //     'realm' : 'BEDIGITAL',
        //     'Content-Type' : 'application/json',
        // },
        type: (config.method !== null ? config.method : 'POST'),
        url: config.url,
        dataType: 'html',
        data: frm,
        //  data: JSON.stringify({
        //     query: `mutation { 
        //                 login ( 
        //                     whitelabel_id: "",
        //                     captcha: "$recaptcha",
        //                     platform: "$platform",
        //                     app_build_version: "$AppID"
        //                 ) { session_id, user_id, authorization } 
        //             }`
        //           })
       contentType: "application/json",
        success: function(resp) {
          // var energy = resp.join("");
// var array = $.map(resp, function(value, index){
//         return [value];              
//          console.log('arr ='+value);

//     });
               
               console.log('pjg ='+resp);
               console.log('pjg ='+resp.errors);
               let hasil = JSON.stringify(resp);
               let hasil2 = JSON.parse(resp);
                     console.log('h ='+hasil);
                     console.log('h ='+hasil2);
                     console.log('login ='+hasil2.data.login);
                     // console.log('login ='+hasil2.data.errors.message);
                     // console.log('message ='+hasil2.errors['message']);
                     console.log('errors ='+JSON.stringify(hasil2.errors));
                      // let msg=''
                      hasil2.errors.forEach(function(i) {
                                msg = `"${i.message}"`;
// console.log(opt);

                            });



                     // console.log('path ='+JSON.parse(hasil2.errors));
                     
                     // console.log('hasil1 ='+hasil['data']['login']);

                 // hasil.forEach(function(i) { 
                 //                 console.log('hasils ='+i);
                                

                 //            });


               // console.log('energy ='+energy.length);
               // console.log('1 ='+resp.data.errors);
             

            var result = '';//jQuery.parseJSON(resp);
            if(typeof(result) !== 'undefined' && typeof(result.error) !== 'undefined' && typeof(result.error.code) !== 'undefined' && result.error.code === 200) {
                if(config.redirect !== null && config.redirect !== "") {
                    if(config.afterSubmit !== null) {
                        config.afterSubmit();
                    }
                    
                    if(typeof config.redirect === 'function') {
                        var resp = typeof(result.data) !== 'undefined' ? result.data : null;
                        config.redirect(result.data);
                       // console.log("result data"+result.data); 
                    } else {  
                        getPage(config.redirect);
                        //console.log("config-redirect"+config.redirect);
                    } 
                }
            }
            else {

                
                    hasil2.errors.forEach(function(i) {
                        msg = `"${i.message}"`; 
                    });
       

                if (config.form !== null) {
                    $("#" + config.form).children().prop("disabled", false);
                    $('input:button').removeClass('disabled');
                    $("#submitbtn").prop("disabled", false);
                }
                
                if (config.messageView != null) {
                    
                            Swal.fire({
                            title: 'Error!',
                            text: msg,
                            icon: 'error',
                            confirmButtonText: 'Close'
                            });
                    
                }
               

                
                if(config.afterSubmit !== null) {
                    config.afterSubmit();
                }
            }
        },
        error: function(resp) {
          console.log("error ="+JSON.stringify(resp));
          console.log("test ="+resp.responseText);
            if (config.form !== null) {
                $("#" + config.form).children().prop("disabled", false);
                $('input:button').removeClass('disabled');
            }
            
            if(config.afterSubmit !== null)
                config.afterSubmit();
                        
            $("#" + config.messageView).html("<div class='alert alert-danger'><strong>Invalid Data Sent: &nbsp;</strong>Unable to connect to server. Please try again.</div>");
            $(window).scrollTop(0);

            return false;
        }
    });
}

    
    function beforeSubmit() { 
        $('#submitbtn').attr('disabled', 'disabled');

        $('#submitbtninfo').html('<div class="spinner-border text-dark" role="status">                                                            <span class="sr-only">Loading...</span>                                                            </div>');
    }
    
    function afterSubmit() {
        $('#submitbtn').removeAttr('disabled', 'disabled');
        $('#submitbtninfo').html('');
    }

function get_uuid() {
  let guid = ""
  try {
    var nav = window.navigator
    var screen = window.screen
    guid = nav.mimeTypes.length
    guid += nav.userAgent.replace(/\D+/g, '')
    guid += nav.plugins.length
    guid += screen.height || ''
    guid += screen.width || ''
    guid += screen.pixelDepth || ''
  } catch (error) {
    log_imp(error)
  }
  return guid;
}


 
    
//==========================================================
// login
//==========================================================


var frm = $('#formlogin');
    frm.submit(function (e) {

        e.preventDefault();
          beforeSubmit();
        $.ajax({
            type: frm.attr('method'),
            url:  '{{ secure_url('fastku/login') }}',
            data: frm.serialize(),
            success: function (data) {
                console.log('Submission was successful.');
                console.log(data);
               let hasil = JSON.stringify(data);
               let hasil2 = JSON.parse(data);
                console.log('hasil json stringify ='+hasil);
                console.log('hasil json parse  ='+hasil2);
                console.log('hasil error  ='+hasil2.errors);
                // console.log('hasil data  ='+hasil2.data.login);


               var result = '';
            // if( (hasil2.errors === null ) || ( hasil2.data.login !== null )) {
               if(typeof(hasil2.errors) === 'undefined'){
                console.log('not null.');
                // console.log('has ='+hasil2.data.login);
                        getPage('{{ secure_url('fastku/homeindex') }}');
                          // Swal.fire({
                          //   title: 'selamat!',
                          //   text: 'Selamat Datang joni',
                          //   icon: 'success',
                          //   confirmButtonText: 'Close'
                          //   });
            }
            else { 
              afterSubmit();
                console.log('null, data errors');
                    hasil2.errors.forEach(function(i) {
                        msg = `"${i.message}"`; 
                    });
         
                
                if (msg != null) {
                    
                            Swal.fire({
                            title: 'Error!',
                            text: msg,
                            icon: 'error',
                            confirmButtonText: 'Close'
                            });
                    
                }
                
            }


            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
    });

    
//==========================================================
// registration
//==========================================================


var frmreg = $('#frmsignup');
    frmreg.submit(function (e) {
console.log('start registration');
let pin = $("#pin");
let pin2 = $("#pin2");
// alert(pin.val().length);
 if(pin.val().length < 6)
{
	Swal.fire({
	    title: 'Error!',
	    text: 'Format password tidak valid (PIN 6 digits)',
	    icon: 'error',
	    confirmButtonText: 'Close'
    });
	return false;
} else if(pin.val() != pin2.val())
{
	Swal.fire({
	    title: 'Error!',
	    text: 'Pin Konfirmasi tidak sama',
	    icon: 'error',
	    confirmButtonText: 'Close'
    });
	return false;
} 


        e.preventDefault();
          // beforeSubmit();
        $('#registerbtn').attr('disabled', 'disabled');
        $('#registerload').show();
        $.ajax({
            type: frmreg.attr('method'),
            url:  '{{ secure_url('fastku/daftar') }}',            
            data: frmreg.serialize(),
            success: function (data) {
                  console.log('Registration was successful.');
                  const obj = JSON.parse(data); 

                // console.log(data);
               let hasil = JSON.stringify(data);
               let hasil2 = JSON.parse(data);
                // console.log('hasil json stringify ='+hasil);
                // console.log('hasil json parse  ='+hasil2);
                // console.log('hasil error  ='+hasil2.errors);
                // console.log('hasil data  ='+hasil2.data.login);


               var result = '';
            // if( (hasil2.errors === null ) || ( hasil2.data.login !== null )) {
               if(typeof(hasil2.errors) === 'undefined'){
                
				let msgotp = obj.data.signup.message;
				let txtuser_id = obj.data.signup.user_id;
                //{"data":{"signup":{"identity_type":"PHONENUMBER","message":"Kode token OTP/PIN untuk verifikasi dikirim ke nomor telpon +6285311223*** melalui SMS","user_id":"QRJIA2","username":"6285311223131"}}}

                // console.log('has ='+hasil2.data.login);
                        // getPage('{{ secure_url('fastku/homeindex') }}');

                          // Swal.fire({
                          //   title: 'OTP',
                          //   text: msgotp,
                          //   icon: 'success',
                          //   confirmButtonText: 'Close'
                          //   });
                          	$(frmpin).removeClass("hide"); 
						    $(frmregis).addClass("hide");
							$("#frmregis").trigger("reset");
 						    $("#useridotp").val(txtuser_id); 						    
						    
						    //frmotp
						    //useridotp
 


            }
            else { 
              afterSubmit();
                console.log('null, data errors');
                //{"data":{"signup":null},"errors":[{"message":"[1109] Captcha tidak valid","locations":[{"line":2,"column":11}],"path":["signup"]}]}

                    hasil2.errors.forEach(function(i) {
                        msg = `"${i.message}"`; 
                    });
         
                
                if (msg != null) {
                    
                            Swal.fire({
                            title: 'Error!',
                            text: msg,
                            icon: 'error',
                            confirmButtonText: 'Close'
                            });
                    
                }
					$('#registerbtn').removeAttr('disabled', 'disabled');
					$('#registerload').hide();
                
            }


            },
            error: function (data) {
                console.log('An error occurred.');
                 let hasil = JSON.stringify(data);
               // let hasil2 = JSON.parse(data);
               for (var i = 0; i < data.length; ++i) {
                    console.log(data[i].responseText);
                }

                console.log(hasil);
                console.log(hasil2);
                console.log(data.responseText);
            },
        });
    });

//==========================================================
// otp send
//==========================================================

var frmotps = $('#frmotp');
    frmotps.submit(function (e) {

        e.preventDefault();
          // beforeSubmit();
          $('#sendpin').hide();
        $.ajax({
            type: frmotps.attr('method'),
            url:  '{{ secure_url('fastku/sendotp') }}',
            data: frmotps.serialize(),
            success: function (data) {
                console.log('sending OTP was successful.');
                console.log(data);
                 const obj = JSON.parse(data); 
				
				 //{"data":{"verifyPIN":{"user_status":"ACTIVE","username":"6285311223131"}}}
               let hasil = JSON.stringify(data);
               let hasil2 = JSON.parse(data);
                console.log('hasil json stringify ='+hasil);
                console.log('hasil json parse  ='+hasil2);
                console.log('hasil error  ='+hasil2.errors);
                // console.log('hasil data  ='+hasil2.data.login);


               var result = '';
            // if( (hasil2.errors === null ) || ( hasil2.data.login !== null )) {
               if(typeof(hasil2.errors) === 'undefined'){
                console.log('not null.');
                 let status = obj.data.verifyPIN.user_status;
				 let username = obj.data.verifyPIN.username;
                // console.log('has ='+hasil2.data.login);
                        // getPage('{{ secure_url('fastku/homeindex') }}');
                          Swal.fire({
                            title: 'selamat!',
                            text: 'Selamat Akun '+username+' Telah Aktif, Silahkan login',
                            icon: 'success',
                            confirmButtonText: 'Close'
                            });

                          	$(frmpin).removeClass("hide"); 
						    $(frmregis).addClass("hide");
							$("#frmregis").trigger("reset");
 
                          // getPage('{{ secure_url('fastku/login') }}'); 

            }
            else { 
              afterSubmit();
                console.log('null, data errors');
                    hasil2.errors.forEach(function(i) {
                        msg = `"${i.message}"`; 
                    });
         
                
                if (msg != null) {
                    
                            Swal.fire({
                            title: 'Error!',
                            text: msg,
                            icon: 'error',
                            confirmButtonText: 'Close'
                            });
                    
                }
                $('#sendpin').show();
            }


            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
    });


//==========================================================
// lupa password
//==========================================================


var frmlup = $('#formforgot');
    frmlup.submit(function (e) {

        e.preventDefault();
        $('#submitforgot').attr('disabled', 'disabled');

        $.ajax({
            type: frmlup.attr('method'),
            url:  '{{ secure_url('fastku/forgot') }}',
            data: frmlup.serialize(),
            success: function (data) {
                console.log('Forgot Password was successful.');
                console.log(data);
                const obj = JSON.parse(data); 
               let hasil = JSON.stringify(data);
               let hasil2 = JSON.parse(data);
                console.log('hasil json stringify ='+hasil);
                console.log('hasil json parse  ='+hasil2);
                console.log('hasil error  ='+hasil2.errors);
                // console.log('hasil data  ='+hasil2.data.login);
// forgotPassword

               var result = '';
            // if( (hasil2.errors === null ) || ( hasil2.data.login !== null )) {
               if(typeof(hasil2.errors) === 'undefined'){
                console.log('not null.');
               
                 let status = obj.data.forgotPassword.code;
				 let msg = obj.data.forgotPassword.message;
                // console.log('has ='+hasil2.data.login);
                        // getPage('{{ secure_url('fastku/homeindex') }}');
                          Swal.fire({
                            title: 'Lupa PIN!',
                            text: msg,
                            icon: 'success',
                            confirmButtonText: 'Close'
                            });

                          	$(formforgot).removeClass("hide"); 
						    $(formlogin).addClass("hide");
							$(formforgot).trigger("reset");
		$('#submitforgot').removeAttr('disabled', 'disabled');

            }
            else { 
              afterSubmit();
                console.log('null, data errors');
                    hasil2.errors.forEach(function(i) {
                        msg = `"${i.message}"`; 
                    });
         
                
                if (msg != null) {
                    
                            Swal.fire({
                            title: 'Error!',
                            text: msg,
                            icon: 'error',
                            confirmButtonText: 'Close'
                            });
                    
                }
                		$('#submitforgot').removeAttr('disabled', 'disabled');

                
            }


            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
    });


// template pin otp



(function($) {
  //Declare our function
  $.fn.validatePin = function(options) {
    var defaults = {
      //Default Settings
      numericKeyboardOnMobile: false,
      blurOnSuccess: false,

      //Declaring our callback functions
      onSuccess: function() {},
      onFailure: function() {}
    };

    var settings = $.extend({}, defaults, options);

    //Cache the DOM into a jquery object so that repetitive scanning of DOM won't be necessary
    var $wrapper = $(this),
      $el = $wrapper.find('[data-role="pin"]'),
      $elCount = $wrapper.find('[data-role="pin"]').length;
    pin = "";

    $el.each(function() {
      pin += ".";
    });

    //Event Initializations
    bindEvents();

    //Function Declarations
    function bindEvents() {
      $($el).on("focus", function() {
        selectText(this);
      });

      if (checkForMobileDevices()) {
        $($el).on("keyup", function(e) {
          var $that = this;
          validateUserInput(e, $that, "keypress");
        });
      } else {
        $($el).on("keypress", function(e) {
          var $that = this;
          setTimeout(function() {
            validateUserInput(e, $that, "keypress");
          }, 0);
        });
      }
      $($el).on("keydown", function(e) {
        var $that = this;
        setTimeout(function() {
          validateUserInput(e, $that, "keydown");
        }, 0);
      });
    }

    //Select the text in an input field
    function selectText(obj) {
      var value = $(obj).val();
      if (!checkForMobileDevices() && $.trim(value) != "") {
        $(obj).select();
      }
    }

    //Validate User Input
    function validateUserInput(e, obj, event) {
      var keycode = e.charCode || e.keyCode || e.which;
      var prevInput = $(obj).prev('[data-role="pin"]'),
        nextInput = $(obj).next('[data-role="pin"]'),
        index = $(obj).index(),
        value = $(obj).val(),
        empty;

      if (event == "keydown") {
        //Case - User Hits Left Arrow
        if (keycode === 37) {
          $(prevInput).focus();
          selectText(prevInput);
        } else if (keycode === 39) {
          //Case - User Hits Right Arrow
          $(nextInput).focus();
          selectText(nextInput);
        }

        if ($.trim(value) == "") {
          if (keycode === 8) {
            $(prevInput).focus();
            settings.onFailure.call(this);
          }
        } else {
          return false;
        }
      }

      if (event == "keypress") {
        if (keycode == 0) {
          return false;
        }

        //Case - User Enters an alphabet or a special character
        if (
          (keycode >= 65 && keycode <= 90) ||
          (keycode >= 186 && keycode <= 222)
        ) {
          e.preventDefault();
        }

        //Case - User enters a number from the main keypad or the numpad
        if (
          (keycode >= 48 && keycode <= 57) ||
          (keycode >= 96 && keycode <= 105)
        ) {
          pin = $.trim(pin.replace(/\s/g, ""));
          pin = pin.split("");
          pin[index] = value;
          pin = pin.join("");

          $(nextInput).focus();

          if (!checkForMobileDevices()) {
            setTimeout(function() {
              $(obj).val("•");
            }, 200);
          } else {
            $(obj).val("•");
          }
        }

        var empty = $($el).filter(function() {
          return this.value === "";
        });

        if (empty.length) {
          settings.onFailure.call(this);
        } else {
          settings.onSuccess.call(this);
          //Check if the user wants to move the focus out of the inputs on success
          if (settings.blurOnSuccess) {
            $($el).blur();
          }
        }
      }

      //Check if default settings have been overrided by the user

      //Prompts a numberic keyboard on mobile
    }

    function checkForMobileDevices() {
      if (
        /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
          navigator.userAgent
        )
      ) {
        return true;
      } else {
        return false;
      }
    }

    if (settings.numericKeyboardOnMobile) {
      if (checkForMobileDevices()) {
        $el.prop("type", "tel");
      }
    }
  };
})(jQuery);

$(document).ready(function() {
  $(".pin-wrapper").validatePin({
    numericKeyboardOnMobile: true,
    blurOnSuccess: true,
    onSuccess: function() {
      $(".pin").html(pin);
      $("#isiotp").val(pin);
    },
    onFailure: function() {
      $(".pin").html("");
    }
  });
});

</script>


    </body>
</html>
