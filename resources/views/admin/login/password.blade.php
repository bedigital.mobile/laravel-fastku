@extends('admin.main_agent.layout')

@section('content')
 
<script src="js/charisma.js"></script>
<script src="gen/jquery/dist/jquery.min.js"></script>

<style>
    .col-md-6.center {
    text-align: center;
}
</style>

<ul class="breadcrumb">
     <li>Change Password</li>
 </ul>


<br><br> 
   <form  id="frms" name="frms" > 
    <div class="row">
  <div class="form-group col-md-12">   
    <label style="font-size: 15px" for="from" class="col-md-3">New Password</label>
    <input type="password" name="new_pswd" id="new_pswd" required="" class="form-control col-md-3">
  </div> 
<div class="form-group col-md-12">   
    <label style="font-size: 15px" for="from"  class="col-md-3">Confirm New Password</label>
    <input type="password" name="new_pswd2" id="new_pswd2" required="" class="form-control col-md-3">
</div> 
 
</div>     

<div class="row">
      <div class=" col-md-6 center">
                        <button type="button" id="savedatas" name="savedatas" class="btn btn-success">Save</button>
        </div>
</div>



      </form> 


<div id="msginfo"></div> 
<div id="msg"></div> 


<script type="text/javascript"> 
     
  
    // function doUpdPass() {     
   
        //   sendForm2({
        //     method: 'POST',
        //     form: 'frms',
        //     messageView: 'msginfo',            
        //     messageSuccess: 'Password Update Saved',
        //     url: '{{ secure_url('/ma/update_c_password.html') }}',
        //     redirect:  '/ma/c_password.html',
        //     beforeSubmit: function () {                
        //         beforeSubmit();
        //     },
        //     afterSubmit: afterSubmit
        // }); 


   $("#savedatas").on('click', function(e) {
    e.preventDefault();
    
    var data = $("#frms").serialize();

     if (confirm('Are you sure to Save?')) {
        var news = $("#new_pswd").val();
        // alert(news);
        var new2 = $("#new_pswd2").val();
        if(news != new2)
        {
            alert("Konfirmasi Password tidak sama");
           
            $("#new_pswd2").val('');
            $("#new_pswd2").focus(); return false;
        }
        else
        {
         $.ajax({
            type: "post",
            url: "{{ secure_url('/ma/update_c_password.html') }}",
            data: data,
            dataType: "json",
            success: function(data) {
                console.log('Data Tersimpan');
                 alert("Password Baru Tersimpan");
               
                 $('#frms')[0].reset();

                 
            },
            error: function(error) {
                console.log('error');
            }
            });
        }
     }
 });
        


    function beforeSubmit() {
        $('#button-close').attr('disabled', 'disabled');
        $('#sbmt_upd').attr('disabled', 'disabled');
        $('#sbmt_upd').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Saving...');
    }

    function afterSubmit() {
        $('#button-close').removeAttr('disabled');
alert("saved");
        $('#sbmt_upd').removeAttr('disabled');
        $('#sbmt_upd').html('<i class="glyphicon glyphicon-save"></i> Save');
    }
 
</script>
@endsection