<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>PaketkuPOS</title>
<link rel="icon" 
      type="image/png" 
      href="favicon.ico">
  <!-- Custom fonts for this template-->
  <link href="admin_template/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="admin_template/css/styles.css" rel="stylesheet"> 
  <link href="admin_template/css/sb-admin-2-amanah.css" rel="stylesheet">
 
 @stack('styles')
 <style>
 .h2, h2 {
    font-size: 1rem;
}

.box {
    margin-top: 10px;
    margin-bottom: 10px;
}

.box-inner {
    border: 1px solid #DEDEDE;
    border-radius: 3px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    box-shadow: 0 0 10px rgba(189, 189, 189, 0.4);
    -webkit-box-shadow: 0 0 10px rgba(189, 189, 189, 0.4);
    -moz-box-shadow: 0 0 10px rgba(189, 189, 189, 0.4);
}

.box-header {
    border: none;
    padding-top: 5px;
    border-bottom: 1px solid #DEDEDE;
    border-radius: 3px 3px 0 0;
    -webkit-border-radius: 3px 3px 0 0;
    -moz-border-radius: 3px 3px 0 0;
    height: 35px;
    min-height: 35px !important;
    margin-bottom: 0;
    font-weight: bold;
    font-size: 16px;
    background: -moz-linear-gradient(top, rgba(255, 255, 255, 0) 0%, rgba(0, 0, 0, 0.1) 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(255, 255, 255, 0)), color-stop(100%, rgba(0, 0, 0, 0.1)));
    background: -webkit-linear-gradient(top, rgba(255, 255, 255, 0) 0%, rgba(0, 0, 0, 0.1) 100%);
    background: -o-linear-gradient(top, rgba(255, 255, 255, 0) 0%, rgba(0, 0, 0, 0.1) 100%);
    background: -ms-linear-gradient(top, rgba(255, 255, 255, 0) 0%, rgba(0, 0, 0, 0.1) 100%);
    background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 0%, rgba(0, 0, 0, 0.1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00ffffff', endColorstr='#1a000000', GradientType=0);

}

.box-header h2 {
    font-size: 15px;
    width: auto;
    clear: none;
    float: left;
    line-height: 25px;
    white-space: nowrap;
    font-weight: bold;
    margin-top: 0;
    margin-bottom: 0;padding-left: 15px;
}

.box-header h3 {
    font-size: 13px;
    width: auto;
    clear: none;
    float: left;
    line-height: 25px;
    white-space: nowrap;
}

.box-header h2 > i {
    margin-top: 1px;
}

.box-icon {
    float: right;
}

.box-icon a {
    clear: none;
    float: left;
    margin: 0 2px;
    height: 20px;
    width: 5px;
    margin-top: 1px;
}

.box-icon a i {
    margin-left: -6px;
    top: -1px;
}

.box-content {
    padding: 10px;
}

.btn-round {
    border-radius: 40px;
    -webkit-border-radius: 40px;
    -moz-border-radius: 40px;
    font-size: 12px;
    padding-top: 4px;
}

.sidebar-brand-text.mx-3 {
    text-align: left!important;
}
.sidebar .sidebar-brand {
padding: 1rem!important;
height: 1rem!important;
  }

  footer.sticky-footer.bg-orange {
    background-color: #f76b1c;
    color: #ffffff;
}

footer.sticky-footer {
    padding: 1rem 0!important;
    -ms-flex-negative: 0;
    flex-shrink: 0;
}


</style>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center" href="#">
        <div class="sidebar-brand-icon">
          <!-- <i class="fas  fa-home "></i>  -->
        </div>
        <!-- <div class="sidebar-brand-text mx-3">BAKRIE<br>AMANAH</div> -->
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="#">
          <i class="fas fa-fw fa-home"></i>
          <span>Beranda</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Layanan
      </div>


      <li class="nav-item">
        <a class="nav-link" href="registrasi" target="_blank">
          <i class="fas fa-fw fa-mobile"></i>
          <span>Daftar Online</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="verifikasi">
          <i class="fas fa-fw fa-calendar-check"></i>
          <span>Verifikasi</span></a>
      </li> 

      <li class="nav-item">
        <a class="nav-link" href="listgerai">
          <i class="fas fa-fw fa-calendar"></i>
          <span>List Gerai</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="summary">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Report</span></a> 
      </li>

  
  <!-- Heading -->
      <div class="sidebar-heading">
        Riwayat
      </div>


      <li class="nav-item">
        <a class="nav-link" href="charts.html">
          <i class="fas fa-fw fa-calculator"></i>
          <span>Transaksi</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="charts.html">
          <i class="fas fa-fw fa-credit-card"></i>
          <span>Deposit</span></a>
      </li> 

   <!-- Heading 
      <div class="sidebar-heading">
        Lain-lain
      </div>


      <li class="nav-item">
        <a class="nav-link" href="charts.html">
          <i class="fas fa-fw fa-users"></i>
          <span>Tentang Kami</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="charts.html">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Hubungi Kami</span></a>
      </li>  

       <li class="nav-item">
        <a class="nav-link" href="charts.html">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Ubah Sandi</span></a>
      </li> 
       <li class="nav-item">
        <a class="nav-link" href="charts.html">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Keluar</span></a>
      </li> 
-->
  
      
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>






    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

<img src="img/logo_paketku.png">
               <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-id-badge fa-sm text-white-50"></i>  {{ $session->get('group') }} </a> -->
               
          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">


            


            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <!-- Nav Item - User Information --> 
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Welcome,  {{ $session->get('username') }} </span>
                <img class="img-profile rounded-circle" src="https://image.flaticon.com/icons/svg/181/181549.svg">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                  
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
 {{ $session->get('username') }}
                </a>

                 <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ secure_url('/amanah/c_password.html') }}" >
                  <i class="fas fa-key fa-sm fa-fw mr-2 text-gray-400"></i> 
                  Change Password
                </a>

                 <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ secure_url('/amanah/logout.html') }}" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">  

            <div id="msginfo"></div>  
            @yield('content')
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-orange">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; paketkuPOS 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login">Logout</a>
        </div>
      </div>
    </div>
  </div>
<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script> -->

  <!-- Bootstrap core JavaScript
  <script src="admin_template/vendor/jquery/jquery.min.js"></script>-->
  <script src="gen/jquery/dist/jquery.min.js"></script> 
  <script src="admin_template/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="admin_template/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="admin_template/js/sb-admin-2.min.js"></script>
  <script src="js/jquery-1.9.1.js"></script>
    
  <!-- Page level plugins -->
  <script src="admin_template/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts --> 
  <script src="admin_template/js/demo/chart-pie-demo.js"></script>
  
<script src="../agency/js/common_1.js"></script>

@stack('scripts')
</body>

</html>
