@extends('layout')

@prepend('scripts')
<script>
    function beforeSubmit() {
        $('#button-close').attr('disabled', 'disabled');

        $('#button-save').attr('disabled', 'disabled');
        $('#button-save').html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif"> Saving...');
    }

    function afterSubmit() {
        $('#button-close').removeAttr('disabled');

        $('#button-save').removeAttr('disabled');
        $('#button-save').html('<i class="glyphicon glyphicon-save"></i> Save');
    }

    function doEdit() {
        sendForm2({
            method: 'POST',
            form: 'frm',
            messageView: 'msg',
            url: '{{ url('/wlbalancetype_edit.html') }}',
            redirect:  '{{ url('/wlbalancetype.html') }}',
            beforeSubmit: beforeSubmit,
            afterSubmit: afterSubmit
        });
    }
    
</script>
@endprepend
