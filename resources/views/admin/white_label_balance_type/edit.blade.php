@extends('white_label_balance_type.layout')

@section('content')
    <div>
        <ul class="breadcrumb">
            <li><a href="{{ url('/home.html') }}">Home</a></li>
            <li><a href="javascript:getPage('{{ url('/wlbalancetype.html') }}');">Balance Types</a></li>
            <li>Edit</li>
        </ul><!-- /.breadcrumb -->      
    </div>

    <div class="row-fluid">
        <form role="form" id="frm" onsubmit="doEdit();return false;">
            @csrf
            <input type="hidden" name="id" value="{{ $rs->wlb_id }}" />

            <div class="box col-md-12">
                <div class="box-inner">                
                    <div class="box-content">
                        <div id="msg"></div>            
                        <div class="clearfix"></div>
                        <h4 class="blue">Edit Product</h4>
                        <hr/>

                        <div class="form-group col-md-12">
                            <label for="balance-name"> Balance Name </label>

                            <div class="clearfix"></div>

                            <span class="form-control" id="balance-name">{{ $rs->balance_name }}</span>
                        </div>
                        
                        <div class="clearfix"></div>

                        <div class="form-group col-md-12" id="box-alias">
                            <label for="alias"> Alias</label>

                            <input class="form-control" type="text" name="alias" id="alias" placeholder="Alias for balance name" value="{{ $rs->balance_name_alias }}" />
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-3" id="box-curcode">
                            <label for="paymode"> Pay Mode</label>

                            <div class="clearfix"></div>
                            
                            <span class="form-control">{{ $rs->paymode }}</span>
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-3" id="box-curcode">
                            <label for="currency"> Currency Code</label>

                            <div class="clearfix"></div>
                            
                            <span class="form-control">{{ $rs->currency_code }}</span>
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-12">
                            <label for="description"> Description</label>

                            <input class="form-control" type="text" name="description" id="description" placeholder="Description" value="{{ $rs->description }}" />
                        </div>
                        
                        <div class="clearfix"></div>
                                                
                        <div class="form-group col-md-3">
                            <label for="status">Status</label>
                            
                            <div class="clearfix"></div>

                            <div class="input-group {{ ($rs->is_active == 1 ? 'has-success' : 'has-error') }}">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-flag"></i></span>
                                <span class="form-control {{ ($rs->is_active == 1 ? 'green' : 'red') }}">{{ ($rs->is_active == 1 ? 'Active' : 'Inactive') }}</span>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr/>
                        
                        <div class="form-actions">
                            <a href="javascript:getContentInBox('{{ url('/wlbalancetype.html') }}');" class="btn btn-warning btn-sm pull-left" id="button-close"><i class="glyphicon glyphicon-arrow-left"></i> Close</a>
                            &nbsp;
                            <button class="btn btn-info btn-sm pull-right" type="submit" id="button-save"><i class="glyphicon glyphicon-save"></i> Save</button>
                        </div>
                        <div class="clearfix"></div>
                        
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
