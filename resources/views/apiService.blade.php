<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<style>

body {font-family: "Lato", sans-serif;}

#myInput {
  background-image: url('img/search.png');
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

li{
  padding-left: 12px;
  color: #D14;
}

.main-content{
  margin: auto;
    width: 90%;
    border: 3px solid #F1F1F1;
    padding: 10px;
}

.tablink {
    background-color: #555;
    color: white;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    font-size: 12px;
    width: 12%;
}

.tablink:hover {
    background-color: #777;
}

/* Style the tab content */
.tabcontent {
    color: black;
    display: none;
    padding: 10px;
}

.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 10px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}

.box{
    background-color: #F7F7F9;
    width: 600px;
    border: 2px solid grey;
}

#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}

table, th, td {
                border: 1px solid black;
                padding: 5px;
            }

#Oslo {background-color:white;}
#London {background-color:white;}
#Paris {background-color:white;}
#Tokyo {background-color:white;}
#Jakarta {background-color:white;}
#Jogja {background-color:white;}
#Bandung {background-color:white;}
#Makasar {background-color: white;}
</style>
</head>
<body>
<div class="main-content">
<div class="container">
  <div align="center">
    <h1>Sandbox POSFIN H2H Platform</h1>
  </div>
</div>
<div class="container">
<button class="tablink" onclick="openCity('Oslo', this, 'Green')" id="defaultOpen">Api Service</button>
<button class="tablink" onclick="openCity('London', this, 'Green')" >Overview</button>
<button class="tablink" onclick="openCity('Paris', this, 'Green')">Authentication</button>
<button class="tablink" onclick="openCity('Tokyo', this, 'Green')">Api Parameter</button>
<button class="tablink" onclick="openCity('Jakarta', this, 'Green')">Errors Code</button>
<button class="tablink" onclick="openCity('Jogja', this, 'Green')">Data Dummy</button>
<button class="tablink" onclick="openCity('Bandung', this, 'Green')">Daftar Product</button>
<button class="tablink" onclick="openCity('Makasar', this, 'Green')">Verified Product</button>

<div id="Oslo" class="tabcontent">
  <h4 align="center">Berikut beberapa parameter yang harus anda kirim untuk API BWN :</h4>
    
    <h5>Request</h5>
    <form method="post" action="{{ secure_url('/h2h/docs/apiservice') }}" target="">
      API Key:
      <input type="text" id="api_key" name="api_key" style="padding: 5px; margin-bottom: 10px; width:30%; margin-left: 30px;">
      <br>
      Token:
      <input type="text" id="token" name="token" style="padding: 5px; margin-bottom: 10px; width:30%; margin-left: 45px;">
      <br>
      Signature:
      <input type="text" id="sign" name="sign" style="padding: 5px; margin-bottom: 10px; width:30%; margin-left: 20px;">
      <br>
      Type:
      <input type="text" list="types" id="type" name="type" style="padding: 5px; margin-bottom: 10px; width:30%; margin-left: 55px;">
      <datalist id="types">
        <option value="inquiry">
        <option value="payment">
        <option value="purchase">
        <option value="balance">
      </datalist>
      <br>
      Product:
      <input type="text" id="product" name="product" style="padding: 5px; margin-bottom: 10px; width:30%; margin-left: 35px;">
      <br>
      Bill Number:
      <input type="text" id="billNumber" name="billNumber" style="padding: 5px; margin-bottom: 10px; width:30%; margin-left: 10px;">
     <br>
      Callback Url:
       <input type="text" id="callback_url" name="callback_url" style="padding: 5px; margin-bottom: 10px; width:30%; margin-left: 5px;">
      <br><br>
      <input type="submit" value="Submit" >
      </form>
      <div>
        <p>Response :</p>
        <pre id="myResponse" name="myResponse">{!! $response !!}</pre>
      </div>
      <!-- <a id="send-thoughts" href="">Lihat Json</a> -->
</div>

<div id="London" class="tabcontent">

  <h4 align="center">Berikut Overview dari POSFIN H2H SandBox</h4>
      <table id="customers">
<tr>
  <th>Method</th>
  <th>Endpoint</th>
  <th>Usage</th>
</tr>
<tr>
  <td>POST</td>
  <td>https://h2h-stg.bedigital.co.id/h2h/trx/request</td>
  <td>API H2H Messaging</td>
</tr>
</table>
</div>

<div id="Paris" class="tabcontent">
  <h4 align="center">HTTP Method</h4>
    <h4 align="center">* HTTP Method yang digunakan adalah POST.</h4>
  </br>
<h4 align="center">Headers</h4>
<h4 align="center">API POSFIN H2H : </h4>
</br>
      <table id="customers">
          <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Description</th>
          </tr>
          <tr>
            <td>Content-Type</td>
            <td>Alphanumeric</td>
            <td>application/json)</td>
          </tr>
      </table>
    </br>
</div>

<div id="Tokyo" class="tabcontent">
  <h4 align="center">Berikut beberapa parameter yang harus anda kirim untuk API H2H :</h4>
    </br>
    <h4>Request</h4>
      <table id="customers">
          <tr>
            <th>Field</th>
            <th>Type</th>
            <th>Mandatory</th>
            <th>Description</th>
          </tr>
          <tr>
            <td>api_key</td>
            <td>String(50)</td>
            <td>Y</td>
            <td>Identitas Partner (Hubungi Admin)</td>
          </tr>
          <tr>
            <td>token</td>
            <td>String(50)</td>
            <td>Y</td>
            <td>Hasil encrip md5 dari secret key + api key + unix epoch time dan berlaku hanya 30 detik</td>
          </tr>
          <tr>
            <td>signature</td>
            <td>String(100)</td>
            <td>Y</td>
            <td>Tidak wajib di isi jika diisi harus unik</td>
          </tr>
          <tr>
            <td>type</td>
            <td>String(10)</td>
            <td>Y</td>
            <td>Tipe Transaksi. Available types are <b>inquiry , payment , purchase , balance</b></td>
          </tr>
          <tr>
            <td>product</td>
            <td>String(100)</td>
            <td>Y</td>
            <td>Product Code , Mandatory Kecuali untuk type transaksi balance</td>
          </tr>
          <tr>
            <td>billNumber</td>
            <td>String(50)</td>
            <td>Y</td>
            <td>Customer Id , Mandatory Kecuali untuk type transaksi balance</td>
          </tr>
          <tr>
            <td>callback_url</td>
            <td>String(max.255)</td>
            <td>N</td>
            <td>full http url format (for async connection only) example: http://example.com/callback</td>
          </tr>
      </table>
      <br>
      Contoh Request Sync:
      <div>
      <br>
      <div class="box">
        <ol class="linemuns">
          <li>
            <ol style="background-color: #FBFBFC">
              <li>{</li>
              <li>"api_key":"8ab0cfc3efe7d3ec4a43072be066f5adc514ac0c",</li>
              <li>"token":"8022711df33c51fd667e46f7a3769abc",</li>
              <li>"type":"inquiry",</li>
              <li>"product":"2042",</li>
              <li>"billNumber":"061218100242",</li>
              <li>"sign":"Jakarta1990"</li>
              <li>}</li>
            </ol>
          </li>
        </ol>
      </div>
      </div>
      <br>
      Contoh Request Async:
      <div>
        <br>
        <div class="box">
        <ol class="linemuns">
          <li>
            <ol style="background-color: #FBFBFC">
              <li>{</li>
              <li>"api_key":"8ab0cfc3efe7d3ec4a43072be066f5adc514ac0c",</li>
              <li>"token":"8022711df33c51fd667e46f7a3769abc",</li>
              <li>"type":"inquiry",</li>
              <li>"product":"2042",</li>
              <li>"billNumber":"061218100242",</li>
              <li>"sign":"Jakarta1990",</li>
              <li>"callback_url":"http://example.com/callback"</li>
              <li>}</li>
            </ol>
          </li>
        </ol>
      </div>
      </div>
</br>
<h4>Response</h4>
  <table id="customers">
      <tr>
        <th>Field</th>
        <th>Data Field</th>
        <th>Description</th>
      </tr>
      <tr>
        <td>type</td>
        <td>String</td>
        <td>Type Transaction</td>
      </tr>
      <tr>
        <td>service</td>
        <td>String</td>
        <td>Transaction service Name</td>
      </tr>
      <tr>
        <td>product</td>
        <td>String</td>
        <td>Product Code</td>
      </tr>
      <tr>
        <td>billNumber</td>
        <td>String</td>
        <td>Customer Number</td>
      </tr>
      <tr>
        <td>nominal</td>
        <td>String</td>
        <td>Nominal Transaction</td>
      </tr>
      <tr>
        <td>admin</td>
        <td>String</td>
        <td>Fee Admin</td>
      </tr>
      <tr>
        <td>paymentCode</td>
        <td>String</td>
        <td>Payment Code</td>
      </tr>
      <tr>
        <td>trxId</td>
        <td>String</td>
        <td>Transaction ID</td>
      </tr>
      <tr>
        <td>info1</td>
        <td>String</td>
        <td>Custumer Info</td>
      </tr>
      <tr>
        <td>info2</td>
        <td>String</td>
        <td>Customer Info</td>
      </tr>
      <tr>
        <td>info3</td>
        <td>String</td>
        <td>Customer Info</td>
      </tr>
      <tr>
        <td>jmlLembar</td>
        <td>String</td>
        <td>number of sheets transaction</td>
      </tr>
      <tr>
        <td>current_Balance</td>
        <td>String</td>
        <td>Saldo Terakhir Setelah Transaksi</td>
      </tr>
  </table>
  <h3> Please register your IP</h3> 
</div>

<div id="Jakarta" class="tabcontent">

  <h4 align="center">Berikut Errors list dari POSFIN H2H SandBox</h4>
      <table id="customers">
<tr>
  <th>Kode Response</th>
  <th>Pesan Response</th>
</tr>
      <tr>
        <td>0</td>
        <td>Sukses</td>
      </tr> 
      <tr>
        <td>400</td>
        <td>Invalid Parameter</td>
      </tr>
      <tr>
        <td>401</td>
        <td>Partner Tidak Aktif</td>
      </tr>
      <tr>
        <td>405</td>
        <td>Access Not Allowed</td>
      </tr>
      <tr>
        <td>500</td>
        <td>Internal Server error</td>
      </tr>
      <tr>
        <td>1005</td>
        <td>Insufficent balance</td>
      </tr>
      <tr>
        <td>1006</td>
        <td>Exceed maximum deposit</td>
      </tr>
      <tr>
        <td>4000</td>
        <td>Kode Product Tidak Valid</td>
      </tr>
      <tr>
        <td>4000</td>
        <td>Data Pelanggan Tidak Ditemukan</td>
      </tr>
</table>
</div>

<div id="Jogja" class="tabcontent">

  <h4 align="center">Berikut Data Dummy dari POSFIN H2H SandBox</h4>
  <br>
  <select name="dataDummy" id="dataDummy" onchange="onChangeListener()">
    <br>
    <option value=" "> </option>
    @foreach ($products as $dataProduct)
  <option value="{{ $dataProduct->prodid }}">{{ $dataProduct->product_name }} | {{ $dataProduct->prodid }}</option>
  @endforeach
</select>
<br>
<div id="view"></div>
<br>
@include('dataDummy')
<div id="Oslo" class="tabcontent">
  <h4>Berikut beberapa parameter yang harus anda kirim untuk API B2B :</h4>
    
    <h5>Request</h5>
    <form method="post" action="/trx/request" >
      API Key:<br>
      <input type="text" id="api_key" name="api_key">
      <br>
      Token:<br>
      <input type="text" id="token" name="token">
      <br>
      Signiture:<br>
      <input type="text" id="sign" name="sign">
      <br>
      Type:<br>
      <input type="text" id="type" name="type">
      <br>
      Product:<br>
      <input type="text" id="product" name="product">
      <br>
      Bill Number:<br>
      <input type="text" id="billNumber" name="billNumber">
     <!--  <br>
      Callback Url:<br>
       <input type="text" id="callback_url" name="callback_url">-->
      <br><br>
      <input type="submit" value="Submit" >
      </form>
      <div>
        <p>Response :</p>
        <p></p>
      </div>
</div>

<div id="Bandung" class="tabcontent">
  <h4 align="center">Berikut Daftar Product dari POSFIN H2H SandBox</h4>
 
      <table id="customers">
<tr>
  <th>Kode Product</th>
  <th>Daftar Product</th>
</tr>
      @foreach ($products as $dataProduct)
      <tr>
        <td>{{ $dataProduct->prodid }}</td>
        <td>{{ $dataProduct->product_name }}</td>
      </tr>
      @endforeach
</table>
</div>
<div id="Makasar" class="tabcontent">
  <h4 align="center">Berikut Daftar Product dari POSFIN H2H SandBox</h4>
 
      <table id="customers">
<tr>
  <th>Kode Product</th>
  <th>Type Transaksi</th>
  <th>Daftar Product</th>
  <th>Bill Number</th>
</tr>
      <tr>
        <td>6001 V010TSI</td>
        <td>Prepaid</td>
        <td>Telco</td>
        <td>082283909509</td>
      </tr>
      <tr>
        <td>5003</td>
        <td>Postpaid</td>
        <td>Telco</td>
        <td>08117508836 / 081916773888</td>
      </tr>
      <tr>
        <td>3008</td>
        <td>postpaid</td>
        <td>PLN</td>
        <td>141101138295</td>
      </tr>
      <tr>
        <td>3010</td>
        <td>Non tagihan</td>
        <td>PLN</td>
        <td>5114013033466</td>
      </tr>
      <tr>
        <td>3009 30094</td>
        <td>Prepaid</td>
        <td>PLN</td>
        <td>01101810834 / 423110025307</td>
      </tr>
      <tr>
        <td>2047</td>
        <td>  -  </td>
        <td>Multi Finance</td>
        <td>148900961615</td>
      </tr>
      <tr>
        <td>5001</td>
        <td>  -  </td>
        <td>Telkom</td>
        <td>0361974210</td>
      </tr>
      <tr>
        <td>4013</td>
        <td>  -  </td>
        <td>PDAM</td>
        <td>031150010052</td>
      </tr>
      <tr>
        <td>7012</td>
        <td>Postpaid</td>
        <td>Pay TV </td>
        <td>1060059862</td>
      </tr>
      <tr>
        <td>7013</td>
        <td>Prepaid</td>
        <td>Pay TV</td>
        <td>3000007127</td>
      </tr>
      <tr>
        <td>9007</td>
        <td>  -  </td>
        <td>BPJS</td>
        <td>0001266814405</td>
      </tr>
      <tr>
        <td>8019</td>
        <td>  -  </td>
        <td>TAX (pajak pbb)</td>
        <td>3271010008007039502016</td>
      </tr>
      <tr>
        <td>2042</td>
        <td>  -  </td>
        <td>Tokopedia</td>
        <td>AD081912992151</td>
      </tr>
      <tr>
        <td>5001</td>
        <td>  -  </td>
        <td>Telkom Jastel</td>
        <td>0111761101125</td>
      </tr>
</table>
</div>
<script>
function openCity(cityName,elmnt,color) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
    }
    document.getElementById(cityName).style.display = "block";
    elmnt.style.backgroundColor = color;

}
// Get the element with id="defaultOpen" and click on it

document.getElementById("defaultOpen").click();

document.addEventListener("DOMContentLoaded", function(event) {
    var input = document.getElementById("myResponse");
    var dataJSON = '{!! $response !!}';
    var json = JSON.parse(dataJSON);
    if (dataJSON.length != 0) {
      input.innerHTML = JSON.stringify(json, undefined, 2);
    }
});
</script>

<script>
function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("customers");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script type="text/javascript">
  function onChangeListener() {
    var x = document.getElementById("dataDummy").value;
    document.getElementById('view').innerHTML = document.getElementById('data_'+x).innerHTML;
}
</script>
</option>
</body>
</html> 
