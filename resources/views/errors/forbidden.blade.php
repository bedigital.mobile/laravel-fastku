 

@section('content')
<style type="text/css">
  .btn-group-lg>.btn, .btn-lg {
    padding: 9rem 9rem;
  }
  .btn-circle.btn-lg, .btn-group-lg>.btn-circle.btn {
    height: 3.5rem;
    width: 3.5rem;
    font-size: 9rem;
}
</style>
  <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center"><!-- <img src="https://fintechagent-stg.bedigital.co.id/swipe/img/posfin-logo.png" height="150px" style="background-color: #fff;padding: 10px;"> -->
              <a href="#" class="btn btn-danger btn-circle btn-lg">
                    <i class="fas fa-exclamation-triangle"></i>
              </a>
              <br><br>
               <h2>WARNING</h2>
              <p>This page is forbidden to access  <br>
                please contact Posfin administrator
              </p>
               <div class="mid_center">
              	<br>
              	<br>
              	<br>     
              </div>
            </div>
          </div>
        </div>

        <!-- /page content -->
@endsection